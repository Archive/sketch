import sys, os

datafile_basedir="Lib/site-packages/Sketch"

if len(sys.argv) > 1 and sys.argv[1] == "-install":
    try:
        start_menu = get_special_folder_path("CSIDL_COMMON_PROGRAMS")
    except OSError: # probably Win98
        start_menu = get_special_folder_path("CSIDL_PROGRAMS")

    sketch_dir=os.path.join(sys.prefix, datafile_basedir)
    sketch_py=os.path.join(sketch_dir, "Sketch.py")
    pythonw_exe=os.path.join(sys.prefix, "python.exe")

    shortcut_dir=os.path.join(start_menu, "Sketch")
    if not os.path.exists(shortcut_dir):
        os.mkdir(shortcut_dir)
        directory_created(shortcut_dir)

    shortcut_path=os.path.join(shortcut_dir, "Sketch.lnk")
    if not os.path.exists(shortcut_path):
        create_shortcut(pythonw_exe, "Sketch", shortcut_path, sketch_py, sketch_dir)
        file_created(shortcut_path)
