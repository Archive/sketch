

Some notes on the new architecture/design of Sketch.


General
=======

The code in Sketch/Graphics should contain only contain code for the
representation of the drawing.

All of the toolkit independent editing logic is in Sketch/Editor.

All toolkit/platform dependent stuff is in Sketch/UI. Perhaps this
should be split into abstract/generic window/event handling and toolkit
dependent code.


Objects
=======

Application
-----------

The application object manages the list of currently loaded documents
and their toplevel windows, the context (see below), the Sketch specific
cut-buffer, the toolbox, some standard dialogs like message box, file
open/save.




Context
-------

The context holds the _current state_ the application. This has a
toolkit independent part which is defined in Sketch/Editor and a
toolkit dependent part in Sketch/UI.

The toolkit independent part defines the active document, the active
editor and the active tool.

The toolkit dependent part defines the active toplevel window (which
displays the active document).


Public Attributes:

tool:		the active tool as a string (e.g. "SelectionTool")
document:	the active document (None if no document was loaded/created)
editor:		the active editor. document is editor.Document()
window:		the active toplevel window (shows document and uses editor)


Methods:

SetTool(tool)	Set the active tool. tool is a string



Canvas
------

The canvas is the window inside of a toplevel window where the drawing
is displayed and where the use edits the drawing.

The canvas takes care of handling expose events by redrawing the
document and it manages the viewport transformation (these things are
actually handled by the canvas' base classes). It also handles the user
events (mouse and keyboard), translates window coordinates to document
coordinates and forwards the mouse events to the editor (in doc-coords).

The canvas is also responsible for displaying the handles for the
document editor, to determine whether a mouse click hits a handle and to
change the cursor when the mouse is above a handle.



Editor
------

The editor and the tools implement the toolkit independent editing
logic. The editor accepts the mouse events from the canvas and forwards
them to the current tool object.

Methods:

Destroy()	to be called when the canvas/toplevel is closed

Handles()	return the list of handles for the current tool

Document()	return the document edited by the editor

Delete() 

	 Delete the currently selected objects or parts of objects
	 depending on the current tool. The editor simply calls the tool
	 method of the same name.


HasSelection() 

	return true if one or more objects are selected false otherwise.

Selection()

	return the selection object

DelayButtonPress()

	return true, if the canvas should try to distinguish between a
        simple mouse click and a drag.

	If this returns true, the canvas will not forward a button press
	event immediately; instead it waits for more mouse events. 

	If the user move the mouse more than a few pixels it finally
	forwards the press-event to the editor's ButtonPress method and
	immediately forwards the last motion event to PointerMotion, all
	following motion events are similarly forwarded until the user
	releases the mouse button. The release event is forwarded to the
	editor's ButtonRelease method

	If the user releases the button without moving the mouse more
	than a few pixels, the press-event is forwarded to the editor's
	ButtonClick method and the button release event is discarded.

	
	If this method returns false, mouse events are immediately
	forwarded to ButtonPress, PointerMotion and ButtonRelease
	respectively. The ButtonClick method will not be called.

	The return value is determined by the current tool and is the
	same as the return value of the DelayButtonPress method of the
	current tool object.

ButtonPress(context, p, button, state, handle = None)

	Called by the canvas when the user presses a mouse button. p is
	the position of the mouse pointer in doc-coords. handle, if not
	None, is the handle object that is hit by the button press.

	The editor object basically forwards the parameters to the
	editor method of the same name.

PointerMotion(context, p, state):

	Called by the canvas when the mouse pointer was moved during a
	drag. p is given in doc-coords

	The editor object basically forwards the parameters to the
	editor method of the same name.

ButtonRelease(context, p, button, state):

	Called by the canvas when the user releases a mouse button. p is
	the position of the mouse pointer in doc-coords.

	The editor object basically forwards the parameters to the
	editor method of the same name.

ButtonClick(context, p, button, state, handle = None):
		  
	Called by the canvas when the user performs a simple mouse
	click. p is the position of the mouse pointer in doc-coords.
	handle is like in ButtonPress

	The editor object basically forwards the parameters to the
	editor method of the same name.

Cancel(context)

	Cancel the current click/drag. Forwarded to the tool method of
	the same name.

ToggleSnapToGrid()

IsSnappingToGrid()

ToggleSnapToGuides()

IsSnappingToGuides()

ToggleSnapToObjects()

IsSnappingToObjects()



Tools
-----

The editor object has a tool object that defines the behavior of the
current tool.


__init__(editor)

	The contructor is called with the editor for which the tool is
	instantiated as parameter.

DelayButtonPress()

	See the editor method of the same name.
        

ButtonPress(context, p, snapped, button, state, handle = None)
PointerMotion(context, p, snapped, state)
ButtonRelease(context, p, snapped, button, state):
ButtonClick(context, p, snapped, button, state, handle = None):
        
	See the editor method of the same name. The additional parameter
	snapped is the snapped pointer position. If no snapping is
	active, its equal to p.

Cancel(context)

	See the editor method of the same name.

Delete()

	See the editor method of the same name.

End(context)

	Called by the editor when the user ends working with the current
	tool (and switches to a different one)



Selection Object
----------------

The selection object represents the set of currently selected objects

GetObjects()

	Return the selected objects as a list

