# Sketch - A Python-based interactive drawing program
# Copyright (C) 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the pstokenize module
"""

# TODO: Add more complex tests

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support

support.add_sketch_dir_to_path()

from Sketch.pstokenize import PSTokenizer, NAME, INT, FLOAT, STRING, \
OPERATOR, DSC_COMMENT, END
import streamfilter

def tokenize(ps):
    """Tokenize the postscript code ps."""
    decoder = streamfilter.StringDecode(ps, None)
    tokenizer = PSTokenizer(decoder)
    tokens = []
    while 1:
        token, value = tokenizer.next()
        if token != END:
            tokens.append((token, value))
        else:
            break
    return tokens


class TestPSTokenize(unittest.TestCase):

    def test_int(self):
        """Test PSTokenizer for int literals"""
        self.assertEquals(tokenize("123"), [(INT, 123)])
        self.assertEquals(tokenize("-654"), [(INT, -654)])

    def test_float(self):
        """Test PSTokenizer for float literals"""
        self.assertEquals(tokenize("123.5"), [(FLOAT, 123.5)])
        self.assertEquals(tokenize("-3.125"), [(FLOAT, -3.125)])

    def test_name(self):
        """Test PSTokenizer for names"""
        self.assertEquals(tokenize("/name"), [(NAME, "name")])
        self.assertEquals(tokenize("/.-*+:\\"), [(NAME, ".-*+:\\")])
        self.assertEquals(tokenize("/123abc"), [(NAME, "123abc")])
        self.assertEquals(tokenize("/"), [(NAME, "")])

    def test_string(self):
        """Test PSTokenizer for string literals"""
        self.assertEquals(tokenize("()"), [(STRING, "")])
        self.assertEquals(tokenize("(())"), [(STRING, "()")])
        self.assertEquals(tokenize("(\()"), [(STRING, "(")])
        self.assertEquals(tokenize("(\000\001\040)"),
                          [(STRING, "\x00\x01\x20")])

    def test_hex_string(self):
        """Test PSTokenizer for hex string literals"""
        self.assertEquals(tokenize("<>"), [(STRING, "")])
        self.assertEquals(tokenize("<ABCDEF>"), [(STRING, "\xab\xcd\xef")])
        self.assertEquals(tokenize("< 09 ab\nc\tdef>"),
                          [(STRING, "\x09\xab\xcd\xef")])
        self.assertRaises(SyntaxError, tokenize, "<09184323adsf>")

    def test_operator(self):
        """Test PSTokenizer for operators"""
        self.assertEquals(tokenize("showpage"), [(OPERATOR, "showpage")])
        self.assertEquals(tokenize("["), [(OPERATOR, "[")])
        self.assertEquals(tokenize("]"), [(OPERATOR, "]")])
        self.assertEquals(tokenize("<<"), [(OPERATOR, "<<")])
        self.assertEquals(tokenize(">>"), [(OPERATOR, ">>")])
        self.assertEquals(tokenize("123cde"), [(OPERATOR, "123cde")])

    def test_dsc_comments(self):
        """Test PSTokenizer for DSC comments"""
        self.assertEquals(tokenize("%%BoundingBox: 0 1 2 3"),
                          [(DSC_COMMENT, "BoundingBox: 0 1 2 3")])

    def test_comments(self):
        """Test PSTokenizer for comments"""
        self.assertEquals(tokenize("def%bla"), [(OPERATOR, 'def')])

if __name__ == "__main__":
    unittest.main()
