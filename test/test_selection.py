# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test cases for the selection
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Editor.selection import Selection
from Sketch.Editor import InteractiveDocument
from Sketch.Graphics import Rectangle, Group
from Sketch import Trafo


class Rectangle1(Rectangle):

    """Rectangle with a simple Info text"""

    def Info(self):
        """Override the Info method to return a simple string"""
        return "Rectangle"


class Rectangle2(Rectangle):

    """Rectangle with a more complex Info text"""

    def Info(self):
        """Override the Info method to return a string and a dict."""
        return "%(name)s", {"name": "Rectangle"}



class TestInfoText(unittest.TestCase):

    def setUp(self):
        """Create a document with three rectangles and two layers.

        The layers are 'Background' and 'Foreground' and are bound to
        layer1 and layer2 repectively.

        The rectangles are rect11 and rect12 on layer1 and self.rect2 on
        layer2. The rectangles are actually instances of Rectangle1 resp
        rectangle2 so that it's easier to compare InfoText results.

        The document is bound to doc.
        """
        # First build a document
        self.doc = InteractiveDocument(create_layer = 1)

        # Create two layers
        self.layer1 = self.doc.AppendLayer("Background")
        self.layer2 = self.doc.AppendLayer("Foreground")

        # Insert a rectangle into each
        self.rect11 = Rectangle1()
        self.layer1.Insert(self.rect11)
        self.rect12 = Rectangle1()
        self.layer1.Insert(self.rect12)

        self.rect2 = Rectangle2()
        self.layer2.Insert(self.rect2)

    def test_info_text_no_object(self):
        """Test Selection.InfoText with no object"""
        # Create the selection. Initially selections are empty
        # all
        sel = Selection(self.doc)

        self.assertEquals(sel.InfoText(), "No Selection")

    def test_info_text_one_object_simple_info(self):
        """Test Selection.InfoText with one object with simple Info"""
        # Create the selection
        sel = Selection(self.doc)

        # Now select a rect
        sel.SetSelection([self.rect11])

        text, values = sel.InfoText()
        self.assertEquals(text % values, "Rectangle on `Background'")

    def test_info_text_one_object_complex_info(self):
        """Test Selection.InfoText with one object with complex Info"""
        # Create the selection
        sel = Selection(self.doc)

        # Now select a rect
        sel.SetSelection([self.rect2])

        text, values = sel.InfoText()
        self.assertEquals(text % values, "Rectangle on `Foreground'")

    def test_info_text_several_layers(self):
        """Test Selection.InfoText with two objects on separate layers"""
        # Create the selection
        sel = Selection(self.doc)

        # Now select two rects
        sel.SetSelection([self.rect11, self.rect2])

        self.assertEquals(sel.InfoText(), "2 objects on several layers")

    def test_info_text_several_objects_one_layer(self):
        """Test Selection.InfoText with two objects on the same layer"""
        # Create the selection
        sel = Selection(self.doc)

        # Now select two rects
        sel.SetSelection([self.rect11, self.rect12])

        self.assertEquals(sel.InfoText(), "2 objects on `Background'")

class SelectionTest(unittest.TestCase):

    """Base class for testing selection and deselection"""

    def setUp(self):
        """Create a document and various graphic objects"""

        self.doc = InteractiveDocument(create_layer = 1)
        self.layer = self.doc.active_layer

        # create several rectangles and a group
        self.r1 = Rectangle(Trafo(100, 0, 0, 100, 10, 10))
        self.r2 = Rectangle(Trafo(100, 0, 0, 100, 120, 240))
        self.g = Group([self.r1, self.r2])
        self.doc.Insert(self.g, self.layer)

        self.r3 = Rectangle(Trafo(100, 0, 0, 100, 240, 120))
        self.doc.Insert(self.r3, self.layer)

        self.r4 = Rectangle(Trafo(100, 0, 0, 100, 10, 120))
        self.doc.Insert(self.r4, self.layer)

        self.sel = Selection(self.doc)

class TestSelection_private(SelectionTest):

    """Some tests for non public methods of the Selection class"""

    def test_normalize_children(self):
        """Test Selection.normalize with parent and children"""
        self.sel.objects = [self.r1, self.r2, self.g]
        self.sel.normalize()
        # since r1 and r2 are the children of the group g, they should have
        # been removed from self.objects
        self.assertEquals(self.sel.objects, [self.g])

    def test_normalize_nested_groups(self):
        """Test Selection.normalize with a group containing another group"""
        g2 = Group([self.g, self.r3])
        self.layer.Remove(self.g)
        self.layer.Remove(self.r3)
        self.layer.Insert(g2)
        self.sel.objects = [self.r2, self.g, self.r3, g2]
        self.sel.normalize()
        # self.r3, self.r2 and self.g should be removed from the selection as
        # they are all children/grandchildren of g2
        self.assertEquals(self.sel.objects, [g2])

    def test_normalize_duplicate(self):
        """Test Selection.normalize with duplicate entries"""
        self.sel.objects = [self.r3, self.r3, self.r3, self.r4]
        self.sel.normalize()
        # The duplicate entries should be removed:
        self.assertEquals(self.sel.objects, [self.r3, self.r4])

    def test_normalize_sort(self):
        """Test Selection.normalize sorting"""
        self.sel.objects = [self.r4, self.g, self.r3]
        self.sel.normalize()
        # The objects should be sorted:
        self.assertEquals(self.sel.objects, [self.g, self.r3, self.r4])

    #
    # tests for the last_selected list
    #
    def test_normalize_last_children_parent(self):
        """Test Selection.normalize with child and parent as last selected"""
        self.sel.objects = [self.r2, self.g, self.r3]
        self.sel.last_selected = [self.r2, self.g]
        self.sel.normalize()
        self.assertEquals(self.sel.objects, [self.g, self.r3])
        # The last selected list should not contain both a parent and one of
        # its children, so self.r2 should have been removed:
        self.assertEquals(self.sel.last_selected, [self.g])

    def test_path_for_object(self):
        """Test Selection.path_for_object"""
        self.assertEquals(self.sel.path_for_object(self.g, {}), (0, 0))
        self.assertEquals(self.sel.path_for_object(self.r1, {}), (0, 0, 0))
        self.assertEquals(self.sel.path_for_object(self.r2, {}), (0, 0, 1))
        self.assertEquals(self.sel.path_for_object(self.r3, {}), (0, 1))
        self.assertEquals(self.sel.path_for_object(self.r4, {}), (0, 2))

    def test_update_common_path(self):
        """Test Selection.update_common_path"""
        self.sel.objects = [self.g, self.r3, self.r4]
        self.assertEquals(self.sel._common_path, (0,))

class TestSelection(SelectionTest):

    def test_set_selection_simple(self):
        """Test Selection.SetSelection with a single object"""
        # Set the selection to a regular object:
        self.sel.SetSelection(self.r3)
        self.assertEquals(self.sel.GetObjects(), [self.r3])
        self.assertEquals(self.sel.GetLastSelected(), [self.r3])

        # Set the selection to a child of a group.
        self.sel.SetSelection(self.r2)
        self.assertEquals(self.sel.GetObjects(), [self.r2])
        self.assertEquals(self.sel.GetLastSelected(), [self.r2])

    def test_set_selection_multiple(self):
        """Test Selection.SetSelection with multiple objects"""

        self.sel.SetSelection([self.r3, self.r4])
        self.assertEquals(self.sel.GetObjects(), [self.r3, self.r4])
        self.assertEquals(self.sel.GetLastSelected(), [self.r3, self.r4])

        # Try to set the selection to a combination of a group, one of its
        # children and a rectangle specified multiple times.
        # As a result of normalization, the following should happen:
        #     - the children (r2) should be removed from the list because its
        #     parent is also present
        #     - the list should contain a single instance of the rectangle r3
        #     - the objects should be sorted
        self.sel.SetSelection([self.r3, self.r2, self.g, self.r3, self.r3])
        self.assertEquals(self.sel.GetObjects(), [self.g, self.r3])
        self.assertEquals(self.sel.GetLastSelected(), [self.g, self.r3])

    def test_add_simple(self):
        """Test Selection.Add simple"""
        self.sel.SetSelection([self.r3, self.r4])

        # Add a group to selection
        self.sel.Add(self.g)
        self.assertEquals(self.sel.GetObjects(), [self.g, self.r3, self.r4])
        self.assertEquals(self.sel.GetLastSelected(), [self.g])

    def test_add_multiple(self):
        """Test Selection.Add multiple"""
        self.sel.SetSelection([self.r3])

        self.sel.Add([self.r4, self.g])
        # The list should sorted
        self.assertEquals(self.sel.GetObjects(), [self.g, self.r3, self.r4])
        self.assertEquals(self.sel.GetLastSelected(), [self.g, self.r4])

    def test_add_child_parent(self):
        """Test Selection.Add child and parent"""
        self.sel.SetSelection([self.r3])

        # We add to selection a group and one of its children.
        # The child should be excluded from both lists
        self.sel.Add([self.r2, self.g])
        self.assertEquals(self.sel.GetObjects(), [self.g, self.r3])
        self.assertEquals(self.sel.GetLastSelected(), [self.g])

    def test_subtract_simple(self):
        """Test Selection.Subtract with a single object"""
        self.sel.SetSelection(self.r3)
        self.sel.Subtract(self.r3)
        self.assertEquals(self.sel.GetObjects(), [])
        self.assertEquals(self.sel.GetLastSelected(), [])

    def test_subtract_multiple(self):
        """Test Selection.Subtract with multiple objects"""
        self.sel.SetSelection([self.g, self.r3, self.r4])
        self.sel.Subtract(self.r3)
        self.assertEquals(self.sel.GetObjects(), [self.g, self.r4])
        self.assertEquals(self.sel.GetLastSelected(), [self.g, self.r4])

        self.sel.SetSelection([self.g, self.r3, self.r4])
        self.sel.Subtract([self.r3, self.r4])
        self.assertEquals(self.sel.GetObjects(), [self.g])
        self.assertEquals(self.sel.GetLastSelected(), [self.g])

    def test_get_path(self):
        """Test Selection.GetPath with nothing selected"""
        # With nothing selected return an empty tuple
        self.assertEquals(self.sel.GetPath(), ())

        # With a single object selected, return its path
        self.sel.SetSelection(self.r2)
        self.assertEquals(self.sel.GetPath(), (0,0,1))

        # With multiple objects selected, return an empty tuple
        self.sel.SetSelection([self.r3, self.r4])
        self.assertEquals(self.sel.GetPath(), ())

    def test_common_path(self):
        """Test Selection.CommonPath"""
        self.sel.objects = [self.g, self.r3, self.r4]
        # The common parent most deeply contained in the object hierarchy is
        # self.layer
        self.assertEquals(self.sel.CommonPath(), (0,))

if __name__ == "__main__":
    unittest.main()
