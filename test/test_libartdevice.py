# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Test the LibartDevice class"""

__version__ = "$Revision$"
# $Source$
# $Id$

import binascii
import zlib
import unittest

import PIL

import support
support.add_sketch_dir_to_path()

from Sketch import Identity, Trafo, Rect
from Sketch.Graphics import Document, Rectangle, SolidPattern, StandardColors
from Sketch.UI.libartdevice import LibartDevice

from Sketch import _libart


class mock_window:

    def __init__(self, width, height):
        self.width = width
        self.height = height

    def new_gc(self, *args, **kw):
        return None

    def get_size(self):
        return (self.width, self.height)


class MyLibartDevice(LibartDevice):

    def EndDraw(self, region = None):
        pass


class LibartDeviceTest(unittest.TestCase, support.VolatileFileMixin):

    def setUp(self):
        """Create a document and a libart device"""
        self.doc = Document(1)
        for layer in self.doc.Layers():
            if not layer.is_SpecialLayer:
                self.layer = layer
        self.window = mock_window(self.width, self.height)
        self.dev = MyLibartDevice()
        self.dev.init_gc(self.window, None)
        invert = Trafo(1, 0, 0, -1, 0, self.height)
        self.dev.SetViewportTransform(1.0, invert, invert)

    def make_image_filename(self, suffix = ""):
        return self.volatile_file_name(self.id() + suffix + ".ppm")

    def check_images(self):
        """Compare the rendered image in self.dev to the reference image

        Write both images to files in volatile, create a difference
        image between the two and write that to volatile as well, then
        check whether the difference is 0 everywhere.
        """
        # Write the reference image
        reference = zlib.decompress(binascii.a2b_base64(self.reference_data))
        reference_name = self.make_image_filename("-reference")
        f = file(reference_name, "wb")
        f.write("P6 %d %d 255\n" % (self.width, self.height))
        f.write(reference)
        f.close()

        # write the rendered image
        rendered_name = self.make_image_filename("-rendered")
        self.dev.export_image(rendered_name)

        # Compare images
        reference_image = PIL.Image.open(reference_name)
        rendered_image = PIL.Image.open(rendered_name)

        difference = PIL.ImageChops.difference(reference_image, rendered_image)
        difference.save(self.make_image_filename("-difference"))

        # Check whether the difference between the images is small
        # enough.  Different libart versions can sometimes produce very
        # slightly different results, so we allow at 1/1000th of he
        # pixels to differ by 1 in any color component.
        self.failUnless(max(map(max, difference.getextrema())) <= 1)
        hist = difference.histogram()
        num_pixels = difference.size[0] * difference.size[1]
        max_ratio = 0.001
        self.failUnless(float(hist[1]) / num_pixels < max_ratio)
        self.failUnless(float(hist[256 + 1]) / num_pixels < max_ratio)
        self.failUnless(float(hist[2 * 256 + 1]) / num_pixels < max_ratio)

        if 0:
            # Code to generate the python code containing the reference
            # data
            data = _libart.export_pixbuf(self.dev.image)[-1]
            encoded = binascii.b2a_base64(zlib.compress(data))
            while encoded:
                print repr(encoded[:65])
                encoded = encoded[65:]


class TestOpacity(LibartDeviceTest):

    """Test PostScriptDevice rendering objects with opacity

    Since the PostScriptDevice doesn't really support opacity yet this
    basically amounts to checking whether it simply ignores the opacity
    settings.
    """

    width = 100
    height = 100
    reference_data = (
        'eJzt28ERwjAMRUGV4k4QlUAnTmeUQinBUIMYLGb/vEOu3vjq86zZnDPiiDj/uKqxY'
        'sWKFat9YsWKFasusWLFilWXWLFixapLrFixYtUlVqy+0VG0zIy4frh261FlNTNLuo'
        '1xiVVu1YhReNvXxSrpscTiOuPYqsXFihUrVqxYsWLFihUrVqxYsWLFihUrVqxYsWL'
        'FihUrVqxYsWK1VaxYsWLFihUrVqxYsWLFihUrVqxYsWLFilVDq/Hh2qjL+61l2WvZ'
        'Kqvn/b64dus2Rq6fWLQqqz2bmcf6qNqvj8Nqk1ixYsWqS6xYsWLVJVasWLHqEitWr'
        'Fh1iRUrVqy6xOpXVi+BITbv\n')

    def test_fill_opacity(self):
        rect = Rectangle(Trafo(self.width, 0, 0, self.height,
                               0.25 * self.width, 0.25 * self.height))
        pattern = SolidPattern(StandardColors.blue)
        rect.Properties().SetProperty(fill_pattern = pattern)
        self.doc.Insert(rect, self.layer)
        rect = Rectangle(Trafo(self.width, 0, 0, self.height,
                               -0.25 * self.width, -0.25 * self.height))
        pattern = SolidPattern(StandardColors.red)
        rect.Properties().SetProperty(fill_pattern = pattern)
        rect.Properties().SetProperty(fill_opacity = 0.5)
        self.doc.Insert(rect, self.layer)

        self.dev.DrawDocument(self.doc, Rect(0, 0, self.width, self.height),
                              draw_page_outline = 0)
        self.check_images()


if __name__ == "__main__":
    unittest.main()
