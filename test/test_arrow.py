# Sketch - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Arrow objects
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch import Point, Rect
from Sketch.Graphics import Arrow

class TestArrow(unittest.TestCase):

    def test_bounding_rect(self):
        """Test Arrow.BoundingRect"""
        arrow = Arrow([(0, 0), (-10, 10), (-10, -10), (0, 0)], 1)
        self.assertEquals(arrow.BoundingRect(Point(0, 0), Point(1, 0), 1),
                          Rect(-10, -10, 0, 10))

    def test_bounding_rect_dir_0(self):
        """Test ArrowBoundingRect() with direction Point(0, 0)

        Normally the direction should be a unit vector, but it can be
        difficult to determine the direction at the end of a poly bezier
        curve properly and it may end up as the Null-Vector.  For arrow
        directions Skencil treats the Null-Vector like Point(1, 0).  The
        code in arrow.py uses atan2 on the coordinates of the direction
        vector which on some systems raises an exception when both
        coordinates are 0, so we implicitly test for that as well here.
        """
        arrow = Arrow([(0, 0), (-10, 10), (-10, -10), (0, 0)], 1)
        self.assertEquals(arrow.BoundingRect(Point(0, 0), Point(0, 0), 1),
                          Rect(-10, -10, 0, 10))


if __name__ == "__main__":
    unittest.main()
