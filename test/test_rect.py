# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Rect objects
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import math, operator
import unittest

import support

support.add_sketch_module_dir_to_path()

import _sketch
from _sketch import Rect, PointsToRect, InfinityRect, EmptyRect, Point


class RectTest(support.FPTestCase):

    """Test cases for the Rect objects"""

    def test_constructors(self):
        """Test Rect constructors"""
        eq = self.assertEquals

        # Rect(left, bottom, right, top)
        r = Rect(-1, -2, 10, 20)
        eq(r.left, -1)
        eq(r.bottom, -2)
        eq(r.right, 10)
        eq(r.top, 20)

        # rects are automatically normalized
        r = Rect(10, 20, -1, -2)
        eq(r.left, -1)
        eq(r.bottom, -2)
        eq(r.right, 10)
        eq(r.top, 20)

        # Rect() accepts point objects
        p1 = Point(10, 15); p2 = Point(-1, 0)
        r = Rect(p1, p2)
        eq(r.left, -1)
        eq(r.bottom, 0)
        eq(r.right, 10)
        eq(r.top, 15)

        # the order doesn't matter
        r = Rect(p2, p1)
        eq(r.left, -1)
        eq(r.bottom, 0)
        eq(r.right, 10)
        eq(r.top, 15)

    def test_PointsToRect(self):
        """Test PointsToRect"""
        eq = self.assertEquals

        # The sequence can contain point specs
        sequence = [Point(0, 0), (-1, 10), [-2, -3], Point(10, 0)]
        r = PointsToRect(sequence)
        eq(r.left, -2)
        eq(r.bottom, -3)
        eq(r.right, 10)
        eq(r.top, 10)

    def test_contains_point(self):
        """Test Rect.contains_point"""
        eq = self.assertEquals

        r = Rect(-1, -1, 1, 1)
        # These points must be inside the rect. The edges and corners
        # are considered part of the rect. Points can be specified as
        # two parameters or as one pointspec
        eq(r.contains_point((0, 0)), 1)
        eq(r.contains_point((0, 0.2)), 1)
        eq(r.contains_point((-0.5, 0.9)), 1)
        eq(r.contains_point(1, 0), 1)
        eq(r.contains_point(0, 1), 1)
        eq(r.contains_point(Point(1, 1)), 1)
        eq(r.contains_point(Point(-1, 1)), 1)
        eq(r.contains_point(Point(-1, -1)), 1)
        eq(r.contains_point(Point(1, -1)), 1)

        # These are outside of the rect
        eq(r.contains_point(10, 0), 0)
        eq(r.contains_point(Point(0, 1.2)), 0)
        eq(r.contains_point((-1.1, 1.001)), 0)

    def test_contains_rect(self):
        """Test Rect.contains_rect"""
        eq = self.assertEquals

        r = Rect(-1, -1, 1, 1)
        quarter = Rect(0, 0, 1, 1) # contained in r in on corner
        inside = Rect(-0.1, 0, 0.1, 0.5) # completely inside r
        outside = Rect(10, 10, 11, 100) # completely outside of r
        # r, quarter and other must be inside the r. The edges and
        # corners are part of a rect and a rect contains itself
        eq(r.contains_rect(quarter), 1)
        eq(r.contains_rect(inside), 1)
        eq(r.contains_rect(r), 1)
        eq(r.contains_rect(outside), 0)
        eq(outside.contains_rect(r), 0)

        # InfinityRect contains all other rects and EmptyRect is
        # contained in all other rects
        eq(r.contains_rect(EmptyRect), 1)
        eq(r.contains_rect(InfinityRect), 0)
        eq(InfinityRect.contains_rect(r), 1)
        eq(EmptyRect.contains_rect(r), 0)

        eq(EmptyRect.contains_rect(EmptyRect), 1)
        eq(EmptyRect.contains_rect(InfinityRect), 0)
        eq(InfinityRect.contains_rect(InfinityRect), 1)
        eq(InfinityRect.contains_rect(EmptyRect), 1)

    def test_overlaps(self):
        """Test Rect.overlaps"""
        eq = self.assertEquals

        r = Rect(-1, -1, 1, 1)
        edge = Rect(1, 0.5, 3, 4) # touches r on an edge
        contained = Rect(-0.1, 0, 0.1, 0.5) # contained in r
        overlaps = Rect(0.5, 0.5, 3, 4) # strictly overlaps r
        other = Rect(-10, -10, -4, -4) # not contained in r
        # r, edge and containded overlap r.
        eq(r.overlaps(edge), 1)
        eq(edge.overlaps(r), 1)
        eq(r.overlaps(contained), 1)
        eq(contained.overlaps(r), 1)
        eq(r.overlaps(overlaps), 1)
        eq(overlaps.overlaps(r), 1)
        eq(r.overlaps(r), 1)
        eq(r.overlaps(other), 0)
        eq(other.overlaps(r), 0)

        # InfinityRect and EmptyRect overlap all other rects and each
        # other
        eq(r.overlaps(EmptyRect), 1)
        eq(r.overlaps(InfinityRect), 1)
        eq(InfinityRect.overlaps(r), 1)
        eq(EmptyRect.overlaps(r), 1)

        eq(EmptyRect.overlaps(EmptyRect), 1)
        eq(EmptyRect.overlaps(InfinityRect), 1)
        eq(InfinityRect.overlaps(InfinityRect), 1)
        eq(InfinityRect.overlaps(EmptyRect), 1)

    def test_translated(self):
        """Test Rect.translated"""
        eq = self.assertEquals
        fse = self.assert_float_sequences_equal
        ident = self.assert_identity

        r = Rect(-1, -1, 1, 1)

        t = r.translated(Point(0.5, -0.5))
        fse(tuple(t), (-0.5, -1.5, 1.5, 0.5))
        t = r.translated((0.5, -0.5))
        fse(tuple(t), (-0.5, -1.5, 1.5, 0.5))
        t = r.translated([0.5, -0.5])
        fse(tuple(t), (-0.5, -1.5, 1.5, 0.5))
        t = r.translated(0.5, -0.5)
        fse(tuple(t), (-0.5, -1.5, 1.5, 0.5))

        # For InfinityRect and EmptyRect the same rect is returned
        ident(InfinityRect.translated(0.5, 1), InfinityRect)
        ident(EmptyRect.translated(0.5, 1), EmptyRect)

    def test_grown(self):
        """Test Rect.grown"""
        fse = self.assert_float_sequences_equal
        ident = self.assert_identity

        r = Rect(-1, -1, 1, 1)
        amount = 0.25
        fse(r.grown(amount), (r.left - amount, r.bottom - amount,
                              r.right + amount, r.top + amount))

        # For InfinityRect and EmptyRect, grown returns the same rect
        ident(InfinityRect.grown(0.5), InfinityRect)
        ident(EmptyRect.grown(10), EmptyRect)

    def test_center(self):
        """Test Rect.center"""
        fse = self.assert_float_sequences_equal
        ident = self.assert_identity
        PointType = type(Point(0, 0))

        r = Rect(-1, -1, 1, 1)
        fse(r.center(), (0.0, 0.0))
        # center() returns a point object
        ident(type(r.center()), PointType)
        
        # For InfinityRect and EmptyRect, center() returns Point(0, 0)
        fse(InfinityRect.center(), (0.0, 0.0))
        fse(EmptyRect.center(), (0.0, 0.0))

    def test_sequence(self):
        """Test Rect object with sequence protocol"""
        eq = self.assertEquals
        exc = self.assertRaises        

        # the rects can be treated as sequences
        r = Rect(0, 1, 2, 3)
        eq(len(r), 4) 
        eq(r[0], 0)
        eq(r[1], 1)
        eq(r[2], 2)
        eq(r[3], 3)
        eq(r[-4], 0)
        eq(r[-3], 1)
        eq(r[-2], 2)
        eq(r[-1], 3)

        exc(IndexError, operator.getitem, r, 4)
        exc(IndexError, operator.getitem, r, -5)



if __name__ == "__main__":
    unittest.main()
