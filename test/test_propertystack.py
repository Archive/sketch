# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the PropertyStack
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()


from Sketch.Graphics import PropertyStack, EmptyPattern, Style

class TestPropertyStack(unittest.TestCase):

    def test_init_base(self):
        """Test PropertyStack init with an explicit base style"""
        base_style = Style(fill_pattern = EmptyPattern, line_width = 10)
        stack = PropertyStack(base = base_style)
        self.assertEquals(stack.fill_pattern, EmptyPattern)
        self.assertEquals(stack.line_width, 10)

    def test_init_duplicate(self):
        """Test PropertyStack init with a 'duplicate' argument"""
        base_style = Style(fill_pattern = EmptyPattern, line_width = 10)
        stack1 = PropertyStack(base = base_style)
        stack2 = PropertyStack(duplicate = stack1)
        self.assertEquals(stack2.fill_pattern, EmptyPattern)
        self.assertEquals(stack2.line_width, 10)

    def test_cache(self):
        """Test PropertyStack attribute caching"""
        base_style = Style(fill_pattern = EmptyPattern, line_width = 10)
        stack = PropertyStack(base = base_style)

        # The properties can be accessed as attributes of the stack
        self.assert_(hasattr(stack, "fill_pattern"))
        self.assertEquals(stack.line_width, 10)

        # Now change a value and check whether the attribute has changed
        # as well
        stack.SetProperty(line_width = 20)
        self.assertEquals(stack.line_width, 20)

        # Something more elaborate: Create and add a dynamic style that
        # shadows the line_width. Then change the line width again.
        dynstyle = Style("line width", line_width = 15, dashes = None)
        dynstyle = dynstyle.AsDynamicStyle()
        stack.AddStyle(dynstyle)
        self.assertEquals(stack.line_width, 15)
        stack.SetProperty(line_width = 2)
        self.assertEquals(stack.line_width, 2)

    def test_shadow(self):
        """Test PropertyStack's removal of shadowed styles"""
        style1 = Style(fill_pattern = EmptyPattern, line_width = 10)
        dynstyle = Style("line width", line_width = 15).AsDynamicStyle()
        style2 = Style(line_width = 5)

        # Create a PropertyStack with style1 as base and add dynstyle
        stack = PropertyStack(base = style1)
        stack.AddStyle(dynstyle)

        # Now the stack contains dynstyle
        self.assertEquals(stack.DynamicStyleNames(), ["line width"])

        # style2 shadows dynstyle so it should have been removed.
        stack.AddStyle(style2)
        self.assertEquals(stack.DynamicStyleNames(), [])

        
