"""Script to bas64 dump a file for use in test case code

Usage:
make_image_filename inputfile

Print a code snippet like this to stdout:

    reference_data = (
        'eJzt27FtxDAQRcEthVeJqE7UCdnZleJSZJ7hwLGxxnGN+XiBUo6Y8r5zNsaImBH3P'
        'y5rrFixYsVqn1ixYsWqSqxYsWJVJVasWLGqEitWrFhViRWrv2jOOTJ2HEdEjxj79c'
        'yyGr3/bJ7n77pa6/HocW5Vi5Z429fFSum5qOMcMbdqcbFixYoVK1asWLFixYoVK1a'
        'sWLFixYoVK1asWLFixYoVK1asWG0VK1asWLFixYoVK1asWLFixYoVK1asWLFiVdCq'
        'fXFtVI9HfHMllGX1cV2La7der2XXT0xaltWevd4Xr4+svfs4rDaJFStWrKrEihUrV'
        'lVixYoVqyqxYsWKVZVYsWLFqkqs3mX1CZuKN7w=')

The value of reference_data is a string with the base64 encoded contents
of the file inputfile.  The code is to be used in some test cases which
need binary reference data.
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import sys
import binascii
import zlib

def main():
    filename = sys.argv[1]

    data = open(filename, "rb").read()
    encoded = binascii.b2a_base64(zlib.compress(data))

    start_prefix = "    reference_data = ("
    prefix = " " * 8
    code = [start_prefix]
    lines = []
    while encoded:
        lines.append(repr(encoded[:65]))
        encoded = encoded[65:]

    for line in lines:
        code.append("\n")
        code.append(prefix + line)
    code.append(")")

    print "".join(code)

main()
