# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for Sketch.Lib.util
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Lib.util import format, commonbasedir, relpath

class TestFormat(unittest.TestCase):

    def test_format(self):
        """Test Sketch.Lib.util.format"""
        def upper(arg):
            return str(arg).upper()
        def twice(arg):
            return str(2 * arg)
        converters = {"upper":upper, "twice": twice}
        self.assertEquals(format("%(value)d %(str)[upper] %(f)f %(num)[twice]",
                                 converters, {"value":12, "str": "Sketch",
                                              "f": 3.1415926, "num": 1.5}),
                          "12 SKETCH 3.141593 3.0")

    def test_format_digits(self):
        """Test Sketch.Lib.util.format with digits and underscores"""
        def up_case(arg):
            return str(arg).upper()
        def times2(arg):
            return str(2 * arg)
        converters = {"up_case":up_case, "times2": times2}
        self.assertEquals(format("%(str1)[up_case]", converters,
                                 {"str1": "Sketch"}),
                          "SKETCH")
        self.assertEquals(format("%(value_0)[times2]", converters,
                                 {"value_0": 1.5}),
                          "3.0")

    def test_format_single_char_names(self):
        """Test Sketch.Lib.util.format with signle character names"""
        def upper(arg):
            return str(arg).upper()
        converters = {"u":upper}
        self.assertEquals(format("%(v)[u] %(w)[u]",
                                 converters, {"v":"lOw", "w": "NOIse"}),
                          "LOW NOISE")
        

class Test_fileutils(unittest.TestCase):

    def test_commonbasedir(self):
        """Test commonbasedir"""
        self.assertEquals(commonbasedir("/tmp/parrot", "/tmp/parrot/dead"),
                          "/tmp/parrot")
        self.assertEquals(commonbasedir("/tmp/parrot", "/tmp/parr"),
                          "/tmp")

    def test_relpath(self):
        """Test relpath"""
        self.assertEquals(relpath("/usr/local/lib", "/usr/local/lib/python"),
                          'python')
        self.assertEquals(relpath("/usr/local/lib", "/usr/local/bin/python"),
                          '../bin/python')
        self.assertEquals(relpath("/usr/local/lib", "/usr/bin/python"),
                          '../../bin/python')
        self.assertEquals(relpath("/usr/local/lib", "/var/spool/mail"),
                          '/var/spool/mail')
        self.assertEquals(relpath("/home/", "xyzzy"), 'xyzzy')

if __name__ == "__main__":
    unittest.main()
