# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test cases for Sketch/Editor/doceditor.py
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import sys
import unittest
import traceback

import support
support.add_sketch_dir_to_path()

# init_base is necessary so that we can use text objects since they need
# fonts.
support.init_base()

import Sketch
from Sketch import Point, Rect, Trafo, CreatePath, ContAngle, ContSmooth, \
     ContSymmetrical, Bezier, Line
from Sketch.Base.const import HANDLES, CURRENTINFO, OBJECT_CHANGED, \
     CHILDREN, INSERTED, REMOVED, REARRANGED, REDRAW, SHAPE, SELECTION, \
     SelectAdd
from Sketch.Graphics import PolyBezier, Layer, Rectangle, Group
from Sketch.Graphics.base import Primitive
from Sketch.Editor import EditorWithSelection, InteractiveDocument, Context, \
     toolmap, Button1Mask, ShiftMask, ControlMask, HandleSelectedNode
from Sketch.Editor.tools import ToolInstance, ToolInfo


class TestEditorWithSelection(unittest.TestCase):

    """Simple tests for EditorWithSelection"""

    def setUp(self):
        """Override to create a document and editor

        Bind the document to self.document and the editor to self.editor
        """
        self.document = InteractiveDocument(create_layer = 1)
        self.editor = EditorWithSelection(self.document)

    def tearDown(self):
        """Destroy the editor object"""
        self.editor.Destroy()

    def test_initial_settings(self):
        """Test EditorWithSelection initial settings"""
        editor = self.editor

        self.assert_(editor.Document() is self.document)

        # No snapping by default
        self.failIf(editor.IsSnappingToGrid())
        self.failIf(editor.IsSnappingToGuides())
        self.failIf(editor.IsSnappingToObjects())

        # No selection yet
        self.failIf(editor.HasSelection())

        # There's no selection so there shouldn't be any handles either
        self.assertEquals(editor.Handles(), [])



class DocEditorTestsWithMessages(unittest.TestCase):

    """Base class for EditorWithSelection tests that involve messages"""

    subscribed_channels = ()

    def setUp(self):
        """Create a document and an editor and subscribe some messages

        The document is an instance of InteractiveDocument and is bound
        to self.document.  The document contains a layer which is
        available as self.layer.  The editor is bound to self.editor and
        is an instance of EditorWithSelection.

        After these objects have been created setUp subscrubes
        self.receiver to messages as described by the
        subscribed_channels class variable.  This variable is a sequence
        of (attr, channels) pairs with attr being the name of an
        instance attribute and channels a sequence of channel names.
        For all the channels self.receiver is subscribed once to the
        attribute of self called attr (so that an attr value 'document'
        refers to self.document).  The channel name will be passed to
        self.receiver as well as the last positional argument.
        self.receiver records all messages in self.received_messages
        (see receiver's doc string for more details).  The test methods
        can check that variable to verify which messages have been sent
        since the call to setUp.
        """
        self.document = InteractiveDocument(create_layer = 1)
        self.layer = self.document.AppendLayer("test")
        self.editor = EditorWithSelection(self.document)
        for attr, channels in self.subscribed_channels:
            for channel in channels:
                getattr(self, attr).Subscribe(channel, self.receiver, channel)
        self.received_messages = []

    def tearDown(self):
        """Clean up resources initialized in setup.

        The cleanup destros the editor object and removes the references
        to the document, the editor and the layer, and clears the
        received_messages list.
        """
        self.received_messages = []
        if self.editor is not None:
            self.editor.Destroy()
        self.editor = self.document = self.layer = None

    def receiver(self, *args):
        """Generic receiver for connector messages.

        Simply append the argument tuple to self.received_messages().
        """
        self.received_messages.append(args)


class MockToolInstance(ToolInstance):

    """
    ToolInstance that records whether some of the methods have been called
    """

    def __init__(self, *args):
        ToolInstance.__init__(self, *args)
        self.called_methods = []

    def DelayButtonPress(self):
        self.called_methods.append(("DelayButtonPress",))
        return 1

    def ButtonPress(self, canvas, p, snapped, button, state, handle = None):
        ToolInstance.ButtonPress(self, canvas, p, snapped, button, state,
                                 handle = None)
        self.called_methods.append(("ButtonPress", canvas, p, snapped, button,
                                    state, handle))

    def PointerMotion(self, canvas, p, snapped, state):
        ToolInstance.PointerMotion(self, canvas, p, snapped, state)
        self.called_methods.append(("PointerMotion", canvas, p, snapped,
                                    state))

    def ButtonRelease(self, canvas, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, canvas, p, snapped, button, state)
        self.called_methods.append(("ButtonRelease", canvas, p, snapped,
                                    button, state))

    def ButtonClick(self, canvas, p, snapped, button, state, handle = None):
        ToolInstance.ButtonClick(self, canvas, p, snapped, button, state,
                                 handle = None)
        self.called_methods.append(("ButtonClick", canvas, p, snapped,
                                    button, state, handle))

    def End(self):
        """Extend the inherited method to record that it has been called."""
        ToolInstance.End(self)
        self.called_methods.append(("End",))

MockTool = ToolInfo("MockTool", MockToolInstance, cursor = 'CurZoom')

class MockCanvas:

    """Mock canvas the does nothing"""

    def max_snap_distance(self):
        return 1

    def __dummy(self, *args, **kw):
        pass

    hide_handles = __dummy
    show_handles = __dummy

    def inverting_device(self):
        return MockGC()

    def test_device(self):
        # The MockGC isn't reall right but the only test that needs it
        # currentyl is the very simple selection tool test which should
        # get by with this.
        return MockGC()


class MockGC:

    """Very simple Mock GC

    Objects of this class allow arbitrary methods to be called with
    arbitrary arguments.
    """

    def __getattr__(self, methodname):
        def dummy(*args, **kw):
            pass
        return dummy

    def HitRectAroundPoint(self, p):
        rad = 1
        return Rect(p.x - rad, p.y - rad, p.x + rad, p.y + rad)

    def MultiBezierHit(self, paths, p, properties, filled,
                       ignore_outline_mode = 0):
        # Always return 1 because that's all tests that call this method
        # expect for now
        return 1

    def ParallelogramHit(self, p, trafo, maxx, maxy, filled, properties=None,
                         ignore_outline_mode = 0):
        # Always return 1 because that's all tests that call this method
        # expect for now
        return 1


class RecordingMockGC(MockGC):

    """A MockGC that optionally records some methods.
    """

    def __init__(self, *methodnames):
        self.logged_methods = methodnames
        self.called_methods = []

    def __getattr__(self, methodname):
        def dummy(*args, **kw):
            if methodname in self.logged_methods:
                self.called_methods.append((methodname, args, kw))
        return dummy

    def __call__(self):
        return self


class TestEditorWithSelectionTools(DocEditorTestsWithMessages):

    """EditorWithSelection test that involve the MockTool"""

    subscribed_channels = [("editor", [HANDLES, CURRENTINFO])]

    def setUp(self):
        """Prepare for a unit test

        Create a document as self.document and an editor as self.editor.
        Subscribe to some editor channels.

        Store a copy of the toolmap and add the MockTool to the toolmap.
        Initialize the received_messages list.
        """
        DocEditorTestsWithMessages.setUp(self)
        self.old_toolmap = toolmap.copy()
        toolmap["MockTool"] = MockTool

    def tearDown(self):
        """Restore the toolmap and destroy the editor object"""
        toolmap.clear()
        toolmap.update(self.old_toolmap)
        DocEditorTestsWithMessages.tearDown(self)

    def test_set_tool(self):
        """Test EditorWithSelection.SetTool"""
        editor = self.editor

        # Select the mock tool, get a reference to the actual instance
        # used and select another tool
        editor.SetTool("MockTool")
        mock_tool = editor.Tool()
        self.assert_(isinstance(mock_tool, MockToolInstance))
        editor.SetTool("SelectionTool")

        # Now the mock tool's End method should have been called and a
        # HANDLES should have been emitted twice because we've changed
        # tools twice
        self.assertEquals(mock_tool.called_methods, [("End",)])
        self.assertEquals(self.received_messages, [(HANDLES,), (HANDLES,)])

    def test_mouse_interaction_drag(self):
        """Test EditorWithSelection mouse interaction: drag"""
        editor = self.editor

        # Select the mock tool, get a reference to the actual instance
        # used so that we can look at which of its methods were called
        # later.
        editor.SetTool("MockTool")
        mock_tool = editor.Tool()

        # simulate a click-drag-release
        canvas = MockCanvas()

        # Clear the messages list before each method call so that we can
        # check more precisely which method call leads to which
        # messages.
        self.received_messages = []
        editor.ButtonPress(canvas, Point(10, 20), 1, 0)
        self.assertEquals(self.received_messages, [(CURRENTINFO,)])

        self.received_messages = []
        editor.PointerMotion(canvas, Point(20, 20), Button1Mask)
        self.assertEquals(self.received_messages, [(CURRENTINFO,)])

        self.received_messages = []
        editor.ButtonRelease(canvas, Point(25, 28), 1, Button1Mask)
        # The ButtonRelease call should result in at least CURRENTINFO
        # and perhaps HANDLES but that doesn't happen at the moment
        # which is a bug.
        self.assertEquals(self.received_messages, [])
        #[(CURRENTINFO,), (HANDLES,)])

        self.assertEquals(mock_tool.called_methods,
                          [("ButtonPress",
                            canvas, Point(10, 20), Point(10, 20), 1, 0, None),
                           ("PointerMotion",
                            canvas, Point(20, 20), Point(20, 20), Button1Mask),
                           ("ButtonRelease",
                            canvas, Point(25,28), Point(25,28), 1,Button1Mask),
                          ])

    def test_mouse_interaction_click(self):
        """Test EditorWithSelection mouse interaction: click"""
        editor = self.editor

        # Select the mock tool, get a reference to the actual instance
        # used so that we can look at which of its methods were called
        # later.
        editor.SetTool("MockTool")
        mock_tool = editor.Tool()

        # simulate a click-drag-release
        canvas = MockCanvas()

        # Clear the messages list before each method call so that we can
        # check more precisely which method call leads to which
        # messages.
        self.received_messages = []
        editor.ButtonClick(canvas, Point(10, 20), 1, ShiftMask)
        self.assertEquals(self.received_messages, [])

        self.assertEquals(mock_tool.called_methods,
                          [("ButtonClick",
                            canvas, Point(10, 20), Point(10, 20), 1, ShiftMask,
                            None),
                          ])

    def test_set_tool_with_unfinished_poly_line(self):
        """Test EditorWithSelection.SetTool() with unfinished poly line

        Test for savannah bugs #3760 and #3918.  applies to both
        PolyBezierToolInstance and PolyLineToolInstance.
        """
        editor = self.editor
        canvas = MockCanvas()

        # Start creating a poly line with a singe click-drag-release
        editor.SetTool("CreatePoly")
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)
        editor.PointerMotion(canvas, Point(10, 10), Button1Mask)
        editor.ButtonRelease(canvas, Point(10, 10), 1, Button1Mask)

        # Now we have an unfinished polyline. It finished enough to be
        # inserted into the document but the editor still accepts
        # additonal mouse interaction.
        #
        # In this state we switch the tools.  This should result in the
        # poly line being inserted into the document.  The bug was an
        # exception during this.
        editor.SetTool("CreateRect")

        # If all went well the object has been inserted and is now the
        # selected object
        obj = self.editor.CurrentObject()
        self.failIf(obj is None)
        self.failUnless(obj.is_Bezier)

    def test_create_poly_tool_single(self):
        """Test EditorWithSelection and CreatePoly single press/drag/release
        """
        editor = self.editor
        canvas = MockCanvas()

        # Start creating a poly line with a single click-drag-release
        editor.SetTool("CreatePoly")
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)
        editor.PointerMotion(canvas, Point(10, 10), Button1Mask)
        editor.ButtonRelease(canvas, Point(10, 10), 1, Button1Mask)

        # Middle button click finishes drawing
        editor.ButtonClick(canvas, Point(100, 100), 2, 0)

        # If all went well the object has been inserted and is now the
        # selected object
        obj = self.editor.CurrentObject()
        self.failIf(obj is None)
        self.failUnless(obj.is_Bezier)
        self.assertEquals(len(obj.Paths()), 1)
        self.assertEquals(obj.Paths()[0].get_save(),
                          [(0, 0, ContAngle),
                           (10, 10, ContAngle)])

    def test_create_poly_tool_click(self):
        """Test EditorWithSelection and CreatePoly starting with click
        """
        editor = self.editor
        canvas = MockCanvas()

        # Start creating a poly line with a click for the first point
        # and a click-drag-release for the second.  Then finish with a
        # middle button click
        editor.SetTool("CreatePoly")
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)
        editor.ButtonRelease(canvas, Point(0, 0), 1, Button1Mask)
        editor.ButtonPress(canvas, Point(8, 2), 1, 0)
        editor.PointerMotion(canvas, Point(9, 1), Button1Mask)
        editor.ButtonRelease(canvas, Point(10, 0), 1, Button1Mask)
        editor.ButtonClick(canvas, Point(100, 100), 2, 0)

        # If all went well the object has been inserted and is now the
        # selected object
        obj = self.editor.CurrentObject()
        self.failIf(obj is None)
        self.failUnless(obj.is_Bezier)
        self.assertEquals(len(obj.Paths()), 1)
        self.assertEquals(obj.Paths()[0].get_save(),
                          [(0, 0, ContAngle),
                           (10, 0, ContAngle)])

    def test_create_poly_tool_multiple(self):
        """Test EditorWithSelection and CreatePoly multiple press/drag/release
        """
        editor = self.editor
        canvas = MockCanvas()

        # Start creating a poly line with a singe click-drag-release
        editor.SetTool("CreatePoly")
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)
        editor.PointerMotion(canvas, Point(10, 0), Button1Mask)
        editor.ButtonRelease(canvas, Point(10, 0), 1, Button1Mask)
        editor.ButtonPress(canvas, Point(20, 20), 1, 0)
        editor.PointerMotion(canvas, Point(20, 10), Button1Mask)
        editor.ButtonRelease(canvas, Point(10, 20), 1, Button1Mask)
        editor.ButtonClick(canvas, Point(100, 100), 2, 0)

        # If all went well the object has been inserted and is now the
        # selected object
        obj = self.editor.CurrentObject()
        self.failIf(obj is None)
        self.failUnless(obj.is_Bezier)
        self.assertEquals(len(obj.Paths()), 1)
        self.assertEquals(obj.Paths()[0].get_save(),
                          [(0, 0, ContAngle),
                           (10, 0, ContAngle),
                           (10, 20, ContAngle)])

    def test_switch_tool_before_release_simple(self):
        """Test EditorWithSelection.SetTool() with unfinished rectangle.

        The generic case would be when the creation of an object that requires
        a click-drag-release cycle is interrupted between drag and release by
        swithing the tool.
        For example, this situation may arise when pressing the shortcut for
        another tool while dragging the mouse to create a rectangle.

        This method tests that the tool can be switched without errors.
        """
        editor = self.editor
        canvas = MockCanvas()

        # Start creating a rectangle with click-drag-release
        editor.SetTool("CreateRect")
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)
        editor.PointerMotion(canvas, Point(10, 10), Button1Mask)
        # Switch the tool:
        editor.SetTool("CreateEllipse")
        # ButtonRelease should not cause errors:
        editor.ButtonRelease(canvas, Point(10, 10), 1, Button1Mask)

    def test_switch_tool_before_release(self):
        """Test EditorWithSelection.SetTool() with unfinished rectangle cleanup

        Same as test_switch_tool_before_release_simple but this time
        test whether the canvas is cleaned up afterwards.
        """
        editor = self.editor
        canvas = MockCanvas()
        # Override the inverting_device method to return a RecordingMockGC
        # so that we can keep track of what's drawn.
        canvas.inverting_device = RecordingMockGC('DrawRectangle')

        # Start creating a rectangle with click-drag-release
        editor.SetTool("CreateRect")
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)
        self.assertEquals(canvas.inverting_device.called_methods,
                          [('DrawRectangle', (Point(0, 0), Point(0, 0)), {})])

        editor.PointerMotion(canvas, Point(10, 10), Button1Mask)
        self.assertEquals(canvas.inverting_device.called_methods,
                          [('DrawRectangle', (Point(0, 0), Point(0, 0)), {}),
                           ('DrawRectangle', (Point(0, 0), Point(0, 0)), {}),
                           ('DrawRectangle', (Point(0, 0), Point(10, 10)), {})
                          ])

        # Now we have started to create the rectangle.
        # In this state we switch the tools.
        # This should cancel the rectangle tool and clear the canvas.
        editor.SetTool("CreateEllipse")

        # Now the inverting_device should be cleaned up; in the
        # end each method should have been called twice with the same
        # arguments.
        self.assertEquals(canvas.inverting_device().called_methods,
                          [('DrawRectangle', (Point(0, 0), Point(0, 0)), {}),
                           ('DrawRectangle', (Point(0, 0), Point(0, 0)), {}),
                           ('DrawRectangle', (Point(0, 0), Point(10, 10)), {}),
                           ('DrawRectangle', (Point(0, 0), Point(10, 10)), {})
                          ])

        # ButtonRelease should not cause errors
        editor.ButtonRelease(canvas, Point(10, 10), 1, Button1Mask)

    def test_edit_simple(self):
        """Test EditorWithSelection with EditTool (very simple)"""
        editor = self.editor
        canvas = MockCanvas()

        path = CreatePath()
        path.AppendLine(0, 0)
        path.AppendLine(10, 10)
        poly = PolyBezier(paths = (path,))

        self.document.Insert(poly, self.layer)
        self.editor.SelectObject(poly)

        editor.SetTool("EditTool")

        # Get a handle so that we can drag it
        handle = editor.Handles()[0]

        # Now drag it to (5, 0)
        editor.ButtonPress(canvas, Point(0, 0), 1, 0, handle=handle)
        editor.PointerMotion(canvas, Point(2, 0), Button1Mask)
        editor.ButtonRelease(canvas, Point(5, 0), 1, Button1Mask)

        # Check the new state of the poly line
        obj = self.editor.CurrentObject()
        self.failIf(obj is None)
        self.failUnless(obj.is_Bezier)
        self.assertEquals(len(obj.Paths()), 1)
        self.assertEquals(obj.Paths()[0].get_save(),
                          [(5, 0, ContAngle),
                           (10, 10, ContAngle)])

    def test_edit_noeditor(self):
        """Test EditTool with an object without an editor"""
        # Test for a case in which the selection contains objects that cannot
        # be edited with the EditTool (e.g. groups, images)
        #
        # In Skencil terms that means the GetEditor function defined
        # in Sketch/Editor/__init__.py returns None since it cannot find an
        # editor for the object.
        editor = self.editor
        canvas = MockCanvas()

        # Create an object that is hit regardless of click location
        class AlwaysHit(MockNonCurve):
            bounding_rect = Sketch.InfinityRect
            def Hit(self, *args):
                return True
            def Info(self):
                return "AlwaysHit"

        hitme = AlwaysHit()
        self.document.Insert(hitme, self.layer)
        self.editor.SelectObject(hitme)

        # Check if the object is indeed hit
        self.failUnless(hitme.Hit())
        self.failUnless(self.editor.selection.Hit(Point(0,0), Rect(0,0,0,0),
                        MockGC()))

        editor.SetTool("EditTool")

        #
        # simulate a mouse click+drag+release:
        #

        # ButtonPress should not give an error:
        editor.ButtonPress(canvas, Point(0, 0), 1, 0)

        editor.PointerMotion(canvas, Point(2, 0), Button1Mask)

        # And neither this (during a drag this ends up calling the
        # MultiEditor's CurrentInfoText method).  Since there's no
        # editable object, the document editor falls back to
        # SelectionInfoText, so check for that.
        self.assertEquals(editor.InfoText(), editor.SelectionInfoText())

        editor.ButtonRelease(canvas, Point(5, 0), 1, Button1Mask)

    def test_edit_noeditor_rubberbanding(self):
        """Test EditTool with a noneditable object and rubberbanding

        When e.g. a group containing a bezier curve is selected in edit
        mode and the user clicks on the bezier curve in the group, the
        editor should start rubberbanding.
        """
        editor = self.editor
        canvas = MockCanvas()

        # create a group with a line
        path = CreatePath()
        path.AppendLine(0, 0)
        path.AppendLine(10, 0)
        group = Group([PolyBezier(paths = (path,))])
        self.document.Insert(group, self.layer)

        # create another bezier curve
        path = CreatePath()
        path.AppendLine(5, 5)
        path.AppendLine(5, 100)
        poly = PolyBezier(paths = (path,))
        self.document.Insert(poly, self.layer)

        # select both objects and enter edit mode
        self.editor.SelectObject([group,  poly])
        editor.SetTool("EditTool")

        # simulate a mouse click+drag+release starting on the line in
        # the group.  The editor should start rubberbanding.  After the
        # release the start point of poly should be selected.
        editor.ButtonPress(canvas, Point(2, 0), 1, 0)
        editor.PointerMotion(canvas, Point(5, 3), Button1Mask)
        editor.ButtonRelease(canvas, Point(8, 20), 1, Button1Mask)

        # The first handle of poly should be selected now.
        # FIXME: This test is a bit too strict in that it assumes that
        # the handle for the selected node is the first in the list
        # returned by the Handles() method
        self.assertEquals(editor.Handles()[0].type, HandleSelectedNode)

    def test_select_simple(self):
        """Test EditorWithSelection with SelectTool (very simple)"""
        editor = self.editor
        canvas = MockCanvas()

        path = CreatePath()
        path.AppendLine(0, 0)
        path.AppendLine(10, 10)
        poly = PolyBezier(paths = (path,))

        self.document.Insert(poly, self.layer)

        editor.SetTool("SelectionTool")

        # Select with rubber banding.  Since the default join is mitred
        # join, the bounding rect of poly is quite a bit larger (by more
        # than 5pt) than (0, 0, 10, 10), so make sure that start and
        # stop point define a sufficiently large rectangle.
        editor.ButtonPress(canvas, Point(-10, -10), 1, 0)
        editor.PointerMotion(canvas, Point(5, 0), Button1Mask)
        editor.ButtonRelease(canvas, Point(20, 25), 1, Button1Mask)

        # Check the new state of the poly line
        obj = self.editor.CurrentObject()
        self.failUnless(obj is poly)


    def test_text_simple(self):
        """Test EditorWithSelection with TextTool (very simple)"""
        editor = self.editor
        canvas = MockCanvas()

        editor.SetTool("TextTool")

        # Click somewhere on the document to start creating a new text
        # object
        editor.ButtonPress(canvas, Point(100, 100), 1, 0)
        editor.ButtonRelease(canvas, Point(100, 100), 1, Button1Mask)

        # Now type some text
        tool = editor.Tool()
        for c in "Skencil":
            tool.CallObjectEditorMethod(Sketch.Editor.CommonTextEditor,
                                        'InsertCharacter', c)

        # Now select a different tool, e.g. the selection tool.  Text
        # editing should be finished and the new text object the
        # selected object
        editor.SetTool("SelectionTool")

        obj = editor.CurrentObject()
        self.assert_(obj.is_Text)
        self.assertEquals(obj.Text(), "Skencil")

    def test_restore_last_selected(self):
        """Test restoring the last selected list after undo.

        Create and insert two rectangles into the document.
        Select them both, and the select the second one again to make it the
        last selected object.

        Then insert another rectangle. Undoing this operation should restore
        the previous selection.
        """
        editor = self.editor
        r1 = Rectangle(Trafo(100, 0, 0, 100, 10, 10))
        editor.Insert(r1)
        r2 = Rectangle(Trafo(100, 0, 0, 100, 120, 240))
        editor.Insert(r2)
        editor.SelectObject([r1, r2])
        editor.SelectObject([r2], SelectAdd)
        self.assertEquals(self.editor.SelectedObjects(), [r1, r2])
        self.assertEquals(self.editor.LastSelectedObjects(), [r2])

        r3 = Rectangle(Trafo(100, 0, 0, 100, 240, 120))
        self.editor.Insert(r3)
        self.assertEquals(self.editor.SelectedObjects(), [r3])
        self.assertEquals(self.editor.LastSelectedObjects(), [r3])

        self.document.Undo()
        # Check if the selection is the same as before the last insert.
        self.assertEquals(self.editor.SelectedObjects(), [r1, r2])
        self.assertEquals(self.editor.LastSelectedObjects(), [r2])

class TestSelectionMessages(DocEditorTestsWithMessages):

    subscribed_channels = [("editor", [OBJECT_CHANGED, SELECTION])]

    def setUp(self):
        DocEditorTestsWithMessages.setUp(self)
        self.canvas = MockCanvas()

        self.r1 = Rectangle(Trafo(100, 0, 0, 100, 10, 10))
        self.r2 = Rectangle(Trafo(100, 0, 0, 100, 120, 240))
        self.r3 = Rectangle(Trafo(100, 0, 0, 100, 240, 120))

        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)


        self.editor.SetTool("SelectionTool")

    def test_select_deselect(self):
        """Test SELECTION messages"""
        editor = self.editor
        canvas = self.canvas
        r1 = self.r1
        r2 = self.r2
        r3 = self.r3
        # Select first two rectangles with rubber banding.
        editor.ButtonPress(canvas, Point(-10, -10), 1, 0)
        editor.PointerMotion(canvas, Point(240, 380), Button1Mask)
        editor.ButtonRelease(canvas, Point(240, 380), 1, Button1Mask)

        # A SELECTION message should have been sent
        self.assertEquals(self.received_messages, [(SELECTION,)])
        self.assertEquals(editor.SelectedObjects(), [r1, r2])
        self.assertEquals(editor.LastSelectedObjects(), [r1, r2])

        # select r2 again by shift-clicking on it
        editor.ButtonClick(canvas, Point(120, 240), 1, ShiftMask)
        # A second SELECTION message should have been sent
        self.assertEquals(self.received_messages, [(SELECTION,), (SELECTION,)])
        self.assertEquals(editor.SelectedObjects(), [r1, r2])
        # Last selected should contain r2
        self.assertEquals(editor.LastSelectedObjects(), [r2])

        # Remove r2 from selection with ctrl-click
        editor.ButtonClick(canvas, Point(120, 240), 1, ControlMask)
        # A third SELECTION message should have been sent
        self.assertEquals(self.received_messages, [(SELECTION,), (SELECTION,),
                                                   (SELECTION,)])
        self.assertEquals(editor.SelectedObjects(), [r1])
        # Since r2 was the last selected, now the list is the same as the
        # whole selection
        self.assertEquals(editor.LastSelectedObjects(),
                          editor.SelectedObjects())

    def test_normalize(self):
        """Test whether the selection is normalized"""
        editor = self.editor
        canvas = self.canvas
        r1 = self.r1
        r2 = self.r2
        r3 = self.r3

        editor.SelectObject([r3, r2, r2, r2, r1])

        self.assertEquals(self.received_messages, [(SELECTION,)])
        # Duplicates should have been removed from the selection and the
        # objects sorted according to stacking order
        self.assertEquals(editor.SelectedObjects(), [r1, r2, r3])
        self.assertEquals(editor.LastSelectedObjects(), [r1, r2, r3])

    def test_select_group(self):
        """Test selecting a group when clicking on a child"""
        editor = self.editor
        canvas = self.canvas

        # Create a group from r1 and r2
        self.layer.RemoveObjects([self.r1, self.r2])
        g = Group([self.r1, self.r2])
        self.document.Insert(g, self.layer)
        self.assertEquals(self.layer.GetObjects(), [self.r3, g])

        # click on r2, the group should be selected
        editor.ButtonClick(canvas, Point(120, 240), 1, 0)
        self.assertEquals(self.received_messages, [(SELECTION,)])
        self.assertEquals(editor.SelectedObjects(), [g])
        self.assertEquals(editor.LastSelectedObjects(), [g])

        # select r2 with Control-Shift-Click
        editor.ButtonClick(canvas, Point(120, 240), 1, ShiftMask | ControlMask)
        self.assertEquals(self.received_messages, [(SELECTION,), (SELECTION,)])
        self.assertEquals(editor.SelectedObjects(), [self.r2])
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])


class EditorTransactionTest(DocEditorTestsWithMessages):

    """Run a test for a method of EditorWithSelection

    This class implements a generic test for a method of the
    EditorWithSelection class that is implemented as a transaction
    modifying the document.  The test tests the action and the undoing
    and redoing of the action.

    Derived classes have to implement the following methods which will
    be called automatically:

    action()

       Called without arguments by the test to perform the action to
       test.  Usually the implementation will be a simple call to the
       method of self.editor that is to be tested.

    check_before(after_undo)

       Called before the call to the action method and after the undo
       has been performed.  When called before the action the single
       boolean parameter is False and when called after the undo the
       parameter is True.  The implementation should test the state of
       the editor and document before the action resp. after the undo
       which is usually very similar, but there can be slight
       differences, especially when checking which messages were sent by
       the document, so the parameter can be used to distinguish between
       the two calls.

    check_after(after_redo)

       Called after the call to action and after the redo.  When called
       after the action the single boolean parameter is False and when
       called after the redo it is True.  The implementation should test
       the state of the editor and document after the action.  The
       boolean parameter servers the same purpose as in check_before.

    Additionally derived classes will usually want to extend the setUp
    method to perform additional initialization.

    The class arranges for two document messages to be subscribed:
    OBJECT_CHANGED and REDRAW.  Derived classes can check the messages
    with the methods check_redraw_messages and
    check_object_changed_messages.  The generic test implementation
    clears the list of received messages immediately before the action()
    call and before undo and redo so that the check_before and
    check_after methods only see the messages that were sent during the
    latest change to self.document.
    """


    subscribed_channels = [("document", [OBJECT_CHANGED, REDRAW])]

    def clear_messages(self):
        """Clear the list of received messages."""
        self.received_messages = []

    def messages_from_channel(self, channel):
        """Return a list with all messages of the given channel

        The return value is a list with all items of
        self.received_messages that were received on the given channel.
        The channel itself which is the last item in each of the tuples
        due to the way the subscription works in
        DocEditorTestsWithMessages is omitted, though.  The order of the
        items in the return value is the same as in
        self.received_messages.
        """
        return [message[:-1] for message in self.received_messages
                             if message[-1] == channel]

    def check_redraw_messages(self, rects):
        """Check whether there was one REDRAW message with the given rects.

        Fail the test unless:

         - There is exactly one REDRAW message

         - That message does not indicate that the entire window has to
           be redrawn.

         - The set of rectangles in the message is the same as that in
           the rects parameter of this method.  Both sets are sequences
           with arbitrary order so sorted copies of them are compared.
        """
        redraw_messages = self.messages_from_channel(REDRAW)
        self.assertEquals(len(redraw_messages), 1)

        # check that this is not a redraw_all message
        self.assertEquals(redraw_messages[0][0], 0)

        # check the rects.  sort them because the order doesn't matter.
        # Make sure to sort copies so that we avoid unexpected
        # modifications.
        msg_rects = list(redraw_messages[0][1])
        msg_rects.sort()
        rects = list(rects)
        rects.sort()
        self.assertEquals(msg_rects, rects)

    def check_object_changed_messages(self, expected):
        """Check the OBJECT_CHANGED messages that were received.

        Since the order of the messages is not defined sorted copies of
        self.messages_from_channel(OBJECT_CHANGED) and the given list of
        messages in the expected parameters are compared.  This also
        means that expected should be a sequence of expected messages in
        the format returned by self.messages_from_channel.
        """
        messages = self.messages_from_channel(OBJECT_CHANGED)
        messages.sort()

        expected = list(expected)
        expected.sort()
        self.assertEquals(messages, expected)

    def test(self):
        # Implement the generic test.  This not a doc-string because
        # under some circumstances the unittest framework uses it to
        # derive a title for the test which would be the same for all
        # tests implemented by the derived classes and that would be
        # more or less useless.
        self.check_before(False)

        try:
            self.clear_messages()
            self.action()
        except:
            self.fail("Exception during action(): %s"
                      % "".join(traceback.format_exception(*sys.exc_info())))

        self.check_after(False)

        self.clear_messages()
        self.document.Undo()
        self.check_before(True)

        self.clear_messages()
        self.document.Redo()
        self.check_after(True)

    def __call__(self, result):
        """Override to only execute real test implementations.

        A real test implementations as far as this method is concerned
        is an instance which has a check_after method.  The check is for
        check_after and not e.g. action because it's useful to implement
        the latter in a common baseclass for all tests for a given
        editor method and that base class would not be a real test
        either.  The check_after method on the other hand actually tests
        somethin and has to be much more specific to an actual test.
        """
        if hasattr(self, "check_after"):
            DocEditorTestsWithMessages.__call__(self, result)


class DuplicationTest(EditorTransactionTest):

    """Common base class for EditorWithSelection.DuplicateSelected() tests"""

    def action(self):
        self.editor.DuplicateSelected()


class TestDuplicateSimple(DuplicationTest):

    """Test EditorWithSelection.DuplicateSelected() very simple

    Test a very simple version of DuplicateSelected: two objects on one
    layer.  One is selected and then duplicated.  Test whether the new
    object is inserted immediately above the originally selected object.
    """

    def setUp(self):
        DuplicationTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.editor.SelectObject(self.r1)

    def check_before(self, after_undo):
        # check whether the structure of the document and the selection
        # before the action to test is as we expect it.  When called
        # before the action this serves a sanity check.  When called
        # after the undo this is the test that the undo reverted things
        # to their original state.
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])

        if after_undo:
            # One OBJECT_CHANGED message should have been sent by
            # the layer for the removed rectangle.
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                               (REMOVED, (self.new_object,)))])
            # A REDRAW message should have been sent by the document with
            # the bounding rect of the new rectangle as parameter
            self.assertEquals(self.messages_from_channel(REDRAW),
                              [(0, [self.new_object.bounding_rect])])

    def action(self):
        self.editor.DuplicateSelected()

    def check_after(self, after_redo):
        # Now the duplicated object has been added to the layer just
        # above the originally selected object self.r1.
        objects = self.layer.GetObjects()
        self.new_object = objects[1]
        self.assertEquals(len(objects), 3)
        self.failUnless(objects[0] is self.r1)
        self.failUnless(objects[2] is self.r2)
        self.assertEquals(objects[1].Trafo(), Trafo(10, 0, 0, 10, 0, 0))

        # After duplication the new object should be selected
        self.assertEquals(self.editor.SelectedObjects(), [self.new_object])

        # One OBJECT_CHANGED message should have been sent by the
        # layer for the newly inserted rectangle.
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (INSERTED, (self.new_object,)))])
        # A REDRAW message should have been sent by the document with
        # the bounding rect of the new rectangle as parameter
        self.assertEquals(self.messages_from_channel(REDRAW),
                          [(0, [self.new_object.bounding_rect,])])


class TestDuplicateTwoObjects(DuplicationTest):

    """Test EditorWithSelection.DuplicateSelected() two selected objects

    Two objects on one layer.  Both are selected and then duplicated.
    Test whether the each new object is inserted immediately above the
    original object.
    """

    def setUp(self):
        DuplicationTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)

        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        # check whether the structure of the document and the selection
        # before the action to test is as we expect it.  When called
        # before the action this serves a sanity check.  When called
        # after the undo this is the test that the undo reverted things
        # to their original state.
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])

        if after_undo:
            # A REDRAW message should have been sent by the document with
            # the bounding rects of the new rectangles as parameters.
            self.check_redraw_messages([self.r1_duplicate.bounding_rect,
                                        self.r2_duplicate.bounding_rect])

    def action(self):
        # The actual operation to test
        self.editor.DuplicateSelected()

    def check_after(self, after_redo):
        # After the action or after a redo, the duplicated objects have
        # been added to the layer layer just above their respective
        # originals so the order is self.r1, self.r1's duplicate,
        # self.r2, self.r2's duplicate
        objects = self.layer.GetObjects()
        self.r1_duplicate = objects[1]
        self.r2_duplicate = objects[3]
        self.assertEquals(len(objects), 4)
        self.failUnless(objects[0] is self.r1)
        self.failUnless(objects[2] is self.r2)
        self.assertEquals(self.r1_duplicate.Trafo(), Trafo(10, 0, 0, 10, 0, 0))
        self.assertEquals(self.r2_duplicate.Trafo(), Trafo(10, 0, 0, 10,0,100))

        # After duplication the new object should be selected
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r1_duplicate, self.r2_duplicate])

        # A REDRAW message should have been sent by the document with
        # the bounding rects of the new rectangles as parameters.
        self.check_redraw_messages([self.r1_duplicate.bounding_rect,
                                    self.r2_duplicate.bounding_rect])

        # FIXME: Add test for the OBJECT_CHANGED messages.  This is a
        # bit tricky since there's only one message listing one of the
        # new rectangles.  This is due to the
        # EditDocument.object_changed method which forwards only one
        # message per (object, what) pair.  That is probably a bug.


class TestDuplicateTwoObjectsTwoLayers(DuplicationTest):

    """Test EditorWithSelection.DuplicateSelected() two selected objects

    Three objects on two different layers.  Two objects on different
    layers are selected and then duplicated.  Test whether the each new
    object is inserted immediately above the original object in the same
    layer as the original.
    """

    def setUp(self):
        DuplicationTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)

        self.new_layer = self.document.AppendLayer("new layer")
        self.r3 = Rectangle(Trafo(100, 0, 0, 20, -100, 50))
        self.document.Insert(self.r3, self.new_layer)

        self.editor.SelectObject([self.r1, self.r3])

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r3])
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.new_layer.GetObjects(), [self.r3])

        if after_undo:
            # A REDRAW message should have been sent by the document with
            # the bounding rects of the new rectangles as parameters.
            self.check_redraw_messages([self.r1_duplicate.bounding_rect,
                                        self.r3_duplicate.bounding_rect])

            # Two OBJECT_CHANGED messages should have been sent by the
            # layers because the new objects have been removed by the
            # undo.
            self.check_object_changed_messages(
                [(self.layer, CHILDREN, (REMOVED, (self.r1_duplicate,))),
                 (self.new_layer, CHILDREN, (REMOVED, (self.r3_duplicate,)))])

    def action(self):
        # The actual operation to test
        self.editor.DuplicateSelected()

    def check_after(self, after_redo):
        # The duplicate of self.r1 should be in self.layer immediately
        # above self.r1.  The duplicate of self.r3 is in self.new_layer
        # immediately above self.r3.
        objects = self.layer.GetObjects()
        self.r1_duplicate = objects[1]
        self.assertEquals(len(objects), 3)
        self.failUnless(objects[0] is self.r1)
        self.failUnless(objects[2] is self.r2)
        self.assertEquals(self.r1_duplicate.Trafo(), Trafo(10, 0, 0, 10, 0, 0))

        objects = self.new_layer.GetObjects()
        self.r3_duplicate = objects[1]
        self.assertEquals(len(objects), 2)
        self.failUnless(objects[0] is self.r3)
        self.assertEquals(self.r3_duplicate.Trafo(),
                          Trafo(100, 0, 0, 20, -100, 50))

        # After duplication the new object should be selected
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r1_duplicate, self.r3_duplicate])

        # A REDRAW message should have been sent by the document with
        # the bounding rects of the new rectangles as parameters.
        self.check_redraw_messages([self.r1_duplicate.bounding_rect,
                                    self.r3_duplicate.bounding_rect])

        # Two OBJECT_CHANGED messages should have been sent by the
        # layers for the newly inserted rectangles.
        self.check_object_changed_messages(
            [(self.layer, CHILDREN, (INSERTED, (self.r1_duplicate,))),
             (self.new_layer, CHILDREN, (INSERTED, (self.r3_duplicate,)))])


class TestDuplicateInGroup(DuplicationTest):

    """Test EditorWithSelection.DuplicateSelected() with grouped objects

    Two objects in a group.  One is selected and then duplicated.  Test
    whether the new object is inserted immediately above the originally
    selected object but in the same group.
    """

    def setUp(self):
        DuplicationTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.group = Group([self.r1, self.r2])
        self.document.Insert(self.group, self.layer)

        self.editor.SelectObject(self.r1)

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        if after_undo:
            # A REDRAW message should have been sent by the document with
            # the bounding rect of the new rectangle as parameter.
            self.check_redraw_messages([self.new_object.bounding_rect])

            # One OBJECT_CHANGED messages should have been sent by the
            # group because the new object has been removed again by the
            # undo
            self.check_object_changed_messages(
                [(self.group, CHILDREN, (REMOVED, (self.new_object,)))])

    def action(self):
        # The actual operation to test
        self.editor.DuplicateSelected()

    def check_after(self, after_redo):
        # Now the duplicated object has been added to the layer just
        # above the originally selected object self.r1.
        objects = self.group.GetObjects()
        self.new_object = objects[1]
        self.assertEquals(len(objects), 3)
        self.failUnless(objects[0] is self.r1)
        self.failUnless(objects[2] is self.r2)
        self.assertEquals(self.new_object.Trafo(), Trafo(10, 0, 0, 10, 0, 0))

        # After duplication the new object should be selected
        self.assertEquals(self.editor.SelectedObjects(), [self.new_object])

        # A REDRAW message should have been sent by the document with
        # the bounding rect of the new rectangle as parameter.
        self.check_redraw_messages([self.new_object.bounding_rect])

        # One OBJECT_CHANGED messages should have been sent by the group
        # for the newly inserted rectangle.
        self.check_object_changed_messages(
            [(self.group, CHILDREN, (INSERTED, (self.new_object,)))])



class RemovalTest(EditorTransactionTest):

    """Common base class for EditorWithSelection.RemoveSelected() tests"""

    def action(self):
        self.editor.RemoveSelected()


class TestRemoveSimple(RemovalTest):
    """Test EditorWithSelection.RemoveSelected() simple

    Create three rectangles in the current layer.  Select one of them.
    RemoveSelected should remove the selected object.  The others should
    still be there.  The selection should be empty afterwards because
    the selected object has been removed.
    """

    def setUp(self):
        RemovalTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)

        self.editor.SelectObject(self.r2)

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])
        self.assertEquals(self.editor.LastSelectedObjects(), [self.r2])

        if after_undo:
            # The area defined by the bounding box of the selected
            # object has to be redrawn after the undo
            self.check_redraw_messages([self.r2.bounding_rect])

            # One OBJECT_CHANGED message for the removed rectangle which
            # was reinserted by the undo
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (INSERTED, (self.r2,)))])

    def check_after(self, after_redo):
        # self.r2 should be gone now
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r3])

        # The selection should be empty now because the selected objects
        # have been removed
        self.assertEquals(self.editor.SelectedObjects(), [])
        self.assertEquals(self.editor.LastSelectedObjects(), [])

        # The area defined by the bounding box of the selected object
        # has to be redrawn
        self.check_redraw_messages([self.r2.bounding_rect])

        # One OBJECT_CHANGED message for the removed rectangle.
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REMOVED, (self.r2,)))])

class TestRemoveFromGroup(RemovalTest):

    """Test EditorWithSelection.RemoveSelected() from group

    Create a group with three rectangles and select one of them.
    RemoveSelected should remove the selected object only.  The group
    and the other rectangles should still be there.  The selection
    should be empty afterwards because the selected object has been
    removed.
    """

    def setUp(self):
        RemovalTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.group = Group([self.r1, self.r2, self.r3])
        self.document.Insert(self.group, self.layer)
        self.editor.SelectObject(self.r2)

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])
        self.assertEquals(self.editor.LastSelectedObjects(), [self.r2])

        if after_undo:
            # The area defined by the bounding box of the selected
            # object has to be redrawn after the undo
            self.check_redraw_messages([self.r2.bounding_rect])

            # One OBJECT_CHANGED message for the removed rectangle which
            # was reinserted by the undo
            self.check_object_changed_messages([(self.group, CHILDREN,
                                                 (INSERTED, (self.r2,)))])

    def check_after(self, after_redo):
        # self.r2 should be gone now
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r3])
        # but self.group should still be there
        self.assertEquals(self.layer.GetObjects(), [self.group])

        # The selection should be empty now because the selected objects
        # have been removed
        self.assertEquals(self.editor.SelectedObjects(), [])
        self.assertEquals(self.editor.LastSelectedObjects(), [])

        # The area defined by the bounding box of the selected object
        # has to be redrawn
        self.check_redraw_messages([self.r2.bounding_rect])

        # One OBJECT_CHANGED message for the removed rectangle.
        self.check_object_changed_messages([(self.group, CHILDREN,
                                             (REMOVED, (self.r2,)))])



class MoveUpTest(EditorTransactionTest):

    """Common base class for tests of EditorWithSelection.MoveSelectedUp()"""

    def action(self):
        self.editor.MoveSelectedUp()


class TestMoveUpSimple(MoveUpTest):

    """Test EditorWithSelection.MoveSelectedUp() very simple

    Test a very simple version of MoveSelectedUp: two objects on one
    layer.  The bottom one is selected and then moved up.  Test whether
    the order of the objects is correct after that.
    """

    def setUp(self):
        MoveUpTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.editor.SelectObject(self.r1)

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED, [self.r1]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r1.bounding_rect])

    def check_after(self, after_redo):
        # Now self.r1 should be above self.r2
        self.assertEquals(self.layer.GetObjects(), [self.r2, self.r1])

        # self.r1 should still be selected
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        # There must have been one OBJECT_CHANGED message from
        # self.layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED, [self.r1]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r1.bounding_rect])


class TestMoveUpSeveral(MoveUpTest):

    """Test EditorWithSelection.MoveSelectedUp() several objects at once

    Several objects on one layer.  Two immediately above each other but
    below the topmost object are selected and then moved up.  This was
    chosen so that they actually would move up and to test that their
    relative order doesn't change.  Test whether the order of the
    objects is correct after that.
    """

    def setUp(self):
        MoveUpTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)
        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r1, self.r2]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r1.bounding_rect,
                                        self.r2.bounding_rect])

    def check_after(self, after_redo):
        # Now self.r1 and self.r2 should be above self.r3 and self.r2
        # still above self.r1
        self.assertEquals(self.layer.GetObjects(), [self.r3, self.r1, self.r2])

        # The selection must not have changed
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

        # There must have been one OBJECT_CHANGED message from
        # self.layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r1, self.r2]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r1.bounding_rect,
                                    self.r2.bounding_rect])


class TestMoveUpAllChildren(MoveUpTest):

    """Test EditorWithSelection.MoveSelectedUp() all objects in layer

    Two objects on one layer.  Both are selected and then moved up.
    This is a special case in which no reordering must take place
    because the topmost object can't be moved up and if the next lower
    object would were moved up the relative order of the selected
    objects would change.  Test that indeed nothing happens.
    """

    # We do not check for REDRAW or OBJECT_CHANGED messages in this test
    # because ideally the document should not send those messages if
    # nothing actually changes.  The document does send them currently
    # (20050115) but it may stop doing so in the future.

    def setUp(self):
        MoveUpTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        # sanity check whether the selection is correct
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

    def check_after(self, after_redo):
        # Since all objects in the layer are selected, nothing should
        # have been done
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])

        # The selection should still be the same
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])


class TestMoveUpInSeveralLayers(MoveUpTest):

    """Test EditorWithSelection.DuplicateSelected() with multiple layers

    Two different layers with several objects each.  On each layer
    several objects are seleced and then moved up.  Test the order of
    the objects afterwards.
    """

    def setUp(self):
        MoveUpTest.setUp(self)
        self.r11 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r12 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r13 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r11, self.layer)
        self.document.Insert(self.r12, self.layer)
        self.document.Insert(self.r13, self.layer)

        self.new_layer = self.document.AppendLayer("new layer")
        self.r21 = Rectangle(Trafo(10, 0, 0, 10, 100, 0))
        self.r22 = Rectangle(Trafo(10, 0, 0, 10, 100, 100))
        self.r23 = Rectangle(Trafo(10, 0, 0, 10, 100, 200))
        self.document.Insert(self.r21, self.new_layer)
        self.document.Insert(self.r22, self.new_layer)
        self.document.Insert(self.r23, self.new_layer)

        self.editor.SelectObject([self.r11, self.r13, self.r21, self.r22])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r11, self.r12,
                                                    self.r13])
        self.assertEquals(self.new_layer.GetObjects(), [self.r21, self.r22,
                                                        self.r23])
        self.assertEquals(self.editor.SelectedObjects(), [self.r11, self.r13,
                                                          self.r21, self.r22])
        if after_undo:
            # There must have been two OBJECT_CHANGED messages from
            # the layers
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r11, self.r13])),
                                                (self.new_layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r21, self.r22]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r11.bounding_rect,
                                        self.r13.bounding_rect,
                                        self.r21.bounding_rect,
                                        self.r22.bounding_rect])

    def check_after(self, after_redo):
        # Check the order of the children of self.layer and self.new_layer
        self.assertEquals(self.layer.GetObjects(), [self.r12, self.r11,
                                                    self.r13])
        self.assertEquals(self.new_layer.GetObjects(), [self.r23, self.r21,
                                                        self.r22])
        # the selection should not have changed.
        self.assertEquals(self.editor.SelectedObjects(), [self.r11, self.r13,
                                                          self.r21, self.r22])

        # There must have been two OBJECT_CHANGED messages from
        # the layers
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r11, self.r13])),
                                            (self.new_layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r21, self.r22]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r11.bounding_rect,
                                    self.r13.bounding_rect,
                                    self.r21.bounding_rect,
                                    self.r22.bounding_rect])

class TestMoveUpInGroup(MoveUpTest):

    """Test EditorWithSelection.MoveSelectedUp() objects in a group

    A group with two objects.  The lower one is selected and moved up.
    Check the order of the children afterwards.
    """

    def setUp(self):
        MoveUpTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.group = Group([self.r1, self.r2])
        self.document.Insert(self.group, self.layer)
        self.editor.SelectObject(self.r1)

    def check_before(self, after_undo):
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.group
            self.check_object_changed_messages([(self.group, CHILDREN,
                                                 (REARRANGED, [self.r1]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r1.bounding_rect])

    def check_after(self, after_redo):
        # The order of the children of the self.group should have changed
        # now.
        self.assertEquals(self.group.GetObjects(), [self.r2, self.r1])

        # The selection should still be the same, though.
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        # There must have been one OBJECT_CHANGED message from
        # self.group
        self.check_object_changed_messages([(self.group, CHILDREN,
                                             (REARRANGED, [self.r1]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r1.bounding_rect])


class MoveDownTest(EditorTransactionTest):

    """Common base class for tests of EditorWithSelection.MoveSelectedDown()"""

    def action(self):
        self.editor.MoveSelectedDown()



class TestMoveDownSimple(MoveDownTest):

    """Test EditorWithSelection.MoveSelectedDown() very simple

    Test a very simple version of MoveSelectedDown: two objects on one
    layer.  The top one is selected and then moved down.  Test the order
    of the children afterwards.
    """

    def setUp(self):
        MoveDownTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)

        self.editor.SelectObject(self.r2)

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED, [self.r2]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r2.bounding_rect])

    def check_after(self, after_redo):
        # Now self.r2 should be below self.r1
        self.assertEquals(self.layer.GetObjects(), [self.r2, self.r1])

        # self.r2 should still be selected
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])

        # There must have been one OBJECT_CHANGED message from
        # self.layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED, [self.r2]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r2.bounding_rect])



class TestMoveDownSeveral(MoveDownTest):

    """Test EditorWithSelection.MoveSelectedDown() several objects at once

    Several objects on one layer.  Two immediately above each other but
    above the lowermost object are selected and then moved down.  This
    was chosen so that they actually would move down and to test that
    their relative order doesn't change.  Test the order of the children
    afterwards.
    """

    def setUp(self):
        MoveDownTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)

        self.editor.SelectObject([self.r2, self.r3])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r2, self.r3])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r2, self.r3]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r2.bounding_rect,
                                        self.r3.bounding_rect])

    def check_after(self, after_redo):
        # Now self.r1 should be above self.r2 and self.r3 and self.r3
        # still above self.r2
        self.assertEquals(self.layer.GetObjects(), [self.r2, self.r3, self.r1])

        # The selection should not have changed
        self.assertEquals(self.editor.SelectedObjects(), [self.r2, self.r3])

        # There must have been one OBJECT_CHANGED message from
        # self.layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED, [self.r2,self.r3]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r2.bounding_rect,
                                    self.r3.bounding_rect])


class TestMoveDownAllChildren(MoveDownTest):

    """Test EditorWithSelection.MoveSelectedDown() all objects in layer

    Two objects on one layer.  Both are selected and then moved down.
    This is a special case in which no reordering must take place
    because the lowermost object can't be moved down and if the next
    higher object were moved down the relative order of the selected
    objects would change.  Test that indeed nothing happens.
    """

    # We do not check for REDRAW or OBJECT_CHANGED messages in this test
    # because ideally the document should not send those messages if
    # nothing actually changes.  The document does send them currently
    # (20050115) but it may stop doing so in the future.

    def setUp(self):
        MoveDownTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)

        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

    def check_after(self, after_redo):
        # Since all objects in the layer are selected, nothing should
        # have been done
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])

        # The selection should still be the same
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])


class TestMoveDownInSeveralLayers(MoveDownTest):

    """Test EditorWithSelection.MoveSelectedDown() with multiple layers

    Two different layers with several objects each.  On each layer
    several objects are seleced and then moved down.  Test the order of
    the objects afterwards.
    """

    def setUp(self):
        MoveDownTest.setUp(self)
        self.r11 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r12 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r13 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r11, self.layer)
        self.document.Insert(self.r12, self.layer)
        self.document.Insert(self.r13, self.layer)

        self.new_layer = self.document.AppendLayer("new layer")
        self.r21 = Rectangle(Trafo(10, 0, 0, 10, 100, 0))
        self.r22 = Rectangle(Trafo(10, 0, 0, 10, 100, 100))
        self.r23 = Rectangle(Trafo(10, 0, 0, 10, 100, 200))
        self.document.Insert(self.r21, self.new_layer)
        self.document.Insert(self.r22, self.new_layer)
        self.document.Insert(self.r23, self.new_layer)

        self.editor.SelectObject([self.r11, self.r13, self.r22, self.r23])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(),
                          [self.r11, self.r12, self.r13])
        self.assertEquals(self.new_layer.GetObjects(),
                          [self.r21, self.r22, self.r23])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r11, self.r13, self.r22, self.r23])

        if after_undo:
            # There must have been two OBJECT_CHANGED messages from
            # the layers
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r11, self.r13])),
                                                (self.new_layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r22, self.r23]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r11.bounding_rect,
                                        self.r13.bounding_rect,
                                        self.r22.bounding_rect,
                                        self.r23.bounding_rect])

    def check_after(self, after_redo):
        # Check the order of the children of self.layer and self.new_layer
        self.assertEquals(self.layer.GetObjects(),
                          [self.r11, self.r13, self.r12])
        self.assertEquals(self.new_layer.GetObjects(),
                          [self.r22, self.r23, self.r21])
        # the selection should not have changed.
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r11, self.r13, self.r22, self.r23])

        # There must have been two OBJECT_CHANGED messages from
        # the layers
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r11, self.r13])),
                                            (self.new_layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r22, self.r23]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r11.bounding_rect,
                                    self.r13.bounding_rect,
                                    self.r22.bounding_rect,
                                    self.r23.bounding_rect])


class TestMoveDownInGroup(MoveDownTest):

    """Test EditorWithSelection.MoveSelectedDown() objects in a group

    A group with two objects.  The upper one is selected and moved down.
    Check the order of the children afterwards.
    """

    def setUp(self):
        MoveDownTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.group = Group([self.r1, self.r2])
        self.document.Insert(self.group, self.layer)

        self.editor.SelectObject(self.r2)

    def check_before(self, after_undo):
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.group
            self.check_object_changed_messages([(self.group, CHILDREN,
                                                 (REARRANGED, [self.r2]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r2.bounding_rect])

    def check_after(self, after_redo):
        # The order of the children of the group should have changed
        # now.
        self.assertEquals(self.group.GetObjects(), [self.r2, self.r1])

        # The selection should still be the same, though.
        self.assertEquals(self.editor.SelectedObjects(), [self.r2])

        # There must have been one OBJECT_CHANGED message from
        # self.group
        self.check_object_changed_messages([(self.group, CHILDREN,
                                             (REARRANGED, [self.r2]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r2.bounding_rect])


class MoveToTopTest(EditorTransactionTest):

    """Common base class for tests of EditorWithSelection.MoveSelectedToTop()
    """

    def action(self):
        self.editor.MoveSelectedToTop()


class TestMoveToTopSimple(MoveToTopTest):

    """Test EditorWithSelection.MoveSelectedToTop() very simple

    Three objects on one layer.  The bottom one is selected and then
    moved up.  Test whether the object is at the top afterwards.
    """
    # Note: Make sure that this testcase can't be fulfilled with the
    # semantics of MoveUp so we have three objects and select he
    # lowermost one.

    def setUp(self):
        MoveToTopTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)

        self.editor.SelectObject(self.r1)

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED, [self.r1]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r1.bounding_rect])

    def check_after(self, after_redo):
        # self.r1 should be on top now
        self.assertEquals(self.layer.GetObjects(), [self.r2, self.r3, self.r1])

        # The selection should not have changed
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        # There must have been one OBJECT_CHANGED message from
        # self.layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED, [self.r1]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r1.bounding_rect])


class TestMoveToTopSeveral(MoveToTopTest):

    """Test EditorWithSelection.MoveSelectedToTop() several objects

    Several objects on one layer.  Two objects not including the topmost
    one are selected and then moved to the top.  This was chosen so that
    they actually would move up and to test that their relative order
    doesn't change.  Test the order of the objects is correct afterwards.
    """
    def setUp(self):
        MoveToTopTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.r4 = Rectangle(Trafo(10, 0, 0, 10, 0, 400))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)
        self.document.Insert(self.r4, self.layer)

        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(),
                          [self.r1, self.r2, self.r3, self.r4])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r1, self.r2]))])

            # The area of the selected rectangles' bounding boxes must
            # be redrawn
            self.check_redraw_messages([self.r1.bounding_rect,
                                        self.r2.bounding_rect])

    def check_after(self, after_redo):
        # self.r1 and self.r2 should be at the top.  The relative order of the
        # other objects should not have changed.
        self.assertEquals(self.layer.GetObjects(),
                          [self.r3, self.r4, self.r1, self.r2])

        # The selection should not have changed
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

        # There must have been one OBJECT_CHANGED message from
        # self.layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r1, self.r2]))])

        # The area of the selected rectangles' bounding boxes must
        # be redrawn
        self.check_redraw_messages([self.r1.bounding_rect,
                                    self.r2.bounding_rect])


class TestMoveToTopAllChildren(MoveToTopTest):

    """Test EditorWithSelection.MoveSelectedToTop() all objects in layer

    Two objects on one layer.  Both are selected and then moved to the
    top.  This is a special case in which no reordering must take place
    because the none of the objects can be moved without changing their
    relative stacking order.  Test that indeed nothing happens.
    """

    # We do not check for REDRAW or OBJECT_CHANGED messages in this test
    # because ideally the document should not send those messages if
    # nothing actually changes.  The document does send them currently
    # (20050115) but it may stop doing so in the future.

    def setUp(self):
        MoveToTopTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)

        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

    def check_after(self, after_redo):
        # Since all objects in the layer are selected, nothing should
        # have been done
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])

        # The selection should still be the same
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])


class TestMoveToTopInSeveralLayers(MoveToTopTest):

    """Test EditorWithSelection.MoveSelectedToTop() with multiple layers

    Two different layers with several objects each.  On each layer
    several objects are seleced and then moved to the top.  Test the
    order of the objects afterwards.
    """

    def setUp(self):
        MoveToTopTest.setUp(self)
        self.r11 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r12 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r13 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.r14 = Rectangle(Trafo(10, 0, 0, 10, 0, 300))
        self.document.Insert(self.r11, self.layer)
        self.document.Insert(self.r12, self.layer)
        self.document.Insert(self.r13, self.layer)
        self.document.Insert(self.r14, self.layer)

        self.new_layer = self.document.AppendLayer("new layer")
        self.r21 = Rectangle(Trafo(10, 0, 0, 10, 100, 0))
        self.r22 = Rectangle(Trafo(10, 0, 0, 10, 100, 100))
        self.r23 = Rectangle(Trafo(10, 0, 0, 10, 100, 200))
        self.r24 = Rectangle(Trafo(10, 0, 0, 10, 100, 300))
        self.document.Insert(self.r21, self.new_layer)
        self.document.Insert(self.r22, self.new_layer)
        self.document.Insert(self.r23, self.new_layer)
        self.document.Insert(self.r24, self.new_layer)

        self.editor.SelectObject([self.r11, self.r13, self.r22, self.r23])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(),
                          [self.r11, self.r12, self.r13, self.r14])
        self.assertEquals(self.new_layer.GetObjects(),
                          [self.r21, self.r22, self.r23, self.r24])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r11, self.r13, self.r22, self.r23])

        if after_undo:
            # There must have been two OBJECT_CHANGED messages from
            # the layers
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r11, self.r13])),
                                                (self.new_layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r22, self.r23]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r11.bounding_rect,
                                        self.r13.bounding_rect,
                                        self.r22.bounding_rect,
                                        self.r23.bounding_rect])

    def check_after(self, after_redo):
        # Check the order of the children of self.layer and self.new_layer
        self.assertEquals(self.layer.GetObjects(),
                          [self.r12, self.r14, self.r11, self.r13])
        self.assertEquals(self.new_layer.GetObjects(),
                          [self.r21, self.r24, self.r22, self.r23])
        # the selection should not have changed.
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r11, self.r13, self.r22, self.r23])

        # There must have been two OBJECT_CHANGED messages from
        # the layers
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r11, self.r13])),
                                            (self.new_layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r22, self.r23]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r11.bounding_rect,
                                    self.r13.bounding_rect,
                                    self.r22.bounding_rect,
                                    self.r23.bounding_rect])


class TestMoveToTopInGroup(MoveToTopTest):

    """Test EditorWithSelection.MoveSelectedToTop() objects in a group

    A group with two objects.  The upper one is selected and moved down.
    Check the order of the children afterwards.
    """

    def setUp(self):
        MoveToTopTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.group = Group([self.r1, self.r2, self.r3])
        self.document.Insert(self.group, self.layer)

        self.editor.SelectObject(self.r1)

    def check_before(self, after_undo):
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from
            # self.group
            self.check_object_changed_messages([(self.group, CHILDREN,
                                                 (REARRANGED, [self.r1]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r1.bounding_rect])

    def check_after(self, after_redo):
        # The order of the children of the group should have changed
        # now.
        self.assertEquals(self.group.GetObjects(), [self.r2, self.r3, self.r1])

        # The selection should still be the same, though.
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])

        # There must have been one OBJECT_CHANGED message from
        # self.group
        self.check_object_changed_messages([(self.group, CHILDREN,
                                             (REARRANGED, [self.r1]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r1.bounding_rect])


class TestMoveToTopMixed(MoveToTopTest):

    """Test EditorWithSelection.MoveSelectedToTop() in groups and layers

    A trickier case: A layer with 4 rectanges r1, ..., r4 and a group G
    with three rectangles [r21, r22, r23].  The children of the layer
    are [r1, r2, G, r3, r4].  The selection is [r1, r21, r3].  The
    selection is moved to the top.  Afterwards, The order in the layer
    must be [r2, G, r4, r1, r3] and in the group [r22, r23, r21].
    """

    def setUp(self):
        MoveToTopTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.r4 = Rectangle(Trafo(10, 0, 0, 10, 0, 300))
        self.r21 = Rectangle(Trafo(10, 0, 0, 10, 100, 0))
        self.r22 = Rectangle(Trafo(10, 0, 0, 10, 100, 100))
        self.r23 = Rectangle(Trafo(10, 0, 0, 10, 100, 200))
        self.group = Group([self.r21, self.r22, self.r23])
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.group, self.layer)
        self.document.Insert(self.r3, self.layer)
        self.document.Insert(self.r4, self.layer)

        self.editor.SelectObject([self.r1, self.r21, self.r3])

    def check_before(self, after_undo):
        self.assertEquals(self.group.GetObjects(),
                          [self.r21, self.r22, self.r23])
        self.assertEquals(self.layer.GetObjects(),
                          [self.r1, self.r2, self.group, self.r3, self.r4])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r1, self.r21, self.r3])

        if after_undo:
            # There must have been two OBJECT_CHANGED messages from
            # the layers
            self.check_object_changed_messages([(self.group, CHILDREN,
                                                 (REARRANGED, [self.r21])),
                                                (self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r1, self.r3]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r21.bounding_rect,
                                        self.r1.bounding_rect,
                                        self.r3.bounding_rect])

    def check_after(self, after_redo):
        # The order of the children of the group should have changed
        # now.
        self.assertEquals(self.group.GetObjects(),
                          [self.r22, self.r23, self.r21])
        self.assertEquals(self.layer.GetObjects(),
                          [self.r2, self.group, self.r4, self.r1, self.r3])

        # The selection should still be the same, though.
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r21, self.r1, self.r3])

        # There must have been two OBJECT_CHANGED messages from
        # the layers
        self.check_object_changed_messages([(self.group, CHILDREN,
                                             (REARRANGED, [self.r21])),
                                            (self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r1, self.r3]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r21.bounding_rect,
                                    self.r1.bounding_rect,
                                    self.r3.bounding_rect])


class MoveToBottomTest(EditorTransactionTest):

    """Base class for tests of EditorWithSelection.MoveSelectedToBottom()
    """

    def action(self):
        self.editor.MoveSelectedToBottom()


class TestMoveToBottomSimple(MoveToBottomTest):

    """Test EditorWithSelection.MoveSelectedToBottom() very simple

    Three objects on one layer.  The top one is selected and then moved
    up.  Test whether the object is at the bottom afterwards.
    """
    # Note: Make sure that this testcase can't be fulfilled with the
    # semantics of MoveDown so we have three objects and select he
    # topmost one.

    def setUp(self):
        MoveToBottomTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)

        self.editor.SelectObject(self.r3)

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r3])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from the layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED, [self.r3]))])

            # The area of the selected rectangle's bounding box must be
            # redrawn
            self.check_redraw_messages([self.r3.bounding_rect])

    def check_after(self, after_redo):
        # self.r3 should be on bottom now
        self.assertEquals(self.layer.GetObjects(), [self.r3, self.r1, self.r2])

        # The selection should not have changed
        self.assertEquals(self.editor.SelectedObjects(), [self.r3])

        # There must have been one OBJECT_CHANGED message from the layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED, [self.r3]))])

        # The area of the selected rectangle's bounding box must be
        # redrawn
        self.check_redraw_messages([self.r3.bounding_rect])


class TestMoveToBottomSeveral(MoveToBottomTest):

    """Test EditorWithSelection.MoveSelectedToBottom() several objects

    Several objects on one layer.  Two objects not including the
    bottommost one are selected and then moved to the bottom.  This was
    chosen so that they actually would move down and to test that their
    relative order doesn't change.  Test the order of the objects is
    correct afterwards.
    """

    def setUp(self):
        MoveToBottomTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.r4 = Rectangle(Trafo(10, 0, 0, 10, 0, 400))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)
        self.document.Insert(self.r4, self.layer)

        self.editor.SelectObject([self.r3, self.r4])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(),
                          [self.r1, self.r2, self.r3, self.r4])
        self.assertEquals(self.editor.SelectedObjects(), [self.r3, self.r4])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from the layer
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r3, self.r4]))])

            # The area of the selected rectangles' bounding box must be
            # redrawn
            self.check_redraw_messages([self.r3.bounding_rect,
                                        self.r4.bounding_rect])

    def check_after(self, after_redo):
        # self.r3 and self.r4 should be at the bottom.  The relative
        # order of the other objects should not have changed.
        self.assertEquals(self.layer.GetObjects(),
                          [self.r3, self.r4, self.r1, self.r2])

        # The selection should not have changed
        self.assertEquals(self.editor.SelectedObjects(), [self.r3, self.r4])

        # There must have been one OBJECT_CHANGED message from the layer
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r3, self.r4]))])

        # The area of the selected rectangles' bounding box must be
        # redrawn
        self.check_redraw_messages([self.r3.bounding_rect,
                                    self.r4.bounding_rect])


class TestMoveToBottomAllChildren(MoveToBottomTest):

    """Test EditorWithSelection.MoveSelectedToBottom() all objects in layer

    Two objects on one layer.  Both are selected and then moved to the
    bottom.  This is a special case in which no reordering must take
    place because the none of the objects can be moved without changing
    their relative stacking order.  Test that indeed nothing happens.
    """

    # We do not check for messages in this test because ideally the
    # document should not send REDRAW or OBJECT_CHANGED messages because
    # nothing actually changes.  The document does send them currently
    # (20050115) but it may correctly stop doing so in the future.

    def setUp(self):
        MoveToBottomTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)

        self.editor.SelectObject([self.r1, self.r2])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])

    def check_after(self, after_redo):
        # Since all objects in the layer are selected, nothing should
        # have been done
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2])

        # The selection should still be the same
        self.assertEquals(self.editor.SelectedObjects(), [self.r1, self.r2])


class TestMoveToBottomInSeveralLayers(MoveToBottomTest):

    """Test EditorWithSelection.MoveSelectedToBottom() with multiple layers

    Two different layers with several objects each.  On each layer
    several objects are seleced and then moved to the bottom.  Test the
    order of the objects afterwards.
    """

    def setUp(self):
        MoveToBottomTest.setUp(self)
        self.r11 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r12 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r13 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.r14 = Rectangle(Trafo(10, 0, 0, 10, 0, 300))
        self.document.Insert(self.r11, self.layer)
        self.document.Insert(self.r12, self.layer)
        self.document.Insert(self.r13, self.layer)
        self.document.Insert(self.r14, self.layer)

        self.new_layer = self.document.AppendLayer("new layer")
        self.r21 = Rectangle(Trafo(10, 0, 0, 10, 100, 0))
        self.r22 = Rectangle(Trafo(10, 0, 0, 10, 100, 100))
        self.r23 = Rectangle(Trafo(10, 0, 0, 10, 100, 200))
        self.r24 = Rectangle(Trafo(10, 0, 0, 10, 100, 300))
        self.document.Insert(self.r21, self.new_layer)
        self.document.Insert(self.r22, self.new_layer)
        self.document.Insert(self.r23, self.new_layer)
        self.document.Insert(self.r24, self.new_layer)

        self.editor.SelectObject([self.r12, self.r14, self.r22, self.r23])

    def check_before(self, after_undo):
        self.assertEquals(self.layer.GetObjects(),
                          [self.r11, self.r12, self.r13, self.r14])
        self.assertEquals(self.new_layer.GetObjects(),
                          [self.r21, self.r22, self.r23, self.r24])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r12, self.r14, self.r22, self.r23])

        if after_undo:
            # There must have been two OBJECT_CHANGED messages from the
            # layers
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r12, self.r14])),
                                                (self.new_layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r22, self.r23]))])

            # The area of the selected rectangles' bounding box must be
            # redrawn
            self.check_redraw_messages([self.r12.bounding_rect,
                                        self.r14.bounding_rect,
                                        self.r22.bounding_rect,
                                        self.r23.bounding_rect])

    def check_after(self, after_redo):
        # Check the order of the children of self.layer and self.new_layer
        self.assertEquals(self.layer.GetObjects(),
                          [self.r12, self.r14, self.r11, self.r13])
        self.assertEquals(self.new_layer.GetObjects(),
                          [self.r22, self.r23, self.r21, self.r24])
        # the selection should not have changed.
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r12, self.r14, self.r22, self.r23])

        # There must have been two OBJECT_CHANGED messages from the
        # layers
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r12, self.r14])),
                                            (self.new_layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r22, self.r23]))])

        # The area of the selected rectangles' bounding box must be
        # redrawn
        self.check_redraw_messages([self.r12.bounding_rect,
                                    self.r14.bounding_rect,
                                    self.r22.bounding_rect,
                                    self.r23.bounding_rect])


class TestMoveToBottomInGroup(MoveToBottomTest):

    """Test EditorWithSelection.MoveSelectedToBottom() objects in a self.group

    A self.group with two objects.  The upper one is selected and moved down.
    Check the order of the children afterwards.
    """

    def setUp(self):
        MoveToBottomTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.group = Group([self.r1, self.r2, self.r3])
        self.document.Insert(self.group, self.layer)

        self.editor.SelectObject(self.r3)

    def check_before(self, after_undo):
        self.assertEquals(self.group.GetObjects(), [self.r1, self.r2, self.r3])
        self.assertEquals(self.editor.SelectedObjects(), [self.r3])

        if after_undo:
            # There must have been one OBJECT_CHANGED message from the group
            self.check_object_changed_messages([(self.group, CHILDREN,
                                                 (REARRANGED, [self.r3]))])

            # The area of the selected rectangles' bounding box must be
            # redrawn
            self.check_redraw_messages([self.r3.bounding_rect])

    def check_after(self, after_redo):
        # The order of the children of the group should have changed
        # now.
        self.assertEquals(self.group.GetObjects(), [self.r3, self.r1, self.r2])

        # The selection should still be the same, though.
        self.assertEquals(self.editor.SelectedObjects(), [self.r3])

        # There must have been one OBJECT_CHANGED message from the group
        self.check_object_changed_messages([(self.group, CHILDREN,
                                             (REARRANGED, [self.r3]))])

        # The area of the selected rectangles' bounding box must be
        # redrawn
        self.check_redraw_messages([self.r3.bounding_rect])


class TestMoveToBottomMixed(MoveToBottomTest):

    """Test EditorWithSelection.MoveSelectedToBottom() in groups and layers

    A trickier case: A layer with 4 rectanges r1, ..., r4 and a group G
    with three rectangles [r21, r22, r23].  The children of the layer
    are [r1, r2, G, r3, r4].  The selection is [r2, r23, r4].  The
    selection is moved to the bottom.  Afterwards, The order in the
    layer must be [r2, r4, r1, G, r3] and in the group [r23, r21, r22].
    """

    def setUp(self):
        MoveToBottomTest.setUp(self)
        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))
        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))
        self.r3 = Rectangle(Trafo(10, 0, 0, 10, 0, 200))
        self.r4 = Rectangle(Trafo(10, 0, 0, 10, 0, 300))
        self.r21 = Rectangle(Trafo(10, 0, 0, 10, 100, 0))
        self.r22 = Rectangle(Trafo(10, 0, 0, 10, 100, 100))
        self.r23 = Rectangle(Trafo(10, 0, 0, 10, 100, 200))
        self.group = Group([self.r21, self.r22, self.r23])
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.group, self.layer)
        self.document.Insert(self.r3, self.layer)
        self.document.Insert(self.r4, self.layer)

        self.editor.SelectObject([self.r2, self.r23, self.r4])

    def check_before(self, after_undo):
        self.assertEquals(self.group.GetObjects(),
                          [self.r21, self.r22, self.r23])
        self.assertEquals(self.layer.GetObjects(),
                          [self.r1, self.r2, self.group, self.r3, self.r4])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r2, self.r23, self.r4])

        if after_undo:
            # There must have been two OBJECT_CHANGED messages, one each
            # from the layer and the group
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (REARRANGED,
                                                  [self.r2, self.r4])),
                                                (self.group, CHILDREN,
                                                 (REARRANGED, [self.r23]))])

            # The area of the selected rectangles' bounding box must be
            # redrawn
            self.check_redraw_messages([self.r2.bounding_rect,
                                        self.r4.bounding_rect,
                                        self.r23.bounding_rect])

    def check_after(self, after_redo):
        # The order of the children of the group should have changed
        # now.
        self.assertEquals(self.group.GetObjects(),
                          [self.r23, self.r21, self.r22])
        self.assertEquals(self.layer.GetObjects(),
                          [self.r2, self.r4, self.r1, self.group, self.r3])

        # The selection should still be the same, though.
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.r2, self.r4, self.r23])

        # There must have been two OBJECT_CHANGED messages, one each
        # from the layer and the group
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REARRANGED,
                                              [self.r2, self.r4])),
                                            (self.group, CHILDREN,
                                             (REARRANGED, [self.r23]))])

        # The area of the selected rectangles' bounding box must be
        # redrawn
        self.check_redraw_messages([self.r2.bounding_rect,
                                    self.r4.bounding_rect,
                                    self.r23.bounding_rect])


class MockNonCurve(Primitive):

    """A minimal graphics object that is not a curve and cannot be converted
    to a curve.

    Used in the tests for commands that affect curve objects: combine,
    convert to curves, etc.
    This object is meant to be ignored by these commands, but still kept in
    the selection. It's the simplest placeholder for objects that are not
    curves, like groups or images.
    """
    is_curve = 0
    is_Bezier = 0
    is_Primitive = 0
    coord_rect = bounding_rect = Rect(0, 0, 0, 0)


class CurveCommandTest(EditorTransactionTest):

    """Base class for the methods that affect curve objects: combine,
    convert to curves, etc.

    Used to make available a number of graphics objects ready to be
    inserted in the document.
    """

    def setUp(self):
        EditorTransactionTest.setUp(self)
        # create the following objects:
        #     - a simple PolyBeziers, poly1
        #     - a PolyBezier containing two paths, poly2
        #     - three rectangles r1, r2, r3
        #     - a non-curve object nonc1

        path = CreatePath()
        path.AppendLine(0, 0)
        path.AppendLine(10, 10)
        self.poly1 = PolyBezier(paths = (path,))

        path = CreatePath()
        path.AppendLine(40, 40)
        path.AppendLine(50, 50)
        path2 = CreatePath()
        path2.AppendLine(60, 60)
        path2.AppendLine(70, 70)
        self.poly2 = PolyBezier(paths = (path, path2))

        self.r1 = Rectangle(Trafo(10, 0, 0, 10, 0, 0))

        self.r2 = Rectangle(Trafo(10, 0, 0, 10, 0, 100))

        self.r3 = Rectangle(Trafo(100, 0, 0, 20, -100, 50))

        self.nonc1 = MockNonCurve()

    def assertPathsEqual(self, paths1, paths2):
        """Assert that two tuples of paths are equal

        Check that both parameters have the same lengths, and assuming
        that the items of both parameters are path objects check that
        they have the same coordinates.  The path objects need not be
        identical.
        """
        self.assertEquals(len(paths1), len(paths2))
        for path1, path2 in zip(paths1, paths2):
            self.assertEquals(path1.get_save(), path2.get_save())


class CombineBeziersTest(CurveCommandTest):

    """Base class for EditorWithSelection.CombineBeziers() tests.

    These tests don't check for OBJECT_CHANGED messages, as there will
    be only one message per layer.
    See the FIXME in TestDuplicateTwoObjects.check_after above.
    """

    def action(self):
        self.editor.CombineBeziers()


class TestCombineBeziersSimple(CombineBeziersTest):

    """Test EditorWithSelection.CombineBeziers() with two paths,
    both in the same layer.
    """

    def setUp(self):
        CombineBeziersTest.setUp(self)
        # insert the two paths in the document and select them.
        self.document.Insert(self.poly1, self.layer)
        self.document.Insert(self.poly2, self.layer)
        self.editor.SelectObject([self.poly1, self.poly2])

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.poly1, self.poly2])
        self.assertEquals(self.layer.GetObjects(), [self.poly1, self.poly2])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.poly1, self.poly2])
        self.failUnless(self.editor.CanCombineBeziers())

        if after_undo:
            # There must have been three redraw messages, one for each
            # path to be combined and one for the resulted object.
            self.check_redraw_messages([self.poly1.bounding_rect,
                                        self.poly2.bounding_rect,
                                        self.combined.bounding_rect])

    def check_after(self, after_redo):
        self.combined = self.layer.GetObjects()[0]

        # The paths tuple of the new object should be the concatenation
        # of the paths tuples of the selected objects
        self.assertPathsEqual(self.combined.Paths(),
                              self.poly1.Paths() + self.poly2.Paths())

        # The new object should be selected
        self.assertEquals(self.editor.SelectedObjects(), [self.combined])

        # There must have been three redraw messages, one for each
        # path to be combined and one for the resulted object.
        self.check_redraw_messages([self.poly1.bounding_rect,
                                    self.poly2.bounding_rect,
                                    self.combined.bounding_rect])


class TestCombineBeziersComplex(CombineBeziersTest):

    """A more complex test for EditorWithSelection.CombineBeziers()

    A curve (poly1), a rectangle (r3) and an object that is not a curve
    (nonc1) from different layers will be selected and combined.  In one of
    the layers there are also two rectangles, used to check the stacking order
    after inserting the combined object.
    The non-curve object should be unaffected by the command and still be
    selected afterwards.

    The initial arrangement, with selected objects marked with [] is:
        layer1
            r1
            [poly1]
            r2
        layer2 (active)
            [r3]
            [nonc1]

    Afterwards, the arrangement should be:
        layer1
            r1
            [combined]
            r2
        layer2 (active):
            [nonc1]
    """

    def setUp(self):
        CombineBeziersTest.setUp(self)

        self.layer1 = self.document.AppendLayer("layer1")
        self.document.Insert(self.r1, self.layer1)
        self.document.Insert(self.poly1, self.layer1)
        self.document.Insert(self.r2, self.layer1)

        self.layer2 = self.document.AppendLayer("layer2")
        self.document.Insert(self.r3, self.layer2)

        self.nonc1 = MockNonCurve()
        self.document.Insert(self.nonc1, self.layer2)
        self.editor.SelectObject([self.poly1, self.r3, self.nonc1])
        self.document.SetActiveLayer(self.document.LayerIndex(self.layer2))

    def check_before(self, after_undo):
        self.assertEquals(self.document.ActiveLayer(), self.layer2)
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.poly1, self.r3, self.nonc1])
        self.assertEquals(self.layer1.GetObjects(), [self.r1, self.poly1,
                                                     self.r2])
        self.assertEquals(self.layer2.GetObjects(), [self.r3, self.nonc1])
        self.assertEquals(self.editor.SelectedObjects(),
                          [self.poly1, self.r3, self.nonc1])
        self.failUnless(self.editor.CanCombineBeziers())

        if after_undo:
            # There must have been three redraw messages, one for each
            # object to be combined and one for the combined object.
            self.check_redraw_messages([self.poly1.bounding_rect,
                                        self.r3.bounding_rect,
                                        self.combined.bounding_rect])

    def check_after(self, after_redo):
        self.combined = self.layer1.GetObjects()[1]
        self.failUnless(self.combined.is_Bezier)

        # The paths tuple of the new object should be the concatenation
        # of the paths tuples of the selected objects
        self.assertPathsEqual(self.combined.Paths(),
                              self.poly1.Paths() + self.r3.Paths())


        # The new object and the non-curve should be selected/
        self.assertEquals(self.editor.SelectedObjects(), [self.combined,
                                                          self.nonc1])

        # The new object should have been inserted between the two rectangles
        self.assertEquals(self.layer1.GetObjects(), [self.r1,
                                                     self.combined, self.r2])
        self.assertEquals(self.layer2.GetObjects(), [self.nonc1])
        self.assertEquals(self.document.ActiveLayer(), self.layer2)

        # There must have been three redraw messages, one for each
        # object to be combined and one for the combined object.
        self.check_redraw_messages([self.poly1.bounding_rect,
                                    self.r3.bounding_rect,
                                    self.combined.bounding_rect])


class SplitBeziersTest(CurveCommandTest):

    """Base class for EditorWithSelection.SplitBeziers() tests.

    These tests don't check for OBJECT_CHANGED messages, as there will
    be only one message per layer.
    See the FIXME in TestDuplicateTwoObjects.check_after above.
    """

    def action(self):
        self.editor.SplitBeziers()


class TestSplitBezierSimple(SplitBeziersTest):

    """Test EditorWithSelection.SplitBeziers() with a single curve."""

    def setUp(self):
        SplitBeziersTest.setUp(self)

        self.document.Insert(self.poly2, self.layer)
        self.editor.SelectObject([self.poly2])

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.poly2])
        self.assertEquals(self.layer.GetObjects(), [self.poly2])
        self.failUnless(self.editor.CanSplitBeziers())

        if after_undo:
            # There must have been three redraw messages, one for the
            # initial PolyBezier and two from the resulted paths.
            self.check_redraw_messages([self.split1.bounding_rect,
                                        self.split2.bounding_rect,
                                        self.poly2.bounding_rect
                                        ])

    def check_after(self, after_redo):
        self.split1, self.split2 = self.layer.GetObjects()
        # check if the new objects are selected
        self.assertEquals(self.editor.SelectedObjects(), [self.split1,
                                                          self.split2])

        # check whether the paths of the split polybeziers have the same
        # coordinates as the ones from the original polybezier.
        self.assertPathsEqual(self.poly2.Paths(),
                              self.split1.Paths() + self.split2.Paths())

        # There must have been three redraw messages, one for the
        # initial PolyBezier and two from the resulted paths.
        self.check_redraw_messages([self.split1.bounding_rect,
                                    self.split2.bounding_rect,
                                    self.poly2.bounding_rect
                                    ])

class TestSplitBezierComplex(SplitBeziersTest):

    """A more complex test for EditorWithSelection.SplitBeziers()

    There are three objects selected: a PolyBezier with a single path
    (poly1), a PolyBezier with two paths (poly2) and a non-curve object.
    """

    def setUp(self):
        SplitBeziersTest.setUp(self)

        self.document.Insert(self.poly1, self.layer)
        self.document.Insert(self.poly2, self.layer)
        self.document.Insert(self.nonc1, self.layer)
        self.editor.SelectObject([self.poly1, self.poly2, self.nonc1])

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.poly1,
                                                          self.poly2,
                                                          self.nonc1])
        self.assertEquals(self.layer.GetObjects(), [self.poly1,
                                                    self.poly2,
                                                    self.nonc1])
        self.failUnless(self.editor.CanSplitBeziers())

        if after_undo:
            # There must have been three redraw messages, one for the
            # PolyBezier to be sptitted and two from the resulted paths.
            self.check_redraw_messages([self.split1.bounding_rect,
                                        self.split2.bounding_rect,
                                        self.poly2.bounding_rect
                                        ])

    def check_after(self, after_redo):
        self.split1 = self.layer.GetObjects()[1]
        self.split2 = self.layer.GetObjects()[2]

        # check whether the paths of the split polybeziers have the same
        # coordinates as the ones from the original polybezier.
        self.assertPathsEqual(self.poly2.Paths(),
                              self.split1.Paths() + self.split2.Paths())

        # check the stacking order; the new objects should have been inserted
        # between poly1 and nonc1
        self.assertEquals(self.layer.GetObjects(), [self.poly1,
                                                    self.split1,
                                                    self.split2,
                                                    self.nonc1])
        # check if the new objects are now selected and the old one were
        # preserved in the selection:
        self.assertEquals(self.editor.SelectedObjects(), [self.poly1,
                                                          self.split1,
                                                          self.split2,
                                                          self.nonc1])

        # There must have been three redraw messages, one for the
        # PolyBezier to be sptitted and two from the resulted paths.
        self.check_redraw_messages([self.split1.bounding_rect,
                                    self.split2.bounding_rect,
                                    self.poly2.bounding_rect
                                    ])


class ConvertToCurveTest(CurveCommandTest):

    """Base class for EditorWithSelection.ConvertToCurve() tests.
    """

    def action(self):
        self.editor.ConvertToCurve()


class TestConvertToCurveSimple(ConvertToCurveTest):

    """Test EditorWithSelection.ConvertToCurve() with one object.
    """
    def setUp(self):
        ConvertToCurveTest.setUp(self)
        self.document.Insert(self.r1, self.layer)
        self.editor.SelectObject([self.r1])

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.r1])
        self.assertEquals(self.layer.GetObjects(), [self.r1])
        self.failUnless(self.editor.CanConvertToCurve())

        if after_undo:
            # Two REDRAW messages are sent, on for the rectangle, one
            # for the curve
            self.check_redraw_messages([self.r1.bounding_rect,
                                        self.converted.bounding_rect])

    def check_after(self, after_redo):
        self.converted = self.layer.GetObjects()[0]
        self.assertEquals(self.layer.GetObjects(), [self.converted])
        # Check if the converted object is selected
        self.assertEquals(self.editor.SelectedObjects(), [self.converted])

        # Two REDRAW messages are sent, on for the rectangle, one
        # for the curve
        self.check_redraw_messages([self.r1.bounding_rect,
                                    self.converted.bounding_rect])


class TestConvertToCurveMultiple(ConvertToCurveTest):

    """Test EditorWithSelection.ConvertToCurve() with more objects.

    All the objects that are not convertible to curves are simply kept in the
    selection.
    """
    def setUp(self):
        ConvertToCurveTest.setUp(self)

        self.document.Insert(self.poly1, self.layer)
        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.nonc1, self.layer)

        self.editor.SelectObject([self.poly1, self.r1, self.r2, self.nonc1])

    def check_before(self, after_undo):
        self.assertEquals(self.editor.SelectedObjects(), [self.poly1,
                                                          self.r1,
                                                          self.r2,
                                                          self.nonc1])
        self.assertEquals(self.layer.GetObjects(), [self.poly1,
                                                    self.r1,
                                                    self.r2,
                                                    self.nonc1])
        self.failUnless(self.editor.CanConvertToCurve())

        if after_undo:
            # Four REDRAW messages are sent, one for each rectangle and one
            # for the respective curves.
            self.check_redraw_messages([self.r1.bounding_rect,
                                        self.r2.bounding_rect,
                                        self.conv1.bounding_rect,
                                        self.conv2.bounding_rect])

    def check_after(self, after_redo):
        self.conv1 = self.layer.GetObjects()[1]
        self.conv2 = self.layer.GetObjects()[2]

        # check the coordinates of the paths of the new bezier objects
        self.assertPathsEqual(self.conv1.Paths(), self.r1.Paths())
        self.assertPathsEqual(self.conv2.Paths(), self.r2.Paths())

        self.assertEquals(self.layer.GetObjects(), [self.poly1,
                                                    self.conv1,
                                                    self.conv2,
                                                    self.nonc1])
        # Check if the converted object is selected
        self.assertEquals(self.editor.SelectedObjects(), [self.poly1,
                                                          self.conv1,
                                                          self.conv2,
                                                          self.nonc1])
        # Four REDRAW messages are sent, one for each rectangle and one
        # for the respective curves.
        self.check_redraw_messages([self.r1.bounding_rect,
                                    self.r2.bounding_rect,
                                    self.conv1.bounding_rect,
                                    self.conv2.bounding_rect])


def complex_polybezier():
    """Returns a polybezier composed of two paths with various types of
    segments. The first path is open and the second is closed.

    This polybezier is used in some tests in test_doceditor.py and in the
    tests from test_edittool.py
    """
    path1 = CreatePath()
    path1.AppendLine(0, 0)
    path1.AppendLine(40, 0)
    path1.AppendLine(40, 60)
    path1.AppendBezier(Point(40, 70), Point(40, 80), Point(20, 80),
                       ContSymmetrical)
    path1.AppendBezier(Point(0, 80), Point(0, 70), Point(0, 60),
                       ContSmooth)
    path1.AppendBezier(Point(0, 40), Point(10, 40), Point(20, 40),
                       ContAngle)
    path1.AppendBezier(Point(10, 30), Point(0, 20), Point(20, 20))
    path1.AppendLine(Point(20, 10))

    path2 = CreatePath()
    path2.AppendLine(100, 0)
    path2.AppendBezier(Point(120, 0), Point(120, 10), Point(120, 20))
    path2.AppendBezier(Point(120, 30), Point(80, 30), Point(80, 20))
    path2.AppendBezier(Point(80, 10), Point(80, 0), Point(100, 0))
    path2.ClosePath()
    return PolyBezier(paths = (path1, path2))


class PolyBezierOpsTest(EditorTransactionTest):

    """Base class for the methods that operate on curve objects
    when EditTool is active: insert/delete nodes, etc.

    These tests check that the command is indeed available before
    performing the actual operation.
    """

    # The command that is tested
    command = ''

    def setUp(self):
        EditorTransactionTest.setUp(self)

        self.cmd = Sketch.Editor.Registry.Command(self.command)

        self.poly = complex_polybezier()
        self.path1, self.path2 = self.poly.Paths()
        self.path1_nodes = self.path1.NodeList()
        self.path2_nodes = self.path2.NodeList()

        self.document.Insert(self.poly, self.layer)
        self.editor.SelectObject(self.poly)

        # The functions that check if the menu item should be
        # sensitive or not need an AppContext instance as an argument
        # Another place where a context is needed is the cmd.Execute call
        # from the action.
        #
        # OTOH, AppContext needs a SketchApplication instance as argument, so
        # all this can became too complex for the purpose of a simple test.
        #
        # So, instead we use a custom class that provides only the minimum
        # functionality required.
        class Context:
            editor = self.editor
            document = self.document
            def SetTool(self, tool):
                self.tool = tool
                self.editor.SetTool(tool)

            def HasEditor(self):
                return True

        self.context = Context()
        self.context.SetTool('EditTool')
        self.canvas = MockCanvas()

    def select_nodes(self, *nodes):
        """Helper for selecting node handles.

        Simulates shift-clicks on the specified nodes.
        """
        handles = self.editor.tool.Handles()
        for n in nodes:
            self.editor.ButtonClick(self.canvas, handles[n].p, 1, ShiftMask,
                                    handle = handles[n])

    def action(self):
        self.cmd.Execute(self.context)

    def sensitive(self):
        return self.cmd.Sensitive(self.context)


class TestOpenNodesSingle(PolyBezierOpsTest):

    """Test for PolyBezierEditor.OpenNodes with a single node selected
    """
    command = 'open_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Select the fourth node
        self.select_nodes(3)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        paths = self.poly.Paths()
        self.assertEquals(len(paths), 2)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        paths = self.poly.Paths()
        # We should have three paths now
        self.assertEquals(len(paths), 3)
        self.assertEquals(paths[0].NodeList(), self.path1_nodes[:4])
        self.assertEquals(paths[1].NodeList(), self.path1_nodes[3:])
        # path2 should be unchanged
        self.assertEquals(paths[2].get_save(), self.path2.get_save())
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestOpenNodesMultiple(PolyBezierOpsTest):

    """Test for PolyBezierEditor.OpenNodes with two nodes selected
    """
    command = 'open_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Select two nodes
        self.select_nodes(2, 5)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        paths = self.poly.Paths()
        self.assertEquals(len(paths), 2)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        paths = self.poly.Paths()
        # We should have four paths
        self.assertEquals(len(paths), 4)
        self.assertEquals(paths[0].NodeList(), self.path1_nodes[:3])
        self.assertEquals(paths[1].NodeList(), self.path1_nodes[2:6])
        self.assertEquals(paths[2].NodeList(), self.path1_nodes[5:])
        # path2 should be unchanged
        self.assertEquals(paths[3].get_save(), self.path2.get_save())
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestOpenNodesWithCurvePoint(PolyBezierOpsTest):

    """Test for PolyBezierEditor.OpenNodes with a point selected on a curve
    """
    command = 'open_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Select a point on curve between the second and the third node
        self.point = Point(40, 40)
        self.editor.ButtonClick(self.canvas, self.point, 1, 0)

    def check_before(self, after_undo):
        paths = self.poly.Paths()
        self.assertEquals(len(paths), 2)
        if after_undo:
            # XXX: The selection point is not restored after undo,
            # maybe it should be.
            self.failIf(self.sensitive())
            self.check_object_changed_messages([(self.poly, SHAPE, ())])
        else:
            self.failUnless(self.sensitive())

    def check_after(self, after_redo):
        paths = self.poly.Paths()
        point = self.point
        # We should have three paths now
        self.assertEquals(len(paths), 3)
        self.assertEquals(paths[0].NodeList(), self.path1_nodes[:2] + [point])
        self.assertEquals(paths[1].NodeList(), [point] + self.path1_nodes[2:])
        # path2 should be unchanged
        self.assertEquals(paths[2].get_save(), self.path2.get_save())
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestCloseNodes(PolyBezierOpsTest):

    """Test for PolyBezierEditor.CloseNodes
    """
    command = 'close_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # select two open nodes
        self.select_nodes(0, 7)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        # The first path is open
        self.failIf(self.poly.Paths()[0].closed)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        paths = self.poly.Paths()
        self.failUnless(paths[0].closed)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestSetContinuityAngle(PolyBezierOpsTest):

    """Test for PolyBezierEditor.SetContinuity with ContAngle
    """
    command = 'cont_angle'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Select a symmetrical node
        self.select_nodes(3)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        path = self.poly.Paths()[0]
        self.assertEquals(path.Continuity(3), ContSymmetrical)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        path = self.poly.Paths()[0]
        self.assertEquals(path.Continuity(3), ContAngle)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestSegmentsToLinesWithNodes(PolyBezierOpsTest):

    """Test for PolyBezierEditor.SegmentsToLines with two nodes selected
    """
    command = 'segments_to_lines'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        self.select_nodes(4, 5)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(5), Bezier)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(5), Line)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestSegmentsToLinesWithCurvePoint(PolyBezierOpsTest):

    """Test for PolyBezierEditor.SegmentsToLines with a point on curve
    """
    command = 'segments_to_lines'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Click on the bezier segment between the third and the foutrh node
        self.editor.ButtonClick(self.canvas, Point(34.5, 76.5), 1, 0)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(3), Bezier)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(3), Line)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestSegmentsToCurveWithNodes(PolyBezierOpsTest):

    """Test for PolyBezierEditor.SegmentsToCurve with two nodes selected
    """
    command = 'segments_to_curve'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        self.select_nodes(1, 2)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(2), Line)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(2), Bezier)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])

class TestSegmentsToCurveWithCurvePoint(PolyBezierOpsTest):

    """Test for PolyBezierEditor.SegmentsToCurve with a point on curve
    """
    command = 'segments_to_curve'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Click on the line segment between the second and the third node
        self.editor.ButtonClick(self.canvas, Point(40, 40), 1, 0)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(2), Line)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        path = self.poly.Paths()[0]
        self.assertEquals(path.SegmentType(2), Bezier)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestDeleteNodesOne(PolyBezierOpsTest):

    """Test for PolyBezierEditor.DeleteNodes with a single node selected.
    """
    command = 'delete_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # select the second node of the first path
        self.select_nodes(1)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        self.assertEquals(self.poly.Paths()[0].NodeList(),
                          [Point(0, 0), Point(40, 0), Point(40, 60),
                           Point(20, 80), Point(0, 60), Point(20, 40),
                           Point(20, 20), Point(20, 10)])
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        # The node should have been deleted
        self.assertEquals(self.poly.Paths()[0].NodeList(),
                          [Point(0, 0), Point(40, 60),
                           Point(20, 80), Point(0, 60), Point(20, 40),
                           Point(20, 20), Point(20, 10)])


class TestDeleteNodesOneClosed(PolyBezierOpsTest):

    """Test for PolyBezierEditor.DeleteNodes with a single node selected on a
    closed path
    """
    command = 'delete_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Select a node of the second path
        # Note that select_nodes uses the handle list as returned by the
        # tool, which in turn calls PolyBezierEditor.GetHandles.
        # If the path is closed, the first handle returned by the GetHandles
        # corresponds to the second node added when creating the curve.
        # This means the 8th handle corresponds to Point(120, 20) and not
        # Point(100, 0) which is returned last.
        self.select_nodes(8)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        self.assertEquals(self.poly.Paths()[1].NodeList(),
                          [Point(100, 0), Point(120, 20), Point(80, 20)])
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        # The node should have been deleted
        self.assertEquals(self.poly.Paths()[1].NodeList(),
                          [Point(100, 0), Point(80, 20)])
        # the path sould be still closed
        self.failUnless(self.poly.Paths()[1].closed)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestDeleteNodesMultiple(PolyBezierOpsTest):

    """Test for PolyBezierEditor.DeleteNodes with several nodes selected.
    """
    command = 'delete_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        self.select_nodes(0, 2, 3)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        self.assertEquals(self.poly.Paths()[0].NodeList(), self.path1_nodes)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        self.assertEquals(self.poly.Paths()[0].NodeList(),
                          [Point(40, 0), Point(0, 60), Point(20, 40),
                           Point(20, 20), Point(20, 10)])
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestDeleteNodesAllFromPath(PolyBezierOpsTest):

    """Test for PolyBezierEditor.DeleteNodes with all the nodes of a path
    selected.
    """
    command = 'delete_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        self.select_nodes(8, 9, 10)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        paths = self.poly.Paths()
        self.assertEquals(len(paths), 2)
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        paths = self.poly.Paths()
        self.assertEquals(len(paths), 1)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestDeleteNodesAll(PolyBezierOpsTest):

    """Test for PolyBezierEditor.DeleteNodes with all nodes selected.

    This should result in the complete removal of the polybezier.
    """
    command = 'delete_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        self.select_nodes(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        self.assertEquals(self.layer.GetObjects(), [self.poly])
        self.assertEquals(self.editor.SelectedObjects(), [self.poly])
        if after_undo:
            # redraw the polybezier area
            self.check_redraw_messages([self.poly.bounding_rect])
            # The polybezier is inserted back
            self.check_object_changed_messages([(self.layer, CHILDREN,
                                                 (INSERTED, (self.poly,)))])

    def check_after(self, after_redo):
        # The layer does'n have any children now
        self.assertEquals(self.layer.GetObjects(), [])
        # The selection should be empty
        self.assertEquals(self.editor.SelectedObjects(), [])
        # redraw the polybezier area
        self.check_redraw_messages([self.poly.bounding_rect])
        self.check_object_changed_messages([(self.layer, CHILDREN,
                                             (REMOVED, (self.poly,)))])


class TestInsertNodes(PolyBezierOpsTest):

    """Test for PolyBezierEditor.InsertNodes with two nodes selected
    """
    command = 'insert_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        self.select_nodes(1, 2)
        # the new node should be created at:
        self.new_node = (self.path1_nodes[1] + self.path1_nodes[2]) / 2

    def check_before(self, after_undo):
        self.failUnless(self.sensitive())
        if after_undo:
            self.check_object_changed_messages([(self.poly, SHAPE, ())])

    def check_after(self, after_redo):
        paths = self.poly.Paths()
        new_nodes = self.path1_nodes[:]
        new_nodes.insert(2, self.new_node)
        self.assertEquals(self.poly.Paths()[0].NodeList(), new_nodes)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


class TestInsertNodesWithCurvePoint(PolyBezierOpsTest):
    """Test for PolyBezierEditor.InsertNodes with point on curve
    between the first two nodes
    """
    command = 'insert_nodes'

    def setUp(self):
        PolyBezierOpsTest.setUp(self)
        # Click at 10, 0 on the first segment
        self.new_node = Point(10, 0)
        self.editor.ButtonClick(self.canvas, self.new_node, 1, 0)

    def check_before(self, after_undo):
        if after_undo:
            # XXX: The selection point is not restored after undo,
            # maybe it should be.
            self.failIf(self.sensitive())
            self.check_object_changed_messages([(self.poly, SHAPE, ())])
        else:
            self.failUnless(self.sensitive())

    def check_after(self, after_redo):
        new_nodes = self.path1_nodes[:]
        new_nodes.insert(1, self.new_node)
        self.assertEquals(self.poly.Paths()[0].NodeList(), new_nodes)
        self.check_object_changed_messages([(self.poly, SHAPE, ())])


if __name__ == "__main__":
    unittest.main()
