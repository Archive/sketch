# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Color objects
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import math, operator
import unittest

import support

support.add_sketch_module_dir_to_path()


import _sketch
from _sketch import RGBColor



class ColorTest(support.FPTestCase):

    """Test cases for the color objects"""

    def test_constructors(self):
        """Test color object constructors"""
        eq = self.assert_float_equal

        #
        # Color(...)
        #
        color = RGBColor(0.1, 0.0, 1.0)
        eq(color.red, 0.1)
        eq(color.green, 0.0)
        eq(color.blue, 1.0)

    def test_constructor_exception(self):
        """Test color object constructor exceptions"""
        self.assertRaises(ValueError, RGBColor, -0.1, 0.0, 1.0)
        self.assertRaises(ValueError, RGBColor, 100, 0.0, 1.0)
        self.assertRaises(ValueError, RGBColor, 0.1, -10, 1.0)
        self.assertRaises(ValueError, RGBColor, 0.1, 20, 1.0)
        self.assertRaises(ValueError, RGBColor, 0.1, 0.0, -1.0)
        self.assertRaises(ValueError, RGBColor, 0.1, 0.0, 1233456)

    def test_sequence(self):
        """Test color object sequence protocol"""
        # color objects can be used as sequenc objects
        eq = self.assert_float_equal

        color = RGBColor(0.4, 0.5, 0.9)

        # a color object always has length 3
        self.assertEquals(len(color), 3)

        # a color object can be unpacked into R, G and B
        color = RGBColor(0.4, 0.5, 0.9)
        r, g, b = color
        eq(r, 0.4)
        eq(g, 0.5)
        eq(b, 0.9)
        
    def test_methods(self):
        """Test color object methods"""
        eq = self.assert_float_equal

        color1 = RGBColor(0.1, 0.9, 1.0)
        color2 = RGBColor(0.0, 0.5, 0.2)
        c = color1.Blend(color2, 0.2, 0.8)
        eq(c.red, 0.02)
        eq(c.green, 0.58)
        eq(c.blue, 0.36)

    def test_hash(self):
        """Test color object used as dict key"""
        # Create two equal but not identical color objects
        color1 = RGBColor(0.1, 0.9, 1.0)
        color2 = RGBColor(0.1, 0.9, 1.0)

        # A dictionary with a color as key
        d = {color1: 1}

        # Assert that we can retrieve the value with both color objects.
        # Also test that the objects are indeed not identical
        self.assert_(color1 is not color2)
        self.assertEquals(d.get(color1), 1)
        self.assertEquals(d.get(color2), 1)


if __name__ == "__main__":
    unittest.main()
