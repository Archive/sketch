# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Test Type1 font parser"""

__version__ = "$Revision$"
# $Source$
# $Id$

import os
import unittest

import support
support.add_sketch_dir_to_path()


import Sketch.Lib.type1

class TestType1(unittest.TestCase):

    def test(self):
        dir = os.path.dirname(support.__file__)
        filename = os.path.abspath(os.path.join(dir, "data",
                                                "URWChanceryL-MediItal.pfb"))
        char_strings, interp = Sketch.Lib.type1.read_outlines(filename)

        # Test the 'I' because it contains flexes
        interp.execute(char_strings["I"])
        self.assertEquals(interp.paths,
                          ((1, [(81.0, 0.0),
                                (146.0, 1.0, 146.0, 1.0, 160.0, 1.0),
                                (160.0, 1.0, 316.0, 0.0, 316.0, 0.0),
                                (352.0, 25.0),
                                (259.0, 32.0),
                                (278.0, 120.0, 341.0, 370.0, 388.0, 542.0),
                                (446.0, 548.0),
                                (481.0, 573.0),
                                (447.0, 573.0),
                                (345.0, 572.0, 345.0, 572.0, 328.0, 572.0),
                                (312.0, 572.0, 308.0, 572.0, 251.0, 573.0),
                                (216.0, 548.0),
                                (306.0, 542.0),
                                (298.0, 505.0, 292.0, 480.0, 291.0, 475.0),
                                (270.0, 393.0),
                                (207.0, 146.0, 196.0, 102.0, 191.0, 85.0),
                                (189.0, 79.0, 183.0, 56.0, 176.0, 32.0),
                                (117.0, 25.0),
                                (81.0, 0.0)]),))

if __name__ == "__main__":
    unittest.main()
