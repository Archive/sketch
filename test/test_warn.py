# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test Skencil warning infrastructure
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import sys
import StringIO
import unittest

import support
support.add_sketch_dir_to_path()

import Sketch
from Sketch.Base.warn import warn, warn_tb, INTERNAL, USER


class TestWarn(unittest.TestCase):

    def setUp(self):
        """Install a warning handler for the tests
        """
        Sketch.Subscribe(Sketch.Base.const.PRINT_WARNING,
                         self.warn_receiver, "user_warning")
        Sketch.Subscribe(Sketch.Base.const.PRINT_TRACEBACK,
                         self.warn_receiver, "user_traceback")
        self.warnings = []
        self.old_stderr = sys.stderr
        self.stderr = StringIO.StringIO()
        sys.stderr = self.stderr

    def tearDown(self):
        sys.stderr = self.old_stderr
        Sketch.Unsubscribe(Sketch.Base.const.PRINT_TRACEBACK,
                           self.warn_receiver, "user_traceback")
        Sketch.Unsubscribe(Sketch.Base.const.PRINT_WARNING,
                           self.warn_receiver, "user_warning")

    def warn_receiver(self, *args):
        self.warnings.append(args)

    def test_warn_user(self):
        """Test warn(USER) with a simple message

        Make sure that there's a percent sign in the message to test
        that when only the message string is given no %-formatting takes
        place.
        """
        warn(USER, "test % warning")
        self.assertEquals(self.stderr.getvalue(), "test % warning\n")
        self.assertEquals(self.warnings, [("test % warning", "user_warning")])

    def test_warn_user_format_positional(self):
        """Test warn(USER) with a format string and positional arguments"""
        warn(USER, "Warning: Importing %s failed", "mydrawing.sk")
        self.assertEquals(self.stderr.getvalue(),
                          "Warning: Importing mydrawing.sk failed\n")
        self.assertEquals(self.warnings,
                          [("Warning: Importing mydrawing.sk failed",
                            "user_warning")])

    def test_warn_user_format_kw(self):
        """Test warn(USER) with a format string and positional arguments"""
        warn(USER, "Warning: Importing %(filename)s failed",
             filename="mydrawing.sk")
        self.assertEquals(self.stderr.getvalue(),
                          "Warning: Importing mydrawing.sk failed\n")
        self.assertEquals(self.warnings,
                          [("Warning: Importing mydrawing.sk failed",
                            "user_warning")])

    def test_warn_internal(self):
        """Test warn(INTERNAL) with a simple message

        Make sure that there's a percent sign in the message to test
        that when only the message string is given no %-formatting takes
        place.
        """
        warn(INTERNAL, "test % warning")
        self.assertEquals(self.stderr.getvalue(), "test % warning\n")

        # No PRINT_WARNING messages are sent for internal warnings
        self.assertEquals(self.warnings, [])

    def test_warn_internal_format_positional(self):
        """Test warn(INTERNAL) with a format string and positional arguments"""
        warn(INTERNAL, "Warning: Importing %s failed", "mydrawing.sk")
        self.assertEquals(self.stderr.getvalue(),
                          "Warning: Importing mydrawing.sk failed\n")

        # No PRINT_WARNING messages are sent for internal warnings
        self.assertEquals(self.warnings, [])

    def test_warn_internal_format_kw(self):
        """Test warn(INTERNAL) with a format string and positional arguments"""
        warn(INTERNAL, "Warning: Importing %(filename)s failed",
             filename="mydrawing.sk")
        self.assertEquals(self.stderr.getvalue(),
                          "Warning: Importing mydrawing.sk failed\n")

        # No PRINT_WARNING messages are sent for internal warnings
        self.assertEquals(self.warnings, [])

    def test_warn_tb_user_no_message(self):
        """Test warn_tb(USER) without a message"""
        try:
            raise ValueError
        except:
            warn_tb(USER)
        msg = self.stderr.getvalue()
        self.assert_(msg.startswith("Traceback (most recent call last):\n"))
        self.assert_(msg.endswith("raise ValueError\nValueError\n"))

        # check the messages received
        self.assertEquals(len(self.warnings), 1)
        msg, tb, kind = self.warnings[0]
        self.assertEquals(kind, "user_traceback")
        self.assertEquals(msg, "")
        self.assert_(tb.startswith("Traceback (most recent call last):\n"))
        self.assert_(tb.endswith("raise ValueError\nValueError\n"))

    def test_warn_tb_user_simple(self):
        """Test warn_tb(USER) with a simple message"""
        try:
            raise ValueError
        except:
            warn_tb(USER, "An error occurred:")
        msg = self.stderr.getvalue()
        self.assert_(msg.startswith("An error occurred:\n"
                                    "Traceback (most recent call last):\n"))
        self.assert_(msg.endswith("raise ValueError\nValueError\n"))

        # check the messages received
        self.assertEquals(len(self.warnings), 1)
        msg, tb, kind = self.warnings[0]
        self.assertEquals(kind, "user_traceback")
        self.assertEquals(msg, "An error occurred:")
        self.assert_(tb.startswith("Traceback (most recent call last):\n"))
        self.assert_(tb.endswith("raise ValueError\nValueError\n"))

    def test_warn_fallback(self):
        """test warn fallback for invalid format strings

        When the format string is invalid and raises TypeErrors, warn
        prints the information anyway, so it doesn't get replaced by an
        exception in warn.
        """
        warn(USER, "foo %d", "bar")
        self.assertEquals(self.stderr.getvalue(), "foo %d bar\n")
        self.assertEquals(self.warnings, [("foo %d bar", "user_warning")])



if __name__ == "__main__":
    unittest.main()
