# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the sksaver plugin.
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest
import support
support.add_sketch_dir_to_path()
support.init_plugins()

from Sketch.Graphics import Document, Rectangle
from Sketch.Plugin import plugins


class SKExportTest(unittest.TestCase, support.VolatileFileMixin):

    def make_filename(self):
        return self.volatile_file_name(self.id())

    def setUp(self):
        self.doc = Document(1)
        for layer in self.doc.Layers():
            if not layer.is_SpecialLayer:
                self.layer = layer

    def do_test(self):
        saver = plugins.find_export_plugin('SK-2')
        filename = self.make_filename()
        saver(self.doc, filename)
        f = file(filename, "rb")
        data = f.read()
        f.close()
        self.assertEquals(data, self.file_contents)


class TestFillOpacity(SKExportTest):

    file_contents = """\
##Sketch 2 1
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
fo(0.5)
lw(1)
r((1,0,0,1,0,0))
layer_()
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
guidelayer_()
grid((0,0,20,20),0,(0,0,1),'Grid')
document_()
"""

    def test(self):
        rect = Rectangle()
        rect.Properties().SetProperty(fill_opacity = 0.5)
        self.doc.Insert(rect, self.layer)
        self.do_test()


if __name__ == "__main__":
    unittest.main()
