# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Trafo objects
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import math, operator
import unittest

import support

support.add_sketch_module_dir_to_path()

import _sketch
from _sketch import Trafo, Scale, Translation, Rotation, \
     SingularMatrix, Point, PointsToRect, Rect, PointType, TrafoType



class TrafoTest(support.FPTestCase):

    """Test cases for Trafo objects"""

    def test_constructors(self):
        """Test Trafo constructors"""
        eq = self.assertEquals
        fse = self.assert_float_sequences_equal

        #
        # Trafo(...)
        #

        # Trafo(m11, m21, m12, m22, v1, v2)
        coefficients = (1.0, 2.0, 3.0, 4.0, 5.0, 6.0)
        trafo = apply(Trafo, coefficients)
        eq(trafo.m11, coefficients[0])
        eq(trafo.m21, coefficients[1])
        eq(trafo.m12, coefficients[2])
        eq(trafo.m22, coefficients[3])
        eq(trafo.v1, coefficients[4])
        eq(trafo.v2, coefficients[5])

        # Trafo(m11, m21, m12, m22)
        # v1 and v2 default to 0.0
        coefficients = (1.0, 2.0, 3.0, 4.0)
        trafo = apply(Trafo, coefficients)
        eq(trafo.m11, coefficients[0])
        eq(trafo.m21, coefficients[1])
        eq(trafo.m12, coefficients[2])
        eq(trafo.m22, coefficients[3])
        eq(trafo.v1, 0.0)
        eq(trafo.v2, 0.0)

        # Trafo(). The default is an identity transformation
        trafo = Trafo()
        eq(trafo.m11, 1.0)
        eq(trafo.m21, 0.0)
        eq(trafo.m12, 0.0)
        eq(trafo.m22, 1.0)
        eq(trafo.v1, 0.0)
        eq(trafo.v2, 0.0)

        #
        # Scale(f), Scale(x, y)
        #

        # Scale(f)
        f = 1.5
        trafo = Scale(f)
        fse(trafo.coeff(), (1.5, 0.0, 0.0, 1.5, 0.0, 0.0))

        # Scale(fx, fy)
        fx = 1.5; fy = -0.5
        trafo = Scale(fx, fy)
        fse(trafo.coeff(), (fx, 0.0, 0.0, fy, 0.0, 0.0))

        #
        # Rotation
        #

        # Rotation(angle)
        angle = math.pi / 3
        trafo = Rotation(angle)
        fse(trafo.coeff(), (math.cos(angle), math.sin(angle),
                            -math.sin(angle), math.cos(angle),
                            0.0, 0.0))

        # Rotation(angle, center)
        center = (2.5, -0.25)
        trafo = Rotation(angle, center)
        s = math.sin(angle); c = math.cos(angle)
        fse(trafo.coeff(), (c, s, -s, c,
                            center[0] - c * center[0] + s * center[1],
                            center[1] - s * center[0] - c * center[1]))
       
        trafo = Rotation(angle, center[0], center[1])
        s = math.sin(angle); c = math.cos(angle)
        fse(trafo.coeff(), (c, s, -s, c,
                            center[0] - c * center[0] + s * center[1],
                            center[1] - s * center[0] - c * center[1]))

        #
        # Translation()
        #
        trafo = Translation(0.5, 2)
        fse(trafo.coeff(), (1, 0, 0, 1, 0.5, 2))
        trafo = Translation((0.2, 5))
        fse(trafo.coeff(), (1, 0, 0, 1, 0.2, 5))
        trafo = Translation(Point(3, -1.5))
        fse(trafo.coeff(), (1, 0, 0, 1, 3, -1.5))

    def test_methods(self):
        """Test Trafo methods"""
        eq = self.assertEquals
        fse = self.assert_float_sequences_equal
        exc = self.assertRaises        

        p1 = Point(0, 0); p2 = (10.0, 20.0); p3 = [-100.0, 0.0]
        trafo = Trafo(0.25, 0, 0, 0.125, 100.75, 123.25)

        # DocToWin()
        eq(trafo.DocToWin(110, 30), (128, 127))
        eq(trafo.DocToWin(p1), (101, 123))
        eq(trafo.DocToWin(p2), (103, 126))
        eq(trafo.DocToWin(p3), (76, 123))

        # DTransform()
        eq(trafo.DTransform(110, 30), Point(27.5, 3.75))
        eq(trafo.DTransform(p1), Point(0.0, 0.0))
        eq(trafo.DTransform(p2), Point(2.5, 2.5))
        eq(trafo.DTransform(p3), Point(-25.0, 0))
        
        # inverse()
        eq(trafo.inverse().coeff(), (4, 0, 0, 8, -403, -986))
        exc(SingularMatrix, Trafo(1, 0, 1, 0).inverse)

        # matrix(), coeff(), offset()
        trafo = Trafo(1, 2, 0, 4, 5, 6)
        eq(trafo.coeff()[:4], trafo.matrix())
        eq(trafo.coeff(), trafo.matrix() + tuple(trafo.offset()))
        # offset() returns a point object
        eq(type(trafo.offset()), PointType)

    def test_transform(self):
        """Test Trafo tranformations"""
        eq = self.assertEquals
        fse = self.assert_float_sequences_equal
        exc = self.assertRaises

        trafo1 = Trafo(0.25, 0.1, 2, 0.125, 100.75, 123.25)
        trafo2 = Trafo(10, 0, 2, 4, 1, 2)

        # Transform points
        p1 = Point(0, 0); p2 = (10.0, 20.0); p3 = [-100.0, 0.0]

        result = trafo1(p1)
        eq(type(result), PointType)
        eq(result, Point(100.75, 123.25))

        result = trafo1(p2)
        eq(type(result), PointType)
        eq(result, Point(143.25, 126.75))

        result = trafo1(p3)
        eq(type(result), PointType)
        eq(result, Point(75.75, 113.25))

        result = trafo1(30, 0.1)
        eq(type(result), PointType)
        eq(result, Point(108.45, 126.2625))

        # concatenate transformations
        result = trafo1(trafo2)
        eq(type(result), TrafoType)
        fse(result.coeff(),
            (trafo1.m11 * trafo2.m11 + trafo1.m12 * trafo2.m21,
             trafo1.m21 * trafo2.m11 + trafo1.m22 * trafo2.m21,
             trafo1.m11 * trafo2.m12 + trafo1.m12 * trafo2.m22,
             trafo1.m21 * trafo2.m12 + trafo1.m22 * trafo2.m22,
             trafo1.m11 * trafo2.v1 + trafo1.m12 * trafo2.v2 + trafo1.v1,
             trafo1.m21 * trafo2.v1 + trafo1.m22 * trafo2.v2 + trafo1.v2))

        # The inverse of a concatenation is the concatenation of the
        # inverses
        inv1 = trafo1(trafo2).inverse()
        inv2 = trafo2.inverse()(trafo1.inverse())
        fse(inv1.coeff(), inv2.coeff())

        # rects can be transformed too
        r = Rect(0, 0, 1, 2)
        fse(tuple(trafo1(r)), tuple(PointsToRect((trafo1(r.left, r.bottom),
                                                  trafo1(r.left, r.top),
                                                  trafo1(r.right, r.top),
                                                  trafo1(r.right, r.bottom)))))
        


if __name__ == "__main__":
    unittest.main()
