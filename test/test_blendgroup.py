# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

"""
Tests for the BlendGroup and related classes
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Base import Undo
from Sketch.Graphics import BlendGroup, Rectangle, Document
from Sketch.Base.const import TRANSACTION_END
from Sketch import Trafo


def all_descendants(obj):
    """Return a list with all descendants of obj"""
    descendants = []
    if obj.is_Compound:
        for child in obj.GetObjects():
            descendants.append(child)
            if child.is_Compound:
                descendants.extend(all_descendants(child))
    return descendants

class TestBlendGroup(unittest.TestCase):

    def setUp(self):
        self.messages = []
        self.document = Document()
        self.layer = self.document.AppendLayer("test")
        self.document.Subscribe(TRANSACTION_END, self.transaction_end)

    def tearDown(self):
        self.document.Destroy()
        self.document = self.layer = None
        self.messages = None

    def clear_messages(self):
        del self.messages[:]

    def transaction_end(self, *args):
        self.messages.append((TRANSACTION_END, args))

    def compare_object_lists(self, list1, list2):
        """Compare two lists of objects.

        The lists are treated as sets of objects to be compared by
        identity.  If the lists don't match an AssertionError is raised
        """
        ids1 = map(id, list1)
        ids1.sort()
        ids2 = map(id, list2)
        ids2.sort()
        self.failUnless(ids1 == ids2, "%r != %r" % (list1, list2))

    def execute_test(self, init, action, check_before, check_after):
        """Run one test involving undo and redo

        The parameters are callables.  init is called to initialize
        self.document, action is called to perform the action that is to
        be tested.  Both init and action are called only once, without
        parameters, and they're called from within a transaction on
        self.document.

        The callable check_before is called to test the state before the
        action.  It's called twice, once withough arguments before the
        action is performed and once with the keyword argument
        after_undo=True after the action has been undone.

        The callable check_after is called to test the state after the
        action.  It's called twice, once withough arguments immediately
        after the transaction with the action call is finished and once
        with the keyword argument after_redo=True after the action has
        been redone.

        Immediately before the action transaction and before undo and
        redo are performed self.clear_messages is called so that the
        check functions called afterwards can check which messages have
        been issued.
        """
        try:
            self.document.BeginTransaction(self.id())
            init()
            self.document.EndTransaction()
        except:
            self.fail("Exception during init()")

        # sanity check
        check_before()

        try:
            self.clear_messages()
            self.document.BeginTransaction(self.id())
            action()
            self.document.EndTransaction()
        except:
            self.fail("Exception during action()")

        check_after()

        self.clear_messages()
        self.document.Undo()
        check_before(after_undo = True)

        self.clear_messages()
        self.document.Redo()
        check_after(after_redo = True)

    def test_remove_objects_interpolation(self):
        """Test BlendGroup.RemoveObjects() interpolation

        Trying to remove the interpolation object results in a
        ValueError.
        """
        r1 = Rectangle(Trafo(1, 0, 0, 1, 0, 0))
        r2 = Rectangle(Trafo(1, 0, 0, 1, 100, 100))

        blend = BlendGroup(2, r1, r2)

        self.assertRaises(ValueError,
                          blend.RemoveObjects, [blend.GetObjects()[1]])

    def test_remove_objects_one_of_two_controls(self):
        """Test BlendGroup.RemoveObjects() one of two controls

        The behavior tested for is this: No blend is possible with just
        one control object, so the blend group tries to remove itself
        from its parent.

        This is probably not really the best possible behavior but it's
        how the blend group behaved long before the test was written and
        for now it's best to just test for existing behavior.  Possible
        better behavior would be to have a blend group with just the
        remaining control object as child.
        """
        r1 = Rectangle(Trafo(1, 0, 0, 1, 0, 0))
        r2 = Rectangle(Trafo(1, 0, 0, 1, 100, 100))

        blend = BlendGroup(2, r1, r2)

        # References for later tests
        descendants = all_descendants(blend)

        def init():
            self.document.Insert(blend, self.layer)

        def action():
            self.document.AddUndo(blend.RemoveObjects([r1]))

        def check_before(after_undo = False):
            self.assertEquals(self.layer.GetObjects(), [blend])
            self.assertEquals(all_descendants(blend), descendants)

        def check_after(after_redo = False):
            # The blend group should have been removed from the group and
            # the bledn group itself shouldn't have changed.
            self.assertEquals(self.layer.GetObjects(), [])
            self.assertEquals(all_descendants(blend), descendants)

            # There should have been one TRANSACTION_END message
            self.assertEquals(len(self.messages), 1)
            msg = self.messages[0]
            self.assertEquals(msg[0], TRANSACTION_END)
            if after_redo:
                undo_type = None
            else:
                undo_type = "added"
            self.assertEquals(msg[1][:3], ("normal", 1, undo_type))
            self.compare_object_lists(msg[1][3].InsertedObjects(), [])
            self.compare_object_lists(msg[1][3].RemovedObjects(),
                                      [blend] + descendants)

        self.execute_test(init, action, check_before, check_after)

    def test_remove_objects_middle_of_three(self):
        """Test BlendGroup.RemoveObjects() middle of three controls

        In this case the middle control object and of the interpolation
        objects is removed and the number of steps of the removed
        interpolation is added to the number of steps in the remaining
        interpolation.
        """
        r1 = Rectangle(Trafo(1, 0, 0, 1, 0, 0))
        r2 = Rectangle(Trafo(1, 0, 0, 1, 100, 100))
        r3 = Rectangle(Trafo(1, 0, 0, 1, 200, 0))

        blend = BlendGroup(2, r1, r2)
        blend.ExtendBlend(r2, r3, 2)

        def init():
            self.document.Insert(blend, self.layer)

        def action():
            self.document.AddUndo(blend.RemoveObjects([r2]))

        def check_after(after_redo = False):
            self.assertEquals(len(blend.GetObjects()), 3)
            self.assertEquals(blend.GetObjects()[0], r1)
            self.assertEquals(blend.GetObjects()[2], r3)
            interpolation = blend.GetObjects()[1]
            self.assertEquals([o.Trafo() for o in interpolation.GetObjects()],
                              [Trafo(1, 0, 0, 1, 50, 0),
                               Trafo(1, 0, 0, 1, 100, 0),
                               Trafo(1, 0, 0, 1, 150, 0)])

            # There should have been one TRANSACTION_END message
            self.assertEquals(len(self.messages), 1)
            msg = self.messages[0]
            self.assertEquals(msg[0], TRANSACTION_END)
            if after_redo:
                undo_type = None
            else:
                undo_type = "added"
            self.assertEquals(msg[1][:3], ("normal", 1, undo_type))
            inserted = msg[1][3]
            self.compare_object_lists(inserted.InsertedObjects(),
                                      interpolation.GetObjects())
            # It's difficult without relying on implementation details
            # to determine exactly which objects have been removed.
            # However with only the knowledge that one of the original
            # interpolation objects is removed and that if an
            # interpolation is recomputed, the old interpolated children
            # are replaced by new objects, we can determine how many
            # objects should be in there: r2, the removed interpolation,
            # two rectangles, one from each interpolation object.
            # Therefore we check whether the list of removed objects has
            # four items and whether r2 is in it.
            self.assertEquals(len(inserted.RemovedObjects()), 4)
            self.failUnless(r2 in inserted.RemovedObjects())


        def check_before(after_undo = False):
            self.assertEquals(len(blend.GetObjects()), 5)
            self.assertEquals(blend.GetObjects()[0], r1)
            self.assertEquals(blend.GetObjects()[2], r2)
            self.assertEquals(blend.GetObjects()[4], r3)
            interpolated = blend.GetObjects()[1].GetObjects()
            self.assertEquals([o.Trafo() for o in interpolated],
                              [Trafo(1, 0, 0, 1, 50, 50)])
            interpolated = blend.GetObjects()[3].GetObjects()
            self.assertEquals([o.Trafo() for o in interpolated],
                              [Trafo(1, 0, 0, 1, 150, 50)])

        self.execute_test(init, action, check_before, check_after)


if __name__ == "__main__":
    unittest.main()
