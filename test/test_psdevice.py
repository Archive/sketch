# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Test PostScriptDevice class"""

__version__ = "$Revision$"
# $Source$
# $Id$

import os
import binascii
import zlib
import unittest
import cStringIO as StringIO

import PIL

import support
support.add_sketch_dir_to_path()

from Sketch import Identity, Trafo, Rect
from Sketch.Graphics import Document, Rectangle, SolidPattern, StandardColors,\
     PostScriptDevice


from Sketch import _libart


class PostScriptDeviceTest(unittest.TestCase, support.VolatileFileMixin):

    def setUp(self):
        """Create a document and a libart device"""
        self.doc = Document(1)
        for layer in self.doc.Layers():
            if not layer.is_SpecialLayer:
                self.layer = layer

    def make_image_filename(self, suffix = ""):
        return self.volatile_file_name(self.id() + suffix + ".ppm")

    def make_ps_filename(self):
        return self.volatile_file_name(self.id() + ".ps")

    def render_ps(self, infile, outfile):
        width = 100
        height = 100
        resolution = 10
        gs_command = ('gs -sDEVICE=ppmraw -dNOPAUSE -dSAFER'
                      ' -q -sOutputFile=%(outfile)s -g%(width)dx%(height)d'
                      ' -c /oldshowpage /showpage load def'
                      ' /showpage \'{}\' def '
                      ' -f %(infile)s -c oldshowpage quit') % locals()
                      #' -f%(infile)s') % locals()
        if os.system(gs_command) != 0:
            raise RuntimeError("ghostscript command %r didn't succeed"
                               % gs_command)

        image = PIL.Image.open(outfile)
        return image

    def check_images(self, psfilename):
        # Write the reference image
        reference = zlib.decompress(binascii.a2b_base64(self.reference_data))
        reference_name = self.make_image_filename("-reference")
        f = file(reference_name, "wb")
        f.write(reference)
        f.close()

        reference_image = PIL.Image.open(reference_name)

        rendered_name = self.make_image_filename("-rendered")
        rendered_image = self.render_ps(psfilename, rendered_name)

        difference = PIL.ImageChops.difference(reference_image, rendered_image)
        difference.save(self.make_image_filename("-difference"))

        # This check is extremely pedantic as it allows no difference
        # whatsoever. So far this works well enough, though.
        self.assertEquals(difference.getextrema(), ((0, 0), (0, 0), (0, 0)))


class TestOpacity(PostScriptDeviceTest):

    reference_data = (
        'eJzt2zFOAkEYhmHrPcUkNtqhCXYWNhoTC66wwAQo0M2y0Xh61COYbIAPnj/vHGCe+'
        'duZPTTX5XXbrmpZ1ffat0Ndlvl3eXqevZWX9cdu2C36TTeUm2X93CzqY9dt+/brtr'
        'mbTMrvae6n02Y/3lz9zf6MY8WKFauUWLFixSolVqxYsUqJFStWrFJixYoVq5RYsWL'
        'FKqdLmJH26thPdZBVYMWKFauAWLFixSolVqxYsUqJFStWrFJixYoVq5RYsWLFKiVW'
        'rFixSokVK1asUmLFihWrlFixYsUqJVasWLFKiRUrVqxSYvVPq3Hm6Hc5gNWIP7rPO'
        '1asWLFKiRUrVqxSYsWKFauUWLFixSolVqxYsUqJFasTt/oBlKSTRQ==\n')

    def test_fill_opacity(self):
        self.width = self.height = 100
        rect = Rectangle(Trafo(self.width, 0, 0, self.height,
                               0.25 * self.width, 0.25 * self.height))
        pattern = SolidPattern(StandardColors.blue)
        rect.Properties().SetProperty(fill_pattern = pattern)
        self.doc.Insert(rect, self.layer)
        rect = Rectangle(Trafo(self.width, 0, 0, self.height,
                               -0.25 * self.width, -0.25 * self.height))
        pattern = SolidPattern(StandardColors.red)
        rect.Properties().SetProperty(fill_pattern = pattern)
        rect.Properties().SetProperty(fill_opacity = 0.5)
        self.doc.Insert(rect, self.layer)

        psfilename = self.make_ps_filename()
        bbox = tuple(self.doc.BoundingRect(visible = 0, printable = 1))
        dev = PostScriptDevice(psfilename, document = self.doc,
                               as_eps = 1, bounding_box = bbox)
        self.doc.Draw(dev)
        dev.Close()
        self.check_images(psfilename)
