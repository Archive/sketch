# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the pattern classes
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import unittest
import PIL

import support

support.add_sketch_dir_to_path()

from Sketch.Graphics import EmptyPattern, SolidPattern, LinearGradient, \
     RadialGradient, RadialGradient, ConicalGradient, HatchingPattern, \
     ImageTilePattern, ImageData, \
     CreateSimpleGradient, StandardColors, CreateRGBColor
from Sketch import Point, Trafo, Rect

type_attributes = ["is_procedural", "is_Empty", "is_Solid", "is_Gradient",
                   "is_RadialGradient", "is_AxialGradient",
                   "is_ConicalGradient", "is_Hatching", "is_Tiled", "is_Image"]


class PatternTestMixin:

    """Mixin class for the pattern tests."""

    def check_type_attributes(self, pattern, **kw):
        """Check whether the type attributes of the pattern.

        Check whether the attributes described by the keyword arguments
        have he given values. For all other type attributes, check
        whether they're false.
        """
        for name in type_attributes:
            value = getattr(pattern, name)
            expected = kw.get(name, 0)
            self.assertEquals(expected, value,
                              "%s has value %r, expected %r" % (name, value,
                                                                expected))

class TestEmptyPattern(unittest.TestCase, PatternTestMixin):

    def test(self):
        """Test EmptyPattern type attributes"""
        self.check_type_attributes(EmptyPattern, is_Empty = 1)

    def test_transform(self):
        """Test EmptyPattern.Transformed()

        The transformed empty pattern is the empty pattern.
        """
        self.assertEquals(EmptyPattern.Transformed(Trafo(1, 0, 1, 1, 10, 10)),
                          EmptyPattern)


class TestSolidPattern(unittest.TestCase, PatternTestMixin):

    def test_construction(self):
        """Test SolidPattern(color)"""
        pattern = SolidPattern(StandardColors.blue)
        self.check_type_attributes(SolidPattern(StandardColors.blue),
                                   is_Solid = 1)
        self.assertEquals(pattern.Color(), StandardColors.blue)

    def test_transform(self):
        """Test SolidPattern.Transformed()

        The transformed solid pattern is the same pattern.
        """
        pattern = SolidPattern(StandardColors.blue)
        self.assertEquals(pattern.Transformed(Trafo(1, 0, 1, 1, 10, 10)),
                          pattern)

    def test_blend(self):
        """Test SolidPattern.Blend()"""
        # Use keyword arguments in the constructors to test them
        pattern1 = SolidPattern(color = StandardColors.blue)
        pattern2 = SolidPattern(color = StandardColors.black)
        blend = pattern1.Blend(pattern2, 0.25, 0.75)
        self.assertEquals(blend.Color(), CreateRGBColor(0, 0, 0.25))

    def test_equality(self):
        """Test SolidPattern equality"""
        pattern_blue = SolidPattern(color = StandardColors.blue)
        pattern_blue2 = SolidPattern(color = StandardColors.blue)
        pattern_black = SolidPattern(color = StandardColors.black)
        self.failUnless(pattern_blue == pattern_blue2)
        self.failUnless(pattern_blue != pattern_black)

    def test_str(self):
        """Test SolidPattern.__str__"""
        pattern = SolidPattern(StandardColors.blue)
        self.assertEquals(str(pattern), "SolidPattern(RGBColor(0,0,1))")


class TestLinearGradientPattern(unittest.TestCase, PatternTestMixin):

    """Test cases for axial/linear gradients"""

    def test_construction(self):
        """Test LinearGradient(gradient)"""
        pattern = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green))
        self.check_type_attributes(pattern, is_AxialGradient = 1,
                                   is_Gradient = 1, is_procedural = 1)
        self.assertEquals(pattern.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])
        self.assertEquals(pattern.Border(), 0)
        self.assertEquals(pattern.Direction(), Point(0, -1))

    def test_construction_all_args(self):
        """Test LinearGradient(gradient, direction, border)"""
        pattern = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)
        self.assertEquals(pattern.Direction(), Point(1, 0))
        self.assertEquals(pattern.Border(), 0.25)
        self.assertEquals(pattern.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_direction(self):
        """Test LinearGradient.WithDirection()"""
        blue = StandardColors.blue
        green = StandardColors.green
        pattern = LinearGradient(gradient = CreateSimpleGradient(blue, green),
                                 direction = Point(1, 0), border = 0.25)
        p = pattern.WithDirection(Point(-1, 0))
        self.assertEquals(p.Direction(), Point(-1, 0))
        self.assertEquals(p.Border(), 0.25)
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_border(self):
        """Test LinearGradient.WithBorder()"""
        pattern = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)
        p = pattern.WithBorder(0.75)
        self.assertEquals(p.Direction(), Point(1, 0))
        self.assertEquals(p.Border(), 0.75)
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_gradient(self):
        """Test LinearGradient.WithBorder()"""
        pattern = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)
        p = pattern.WithGradient(CreateSimpleGradient(StandardColors.red,
                                                      StandardColors.black))
        self.assertEquals(p.Direction(), Point(1, 0))
        self.assertEquals(p.Border(), 0.25)
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.red),
                           (1, StandardColors.black)])

    # Transformation tests.
    #
    # We only have to check the variant without rectangles as they can't
    # influence the result.

    def test_shear_parallel(self):
        """Test LinearGradient.Transformed(<shear parallel to direction>)

        Transforming the linear gradient only changes its direction.
        Neither not the border nor the colors are affected.
        """
        pattern = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)

        # 1. Transform with a shear parallel to the x-axis

        # The vector (1, 0) is mapped to itself whereas (0, 1) is mapped
        # to (1, 1).
        shear = Trafo(1, 0, 1, 1, 10, 10)
        # Transforming the axial gradient with direction (1, 0) by this
        # shear means that the new direction is Norm(1, -1). The reason
        # for this is that the lines of equal color are parallel to (0,
        # 1) so after the shear they're parallel to (1, 1) and the
        # direction must be perpendicular to that.
        transformed = pattern.Transformed(shear)
        self.assertEquals(transformed.Direction(), Point(1, -1).normalized())
        self.assertEquals(transformed.Border(), 0.25)
        # Don't bother to check the colors as only the geometry
        # parameters might be affected at all.

    def test_shear_orthogonal(self):
        """Test LinearGradient.Transformed(<shear orthogonal to direction>)

        Transforming the linear gradient only changes its direction.
        Neither not the border nor the colors are affected.
        """
        pattern = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)

        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, 10)
        # Transforming the axial gradient with direction (1, 0) by this
        # shear means that the new direction is the same as the old one.
        # The reason for this is that the lines of equal color are
        # parallel to (0, 1) so they're not affected by the shear.
        transformed = pattern.Transformed(shear)
        self.assertEquals(transformed.Direction(), Point(1, 0))
        self.assertEquals(transformed.Border(), 0.25)
        # Don't bother to check the colors as only the geometry
        # parameters might be affected at all.


    #
    # Blend tests
    #

    def test_blend_with_linear_gradient(self):
        """Test LinearGradient.Blend(<linear gradient>)"""
        pattern1 = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), 0.0)
        pattern2 = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.black),
                                  Point(0, 1), 0.5)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.assertEquals(blended.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, CreateRGBColor(0.0, 0.25, 0.0))])
        self.assertEquals(blended.Border(), 0.375)

        # That the direction of the blended pattern isn't normalized is
        # a quirk of the implementation that simply takes the weighted
        # sum of both directions. It can even yield Point(0, 0) which
        # effectively means Point(1, 0)
        self.assertEquals(blended.Direction(),
                          Point(0.25, 0.75))

    def test_blend_with_solid(self):
        """Test LinearGradient.Blend(<solid pattern>)"""
        pattern1 = LinearGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), 0.0)
        pattern2 = SolidPattern(StandardColors.black)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.check_type_attributes(blended, is_AxialGradient = 1,
                                   is_Gradient = 1, is_procedural = 1)
        self.assertEquals(blended.Gradient().Colors(),
                          [(0, CreateRGBColor(0.0, 0.0, 0.25)),
                           (1, CreateRGBColor(0.0, 0.25, 0.0))])
        # Border and direction are the same as for the linear gradient
        self.assertEquals(blended.Border(), 0.0)
        self.assertEquals(blended.Direction(), Point(1, 0))


class TestRadialGradientPattern(unittest.TestCase, PatternTestMixin):

    """Test cases for radial gradients"""

    def test_construction(self):
        """Test RadialGradient(gradient)"""
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green))
        self.check_type_attributes(pattern, is_RadialGradient = 1,
                                   is_Gradient = 1, is_procedural = 1)
        self.assertEquals(pattern.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])
        self.assertEquals(pattern.Border(), 0)
        self.assertEquals(pattern.Center(), Point(0.5, 0.5))

    def test_construction_all_args(self):
        """Test RadialGradient(gradient, direction, border)"""
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)
        self.assertEquals(pattern.Center(), Point(1, 0))
        self.assertEquals(pattern.Border(), 0.25)
        self.assertEquals(pattern.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_center(self):
        """Test RadialGradient.WithCenter()"""
        blue = StandardColors.blue
        green = StandardColors.green
        pattern = RadialGradient(gradient = CreateSimpleGradient(blue, green),
                                 center = Point(1, 0), border = 0.25)
        p = pattern.WithCenter(Point(-1, 0))
        self.assertEquals(p.Center(), Point(-1, 0))
        self.assertEquals(p.Border(), 0.25)
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_border(self):
        """Test RadialGradient.WithBorder()"""
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)
        p = pattern.WithBorder(0.75)
        self.assertEquals(p.Center(), Point(1, 0))
        self.assertEquals(p.Border(), 0.75)
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_gradient(self):
        """Test RadialGradient.WithBorder()"""
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)
        p = pattern.WithGradient(CreateSimpleGradient(StandardColors.red,
                                                      StandardColors.black))
        self.assertEquals(p.Center(), Point(1, 0))
        self.assertEquals(p.Border(), 0.25)
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.red),
                           (1, StandardColors.black)])

    #
    # Transform tests
    #

    def test_transform_no_rects(self):
        """Test RadialGradient.Transformed(<trafo only, no rects>)

        Transforming the radial gradient only changes its center.
        Neither not the border nor the colors are affected.
        """
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(1, 0),
                                 0.25)

        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, 10)
        # Without rectangles the center of the transformed pattern is
        # the same as before.
        transformed = pattern.Transformed(shear)
        self.assertEquals(transformed.Center(), Point(1, 0))
        self.assertEquals(transformed.Border(), 0.25)


    def test_shear_with_rects(self):
        """Test RadialGradient.Transformed(<trafo and rects>)

        Transforming the radial gradient only changes its center.
        Neither not the border nor the colors are affected.
        """
        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, -20)

        # The center of the radial gradient is a position relative to
        # the bounding box of the object it is used with. To really
        # transform the radial gradient one therefore needs the bounding
        # rects of the object before and after the transformation.
        rects = (Rect(0, 0, 100, 100),
                 Rect(10, -20, 110, 180))

        # A pattern with the center in the middle of the bounding rect.
        # In this case the resulting center relative to the new bounding
        # rect is the same as the old one.
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(0.5, 0.5),
                                 0.25)
        transformed = pattern.Transformed(shear, rects = rects)
        self.assertEquals(transformed.Center(), Point(0.5, 0.5))
        self.assertEquals(transformed.Border(), 0.25)

        # A pattern with the center in the middle of the top edge of the
        # bounding rect. In this case the resulting center relative to
        # the new bounding rect is horizontally in the center but
        # vertically at 0.75
        pattern = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                      StandardColors.green),
                                 Point(0.5, 1.0),
                                 0.25)
        transformed = pattern.Transformed(shear, rects = rects)
        self.assertEquals(transformed.Center(), Point(0.5, 0.75))
        self.assertEquals(transformed.Border(), 0.25)

    #
    # Blend tests
    #

    def test_blend_with_radial_gradient(self):
        """Test RadialGradient.Blend(<radial gradient>)"""
        pattern1 = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(0.0, 1.0), 0.5)
        pattern2 = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.white),
                                  Point(0.5, 0.5), 0.0)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.assertEquals(blended.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, CreateRGBColor(0.75, 1.0, 0.75))])
        self.assertEquals(blended.Center(), Point(0.375, 0.625))
        self.assertEquals(blended.Border(), 0.125)

    def test_blend_with_solid(self):
        """Test RadialGradient.Blend(<solid pattern>)"""
        pattern1 = RadialGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(0.0, 1.0), 0.5)
        pattern2 = SolidPattern(StandardColors.black)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.check_type_attributes(blended, is_RadialGradient = 1,
                                   is_Gradient = 1, is_procedural = 1)
        self.assertEquals(blended.Gradient().Colors(),
                          [(0, CreateRGBColor(0.0, 0.0, 0.25)),
                           (1, CreateRGBColor(0.0, 0.25, 0.0))])
        # Border and direction are the same as for the linear gradient
        self.assertEquals(blended.Center(), Point(0, 1))
        self.assertEquals(blended.Border(), 0.5)


class TestConicalGradientPattern(unittest.TestCase, PatternTestMixin):

    """Test cases for conical gradients"""

    def test_construction(self):
        """Test ConicalGradient(gradient)"""
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green))
        self.check_type_attributes(pattern, is_ConicalGradient = 1,
                                   is_Gradient = 1, is_procedural = 1)
        self.assertEquals(pattern.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])
        self.assertEquals(pattern.Center(), Point(0.5, 0.5))
        self.assertEquals(pattern.Direction(), Point(1, 0))

    def test_construction_all_args(self):
        """Test ConicalGradient(gradient, direction, border)"""
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), Point(0, -1))
        self.assertEquals(pattern.Center(), Point(1, 0))
        self.assertEquals(pattern.Direction(), Point(0, -1))
        self.assertEquals(pattern.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_center(self):
        """Test ConicalGradient.WithCenter()"""
        blue = StandardColors.blue
        green = StandardColors.green
        pattern = ConicalGradient(gradient = CreateSimpleGradient(blue, green),
                                  center = Point(1, 0),
                                  direction = Point(0, -1))
        p = pattern.WithCenter(Point(-1, 0))
        self.assertEquals(p.Center(), Point(-1, 0))
        self.assertEquals(p.Direction(), Point(0, -1))
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_direction(self):
        """Test ConicalGradient.WithDirection()"""
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), Point(0, -1))
        p = pattern.WithDirection(Point(-1, 0))
        self.assertEquals(p.Center(), Point(1, 0))
        self.assertEquals(p.Direction(), Point(-1, 0))
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, StandardColors.green)])

    def test_with_gradient(self):
        """Test ConicalGradient.WithBorder()"""
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), Point(0, -1))
        p = pattern.WithGradient(CreateSimpleGradient(StandardColors.red,
                                                      StandardColors.black))
        self.assertEquals(p.Center(), Point(1, 0))
        self.assertEquals(p.Direction(), Point(0, -1))
        self.assertEquals(p.Gradient().Colors(),
                          [(0, StandardColors.red),
                           (1, StandardColors.black)])

    #
    # Transform tests
    #

    def test_shear_with_rects(self):
        """Test ConicalGradient.Transformed(<trafo and rects>)

        Transforming the conical gradient changes its direction and
        center. The colors are not affected.
        """
        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, -20)

        # The center of the conical gradient is a position relative to
        # the bounding box of the object it is used with. To really
        # transform the conical gradient one therefore needs the bounding
        # rects of the object before and after the transformation.
        rects = (Rect(0, 0, 100, 100),
                 Rect(10, -20, 110, 180))

        # A pattern with the center in the middle of the bounding rect.
        # In this case the resulting center relative to the new bounding
        # rect is the same as the old one. The direction stays the same
        # too as it's parallel to the y-axis
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(0.5, 0.5), Point(0, 1))
        transformed = pattern.Transformed(shear, rects = rects)
        self.assertEquals(transformed.Center(), Point(0.5, 0.5))
        self.assertEquals(transformed.Direction(), Point(0, 1))

        # A pattern with the center in the middle of the right edge of
        # the bounding rect. In this case the resulting center relative
        # to the new bounding rect is vertically in the center but
        # horizontally still at the right edge. The direction becomes
        # Norm(1, 1) as it's parallel to the x-axis
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), Point(1, 0))
        transformed = pattern.Transformed(shear, rects = rects)
        self.assertEquals(transformed.Center(), Point(1.0, 0.5))
        self.assertEquals(transformed.Direction(), Point(1, 1).normalized())

    def test_shear_without_rects(self):
        """Test ConicalGradient.Transformed(<trafo but no rects>)

        Transforming the conical gradient changes its direction and
        center, however, without the rects argument the center stays the
        same. The colors are not affected in any case.
        """
        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, -20)

        # The direction becomes Norm(1, 1) as it's parallel to the
        # x-axis
        pattern = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                       StandardColors.green),
                                  Point(1, 0), Point(1, 0))
        transformed = pattern.Transformed(shear)
        self.assertEquals(transformed.Center(), Point(1, 0))
        self.assertEquals(transformed.Direction(), Point(1, 1).normalized())

    #
    # Blend tests
    #

    def test_blend_with_conical_gradient(self):
        """Test ConicalGradient.Blend(<conical gradient>)"""
        pattern1 = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                        StandardColors.green),
                                   Point(0.5, 0.5), Point(1, 0))
        pattern2 = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                        StandardColors.black),
                                   Point(1, 0), Point(0, 1))
        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.assertEquals(blended.Gradient().Colors(),
                          [(0, StandardColors.blue),
                           (1, CreateRGBColor(0.0, 0.25, 0.0))])
        self.assertEquals(blended.Center(), Point(0.875, 0.125))

        # That the direction of the blended pattern isn't normalized is
        # a quirk of the implementation that simply takes the weighted
        # sum of both directions. It can even yield Point(0, 0) which
        # effectively means Point(1, 0)
        self.assertEquals(blended.Direction(), Point(0.25, 0.75))

    def test_blend_with_solid(self):
        """Test ConicalGradient.Blend(<solid pattern>)"""
        pattern1 = ConicalGradient(CreateSimpleGradient(StandardColors.blue,
                                                        StandardColors.green),
                                   Point(0.5, 0.5), Point(1, 0))
        pattern2 = SolidPattern(StandardColors.black)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.check_type_attributes(blended, is_ConicalGradient = 1,
                                   is_Gradient = 1, is_procedural = 1)
        self.assertEquals(blended.Gradient().Colors(),
                          [(0, CreateRGBColor(0.0, 0.0, 0.25)),
                           (1, CreateRGBColor(0.0, 0.25, 0.0))])
        # Border and direction are the same as for the linear gradient
        self.assertEquals(blended.Center(), Point(0.5, 0.5))
        self.assertEquals(blended.Direction(), Point(1, 0))


class TestHatchingGradientPattern(unittest.TestCase, PatternTestMixin):

    """Test cases for HatchingPattern"""

    def test_construction(self):
        """Test HatchingPattern(fg)"""
        pattern = HatchingPattern(StandardColors.blue)
        self.check_type_attributes(pattern, is_Hatching = 1, is_procedural = 1)
        self.assertEquals(pattern.Foreground(), StandardColors.blue)
        self.assertEquals(pattern.Background(), StandardColors.white)
        self.assertEquals(pattern.Direction(), Point(1, 0))
        self.assertEquals(pattern.Spacing(), 5.0)
        self.assertEquals(pattern.Width(), 0.5)

    def test_construction_all_args(self):
        """Test HatchingPattern(fg, bg, direction, spacing, width)"""
        pattern = HatchingPattern(StandardColors.blue, StandardColors.green,
                                  Point(0, 1), 2.0, 0.25)
        self.assertEquals(pattern.Foreground(), StandardColors.blue)
        self.assertEquals(pattern.Background(), StandardColors.green)
        self.assertEquals(pattern.Direction(), Point(0, 1))
        self.assertEquals(pattern.Spacing(), 2.0)
        self.assertEquals(pattern.Width(), 0.25)

    def test_with_direction(self):
        """Test HatchingPattern.WithDirection()"""
        pattern = HatchingPattern(foreground = StandardColors.blue,
                                  background = StandardColors.green,
                                  direction = Point(0, 1),
                                  spacing = 2.0,
                                  width = 0.25)
        p = pattern.WithDirection(Point(-1, 0))
        self.assertEquals(p.Foreground(), StandardColors.blue)
        self.assertEquals(p.Background(), StandardColors.green)
        self.assertEquals(p.Direction(), Point(-1, 0))
        self.assertEquals(p.Spacing(), 2.0)
        self.assertEquals(p.Width(), 0.25)

    def test_with_foreground(self):
        """Test HatchingPattern.WithForeground()"""
        pattern = HatchingPattern(foreground = StandardColors.blue,
                                  background = StandardColors.green,
                                  direction = Point(0, 1),
                                  spacing = 2.0,
                                  width = 0.25)
        p = pattern.WithForeground(StandardColors.black)
        self.assertEquals(p.Foreground(), StandardColors.black)
        self.assertEquals(p.Background(), StandardColors.green)
        self.assertEquals(p.Direction(), Point(0, 1))
        self.assertEquals(p.Spacing(), 2.0)
        self.assertEquals(p.Width(), 0.25)

    def test_with_background(self):
        """Test HatchingPattern.WithBackground()"""
        pattern = HatchingPattern(foreground = StandardColors.blue,
                                  background = StandardColors.green,
                                  direction = Point(0, 1),
                                  spacing = 2.0,
                                  width = 0.25)
        p = pattern.WithBackground(StandardColors.black)
        self.assertEquals(p.Foreground(), StandardColors.blue)
        self.assertEquals(p.Background(), StandardColors.black)
        self.assertEquals(p.Direction(), Point(0, 1))
        self.assertEquals(p.Spacing(), 2.0)
        self.assertEquals(p.Width(), 0.25)

    #
    #   Transform Tests
    #

    def test_shear_parallel(self):
        """Test HatchingPattern.Transformed(<shear parallel to direction>)

        Only the geometry attributes can be affected at all and only the
        direction is currenly actually affected by any transformation of
        the hatching pattern.
        """
        pattern = HatchingPattern(foreground = StandardColors.blue,
                                  background = StandardColors.green,
                                  direction = Point(0, 1),
                                  spacing = 2.0,
                                  width = 0.25)

        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, -20)
        transformed = pattern.Transformed(shear)
        # Since the direction is parallel to the shear it stays the same
        self.assertEquals(transformed.Direction(), Point(0, 1))
        # The spacing and width don't change either.
        self.assertEquals(transformed.Spacing(), 2.0)
        self.assertEquals(transformed.Width(), 0.25)

    def test_shear_orthogonal(self):
        """Test HatchingPattern.Transformed(<shear orthogonal to direction>)

        Only the geometry attributes can be affected at all and only the
        direction is currenly actually affected by any transformation of
        the hatching pattern.
        """
        pattern = HatchingPattern(foreground = StandardColors.blue,
                                  background = StandardColors.green,
                                  direction = Point(0, 1),
                                  spacing = 2.0,
                                  width = 0.25)

        # A shear parallel to the x-axis. The vector (1, 0) is mapped to
        # itself whereas (0, 1) is mapped to (1, 1).
        shear = Trafo(1, 0, 1, 1, 10, -20)
        transformed = pattern.Transformed(shear)
        # The directin becomes Norm(1, 1)
        self.assertEquals(transformed.Direction(), Point(1, 1).normalized())
        # The spacing and width don't change.
        self.assertEquals(transformed.Spacing(), 2.0)
        self.assertEquals(transformed.Width(), 0.25)

    #
    #   Blend Tests
    #

    def test_blend_with_hatching(self):
        """Test HatchingPattern.Blend(<hatching pattern>)"""
        pattern1 = HatchingPattern(foreground = StandardColors.blue,
                                   background = StandardColors.green,
                                   direction = Point(0, 1),
                                   spacing = 1.0,
                                   width = 0.5)
        pattern2 = HatchingPattern(foreground = StandardColors.blue,
                                   background = StandardColors.black,
                                   direction = Point(1, 0),
                                   spacing = 2.0,
                                   width = 1.0)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)

        self.assertEquals(blended.Foreground(), StandardColors.blue)
        self.assertEquals(blended.Background(), CreateRGBColor(0, 0.25, 0))
        self.assertEquals(blended.Spacing(), 1.75)
        self.assertEquals(blended.Width(), 0.875)

        # That the direction of the blended pattern isn't normalized is
        # a quirk of the implementation that simply takes the weighted
        # sum of both directions. It can even yield Point(0, 0) which
        # effectively means Point(1, 0)
        self.assertEquals(blended.Direction(), Point(0.75, 0.25))

    def test_blend_with_solid(self):
        """Test HatchingPattern.Blend(<solid pattern>)"""
        pattern1 = HatchingPattern(foreground = StandardColors.blue,
                                   background = StandardColors.green,
                                   direction = Point(0, 1),
                                   spacing = 1.0,
                                   width = 0.5)
        pattern2 = SolidPattern(StandardColors.black)
        blended = pattern1.Blend(pattern2, 0.25, 0.75)

        self.check_type_attributes(blended, is_Hatching = 1, is_procedural = 1)
        self.assertEquals(blended.Foreground(), CreateRGBColor(0, 0, 0.25))
        self.assertEquals(blended.Background(), CreateRGBColor(0, 0.25, 0))
        # Spacing and width and direction are the same as in the input
        # hatching pattern
        self.assertEquals(blended.Spacing(), 1)
        self.assertEquals(blended.Width(), 0.5)
        self.assertEquals(blended.Direction(), Point(0, 1))


class TestImageTilePattern(unittest.TestCase, PatternTestMixin):

    """Test cases for ImageTilePattern"""

    def test_construction(self):
        """Test ImageTilePattern(image)"""
        image = ImageData(PIL.Image.new('RGB', (16, 16), (255, 255, 255)))
        pattern = ImageTilePattern(image)
        self.check_type_attributes(pattern, is_Image = 1, is_Tiled = 1,
                                   is_procedural = 1)
        self.assertEquals(pattern.Image(), image)
        self.assertEquals(pattern.Trafo(), Trafo(1, 0, 0, -1, 0, 0))

    def test_construction_all_args(self):
        """Test ImageTilePattern(image, trafo)"""
        image = ImageData(PIL.Image.new('RGB', (16, 16), (255, 255, 255)))
        pattern = ImageTilePattern(image, Trafo(0.5, 0.0, 0.5, -0.5))
        self.assertEquals(pattern.Image(), image)
        self.assertEquals(pattern.Trafo(), Trafo(0.5, 0.0, 0.5, -0.5))

    def test_with_image(self):
        """Test ImageTilePattern.WithImage()"""
        image = ImageData(PIL.Image.new('RGB', (16, 16), (255, 255, 255)))
        # Use keyword arguments in the constructor to test whether they
        # work
        pattern = ImageTilePattern(image, trafo = Trafo(0.5, 0.0, 0.5, -0.5))

        image2 = PIL.Image.new('RGB', (16, 16), (255, 0, 0))
        p = pattern.WithImage(image2)
        self.assertEquals(p.Image(), image2)
        self.assertEquals(p.Trafo(), Trafo(0.5, 0.0, 0.5, -0.5))

    #
    #   Transform tests
    #

    def test_shear_without_rects(self):
        """Test ImageTilePattern.Transformed(<shear, no rects>)

        Only the trafo associated with the image tile pattern can be
        affected. The image itself stays the same.
        """
        image = ImageData(PIL.Image.new('RGB', (16, 16), (255, 255, 255)))
        pattern = ImageTilePattern(image, trafo = Trafo(0.5, 0.0, 0.5, -0.5))

        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, -20)

        # Transform
        transformed = pattern.Transformed(shear)

        # Since no rects were given the resulting transformation is the
        # shear applied to the original one
        self.assertEquals(transformed.Trafo(),
                          Trafo(0.5, 0.5, 0.5, 0, 10, -20))


    def test_shear_with_rects(self):
        """Test ImageTilePattern.Transformed(<shear and rects>)

        Only the trafo associated with the image tile pattern can be
        affected. The image itself stays the same.
        """
        image = ImageData(PIL.Image.new('RGB', (16, 16), (255, 255, 255)))
        pattern = ImageTilePattern(image, trafo = Trafo(0.5, 0.0, 0.5, -0.5))

        # A shear parallel to the y-axis. The vector (1, 0) is mapped to
        # (1, 1) whereas (0, 1) is mapped to iself.
        shear = Trafo(1, 1, 0, 1, 10, -20)

        # Define suitable rectangles
        rects = (Rect(0, 0, 100, 100),
                 Rect(10, -20, 110, 180))

        # Transform
        transformed = pattern.Transformed(shear, rects = rects)

        # The resulting transformation is a translation by (-10, -180)
        # (minus the top left corner of the second rectangle) applied to
        # the shear applied to a translation by (0, 100) (the top left
        # corner of the first rectangle) applied to the original
        # transformation
        self.assertEquals(transformed.Trafo(),
                          Trafo(0.5, 0.5, 0.5, 0, 0, -100))

    #
    #   Blend tests
    #

    def test_blend_with_tiled_image(self):
        """Test ImageTilePattern.Blend(<image tile pattern>)"""
        image = ImageData(PIL.Image.new('RGB', (16, 16), (255, 255, 255)))
        pattern1 = ImageTilePattern(image, trafo = Trafo(1, 0, 0, 1, 0, 0))
        pattern2 = ImageTilePattern(image, trafo = Trafo(2, 0, 1, 1, -100, 10))

        blended = pattern1.Blend(pattern2, 0.25, 0.75)
        self.assertEquals(blended.Image(), image)
        self.assertEquals(blended.Trafo(), Trafo(1.75, 0, 0.75, 1, -75, 7.5))


if __name__ == "__main__":
    unittest.main()
