# Skencil - A Python-based interactive drawing program
# Copyright (C) 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Align tool.

Test mouse interaction:
    Test selecting and deselecting with mouse clicks.
    Test clicking outside the selection but not on an object.
    Test selecting and deselecting with press-drag-release cycles.
    Test press-drag-release on an already selected object.
    Test clicking on the page border.
    Test clicking on handles.

Test commands:
    Test setting the reference rectangle.
    Test the align left command.
"""

import unittest

import support
support.add_sketch_dir_to_path()

import Sketch
from Sketch import Trafo, Identity, Point, Rectangle, UnionRects
from Sketch.Editor import EditorWithSelection, InteractiveDocument, Context, \
     Button1Mask, ShiftMask, ControlMask, HandleAlignL, Registry
from Sketch.Editor.aligntool import AlignToolInstance
from Sketch.Editor.selectiontool import SelectionToolInstance
from Sketch.Base.const import SelectAdd

from test_doceditor import MockCanvas, MockGC, EditorTransactionTest


#
# Helper classes
#

class MinimalCanvas(MockCanvas):

    def __init__(self, editor):
        # FIXME: it shouldn't be necessary to bind editor and document
        # here, but the align tool accesses them it because it passes
        # the canvas as the context parameter to a command.
        self.editor = editor
        self.document = editor.Document()

    def DocToWin(self, *args):
        return Identity.DocToWin(*args)


#
# Align tool tests
#
class AlignTest(unittest.TestCase):

    """Base class for the AlignTool tests.
    """

    def setUp(self):
        self.document = InteractiveDocument(create_layer = 1)
        self.layer = self.document.AppendLayer("test")
        self.editor = EditorWithSelection(self.document)

    def set_objects(self):
        # Insert some objects into the document
        self.r1 = Rectangle(Trafo(100, 0, 0, 100, 10, 10))
        self.r2 = Rectangle(Trafo(100, 0, 0, 100, 120, 240))
        self.r3 = Rectangle(Trafo(100, 0, 0, 100, 240, 120))
        self.r4 = Rectangle(Trafo(100, 0, 0, 100, 300, 300))

        self.document.Insert(self.r1, self.layer)
        self.document.Insert(self.r2, self.layer)
        self.document.Insert(self.r3, self.layer)
        self.document.Insert(self.r4, self.layer)

        # Select three rectangles, leave one (r4) unselected
        self.editor.SelectObject([self.r1, self.r2, self.r3])

    def set_application(self):
        self.appcontext = Context(None)
        self.appcontext.set_editor(self.editor)
        self.canvas = MinimalCanvas(self.editor)
        self.editor.SetTool('AlignTool')

    def tearDown(self):
        if self.editor is not None:
            self.editor.Destroy()
        self.editor = self.document = self.layer = self.canvas = None
        self.appcontext = None


class TestAlignToolInteraction(AlignTest):

    """Tests for the Align tool interaction: mouse clicks, commands.
    """

    def setUp(self):
        AlignTest.setUp(self)
        AlignTest.set_objects(self)
        AlignTest.set_application(self)

    def test_setup(self):
        """Test Align tool setup.
        """
        editor = self.editor
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(self.layer.GetObjects(), [self.r1, self.r2,
                                                     self.r3, self.r4])
        self.failUnless(isinstance(editor.Tool(), AlignToolInstance))

    #
    # Test mouse interaction - selecting and deselecting with clicks
    #

    def test_click_selected_simple(self):
        """Test Align tool mouse interaction: click on a selected object.

        As a result, the clicked object should become the last selected one.
        """
        editor = self.editor
        point_on_r2 = Point(150, 240)

        self.editor.ButtonClick(self.canvas, point_on_r2, 1, 0)
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r2.coord_rect)

    def test_click_selected_add(self):
        """Test Align tool mouse interaction: shift-click a selected object.

        As a result, the clicked object should become the last selected one.
        """
        editor = self.editor
        point_on_r2 = Point(150, 240)

        self.editor.ButtonClick(self.canvas, point_on_r2, 1, ShiftMask)
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r2.coord_rect)

    def test_click_selected_remove(self):
        """Test Align tool mouse interaction: control-click a selected object.

        As a result, the clicked object should be deselected.
        """
        editor = self.editor
        point_on_r2 = Point(150, 240)

        self.editor.ButtonClick(self.canvas, point_on_r2, 1, ControlMask)
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r3])
        self.assertEquals(editor.tool.rectangle.coord_rect,
                          UnionRects(self.r1.coord_rect, self.r3.coord_rect))

    def test_click_unselected_simple(self):
        """Test Align tool mouse interaction: click an unselected object.

        As a result, the selection should contain only the clicked object.
        """
        editor = self.editor
        point_on_r4 = Point(350, 300)

        self.editor.ButtonClick(self.canvas, point_on_r4, 1, 0)
        self.assertEquals(editor.LastSelectedObjects(), [self.r4])
        self.assertEquals(editor.SelectedObjects(), [self.r4])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r4.coord_rect)

    def test_click_unselected_add(self):
        """Test Align tool mouse interaction: shift-click an unselected object.

        As a result, the object should be added to the selection.
        """
        editor = self.editor
        point_on_r4 = Point(350, 300)

        self.editor.ButtonClick(self.canvas, point_on_r4, 1, ShiftMask)
        self.assertEquals(editor.LastSelectedObjects(), [self.r4])
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3, self.r4])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r4.coord_rect)

    def test_click_unselected_remove(self):
        """Test Align tool mouse interaction: ctrl-click an unselected object.

        As a result, nothing should change.
        """
        editor = self.editor
        point_on_r4 = Point(350, 300)

        self.editor.ButtonClick(self.canvas, point_on_r4, 1, ControlMask)

        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r2,
                                                     self.r3])

    def test_click_outside_selection(self):
        """Test Align tool mouse interaction: click outside the selection.

        As a result, the selection is unchanged and the Selection tool is
        activated.
        """
        editor = self.editor
        # Click somewhere not on an object.
        self.editor.ButtonClick(self.canvas, Point(500,500), 1, 0)

        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.failUnless(isinstance(editor.Tool(), SelectionToolInstance))

    #
    # Test mouse interaction - selecting and deselecting with
    # press-drag-release cycles.
    #

    def test_drag_select(self):
        """Test Align tool mouse interaction: select with press-drag-release.
        """
        editor = self.editor
        # Select r1 and r2 with rubber banding.
        editor.ButtonPress(self.canvas, Point(-10, -10), 1, 0)
        editor.PointerMotion(self.canvas, Point(220, 340), Button1Mask)
        editor.ButtonRelease(self.canvas, Point(250, 370), 1, Button1Mask)

        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2])
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r2])

    def test_drag_reselect(self):
        """Test Align tool mouse interaction: reselect with press-drag-release.
        """
        editor = self.editor
        # Reselect r1 and r2 with rubber banding.
        editor.ButtonPress(self.canvas, Point(-10, -10), 1, ShiftMask)
        editor.PointerMotion(self.canvas, Point(220, 340), ShiftMask)
        editor.ButtonRelease(self.canvas, Point(250, 370), 1, ShiftMask)

        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        # r1 and r2 should be the last selected objects
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r2])

    def test_drag_deselect(self):
        """Test Align tool mouse interaction: deselect with press-drag-release.
        """
        editor = self.editor
        # Deselect r1 and r2 with rubber banding.
        editor.ButtonPress(self.canvas, Point(-10, -10), 1, ControlMask)
        editor.PointerMotion(self.canvas, Point(220, 340), ControlMask)
        editor.ButtonRelease(self.canvas, Point(250, 370), 1, ControlMask)

        # Now the selection should contain only r3
        self.assertEquals(editor.SelectedObjects(), [self.r3])
        self.assertEquals(editor.LastSelectedObjects(), [self.r3])

    def test_click_page_border(self):
        """Test Align tool mouse interaction: click on page border.

        This page rectangle should become the reference.
        """
        editor = self.editor
        editor.ButtonClick(self.canvas, Point(0, 0), 1, 0)

        self.assertEquals(editor.tool.rectangle.coord_rect,
                          self.document.PageRect())

    #
    # Test mouse interaction - clicking on handles
    #

    def click_handle(self):
        """Test Align tool mouse interaction: click on a handle.

        The handle is the 'align left' one, so the selected objects should be
        aligned to the left of the selection.
        """
        editor = self.editor
        left = editor.Selection().coord_rect.left
        # Get the "align left" handle
        handle = [h for h in editor.Handles() if h.type == HandleAlignL][0]

        self.editor.ButtonClick(self.canvas, handle.p, 1, 0, handle = handle)
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.LastSelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        # The selected objects should be aligned to the left
        self.assertEquals(self.r1.coord_rect.left, left)
        self.assertEquals(self.r2.coord_rect.left, left)
        self.assertEquals(self.r3.coord_rect.left, left)

    def test_command_set_reference(self):
        """Test Align tool commands: set the reference rectangle.
        """
        cmd = Registry.Command('set_align_reference_page')
        cmd.Execute(self.appcontext)
        self.assertEquals(self.editor.Tool().ref_rect,
                          self.document.PageRect())

    def test_command_align(self):
        """Test Align tool commands: align left.
        """
        cmd = Registry.Command('align_left')
        cmd.Execute(self.appcontext)

        editor = self.editor

        # Test whether objects are aligned:
        left = editor.Selection().coord_rect.left
        self.assertEquals(self.r1.coord_rect.left, left)
        self.assertEquals(self.r2.coord_rect.left, left)
        self.assertEquals(self.r3.coord_rect.left, left)


class AlignToolTransactionTest(EditorTransactionTest, AlignTest):

    def setUp(self):
        EditorTransactionTest.setUp(self)
        AlignTest.set_objects(self)
        AlignTest.set_application(self)
        self.editor.SelectObject([self.r2], SelectAdd)


class TestAlignToolTransactionClick(AlignToolTransactionTest):

    def action(self):
        editor = self.editor
        # Click on the "align left" handle.
        handle = [h for h in editor.Handles() if h.type == HandleAlignL][0]
        editor.ButtonClick(self.canvas, handle.p, 1, 0, handle = handle)

    def check_before(self, after_undo):
        editor = self.editor
        self.failUnless(isinstance(editor.Tool(), AlignToolInstance))
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r2.coord_rect)

    def check_after(self, after_redo):
        editor = self.editor
        self.failUnless(isinstance(editor.Tool(), AlignToolInstance))
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r2.coord_rect)


class TestAlignToolTransactionCommand(AlignToolTransactionTest):

    def action(self):
        # simulate pressing l
        cmd = Registry.Command('align_left')
        cmd.Execute(self.appcontext)

    def check_before(self, after_undo):
        editor = self.editor
        self.failUnless(isinstance(editor.Tool(), AlignToolInstance))
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r2.coord_rect)

    def check_after(self, after_redo):
        editor = self.editor
        self.failUnless(isinstance(editor.Tool(), AlignToolInstance))
        self.assertEquals(editor.LastSelectedObjects(), [self.r2])
        self.assertEquals(editor.SelectedObjects(), [self.r1, self.r2,
                                                     self.r3])
        self.assertEquals(editor.tool.rectangle.coord_rect, self.r2.coord_rect)


if __name__ == "__main__":
    unittest.main()

