# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test cases for loading sketch files written by Sketch 0.6.x
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import sys
import unittest
import cStringIO as StringIO

import support
support.add_sketch_dir_to_path()
support.init_plugins()

from Sketch.Plugin import load, plugins
from Sketch.Base import SketchLoadError

class SKFileTest(support.FileLoadTestCase):

    """Base class for sketch file tests

    Basically the same as the FileLoadTestCase, except that all sketch
    file tests use the '.sk' extension by default.

    Furthermore, sub-classes can set suppress_stderr to true to install
    the test case itself as sys.stderr. In this case all strings written
    to stderr between setUp and tearDown will be collected in
    self.messages.
    """

    file_extension = ".sk"

    suppress_stderr = 0

    def setUp(self):
        """Extend the inherited method to substitute sys.stderr

        Ideally it should be possible to suppress the warnings that the
        skloader prints without having to use a custom sys.stderr.
        """
        support.FileLoadTestCase.setUp(self)
        if self.suppress_stderr:
            # List to accumulate everything printed to stderr
            self.messages = []
            sys.stderr = self

    def tearDown(self):
        """Restore sys.stderr"""
        if self.suppress_stderr:
            sys.stderr = sys.__stderr__

    def write(self, string):
        """Append the single string argument to self.messages.

        This method make the test case look sufficiently like a file
        object to make it usable as sys.stdout.
        """
        self.messages.append(string)


class test_long_line_after_bezier_object(SKFileTest):


    """Test sk1 file with a long line after a bezier object.

    There was a bug in curve_append_from_file function in
    curveobject.c which didn't handle lines longer than about 500
    chars following the last of the bc or bs lines of a bezier
    curve.
    """

    suppress_stderr = 1
    file_contents = """\
##Sketch 1 2
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
lw(1)
b()
bs(90.468,678.721,0)
bc(141.074,713.513,175.866,691.373,232.798,675.558,2)
bc(289.731,659.744,337.174,681.884,351.407,697.698,2)
PC('SketchTestPluginObject', '%s')
r(10, 0, 0, 10, 0, 0)
PC_()
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
grid((0,0,20,20),0,(0,0,1),'Grid')
""" % ("#" * 600)

    def test(self):
        """Test SK1 file with a long line after a bezier object."""
        try:
            doc = load.load_drawing(self.filename())
        except SketchLoadError:
            self.fail("File with long line after Bezier curve"
                      " could not be loaded:\n%s" % "".join(self.messages))


class test_stringio(SKFileTest):

    """Test loading a sk1 file from a StringIO object
    """

    suppress_stderr = 1
    file_contents = """\
##Sketch 1 2
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
lw(1)
b()
bs(90.5,678.0,0)
bc(141.25,713.75,175.125,691.0,232.625,675.625,2)
bc(289.0,659.25,337.5,681.25,351.875,697.0,2)
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
grid((0,0,20,20),0,(0,0,1),'Grid')
"""

    def test(self):
        """Test loading a sk1 file from a StringIO object."""
        # event though there was a very strict check whether the object
        # passed in as a file object was really an instance of the
        # built-in file object (which neither StringIO isn't) loading
        # from a string IO still succeeded correctly because of the way
        # the error in append_from_file was handled.  The only noticable
        # problem are error messages.  So while we check whether the
        # file was loaded correctly, we also check that no error
        # messages have occurred during the load.
        #
        # Note that this test only tests whether a drawing with bezier
        # objects is loaded correctly.  There are still other places
        # where a real file object is required, e.g. when reading a file
        # with an embedded image.
        doc = load.load_drawing_from_file(StringIO.StringIO(self.file_contents))
        self.assertEquals(doc[0][0].Paths()[0].get_save(),
                 [(90.5, 678.0, 0),
                  (141.25, 713.75, 175.125, 691.0, 232.625, 675.625, 2),
                  (289.0, 659.25, 337.5, 681.25, 351.875, 697.0, 2)])
        self.assertEquals(self.messages, [])


class test_empty_plugin_object(SKFileTest):


    """Test sk1 file with an empty plugin object

    Empty plugin objects caused an EmptyComposite error. This could
    happen with the multiline text object if it doesn't contain any
    text.
    """

    suppress_stderr = 1
    file_extension = ".sk"
    file_contents = """\
##Sketch 1 2
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
PC('SketchTestPluginObject', 'dummy parameter')
PC_()
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
grid((0,0,20,20),0,(0,0,1),'Grid')
"""

    def test(self):
        """Test SK1 file with an empty plugin object."""
        try:
            doc = load.load_drawing(self.filename())
        except SketchLoadError:
            self.fail("File with long line after Bezier curve"
                      " could not be loaded:\n%s" % "".join(self.messages))
        # The empty plugin object should not show up in the document
        # because it's empty and therefore invisible. The layer that
        # would be containing the plugin object is the zeroth one.
        self.assertEquals(doc[0].GetObjects(), [])


class TestStyleProperties(unittest.TestCase):

    def test_make_base_style_missing_properties(self):
        """Test whether make_base_style is missing some properties

        When new properties are added to Skencil the pluging filters may
        have to be updated to provide suitable values for them.  This
        test is useful to notice whether the sk1 filter is missing any
        properties.
        """
        format = "SK-1"
        for plugin in plugins.import_plugins:
            if plugin.format_name == format:
                make_base_style = plugin.load_module().make_base_style
                try:
                    make_base_style(missing_attributes_are_errors = True)
                except SketchLoadError, val:
                    self.fail(str(val))
                break
        else:
            self.fail("No plugin for format %r" % format)


if __name__ == "__main__":
    unittest.main()
