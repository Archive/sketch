# Sketch - A Python-based interactive drawing program
# Copyright (C) 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the skread module.
"""

# TODO: Add tests for parse_sk_line

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support

support.add_sketch_module_dir_to_path()


import skread
parse2 = skread.parse_sk_line2


class SKReadTest(support.FPTestCase):

    """Test cases for the skread functions"""

    def run_parse2_test(self, line, expected_name, expected_args,
                        expected_kwargs):
        """Check a call to parse_sk_line2

        Assert that parsing the line line with parse_sk_line2 yields the
        given name, arguments and keyword arguments.
        """
        eq = self.assertEquals
        name, args, kwargs = parse2(line)
        eq(name, expected_name)
        eq(args, expected_args)
        eq(kwargs, expected_kwargs)

    def test_parse_sk_line2(self):
        """Test skread.parse_sk_line2"""
        test = self.run_parse2_test

        # an empty argument list
        test("func()", "func", (), {})

        # strings (use rawstring to preserve the backslashes)
        test('String("abc")', "String", ("abc",), {})
        # string, single quotes, NUL ()
        test(r"single_quotes('a b \000 c')",
             "single_quotes", ('a b \000 c',), {})
        # string, hex escapes (rawstring to preserve the backslash)
        test(r"XyZzy('\xe4')", "XyZzy", ('\xE4',), {})
        # normal escapes
        test(r"escape('\a\b\f\r\n\t\v\\\'\"')",
             "escape", ('\a\b\f\r\n\t\v\\\'\"',), {})

        # an int argument
        test('_int(898)', "_int", (898,), {})

        # a float argument
        test('FLO_at(3.14159)', "FLO_at", (3.14159,), {})

        # lists
        test("empty_list([])", "empty_list", ([],), {})
        test(r"list([123, '\'\t', 2.5])", "list", ([123, '\'\t', 2.5],), {})
        test("nested([[1,2,3], 999])", "nested", ([[1,2,3], 999],), {})

        # tuples
        test("empty(())", "empty", ((),), {})
        test(r"tuple((123, '\'\r\n', 2.5))",
             "tuple", ((123, '\'\r\n', 2.5),), {})
        test("nested(((1,2,3), 999))", "nested", (((1,2,3), 999),), {})
        

        # a keyword argument
        test('kw(length = 10)', "kw", (), {"length": 10})

        # several keyword arguments
        test('multikw(a= 1.0, _ =432, parrot = "dead")',
             "multikw", (), {"a": 1.0, "_": 432, 'parrot': "dead"})

    def test_parse_sk_line2_errors(self):
        """Test skread.parse_sk_line2 exceptions"""
        # test various syntax errors
        exc = self.assertRaises

        # missing (
        exc(SyntaxError, parse2, 'func')
        # missing )
        exc(SyntaxError, parse2, 'func(')

        # incomplete string
        exc(SyntaxError, parse2, 'func(")')

        # embedded newline string (be careful with the escapes here)
        exc(SyntaxError, parse2, 'func("\n")')

        # missing function name
        exc(SyntaxError, parse2, '("abc")')

    def test_tokenize_line(self):
        """Test skread.tokenize_line"""
        eq = self.assertEquals

        # Test what's needed for the XFig loader where we only have
        # numbers (ints and floats)
        eq(skread.tokenize_line("1 2 0.000"), [1, 2, 0.0])

        # Test what's needed for parsing SVG paths. The special problem
        # there is that signed numbers might follow a previous number
        # without whitespace or other tokens inbetween. The commas are
        # not returned as tokens from the tokenize_line.
        eq(skread.tokenize_line("1-5"), [1, -5])
        eq(skread.tokenize_line("0+10.25-5.5,3.125, 8"),
           [0, 10.25, -5.5, 3.125, 8])

if __name__ == "__main__":
    unittest.main()
