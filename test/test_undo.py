# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test Sketch.Base.undo
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Base import Undo, CreateListUndo, CreateMultiUndo, UndoAfter,\
     UndoRedo, NullUndo

class TestUndo(unittest.TestCase):

    def test_undo_notext_noargs(self):
        """Test undo no text no args"""
        # state is manipulated by forward and backward which
        # append/remove objects from state
        state = []
        def forward():
            state.append(0)
            return (backward,)
        def backward():
            state.pop()
            return (forward,)

        # sanity check.  At first state is empty
        self.assertEquals(len(state), 0)

        # forward.  We get an undo object.  check whether state has been
        # manipulated
        undo_info = forward()
        self.assertEquals(len(state), 1)

        # Now do the undo.  We get a redo object back.  State must have
        # been reverted
        redo_info = Undo(undo_info)
        self.assertEquals(len(state), 0)

        # No undo the undo
        undo_info = Undo(redo_info)
        self.assertEquals(len(state), 1)


    def test_undo_notext_args(self):
        """Test undo no text but with args"""
        # state is manipulated by forward and backward which
        # append/remove objects from state
        state = []
        def forward(obj):
            state.append(obj)
            return (backward,)
        def backward():
            return (forward, state.pop())

        # sanity check.  At first state is empty
        self.assertEquals(state, [])

        # forward.  We get an undo object.  check whether state has been
        # manipulated
        undo_info = forward(1)
        self.assertEquals(state, [1])

        # Now do the undo.  We get a redo object back.  State must have
        # been reverted
        redo_info = Undo(undo_info)
        self.assertEquals(state, [])

        # No undo the undo
        undo_info = Undo(redo_info)
        self.assertEquals(state, [1])

    def test_undo_text_args(self):
        """Test undo with text and args"""
        # state is manipulated by forward and backward which
        # append/remove objects from state
        state = []
        def forward(obj):
            state.append(obj)
            return (backward,)
        def backward():
            return (forward, state.pop())

        # sanity check.  At first state is empty
        self.assertEquals(state, [])

        # forward.  We get an undo object.  check whether state has been
        # manipulated
        undo_info = forward(1)
        self.assertEquals(state, [1])
        # add the text.
        undo_info = ("add object",) + undo_info

        # Now do the undo.  We get a redo object back.  State must have
        # been reverted.  The text added to undo_info must also be in
        # redo_info
        redo_info = Undo(undo_info)
        self.assertEquals(state, [])
        self.assertEquals(redo_info[0], "add object")

        # No undo the undo.  The text added from redo_info must also be
        # in the new undo_info
        undo_info = Undo(redo_info)
        self.assertEquals(state, [1])
        self.assertEquals(undo_info[0], "add object")


class TestNullUndo(unittest.TestCase):

    def test_null_undo(self):
        """Test NullUndo.  Undoing NullUndo yields NullUndo"""
        self.assertEquals(Undo(NullUndo), NullUndo)


class TestMultiUndo(unittest.TestCase):

    """Tests for CreateMultiUndo"""

    def test_multi_undo(self):
        """Test CreateMultiUndo"""
        # Make sure to design the undo/redo operations so that it fails
        # to work if the undo doesn't perform the action in the exact
        # reverse order.  We do that here by giving the index of the
        # object to remove to backward().
        state = []
        def forward(obj):
            state.append(obj)
            return (backward, len(state) - 1)
        def backward(index):
            return (forward, state.pop(index))

        # Sanity check. state is empty at the start
        self.assertEquals(state, [])

        # Do something and check the state afterwards
        undo_info = CreateMultiUndo(forward(1), forward(2), forward(3))
        self.assertEquals(state, [1, 2, 3])

        # Now undo
        redo_info = Undo(undo_info)
        self.assertEquals(state, [])

        # Now undo the undo
        undo_info = Undo(redo_info)
        self.assertEquals(state, [1, 2, 3])

    def test_multi_undo_nested(self):
        """Test nested CreateMultiUndo

        Since CreateMultiUndo makes some optimizations when multi undos
        are nested, it's worth checking whether that works correctly
        """
        # Make sure to design the undo/redo operations so that it fails
        # to work if the undo doesn't perform the action in the exact
        # reverse order.  We do that here by giving the index of the
        # object to remove to backward().
        state = []
        def forward(obj):
            state.append(obj)
            return (backward, len(state) - 1)
        def backward(index):
            return (forward, state.pop(index))

        # Sanity check. state is empty at the start
        self.assertEquals(state, [])

        # Do something and check the state afterwards
        undo_info = CreateMultiUndo(forward(1),
                                    CreateMultiUndo(forward(2), forward(3)))
        self.assertEquals(state, [1, 2, 3])

        # Now undo
        redo_info = Undo(undo_info)
        self.assertEquals(state, [])

        # Now undo the undo
        undo_info = Undo(redo_info)
        self.assertEquals(state, [1, 2, 3])

    def test_multi_undo_of_null_undo(self):
        """Test CreateMultiUndo of NullUndo objects

        CreateMultiUndo is smart enough to remove NullUndo object and to
        return NullUndo if all undo infos are NullUndo.
        """
        self.assertEquals(CreateMultiUndo(NullUndo, NullUndo), NullUndo)

    def test_list_undo(self):
        """Test CreateListUndo

        Since CreateMultiUndo is implemented in terms of CreateListUndo
        that is inderectly tested as well in the other methods.  Here we
        only do some simple checks of CreateListUndo.
        """
        # Make sure to design the undo/redo operations so that it fails
        # to work if the undo doesn't perform the action in the exact
        # reverse order.  We do that here by giving the index of the
        # object to remove to backward().
        state = []
        def forward(obj):
            state.append(obj)
            return (backward, len(state) - 1)
        def backward(index):
            return (forward, state.pop(index))

        # Sanity check. state is empty at the start
        self.assertEquals(state, [])

        # Do something and check the state afterwards
        undo_info = CreateListUndo([forward(1), forward(2), forward(3)])
        self.assertEquals(state, [1, 2, 3])

        # Now undo
        redo_info = Undo(undo_info)
        self.assertEquals(state, [])

        # Now undo the undo
        undo_info = Undo(redo_info)
        self.assertEquals(state, [1, 2, 3])



class TestUndoAfter(unittest.TestCase):

    def test_undo_after(self):
        """Test UndoAfter"""
        # Be careful when changing something here that it makes a
        # difference whether the undo indo returned by after is undone
        # after the undo info returned by forward.
        state = []
        def forward(obj):
            state.append(obj)
            return (backward, len(state) - 1)
        def backward(index):
            return (forward, state.pop(index))
        state_seen_in_after = []
        def after():
            state_seen_in_after.append(state[:])
            return (after,)

        # Do it.  After records a copy of state
        undo_info = (UndoAfter, forward(1), after())
        self.assertEquals(state_seen_in_after, [[1]])
        del state_seen_in_after[:]

        # Undo it.  The state seen by after must be the state after
        # undoing forward
        redo_info = Undo(undo_info)
        self.assertEquals(state_seen_in_after, [[]])
        del state_seen_in_after[:]

        # Redo it.  The state seen by after must be the state after
        # undoing forward
        undo_info = Undo(redo_info)
        self.assertEquals(state_seen_in_after, [[1]])
        del state_seen_in_after[:]



class EditableObject:

    """Class with undoable methods"""

    def __init__(self):
        self.state = []

    def append(self, item):
        self.state.append(item)
        return ("append", self.pop, len(self.state) - 1)

    def insert(self, index, item):
        self.state.insert(index, item)
        return ("insert", self.pop, index)

    def pop(self, index):
        return ("pop", self.insert, index, self.state.pop(index))


class TestUndoRedo(unittest.TestCase):

    def test_undo_redo_default_state(self):
        """Test UndoRedo default state"""
        undo_redo = UndoRedo()

        self.failIf(undo_redo.CanUndo())
        self.failIf(undo_redo.CanRedo())
        self.failIf(undo_redo.WasEdited())
        self.assertEquals(undo_redo.UndoText(), "Undo")
        self.assertEquals(undo_redo.RedoText(), "Redo")

    def test_undo_redo_simple(self):
        """Test UndoRedo simple"""
        editable = EditableObject()
        undo_redo = UndoRedo()

        # Change editable and add the undo info to undo_redo.  The check
        # the state of undo_redo
        undo_redo.AddUndo(editable.append(1))
        self.assert_(undo_redo.CanUndo())
        self.failIf(undo_redo.CanRedo())
        self.assert_(undo_redo.WasEdited())
        self.assertEquals(undo_redo.UndoText(), "Undo append")
        self.assertEquals(undo_redo.RedoText(), "Redo")

        # now undo and check again
        undo_redo.Undo()
        self.failIf(undo_redo.CanUndo())
        self.assert_(undo_redo.CanRedo())
        self.failIf(undo_redo.WasEdited())
        self.assertEquals(undo_redo.UndoText(), "Undo")
        self.assertEquals(undo_redo.RedoText(), "Redo pop")

        # Now redo and check again
        undo_redo.Redo()
        self.assert_(undo_redo.CanUndo())
        self.failIf(undo_redo.CanRedo())
        self.assert_(undo_redo.WasEdited())
        self.assertEquals(undo_redo.UndoText(), "Undo insert")
        self.assertEquals(undo_redo.RedoText(), "Redo")


    def test_undo_redo_unlimited(self):
        """Test UndoRedo with unlimited undo"""
        editable = EditableObject()
        undo_redo = UndoRedo(None)

        # Sanity check: editable.state must be empty at first
        self.assertEquals(editable.state, [])

        # We can't really test unlimited undo, but we can choose a
        # reasonable large number of steps, say 1000.
        for i in range(1000):
            undo_redo.AddUndo(editable.append(i))

        self.assertEquals(editable.state, range(1000))

        # Now undo while we can undo
        while undo_redo.CanUndo():
            undo_redo.Undo()

        # Now we should be back where we started.
        self.assertEquals(editable.state, [])

    def test_undo_redo_limited(self):
        """Test UndoRedo with limited undo"""
        editable = EditableObject()
        undo_redo = UndoRedo(5)

        # Sanity check: editable.state must be empty at first
        self.assertEquals(editable.state, [])

        # Do more than 5 operations, undo all that we can undo and check
        # the state.  We should have 5 left.
        for i in range(10):
            undo_redo.AddUndo(editable.append(i))
        self.assertEquals(editable.state, range(10))
        while undo_redo.CanUndo():
            undo_redo.Undo()
        self.assertEquals(editable.state, range(5))

    def test_undo_redo_reset(self):
        """Test UndoRedo.Reset()"""
        editable = EditableObject()
        undo_redo = UndoRedo(5)

        # Sanity check: editable.state must be empty at first
        self.assertEquals(editable.state, [])

        # Do 10 operations.  Then undo one.  Reset.  After that we can't
        # undo nor redo.
        for i in range(10):
            undo_redo.AddUndo(editable.append(i))
        self.assertEquals(editable.state, range(10))

        undo_redo.Undo()
        undo_redo.Reset()

        self.failIf(undo_redo.CanUndo())
        self.failIf(undo_redo.CanRedo())
        self.assert_(undo_redo.WasEdited())
        self.assertEquals(undo_redo.UndoText(), "Undo")
        self.assertEquals(undo_redo.RedoText(), "Redo")

        # Now do more than 5 further operations to check whether the
        # undo limit has stayed the same.  Undo as much as we can
        # afterwards and check state.
        for i in range(9, 20):
            undo_redo.AddUndo(editable.append(i))
        self.assertEquals(editable.state, range(20))
        while undo_redo.CanUndo():
            undo_redo.Undo()
        self.assertEquals(editable.state, range(15))


    def test_undo_redo_mark_as_saved(self):
        """Test UndoRedo.MarkAsSaved()"""
        editable = EditableObject()
        undo_redo = UndoRedo(5)

        # Sanity check: editable.state must be empty at first
        self.assertEquals(editable.state, [])

        # Do 10 operations.  Then undo some and MarkAsSaved
        for i in range(10):
            undo_redo.AddUndo(editable.append(i))
        self.assertEquals(editable.state, range(10))
        undo_redo.Undo()
        undo_redo.Undo()
        undo_redo.MarkAsSaved()

        self.assert_(undo_redo.CanUndo())
        self.assert_(undo_redo.CanRedo())
        self.failIf(undo_redo.WasEdited())
        self.assertEquals(undo_redo.UndoText(), "Undo append")
        self.assertEquals(undo_redo.RedoText(), "Redo pop")

        # Now redo.  WasEdited should return true again
        undo_redo.Redo()
        self.assert_(undo_redo.WasEdited())

        # Now undo again. WasEdited should return false after the first
        # undo, and true after the second
        undo_redo.Undo()
        self.failIf(undo_redo.WasEdited())
        undo_redo.Undo()
        self.assert_(undo_redo.WasEdited())


if __name__ == "__main__":
    unittest.main()
