# Skencil - A Python-based interactive drawing program
# Copyright (C)  by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

"""
Test for Sketch.Graphics.Document
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import unittest

import support
support.add_sketch_dir_to_path()

from Sketch import Point
from Sketch.Graphics import Document


class TestDocument(unittest.TestCase):

    def test_add_guide_line_horizontal(self):
        """Test Document.AddGuideLine(horizontal)"""
        document = Document()
        document.AddGuideLine(Point(123, 321), True)

        lines = document.GuideLines()
        self.assertEquals(len(lines), 1)
        guide_line = lines[0]
        self.assertEquals(guide_line.Coordinates(), (321, 1))
        self.failUnless(guide_line.Horizontal())

    def test_add_guide_line_vertical(self):
        """Test Document.AddGuideLine(horizontal)"""
        document = Document()
        document.AddGuideLine(Point(123, 321), False)

        lines = document.GuideLines()
        self.assertEquals(len(lines), 1)
        guide_line = lines[0]
        self.assertEquals(guide_line.Coordinates(), (123, 0))
        self.failIf(guide_line.Horizontal())

    def test_remove_guide_line(self):
        """Test Document.RemoveGuideLine()"""
        document = Document()
        document.AddGuideLine(Point(123, 321), True)
        lines = document.GuideLines()
        self.assertEquals(len(lines), 1)
        guide_line = lines[0]

        # The operation to test
        document.RemoveGuideLine(guide_line)

        # The line should be gone now.
        self.assertEquals(document.GuideLines(), [])

        # FIXME: Also test whether this can be undone and redone.


if __name__ == "__main__":
    unittest.main()
