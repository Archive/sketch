# Skencil - A Python-based interactive drawing program
# Copyright (C) 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test Sketch.Editor.Connector
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Base.connector import Publisher
from Sketch.Editor import Context


class MockEditor(Publisher):

    def __init__(self, document, toolname):
        self.document = document
        self.toolname = toolname

    def Document(self):
        return self.document

class MockDocument(Publisher):

    def IncRef(self):
        pass
    DecRef = IncRef


class TestContext(unittest.TestCase):

    def test_tool_no_editor(self):
        """Test context.tool when no editor is active"""
        # The application is not used, so None should be OK
        context = Context(None)
        self.failUnless(context.tool is None)

    def test_tool_with_editor(self):
        document = MockDocument()
        editor = MockEditor(document, "a_tool")
        context = Context(None)
        context.set_editor(editor)
        self.assertEquals(context.tool, "a_tool")


if __name__ == "__main__":
    unittest.main()

