# Skencil - A Python-based interactive drawing program
# Copyright (C)  by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

"""
Test case for the MaskGroup
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Graphics import MaskGroup, Rectangle
from Sketch import Trafo

class TestMaskGroupRearrangement(unittest.TestCase):

    """Tests that check whether the mask always stays lowest

    The mask must always remain the lowermost object, so nothing must
    change in the mask group if we try to rearrange the children.
    """

    def setUp(self):
        self.mask = Rectangle(Trafo(100, 0, 0, 100, 0, 0))
        self.r1 =  Rectangle(Trafo(100, 0, 0, 100, 50, 60))
        self.r2 =  Rectangle(Trafo(100, 0, 0, 100, 55, 55))
        self.r3 =  Rectangle(Trafo(100, 0, 0, 100, 60, 50))

        self.mask_group = MaskGroup([self.mask, self.r1, self.r2, self.r3])

    def tearDown(self):
        self.mask_group = self.mask = self.r1 = self.r2 = self.r3 = None

    def test_sanity(self):
        """Test TestMaskGroupRearrangement initial state"""
        # Sanity check: are the children what we expect them to be?
        # Also, keep cildren reference for later
        self.assertEquals(self.mask_group.GetObjects(),
                          [self.mask, self.r1, self.r2, self.r3])
        self.failUnless(self.mask_group.Mask(), self.mask)
        self.assertEquals(self.mask_group.MaskedObjects(),
                          [self.r1, self.r2, self.r3])

    def run_rearrangement_test(self, method, objects, expected_masked_objects):
        getattr(self.mask_group, method)(objects)

        self.assertEquals(self.mask_group.GetObjects(),
                          [self.mask] + expected_masked_objects)
        self.failUnless(self.mask_group.Mask(), self.mask)
        self.assertEquals(self.mask_group.MaskedObjects(),
                          expected_masked_objects)

    def test_move_to_top_mask(self):
        """Test MaskGroup.MoveObjectsToTop([mask])

        If only the mask is to be moved to the top, nothing should
        change.
        """
        self.run_rearrangement_test("MoveObjectsToTop",
                                    [self.mask],
                                    [self.r1, self.r2, self.r3])

    def test_move_to_top_mask_and_other(self):
        """Test MaskGroup.MoveObjectsToTop([mask, other])

        If the mask and another, masked, object is to be moved to the
        top, the mask should stay at the bottom, but the other object
        should move to the top.
        """
        self.run_rearrangement_test("MoveObjectsToTop",
                                    [self.mask, self.r1],
                                    [self.r2, self.r3, self.r1])

    def test_move_to_bottom_other(self):
        """Test MaskGroup.MoveObjectsToTop([not mask])

        If an object other than the mask is to be moved to the bottom,
        the mask should stay at the bottom, and the object should end up
        just above the mask.
        """
        self.run_rearrangement_test("MoveObjectsToBottom",
                                    [self.r3],
                                    [self.r3, self.r1, self.r2])

    def test_move_up_mask(self):
        """Test MaskGroup.MoveObjectsUp([mask])

        If only the mask is to be moved up, nothing should change.
        """
        self.run_rearrangement_test("MoveObjectsUp",
                                    [self.mask],
                                    [self.r1, self.r2, self.r3])

    def test_move_to_top_mask_and_other(self):
        """Test MaskGroup.MoveObjectsUp([mask, other])

        If the mask and another, masked, object is to be moved up, the
        mask should stay at the bottom, but the other object should move
        up.
        """
        self.run_rearrangement_test("MoveObjectsUp",
                                    [self.mask, self.r1],
                                    [self.r2, self.r1, self.r3])

    def test_move_down_other(self):
        """Test MaskGroup.MoveObjectsDown([not mask])

        If the the object just above the mask is to be moved down, the
        mask should stay at the bottom and the other object also stays
        where it is.
        """
        self.run_rearrangement_test("MoveObjectsDown",
                                    [self.r1],
                                    [self.r1, self.r2, self.r3])

if __name__ == "__main__":
    unittest.main()
