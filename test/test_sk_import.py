# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test cases for loading sketch files of the current sketch version
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import sys
import unittest
import cStringIO as StringIO

import support
support.add_sketch_dir_to_path()
support.init_plugins()


from Sketch.Plugin import load
from Sketch.Base import SketchLoadError, const
from Sketch.Graphics import EmptyPattern, SolidPattern, StandardColors

class SKFileTest(support.FileLoadTestCase):

    """Base class for sketch file tests

    Basically the same as the FileLoadTestCase, except that all sketch
    file tests use the '.sk' extension by default.
    """

    file_extension = ".sk"


class test_default_properties(SKFileTest):

    """Test whether the sk import filter uses the right default properties"""

    file_contents = """\
##Sketch 2 1
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
r((1,0,0,1,0,0))
layer_()
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
guidelayer_()
grid((0,0,20,20),0,(0,0,1),'Grid')
document_()
"""

    def test(self):
        doc = load.load_drawing(self.filename())
        rect = doc.Layers()[0][0]
        p = rect.Properties()
        self.assertEquals(p.fill_opacity, 1.0)
        self.assertEquals(p.fill_pattern, EmptyPattern)
        self.assertEquals(p.fill_transform, 1)
        self.assertEquals(p.fill_opacity, 1.0)
        self.assertEquals(p.line_pattern, SolidPattern(StandardColors.black))
        self.assertEquals(p.line_width, 0.0)
        self.assertEquals(p.line_join, const.JoinMiter)
        self.assertEquals(p.line_cap, const.CapButt)
        self.assertEquals(p.line_dashes, ())
        self.assertEquals(p.line_arrow1, None)
        self.assertEquals(p.line_arrow2, None)
        self.assertEquals(p.font, None)
        self.assertEquals(p.font_size, 12.0)


class test_fill_opacity(SKFileTest):

    """Test an sk file with fill opacity
    """

    file_contents = """\
##Sketch 2 1
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
fo(0.5)
lw(1)
r((1,0,0,1,0,0))
layer_()
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
guidelayer_()
grid((0,0,20,20),0,(0,0,1),'Grid')
document_()
"""

    def test(self):
        doc = load.load_drawing(self.filename())
        rect = doc.Layers()[0][0]
        self.assertEquals(rect.Properties().fill_opacity, 0.5)


class test_stringio(SKFileTest):

    """Test loading a sk1 file from a StringIO object
    """

    suppress_stderr = 1
    file_contents = """\
##Sketch 2 1
document()
layout('A4',0)
layer('Layer 1',1,1,0,0,(0,0,0))
lw(1)
b()
bs(90.5,678.0,0)
bc(141.25,713.75,175.125,691.0,232.625,675.625,2)
bc(289.0,659.25,337.5,681.25,351.875,697.0,2)
layer_()
guidelayer('Guide Lines',1,0,0,1,(0,0,1))
guidelayer_()
grid((0,0,20,20),0,(0,0,1),'Grid')
document_()
"""

    def test(self):
        """Test loading a .sk file from a StringIO object."""
        # At one point there was a very strict check in the
        # append_from_file method which only allowed instances of the
        # built-in file object and so neither StringIO implementation
        # was allowed.  Unlike the sk1 import filter, the one for the
        # new sk file format would treat it as a real error and not load
        # the drawing correctly, so that's what we test for here.
        #
        # Note that test only tests whether a drawing with bezier
        # objects is loaded correctly.  There are still other places
        # where a real file object is required, e.g. when reading a file
        # with an embedded image.
        doc = load.load_drawing_from_file(StringIO.StringIO(self.file_contents))
        self.assertEquals(doc[0][0].Paths()[0].get_save(),
                 [(90.5, 678.0, 0),
                  (141.25, 713.75, 175.125, 691.0, 232.625, 675.625, 2),
                  (289.0, 659.25, 337.5, 681.25, 351.875, 697.0, 2)])


if __name__ == "__main__":
    unittest.main()
