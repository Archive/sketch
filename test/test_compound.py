# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Sketch.Graphics.Compound class.
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import unittest

import support
support.add_sketch_dir_to_path()


from Sketch import Trafo, Point
from Sketch.Graphics import Compound, Rectangle
from Sketch.Graphics.base import RectangularPrimitive

class TestPrimitive(RectangularPrimitive):

    """GraphicsObject whose GetObjectHandle method uses the multiple argument
    """

    def GetObjectHandle(self, multiple):
        if multiple:
            return self.trafo(Point(0, 0))
        else:
            return [self.trafo(Point(0, 0)), self.trafo(Point(1, 1))]


class TestCompound(unittest.TestCase):

    def test_get_object_handle_single(self):
        """Test Compound.GetObjectHandle(False)

        Even when the argument multiple is False, the compound has to
        call the GetObjectHandle method of the children with it being
        True.
        """
        compound = Compound([TestPrimitive(Trafo(10, 0, 0, 10, 0, 0)),
                             TestPrimitive(Trafo(10, 0, 0, 10, 100, 100))])

        self.assertEquals(compound.GetObjectHandle(False),
                          [Point(0, 0), Point(100, 100)])

    def test_get_object_handle_multiple(self):
        """Test Compound.GetObjectHandle(True)

        When the argument multiple is True, the compound has to call the
        GetObjectHandle method of the children with it being True as
        well.
        """
        compound = Compound([TestPrimitive(Trafo(10, 0, 0, 10, 0, 0)),
                             TestPrimitive(Trafo(10, 0, 0, 10, 100, 100))])

        self.assertEquals(compound.GetObjectHandle(True),
                          [Point(0, 0), Point(100, 100)])



if __name__ == "__main__":
    unittest.main()

