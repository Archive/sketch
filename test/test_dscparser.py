# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test cases for the DSC parser in Sketch.Lib.dscparser
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Lib.dscparser import parse_eps_file

class DSCParserTest(support.FileLoadTestCase):

    file_extension = "eps"


class TestDSCParserEmbeddedRaster(DSCParserTest):

    file_contents="""\
%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: (atend)
%%EndComments

/buf 5 string def
5 5 scale
5 5 8 [5 0 0 5  0 0] { currentfile buf readhexstring pop } image
%%BeginData: 2 Hex Lines
0000000000000000000000000
0000000000000000000000000
%%EndData

%%Trailer
%%BoundingBox: 0 0 5 5
%%EOF
"""

    def test(self):
        info = parse_eps_file(self.filename())
        self.assertEquals(info.BoundingBox, (0, 0, 5, 5))
        self.assert_(info.atend)


if __name__ == "__main__":
    unittest.main()
