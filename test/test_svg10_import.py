# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for importing SVG 1.0 files
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import sys
import unittest

import support
support.add_sketch_dir_to_path()
support.init_plugins()
support.init_base()

from Sketch import Point, Trafo
from Sketch.Base import const
from Sketch.Plugin import load
from Sketch.Graphics import StandardColors


class SVGFileTest(support.FileLoadTestCase):

    """Base class for SVG file tests

    Basically the same as the FileLoadTestCase, except that all SVG file
    tests use the '.svg' extension by default.
    """

    file_extension = ".svg"


class test_svg_default_properties(SVGFileTest):

    file_contents = """\
<?xml version='1.0' encoding='iso-8859-1'?>
<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.0//EN'
              'http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd'>
<svg width='100' height='100'>
  <rect width='10' height='10'/>
</svg>
"""

    def test(self):
        """Test importing SVG 1.0: default properties"""
        doc = load.load_drawing(self.filename())
        rect = doc[0][0]
        prop = rect.Properties()
        self.failUnless(prop.fill_pattern.is_Solid)
        self.assertEquals(prop.fill_pattern.Color(), StandardColors.black)
        self.failUnless(prop.line_pattern.is_Empty)
        self.assertEquals(prop.line_width, 1.0)
        self.assertEquals(prop.line_cap, const.CapButt)
        self.assertEquals(prop.line_join, const.JoinMiter)
        self.assertEquals(prop.line_dashes, ())
        self.assertEquals(prop.line_arrow1, None)
        self.assertEquals(prop.line_arrow2, None)


class test_svg_path(SVGFileTest):

    file_contents = """\
<?xml version='1.0' encoding='iso-8859-1'?>
<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.0//EN'
              'http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd'>
<svg width='100' height='100'>
  <path d='
M 50.0 50.0
L 80 30
L 20 30 Z'/>
</svg>
"""

    def setUp(self):
        SVGFileTest.setUp(self)
        self.doc = load.load_drawing(self.filename())
        self.bezier = self.doc[0][0]
        self.path = self.bezier.Paths()[0]

    def test_path(self):
        """Test importing SVG: simple path"""
        self.assertEquals(self.path.NodeList(),
                          [Point(50, 50), Point(80, 70), Point(20, 70)])

    def test_path_closed(self):
        """Test importing SVG: simple path closed"""
        self.failUnless(self.path.closed)


class test_svg_transform_matrix(SVGFileTest):

    file_contents = """\
<?xml version='1.0' encoding='iso-8859-1'?>
<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.0//EN'
              'http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd'>
<svg width='100' height='100'>
  <rect width='1' height='1' transform='matrix(1.0,2e+0,3,4,5.0e1,6)'/>
</svg>
"""

    def test(self):
        """Test importing SVG 1.0: default properties"""
        doc = load.load_drawing(self.filename())
        rect = doc[0][0]
        self.assertEquals(rect.Trafo(),
                          Trafo(1, 0, 0, -1, 0, 100)(Trafo(1,2,3,4,50,6)))


if __name__ == "__main__":
    unittest.main()
