# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for exporting SVG 1.0 files
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest
from StringIO import StringIO

import xml.sax
import xml.sax.handler
from xml.sax import make_parser, ErrorHandler

import support
support.add_sketch_dir_to_path()
support.init_plugins()
support.init_base()

from Sketch.Plugin import plugins
from Sketch.Graphics import Document, StandardColors, SimpleText, \
     Style, SolidPattern, PolyBezier, EmptyPattern
from Sketch import CreatePath

class ContainsAll:

    """Helper class whose instances contain all objects.

    This is used by SaxEventLister.
    """

    def __contains__(self, obj):
        return 1


class SaxEventLister(xml.sax.handler.ContentHandler):

    def __init__(self, accepted_elements = None):
        """Initialize the event listener

        accepted_elements should be None (the default) or be a list of
        element names. If it's not None, only elements listed in
        accepted_elements are collected.
        """
        if accepted_elements is not None:
            self.accepted_elements = {}
            for element in accepted_elements:
                self.accepted_elements[element] = 1
        else:
            self.accepted_elements = ContainsAll()
        self.eventlist = []

    def startElement(self, name, attrs):
        if name in self.accepted_elements:
            items = attrs.items()
            items.sort()
            self.eventlist.append(("start", name, items))

    def endElement(self, name):
        if name in self.accepted_elements:
            self.eventlist.append(("end", name))


def sax_eventlist(file, accepted_elements = None):
    """Return a list of SAX event generated for the XML data.

    The return value is a list of tuples.  The first item of each tuple
    is a string indicating the event, and the rest are arguments.
    Possible event types are:

        start -- A start tag.  Arguments are the name of the start tag
                 an a sorted list of attribute value pairs

        end -- An end tag.  The only argument is the name of the tag

    The argument accepted_elements if given should be a list of element
    names.  Only events for elements in accepted_elements will be
    included in the event list.  If accepted_elements is not given or
    None, events for all elements are recorded.
    """
    handler = SaxEventLister(accepted_elements = accepted_elements)
    parser = make_parser()
    parser.setContentHandler(handler)
    parser.setErrorHandler(ErrorHandler())

    parser.parse(file)

    return handler.eventlist



class TestSVGExport(unittest.TestCase, support.VolatileFileMixin):

    def setUp(self):
        self.to_destroy = []

    def tearDown(self):
        for obj in self.to_destroy:
            obj.Destroy()
        self.to_destroy = None

    def check_xml_file(self, filename, testdata, accepted_elements = None):
        file = open(filename)
        contents = file.read()
        file.close()
        print contents

        inpsrc = xml.sax.InputSource()
        inpsrc.setByteStream(StringIO(testdata))
        self.assertEquals(sax_eventlist(filename), sax_eventlist(inpsrc))

    def test_text_with_outline(self):
        """Test SVG 1.0 export: document containing text with outline

        Currently (2003-04-12) Text in Sketch can't really have line
        properties and even if it actually does have line properties
        anyway -- due to e.g. dynamic styles -- they're
        ignored. Therefore they must be ignored in exports as well.
        """
        # Create a document containing one layer with a text object that
        # has a non-empty line_pattern. The only "legal" way to do that
        # is to use a dynamic style.
        doc = Document(create_layer = 1)
        self.to_destroy.append(doc)
        layer = doc.AppendLayer("A Layer")
        style = Style(line_pattern = SolidPattern(StandardColors.black),
                      line_width = 1.0)
        style = doc.add_dynamic_style("a dynamic style", style)
        text = SimpleText(text = "test")
        text.AddStyle(style)
        layer.Insert(text)

        # Write it to a file
        filename = self.volatile_file_name("text_with_line_property.svg")
        saver = plugins.find_export_plugin("SVG")
        saver(doc, filename)

        self.assertEquals(sax_eventlist(filename, accepted_elements=["text"]),
                          [('start', 'text',
                            [('style',
                              'stroke:none; fill:none;'
                              ' font-family:Times; font-size:12'),
                             ('transform', 'matrix(1 0 0 1 2 8.948)')
                             ]),
                           ('end', 'text')])


    def test_path_fill_rule(self):
        """Test SVG 1.0 export: correct fill-rule for paths

        All paths in Skencil currently use even-odd rule.  The default
        in SVG is nonzero (aka winding rule), though, so it has to be
        set explicitly.
        """
        doc = Document(create_layer = 1)
        self.to_destroy.append(doc)
        layer = doc.AppendLayer("A Layer")
        path = CreatePath()
        path.AppendLine(0, 0)
        path.AppendLine(10, 0)
        path.AppendLine(10, 10)
        path.AppendLine(0, 10)
        path.AppendLine(0, 0)
        path.ClosePath()
        bezier = PolyBezier(paths=(path,))
        bezier.SetProperties(line_pattern=EmptyPattern,
                             fill_pattern=SolidPattern(StandardColors.black))
        layer.Insert(bezier)

        # Write it to a file
        filename = self.volatile_file_name(self.id() + ".svg")
        saver = plugins.find_export_plugin("SVG")
        saver(doc, filename)

        # Take only the last two events because those are the events
        # that actually describe our path.  There are other path
        # elements at the beginning of the file with arrow descriptions
        # which are completely irrelevant for our test
        file_events = sax_eventlist(filename, accepted_elements=["path"])[-2:]
        self.assertEquals(file_events,
                          [('start', 'path',
                            [("d", "M 0 10L 10 10L 10 0L 0 0L 0 10z"),

                             # Only the fill-rule setting of the style
                             # really matters
                             ('style',
                              'stroke:none; fill-rule:evenodd; fill:#000000'),
                             ]),
                           ('end', 'path')])

if __name__ == "__main__":
    unittest.main()
