# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002, 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Main entry point for the Sketch test suite.

Just run this file as a python script to execute all tests
"""


import os, sys
import unittest
import getopt

import support
support.install_math_exception_simulation()

def main():
    """Run all the tests in the Sketch test suite"""

    verbosity = 1

    # Parse command line options.
    opts, args = getopt.getopt(sys.argv[1:], 'v', ['verbose'])
    for optchar, value in opts:
        if optchar in ("-v", "--verbose"):
            verbosity = 2
        else:
            print>>sys.stderr, "Unknown option", optchar

    # Build the list of test names.  If names were given on the command
    # line, run exactly those.  Othwerwise build a default list of
    # names from the files in the test directory
    if args:
        names = args
    else:
        # All Python files in the directory that contains this file and
        # whose names start with "test" contain contain test cases.
        directory = os.path.dirname(__file__)
        if not directory:
            # __file__ may not have a directroy component.  In that case
            # we should be in the directory containing the file, so use
            # the current directory
            directory = os.path.curdir
        files = os.listdir(directory)
        names = []
        for file in files:
            if file[:4] == "test" and file[-3:] == ".py":
                names.append(file[:-3])

    suite = unittest.defaultTestLoader.loadTestsFromNames(names)
    runner = unittest.TextTestRunner(verbosity = verbosity)
    result = runner.run(suite)
    sys.exit(not result.wasSuccessful())


if __name__ == "__main__":
    main()
