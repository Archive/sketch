# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Test Sketch.Base.undodict
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch.Base.undodict import UndoDict
from Sketch.Base import Undo, NullUndo

class NamedObject:

    """Helper object usable as value in UndoDict"""

    def __init__(self, name):
        self.SetName(name)

    def Name(self):
        return self.name

    def SetName(self, name):
        self.name = name


class TestUndoDict(unittest.TestCase):

    """Test cases for UndoDict

    The tests are primarily for the SetItem and DelItem methods, but the
    other methods are also called in the tests so they should be tested
    well enough too.
    """

    def test_instantiation(self):
        d = UndoDict()
        self.assertEquals(len(d), 0)

    def test_set_item_same_object_same_name(self):
        """Test UndoDict.SetItem() with item already bound to the key"""
        # This case is a no-op really.
        d = UndoDict()
        v = NamedObject("foo")
        d.SetItem("bar", v)

        # Now set it again.  Since it's a no-op we get NullUndo back.
        # It's not all that nice to explicitly check for NullUndo, but
        # it should be OK for now.
        self.assertEquals(d.SetItem("bar", v), NullUndo)


    def test_set_item_different_object_same_name(self):
        """Test UndoDict.SetItem() with item not the one already bound to key
        """
        # This case is a no-op really.
        d = UndoDict()
        v = NamedObject("foo")
        d.SetItem("bar", v)

        # Now assign a new object to "bar" too.  This should raise an
        # exception
        self.assertRaises(ValueError, d.SetItem, "bar", NamedObject("foo"))

    def test_set_item_new_undo(self):
        """Test UndoDict.SetItem() and with as yet unbound item

        The item is not yet in the dictionary and the key is not used
        yet.
        """
        d = UndoDict()
        v = NamedObject("foo")
        undo_info = d.SetItem("bar", v)

        self.assertEquals(len(d), 1)
        self.assertEquals(d.keys(), ["bar"])
        self.assertEquals(d.values(), [v])
        self.assertEquals(d.items(), [("bar", v)])
        self.assertEquals(d["bar"], v)
        self.assertEquals(d.get("bar"), v)
        self.assert_(d.has_key("bar"))
        self.assertEquals(v.Name(), "bar")

        # Now undo and check
        redo = Undo(undo_info)

        self.assertEquals(len(d), 0)
        self.assertEquals(d.keys(), [])
        self.assertEquals(d.values(), [])
        self.assertEquals(d.items(), [])
        self.assertRaises(KeyError, lambda name: d[name], "bar")
        self.assertEquals(d.get("bar"), None)
        self.failIf(d.has_key("bar"))
        # Not sure it's good that the name change of the item isn't
        # undone in this case, but that's the way it has been for a long
        # time.
        self.assertEquals(v.Name(), "bar")

        # Now redo and check again
        undo_info = Undo(redo)

        self.assertEquals(len(d), 1)
        self.assertEquals(d.keys(), ["bar"])
        self.assertEquals(d.values(), [v])
        self.assertEquals(d.items(), [("bar", v)])
        self.assertEquals(d["bar"], v)
        self.assertEquals(d.get("bar"), v)
        self.assert_(d.has_key("bar"))
        self.assertEquals(v.Name(), "bar")

    def test_set_item_rename(self):
        """Test UndoDict.SetItem() renaming an already bound object

        The item is already in the dictionary under a different name.
        The item is renamed and rebound to the new name.
        """
        d = UndoDict()

        # Initial state.  v bound to "bar" in d.
        v = NamedObject("foo")
        d.SetItem("bar", v)
        self.assertEquals(len(d), 1)
        self.assertEquals(d.keys(), ["bar"])
        self.assertEquals(d.values(), [v])
        self.assertEquals(d.items(), [("bar", v)])
        self.assertEquals(d["bar"], v)
        self.assertEquals(d.get("bar"), v)
        self.assert_(d.has_key("bar"))
        self.assertEquals(v.Name(), "bar")

        # Now rename to baz
        undo_info = d.SetItem("baz", v)
        self.assertEquals(len(d), 1)
        self.assertEquals(d.keys(), ["baz"])
        self.assertEquals(d.values(), [v])
        self.assertEquals(d.items(), [("baz", v)])
        self.assertEquals(d["baz"], v)
        self.assertEquals(d.get("baz"), v)
        self.assert_(d.has_key("baz"))
        self.assertEquals(v.Name(), "baz")

        # Now undo and check
        redo = Undo(undo_info)

        self.assertEquals(len(d), 1)
        self.assertEquals(d.keys(), ["bar"])
        self.assertEquals(d.values(), [v])
        self.assertEquals(d.items(), [("bar", v)])
        self.assertEquals(d["bar"], v)
        self.assertEquals(d.get("bar"), v)
        self.assert_(d.has_key("bar"))
        self.assertEquals(v.Name(), "bar")

        # Now redo and check again
        undo_info = Undo(redo)
        self.assertEquals(len(d), 1)
        self.assertEquals(d.keys(), ["baz"])
        self.assertEquals(d.values(), [v])
        self.assertEquals(d.items(), [("baz", v)])
        self.assertEquals(d["baz"], v)
        self.assertEquals(d.get("baz"), v)
        self.assert_(d.has_key("baz"))
        self.assertEquals(v.Name(), "baz")


    def test_del_item_existing(self):
        """Test UndoDict.DelItem() with an existing item"""
        d = UndoDict()
        v = NamedObject("foo")
        d.SetItem("bar", v)

        # Sanity check
        self.assertEquals(d.items(), [("bar", v)])

        # Delete
        undo_info = d.DelItem("bar")
        # It should be empty now
        self.assertEquals(d.items(), [])

        # Undo and check again
        redo = Undo(undo_info)
        self.assertEquals(d.items(), [("bar", v)])

        # redo and check
        undo_info = Undo(redo)
        self.assertEquals(d.items(), [])

    def test_del_item_non_existing(self):
        """Test UndoDict.DelItem() with an existing item

        In this case a KeyError should be raised.
        """
        d = UndoDict()
        v = NamedObject("foo")
        d.SetItem("bar", v)

        self.assertRaises(KeyError, d.DelItem, "baz")


if __name__ == "__main__":
    unittest.main()
