# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002, 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Support classes and function for the test suite
"""

__version__ = "$Revision$"

import os, sys
import unittest

def sketch_dir():
    """Return the directory containing the Sketch package"""
    thisdir = os.path.dirname(__file__)
    return os.path.join(thisdir, os.pardir)


def add_sketch_dir_to_path():
    """Insert the Sketch directory at the beginning of the python path.

    If it's already part of the path, remove later occurrences."""
    dir = sketch_dir()
    while 1:
        try:
            sys.path.remove(dir)
        except ValueError:
            break
    sys.path.insert(0, dir)


def sketch_module_dir():
    """Return the directory containing the Sketch C-modules"""
    thisdir = os.path.dirname(__file__)
    return os.path.join(thisdir, os.pardir, "Sketch", "Modules")

def add_sketch_module_dir_to_path():
    """Insert the Sketch modules directory at the beginning of the python path.

    If it's already part of the path, remove later occurrences."""
    dir = sketch_module_dir()
    while 1:
        try:
            sys.path.remove(dir)
        except ValueError:
            break
    sys.path.insert(0, dir)


_plugins_initialized = 0
def init_plugins():
    global _plugins_initialized
    if not _plugins_initialized:
        import Sketch.Plugin
        Sketch.Plugin.init([])
        _plugins_initialized = 1

_base_initialized = 0
def init_base():
    global _base_initialized
    if not _base_initialized:
        import Sketch.Base
        Sketch.Base.init(["sketch_test_case"])
        _base_initialized = 1


def install_math_exception_simulation():
    """Replace some functions in the math module with stricter versions

    The functions in the math module show some platform specific
    behavior since they are simply wrappers around the corresponding
    libc functions.  On some platforms they raise exceptions in cases
    where glibc does not, for instance.  This function installs versions
    of the problematic functions into the math module which test their
    arguments more strictly to simulate the behavior on other systems to
    make sure Skencil runs on those as well.

    This function must be called before the math module has been
    imported for the first time.  Otherwise it can happen that the
    standard versions of the functions are already bound in some
    modules, e.g. with 'from math import atan2'.
    """
    # First check that the math module has not been imported yet
    if sys.modules.has_key("math"):
        raise RuntimeError("math already imported")
    import math

    old_atan2 = math.atan2
    def strict_atan2(x, y):
        """Like math.atan2 but raise a value error if both arguments are 0

        The atan2 implementation in glibc just return 0 if both arguments
        are 0, but on some systems (such as Solaris) math.atan2 raises a
        ValueError.  This version always raises that error.
        """
        if x == 0 and y == 0:
            raise ValueError("math domain error: atan2 called with 0, 0")
        return old_atan2(x, y)
    math.atan2 = strict_atan2


class FPTestCase(unittest.TestCase):

    """
    TestCase class with a few additional methods for testing floating
    point operations and object identity
    """

    fp_epsilon = 1e-6

    def assert_float_equal(self, test, value):
        return self.assert_(self.fp_epsilon > abs(test - value))

    def assert_float_sequences_equal(self, test, value):
        for i in range(len(test)):
            self.assert_(self.fp_epsilon > abs(test[i] - value[i]))

    def assert_identity(self, test, value):
        return self.assertEquals(id(test), id(value))


class VolatileFileMixin:

    """Mixin class for tests that create volatile files

    In the context of this class, 'volatile' simply means that the files
    are created by the tests but are not necessarily deleted. They are
    not stored in CVS and they can be safely deleted after the tests
    have been run.

    Most test cases that use this functionality do not delete the files
    because it's often convenient to still have them around after the
    tests complete to help debugging.
    """

    def volatile_file_name(self, basename):
        """Return the full name of the file named basename in 'volatile'"""
        return os.path.join(self.volatile_dir(), basename)

    def volatile_dir(self):
        """Return the name of the directory for the volatile files.

        Create the directory if it doesn't exist yet. The volatile
        directory is a subdirectory named 'volatile' of the directory
        containing this mix-in class.
        """
        name = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                            "volatile"))

        # if the directory already exists, we're done
        if os.path.isdir(name):
            return name

        # create the directory
        os.mkdir(name)
        return name


class FileLoadTestCase(unittest.TestCase, VolatileFileMixin):

    """Base class for test case that test loading files.

    This base class provides some common infrastructure for testing the
    reading of files.

    Each test case should be its own class derived from this one. There
    is one file associated with each class. The contents are defined by
    the file_contents class attribute and its name by the filename
    method.

    Derived classes usually only have to provide appropriate values for
    the file_contents and file_extension class attributes.
    """

    file_contents = None
    file_extension = ""

    def filename(self):
        """Return the name of the test file to use.

        The default implementation simply calls self.volatile_file_name
        with a basename derived from the class name by stripping off a
        leading 'test_' and appending self.file_extension.
        """
        name = self.__class__.__name__
        if name.startswith("test_"):
            name = name[5:]
        return self.volatile_file_name(name + self.file_extension)

    def setUp(self):
        """Create the volatile file for the test.

        Write self.contents (which should be a string) to the file named
        by self.filename().
        """
        filename = self.filename()
        file = open(filename, "w")
        file.write(self.file_contents)
        file.close()
