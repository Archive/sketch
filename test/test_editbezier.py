# Skencil - A Python-based interactive drawing program
# Copyright (C) 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the Sketch.Editor.PolyBezierEditor class.
"""

import unittest

import support
support.add_sketch_dir_to_path()

from Sketch import Rect, EmptyRect, Point, CreatePath, ContAngle, ContSmooth, \
                   ContSymmetrical, Line, Bezier
from Sketch.Editor import PolyBezierEditor, Button1Mask, ShiftMask, \
     HandleNode
from Sketch.Editor.handle import Handle,  MakeNodeHandle
from Sketch.Base.const import SelectSet, SelectAdd
from Sketch.Editor.const import SelectSingle, SelectNone
from Sketch.Graphics import PolyBezier

from test_doceditor import MockGC, complex_polybezier


class TestPolyBezierEditor(unittest.TestCase):

    """Tests for the PolyBezierEditor methods that determine the
    sensitivity of the commands.
    """

    def setUp(self):
        # Create a poly with two paths:

        self.poly = complex_polybezier()
        self.path1_nodes = self.poly.Paths()[0].NodeList()
        self.path2_nodes = self.poly.Paths()[1].NodeList()

        self.polyeditor = PolyBezierEditor(self.poly)

    def select_nodes(self, *nodes):
        """Helper for selecting handles.
        """
        self.polyeditor.Deselect()
        handles = self.polyeditor.GetHandles()
        for i in nodes:
            self.polyeditor.SelectHandle(handles[i], mode = SelectAdd)


    def test_all_nodes(self):
        """Test if the nodes are as expected.
        """
        # This is more like a pretext to have a list of nodes as a reference
        # when writing some tests.
        self.assertEquals(self.path1_nodes,
                          [Point(0, 0), Point(40, 0), Point(40, 60),
                           Point(20, 80), Point(0, 60), Point(20, 40),
                           Point(20, 20), Point(20, 10)])

        self.assertEquals(self.path2_nodes,
                          [Point(100, 0), Point(120, 20), Point(80, 20)])

    def test_can_open_nodes(self):
        """Test for PolyBezierEditor.CanOpenNodes
        """
        editor = self.polyeditor

        self.failIf(editor.CanOpenNodes())

        self.select_nodes(0)
        self.failIf(editor.CanOpenNodes())

        self.select_nodes(7)
        self.failIf(editor.CanOpenNodes())

        self.select_nodes(3)
        self.failUnless(editor.CanOpenNodes())

        self.select_nodes(3,5)
        self.failUnless(editor.CanOpenNodes())

        # select a point on curve
        editor.Deselect()
        editor.SelectPoint(Point(20, 0), Rect(19, -1, 21, 1), MockGC(),
                           SelectSet)
        self.failUnless(editor.CanOpenNodes())

    def test_can_close_nodes(self):
        """Test for PolyBezierEditor.CanCloseNodes
        """
        editor = self.polyeditor
        self.failIf(editor.CanCloseNodes())
        self.select_nodes(0)
        self.failIf(editor.CanCloseNodes())
        self.select_nodes(0,1)
        self.failIf(editor.CanCloseNodes())
        self.select_nodes(0,1,7)
        self.failIf(editor.CanCloseNodes())
        self.select_nodes(7)
        self.failIf(editor.CanCloseNodes())
        self.select_nodes(0, 7)
        self.failUnless(editor.CanCloseNodes())

    def test_can_set_continuity(self):
        """Test for PolyBezierEditor.CanSetContinuity
        """
        editor = self.polyeditor

        # Don't allow setting continuity on loose ends.
        # 1. Test first node
        self.select_nodes(0)
        self.failIf(editor.CanSetContinuity(ContAngle))
        self.failIf(editor.CanSetContinuity(ContSmooth))
        self.failIf(editor.CanSetContinuity(ContSymmetrical))
        # 2. Test last node
        self.select_nodes(7)
        self.failIf(editor.CanSetContinuity(ContAngle))
        self.failIf(editor.CanSetContinuity(ContSmooth))
        self.failIf(editor.CanSetContinuity(ContSymmetrical))

        # Don't allow setting continuity  on nodes that don't
        # belong to a Bezier segment
        self.select_nodes(1)
        self.failIf(editor.CanSetContinuity(ContAngle))
        self.failIf(editor.CanSetContinuity(ContSmooth))
        self.failIf(editor.CanSetContinuity(ContSymmetrical))

        self.select_nodes(2)
        self.failIf(editor.CanSetContinuity(ContAngle))
        self.failIf(editor.CanSetContinuity(ContSmooth))
        self.failIf(editor.CanSetContinuity(ContSymmetrical))
        # TODO: allow setting continuity if one of the segments is a Bezier
        """
        # Don't allow setting ContAngle on the 2nd node since it
        # already has it, but allow the other types.
        self.failIf(editor.CanSetContinuity(ContAngle))
        self.failUnless(editor.CanSetContinuity(ContSmooth))
        self.failUnless(editor.CanSetContinuity(ContSymmetrical))
        """

        # Don't allow setting ContSymmetrical on the 3rd node since it
        # already has it, but allow the other types.
        self.select_nodes(3)
        self.failIf(editor.CanSetContinuity(ContSymmetrical))
        self.failUnless(editor.CanSetContinuity(ContSmooth))
        self.failUnless(editor.CanSetContinuity(ContAngle))

        # Don't allow setting ContSmooth on the 5th node since it
        # already has it, but allow the other types.
        self.select_nodes(4)
        self.failUnless(editor.CanSetContinuity(ContSymmetrical))
        self.failIf(editor.CanSetContinuity(ContSmooth))
        self.failUnless(editor.CanSetContinuity(ContAngle))

        # Don't allow setting ContAngle on the 6th node since it
        # already has it, but allow the other types.
        self.select_nodes(5)
        self.failUnless(editor.CanSetContinuity(ContSymmetrical))
        self.failUnless(editor.CanSetContinuity(ContSmooth))
        self.failIf(editor.CanSetContinuity(ContAngle))

        # Don't allow setting continuity on nodes that don't belong
        # to a Bezier segment
        self.select_nodes(6)
        self.failIf(editor.CanSetContinuity(ContSymmetrical))
        self.failIf(editor.CanSetContinuity(ContSmooth))
        self.failIf(editor.CanSetContinuity(ContAngle))
        # TODO: allow setting continuity if one of the segments is a Bezier
        """
        # Don't allow setting ContAngle on the 6th node since it
        # already has it, but allow the other types.
        self.failIf(editor.CanSetContinuity(ContAngle))
        self.failUnless(editor.CanSetContinuity(ContSmooth))
        self.failUnless(editor.CanSetContinuity(ContSymmetrical))
        """

    def test_can_convert_segments(self):
        """Test for PolyBezierEditor.CanConvertSegments
        """
        editor = self.polyeditor
        # nothing selected:
        self.failIf(editor.CanConvertSegments(Line))
        self.failIf(editor.CanConvertSegments(Bezier))

        # select a single node
        self.select_nodes(3)
        self.failIf(editor.CanConvertSegments(Line))
        self.failIf(editor.CanConvertSegments(Bezier))

        # two non-consecutive nodes
        self.select_nodes(3, 5)
        self.failIf(editor.CanConvertSegments(Line))
        self.failIf(editor.CanConvertSegments(Bezier))

        # two consecutive nodes enclosing a Line
        self.select_nodes(1, 2)
        self.failIf(editor.CanConvertSegments(Line))
        self.failUnless(editor.CanConvertSegments(Bezier))

        # two consecutive nodes enclosing a Bezier
        self.select_nodes(2, 3)
        self.failUnless(editor.CanConvertSegments(Line))
        self.failIf(editor.CanConvertSegments(Bezier))

        # four consecutive nodes: allow all
        self.select_nodes(0,1,2,3)
        self.failUnless(editor.CanConvertSegments(Line))
        self.failUnless(editor.CanConvertSegments(Bezier))

        # Select a point on a Line segment
        editor.Deselect()
        editor.SelectPoint(Point(20, 0), Rect(19, -1, 21, 1), MockGC(),
                           SelectSet)
        self.failIf(editor.CanConvertSegments(Line))
        self.failUnless(editor.CanConvertSegments(Bezier))

        # Select a point on a Curve segment (the fourth)
        # The point doesn't have to be specified with precision
        # editor.SelectPoint will automatically choose the nearest point
        # on curve given an rect large enough
        editor.Deselect()
        editor.SelectPoint(Point(35, 70), Rect(0, 0, 100, 100), MockGC(),
                           SelectSet)
        self.failUnless(editor.CanConvertSegments(Line))
        self.failIf(editor.CanConvertSegments(Bezier))

    def test_can_delete_nodes(self):
        """Test PolyBezierEditor.CanDeleteNodes
        """
        # test with no nodes selected
        self.failIf(self.polyeditor.CanDeleteNodes())
        # test with the some nodes selected
        self.select_nodes(0)
        self.failUnless(self.polyeditor.CanDeleteNodes())
        self.select_nodes(3)
        self.failUnless(self.polyeditor.CanDeleteNodes())

    def test_can_insert_nodes(self):
        """Test for PolyBezierEditor.CanInsertNodes
        """
        editor = self.polyeditor
        # nothing selected
        self.failIf(editor.CanInsertNodes())

        # select the first node
        self.select_nodes(0)
        self.failIf(editor.CanInsertNodes())

        # select the first and the third node
        self.select_nodes(0, 2)
        self.failIf(editor.CanInsertNodes())

        # select a point on curve
        editor.Deselect()
        editor.SelectPoint(Point(20, 0), Rect(19, -1, 21, 1), MockGC(),
                           SelectSet)
        self.failUnless(editor.CanInsertNodes())

        # select a segment by selecting consecutive nodes
        self.select_nodes(1, 2)
        self.failUnless(editor.CanInsertNodes())

        self.select_nodes(1, 2, 5, 6)
        self.failUnless(editor.CanInsertNodes())

    def test_selectpoint(self):
        """Test PolyBezierEditor.SelectPoint"""
        editor = self.polyeditor
        # A point on curve
        p = Point(20, 0)
        gc = MockGC()
        rect = gc.HitRectAroundPoint(p)
        self.assertEquals(editor.SelectPoint(p, rect, gc, SelectSet),
                          SelectSingle)

        # try again, with a rect that doesn't contain any point of the poly
        self.assertEquals(editor.SelectPoint(p, EmptyRect, gc, SelectSet),
                          SelectNone)



if __name__ == "__main__":
    unittest.main()

