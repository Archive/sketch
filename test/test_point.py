# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001, 2002, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Tests for the point objects
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import math, operator
import unittest

import support

support.add_sketch_module_dir_to_path()

import _sketch
from _sketch import Point, Polar


class PointTest(support.FPTestCase):

    """Test cases for the point objects"""

    def test_constructors(self):
        """Test point object constructors"""
        eq = self.assertEquals
        fe = self.assert_float_equal
        exc = self.assertRaises

        # p = Point(x, y) -> x == p.x and y == p.y
        p = Point(1.0, 2.0)
        eq(p.x, 1.0)
        eq(p.y, 2.0)

        # Point() accepts floats and ints and sequences of two numbers
        eq(p, Point(1, 2))
        eq(p, Point((1, 2)))
        eq(p, Point([1, 2]))
        # it should fail on other sequences
        exc(TypeError, Point, (1, 2, 3))
        exc(TypeError, Point, (1, []))
        exc(TypeError, Point, ())

        # Polar constructs a point object from polar coordinates
        p = Polar(1, 0)
        eq(p.x, 1)
        eq(p.y, 0)

        p = Polar(1, math.pi)
        fe(p.x, -1)
        fe(p.y, 0)

        angle = math.pi / 4
        p = Polar(2, angle)
        fe(p.x, 2 * math.cos(angle))
        fe(p.y, 2 * math.sin(angle))

    def test_sequence(self):
        """Test point object sequence protocol"""
        eq = self.assertEquals
        # point objects can be used as sequence objects
        p = Point(1, 2)
        eq(len(p), 2)
        eq(p[0], p.x)
        eq(p[1], p.y)
        eq(p[-2], p.x)
        eq(p[-1], p.y)
        self.assertRaises(IndexError, operator.getitem, p, 2)
        self.assertRaises(IndexError, operator.getitem, p, -3)
        eq(tuple(p), (p.x, p.y))

    def test_abs(self):
        """Test abs(point)"""
        fe = self.assert_float_equal

        # abs(Point(x, y)) == hypot(x, y)
        fe(abs(Point(3, 4)), 5)
        fe(abs(Polar(10, math.pi / 6)), 10)


    def test_arithmetic(self):
        """Test point object and arithmetic operators"""
        fe = self.assert_float_equal
        eq = self.assertEquals

        p = Point(1, 2); q = Point(-1, 0.5)

        # unary +, -
        pos = +p
        # should be exactly the same
        eq(pos.x, p.x)
        eq(pos.y, p.y)

        neg = -p
        eq(neg.x, -p.x)
        eq(neg.y, -p.y)

        # sum of two points = vector sum
        sum = p + q
        fe(sum.x, p.x + q.x)
        fe(sum.y, p.y + q.y)

        sum = p - q
        fe(sum.x, p.x - q.x)
        fe(sum.y, p.y - q.y)

        # The product is the scalar product
        fe(p * q, p.x * q.x + p.y * q.y)

        # number * point and point * number. Do both because for objects
        # that are also sequences, multiplication can be tricky to
        # implement, although this has become easier since Python 2.1.
        factor = 3
        product = factor * p
        fe(product.x, factor * p.x)
        fe(product.y, factor * p.y)

        factor = 3
        product = p * factor
        fe(product.x, factor * p.x)
        fe(product.y, factor * p.y)

        # point / number
        d = 3
        quot = p / d
        fe(quot.x, p.x / d)
        fe(quot.y, p.y / d)

    def test_methods(self):
        """Test point object methods"""
        fe = self.assert_float_equal

        # normalized()
        p = Point(3, 4)
        norm = p.normalized()
        fe(abs(norm), 1)
        fe(norm.x, p.x / abs(p))
        fe(norm.y, p.y / abs(p))

        # polar()
        p = Point(3, 4)
        r, phi = p.polar()
        fe(phi, math.atan2(p.y, p.x))
        fe(r, math.hypot(p.y, p.x))

    def test_equality(self):
        """Test point object equality comparisons"""
        self.assert_(Point(0, 0) == Point(0, 0))
        self.assert_(Point(0, 10.5) == Point(0, 10.5))
        self.assert_(Point(-12.5, 0) == Point(-12.5, 0))
        self.assert_(Point(-123.25, 47.75) == Point(-123.25, 47.75))

        self.failIf(Point(0, 0) == Point(1, 0))
        self.failIf(Point(12, 0) == Point(-12, 0))
        self.failIf(Point(12, 0) == Point(0, 12))

        self.failIf(Point(47, 42) == (47, 42))
        self.failIf(Point(12, 0) == [12, 0])
        self.failIf(Point(0, 12) == [Point(0, 12)])


if __name__ == "__main__":
    unittest.main()
