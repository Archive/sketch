# Skencil - A Python-based interactive drawing program
# Copyright (C) 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Distutils setup script for Skencil."""

__version__ = "$Revision$"
# $Source$
# $Id$

import os
from distutils.core import setup, Extension
import distutils

from string import split
import string

#
#   Functions to run gtk-config style programs
#

def run_script(cmdline):
    """Run command and return its stdout or none in case of errors"""
    pipe = os.popen(cmdline)
    result = pipe.read()
    if pipe.close() is not None:
        print '"' + cmdline + '"', 'failed'
        return None
    return result


def run_config_script(command):
    """Run a gtk-config like program and return compiler flags

    If successful, return a tuple of four lists (incdirs, defs, libdirs,
    libs). If running the program fails, return None.
    """
    incdirs = []
    defs = []
    libdirs = []
    libs = []
    # Determine the C-flags
    flags = run_script(command + ' --cflags ')
    if flags is None:
        return None
    for flag in split(flags):
        start = flag[:2]
        value = flag[2:]
        if start == "-I":
            incdirs.append(value)
        elif start == "-D":
            defs.append((value, None))
        # FIXME: handle other cases?

    # determine the library flags
    flags = run_script(command + ' --libs')
    if flags is None:
        return None
    for flag in split(flags):
        start = flag[:2]
        value = flag[2:]
        if start == "-L":
            libdirs.append(value)
        elif start == "-l":
            libs.append(value)
        # FIXME: handle other cases?

    print (incdirs, defs, libdirs, libs)
    return (incdirs, defs, libdirs, libs)


gtk_incdirs, gtk_defs, gtk_libdirs, gtk_libs\
             = run_config_script("pkg-config gtk+-2.0")

pygtk_incdirs, pygtk_defs, pygtk_libdirs, pygtk_libs \
               = run_config_script('pkg-config pygtk-2.0')

libart_incdirs, libart_defs, libart_libdirs, libart_libs \
                = run_config_script("libart2-config")

# Include directories for the header files of the Python Imaging
# Library.
# FIXME: Make this at least as automatic and settable via command line
# as in the setup.py script of 0.6
imaging_incdirs = []


#
# Define some extension and python modules
#
# The C-extension names are prefixed with "Sketch.Modules." so they get
# put into the Sketch/Modules/ subdirectory. Sketch/Modules/ is not
# really a package but distutils doesn't care

# subdirectory containing the extensions
ext_dir = "Sketch/Modules/"

# lists to fill with the module descriptions
py_modules = []

extensions = [Extension("Sketch.Modules._sketchmodule",
                        [ext_dir + file for file in
                         ["_sketchmodule.c", "skpoint.c", "sktrafo.c",
                          "skrect.c", "skfm.c", "curvefunc.c", "curveobject.c",
                          "curvelow.c", "curvemisc.c", "curvefit.c", "skaux.c",
                          "skimage.c", "skcolor.c"]],
                        include_dirs = imaging_incdirs),
              Extension("Sketch.Modules._type1module",
                        [ext_dir + "_type1module.c"]),
              Extension("Sketch.Modules.skreadmodule",
                        [ext_dir + "skreadmodule.c"]),
              Extension("Sketch.Modules.pstokenize",
                        [ext_dir + "pstokenize.c"],
                        include_dirs = ["Filter"])]

skgtk_ext = Extension("Sketch.Modules._skgtkmodule",
                      [ext_dir + file for file in
                       ["_skgtkmodule.c", "regionobject.c", "imageobject.c",
                        "clipmask.c", "skvisual.c", "skgtkimage.c",
                        "skdither.c", "curvedraw.c"]],
                      include_dirs = (gtk_incdirs + pygtk_incdirs
                                      + libart_incdirs + imaging_incdirs),
                      define_macros = gtk_defs + pygtk_defs + libart_defs,
                      libraries = gtk_libs + pygtk_libs + libart_libs,
                      library_dirs = gtk_libdirs+pygtk_libdirs+libart_libdirs)
libart_ext = Extension("Sketch.Modules._libart",
                       [ext_dir + file for file in
                        ["_libartmodule.c", "skrender.c"]],
                       include_dirs = libart_incdirs + imaging_incdirs,
                       define_macros = libart_defs,
                       libraries = libart_libs,
                       library_dirs = libart_libdirs)

extensions.extend([skgtk_ext, libart_ext])

# Stream-filter modules
filter_globals = {}
execfile(os.path.join("Filter", "setup.py"), filter_globals)
define_filter_extensions = filter_globals["define_extensions"]
extensions.extend(define_filter_extensions(0, dir_prefix = "Filter/",
                                           module_prefix = "Filter."))

#
#  Data files
#

# data_files = []

# # bitmaps
# dir = "Resources/Bitmaps"
# bitmaps = []
# for file in os.listdir(os.path.join("Resources", "Bitmaps")):
#     if string.lower(file[-4:]) == ".xpm":
#         bitmaps.append(dir + '/' +  file)
# data_files.append((dir, bitmaps))

#
#       Command definitions
#
# So far distutils are only meant to distribute python extensions, not
# complete applications, so we have to redefine a few commands
#


#
#   Run the script
#


long_description = """\
Skencil is an interactive vector drawing program
"""

setup(name = "Skencil",
      version = "CVS",
      description = "Vector drawing program",
      long_description = long_description,
      licence = "LGPL",
      author = "Bernhard Herzog",
      author_email = "bh@intevation.de",
      url = "http://skencil.org/",

      scripts = ["skencil.py"],

      packages = ["Sketch", "Sketch.Base", "Sketch.Lib", "Sketch.Graphics",
                  "Sketch.Editor", "Sketch.UI", "Sketch.Plugin"],

      ext_modules = extensions,

      # defaults for the install command
      options = {"install":
                 # prefix defaults to python's prefix normally
                 {"prefix": "/usr/local/",
                  # make sure both libs and scripts are installed in the
                  # same directory.
                  "install_lib": "$base/lib/skencil-cvs",
                  "install_scripts": "$base/lib/skencil-cvs",
                  "install_data": "$base/lib/skencil-cvs",

                  # Don't print warning messages about the lib dir not
                  # being on Python's path. The libraries are Thuban
                  # specific and are installed just for Thuban. They'll
                  # be automatically on Python's path when Thuban is run
                  "warn_dir": 0,
                  },
                 "build_ext": {"inplace": 1}})


