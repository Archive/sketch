#! /usr/bin/env python

# Skencil - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2003, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#
# a small script to find out where Skencil is installed and launch Skencil
#

import sys, os

# Add GTK2 path to PATH environment variable on Windows
if sys.platform.startswith("win"):
    try:
        import _winreg
        key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "Software\\GTK\\2.0")
        gtkdir = _winreg.QueryValueEx(key, "Path")[0]
        os.environ['PATH'] += ";%s/lib;%s/bin" % (gtkdir, gtkdir)
    except:
        pass

# XXX hack to work around a bug on SuSE 6.1 + gtk 1.2.x and strtod. I
# don't know whether this is a bug in gtk or libc (it didn't happen on
# SuSE 5.3), but setting LC_CTYPE to 'C' solves it for the german SuSE
# 6.1 version which by default sets this to de_DE.
#
# The result of the bug is a failure in string.atof or the builtin float
# with a message claiming that e.g. '0.0' is too large for a double.
#if os.environ.has_key('LC_CTYPE'):
os.environ['LC_CTYPE'] = 'C'

# Explicitly choose which pygtk version we use. This fails with an
# ImportError if pygtk is not installed in site-packages because it uses
# the .pth mechanism, so put into a try...except.
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass

import Sketch.UI

Sketch.UI.main(sys.argv)
