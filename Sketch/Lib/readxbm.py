# Sketch - A Python-based interactive drawing program
# Copyright (C) 2001 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import re
import PIL.Image
  
# XBM header
xbm_head = re.compile(
    "#define[ \t]+[_a-zA-Z]*_width[ \t]+(?P<width>[0-9]*)[\r\n]+"
    "#define[ \t]+[_a-zA-Z]*_height[ \t]+(?P<height>[0-9]*)[\r\n]+"
    "(?P<hot_spot>"
    "#define[ \t]+[_a-zA-Z]*_x_hot[ \t]+(?P<xhot>[0-9]*)[\r\n]+"
    "#define[ \t]+[_a-zA-Z]*_y_hot[ \t]+(?P<yhot>[0-9]*)[\r\n]+"
    ")?"
    "[\\000-\\377]*_bits\[\]")

def read_xbm(filename):
    data = open(filename).read()
    match = xbm_head.match(data)
    if match is not None:
        size = (int(match.group("width")),
                int(match.group("height")))
        image = PIL.Image.fromstring("1", size, data[match.end():], "xbm")
        if match.group("hot_spot") is not None:
            image.info["hot_spot"] = (int(match.group("xhot")),
                                      int(match.group("yhot")))
    else:
        image = None 
    return image
