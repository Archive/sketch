# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import string

from Sketch import _
import Sketch.Graphics

from Sketch.Base.warn import pdebug


class GenericLoader:

    format_name = ''

    base_style = None

    def __init__(self, file, filename, match):
        self.file = file
        self.filename = filename
        self.match = match
        self.messages = {}
        self.doc_class = None

    def set_doc_class(self, doc_class):
        self.doc_class = doc_class

    def add_message(self, message):
        pdebug(('load', 'echo_messages'), message)
        self.messages[message] = self.messages.get(message, 0) + 1

    def Messages(self):
        messages = self.messages.items()
        list = []
        for message, count in messages:
            if count > 1:
                list.append(_("%(message)s (%(count)d times)") % locals())
            else:
                list.append(message)
        list.sort()
        return string.join(list, '\n')


class SimplifiedLoader(GenericLoader):

    def __init__(self, file, filename, match):
        GenericLoader.__init__(self, file, filename, match)
        self.lineno = 1 # first line has been read

    def readline(self):
        line = self.file.readline()
        self.lineno = self.lineno + 1
        return line
