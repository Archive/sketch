# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import os
import Sketch

class PluginPreferences(Sketch.Base.Preferences):

    _runtime_prefs = Sketch.Base.Preferences._runtime_prefs.copy()
    _runtime_prefs['path'] = 1

    _filename = 'plugin.prefs'

    #
    #   Paths
    #
    #
    #   There are three preferences that define the directories Sketch
    #   searches for plugins, default_path, user_path and path.
    #
    #   default_path is a list of directories relative to Sketch's
    #   installation directory where the standard plugins are located.
    #
    #   user_path is a list of user defined directories which is empty
    #   by default. Use this preference to add directories in your local
    #   ~/.sketch/plugin.prefs. (actually in ~/.devsketch during
    #   development).
    #
    #   path is simply the concatenation of default_path and user_path
    #   (in that order) which is constructed at startup. The filenames
    #   in default_path are expanded to full directory names at that
    #   time as well. path will not be saved in plugin.prefs.
    #
    
    default_path = ['Plugins/']

    user_path = []

    path = []

    def _fix_paths(self):
        prefix = Sketch.Base.config.sketch_dir
        path = []
        for dir in self.default_path:
            path.append(os.path.join(prefix, dir))
        for dir in self.user_path:
            dir = os.path.expanduser(dir)
            dir = os.path.expandvars(dir)
            path.append(dir)
        path = path + self.user_path
        path = filter(os.path.isdir, path)
        self.path = path

    #
    #	Filters
    #

    #	If true, try to unload some of the import/export filter modules
    #	after use. Only filters marked as unloadable in their config
    #	file are affected.
    unload_filters = 1

    #
    #   Misc
    #

    #   The name of the native file format. This is used by the loader
    #   to set the native_format flag of the document.  
    native_format = 'SK-2'


preferences = PluginPreferences()


def init(argv, load_user_preferences = 0):
    import plugins
    if load_user_preferences:
        preferences._load()
    preferences._fix_paths()
    plugins.load_plugin_configuration()
  
