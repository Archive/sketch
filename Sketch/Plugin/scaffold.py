# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from types import TupleType, StringType

from Sketch import Trafo, Translation
from Sketch.Base import SketchLoadError
from Sketch.Graphics import text, PolyBezier, Rectangle, Ellipse, Style, \
     PropertyStack, EmptyPattern, Document, ImageData, Image, pagelayout
import Sketch.Graphics

from Sketch.Base.const import ArcPieSlice


class EmptyCompoundError(SketchLoadError):
    pass



class CompoundScaffold:

    def __init__(self):
        self.compound_stack = None
        self.compound_items = []
        self.compound_object = None
        self.object = None

    def __push(self):
        self.compound_stack = (self.compound_object, self.compound_items,
                               self.compound_stack)

    def __pop(self):
        (self.compound_object, self.compound_items, self.compound_stack) \
                               = self.compound_stack

    def BeginCompound(self, compound_object):
        self.__push()
        self.compound_object = compound_object
        self.compound_items = []
        self.object = None

    def EndCompound(self):
        if self.compound_object is not None:
            compound = self.compound_object
            if self.compound_items or compound.can_be_empty:
                append = compound.load_AppendObject
                for item in self.compound_items:
                    append(item)
                compound.load_Done()
                self.__pop()
                self.AppendObject(compound)
            else:
                self.__pop()
                raise EmptyCompoundError
        else:
            raise SketchLoadError('no compound to end')

    def EndAllCompounds(self):
        while self.compound_stack:
            self.EndCompound()

    def AppendObject(self, object):
        self.compound_items.append(object)
        self.object = object

    def PopLastObject(self):
        # remove the last object in self.compound_items and return it
        object = None
        if self.compound_items:
            object = self.compound_items[-1]
            del self.compound_items[-1]
        return object


class DocumentScaffold(CompoundScaffold):

    def __init__(self, document_class = None, base_style = None):
        CompoundScaffold.__init__(self)
        self.style = Style()
        self.base_style = base_style
        if self.base_style is not None:
            self.prop_stack = PropertyStack(base=self.base_style.Duplicate())
        else:
            self.prop_stack = PropertyStack()
        self.messages = {}
        if document_class is not None:
            self.doc_class = document_class
        else:
            self.doc_class = Document
        self.style_dict = {}
        self.page_layout = None
        self.meta_data = []

    def AddMetaData(self, key, value):
        self.meta_data.append((key, value))

    def add_meta_data_to_object(self, object):
        for key, value in self.meta_data:
            object.SetData(key, value)
        self.meta_data = []

    def BeginCompound(self, compound_object):
        self.add_meta_data_to_object(compound_object)
        CompoundScaffold.BeginCompound(self, compound_object)
        
    def AppendObject(self, object):
        self.add_meta_data_to_object(object)
        CompoundScaffold.AppendObject(self, object)

    def get_prop_stack(self):
        stack = self.prop_stack
        if not self.style.IsEmpty():
            stack.load_AddStyle(self.style)
        stack.condense()
        if self.base_style is not None:
            self.prop_stack = PropertyStack(base =self.base_style.Duplicate())
        else:
            self.prop_stack = PropertyStack()
        self.style = Style()
        return stack

    def set_prop_stack(self, stack):
        self.prop_stack = stack

    def SetProperties(self, **kw):
        style = self.style
        for key, value in kw.items():
            setattr(style, key, value)

    def EmptyLine(self):
        self.style.line_pattern = EmptyPattern

    def EmptyFill(self):
        self.style.fill_pattern = EmptyPattern

    def DefineStyle(self, name):
	style = self.style.AsDynamicStyle()
	style.SetName(name)
	self.style_dict[name] = style
	self.style = Style()

    def UseStyle(self, name):
	if not self.style.IsEmpty():
	    self.prop_stack.load_AddStyle(self.style)
	    self.style = Style()
	style = self.style_dict[name]
	self.prop_stack.load_AddStyle(style)
        
    def PageLayout(self, format, orientation):
	if type(format) == StringType:
            layout = pagelayout.PageLayout(format, orientation=orientation)
	else:
	    w, h = format
	    layout = pagelayout.PageLayout(width = w, height = h,
					   orientation = orientation)
	self.page_layout = layout

    def BeginDocument(self, *args, **kw):
        self.BeginCompound(apply(self.doc_class, args, kw))

    def EndDocument(self):
        self.EndAllCompounds()
	if self.page_layout:
	    self.object.load_SetLayout(self.page_layout)
	for style in self.style_dict.values():
	    self.object.load_AddStyle(style)
	self.object.load_Completed()
        return self.object

    def BeginLayer(self, *args, **kw):
        self.begin_layer_class(Sketch.Graphics.Layer, args, kw)

    def EndLayer(self):
        self.EndCompound()

    def begin_layer_class(self, layer_class, args, kw = None):
        if kw is None:
            kw = {}
        if isinstance(self.compound_object, Sketch.Graphics.Layer):
            self.EndCompound()
        if isinstance(self.compound_object, self.doc_class):
            self.BeginCompound(apply(layer_class, args, kw))
        else:
            raise SketchLoadError('self.compound_object %s, not a document',
                                  `self.compound_object`)

    def PolyBezier(self, paths):
        self.AppendObject(PolyBezier(paths = paths,
                                     properties = self.get_prop_stack()))

    def Rectangle(self, trafo, radius1 = 0, radius2 = 0):
        if type(trafo) == TupleType:
            trafo = apply(Trafo, trafo)
        self.AppendObject(Rectangle(trafo, radius1 = radius1,
                                    radius2 = radius2,
                                    properties = self.get_prop_stack()))

    def Ellipse(self, trafo, start_angle = 0.0, end_angle = 0.0,
                arc_type = ArcPieSlice):
        if type(trafo) == TupleType:
            trafo = apply(Trafo, trafo)
        self.AppendObject(Ellipse(trafo, start_angle, end_angle, arc_type,
                                  properties = self.get_prop_stack()))

    def SimpleText(self, str, trafo = None, valign = text.ALIGN_BASE,
                   halign = text.ALIGN_LEFT):
        if type(trafo) == TupleType:
            if len(trafo) == 2:
                trafo = apply(Translation, trafo)
            else:
                raise TypeError, "trafo must be a Trafo-object or a 2-tuple"
        self.AppendObject(text.SimpleText(text = str, trafo = trafo,
                                          valign = valign, halign = halign,
                                          properties = self.get_prop_stack()))

    def Image(self, image, trafo):
        if type(trafo) == TupleType:
            if len(trafo) == 2:
                trafo = apply(Translation, trafo)
            else:
                raise TypeError, "trafo must be a Trafo-object or a 2-tuple"
        image = ImageData(image)
        self.AppendObject(Image(image, trafo = trafo))

    def BeginGroup(self):
        self.BeginCompound(Sketch.Graphics.Group())

    def EndGroup(self):
        self.EndCompound()


