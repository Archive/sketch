# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from skexceptions import SketchError, SketchInternalError, SketchLoadError

from undo import Undo, CreateListUndo, CreateMultiUndo, UndoAfter,\
     UndoRedo, NullUndo

from connector import Publisher, QueueingPublisher, Subscribe

import config
from configutil import Preferences



class BasePreferences(Preferences):

    _filename = 'base.prefs'

    #
    #	Patterns
    #
    #	How many steps to draw in a gradient pattern
    #
    gradient_steps_editor = 30
    gradient_steps_print = 50

    #	The standard palette. If this is a relative pathname it is
    #	interpreted relative to std_res_dir.
    # XXX should be moved to Sketch.Editor.preferences or
    palette = 'standard.spl'

    arrows = 'standard.arrow'
    dashes = 'standard.dashes'
    pattern = 'pattern.ppm'

    #	Default resolution in pixels/inch for a new raster image that
    #	doesn't specify it itself. (not implemented yet)
    default_image_resolution = 72

    #
    #	EPS Files
    #
    #	The resoulution in pixel/inch of the preview image Sketch
    #	renders for preview. (using gs). Leave this at 72 for now.
    # XXX should be moved to Sketch.UI.preferences
    eps_preview_resolution = 72

    #
    #	Warning Messages
    #
    #	Whether to print internal warning messages. Useful for
    #	debugging.
    print_internal_warnings = 1

    #	print additional messages. these are usually only interesting
    #	for development purposes.
    print_debug_messages = 1

    #
    #	Import Filters
    #

    #	If true, try to unload some of the import filter modules after
    #	use. Only filters marked as unloadable in their config file are
    #	affected.
    unload_import_filters = 1

preferences = BasePreferences()

def init(argv, load_user_preferences = 0):
    # Initialize the base components of Sketch.
    #
    # argv -- the command line parameters
    # load_user_preferences -- if true, load the user preferences
    #
    # set config.sketch_command to argv[0] which is assumed to be the
    # command used to start Sketch.
    #
    # Issue a global INITIALIZE message. (global messages are associated
    # with None)
    import config, const, Sketch
    config.sketch_command = argv[0]
    preferences._load()
    if load_user_preferences:
        config.load_user_preferences()
    Sketch.Issue(const.INITIALIZE)
