# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#
#	Constants...
#

#
#	Standard channel names
#

# common
CHANGED = 'CHANGED'
DOCUMENT = 'DOCUMENT'
SELECTION = 'SELECTION'
TOOL = 'TOOL'

# dialogs
CLOSED = 'CLOSED'

# TKExt
COMMAND = 'COMMAND'
# also uses SELECTION

# APPLICATION
CLIPBOARD = 'CLIPBOARD'

# Global
INITIALIZE = 'INITIALIZE'
APP_INITIALIZED = 'APP_INITIALIZED'
INIT_READLINE = 'INIT_READLINE'
SAVE_PREFERENCES = 'SAVE_PREFERENCES'

PRINT_WARNING = 'PRINT_WARNING'
PRINT_TRACEBACK = 'PRINT_TRACEBACK'

# Editor
HANDLES = 'HANDLES'
CONFIG = 'CONFIG'

# CANVAS
STATE = 'STATE'
UNDO = 'UNDO'
VIEW = 'VIEW'
POSITION = 'POSITION'
CURRENTINFO = 'CURRENTINFO'

# DOCUMENT
EDITED = 'EDITED'
GRID = 'GRID'
LAYER = 'LAYER'
LAYER_STATE = 'LAYER_STATE';	LAYER_ORDER = 'LAYER_ORDER'
LAYER_COLOR = 'LAYER_COLOR';	LAYER_ACTIVE = 'LAYER_ACTIVE'
LAYOUT = 'LAYOUT'
REDRAW = 'REDRAW'
STYLE = 'STYLE'
UNDO = 'UNDO'
GUIDE_LINES = 'GUIDE_LINES'

OBJECT_CHANGED = 'OBJECT_CHANGED'
TRANSACTION_START = 'TRANSACTION_START'
TRANSACTION_END = 'TRANSACTION_END'

# what params for OBJECT_CHANGED
SHAPE = "SHAPE"
PROPERTIES = "PROPERTIES"
CHILDREN = "CHILDREN"
INSERTED = "INSERTED"
REMOVED = "REMOVED"
REARRANGED = "REARRANGED"
META_CHANGED = "META_CHANGED"

#
#       Scripting Access
#

SCRIPT_UNDO = 'SCRIPT_UNDO'
SCRIPT_GET = 'SCRIPT_GET'
SCRIPT_OBJECT = 'SCRIPT_OBJECT'
SCRIPT_OBJECTLIST = 'SCRIPT_OBJECTLIST'

#
#	constants for selections
#

# the same as in curveobject.c
SelectSet = 0
SelectAdd = 1
SelectSubtract = 2
SelectSubobjects = 3
SelectDrag = 4

SelectGuide = 5

# Arc modes. bezier_obj.approx_arc uses these
ArcArc = 0
ArcChord = 1
ArcPieSlice = 2


#
#	Line Styles
#
# These happen to be same values as in GDK and Xlib

JoinMiter	= 0
JoinRound	= 1
JoinBevel	= 2
CapButt		= 1
CapRound	= 2
CapProjecting	= 3
