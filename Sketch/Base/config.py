# Skencil - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

from configutil import init_directories, load_user_preferences, Preferences

#
#       some fundamental defaults for Skencil
#

# The title of the application. The window title will be this plus the
# name of the document.
name = 'Skencil'


# The command used to invoke sketch. set on startup
sketch_command = 'skencil'


#       Some directories. They are updated to full pathnames by the
#       startup script

# The directory where skencil and its modules are found. Set
# automagically from __init__.py of the Sketch package
sketch_dir = ''

# Subdirectory where resources (palettes, arrows, dashes,...) are stored.
# On startup it is expanded to an absolute pathname.
resource_dir = 'Resources'
std_res_dir = resource_dir + '/Misc'

# Subdirectory for the pixmaps. Used by skpixmaps.py when loading the
# pixmaps. Here this is meant relative to sketch_dir; on startup it is
# expanded to an absolute pathname.
pixmap_dir = 'Sketch/Pixmaps'

# Subdirectory for the font metrics. Here, this is meant relative to
# sketch_dir; on startup it is expanded to an absolute pathname.
fontmetric_dir = resource_dir + '/Fontmetrics'

# Directories where skencil looks for font related files such as font
# metrics, Type1 files (pfb or pfa) and font database files. The
# expanded fontmetric_dir is appended to this.
#
# On platforms other than Linux this probably needs a few additional
# directories. (non-existing dirs are automatically removed)
font_path = ['/usr/X11R6/lib/X11/fonts/Type1',
             '/usr/share/ghostscript/fonts']


# List of directories, where Skencil searches for resource files like
# palettes, arrow definitions and dash patterns. The expanded
# resource_dir is appended to this.
# XXX implement this. (together with a way for the user to specify their
# own versions)
#resource_path = []


# PostScript Prolog. PostScript file containing the Skencil-specific
# procset. relative to sketch_dir
postscript_prolog = os.path.join(std_res_dir, 'sketch-proc.ps')

# gtk_defaults. relative to std_res_dir
gtk_defaults = 'gtkrc'


#
#       user config settings
#

# directory in the user's home directory that contains userspecific
# files
user_config_dir = '.devsketch'
# the user's home directory. Set on startup
user_home_dir = ''


