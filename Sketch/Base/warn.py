# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000, 2001, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import sys, string
from types import StringType, DictionaryType
import traceback

import const
import Sketch

INTERNAL = 'INTERNAL'
USER = 'USER'


def write_error(message):
    """Write message to stderr.

    If message isn't empty and doesn't end in a newline write a newline
    afterwards.
    """
    sys.stderr.write(message)
    if message and message[-1] != '\n':
        sys.stderr.write('\n')

def flexible_format(format, args, kw):
    """Smart message formatting

    Parameters:

        format -- The format string or message
        args -- tuple with positional arguments for the format string
        kw -- dictionary with keyword argument for the format string

    The return value is the formatted message.

    This function can be called in three ways.  If both args and kw are
    empty, format is returned as is.  If args is not empty, format is
    applied to args othewise it's applied to kw.

    If a type error occurs during formatting (e.g. because a % specified
    gives the wrong type) a string is constructed from format and args
    or kw and returned.  This value will thus contain the information of
    the message and arguments so that it doesn't get lost because of an
    exception in the formatting due to a bug in the warning call.
    """
    try:
        if args:
            text = format % args
        elif kw:
            text = format % kw
        else:
            text = format
    except TypeError:
        if args:
            text = string.join([format] + map(str, args))
        elif kw:
            text = string.join([format] + map(str, kw.items()))
        else:
            text = format

    return text


def warn(_level, _message, *args, **kw):
    """Issue _message as a warning at level _level

    Parameters:

        _level -- The warning level.  Either USER or INTERNAL.
        _message -- The message as a string

    Additional positional and keyword arguments may be given, in which
    case _message is considered to be a format string and the warning
    message will be constructed with _message applied to the additional
    parameters with the %-operator.  If no additional arguments are
    given, _message is used as is, so in that case it can contain %
    characters without having to quote them.

    If _level is USER a PRINT_WARNING message will be issued with the
    formatted message as parameter.  In addition the message will be
    written to sys.stderr.

    If _level is INTERNAL and
    Sketch.Base.preferences.print_internal_warnings is true, the
    formatted message is written to sys.stderr.  Otherwise the message
    will be ignored.

    The parameters _level and _message start with an underscore because
    they should be given as positional parameters and their names shouldn't
    interfere with keyword arguments.
    """
    _message = flexible_format(_message, args, kw)

    if _level == INTERNAL:
        if Sketch.Base.preferences.print_internal_warnings:
            write_error(_message)
    else:
        write_error(_message)
        Sketch.Issue(const.PRINT_WARNING, _message)
    return _message

def warn_tb(_level, _message = '', *args, **kw):
    """Issue _message and a tracback as a warning at level _level

    Parameters:

        _level -- The warning level.  Either USER or INTERNAL.
        _message -- The message as a string.  Optional.

    Additional positional and keyword arguments may be given, in which
    case _message is considered to be a format string and the warning
    message will be constructed with _message applied to the additional
    parameters with the %-operator.  If no additional arguments are
    given, _message is used as is, so in that case it can contain %
    characters without having to quote them.

    A formatted traceback of the current exception is produced with the
    traceback module.  Therefore this function should only be called
    inside an except clause.

    If _level is USER a PRINT_WARNING message will be issued with the
    formatted message and the formatted traceback as separate
    parameters.  In addition the message and traceback will be written
    to sys.stderr.

    If _level is INTERNAL and
    Sketch.Base.preferences.print_internal_warnings is true, the
    formatted message is written to sys.stderr.  Otherwise the message
    will be ignored.

    The parameters _level and _message start with an underscore because
    they should be given as positional parameters and their names shouldn't
    interfere with keyword arguments.
    """
    _message = flexible_format(_message, args, kw)

    if _level == INTERNAL:
        if Sketch.Base.preferences.print_internal_warnings:
            write_error(_message)
            traceback.print_exc()
    else:
        from cStringIO import StringIO
        file = StringIO()
        traceback.print_exc(file = file)
        tb = file.getvalue()
        Sketch.Issue(const.PRINT_TRACEBACK, _message, tb)
        write_error(_message)
        write_error(tb)
    return _message




def Dict(**kw):
    return kw

_levels = Dict(default = 1,
               __del__ = 0,
               Graphics = 1,
               properties = 0,
               DND = 0,
               context_menu = 0,
               Load = Dict(default = 1,
                           PSK = 1,
                           AI = 1,
                           echo_messages = 1),
               PS = 1,
               bezier = 0,
               styles = 1,
               tkext = 0,
               handles = 0,
               i18n = 0,
               timing = 0)

def pdebug(level, message, *args, **kw):
    if not Sketch.Base.preferences.print_debug_messages:
        return
    if level:
        if type(level) == StringType:
            level = (level,)
        enabled = _levels
        for item in level:
            try:
                enabled = enabled[item]
            except:
                break
        if type(enabled) == DictionaryType:
            enabled = enabled['default']
        if not enabled:
            return
    message = flexible_format(message, args, kw)
    write_error(message)
