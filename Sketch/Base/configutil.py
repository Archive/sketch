# Skencil - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#
#       This module defines some functions that set/read/save/load... the
#       settings and preferences
#

import sys, os

import Sketch, Sketch.Lib.util

import config
import connector

from warn import warn_tb, warn, USER
from const import CHANGED, SAVE_PREFERENCES

#
#       Directories..
#

def init_directories(base_dir):
    config.sketch_dir = base_dir
    dirs = ('pixmap_dir', 'fontmetric_dir', 'resource_dir', 'std_res_dir')
    join = os.path.join
    for dir in dirs:
        setattr(config, dir, join(base_dir, getattr(config, dir)))
    config.font_path.append(config.fontmetric_dir)
    check_path(config.font_path)

    config.user_home_dir = Sketch.Lib.util.gethome()
    config.user_config_dir = os.path.join(config.user_home_dir,
                                          config.user_config_dir)

def check_path(path):
    # remove all non-directory or non existing entries from path
    path[:] = filter(os.path.isdir, path)

#
#       User Config Settings
#

def load_user_preferences():
    # Load the user specific configuration.
    if os.path.isdir(config.user_config_dir):
        sys.path.insert(0, config.user_config_dir)

    try:
        import userhooks
    except ImportError:
        tb = sys.exc_info()[2]
        try:
            if tb.tb_next is not None:
                # The ImportError exception was raised from inside the
                # userhooks module.
                warn_tb(USER, "Cannot import the userhooks file")
            else:
                # There's no userhooks module.
                pass
        finally:
            del tb
    except:
        warn_tb(USER, "Cannot import the userhooks file")

class Preferences(connector.Publisher):

    # the keys of _runtime_prefs are the preferences that only exist
    # during runtime and that are not to be saved.
    _runtime_prefs = {}

    def __init__(self):
        connector.Subscribe(SAVE_PREFERENCES, self._save)

    def __setattr__(self, attr, value):
        self.__dict__[attr] = value
        self.issue(CHANGED, attr, value)

    def _load(self):
        filename = os.path.join(config.user_config_dir, self._filename)
        if os.path.isfile(filename):
            try:
                execfile(filename, self.__dict__)
            except:
                warn_tb(USER, "Cannot read the preferences file `%s'",
                        filename)
            if hasattr(self, '__builtins__'):
                del self.__builtins__

    def _save(self):
        if len(self.__dict__) == 0:
            return

        filename = os.path.join(config.user_config_dir, self._filename)
        try:
            Sketch.Lib.util.create_directory(config.user_config_dir)
            file = open(filename, 'w')
        except (IOError, os.error), value:
            import sys
            sys.stderr('cannot write preferences into %s: %s'
                       % (`filename`, value[1]))
            return

        file.write('#### -*- python -*-\n'
                   '# This file was automatically created by Skencil.\n\n')

        defaults = self.__class__.__dict__
        items = self.__dict__.items()
        items.sort()
        for key, value in items:
            if self._runtime_prefs.has_key(key) \
               or (defaults.has_key(key) and defaults[key] == value):
                continue
            file.write('%s = %s\n' % (key, `value`))
