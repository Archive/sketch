# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Patterns

A Pattern in Skencil is an objects that describes how an area should be
filled with color. This not only includes tiles patterns but also an
emtpy pattern for to indicate that the are should not be filled, a solid
pattern for a uniform color, gradients and more.
"""

__version__ = "$Revision$"
# $Source$
# $Id$


import math
from math import atan2, hypot, pi, sin, cos

from Sketch import Point, Rotation, Translation, Trafo, NullPoint
from Sketch.Base import NullUndo

from blend import Blend, MismatchError, BlendTrafo
import color


class Pattern:

    """Baseclass for all patterns.

    The main purpose of having a common baseclass is to provide default
    values for the type attributes that can be used it distinguish the
    different kinds of patterns.
    """

    is_procedural = 1
    is_Empty = 0
    is_Solid = 0
    is_Gradient = 0
    is_RadialGradient = 0
    is_AxialGradient = 0
    is_ConicalGradient = 0
    is_Hatching = 0
    is_Tiled = 0
    is_Image = 0

    preview = None

    def __init__(self, duplicate = None):
        pass

    def Execute(self, device, rect = None):
        pass

    def Transformed(self, trafo, rects = None):
        """Return a transformed copy of the pattern

        The trafo parameter is the transformation objects being applied
        to the object the pattern belongs to. The optional rects
        parameter if given is a tuple of two rects (BEFORE, AFTER) where
        BEFORE is the coord rect of the object before the transformation
        and AFTER the coord rect after the transformation. Derived
        classes that express some coordinates relative to the coord rect
        can use these rects to compute the new coordinate values.

        The default implementation just returns self.
        """
        return self

    def Duplicate(self):
        return self.__class__(duplicate = self)

    Copy = Duplicate


class EmptyPattern_(Pattern):

    """The Class of the EmptyPattern

    There is only one instance odf this class EmptyPattern. This
    constraint is not enforced but only the one instance should be used.

    An empty pattern used as the fill or line pattern of an object that
    the object is not or stroked.

    Check whether the is_Empty attribute is true to determine whether a
    given pattern is the empty pattern.
    """

    is_procedural = 0
    is_Empty = 1

    def Duplicate(self):
        return self

    Copy = Duplicate

    def Blend(self, *args):
        return self

    def SaveToFile(self, file):
        file.EmptyPattern()

    def __str__(self):
        return 'EmptyPattern'

EmptyPattern = EmptyPattern_()


class SolidPattern(Pattern):

    """Pattern for a solid fill with a single color

    Check whether the is_Solid attribute is true to determine whether a
    given pattern is a solid color pattern.
    """

    is_procedural = 0
    is_Solid = 1

    def __init__(self, color = None, duplicate = None):
        """Initialize the SolidPattern instance

        The color argument is required even though it current seems to
        be optional. It should be a color object.
        """
        if duplicate is not None:
            self.color = duplicate.color
        elif color is not None:
            self.color = color
        else:
            raise ValueError,'SolidPattern be must created with color argument'

    def __cmp__(self, other):
        if self.__class__ == other.__class__:
            return cmp(self.color, other.color)
        else:
            return cmp(id(self), id(other))

    def __str__(self):
        return 'SolidPattern(%s)' % `self.color`

    def Execute(self, device, rect = None):
        device.SetFillColor(self.color)

    def Blend(self, other, frac1, frac2):
        """Return a solid color as an interpolation of self and other.

        The parameter other musts be a SolidPattern instance as well,
        otherwise raise a MismatchError.

        The return value is a SolidPattern pattern instance with the
        color being the result of blending the color of self with the
        color of other using the frac12 and frac2 arguments as weights
        as in the Blend function.
        """
        if other.__class__ == self.__class__:
            return SolidPattern(Blend(self.color, other.color, frac1, frac2))
        else:
            raise MismatchError

    def Color(self):
        """Return the color of the solid pattern"""
        return self.color

    def SaveToFile(self, file):
        file.SolidPattern(self.color)


class GradientPattern(Pattern):

    """Base class for the gradient patterns

    To determine whether a given pattern is any of the gradient patterns
    check whether the is_Gradient attribute is true.

    Gradient patterns have two basic categories of attributes: the
    gradient and the gradient geometry.

    Mathematically speaking the gradient geometry defines a mapping from
    the plane into the interval [0.0, 1.0] and the color gradient
    defines a mapping from that interval into a color space, e.g. RGB.
    """

    is_Gradient = 1

    def __init__(self, gradient, duplicate = None):
        """Initialize the gradient pattern instance with the given gradient"""
        if duplicate is not None:
            Pattern.__init__(self, duplicate = duplicate)
            self.gradient = duplicate.gradient.Duplicate()
        elif gradient:
            self.gradient = gradient
        else:
            raise ValueError,\
                  'GradientPattern must be created with gradient argument'

    def Gradient(self):
        """Return the color gradient of the gradient pattern"""
        return self.gradient


class LinearGradient(GradientPattern):

    """A linear (or better axial) gradient pattern

    To determine whether a given pattern is a axial gradient pattern
    check whether the is_AxialGradient attribute is true.

    A linear gradient pattern has a color gradient, a direction and a
    border.

    The direction is a vector describing the direction of the color
    gradient in the document coordinate system. For historical reasons
    the vector effectively points from the end of the gradient to the
    start. E.g. with direction = Point(0, 1) the end color of the
    gradient is at the bottom and the start color at the top.

    The border determines the size of the region of solid color at each
    end of the gradient. The border is represented by a float in the
    range 0.0 to 0.5 inclusive. For 0.0 the region is effectively empty
    and for 0.5 the region with the actual gradient is reduced to a line
    orthogonal to the direction vector that separates to solid color
    areas.
    """

    is_AxialGradient = 1

    def __init__(self, gradient = None, direction = Point(0, -1),
                 border = 0, duplicate = None):
        """Initialize the LinearGradient instance

        Parameters:

        gradient -- The color gradient to use.
        direction -- The direction of the axial gradient as a Point instance
                     Default: Point(0, -1)
        border -- the border value as a float. Default: 0

        The default value of the direction leads to an axial gradient
        with the start color of the color gradient at the bottom and the
        end color at the top.
        """
        GradientPattern.__init__(self, gradient,
                                 duplicate = duplicate)
        self.direction = direction
        self.border = border
        if duplicate is not None:
            if duplicate.__class__ == self.__class__:
                self.direction = duplicate.direction
                self.border = duplicate.border
            elif duplicate.__class__ == ConicalGradient:
                self.direction = duplicate.direction
            elif duplicate.__class__ == RadialGradient:
                self.border = duplicate.border

    def WithGradient(self, gradient):
        """Return a copy of self with the specified gradient"""
        return LinearGradient(gradient, self.direction, self.border)

    def Direction(self):
        """Return the direction of the linear gradient"""
        return self.direction

    def WithDirection(self, direction):
        """Return a copy of self with the specified direction"""
        return LinearGradient(self.gradient, direction, self.border)

    def Border(self):
        """Return the border value of the linear gradient"""
        return self.border

    def WithBorder(self, border):
        """Return a copy of self with the specified border value"""
        return LinearGradient(self.gradient, self.direction, border)

    def Transformed(self, trafo, rects = None):
        """Return a transformed copy of the linear gradient

        The only thing that can change when transforming a linear
        gradient currently is the direction.

        Since the direction is orthogonal to the lines of equal color
        and a transformation for which that direction is invariant (e.g.
        a shear in the direction along the x-axis maps the vector (1, 0)
        to itself) should not affect the direction of the linear
        gradient the direction of the transformed pattern is determined
        as follows:

        1. Determine a vector v orthogonal to the direction

        2. Apply the linear part of the affine transformation trafo to v
           yielding v': v' = trafo.DTransform(v)

        3. The final direction is orthogonal to v'
        """
        dx, dy = self.direction
        dx, dy = trafo.DTransform(dy, -dx)
        dir = Point(dy, -dx).normalized()
        if dir * trafo.DTransform(self.direction) < 0:
            dir = -dir
        return LinearGradient(self.gradient, dir, self.border)

    def Execute(self, device, rect):
        if device.has_axial_gradient:
            self.execute_axial_gradient(device, rect)
            return

        SetFillColor = device.SetFillColor
        FillRectangle = device.FillRectangle
        steps = device.gradient_steps

        colors = self.gradient.Sample(steps)
        SetFillColor(colors[0])
        apply(device.FillRectangle, tuple(rect))

        device.PushTrafo()
        vx, vy = self.direction
        angle = atan2(vy, vx) - pi / 2
        center = rect.center()
        rot = Rotation(angle, center)
        left, bottom, right, top = rot(rect)
        device.Concat(rot)
        device.Translate(center)
        height = top - bottom
        miny = -height / 2
        height = height * (1.0 - self.border)
        width = right - left
        dy = height / steps
        y = height / 2
        x = width / 2
        for i in range(steps):
            SetFillColor(colors[i])
            FillRectangle(-x, y, +x, miny)
            y = y - dy
        device.PopTrafo()

    def execute_axial_gradient(self, device, rect):
        vx, vy = self.direction
        angle = atan2(vy, vx) - pi / 2
        center = rect.center()
        rot = Rotation(angle, center)
        left, bottom, right, top = rot(rect)
        height = (top - bottom) * (1.0 - self.border)
        trafo = rot(Translation(center))
        device.AxialGradient(self.gradient, trafo(0, height / 2),
                             trafo(0, -height / 2))

    def Blend(self, other, frac1, frac2):
        """Return a linear gradient as an interpolation of self and other.

        The parameter other musts be an instance of LinearGradient as
        well or an instance of SolidPattern, otherwise a MismatchError
        will be raised. A solid pattern will be treated as a linear
        gradient with a constant color gradient of the color of the
        solid pattern and direction and border the same as those of
        self.

        The return value is a LinearGradient instance with the gradient
        being the result of blending the gradient of self with the
        gradient of other using the frac12 and frac2 arguments as
        weights as in the Blend function.
        """
        if other.__class__ == self.__class__:
            gradient = other.gradient
            dir = other.direction
            border = other.border
        elif other.__class__ == SolidPattern:
            gradient = other.Color()
            dir = self.direction
            border = self.border
        else:
            raise MismatchError
        return LinearGradient(Blend(self.gradient, gradient, frac1, frac2),
                              frac1 * self.direction + frac2 * dir,
                              frac1 * self.border + frac2 * border)

    def SaveToFile(self, file):
        file.LinearGradientPattern(self.gradient, self.direction, self.border)


class RadialGradient(GradientPattern):

    """A radial gradient pattern

    To determine whether a given pattern is a radial gradient pattern
    check whether the is_RadialGradient attribute is true.

    A linear gradient pattern has a color gradient, a center and a
    border.

    The center is the center point of the gradient given as a Point
    object with coordinates relative to the coord_rect of the object the
    gradient is used with.

    The border value is a float in the range 0.0 to 1.0 and gives the
    size of the region of a solid color at the outer side of the
    gradient. With a border of 0.0 the actual gradient covers a circle
    around the center with radius such that the furthest corner of the
    coord_rectangle of the object is on the periphery of the circle.
    With a border value of 1.0 the circle is just the center point.
    """

    is_RadialGradient = 1

    def __init__(self, gradient = None, center = Point(0.5, 0.5),
                 border = 0, duplicate = None):
        """Initialize the RadialGradient instance

        Parameters:

        gradient -- The color gradient to use.
        center -- The center of raidal gradient as a Point instance
                  Default: Point(0.5, 0.5) i.e. the center of the coord_rect
        border -- the border value as a float. Default: 0
        """
        GradientPattern.__init__(self, gradient,
                                 duplicate = duplicate)
        self.center = center
        self.border = border
        if duplicate is not None:
            if duplicate.__class__ == self.__class__:
                self.center = duplicate.center
                self.border = duplicate.border
            elif duplicate.__class__ == ConicalGradient:
                self.center = duplicate.center
            elif duplicate.__class__ == LinearGradient:
                self.border = duplicate.border

    def WithGradient(self, gradient):
        """Return a copy of self with a different gradient"""
        return RadialGradient(gradient, self.center, self.border)

    def Center(self):
        """Return the center of the radial gradient"""
        return self.center

    def WithCenter(self, center):
        """Return a copy of self with a different center"""
        return RadialGradient(self.gradient, center, self.border)

    def Border(self):
        """Return the border value of the radial gradient"""
        return self.border

    def WithBorder(self, border):
        """Return a copy of self with a different border value"""
        return RadialGradient(self.gradient, self.center, border)

    def Transformed(self, trafo, rects = None):
        """Return a transformed copy of self.

        The rects argument, if given, should be a keyword argument and
        should be a tuple of two rects the coord_rect of the object the
        gradient is used with before and after the transformation has
        been applied to the object.

        If the rect argument is not given the return values is
        effectively the same radial gradient as the center can only be
        transformed in a meaningful way if its absolute position on the
        page is known and for that the rectangles are required.

        The center is transformed in the following way:

        1. Determine the coordinates of the center in document
           coordinates, C, using the first rectangle.

        2. Apply the affine transformation trafo to the absoute center
           yielding C'

        3. Express C' in the coordinate system of the second rectangle
           and use that as the center of the transformed radial
           gradient.
        """
        if rects:
            r1, r2 = rects
            left, bottom, right, top = r1
            cx, cy = self.center
            cx = cx * right + (1 - cx) * left
            cy = cy * top   + (1 - cy) * bottom
            cx, cy = trafo(cx, cy)
            left, bottom, right, top = r2
            len = right - left
            if len:
                cx = (cx - left) / len
            else:
                cx = 0
            len = top - bottom
            if len:
                cy = (cy - bottom) / len
            else:
                cy = 0
            center = Point(cx, cy)
        else:
            center = self.center

        return RadialGradient(self.gradient, center, self.border)

    def Execute(self, device, rect):
        if device.has_radial_gradient:
            self.execute_radial(device, rect)
            return
        steps = device.gradient_steps
        cx, cy = self.center
        cx = cx * rect.right + (1 - cx) * rect.left
        cy = cy * rect.top   + (1 - cy) * rect.bottom
        radius = max(hypot(rect.left - cx, rect.top - cy),
                     hypot(rect.right - cx, rect.top - cy),
                     hypot(rect.right - cx, rect.bottom - cy),
                     hypot(rect.left - cx, rect.bottom - cy))
        color = self.gradient.ColorAt
        SetFillColor = device.SetFillColor
        FillCircle = device.FillCircle
        SetFillColor(color(0))
        apply(device.FillRectangle, tuple(rect))
        radius = radius * (1.0 - self.border)
        dr = radius / steps
        device.PushTrafo()
        device.Translate(cx, cy)
        center = NullPoint
        for i in range(steps):
            SetFillColor(color(float(i) / (steps - 1)))
            FillCircle(center, radius)
            radius = radius - dr
        device.PopTrafo()

    def execute_radial(self, device, rect):
        cx, cy = self.center
        cx = cx * rect.right + (1 - cx) * rect.left
        cy = cy * rect.top   + (1 - cy) * rect.bottom
        radius = max(hypot(rect.left - cx, rect.top - cy),
                     hypot(rect.right - cx, rect.top - cy),
                     hypot(rect.right - cx, rect.bottom - cy),
                     hypot(rect.left - cx, rect.bottom - cy))
        radius = radius * (1.0 - self.border)
        device.RadialGradient(self.gradient, (cx, cy), radius, 0)

    def Blend(self, other, frac1, frac2):
        """Return a radial gradient as an interpolation of self and other.

        The parameter other musts be an instance of RadialGradient as
        well or an instance of SolidPattern, otherwise a MismatchError
        will be raised. A solid pattern will be treated as a radial
        gradient with a constant color gradient of the color of the
        solid pattern and center and the border the same as those of
        self.

        The return value is a RadialGradient instance with the gradient
        being the result of blending the gradient of self with the
        gradient of other using the frac12 and frac2 arguments as
        weights as in the Blend function.
        """
        if other.__class__ == self.__class__:
            gradient = other.gradient
            center = other.center
            border = other.border
        elif other.__class__ == SolidPattern:
            gradient = other.Color()
            center = self.center
            border = self.border
        else:
            raise MismatchError
        return RadialGradient(Blend(self.gradient, gradient, frac1, frac2),
                              frac1 * self.center + frac2 * center,
                              frac1 * self.border + frac2 * border)

    def SaveToFile(self, file):
        file.RadialGradientPattern(self.gradient, self.center, self.border)



class ConicalGradient(GradientPattern):

    """A conical gradient

    To determine whether a given pattern is a conical gradient pattern
    check whether the is_ConicalGradient attribute is true.

    A linear gradient pattern has a color gradient and a center.

    The center is the center point of the gradient given as a Point
    object with coordinates relative to the coord_rect of the object the
    gradient is used with.

    The direction points in the direction of the ray with the start
    color of the color gradient.
    """

    is_ConicalGradient = 1

    def __init__(self, gradient = None,
                 center = Point(0.5, 0.5), direction = Point(1, 0),
                 duplicate = None):
        """Initialize the ConicalGradient instance

        Parameters:

        gradient -- The color gradient to use.
        center -- The center of raidal gradient as a Point instance
                  Default: Point(0.5, 0.5) i.e. the center of the coord_rect
        direction -- The direction of the ray with the start color of
                     the gradient. Default Point(1, 0)
        """
        GradientPattern.__init__(self, gradient, duplicate = duplicate)
        self.center = center
        self.direction = direction
        if duplicate is not None:
            if duplicate.__class__ == self.__class__:
                self.center = duplicate.center
                self.direction = duplicate.direction
            elif duplicate.__class__ == LinearGradient:
                self.direction = duplicate.direction
            elif duplicate.__class__ == RadialGradient:
                self.center = duplicate.center

    def Transformed(self, trafo, rects = None):
        """Return a transformed copy of self.

        The rects argument, if given, should be a keyword argument and
        should be a tuple of two rects the coord_rect of the object the
        gradient is used with before and after the transformation has
        been applied to the object.

        If the rects argument is geven, the center is transformed in the
        following way:

        1. Determine the coordinates of the center in document
           coordinates, C, using the first rectangle.

        2. Apply the affine transformation trafo to the absoute center
           yielding C'

        3. Express C' in the coordinate system of the second rectangle
           and use that as the center of the transformed radial
           gradient.

        If the rects argument is not given or None, the center of the
        return value will be self.center.

        The direction of the transformed conical gradient is the
        normalized return value of the transformation's DTransform
        method applied to self.direction.
        """
        dir = trafo.DTransform(self.direction).normalized()
        if rects:
            r1, r2 = rects
            left, bottom, right, top = r1
            cx, cy = self.center
            cx = cx * right + (1 - cx) * left
            cy = cy * top   + (1 - cy) * bottom
            cx, cy = trafo(cx, cy)
            left, bottom, right, top = r2
            len = right - left
            if len:
                cx = (cx - left) / len
            else:
                cx = 0
            len = top - bottom
            if len:
                cy = (cy - bottom) / len
            else:
                cy = 0
            center = Point(cx, cy)
        else:
            center = self.center

        return ConicalGradient(self.gradient, center, dir)

    def WithGradient(self, gradient):
        """Return a copy of self with a different gradient"""
        return ConicalGradient(gradient, self.center, self.direction)

    def Center(self):
        """Return the center of the conical gradient"""
        return self.center

    def WithCenter(self, center):
        """Return a copy of self with a different center"""
        return ConicalGradient(self.gradient, center, self.direction)

    def Direction(self):
        """Return the direction of the conical gradient"""
        return self.direction

    def WithDirection(self, direction):
        """Return a copy of self with a different direction"""
        return ConicalGradient(self.gradient, self.center, direction)

    def Execute(self, device, rect):
        if device.has_conical_gradient:
            self.execute_conical(device, rect)
            return
        steps = device.gradient_steps
        cx, cy = self.center
        left, bottom, right, top = rect
        cx = cx * right + (1 - cx) * left
        cy = cy * top   + (1 - cy) * bottom
        vx, vy = self.direction
        angle = atan2(vy, vx)
        rot = Rotation(angle, cx, cy)
        radius = max(hypot(left - cx, top - cy),
                     hypot(right - cx, top - cy),
                     hypot(right - cx, bottom - cy),
                     hypot(left-cx,bottom-cy)) + 10
        device.PushTrafo()
        device.Concat(rot)
        device.Translate(cx, cy)
        device.Scale(radius)
        colors = self.gradient.Sample(steps)
        SetFillColor = device.SetFillColor
        FillPolygon = device.FillPolygon
        da = pi / steps
        points = [(1, 0)]
        for i in range(steps):
            a = da * (i + 1)
            x = cos(a); y = sin(a)
            points.insert(0, (x, y))
            points.append((x, -y))
        colors.reverse()
        SetFillColor(colors[0])
        FillPolygon(points)
        points.insert(0, (0, 0))
        for i in range(steps):
            SetFillColor(colors[i])
            del points[1]
            del points[-1]
            FillPolygon(points)
        device.PopTrafo()

    def execute_conical(self, device, rect):
        cx, cy = self.center
        left, bottom, right, top = rect
        cx = cx * right + (1 - cx) * left
        cy = cy * top   + (1 - cy) * bottom
        angle = self.direction.polar()[1]
        device.ConicalGradient(self.gradient, (cx, cy), angle)

    def Blend(self, other, frac1, frac2):
        """Return a conical gradient as an interpolation of self and other.

        The parameter other musts be an instance of ConicalGradient as
        well or an instance of SolidPattern, otherwise a MismatchError
        will be raised. A solid pattern will be treated as a conical
        gradient with a constant color gradient of the color of the
        solid pattern and center and direction the same as those of
        self.

        The return value is a ConicalGradient instance with the gradient
        being the result of blending the gradient of self with the
        gradient of other using the frac12 and frac2 arguments as
        weights as in the Blend function.
        """
        if other.__class__ == self.__class__:
            gradient = other.gradient
            dir = other.direction
            center = other.center
        elif other.__class__ == SolidPattern:
            gradient = other.Color()
            dir = self.direction
            center = self.center
        else:
            raise MismatchError
        return ConicalGradient(Blend(self.gradient, gradient, frac1, frac2),
                               frac1 * self.center +  frac2 * center,
                               frac1 * self.direction + frac2 * dir)

    def SaveToFile(self, file):
        file.ConicalGradientPattern(self.gradient, self.center, self.direction)


class HatchingPattern(Pattern):

    """A Hatching pattern

    To determine whether a given pattern is a hatching pattern check
    whether the is_Hatching attribute is true.

    A hatching pattern consists of (conceptually) infinitely many
    parallel lines on a solid background. Thus the hatching pattern is
    defined by two colors, the forgreound color in which the lines are
    drawn and the solid background color, as well as a direction of the
    lines their width and the spacing between them. The direction is
    represented by a Point instance used as a vector pointing along the
    lines.
    """

    is_Hatching = 1

    def __init__(self, foreground = None, background = None,
                 direction = Point(1, 0),
                 spacing = 5.0, width = 0.5, duplicate = None):
        """Initialize the HatchingPattern

        Parameters:

        foreground, background -- The fore- and background colors

        direction -- The direction of the lines.
                     Default: Point(1, 0), i.e. the lines are horizontal.

        spacing -- The distance between the lines in points. Default 5.0

        width -- The width of the lines in point. Default 0.5
        """
        if duplicate is not None:
            self.foreground = duplicate.foreground
            self.background = duplicate.background
            self.spacing = duplicate.spacing
            self.width = duplicate.width
            self.direction = duplicate.direction
        elif foreground:
            self.foreground = foreground
            if not background:
                background = color.StandardColors.white
            self.background = background
            self.spacing = spacing
            self.width = width
            self.direction = direction
        else:
            raise ValueError,\
                  'HatchingPattern must be created with color argument'

    def Direction(self):
        """Return the direction of self"""
        return self.direction

    def WithDirection(self, direction):
        """Return a copy of the pattern with a new direction"""
        return HatchingPattern(self.foreground, self.background, direction,
                               self.spacing, self.width)

    def Spacing(self):
        """Return the spacing of the pattern"""
        return self.spacing

    def Width(self):
        """Return the line width of the pattern"""
        return self.width

    def Transformed(self, trafo, rects = None):
        # XXX: should spacing be transformed as well? Should the pattern be
        # transformed at all?
        dir = trafo.DTransform(self.direction).normalized()
        return HatchingPattern(self.foreground, self.background, dir,
                               self.spacing, self.width)

    def Foreground(self):
        """Return the foreground color of the pattern"""
        return self.foreground

    def WithForeground(self, color):
        """Return a copy of the pattern with a new foreground color"""
        return HatchingPattern(color, self.background, self.direction,
                               self.spacing, self.width)

    def Background(self):
        """Return the background color of the pattern"""
        return self.background

    def WithBackground(self, color):
        """Return a copy of the pattern with a new background color"""
        return HatchingPattern(self.foreground, color, self.direction,
                               self.spacing, self.width)

    def Execute(self, device, rect):
        left, bottom, right, top = rect
        dy = self.spacing
        if dy > 0:
            device.SetFillColor(self.background)
            device.FillRectangle(left, top, right, bottom)
            device.PushTrafo()
            vx, vy = self.direction
            angle = atan2(vy, vx)
            center = rect.center()
            rot = Rotation(angle, center)
            left, bottom, right, top = rot(rect)
            device.Concat(rot)
            device.Translate(center)
            height = top - bottom
            width = right - left
            steps = int(height / dy + 1)
            y = height / 2
            x = width / 2
            device.SetLineColor(self.foreground)
            device.SetLineAttributes(self.width)
            drawline = device.DrawLineXY
            for i in range(steps):
                drawline(-x, y, +x, y)
                y = y - dy
            device.PopTrafo()
        else:
            device.SetFillColor(self.foreground)
            device.FillRectangle(left, bottom, right, top)

    def Blend(self, other, frac1, frac2):
        """Return a hatching pattern as an interpolation of self and other.

        The parameter other musts be an instance of HatchingPattern as
        well or an instance of SolidPattern, otherwise a MismatchError
        will be raised. A solid pattern will be treated as a hatching
        pattern with the color of the solid pattern as foreground and
        background color and direction, width and spacing the same as
        those of self.

        The return value is a ConicalGradient instance with the gradient
        being the result of blending the gradient of self with the
        gradient of other using the frac12 and frac2 arguments as
        weights as in the Blend function. All other parameters are the
        weighted averages of the corresponding values in self and other.
        """
        if other.__class__ == self.__class__:
            fg = other.foreground
            bg = other.background
            dir = other.direction
            spacing = other.spacing
            width = other.width
        elif other.__class__ == SolidPattern:
            fg = bg = other.Color()
            dir = self.direction
            spacing = self.spacing
            width = self.width
        else:
            raise MismatchError
        return HatchingPattern(Blend(self.foreground, fg, frac1, frac2),
                               Blend(self.background, bg, frac1, frac2),
                               frac1 * self.direction + frac2 * dir,
                               frac1 * self.spacing + frac2 * spacing,
                               frac1 * self.width + frac2 * width)

    def SaveToFile(self, file):
        file.HatchingPattern(self.foreground, self.background,
                             self.direction, self.spacing, self.width)


class ImageTilePattern(Pattern):

    """A pattern with tiled raster images.

    The ImageTilePattern is represented by a raster image and a
    transformation. The raster image should be an instance of the
    Sketch.Graphics.ImageData class.

    To determine whether a given pattern is a tiled image pattern check
    whether both the is_Image and is_Tiled attributes are true.
    """

    is_Tiled = 1
    is_Image = 1
    data = None

    def __init__(self, data = None, trafo = None, duplicate = None):
        """Initialize the ImageTilePattern

        Parameters:
        data -- The image to use as an ImageData instance
        trafo -- affine transformation. Default: Trafo(1, 0, 0, -1, 0, 0)
        """
        if duplicate is not None:
            data = duplicate.data
            self.trafo = duplicate.trafo
        else:
            if trafo is None:
                trafo = Trafo(1, 0, 0, -1, 0, 0)
            self.trafo = trafo
        self.data = data

    def Trafo(self):
        """Return the transformation used in the pattern"""
        return self.trafo

    def Transformed(self, trafo, rects = None):
        if rects:
            r1, r2 = rects
            trafo = trafo(Translation(r1.left, r1.top))
            trafo = Translation(-r2.left, -r2.top)(trafo)
        return ImageTilePattern(self.data, trafo(self.trafo))

    def Image(self):
        """Return the image data object used in the pattern"""
        return self.data

    def WithImage(self, data):
        """Return a copy of the pattern with a different image data object"""
        return ImageTilePattern(data, self.trafo)

    def Execute(self, device, rect):
        device.TileImage(self.data,
                         Translation(rect.left, rect.top)(self.trafo))

    def Blend(self, other, frac1, frac2):
        """
        Return a tiled image pattern with the interpolation of self and other

        The parameter other musts be an instance of ImageTilePattern as
        well using the same image data object. Otherwise a MismatchError
        will be raised.

        The returned ImageTilePattern has the same data object as self
        and other and a transformation determined by the BlendTrafo
        function and with the weights frac1 and frac2.
        """
        if self.__class__ == other.__class__:
            if self.data is other.data:
                return self.__class__(self.data,
                                      BlendTrafo(self.trafo, other.trafo,
                                                 frac1, frac2))
        raise MismatchError

    def SaveToFile(self, file):
        file.ImageTilePattern(self.data, self.trafo)
