# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000, 2003, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
Properties and Styles

In Skencil, properties are the attributes of an object that describe how
it's rendered such as the fill pattern or the line thickness.

Styles are collections of properties, e.g. a line thickness and a line
color. There are static and dynamic styles. Dynamic styles can be shared
between objects and all objects using a particular dynamic style can be
updated simultaneously when the style changes.

A property stack is a list of styles stacked on top of one another. The
value of a property in a property stack is the value of the property in
the top-most property that has the property.
"""

__version__ = "$Revision$"
# $Source$
# $Id$

import Sketch
from Sketch import _
from Sketch.Base import Publisher, CreateListUndo, UndoAfter, NullUndo, \
     SketchInternalError, const
from Sketch.Base.warn import pdebug
CHANGED = const.CHANGED

from pattern import SolidPattern, EmptyPattern
from color import StandardColors
from blend import Blend


class Style(Publisher):

    """
    A collection of properties.

    A style may be either dynamic or static. Most styles used in a
    PropertyStack are static. Static styles belong to only one object.

    Dynamic styles differ from static styles by having a name and that
    they may be shared by several objects. Dynamix styles are managed by
    the document object. If the user modifies a dynamic style all
    objects that use the style are automatically updated.

    Dynmic styles can be distinguished from static styles by the
    is_dynamic attribute. Iff it's true the style is dynamic.
    """

    is_dynamic = 0
    name = ''

    def __init__(self, name = '', duplicate = None, **kw):
        """Initialize the style.

        All keyword arguments besides 'name' and 'duplicate' are assumed
        to be the properties of the style.

        If the 'name' parameter is given use it as the name of the
        style. It defaults to the empty string.

        If 'duplicate' parameter if given and not None assume it's
        another style and make self a duplicate of that style.
        """
        if duplicate is not None:
            self.__dict__ = duplicate.__dict__.copy()
            if hasattr(self, 'fill_pattern'):
                self.fill_pattern = self.fill_pattern.Copy()
            if hasattr(self, 'line_pattern'):
                self.line_pattern = self.line_pattern.Copy()
        else:
            if name:
                self.name = name
            for key, value in kw.items():
                setattr(self, key, value)

    def SetProperty(self, prop, value):
        """Set the property named by prop to the given value.

        Issue a CHANGED message and return undo information.
        """
        dict = self.__dict__
        if dict.has_key(prop):
            undo = (self.SetProperty, prop, dict[prop])
        else:
            undo = (self.DelProperty, prop)
        if prop == 'fill_pattern' or prop == 'line_pattern':
            value = value.Copy()
        dict[prop] = value
        self.issue(CHANGED, self)
        return undo

    def DelProperty(self, prop):
        """Delete the property named by prop.

        Issue a CHANGED message and return undo information.
        """
        undo = (self.SetProperty, prop, getattr(self, prop))
        delattr(self, prop)
        self.issue(CHANGED, self)
        return undo

    def Duplicate(self):
        """Return a duplicate of self.

        If the style is dynamic return self otherwise a copy.
        """
        if self.is_dynamic:
            return self
        return self.__class__(duplicate = self)

    def Copy(self):
        """Return a copy of self.

        This method differs from Duplicate() in that it always makes a
        copy, even for dynamic styles.
        """
        return self.__class__(duplicate = self)

    def Name(self):
        """Return the name of the style."""
        return self.name

    def SetName(self, name):
        """Set the name of the style and return undo information."""
        undo = self.SetName, self.name
        self.name = name
        return undo

    def AsDynamicStyle(self):
        """Return a dynamic style initialized as a copy of self."""
        result = self.Copy()
        result.is_dynamic = 1
        return result

    def AsUndynamicStyle(self):
        """Return a static style as a copy of self."""
        result = self.Copy()
        if self.is_dynamic:
            del result.is_dynamic
            del result.name
        return result

    def SaveToFile(self, file):
        if self.is_dynamic:
            file.DynamicStyle(self)

    def IsEmpty(self):
        return not self.__dict__

def FillStyle(pattern):
    """Return a static style with fill_pattern set to the given pattern"""
    return Style(fill_pattern = pattern)

# As a convenience a style with fill_pattern set to the empty pattern
EmptyFillStyle = Style(fill_pattern = EmptyPattern)

def LineStyle(color = None, width = 0, cap  = const.CapButt,
              join  = const.JoinMiter, dashes = None,
              arrow1 = None, arrow2 = None):
    """Return a static style with the given line properties"""
    return Style(line_pattern = SolidPattern(color), line_width = width,
                 line_cap = cap, line_join = join, line_dashes = dashes,
                 line_arrow1 = arrow1, line_arrow2 = arrow2)

# Convenience functions. for solid lines and 
SolidLine = LineStyle
EmptyLineStyle = Style(line_pattern = EmptyPattern)

class PropertyStack:

    """
    A Stack of styles

    A PropertyStack manages the properties and styles for an object in
    Skencil.

    Because an object may have several dynamic styles in addition to
    properties not managed by a dynamic style the PropertyStack has a
    stack of styles (i.e. instances of Style) that defines the
    properties actually used to render the object.

    The stack consists of a base style which has to contain values for
    all properties the object supports and any number of styles layered
    on top of that. Properties in styles that are higher in the stack
    override those of the styles below.

    Styles added to the stack are added at the top so that they override
    all previous values for the properties that the new style contains.

    For convenient access the value of a property of a PropertyStack can
    be read as an attribute of the PropertyStack instance. E.g. given a
    PropertyStack stack the fill pattern is stack.fill_pattern.
    """

    # Implementation note:
    #
    # The stack is stored in the instance variable stack which is a list
    # of styles *from top to bottom*, i.e. the top most item is
    # self.stack[0]


    # Default value of the instance variable update_cache. See
    # __getattr__ for more information.
    update_cache = 1

    def __init__(self, base = None, duplicate = None):
        """Initialize the property stack.

        If duplicate is given, assume that it's another PropertyStack
        and initialize self as a duplicate of it which means that the
        styles used in self are duplicates of the styles in the other
        stack.

        If duplicate is not given or None and base is given it must be
        the base style to use. The base style has to contain values for
        all properties the object may have so that the PropertyStack
        always has a value for any property that may be needed.

        If base style is omitted or None use a duplicate of the
        factory_defaults style
        """
        if duplicate is not None:
            self.stack = []
            for layer in duplicate.stack:
                self.stack.append(layer.Duplicate())
        else:
            if base is None:
                base = factory_defaults.Duplicate()
            self.stack = [base]

    def __getattr__(self, attr):
        """Implement accessing the properties as attributes

        In Python this method is only called when the instance
        dictionary self.__dict__ does not contain a value for the
        requested attribute so we get automatic caching of the property
        values by simply deleting them from the __dict__ when the stack
        changes and rebuilding __dict__ when __getattr__ is called the
        next time.

        If self.update_cache is true we have to rebuild __dict__.
        """
        if self.update_cache:
            # Rebuild the cache
            cache = self.__dict__
            stack = self.stack[:]
            stack.reverse()
            for layer in stack:
                cache.update(layer.__dict__)
            self.update_cache = 0

        # If it's a valid attribute or property name it should work now.
        # Otherwise raise AttributeError as required by Python.
        try:
            return self.__dict__[attr]
        except KeyError:
            raise AttributeError, attr

    def _clear_cache(self):
        """Internal method: Clear the 'properties as attributes' cache
        
        Remove all properties from self.__dict__. Everything else is
        handled by __getattr__ automatically.

        Return undo info which simply calls this method again so that we
        can use it when constructing other undo info that requires
        resetting the cache.
        """
        # We can simply make __dict__ contain nothing but the stack
        # list. We don't even need to set update_cache as it has a
        # class-level default value of true.
        self.__dict__ = {'stack' : self.stack}
        return (self._clear_cache,)

    def prop_layer(self, prop):
        """Internal: Return the topmost style containing the property prop

        If there's no style containing the property raise
        SketchInternalError.
        """
        # return property layer containing PROP
        for item in self.stack:
            if hasattr(item, prop):
                return item
        # we should never reach this...
        raise SketchInternalError('unknown graphics property "%s"' % prop)

    def set_property(self, prop, value):
        """Internal Method: Set the property named prop to the given value

        Determine the topmost style containg the property and if it's a
        dynamic style add the property to a static style at the top
        creating the static style if necessary. If it's a static style
        modify that style in place.

        Return undo information.
        """
        layer = self.prop_layer(prop)
        if layer.is_dynamic:
            if self.stack[0].is_dynamic:
                layer = Style()
                setattr(layer, prop, value)
                return self.add_layer(layer)
            else:
                layer = self.stack[0]
        return layer.SetProperty(prop, value)

    def SetProperty(self, **kw):
        """Set all the properties given by the keyword arguments

        Return undo information.
        """
        stack = self.stack
        undo = []
        append = undo.append

        # In both cases iterate trough the keyword args, set the
        # properties and collect the undo information in the list undo.
        if len(stack) == 1 and not stack[0].is_dynamic:
            # optimize the common special case of exactly one item on
            # the stack.
            set = stack[0].SetProperty
            for prop, value in kw.items():
                append(set(prop, value))
        else:
            set = self.set_property
            for prop, value in kw.items():
                append(set(prop, value))

        # Check whether some styles are now shadowed and if so delete
        # them and save the undo info.
        if len(self.stack) > 1:
            undo_stack = (self.set_stack, self.stack[:])
            if self.delete_shadowed_layers():
                undo.append(undo_stack)

        # package all the undo info, clear the cache and add a
        # _clear_cache call to the undo info making sure that it will be
        # called after the undo was performed.
        undo = CreateListUndo(undo)
        undo = (UndoAfter, undo, self._clear_cache())
        return undo

    def set_stack(self, stack):
        """Internal: Set the whole stack, clear the cache and return undo info.

        This method is mainly useful as a helper for methods that
        manipulate the stack when constructing the undo information.
        """
        undo = (self.set_stack, self.stack)
        self.stack = stack
        self._clear_cache()
        return undo

    def delete_shadowed_layers(self):
        """Internal: remove styles that don't influence the properties anymore.

        Since every style in the stack overrides the ones below it it
        can happen that a layer doesn't contribute anything anymore
        because all it's properties are overridden by the styles above
        it. Such a style is called a shadowed style or layer. This
        method removes such layers.

        Return whether the stack was modified.
        """
        # check if some styles are completely hidden now
        stack = self.stack
        layers = []
        dict = {'name':1, 'is_dynamic':0}
        dict.update(stack[0].__dict__)
        length = len(dict)
        for layer in stack[1:]:
            dict.update(layer.__dict__)
            if length != len(dict):
                layers.append(layer)
            length = len(dict)
        length = len(stack)
        stack[1:] = layers
        return length != len(stack)

    def add_layer(self, layer):
        """Internal: Add the layer on top of the stack and return undo info.
        """
        undo = (self.set_stack, self.stack[:])
        self.stack.insert(0, layer)
        return undo
    load_AddStyle = add_layer

    def AddStyle(self, style):
        """Add the style to the stack and return undo info.

        If the style is dynamic it's simply inserted as the top most
        item. A static style is merged property by property with the
        stack so that it usually just modifies the static style on top
        of the stack.
        """
        if style.is_dynamic:
            undo = self.add_layer(style)
            self.delete_shadowed_layers()
            self._clear_cache()
            return undo
        else:
            return apply(self.SetProperty, (), style.__dict__)


    def HasFill(self):
        """Return true iff the fill_pattern is not the EmptyPattern"""
        return self.fill_pattern is not EmptyPattern

    def IsAlgorithmicFill(self):
        """Return true iff the fill_pattern is algorithmic"""
        return self.fill_pattern.is_procedural

    def ExecuteFill(self, device, rect = None):
        """Call the fill_patterns Execute method with the device and rect"""
        device.SetFillOpacity(self.fill_opacity)
        self.fill_pattern.Execute(device, rect)

    def HasLine(self):
        """Return true iff the line_pattern is not the EmptyPattern"""
        return self.line_pattern is not EmptyPattern

    def IsAlgorithmicLine(self):
        """Return true iff the fill_pattern is algorithmic"""
        return self.line_pattern.is_procedural

    def ExecuteLine(self, device, rect = None):
        """Call the line_patterns Execute method with the device and rect"""
        line_pattern = self.line_pattern
        if line_pattern is not EmptyPattern:
            device.SetLineOpacity(1.0)
            line_pattern.Execute(device, rect)
            device.SetLineAttributes(self.line_width, self.line_cap,
                                     self.line_join, self.line_dashes)

    def HasFont(self):
        """Return true if the font property is not None"""
        return self.font is not None

    def Styles(self):
        """return a list of the styles in the stack.

        The styles are ordered from top to bottom.
        """
        return self.stack[:]

    def ObjectChanged(self, object):
        """Call with a dynamic style that has been changed.

        If the style is in the stack clear the cache and return true.
        Otherwise return false.
        """
        if object in self.stack:
            self._clear_cache()
            return 1
        return 0

    def ObjectRemoved(self, object):
        """Call with a dynamic style that was remove from the document.

        If the style is in the stack replace it with an undynamic
        version of itself. In any case return undo information.
        """
        if object in self.stack:
            idx = self.stack.index(object)
            undo = (self.set_stack, self.stack[:])
            self.stack[idx] = self.stack[idx].AsUndynamicStyle()
            pdebug('properties', 'made style undynamic')
            return undo
        return NullUndo

    def Untie(self):
        """Untie the stack from the document

        Replace every dynamic style with an undynamic copy. Return
        information to pass to Tie() for tying it in again.

        When an object is copied to the clip-board for instance it has
        to cut all its ties with the document which among other things
        means that it mustn't reference dynamic styles because they're
        managed by the document. However when the object is later pasted
        into the same document again it should use the dynamic styles
        again.
        """
        info = []
        for i in range(len(self.stack)):
            style = self.stack[i]
            if style.is_dynamic:
                self.stack[i] = style.AsUndynamicStyle()
                info.append((i, style))
        self._clear_cache()
        return info

    def Tie(self, document, info):
        """Tie the stack back into the document.

        Given the tying information info as returned by the Untie
        method, replace every static style that used to be a dynamic
        style with the dynamic style if the dynamic style hasn't changed
        since the untying.
        """
        for i, style in info:
            s = document.GetDynamicStyle(style.Name())
            if s == style:
                self.stack[i] = s
        self._clear_cache()

    def Duplicate(self):
        """Return a duplicate of the stack"""
        return self.__class__(duplicate = self)


    # Helper constance for GrowAmount()
    grow_join = [5.240843064, 0.5, 0.5]
    grow_cap = [None, 0.5, 0.5, 0.70710678]

    def GrowAmount(self):
        """
        Return by how much the coord rect has to be grown to cover the object.

        The return value is the amount in point as a float. The amount
        is big enough but for thick lines and mitered joins it's
        unfortunately too big because it's meant for the worst possible
        case.
        """
        return self.line_width * max(self.grow_cap[self.line_cap],
                                     self.grow_join[self.line_join])

    def Blend(self, other, frac1, frac2):
        """Return a linear interpolation between self and another stack.

        Blend the values of each property of the two property stacks
        with the weights frac1 and frac2 and return a property stack
        with a single style that contains the interpolated properties.
        """
        result = {}
        for prop, func in blend_functions:
            if func:
                result[prop] = func(getattr(self, prop), getattr(other, prop),
                                    frac1, frac2)
            else:
                result[prop] = getattr(self, prop)
        return PropertyStack(apply(Style, (), result))

    def Transform(self, trafo, rects):
        """Apply the affine transformation trafo to the properties.

        The parameter rects must be a tuple of two coord-rects of the
        object, the one before the transformation was applied and the
        one afterwards.

        Return undo information.
        """
        # XXX hardcoding which properties may need to be transformed is
        # not really a good idea, but it's significantly faster.
        undo = NullUndo
        if self.fill_transform:
            pattern = self.fill_pattern
            if pattern.is_procedural:
                transformed = pattern.Transformed(trafo, rects)
                if transformed is not pattern:
                    undo = self.set_property('fill_pattern', transformed)
        if undo is not NullUndo:
            undo = (UndoAfter, undo, self._clear_cache())
        return undo

    def CreateStyle(self, which_properties):
        """Create a style with the properties listed in which_properties."""
        properties = {}
        for prop in which_properties:
            if property_types[prop] == FontProperty and not self.HasFont():
                continue
            properties[prop] = getattr(self, prop)
        return apply(Style, (), properties)

    def DynamicStyleNames(self):
        """Return a list with the names of all the dynamic styles in the stack
        """
        names = []
        for style in self.stack:
            if style.is_dynamic:
                names.append(style.Name())
        return names

    def condense(self):
        """Merge contiguous sequences of static styles into one style.

        This is only needed in very special circumstances. The only
        place where it makes sense at all, probably, is in an import
        filter for very old Skencil documents which were written by a
        Skencil version that didn't try to avoid consecutive static
        styles in the first place.
        """
        stack = self.stack
        last = stack[0]
        for style in stack[1:]:
            if not last.is_dynamic and not style.is_dynamic:
                dict = style.__dict__.copy()
                dict.update(last.__dict__)
                last.__dict__ = dict
            last = style
        length = len(stack)
        self.delete_shadowed_layers()

    def SaveToFile(self, file):
        file.Properties(self)

# Singleton class to use as the return value of the Properties method in
# objects that don't have any properties such as compounds.
class _EmptyProperties:
    def HasFill(self):
        return 0
    HasLine = HasFill
    HasFont = HasFill

    def DynamicStyleNames(self):
        return []

EmptyProperties = _EmptyProperties()


#
#  Default and standard properties
#
# A standard property is a property that all objects that have
# properties have. A default property is the default value of a standard
# property.
#
# There are four sets of default properties, two each for text and other
# graphics objects. Factory defaults are the default values used in the
# constructors if no other properties were given. Default values are the
# ones tht should be used in the editor to pass to the constructors.
#
# FIXME: The default property stuff (but not the factory defaults)
# should be moved to Sketch.Editor and it should actually be used.

# The default base style of property stacks
factory_defaults = Style()

# default base style for non-text objects with properties
default_graphics_style = None # set below

# default base style for text
default_text_style = None # set below

# The blend functions to use for the standard properties. Each item is a
# tuple with the name of the property and a function object that takes
# two values and two weights and returns the interpolated value.
blend_functions = []

# List with the names of the standard properties. These names are usable
# as python identifiers.
property_names = []

# The titles of the standard properties. These are the names to be used
# in dialogs. Each item in the list is a tuple with a short title and a
# long title
property_titles = {}

# Map property names to property types.
property_types = {}

# List with the names of the properies that may have to be transformed.
transform_properties = []

def blend_number(n1, n2, frac1, frac2):
    """Return the linear interpolation of the numbers n1 and n2."""
    return n1 * frac1 + n2 * frac2

# The standard property types.
LineProperty = 1
FillProperty = 2
FontProperty = 3
OtherProperty = -1

def _set_defaults(prop, title, short_title, type, value,
                  blend = None, transform = 0):
    """Helper function to define a standard property with a default value.

    Take the arguments and fill the various lists and dicts.
    """
    factory_defaults.SetProperty(prop, value)
    property_names.append(prop)
    property_titles[prop] = (title, short_title)
    property_types[prop] = type
    blend_functions.append((prop, blend))
    if transform:
        transform_properties.append(prop)

black = StandardColors.black

# XXX the default properties should be defined by the user.
_set_defaults('fill_pattern', _("Fill Pattern"), _("Pattern"), FillProperty,
              EmptyPattern, blend = Blend, transform = 1)
_set_defaults('fill_transform', _("Fill Transform Pattern"),
              _("Transform pattern"), FillProperty, 1)
_set_defaults('fill_opacity', _("Fill Opacity"), _("Fill Opacity"),
              FillProperty, 1.0, blend = blend_number)
_set_defaults('line_pattern', _("Line Pattern"), _("Pattern"), LineProperty,
              SolidPattern(black), blend = Blend, transform = 1)
_set_defaults('line_width', _("Line Width"), _("Width"), LineProperty, 1 ,
              blend = blend_number)
_set_defaults('line_cap', _("Line Cap"), _("Cap"), LineProperty,
              const.CapButt)
_set_defaults('line_join', _("Line Join"), _("Join"), LineProperty,
              const.JoinMiter)
_set_defaults('line_dashes', _("Line Dashes"), _("Dashes"), LineProperty, ())
_set_defaults('line_arrow1', _("Line Arrow 1"), _("Arrow 1"), LineProperty,
              None)
_set_defaults('line_arrow2', _("Line Arrow 2"), _("Arrow 2"), LineProperty,
              None)
_set_defaults('font', _("Font"), _("Font"), FontProperty, None)
_set_defaults('font_size', _("Font Size"), _("Size"), FontProperty, 12,
              blend = blend_number)


default_graphics_style = factory_defaults.Copy()
default_text_style = factory_defaults.Copy()
default_text_style.fill_pattern = SolidPattern(black)
default_text_style.line_pattern = EmptyPattern
default_text_style.font = None


def FactoryTextStyle():
    """Return the factory text style."""
    import Sketch
    style = factory_defaults.Copy()
    if style.font is None:
        fontname = Sketch.Graphics.preferences.default_font
        style.font = Sketch.GetFont(fontname)
    return style

def DefaultTextProperties():
    """Return a new property stack with the default text properties.

    This property stack should be used in the editor when instantiating
    a text object.
    """
    import Sketch
    if default_text_style.font is None:
        fontname = Sketch.Graphics.preferences.default_font
        default_text_style.font = Sketch.Graphics.GetFont(fontname)
    return PropertyStack(base = default_text_style.Copy())

def DefaultGraphicsProperties():
    """Return a new property stack with the default graphics properties.

    This property stack should be used in the editor when instantiating
    a text object.
    """
    return PropertyStack(base = default_graphics_style.Copy())

def set_graphics_defaults(kw):
    """Set default properties for graphics objects.

    The argument should be a dictionary mapping property names to their
    new values.
    """
    for key, value in kw.items():
        if not property_types[key] == FontProperty:
            if key in ('fill_pattern', 'line_pattern'):
                value = value.Copy()
            default_graphics_style.SetProperty(key, value)

def set_text_defaults(kw):
    """Set default properties for text objects.

    The argument should be a dictionary mapping property names to their
    new values. Since these are the default values for text objects line
    properties are ignored as text objects currently have no line
    properties.
    """
    for key, value in kw.items():
        if not property_types[key] == LineProperty:
            if key == 'fill_pattern':
                value = value.Copy()
            default_text_style.SetProperty(key, value)
    
