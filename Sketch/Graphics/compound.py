# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#
# Class Compound
#
# a baseclass for objects that contain other graphics objects
# (primitives or other compounds)
#

from types import TupleType, IntType, InstanceType

from Sketch import _, UnionRects, EmptyRect
from Sketch.Base import SketchError, NullUndo, CreateListUndo, Undo
from Sketch.Base.const import CHILDREN, INSERTED, REMOVED, REARRANGED

from base import GraphicsObject, Bounded
from blend import Blend, MismatchError
from properties import EmptyProperties

from selinfo import prepend_idx



class Compound(GraphicsObject):

    has_edit_mode = 0
    is_Group      = 0
    is_Compound   = 1
    can_be_empty  = 0

    allow_traversal = 0

    def __init__(self, objects = None, duplicate = None):
        GraphicsObject.__init__(self, duplicate = duplicate)
        if duplicate is not None:
            objects = []
            for obj in duplicate.objects:
                objects.append(obj.Duplicate())
            self.objects = objects
        elif objects:
            self.objects = objects
        else:
            self.objects = []
        self.changing_children = 0
        self.set_parent()

    def Destroy(self):
        self.destroy_objects()
        GraphicsObject.Destroy(self)

    def destroy_objects(self):
        for obj in self.objects:
            obj.Destroy()
        self.objects = []

    def SetParent(self, parent):
        if parent is self.parent:
            return
        GraphicsObject.SetParent(self, parent)
        if parent is not None:
            self.set_parent()
        else:
            self.unset_parent()

    def set_parent(self):
        for child in self.objects:
            child.SetParent(self)

    def unset_parent(self):
        for child in self.objects:
            child.SetParent(None)

    def SelectionInfo(self, child = None):
        info = GraphicsObject.SelectionInfo(self)
        if info and child is not None:
            path = info[0]
            return (path + (self.objects.index(child),), child)
        return info

    def ChildChanged(self, child):
        self.del_lazy_attrs()
        if self.changing_children:
            return
        self.issue_changed()

    def SetDocument(self, doc):
        for obj in self.objects:
            obj.SetDocument(doc)
        GraphicsObject.SetDocument(self, doc)
        self.set_parent()

    def set_objects(self, new_objs):
        for obj in self.objects:
            obj.SetParent(None)
            obj.SetDocument(None)
        self.objects = new_objs
        for obj in self.objects:
            obj.SetParent(self)
            obj.SetDocument(self.document)
        self._changed(CHILDREN, (INSERTED, self.objects))

    def UntieFromDocument(self):
        for obj in self.objects:
            obj.UntieFromDocument()

    def TieToDocument(self):
        for obj in self.objects:
            obj.TieToDocument()

    def load_AppendObject(self, object):
        self.objects.append(object)

    def load_Done(self):
        pass

    def __getitem__(self, idx):
        if type(idx) == IntType:
            return self.objects[idx]
        elif type(idx) == TupleType:
            if len(idx) > 1:
                return self.objects[idx[0]][idx[1:]]
            elif len(idx) == 1:
                return self.objects[idx[0]]
        raise ValueError, 'invalid index %s' % `idx`

    def GetObjects(self):
        # XXX should this return a copy of self.objects?
        return self.objects

    def del_lazy_attrs(self):
        Bounded.del_lazy_attrs(self)
        return (self.del_lazy_attrs,)

    def update_rects(self):
        # XXX: should we raise an exception here if self.objects is empty?
        boxes = map(lambda o: o.coord_rect, self.objects)
        if boxes:
            self.coord_rect = reduce(UnionRects, boxes, boxes[0])
        else:
            self.coord_rect = EmptyRect

        boxes = map(lambda o: o.bounding_rect, self.objects)
        if boxes:
            self.bounding_rect = reduce(UnionRects, boxes, boxes[0])
        else:
            self.bounding_rect = EmptyRect

    def SelectSubobject(self, p, rect, device, path = None, *rest):
        return self

    def Insert(self, obj, at):
        raise SketchError('Cannot insert in compound')

    def Remove(self, *args, **kw):
        raise SketchError('Cannot remove from compound')

    RemoveSlice = Remove
    RemoveObjects = Remove

    ReplaceChild = Remove

    def MoveObjectsToTop(self, objects):
        raise SketchError('Cannot rearrange objects in compound')

    MoveObjectsToBottom = MoveObjectsToTop
    MoveObjectsDown = MoveObjectsToTop
    MoveObjectsUp = MoveObjectsToTop

    def DuplicateObjects(self, objects):
        raise SketchError('Cannot duplicate objects in compound')

    def WalkHierarchy(self, func):
        for obj in self.objects:
            if obj.is_Compound:
                obj.WalkHierarchy(func)
            else:
                func(obj)

    def begin_change_children(self):
        self.changing_children = self.changing_children + 1
        return (self.end_change_children,)

    def end_change_children(self):
        self.changing_children = self.changing_children - 1
        if not self.changing_children:
            self._changed()
        return (self.begin_change_children,)

    def ForAllUndo(self, func):
        if self.objects:
            undo = [self.begin_change_children()]
            undo = undo + map(func, self.objects)
            undo.append(self.end_change_children())
            return CreateListUndo(undo)
        else:
            return NullUndo

    def NumObjects(self):
        return len(self.objects)

    def Info(self):
        return _("Compound with %d objects") % len(self.objects)

    def AddStyle(self, style):
        undo = self.ForAllUndo(lambda o, style = style: o.AddStyle(style))
        return undo

    def Properties(self):
        return EmptyProperties

    def SetProperties(self, **kw):
        self.del_lazy_attrs()
        func = lambda o, kw = kw: apply(o.SetProperties, (), kw)
        undo = self.ForAllUndo(func)
        return undo

    def Hit(self, p, rect, device):
        test = rect.overlaps
        objects = self.objects
        for obj_idx in range(len(objects) - 1, -1, -1):
            obj = objects[obj_idx]
            if test(obj.bounding_rect):
                if obj.Hit(p, rect, device):
                    return obj_idx + 1
        return 0

    def Transform(self, trafo):
        self.del_lazy_attrs()
        undo = self.ForAllUndo(lambda o, t = trafo: o.Transform(t))
        return undo

    def Translate(self, offset):
        undo = self.ForAllUndo(lambda o, p = offset: o.Translate(p))
        return undo

    def DrawShape(self, device, rect = None):
        if rect:
            test = rect.overlaps
            for o in self.objects:
                if test(o.bounding_rect):
                    o.DrawShape(device, rect)
        else:
            for obj in self.objects:
                obj.DrawShape(device)

    def PickObject(self, point, rect, device):
        objects = self.objects[:]
        objects.reverse()
        test = rect.overlaps
        for obj in objects:
            if test(obj.bounding_rect):
                if obj.is_Compound:
                    result = obj.PickObject(point, rect, device)
                    if result:
                        break
                elif obj.Hit(point, rect, device):
                    result =  obj
                    break
        else:
            result = None

        return result

    def Blend(self, other, frac1, frac2):
        try:
            objs = self.objects
            oobjs = other.objects
            blended = []
            for i in range(min(len(objs), len(oobjs))):
                blended.append(Blend(objs[i], oobjs[i], frac1, frac2))
            return Compound(blended)
        except:
            raise MismatchError

    def GetObjectHandle(self, multiple):
        return [child.GetObjectHandle(True) for child in self.objects]

    def SelectFirstChild(self):
        if self.allow_traversal and self.objects:
            return self.objects[0]

    def SelectLastChild(self):
        if self.allow_traversal and self.objects:
            return self.objects[-1]

    def SelectNextChild(self, child, idx):
        if self.allow_traversal and len(self.objects) > idx + 1:
            return self.objects[idx + 1]

    def SelectPreviousChild(self, child, idx):
        if self.allow_traversal and len(self.objects) > idx - 1 and idx > 0:
            return self.objects[idx - 1]


class EditableCompound(Compound):

    allow_traversal = 1

    def SelectSubobject(self, p, rect, device, path = None, *rest):
        test = rect.overlaps
        if path is None:
            return self
        if path:
            path_idx = path[0]
            path = path[1:]
        else:
            path_idx = -1
            path = None
        objects = self.objects
        for obj_idx in range(len(objects) - 1, -1, -1):
            obj = objects[obj_idx]
            if test(obj.bounding_rect) and obj.Hit(p, rect, device):
                if obj_idx == path_idx:
                    result = obj.SelectSubobject(p, rect, device, path)
                else:
                    result = obj.SelectSubobject(p, rect, device)
                return prepend_idx(obj_idx, result)
        return None

    def Insert(self, object, index = None):
        """Insert object into self's children at position index.

        If object is a single graphics object insert it a index. If it's
        a list of graphics objects, insert them all at index with the
        equivalent of a slice assignment to [index:index]

        If index is None (default), insert object at the end, i.e. on
        top of the existing children.

        Return undo info.
        """
        if not isinstance(index, IntType) or index > len(self.objects):
            index = len(self.objects)
        if isinstance(object, InstanceType):
            # Assume it's a graphics object and insert it at index
            self.objects.insert(index, object)
            object.SetDocument(self.document)
            object.SetParent(self)
            undo_info = (self.Remove, object, index)
            self._changed(CHILDREN, (INSERTED, (object,)))
        else:
            # assume it's a list of graphics objects and insert them
            self.objects[index:index] = object
            for o in object:
                o.SetDocument(self.document)
                o.SetParent(self)
            undo_info = (self.RemoveSlice, index, index + len(object))
            self._changed(CHILDREN, (INSERTED, object))
        return undo_info

    def Remove(self, object, idx = None):
        """Remove object from self's children and return undo info.

        object must be a direct child of self.

        The optional parameter idx, if given must be the index of
        object. This parameter is only indended for internal use.
        """
        if idx is None:
            idx = self.objects.index(object)
        elif self.objects[idx] is not object:
            raise ValueError, 'Compound.Remove(): invalid index'
        obj = self.objects[idx]
        del self.objects[idx]
        obj.SetParent(None)
        obj.SetDocument(None)
        self._changed(CHILDREN, (REMOVED, (obj,)))
        return (self.Insert, obj, idx)

    def RemoveSlice(self, min, max):
        """
        Remove the objects given by the slice [min:max] and return undo info.
        """
        objs = self.objects[min:max]
        self.objects[min:max] = []
        for obj in objs:
            obj.SetParent(None)
            obj.SetDocument(None)
        self._changed(CHILDREN, (REMOVED, objs))
        return (self.Insert, objs, min)

    def _object_id_set(self, objects, id=id):
        """Return a dictionary with the ids of the objects in objects as keys

        In addition, this method checks whether all the objects are
        children of self.
        """
        # This is a method so that we can check whether the objects are
        # in fact children of self.  That slows things down a little,
        ids = {}
        for obj in objects:
            ids[id(obj)] = 1
            if not obj.parent is self:
                raise ValueError("Object %r is not a child of %r"
                                 % (obj, self))
        return ids

    def RemoveObjects(self, objects):
        """Remove the objects from the compound object

        All objects in the list objects must be children of self.  The
        return value is the undo information.
        """
        undo = []
        try:
            ids = self._object_id_set(objects)
            for idx in range(len(self.objects) - 1, -1, -1):
                obj = self.objects[idx]
                if id(obj) in ids:
                    undo.append(self.Remove(obj, idx))
        except:
            Undo(CreateListUndo(undo))
            raise
        return CreateListUndo(undo)

    def ReplaceChild(self, child, object):
        # replace self's child child with object. Return undo info
        idx = self.objects.index(child)
        self.objects[idx] = object
        child.SetParent(None)
        child.SetDocument(None)
        object.SetParent(self)
        object.SetDocument(self.document)
        self._changed(CHILDREN, (REMOVED, (child,)))
        self._changed(CHILDREN, (INSERTED, (object,)))
        return (self.ReplaceChild, object, child)

    def _rearrange_children(self, objects, affected_children):
        """Make objects the new list of children and return undo info

        The only purpose of this method is to be a helper method for the
        methods that change the stacking order of the children without
        adding or removing children (e.g. MoveObjectsToTop,
        MoveObjectsUp, etc).  Therefore, this method expects that the
        set of objects in objects is the same as in the list of children
        but the order may be different.

        In order to report the change to the parent this method expects
        a second list of children, affected_children, which contains the
        children that have been rearranged, e.g. the ones that were
        moved up.  In general this second parameter is the list of
        children that was passed to the the method making the
        rearrangement.
        """
        undo = (self._rearrange_children, self.objects[:],
                list(affected_children))
        self.objects[:] = objects
        self._changed(CHILDREN, (REARRANGED, list(affected_children)))
        return undo

    def MoveObjectsToTop(self, objects):
        """Move the objects to the top of the stacking order

        The parameter objects must be a list of objects all of which
        must be children of self.

        All objects in the list objects are moved to the top of the
        stacking order if possible while still staying a child of self
        and maintaining the same relative stacking order.

        For example, consider a group G with the four children in the
        order [A, B, C, D]:

           G.MoveObjectsToTop([B])    would lead to the order [A, C, D, B]
           G.MoveObjectsToTop([B, C]) would lead to the order [A, D, B, C]
           G.MoveObjectsToTop([C, D]) would lead to the order [A, B, C, D]

        The return value is the undo information.
        """
        ids = self._object_id_set(objects)
        children = self.objects
        bottom = []
        top = []
        for obj in children:
            if id(obj) in ids:
                top.append(obj)
            else:
                bottom.append(obj)
        return self._rearrange_children(bottom + top, objects)

    def MoveObjectsToBottom(self, objects):
        """Move the objects to the bottom of the stacking order

        The parameter objects must be a list of objects all of which
        must be children of self.

        All objects in the list objects are moved to the bottom of the
        stacking order if possible while still staying a child of self
        and maintaining the same relative stacking order.

        For example, consider a group G with the four children in the
        order [A, B, C, D]:

           G.MoveObjectsToBottom([C])    would lead to the order [C, A, B, D]
           G.MoveObjectsToBottom([B, C]) would lead to the order [B, C, A, D]
           G.MoveObjectsToBottom([A, B]) would lead to the order [A, B, C, D]

        The return value is the undo information.
        """
        ids = self._object_id_set(objects)
        children = self.objects
        bottom = []
        top = []
        for obj in children:
            if id(obj) in ids:
                bottom.append(obj)
            else:
                top.append(obj)
        return self._rearrange_children(bottom + top, objects)

    def MoveObjectsDown(self, objects):
        """Move the objects one step lower in the stacking order

        The parameter objects must be a list of objects all of which
        must be children of self.

        All objects in the list objects move one step further to the
        bottom of the stacking order if possible while still staying a
        child of self and maintaining the same relative stacking order.

        For example, consider a group G with the four children in the
        order [A, B, C, D]:

           G.MoveObjectsDown([C])       would lead to the order [A, C, B, D]
           G.MoveObjectsDown([A, C])    would lead to the order [A, C, B, D]
           G.MoveObjectsDown([A, B])    would lead to the order [A, B, C, D]
           G.MoveObjectsDown([B, C, D]) would lead to the order [B, C, D, A]

        The return value is the undo information.
        """
        ids = self._object_id_set(objects)
        children = self.objects
        new_order = []
        bottom = 0
        for obj in children:
            if id(obj) in ids:
                if len(new_order) > bottom:
                    new_order.insert(len(new_order) - 1, obj)
                else:
                    new_order.append(obj)
                    bottom += 1
            else:
                new_order.append(obj)
        return self._rearrange_children(new_order, objects)

    def MoveObjectsUp(self, objects):
        """Move the objects one step higher in the stacking order

        The parameter objects must be a list of objects all of which
        must be children of self.

        All objects in the list objects move one step further to the top
        of the stacking order if possible while still staying a child of
        self and maintaining the same relative stacking order.

        For example, consider a group G with the four children in the
        order [A, B, C, D]:

           G.MoveObjectsUp([C])       would lead to the order [A, B, D, C]
           G.MoveObjectsUp([B, D])    would lead to the order [A, C, B, D]
           G.MoveObjectsUp([C, D])    would lead to the order [A, B, C, D]
           G.MoveObjectsUp([A, B, C]) would lead to the order [D, A, B, C]

        The return value is the undo information.
        """
        ids = self._object_id_set(objects)
        children = self.objects[:]
        children.reverse()
        new_order = []
        bottom = 0
        for obj in children:
            if id(obj) in ids:
                if len(new_order) > bottom:
                    new_order.insert(len(new_order) - 1, obj)
                else:
                    new_order.append(obj)
                    bottom += 1
            else:
                new_order.append(obj)
        new_order.reverse()
        return self._rearrange_children(new_order, objects)

    def DuplicateObjects(self, objects):
        """Duplicate the objects

        The parameter objects is a list of objects all of which must be
        direct children of self.  Each object is duplicated and the
        duplicate is inserted into self immediately above the original
        object.

        The return value is a tuple (duplicates, undo_info) where
        duplicates is a list containing all the newly created duplicates
        and undo_info is the undo information for this operation.
        """
        undo = [self.begin_change_children()]
        duplicates = []
        ids = self._object_id_set(objects)
        try:
            for idx in xrange(len(self.objects) - 1, -1, -1):
                obj = self.objects[idx]
                if id(obj) in ids:
                    dup = obj.Duplicate()
                    undo.append(self.Insert(dup, idx + 1))
                    duplicates.append(dup)
            undo.append(self.end_change_children())
        except:
            # Something went wrong.  Undo all changes already done.
            # Note that because the undo info of begin_change_children()
            # is to call end_change_children() this will take care of
            # calling end_change_children().  The only situation that's
            # not covered is that the undo.append for the undo info
            # could fail in which case the undo info would get lost and
            # in the ase of the last append, end_change_children would
            # be called twice.  In that situation it's likely that we've
            # run out of memory so it's not very likely that the
            # emergency undo would succeed anyway.
            Undo(CreateListUndo(undo))
            raise

        return duplicates, CreateListUndo(undo)
