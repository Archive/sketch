# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#
#	Color Handling
#

from string import atoi

from Sketch._sketch import RGBColor

def CreateRGBColor(r, g, b):
    return RGBColor(round(r, 3), round(g, 3), round(b, 3))

def XRGBColor(s):
    # only understands the old x specification with two hex digits per
    # component. e.g. `#00FF00'
    if s[0] != '#':
	raise ValueError("Color %s dosn't start with a '#'" % s)
    r = atoi(s[1:3], 16) / 255.0
    g = atoi(s[3:5], 16) / 255.0
    b = atoi(s[5:7], 16) / 255.0
    return CreateRGBColor(r, g, b)

def CreateCMYKColor(c, m, y, k):
    r = 1.0 - min(1.0, c + k)
    g = 1.0 - min(1.0, m + k)
    b = 1.0 - min(1.0, y + k)
    return CreateRGBColor(r, g, b)


#
#	some standard colors.
#

class StandardColors:
    black	= CreateRGBColor(0.0, 0.0, 0.0)
    darkgray	= CreateRGBColor(0.25, 0.25, 0.25)
    gray	= CreateRGBColor(0.5, 0.5, 0.5)
    lightgray	= CreateRGBColor(0.75, 0.75, 0.75)
    white	= CreateRGBColor(1.0, 1.0, 1.0)
    red		= CreateRGBColor(1.0, 0.0, 0.0)
    green	= CreateRGBColor(0.0, 1.0, 0.0)
    blue	= CreateRGBColor(0.0, 0.0, 1.0)
    cyan	= CreateRGBColor(0.0, 1.0, 1.0)
    magenta	= CreateRGBColor(1.0, 0.0, 1.0)
    yellow	= CreateRGBColor(1.0, 1.0, 0.0)

