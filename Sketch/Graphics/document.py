# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2001, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#
# Classes:
#
# SketchDocument
# EditDocument(SketchDocument)
#
# The document class represents a complete Sketch drawing. Each drawing
# consists of one or more Layers, which in turn consist of zero of more
# graphics objects. Graphics objects can be primitives like rectangles
# or curves or composite objects like groups which consist of graphics
# objects themselves. Objects may be arbitrarily nested.
#
# The distinction between SketchDocument and EditDocument has only
# historical reasons...
#

from types import ListType, IntType, StringType, TupleType
from string import join

from Sketch import _, Rect, UnionRects, InfinityRect, EmptyRect
from Sketch.Base import QueueingPublisher, SketchInternalError, \
     UndoRedo, Undo, CreateListUndo, NullUndo
from Sketch.Base.warn import pdebug, warn_tb, INTERNAL
from Sketch.Base.undodict import UndoDict

import color, selinfo, pagelayout

from base import Protocols
from layer import Layer, GuideLayer, GridLayer
from group import Group
import guide

from Sketch.Base.const import STYLE, UNDO, REDRAW, LAYOUT,\
     LAYER, LAYER_ORDER, LAYER_ACTIVE, GUIDE_LINES, GRID, OBJECT_CHANGED, \
     SelectSet, SCRIPT_OBJECT, SCRIPT_GET, TRANSACTION_START, \
     TRANSACTION_END, CHILDREN, INSERTED, REMOVED, REARRANGED

#

class Insertions:

    def __init__(self):
        self.count = {}
        self.ids = {}
        self.inserted_objects = None
        self.removed_objects = None

    def Inserted(self, objects):
        count = self.count
        ids = self.ids
        for obj in objects:
            i = id(obj)
            ids[i] = obj
            count[i] = count.get(i, 0) + 1

    def Removed(self, objects):
        count = self.count
        ids = self.ids
        for obj in objects:
            i = id(obj)
            ids[i] = obj
            count[i] = count.get(i, 0) - 1

    def InsertedObjects(self):
        if self.inserted_objects is None:
            inserted = []
            append = inserted.append
            ids = self.ids
            for key, value in self.count.items():
                if value > 0:
                    append(ids[key])
            self.inserted_objects = inserted
        return self.inserted_objects

    def RemovedObjects(self):
        if self.removed_objects is None:
            removed = []
            append = removed.append
            ids = self.ids
            for key, value in self.count.items():
                if value < 0:
                    append(ids[key])
            self.removed_objects = removed
        return self.removed_objects

# SketchDocument is derived from Protocols for the benefit of the loader
# classes

class SketchDocument(Protocols):

    can_be_empty = 1

    script_access = {}

    def __init__(self, create_layer = 0):
        self.snap_grid = GridLayer()
        self.snap_grid.SetDocument(self)
        self.guide_layer = GuideLayer(_("Guide Lines"))
        self.guide_layer.SetDocument(self)
        if create_layer:
            # a new empty document
            self.active_layer = Layer(_("Layer 1"))
            self.active_layer.SetDocument(self)
            self.layers = [self.active_layer, self.guide_layer,
                           self.snap_grid]
        else:
            # we're being created by the load module
            self.active_layer = None
            self.layers = []

    def __del__(self):
        if __debug__:
            pdebug('__del__', 'SketchDocument.__del__',
                   getattr(self.meta, "filename", ""))

    def __getitem__(self, idx):
        if type(idx) == IntType:
            return self.layers[idx]
        elif type(idx) == TupleType:
            if len(idx) > 1:
                return self.layers[idx[0]][idx[1:]]
            elif len(idx) == 1:
                return self.layers[idx[0]]
        raise ValueError, 'invalid index %s' % `idx`

    def AppendLayer(self, layer_name = None, *args, **kw_args):
        try:
            old_layers = self.layers[:]
            if layer_name is None:
                layer_name = _("Layer %d") % (len(self.layers) + 1)
            else:
                layer_name = str(layer_name)
            layer = apply(Layer, (layer_name,) + args, kw_args)
            layer.SetDocument(self)
            self.layers.append(layer)
            if not self.active_layer:
                self.active_layer = layer
            return layer
        except:
            self.layers[:] = old_layers
            raise
    script_access['AppendLayer'] = SCRIPT_OBJECT

    def BoundingRect(self, visible = 1, printable = 0):
        rects = []
        for layer in self.layers:
            if ((visible and layer.Visible())
                or (printable and layer.Printable())):
                rect = layer.bounding_rect
                if rect and rect != InfinityRect:
                    rects.append(rect)
        if rects:
            return reduce(UnionRects, rects)
        return None
    script_access['BoundingRect'] = SCRIPT_GET

    def augment_sel_info(self, info, layeridx):
        if type(layeridx) != IntType:
            layeridx = self.layers.index(layeridx)
        return selinfo.prepend_idx(layeridx, info)

    def Insert(self, object, parent, index = None,
               undo_text = _("Create Object")):
        """Insert object into the parent at position index.

        parent must be a compound object and an object of self. The
        optional index can be None (default) in which case the object is
        inserted at the end or an int giving the position at which to
        insert it into parent.

        The optional undo_text is a text describing the action which is
        used to describe this transaction and may be displayed in the
        undo menu item. It defaults to 'Create Object'.

        The function is executed as a transaction and therefore takes
        care of undo handling.

        This method is mainly provided as a convenience method. The same
        affect (without the automatic undo handling) can be achieved by
        directly calling parent.Insert(object, index).
        """
        self.begin_transaction(undo_text)
        try:
            try:
                undo_info = parent.Insert(object, index)
                self.add_undo(undo_info)
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def selection_from_point(self, p, hitrect, device, path = None):
        # iterate top down (i.e. backwards) through the list of layers
        if path:
            path_layer = path[0]
            path = path[1:]
        else:
            path_layer = -1
        for idx in range(len(self.layers) - 1, -1, -1):
            if idx == path_layer:
                info = self.layers[idx].SelectSubobject(p, hitrect, device,
                                                        path)
            else:
                info = self.layers[idx].SelectSubobject(p, hitrect, device)
            if info:
                return self.augment_sel_info(info, idx)
        else:
            return None

    def selection_from_rect(self, rect):
        info = []
        for layer in self.layers:
            info = info + self.augment_sel_info(layer.SelectRect(rect), layer)
        return info

    def Draw(self, device, rect = None):
        for layer in self.layers:
            layer.Draw(device, rect)

    def Grid(self):
        return self.snap_grid

    def SnapToGrid(self, p):
        return self.snap_grid.Snap(p)

    def SnapToGuide(self, p, maxdist):
        return self.guide_layer.Snap(p) #, maxdist)

    def DocumentInfo(self):
        info = []
        info.append('%d layers' % len(self.layers))
        for idx in range(len(self.layers)):
            layer = self.layers[idx]
            info.append('%d: %s,\t%d objects' % (idx + 1, layer.name,
                                                 len(layer.objects)))
        return join(info, '\n')

    def SaveToFile(self, file):
        file.BeginDocument()
        self.page_layout.SaveToFile(file)
        self.write_styles(file)
        for layer in self.layers:
            layer.SaveToFile(file)
        file.EndDocument()

    def load_AppendObject(self, layer):
        self.layers.append(layer)

    def load_Done(self):
        pass

    def load_Completed(self):
        if not self.layers:
            self.layers = [Layer(_("Layer 1"))]
        if self.active_layer is None:
            for layer in self.layers:
                if layer.CanSelect():
                    self.active_layer = layer
                    break
        add_guide_layer = add_grid_layer = 1
        for layer in self.layers:
            layer.SetDocument(self)
            if isinstance(layer, GuideLayer):
                self.guide_layer = layer
                add_guide_layer = 0
            if isinstance(layer, GridLayer):
                self.snap_grid = layer
                add_grid_layer = 0
        if add_guide_layer:
            self.layers.append(self.guide_layer)
        if add_grid_layer:
            self.layers.append(self.snap_grid)

    def object_inserted(self, obj):
        pass

    def object_removed(self, obj):
        pass




#
#       Class MetaInfo
#
#       Each document has an instance of this class as the variable
#       meta. The application object uses this variable to store various
#       data about the document, such as the name of the file it was
#       read from, the file type, etc. See skapp.py
#
class MetaInfo:
    pass

class AbortTransactionError(SketchInternalError):
    pass


class EditDocument(SketchDocument, QueueingPublisher):

    script_access = SketchDocument.script_access.copy()

    def __init__(self, create_layer = 0):
        self.transaction_insertions = None
        SketchDocument.__init__(self, create_layer)
        QueueingPublisher.__init__(self)
        self._init_undo()
        self.was_dragged = 0
        self.meta = MetaInfo()
        self.hit_cache = None
        self.init_transaction()
        self.init_clear()
        self.init_styles()
        self.init_after_handler()
        self.init_layout()
        self.refcount = 0

    def Destroy(self):
        self.undo = None
        self.destroy_styles()
        QueueingPublisher.Destroy(self)
        for layer in self.layers:
            layer.Destroy()
        self.layers = []
        self.active_layer = None
        self.guide_layer = None
        self.snap_grid = None
        self.transaction_undo = []

    def IncRef(self):
        self.refcount = self.refcount + 1

    def DecRef(self):
        self.refcount = self.refcount - 1
        if self.refcount == 0:
            self.Destroy()

    def queue_layer(self, *args):
        if self.transaction:
            apply(self.queue_message, (LAYER,) + args)
            return (self.queue_layer, args)
        else:
            apply(self.issue, (LAYER,) + args)

    def init_after_handler(self):
        self.after_handlers = []

    def AddAfterHandler(self, handler, args = (), depth = 0):
        handler = (depth, handler, args)
        try:
            self.after_handlers.remove(handler)
        except ValueError:
            pass
        self.after_handlers.append(handler)

    def call_after_handlers(self):
        if not self.after_handlers:
            return 0

        while self.after_handlers:
            handlers = self.after_handlers

            handlers.sort()
            handlers.reverse()
            depth = handlers[0][0]

            count = 0
            for d, handler, args in handlers:
                if d == depth:
                    count = count + 1
                else:
                    break
            self.after_handlers = handlers[count:]
            handlers = handlers[:count]

            for d, handler, args in handlers:
                try:
                    apply(handler, args)
                except:
                    warn_tb(INTERNAL, "In after handler `%s'%s", handler, args)

        return 1

    def init_clear(self):
        self.clear_rects = []
        self.clear_all = 0

    reset_clear = init_clear

    def add_clear_rect(self, rect):
        self.clear_rects.append(rect)
        return (self.add_clear_rect, rect)

    def view_redraw_all(self):
        self.clear_all = 1
        return (self.view_redraw_all,)

    def issue_redraw(self):
        try:
            if self.clear_all:
                self.issue(REDRAW, 1)
            else:
                self.issue(REDRAW, 0, self.clear_rects)
        finally:
            self.clear_rects = []
            self.clear_all = 0

    def object_changed(self, object, what = '', detail = ()):
        changed_objects = self.transaction_changed_objects
        if what == CHILDREN and detail:
            change, children = detail
            if change in (REMOVED, INSERTED, REARRANGED):
                for child in children:
                    # use id of rect because object_changed might be
                    # called several times for the same object with
                    # REMOVED or INSERTED details and all of these rects
                    # have to be redrawn
                    rect = child.bounding_rect
                    changed_objects[id(rect)] = (None, rect)
        elif not changed_objects.has_key(id(object)):
            changed_objects[id(object)] = (object, object.bounding_rect)
        msgid = id(object), what
        if not self.transaction_change_messages.has_key(msgid):
            self.transaction_change_messages[msgid] \
                           = (OBJECT_CHANGED, object, what, detail)

    def object_inserted(self, obj):
        if self.transaction_insertions is not None:
            self.transaction_insertions.Inserted((obj,))

    def object_removed(self, obj):
        if self.transaction_insertions is not None:
            self.transaction_insertions.Removed((obj,))

    def init_transaction(self):
        self.reset_transaction()

    def reset_transaction(self):
        self.transaction = 0
        self.transaction_name = ''
        self.transaction_undo = []
        self.transaction_aborted = 0
        self.transaction_cleanup = []
        self.transaction_changed_objects = {}
        self.transaction_change_messages = {}
        self.transaction_insertions = None

    def cleanup_transaction(self):
        for handler, args in self.transaction_cleanup:
            try:
                apply(handler, args)
            except:
                warn_tb(INTERNAL, "in cleanup handler %s%s", handler, `args`)
        self.transaction_cleanup = []

    def add_cleanup_handler(self, handler, *args):
        handler = (handler, args)
        try:
            self.transaction_cleanup.remove(handler)
        except ValueError:
            pass
        self.transaction_cleanup.append(handler)

    def begin_transaction(self, name = ''):
        if self.transaction_aborted:
            raise AbortTransactionError
        if self.transaction == 0:
            self.transaction_name = name
            self.transaction_undo = []
            self.transaction_insertions = Insertions()
            self.issue(TRANSACTION_START)
        elif not self.transaction_name:
            self.transaction_name = name
        self.transaction = self.transaction + 1

    def end_transaction(self, issue = ()):
        self.transaction = self.transaction - 1
        if self.transaction_aborted:
            # end an aborted transaction
            if self.transaction == 0:
                # undo the changes already done...
                undo = self.transaction_undo
                undo.reverse()
                map(Undo, undo)
                self.cleanup_transaction()
                self.reset_transaction()
                self.reset_clear()
                self.issue(TRANSACTION_END, "aborted")
        else:
            # a normal transaction
            if type(issue) == StringType:
                self.queue_message(issue)
            else:
                for channel in issue:
                    self.queue_message(channel)
            if self.transaction == 0:
                # the outermost end_transaction
                # increase transaction flag temporarily because some
                # after handlers might call public methods that are
                # themselves transactions...
                self.transaction = 1
                self.call_after_handlers()
                self.transaction = 0
                for obj, rect in self.transaction_changed_objects.values():
                    self.add_clear_rect(rect)
                    if obj is not None:
                        self.add_clear_rect(obj.bounding_rect)
                undo = CreateListUndo(self.transaction_undo)
                if undo is not NullUndo:
                    undo = [undo]
                    undo = CreateListUndo(undo)
                    self._real_add_undo(self.transaction_name, undo)
                    undo_type = "added"
                else:
                    undo_type = None
                self.flush_message_queue()
                for msg in self.transaction_change_messages.values():
                    apply(self.issue, msg)
                self.issue_redraw()
                #print 'removed:'
                #print self.transaction_insertions.RemovedObjects()
                #print 'inserted:'
                #print self.transaction_insertions.InsertedObjects()
                self.issue(TRANSACTION_END, "normal",
                           not not self.transaction_changed_objects,
                           undo_type, self.transaction_insertions)
                self.cleanup_transaction()
                self.reset_transaction()
                self.reset_clear()
            elif self.transaction < 0:
                raise SketchInternalError('transaction < 0')

    def abort_transaction(self):
        self.transaction_aborted = 1
        warn_tb(INTERNAL, "in transaction `%s'" % self.transaction_name)
        raise AbortTransactionError

    # public versions of the transaction methods
    BeginTransaction = begin_transaction
    AbortTransaction = abort_transaction

    EndTransaction = end_transaction

    def PickObject(self, device, point, selectable = 0):
        # Return the object that is hit by a click at POINT. The object
        # is not selected and should not be modified by the caller.
        #
        # If selectable is false, this function descends into compound
        # objects that are normally selected as a whole when one of
        # their children is hit. If selectable is true, the search is
        # done as for a normal selection.
        #
        # This method is intended to be used to
        # let the user click on the drawing and extract properties from
        # the indicated object. The fill and line dialogs use this
        # indirectly (through the canvas object's PickObject) for their
        # 'Update From...' button.
        #
        # XXX should this be implemented by calling WalkHierarchy
        # instead of requiring a special PickObject method in each
        # compound? Unlike the normal hit-test, this method is not that
        # time critical and WalkHierarchy is sufficiently fast for most
        # purposes (see extract_snap_points in the canvas).
        # WalkHierarchy would have to be able to traverse the hierarchy
        # top down and not just bottom up.
        object = None
        rect = device.HitRectAroundPoint(point)
        if not selectable:
            layers = self.layers[:]
            layers.reverse()
            for layer in layers:
                object = layer.PickObject(point, rect, device)
                if object is not None:
                    break
        else:
            selected = self.selection_from_point(point, rect, device)
            if selected:
                object = selected[-1]
        return object

    #
    #
    #

    def WalkHierarchy(self, func, printable = 1, visible = 1, all = 0):
        # XXX make the selection of layers more versatile
        for layer in self.layers:
            if (all
                or printable and layer.Printable()
                or visible and layer.Visible()):
                layer.WalkHierarchy(func)

    #
    #   The undo mechanism
    #

    def _init_undo(self):
        self.undo = UndoRedo()

    def CanUndo(self):
        return self.undo.CanUndo()
    script_access['CanUndo'] = SCRIPT_GET

    def CanRedo(self):
        return self.undo.CanRedo()
    script_access['CanRedo'] = SCRIPT_GET

    def Undo(self):
        if self.undo.CanUndo():
            self.begin_transaction()
            try:
                try:
                    self.undo.Undo()
                except:
                    self.abort_transaction()
            finally:
                self.end_transaction(issue = UNDO)
    script_access['Undo'] = SCRIPT_GET

    def add_undo(self, *infos):
        # Add undoinfo for the current transaction. should not be called
        # when not in a transaction.
        if infos:
            if type(infos[0]) == StringType:
                if not self.transaction_name:
                    self.transaction_name = infos[0]
                infos = infos[1:]
                if not infos:
                    return
            for info in infos:
                if type(info) == ListType:
                    info = CreateListUndo(info)
                else:
                    if type(info[0]) == StringType:
                        if __debug__:
                            pdebug(None, 'add_undo: info contains text')
                        info = info[1:]
                self.transaction_undo.append(info)

    # public version of add_undo. to be called between calls to
    # BeginTransaction and EndTransaction/AbortTransaction
    AddUndo = add_undo

    def _real_add_undo(self, text, undo):
        if undo is not NullUndo:
            self.undo.AddUndo((text,) + undo)
            self.queue_message(UNDO)

    def Redo(self):
        if self.undo.CanRedo():
            self.begin_transaction()
            try:
                try:
                    self.undo.Redo()
                except:
                    self.abort_transaction()
            finally:
                self.end_transaction(issue = UNDO)
    script_access['Redo'] = SCRIPT_GET

    def ResetUndo(self):
        self.begin_transaction()
        try:
            try:
                self.undo.Reset()
            except:
                self.abort_transaction()
        finally:
            self.end_transaction(issue = UNDO)
    script_access['ResetUndo'] = SCRIPT_GET

    def UndoMenuText(self):
        return self.undo.UndoText()
    script_access['UndoMenuText'] = SCRIPT_GET

    def RedoMenuText(self):
        return self.undo.RedoText()
    script_access['RedoMenuText'] = SCRIPT_GET

    def SetUndoLimit(self, limit):
        self.begin_transaction()
        try:
            try:
                self.undo.SetUndoLimit(limit)
            except:
                self.abort_transaction()
        finally:
            self.end_transaction(issue = UNDO)
    script_access['SetUndoLimit'] = SCRIPT_GET

    def WasEdited(self):
        # return true if document has changed since last save
        return self.undo.WasEdited()
    script_access['WasEdited'] = SCRIPT_GET

    def MarkAsSaved(self):
        self.undo.MarkAsSaved()
        self.issue(UNDO)

    def StateNumber(self):
        return self.undo.StateNumber()


    #
    #
    #

    def Layers(self):
        return self.layers[:]

    def NumLayers(self):
        return len(self.layers)

    def ActiveLayer(self):
        return self.active_layer

    def ActiveLayerIdx(self):
        if self.active_layer is None:
            return None
        return self.layers.index(self.active_layer)

    def SetActiveLayer(self, idx):
        if type(idx) == IntType:
            layer = self.layers[idx]
        else:
            layer = idx
        if not layer.Locked():
            self.active_layer = layer
        self.queue_layer(LAYER_ACTIVE)

    def LayerIndex(self, layer):
        return self.layers.index(layer)

    def update_active_layer(self):
        if self.active_layer is not None and self.active_layer.CanSelect():
            return
        self.find_active_layer()

    def find_active_layer(self, idx = None):
        if idx is not None:
            layer = self.layers[idx]
            if layer.CanSelect():
                self.SetActiveLayer(idx)
                return
        for layer in self.layers:
            if layer.CanSelect():
                self.SetActiveLayer(layer)
                return
        self.active_layer = None
        self.queue_layer(LAYER_ACTIVE)


    def SetLayerState(self, layer_idx, visible, printable, locked, outlined):
        self.begin_transaction(_("Change Layer State"))
        try:
            try:
                layer = self.layers[layer_idx]
                self.add_undo(layer.SetState(visible, printable, locked,
                                             outlined))
                if not layer.CanSelect():
                    # XXX: this depends on whether we're drawing visible or
                    # printable layers
                    self.update_active_layer()
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def SetLayerColor(self, layer_idx, color):
        self.begin_transaction(_("Set Layer Outline Color"))
        try:
            try:
                layer = self.layers[layer_idx]
                self.add_undo(layer.SetOutlineColor(color))
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def SetLayerName(self, idx, name):
        self.begin_transaction(_("Rename Layer"))
        try:
            try:
                layer = self.layers[idx]
                self.add_undo(layer.SetName(name))
                self.add_undo(self.queue_layer())
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def AppendLayer(self, *args, **kw_args):
        self.begin_transaction(_("Append Layer"))
        try:
            try:
                layer = apply(SketchDocument.AppendLayer, (self,) + args,
                              kw_args)
                self.add_undo((self._remove_layer, len(self.layers) - 1))
                self.queue_layer(LAYER_ORDER, layer)
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()
        return layer

    def NewLayer(self):
        self.begin_transaction(_("New Layer"))
        try:
            try:
                self.AppendLayer()
                self.active_layer = self.layers[-1]
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def _move_layer_up(self, idx):
        # XXX: exception handling
        if idx < len(self.layers) - 1:
            # move the layer...
            layer = self.layers[idx]
            del self.layers[idx]
            self.layers.insert(idx + 1, layer)
            other = self.layers[idx]
            # ... and adjust the selection
            sel = self.selection.GetInfoTree()
            newsel = []
            for i, info in sel:
                if i == idx:
                    i = idx + 1
                elif i == idx + 1:
                    i = idx
                newsel.append((i, info))
            self._set_selection(selinfo.tree_to_list(newsel), SelectSet)
            self.queue_layer(LAYER_ORDER, layer, other)
            return (self._move_layer_down, idx + 1)
        return None

    def _move_layer_down(self, idx):
        # XXX: exception handling
        if idx > 0:
            # move the layer...
            layer = self.layers[idx]
            del self.layers[idx]
            self.layers.insert(idx - 1, layer)
            other = self.layers[idx]
            # ...and adjust the selection
            sel = self.selection.GetInfoTree()
            newsel = []
            for i, info in sel:
                if i == idx:
                    i = idx - 1
                elif i == idx - 1:
                    i = idx
                newsel.append((i, info))
            self._set_selection(selinfo.tree_to_list(newsel), SelectSet)
            self.queue_layer(LAYER_ORDER, layer, other)
            return (self._move_layer_up, idx - 1)
        return NullUndo

    def MoveLayerUp(self, idx):
        if idx < len(self.layers) - 1:
            self.begin_transaction(_("Move Layer Up"))
            try:
                try:
                    self.add_undo(self._move_layer_up(idx))
                except:
                    self.abort_transaction()
            finally:
                self.end_transaction()

    def MoveLayerDown(self, idx):
        if idx > 0:
            self.begin_transaction(_("Move Layer Down"))
            try:
                try:
                    self.add_undo(self._move_layer_down(idx))
                except:
                    self.abort_transaction()
            finally:
                self.end_transaction()

    def _remove_layer(self, idx):
        layer = self.layers[idx]
        del self.layers[idx]
        if layer is self.active_layer:
            if idx < len(self.layers):
                self.find_active_layer(idx)
            else:
                self.find_active_layer()
        sel = self.selection.GetInfoTree()
        newsel = []
        for i, info in sel:
            if i == idx:
                continue
            elif i > idx:
                i = i - 1
            newsel.append((i, info))
        self._set_selection(selinfo.tree_to_list(newsel), SelectSet)

        self.queue_layer(LAYER_ORDER, layer)
        return (self._insert_layer, idx, layer)

    def _insert_layer(self, idx, layer):
        self.layers.insert(idx, layer)
        layer.SetDocument(self)
        self.queue_layer(LAYER_ORDER, layer)
        return (self._remove_layer, idx)

    def CanDeleteLayer(self, idx):
        return (len(self.layers) > 3 and not self.layers[idx].is_SpecialLayer)

    def DeleteLayer(self, idx):
        if self.CanDeleteLayer(idx):
            self.begin_transaction(_("Delete Layer"))
            try:
                try:
                    self.add_undo(self._remove_layer(idx))
                except:
                    self.abort_transaction()
            finally:
                self.end_transaction()



    #
    #   Style management
    #

    def queue_style(self):
        self.queue_message(STYLE)
        return (self.queue_style,)

    def init_styles(self):
        self.styles = UndoDict()
        self.auto_assign_styles = 1
        self.asked_about = {}

    def destroy_styles(self):
        for style in self.styles.values():
            style.Destroy()
        self.styles = None

    def get_dynamic_style(self, name):
        return self.styles[name]

    def GetDynamicStyle(self, name):
        try:
            return self.styles[name]
        except KeyError:
            return None

    def Styles(self):
        names = self.styles.keys()
        names.sort()
        return map(self.styles.get, names)

    def write_styles(self, file):
        for style in self.styles.values():
            style.SaveToFile(file)

    def load_AddStyle(self, style):
        self.styles.SetItem(style.Name(), style)

    def add_dynamic_style(self, name, style):
        if style:
            style = style.AsDynamicStyle()
            self.add_undo(self.styles.SetItem(name, style))
            self.add_undo(self.queue_style())
            return style

    def update_style_dependencies(self, style):
        def update(obj, style = style):
            obj.ObjectChanged(style)
        self.WalkHierarchy(update)
        return (self.update_style_dependencies, style)

    def RemoveDynamicStyle(self, name):
        style = self.GetDynamicStyle(name)
        if not style:
            # style does not exist. XXX: raise an exception ?
            return
        self.begin_transaction(_("Remove Style %s") % name)
        try:
            try:
                def remove(obj, style = style, add_undo = self.add_undo):
                    add_undo(obj.ObjectRemoved(style))
                self.WalkHierarchy(remove)
                self.add_undo(self.styles.DelItem(name))
                self.add_undo(self.queue_style())
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def GetStyleNames(self):
        names = self.styles.keys()
        names.sort()
        return names

    #
    #   Layout
    #

    def queue_layout(self):
        self.queue_message(LAYOUT)
        return (self.queue_layout,)

    def init_layout(self):
        self.page_layout = pagelayout.PageLayout()

    def Layout(self):
        return self.page_layout

    def PageSize(self):
        return (self.page_layout.Width(), self.page_layout.Height())

    def PageRect(self):
        w, h = self.page_layout.Size()
        return Rect(0, 0, w, h)

    def load_SetLayout(self, layout):
        self.page_layout = layout

    def _set_page_layout(self, layout):
        undo = (self._set_page_layout, self.page_layout)
        self.page_layout = layout
        self.queue_layout()
        return undo

    def SetLayout(self, layout):
        self.begin_transaction()
        try:
            try:
                undo = self._set_page_layout(layout)
                self.add_undo(_("Change Page Layout"), undo)
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    #
    #   Grid Settings
    #

    def queue_grid(self):
        self.queue_message(GRID)
        return (self.queue_grid,)

    def SetGridGeometry(self, geometry):
        self.begin_transaction(_("Set Grid Geometry"))
        try:
            try:
                self.add_undo(self.snap_grid.SetGeometry(geometry))
                self.add_undo(self.queue_grid())
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def GridGeometry(self):
        return self.snap_grid.Geometry()

    def GridLayerChanged(self):
        return self.queue_grid()


    #
    #   Guide Lines
    #

    def add_guide_line(self, line):
        self.begin_transaction(_("Add Guide Line"))
        try:
            try:
                self.add_undo(self.guide_layer.Insert(line, 0))
                self.add_undo(self.add_clear_rect(line.get_clear_rect()))
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def AddGuideLine(self, point, horizontal):
        self.add_guide_line(guide.GuideLine(point, horizontal))

    def RemoveGuideLine(self, line):
        if not line.parent is self.guide_layer or not line.is_GuideLine:
            return
        self.begin_transaction(_("Delete Guide Line"))
        try:
            try:
                self.add_undo(line.parent.Remove(line))
                self.add_undo(self.add_clear_rect(line.get_clear_rect()))
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def MoveGuideLine(self, line, point):
        if not line.parent is self.guide_layer or not line.is_GuideLine:
            return
        self.begin_transaction(_("Move Guide Line"))
        try:
            try:
                self.add_undo(self.add_clear_rect(line.get_clear_rect()))
                self.add_undo(line.SetPoint(point))
                self.add_undo(self.add_clear_rect(line.get_clear_rect()))
                self.add_undo(self.GuideLayerChanged(line.parent))
            except:
                self.abort_transaction()
        finally:
            self.end_transaction()

    def GuideLayerChanged(self, layer):
        self.queue_message(GUIDE_LINES, layer)
        return (self.GuideLayerChanged, layer)

    def GuideLines(self):
        return self.guide_layer.GuideLines()


    #
    #
    def as_group(self):
        for name in self.GetStyleNames():
            self.RemoveDynamicStyle(name)
        layers = self.layers
        self.layers = []
        groups = []
        for layer in layers:
            if not layer.is_SpecialLayer:
                layer.UntieFromDocument()
                objects = layer.GetObjects()
                layer.objects = []
                if objects:
                    groups.append(Group(objects))
            else:
                layer.Destroy()
        if groups:
            return Group(groups)
        else:
            return None

