# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from math import pi

from Sketch import Polar, Bezier
from Sketch.Base import const

class CommonDevice:

    # some methods common to GraphicsDevice and PostScriptDevice

    def draw_arrow(self, arrow, width, pos, dir, rect = None):
	self.PushTrafo()
	self.Translate(pos.x, pos.y)
	self.Rotate(dir.polar()[1])
	if width >= 1.0:
	    self.Scale(width)
	self.SetLineSolid()
	arrow.Draw(self, rect)
	self.PopTrafo()

    def draw_arrows(self, paths, rect = None):
	if self.line:
	    arrow1 = self.properties.line_arrow1
	    arrow2 = self.properties.line_arrow2
	    if arrow1 or arrow2:
		width = self.properties.line_width
		for path in paths:
		    if not path.closed and path.len > 1:
			if arrow1:
			    type, controls, p3, cont = path.Segment(1)
			    p = path.Node(0)
			    if type == Bezier:
				p1, p2 = controls
				dir = p - p1
				if not abs(dir):
				    dir = p - p2
			    else:
				dir = p - p3
			    self.draw_arrow(arrow1, width, p, dir, rect)
			if arrow2:
			    type, controls, p, cont = path.Segment(-1)
			    p3 = path.Node(-2)
			    if type == Bezier:
				p1, p2 = controls
				dir = p - p2
				if not abs(dir):
				    dir = p - p1
			    else:
				dir = p - p3
			    self.draw_arrow(arrow2, width, p, dir, rect)

    def draw_ellipse_arrows(self, trafo, start_angle, end_angle, arc_type,
			    rect = None):
	if arc_type == const.ArcArc and self.line and start_angle != end_angle:
	    pi2 = pi / 2
	    width = self.properties.line_width
	    arrow1 = self.properties.line_arrow1
	    if arrow1 is not None:
		pos = trafo(Polar(1, start_angle))
		dir = trafo.DTransform(Polar(1, start_angle - pi2))
		self.draw_arrow(arrow1, width, pos, dir, rect)
	    arrow2 = self.properties.line_arrow2
	    if arrow2 is not None:
		pos = trafo(Polar(1, end_angle))
		dir = trafo.DTransform(Polar(1, end_angle + pi2))
		self.draw_arrow(arrow2, width, pos, dir, rect)

