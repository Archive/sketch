# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

# Rectangle:
#
# The Rectangle is actually better described as a Parallelogram. When
# created interactively, instances of this class are rectangles with
# edges parallel to the axes of the coordinate system. The user can then
# rotate and shear it interactively so that it may become a
# (nonrectangular) Parallelogram.
#

from math import hypot

import Sketch
from Sketch import _, SingularMatrix, PointsToRect, Trafo, Polar,\
     RoundedRectanglePath, RectanglePath
from Sketch.Base.const import SHAPE
from Sketch.Base.warn import warn, INTERNAL

from base import Primitive, RectangularPrimitive
from bezier import PolyBezier

# The corners of the unit rectangle
corners = [(0, 0), (1, 0), (1, 1), (0, 1)]


class Rectangle(RectangularPrimitive):

    is_Rectangle = 1
    is_curve = 1
    is_clip = 1
    has_edit_mode = 1

    _lazy_attrs = RectangularPrimitive._lazy_attrs.copy()
    _lazy_attrs['rect_path'] = 'update_path'

    def __init__(self, trafo = None, radius1 = 0, radius2 = 0,
                 properties = None, duplicate = None):
	RectangularPrimitive.__init__(self, trafo, properties = properties,
                                      duplicate = duplicate)
        if duplicate is not None:
            self.radius1 = duplicate.radius1
            self.radius2 = duplicate.radius2
        else:
            self.radius1 = radius1
            self.radius2 = radius2

    def Radii(self):
        return self.radius1, self.radius2

    def SetTrafoAndRadii(self, trafo, radius1, radius2):
        undo = self.SetTrafoAndRadii, self.trafo, self.radius1, self.radius2
        self.trafo = trafo
        if radius1 <= 0 or radius2 <= 0:
            self.radius1 = 0
            self.radius2 = 0
            if __debug__:
                if radius1 > 0 or radius2 > 0:
                    warn(INTERNAL,
                         'Rectangle radius corrected: r1 = %g, r2 = %g',
                         radius1, radius2)
        else:
            self.radius1 = radius1
            self.radius2 = radius2
        self._changed(SHAPE)
        return undo

    def DrawShape(self, device, rect = None, clip = 0):
	Primitive.DrawShape(self, device)
        if self.radius1 == self.radius2 == 0:
            device.Rectangle(self.trafo, clip)
        else:
            device.RoundedRectangle(self.trafo, self.radius1, self.radius2,
                                    clip)

    def update_path(self):
        if self.radius1 == self.radius2 == 0:
            self.rect_path = (RectanglePath(self.trafo),)
        else:
            self.rect_path = (RoundedRectanglePath(self.trafo, self.radius1,
                                                   self.radius2),)

    def Paths(self):
        return self.rect_path

    def AsBezier(self):
	return PolyBezier(paths = self.rect_path,
                          properties = self.properties.Duplicate())

    def Hit(self, p, rect, device, clip = 0):
        if self.radius1 == self.radius2 == 0:
            return device.ParallelogramHit(p, self.trafo, 1, 1,
                                           clip or self.Filled(),
                                           self.properties,
                                           ignore_outline_mode = clip)
        else:
            return device.MultiBezierHit(self.rect_path, p, self.properties,
                                         clip or self.Filled(),
                                         ignore_outline_mode = clip)
    def GetSnapPoints(self):
	return map(self.trafo, corners)
    
    def Snap(self, p):
	try:
	    x, y = self.trafo.inverse()(p)
            minx = self.radius1
            maxx = 1 - self.radius1
            miny = self.radius2
            maxy = 1 - self.radius2
	    if minx < x < maxx:
		if miny < y < maxy:
                    ratio = hypot(self.trafo.m11, self.trafo.m21) \
                            / hypot(self.trafo.m12, self.trafo.m22)
                    if x < 0.5:
                        dx = x
                    else:
                        dx = 1 - x
                    if y < 0.5:
                        dy = y
                    else:
                        dy = 1 - y
                    if dy / dx > ratio:
                        x = round(x)
                    else:
                        y = round(y)
                elif y > maxy:
                    y = 1
                else:
                    y = 0
	    elif miny < y < maxy:
                if x > maxx:
                    x = 1
                else:
                    x = 0
            elif minx > 0 and miny > 0:
                # the round corners
                if x < 0.5:
                    cx = minx
                else:
                    cx = maxx
                if y < 0.5:
                    cy = miny
                else:
                    cy = maxy
                trafo = Trafo(minx, 0, 0, miny, cx, cy)
                r, phi = trafo.inverse()(x, y).polar()
                x, y = trafo(Polar(1, phi))
            else:
                # normal corners
                x = round(min(max(x, 0), 1))
                y = round(min(max(y, 0), 1))

	    p2 = self.trafo(x, y)
	    return (abs(p - p2), p2)
	except SingularMatrix:
	    return (1e200, p)

    def update_rects(self):
	rect = PointsToRect(map(self.trafo, corners))
	self.coord_rect = rect
	if self.properties.HasLine():
	    self.bounding_rect = rect.grown(self.properties.GrowAmount())
	else:
	    self.bounding_rect = rect

    def Info(self):
	trafo = self.trafo
	w = hypot(trafo.m11, trafo.m21)
	h = hypot(trafo.m12, trafo.m22)
	return _("Rectangle %(size)[size]"), {'size': (w, h)}

    def SaveToFile(self, file):
	Primitive.SaveToFile(self, file)
        file.Rectangle(self.trafo, self.radius1, self.radius2)

    def Blend(self, other, p, q):
        result = RectangularPrimitive.Blend(self, other, p, q)
        result.radius1 = p * self.radius1 + q * other.radius1
        result.radius2 = p * self.radius2 + q * other.radius2
        return result
