# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch


class GraphicsPreferences(Sketch.Base.Preferences):

    _filename = 'graphics.prefs'

    #	Gridding
    #
    #	The initial grid geometry for a new document. It must be a tuple
    #	of the form (ORIG_X, ORIG_Y, WIDTH_X, WIDTH_Y). WIDTH_X and
    #	WIDTH_Y are the horizontal and the vertical distance between
    #	points of the grid, (ORIG_X, ORIG_X) is one point of the grid.
    #	These coordinates are given in Point
    grid_geometry = (0, 0, 20, 20)

    #	If the grid should be visible in a new document, set
    #	grid_visible to a true value
    grid_visible = 0

    #	The grid color of a new document as a tuple of RGB values in the
    #	range 0..1. E.g. (0, 0, 1) for blue
    grid_color = (0, 0, 1)

    #
    #	Guide Layer
    #
    #	The outline color of a new GuideLayer as a tuple of RGB values
    #	in the range 0..1.
    guide_color = (0, 0, 1)

    #
    #   Text
    #

    #   The name of the font used for new text-objects
    default_font = 'Times-Roman'

    #   If the metrics file for a font can't be found or if a requested
    #   font is not known at all, the (metrics of) fallback_font is used
    fallback_font = 'Times-Roman'

    #
    #   Page Layout
    #
    
    #	Default paper format for new documents and documents read from a
    #	files that don't specify a paper format. This should be one of
    #	the formats defined in papersize.py.
    default_paper_format = 'A4'

    #	Default page orientation. Portrait = 0, Landscape = 1. Other
    #	values are silenty ignored.
    default_page_orientation = 0

preferences = GraphicsPreferences()


_initialized = 0
def init(argv, load_user_preferences = 0):
    global _initialized
    if not _initialized:
        Sketch.Base.init(argv, load_user_preferences = load_user_preferences)
        preferences._load()
    _initialized = 1


from arrow import Arrow
from properties import Style, EmptyFillStyle, LineStyle, EmptyLineStyle, \
     PropertyStack, EmptyProperties

from base import Protocols, Bounded, ObjectWithMetaData
from blend import MismatchError, Blend, BlendTrafo
from blendgroup import BlendGroup, CreateBlendGroup, BlendInterpolation

from color import CreateRGBColor, XRGBColor, CreateCMYKColor, StandardColors
from compound import Compound, EditableCompound
from dashes import StandardDashes

from document import EditDocument
Document = EditDocument
del EditDocument

from font import GetFont
from gradient import MultiGradient, CreateSimpleGradient

from group import Group
from guide import GuideLine
from image import Image, load_image, ImageData
from layer import Layer, GuideLayer, GridLayer

from maskgroup import MaskGroup

from pattern import EmptyPattern, SolidPattern, HatchingPattern, \
     LinearGradient, RadialGradient, ConicalGradient, ImageTilePattern

from plugobj import PluginCompound, TrafoPlugin

from rectangle import Rectangle
from ellipse import Ellipse
from bezier import PolyBezier, CombineBeziers

from psdevice import PostScriptDevice


from text import SimpleText, PathText, InternalPathText
