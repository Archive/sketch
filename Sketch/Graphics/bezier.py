# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA


from traceback import print_stack

import Sketch
from Sketch import _, CreatePath, Point, Rect, UnionRects, Bezier
from Sketch.Base import CreateMultiUndo, NullUndo, Undo
from Sketch.Base.const import SCRIPT_OBJECTLIST, SHAPE
from Sketch.Base.warn import warn, INTERNAL

from base import Primitive
from blend import Blend, BlendPaths, MismatchError


#
# PolyBezier
#
# The most important primitive, since it can be used to draw lines,
# curves and arbitrary contours (to some degree; circles for instance
# can only be approximated, but that works quite well).
#

def undo_path(undo, path):
    if undo is not None:
	return apply(getattr(path, undo[0]), undo[1:])
    else:
	return undo

class PolyBezier(Primitive):

    is_Bezier	  = 1
    has_edit_mode = 1
    is_curve	  = 1
    is_clip	  = 1

    script_access = Primitive.script_access.copy()

    def __init__(self, paths = None, properties = None, duplicate = None):
	if duplicate is not None:
	    if paths is None:
		paths = []
		for path in duplicate.paths:
		    paths.append(path.Duplicate())
		self.paths = tuple(paths)
	    else:
		# This special case uses the properties kwarg now, I
		# hope.
		warn(INTERNAL, 'Bezier object created with paths and duplicte')
		print_stack()
		if type(paths) != type(()):
		    paths = (paths,)
		self.paths = paths
	elif paths is not None:
	    if type(paths) != type(()):
		paths = (paths,)
	    self.paths = paths
	else:
	    self.paths = (CreatePath(),)

	Primitive.__init__(self, properties = properties, duplicate=duplicate)

    def Hit(self, p, rect, device, clip = 0):
	for path in self.paths:
	    if path.hit_point(rect):
		return 1
	return device.MultiBezierHit(self.paths, p, self.properties,
				     clip or self.Filled(),
				     ignore_outline_mode = clip)

    def do_undo(self, undo_list):
	undo = map(undo_path, undo_list, self.paths)
	self._changed(SHAPE)
	return (self.do_undo, undo)

    def Transform(self, trafo):
	undo = []
	undostyle = NullUndo
	try:
	    rect = self.bounding_rect
	    for path in self.paths:
		undo.append(path.Transform(trafo))
	    self._changed(SHAPE)
	    undo = (self.do_undo, undo)
	    self.update_rects() # calling update_rects directly is a bit faster
	    undostyle = Primitive.Transform(self, trafo,
					    rects = (rect, self.bounding_rect))
	    return CreateMultiUndo(undostyle, undo)
	except:
	    Undo(undostyle)
	    if type(undo) != type(()):
		undo = (self.do_undo, undo)
	    Undo(undo)
	    raise

    def Translate(self, offset):
	for path in self.paths:
	    path.Translate(offset)
	self._changed(SHAPE)
	return self.Translate, -offset

    def DrawShape(self, device, rect = None, clip = 0):
	Primitive.DrawShape(self, device)
	device.MultiBezier(self.paths, rect, clip)

    def GetObjectHandle(self, multiple):
	if self.paths:
	    return self.paths[0].Node(0)
	return Point(0, 0)

    def GetSnapPoints(self):
	points = []
	for path in self.paths:
	    points = points + path.NodeList()
	return points

    def Snap(self, p):
	found = [(1e100, p)]
	for path in self.paths:
	    t = path.nearest_point(p.x, p.y)
	    if t is not None:
		#print p, t
		p2 = path.point_at(t)
		found.append((abs(p - p2), p2))
	return min(found)

    def update_rects(self):
	if not self.paths:
	    # this should never happen...
	    self.coord_rect = self.bounding_rect = Rect(0, 0, 0, 0)
	    return

	rect = self.paths[0].accurate_rect()
	for path in self.paths[1:]:
	    rect = UnionRects(rect, path.accurate_rect())
	self.coord_rect = rect
	if self.properties.HasLine():
	    rect = self.add_arrow_rects(rect)
	    self.bounding_rect = rect.grown(self.properties.GrowAmount())
	else:
	    self.bounding_rect = rect

    def add_arrow_rects(self, rect):
	if self.properties.HasLine():
	    arrow1 = self.properties.line_arrow1
	    arrow2 = self.properties.line_arrow2
	    if arrow1 or arrow2:
		width = self.properties.line_width
		for path in self.paths:
		    if not path.closed and path.len > 1:
			if arrow1 is not None:
			    type, controls, p3, cont = path.Segment(1)
			    p = path.Node(0)
			    if type == Bezier:
				dir = p - controls[0]
			    else:
				dir = p - p3
			    rect = UnionRects(rect,
					      arrow1.BoundingRect(p,dir,width))
			if arrow2 is not None:
			    type, controls, p, cont = path.Segment(-1)
			    if type == Bezier:
				dir = p - controls[1]
			    else:
				dir = p - path.Node(-2)
			    rect = UnionRects(rect,
					      arrow2.BoundingRect(p,dir,width))
	return rect

    def Info(self):
	nodes = 0
	for path in self.paths:
	    nodes = nodes + path.len
	    if path.closed:
		nodes = nodes - 1
	return _("PolyBezier (%(nodes)d nodes in %(paths)d paths)") \
	       % {'nodes':nodes, 'paths':len(self.paths)}

    def set_paths(self, paths):
	undo = (self.set_paths, self.paths)
	self.paths = tuple(paths)
	self._changed(SHAPE)
	return undo

    SetPaths = set_paths
    def Paths(self):
	return self.paths

    def PathsAsObjects(self):
	result = []
	for path in self.paths:
	    object = self.__class__(paths = (path.Duplicate(),),
				    properties = self.properties.Duplicate())
	    result.append(object)
	return result
    script_access['PathsAsObjects'] = SCRIPT_OBJECTLIST

    def AsBezier(self):
	# is `return self' enough ?
	return self.Duplicate()

    #
    #
    #

    def SaveToFile(self, file):
	Primitive.SaveToFile(self, file)
	file.PolyBezier(self.paths)

    def load_straight(self, *args):
	apply(self.paths[-1].AppendLine, args)

    def load_curve(self, *args):
	apply(self.paths[-1].AppendBezier, args)

    def load_close(self, copy_cont_from_last = 0):
	self.paths[-1].load_close(copy_cont_from_last)

    def guess_continuity(self):
	for path in self.paths:
	    path.guess_continuity()

    def load_IsComplete(self):
	if self.paths[0].len < 2:
	    # we need at least two nodes
	    return 0
	return 1

    def Blend(self, other, frac1, frac2):
	if self.__class__ != other.__class__:
	    try:
		other = other.AsBezier()
		if not other:
		    raise MismatchError
	    except AttributeError, value:
		if value == 'AsBezier':
		    raise MismatchError
		else:
		    raise

        paths = BlendPaths(self.paths, other.paths, frac1, frac2)
	blended = PolyBezier(paths = paths)
	self.set_blended_properties(blended, other, frac1, frac2)
	return blended


def CombineBeziers(beziers):
    combined = beziers[0].Duplicate()
    paths = combined.paths
    for bezier in beziers[1:]:
	paths = paths + bezier.paths
    combined.paths = paths
    return combined


