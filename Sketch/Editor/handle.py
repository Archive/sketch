# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

#
# Some convenience functions for specifying the coordinates and the
# shape of handles.
#

from Sketch import Point
import const

class Handle:

    def __init__(self, type, p, p2 = None, list = None, code = None):
	self.type = type
	self.p = p
	self.p2 = p2
	self.list = list
        self.code = code

    def __str__(self):
        return "Handle(%s, %s)" % (self.type, self.p)
    
    def __repr__(self):
        return "Handle(%s, %s, code = %s)" % (self.type, self.p, self.code)

def MakeNodeHandle(p, selected = 0, code = 0):
    return Handle(selected and const.HandleSelectedNode or const.HandleNode,
                  p, code = code)

def MakeListHandle(type, list):
    return Handle(type, None, list = list)

def MakeIndicatorList(list):
    return Handle(const.HandleIndicatorList, None, list = list)

def MakeControlHandle(p, code = 0):
    return Handle(const.HandleControlPoint, p, code = code)

def MakeCurveHandle(p):
    return Handle(const.HandleCurvePoint, p)

def MakeLineHandle(p1, p2):
    return Handle(const.HandleLine, p1, p2 = p2)

def MakeCaretHandle(p, up):
    return Handle(const.HandleCaret, p, p2 = up)

def MakePathTextHandle(p, up):
    return Handle(const.HandlePathText, p, p2 = up)
