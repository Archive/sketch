# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996 -- 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from __future__ import generators

from types import TupleType, ListType

import Sketch
from Sketch import _, Point
from Sketch.Base.const import SELECTION, UNDO, EDITED, HANDLES, STATE, \
     CURRENTINFO, TRANSACTION_END, TRANSACTION_START, INSERTED, \
     OBJECT_CHANGED, CHILDREN,  REARRANGED, TOOL, \
     SelectSet, SelectAdd, SelectSubtract, SelectSubobjects
from Sketch.Graphics import Group
from Sketch.Editor import AddEditorBuiltin

from selection import Selection


class EditorWithSelection(Sketch.Base.Publisher):

    """Interactive Editing of Sketch documents

    This class implements the main interaction logic of a window that
    allows the user to edit a document. It manages the selection and the
    tools among other things. The actual GUI widget is implemented by a
    different class (currently Sketch.UI.canvas.SketchCanvas) which uses
    an instance of this class for the actual editing logic.

    An editor instance is always bound to a single document object for
    the entire lifetime of the editor. Several editors may use the same
    document object at the same time. Likewise several canvases may use
    the same editor instance at the same time.

    Mouse Interaction

    One important part of the interface between canvas and editor is the
    mouse interaction. Mouse interaction from the user's point of view
    works as follows: The user presses a mouse button in a canvas
    window, drags the mouse with the button held down and releases the
    button.  Dragging the mouse doesn't always happen. At any point
    before the button is releases the user can press the ESC key to
    cancel the mouse operation.

    The canvas deals directly with the window system or toolkit. The
    editor only sees a more abstract sequence of calls to its methods.
    For a normal click-drag-release, the canvas calls the ButtonPress
    method when the button is pressed, the PointerMotion method when the
    mouse is dragged while holding a button and the ButtonPress method
    was called previously and finally the canvas calls the ButtonRelease
    method when the button is released. All of these calls should be
    made by the same canvas.

    The canvas should try to distinguish a click-drag-release from a
    simple click if the editor's DelayButtonPress method returns true. A
    simple click only has a button press event followed by a button
    release event with no motion events inbetween or only motion events
    that don't go further than a few pixels from the point where the
    button was pressed. For a simple click the canvas should only call
    the ButtonClick method and none of the ButtonPress, PointerMotion or
    ButtonRelease methods. This implies that the button press event may
    not result in a ButtonPress call immediately as the canvas has to
    wait until either the button release event or a motion event that
    brings the mouse too far away from the point where the button press
    event occurred.

    If the editor's DelayButtonPress method returns false the canvas
    should always behave as if the mouse interaction involves a drag and
    report the button press immediately. Of course, if no motion event
    follows it won't have to call the PointerMotion method.

    The return value of the DelayButtonPress method depends on the
    current tool, so it changes over time and DelayButtonPress has to be
    called after each button press event to determine whether it should
    be reported immediately.

    If the user cancels the operation the canvas should call the
    editor's Cancel() method.

    Handles

    The canvas displays the handles the editor uses for interaction. the
    canvas should do hit testing with the handles when the user presses
    a mouse button and pass the handle the user clicked on in the
    ButtonPress call.

    The handles the canvas should display are described by a list of
    Handle objects returned by the Handles() method. Whenever the
    handles that the canvas should display change the editor issues a
    HANDLES message. The canvas should subscribe to that message and
    call Handles() in reponse to that message to update its handles.
    """

    def __init__(self, document):
        """Initialize the editor for use with the document.

        The document should be a Sketch.Editor.InteractiveDocument
        instance.
        """
        # Reference count of the editor. Several canvases may use the
        # same editor and the editor has to be destroyed properly when
        # the last of the canvases is removed. This 'manual' reference
        # count keeps track of how many canvases use the editor.
        #
        # FIXME: It might be possible to get rid of this by using
        # weak-references in the connector and/or by relying on the GC
        # of Python 2.1 and later.
        self.refcount = 0

        #
        # Snapping flags.
        #

        # Whether snapping to guides is active
        self.snap_to_guide = 0
        # Whether snapping to the grid is active
        self.snap_to_grid = 0
        # Whether snapping to objects is active
        self.snap_to_object = 0

        # Whether snapping should also take the corners of the bounding
        # box of the selection into account. The actual situation is a
        # bit more complex (see set_correction and correct_and_snap) but
        # that's all it's really used for currently.
        self.snap_correction_rect = 1

        # Sequence of points to snap to when snapping to objects is
        # active.
        self.snap_points = ()

        # Correction. The correction is a vector to subtract from the
        # mouse position during a drag.
        self.correction = Sketch.Point(0, 0)
        self.correction_rect = None

        # Cached handles.
        self.handles = None

        # Cached info text
        self.info_text = None

        # Cached selection text
        self.selection_text = None

        # The selection
        self.selection = Selection(document)

        # The document's state numbers before and after a transaction
        self.start_state = self.end_state = 0

        # Mapping from document state numbers to selection and tool
        # states.
        self.selections = {}

        # Whether a transation is currently active
        self.in_transaction = 0

        # Messages accumulated during a transaction. During a
        # transaction this is a dict with the messages as the keys. See
        # issue.
        self.messages = None

        # The document the editor is bound to
        self.document = document

        # Since there can be more than one editor for a single document
        # the document has a manual ref count too.
        self.document.IncRef()

        # Subscribe to some document channels.
        self.subscribe_doc()

        # Init the tool. tool is the active tool instance and toolname
        # the name of the active tool
        self.tool = None
        self.toolname = ''

        # At first we're using the selection tool. Given that the tool
        # is determined by the toolbox this default will usually be
        # overridden ahortly after instantiation though.
        self.SetTool('SelectionTool')

    def IncRef(self):
        """Increase the manual reference count of the editor.

        Code that stores a reference to the editor in e.g. an instance
        variable should be careful to call IncRef and DecRef as
        appropriate to make sure that object's are destroyed properly.
        """
        self.refcount = self.refcount + 1

    def DecRef(self):
        """Decreas the manual reference count of the editor

        If the manual reference count becomes zero, call the Destroy()
        method.
        """
        self.refcount = self.refcount - 1
        if self.refcount == 0:
            self.Destroy()

    def Destroy(self):
        """Destroy the editor.

        Unsubscribe from channels and unsubscribe any subscribers to any
        of the editor's channels. DecRef the document.

        This method doesn't have to be called explicitly when all
        references always managed with IncRef and DecRef.
        """
        Sketch.Base.Publisher.Destroy(self)
        self.unsubscribe_doc()
        self.document.DecRef()
        self.document = None
        self.tool = None
        self.handles = None

    def subscribe_doc(self):
        """Internal: Subscribe to some of the document's channels"""
        self.document.Subscribe(TRANSACTION_END, self.transaction_end)
        self.document.Subscribe(TRANSACTION_START, self.transaction_start)
        self.document.Subscribe(OBJECT_CHANGED, self.object_changed)

    def unsubscribe_doc(self):
        """Internal: Unsubscribe from some of the document's channels"""
        self.document.Unsubscribe(TRANSACTION_END, self.transaction_end)
        self.document.Unsubscribe(TRANSACTION_START, self.transaction_start)
        self.document.Unsubscribe(OBJECT_CHANGED, self.object_changed)

    def issue(self, channel, *args):
        """Internal: Issue a message on the given channel.

        While in a transaction insert the tuple (channel, args) into the
        messages dict as a key. These messages will be issued once the
        transaction is finished. The order in which these messages will
        be issued is undetermined.

        When not in a transaction simply call the inherited version of
        the method with the same arguments.
        """
        if self.in_transaction:
            self.messages[(channel, args)] = 1
        else:
            apply(Sketch.Base.Publisher.issue, (self, channel) + args)

    def flush_messages(self):
        """Internal: Send all messages in the messages dict

        This method is called when a transaction ends.
        """
        issue = Sketch.Base.Publisher.issue
        for channel, args in self.messages.keys():
            apply(issue, (self, channel) + args)
        self.messages = None

    def object_changed(self, obj, what, detail):
        """Subscribed to the document's OBJECT_CHANGED messages.

        If the objects have been rearranged, normalize the selection.
        """
        # FIXME: We shouldn't have to do that here.  The selection
        # probably should do it by itself.  The selection does know
        # which document it is being used with, so it could subscribe to
        # OBJECT_CHANGED itself.  Also it should try to only normalize
        # on demand and simply set a flag that the selection is not in
        # normalized order any more. For the time being it's easiest to
        # add this here, since the editor already subscribes to lots of
        # document messages and does something similar (deselecting
        # deleted objects) in transaction_end.
        if what == CHILDREN and detail[0] == REARRANGED:
            self.selection.normalize()

    def get_state_info(self):
        """Internal: Return the current state of the editor

        The return value is a tuple of the form (objects, toolname,
        toolstate) with objects being the list of currently selected
        objects, toolname the name of the currently active tool and
        toolstate the return value of the tool's StateInfo() method.

        The state is used to remember the state of the editor for undos
        and redos.
        """
        objects = self.selection.GetObjects()
        last_objects = self.selection.GetLastSelected()
        if self.tool is not None:
            tool_name = self.toolname
            tool_state = self.tool.StateInfo()
        else:
            tool_state = None
            tool_name = ''
        return objects, last_objects, tool_name, tool_state

    def set_state_info(self, info):
        """Internal: bring the editor into the sate described by info

        The info parameter should be the return value of a previous
        get_state_info call or None. If it's None unselect any selected
        objects and call the current tool's state by calling its
        ResetStateInfo method.
        """
        if info is not None:
            objects, last_objects, tool_name, tool_state = info
            self._set_selection(objects, SelectSet)
            self._set_selection(last_objects, SelectAdd)
            if self.tool is not None:
                if self.toolname == tool_name:
                    self.tool.SetStateInfo(tool_state)
                else:
                    self.tool.ResetStateInfo()
        else:
            self._set_selection(None, SelectSet)
            if self.tool is not None:
                self.tool.ResetStateInfo()
            

    def transaction_start(self, *args):
        """Internal: Subscribed to the document's TRANSACTION_START channel

        Initialize the messages dictionary. Store the document's current
        state number in start_state the the editor's state (the return
        value of get_state_info()) in start_sel.
        """
        self.in_transaction = 1
        self.messages = {}
        self.start_state = self.document.StateNumber()
        self.start_sel = self.get_state_info()
        # self.selection.GetObjects()

    def transaction_end(self, cause, *rest):
        """Internal: Subscribed to the document's TRANSACTION_END channel
        """
        if not self.in_transaction:
            # this can happen when a new editor is created during a
            # transaction.
            return
        if cause == "normal":
            end_state = self.document.StateNumber()
            start_state = self.start_state
            edited, undo, insertions = rest
            changed = 0
            if undo == "added":
                # "added" means that the transaction was not a redo and
                # resulted a change to the document we haven't seen yet.
                # Deselect any deleted objects and store the editor
                # state in self.selections.
                self.selection.Subtract(insertions.RemovedObjects())
                self.selections[end_state] = (self.start_sel,
                                              self.get_state_info())
                changed = 1
            elif start_state != end_state:
                # If no new undo information was created, the only
                # interesting case is when the action was an undo or a
                # redo. If it was neither the state number wouldn't have
                # changed.

                # get the state_info for the state the document's now in
                # and bring the editor into that state.
                if start_state > end_state:
                    state_info = self.selections.get(end_state + 1)
                    if state_info is not None:
                        state_info = state_info[0]
                else:
                    assert start_state < end_state
                    state_info = self.selections.get(end_state)
                    if state_info is not None:
                        state_info = state_info[1]
                self.set_state_info(state_info)
                changed = 1
            if changed:
                # If the editor's state has been changed update handles,
                # info texts and issue an EDITED message
                self.update_handles()
                self.update_info_text()
                self.update_snap_points()
                self.issue(EDITED)

                # FIXME: We shouldn't have to call the selection's
                # del_lazy_attrs method.
                self.selection.del_lazy_attrs()
            # In a normal transaction end we issue all pending messages.
            self.flush_messages()
        self.in_transaction = 0

    def BeginTransaction(self, title = ''):
        """Begin a transaction with the given title."""
        self.document.BeginTransaction(title)

    def EndTransaction(self):
        """End a transaction."""
        self.document.EndTransaction()
        
    def AbortTransaction(self):
        """Abort a transaction."""
        self.document.AbortTransaction()

    #
    
    def selection_changed(self, *args):
        """Internal: call when the selection has changed

        Issue a SELECTION message and update selection text, info text
        and handles.
        """
        self.issue(SELECTION)
        self.selection_text = None
        self.update_info_text()
        self.update_handles()
        
    def update_handles(self):
        """Internal: Call this method when the handles have to be updated

        Clear the handles cache and issue a HANDLES message.
        """
        self.handles = None
        self.issue(HANDLES)

    def Handles(self):
        """Return the handles to show and interact with.
        """
        if self.handles is None:
            # There is no cached handles value, so update the cache 
            self.handles = self.tool.Handles()
        return self.handles

    def SetTool(self, toolname):
        """Switch to the tool named toolname.

        If there already is a tool call its End method. Then look up the
        tool in Sketch.Editor.toolmap and instantiate the tool for self.
        Finally, update the handles.
        """
        tool = Sketch.Editor.toolmap.get(toolname)
        old_toolname = self.toolname
        if tool is not None:
            if self.toolname != toolname:
                if self.tool is not None:
                    self.tool.End()
            self.tool = tool.Instantiate(self)
            self.toolname = toolname
        else:
            self.tool = None
            self.toolname = ''
        if self.toolname != old_toolname:
            self.issue(TOOL, self.toolname)
        self.update_handles()

    def Tool(self):
        """Return the currently active tool"""
        return self.tool

    def Document(self):
        """Return the document the editor is bound to"""
        return self.document

    def Insert(self, object):
        """Insert object into the active layer"""
        # FIXME: there is no active layer yet in the doceditor, so
        # simply use the document's as long as it's still got that :)
        parent = self.document.active_layer
        self.document.Insert(object, parent = parent)
        self.SelectObject(object)

    def Delete(self):
        """Execute a delete operation.

        Deletion is implemented by the tools, so simply call the the
        tool's Delete method.
        """
        self.tool.Delete()

    def DelayButtonPress(self):
        """Return whether the current tool prefers delayed button press events

        Call the tool's DelayButtonPress() method and return its return
        value.
        """
        return self.tool.DelayButtonPress()

    def ButtonPress(self, context, p, button, state, handle = None):
        """Called by the canvas when a mouse button is pressed

        Parameters:
            context -- Canvas in which the button press occurred

            p -- Point in document coordinates of the button press

            button -- Which button: 1 = left, 2 = middle, 3 = right
                      
            state -- Bitmask describing which mouse buttons are down and
                     the state of the modifier keys

            handle -- If not None, the handle object over which the
                      button was pressed.

        Snap the point according to whatever snapping is active then
        call the tool's ButtonPress method with both p and the snapped
        point. Finally update the info text.
        """
        snapped = self.snap_point(context, p)
        self.tool.ButtonPress(context, p, snapped, button, state, handle)
        self.update_info_text()

    def PointerMotion(self, context, p, state):
        """Called by the canvas when the mouse moves while a button is pressed

        Parameters:
            context -- Canvas reporting the motion event

            p -- Point in document coordinates of the button press

            state -- Bitmask describing which mouse buttons are down and
                     the state of the modifier keys

        Snap the point according to whatever snapping is active then
        call the tool's PointerMotion method with both p and the snapped
        point. Finally update the info text.
        """
        snapped = self.correct_and_snap(context, p)
        self.tool.PointerMotion(context, p, snapped, state)
        self.update_info_text()

    def ButtonRelease(self, context, p, button, state):
        """Called by the canvas when a mouse button is released

        Parameters:
            context -- Canvas in which the button press occurred

            p -- Point in document coordinates of the button press

            button -- Which button: 1 = left, 2 = middle, 3 = right
                      
            state -- Bitmask describing which mouse buttons are down and
                     the state of the modifier keys

        Snap the point according to whatever snapping is active then
        call the tool's ButtonRelease method with both p and the snapped
        point.

        The tool's ButtonRelease call is executed within a transaction
        because usually a tool will perform a change on the edited
        objects when the user releases the mouse button.

        The canvas should only call this method if it has called the
        ButtonPress method earlier.
        """
        snapped = self.correct_and_snap(context, p)
        self.BeginTransaction()
        try:
            try:
                self.tool.ButtonRelease(context, p, snapped, button, state)
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def ButtonClick(self, context, p, button, state, handle = None):
        """Called by the canvas when a mouse button is clicked

        A 'click' here means that the button was pressed and released
        with little or no motion in between. If the DelayButtonPress
        method does not return true the canvas should report the button
        press event immediately and should not call the ButtonClick
        method.

        Parameters:
            context -- Canvas in which the button press occurred

            p -- Point in document coordinates of the button press

            button -- Which button: 1 = left, 2 = middle, 3 = right

            state -- Bitmask describing which mouse buttons are down and
                     the state of the modifier keys

        Snap the point according to whatever snapping is active then
        call the tool's ButtonClick method with both p and the snapped
        point.

        The canvas should only call this method if it has called the
        ButtonPress method earlier.
        """
        snapped = self.snap_point(context, p)
        self.tool.ButtonClick(context, p, snapped, button, state, handle)

    def Cancel(self, context):
        """Called by the canvas when the user cancels an action.

        The context parameter is the canvas.

        Call the tool's Cancel method. 
        """
        self.tool.Cancel(context)

    def CallObjectMethod(self, aclass, methodname, args = (), title = ""):
        """Call a method on all selected objects

        Call the method named methodname on all selected objects that
        are instances of the class aclass. The optional args parameter
        should be a tuple of arguments to pass to the method.

        This entire method is executed as a transaction whose title is
        given by the optional title argument.
        """
        self.BeginTransaction(title)
        try:
            try:
                undo = self.selection.CallObjectMethod(aclass, methodname,
                                                       args)
                self.document.AddUndo(undo)
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def DrawDragged(self, device, partially):
        self.tool.DrawDragged(device, partially)

    def Show(self, device, partially = 0):
        self.tool.Show(device, partially)

    def Hide(self, device, partially = 0):
        self.tool.Hide(device, partially)

    def Cursor(self, context, p):
        """Return the name for the cursor to use at a given position

        The context argument should be a Sketch.Editor.Context instance
        with at least the canvas that will use the cursor. The position
        p should be a point giving the position in document coordinates.

        If the position is above a guide line return the name of the
        appropriate cursor. Otherwise return whatever the tool's Cursor
        method returns.
        """
        device = context.canvas.test_device()
        object = self.PickActiveObject(device, p)
        if object is not None and object.is_GuideLine:
            if object.Horizontal():
                cursor = 'CurHGuide'
            else:
                cursor = 'CurVGuide'
        else:
            cursor = self.tool.Cursor(context, p, object)
        return cursor

    #
    #   Snapping
    #

    def set_correction(self, correct):
        """Set the correction to use for the mouse interaction

        The correct argument should either be None, a point or a tuple
        consisting of a point and a rect.

        The usual case is a point which is interpreted as a vector to be
        subtracted from the mouse position given to the PointerMotion or
        ButtonRelease methods before the point is snapped.

        If a rect is given the four corner points of the rect translated
        by the correct point are considered as well during snapping as
        described in the method correct_and_snap. The rect should be
        given relative to the corrected point.
        """
        if type(correct) == TupleType:
            correct, rect = correct
        else:
            rect = None
            if correct is None:
                correct = Sketch.Point(0, 0)
        self.correction = correct
        self.correction_rect = rect

    def correct_and_snap(self, context, point):
        """Return the corrected and snapped point.

        Subtract the correction from the point and snap the result. If
        the snap_correction_rect instance variable is true also snap the
        four corner points of the correction_rect after translating it
        by the corrected point.

        Determine which point Q of these up to five points is snapped by
        the shortest distance and return the corrected point translated
        by the distance between the unsnapped Q and the snapped Q.

        The upshot is that the returned point is either itself on the
        grid or guide or whatever else it might be snapped to or that
        one of the corners of the rectangle is on the grid/guide/etc.
        """
        point = point - self.correction
        points = [point]
        if self.correction_rect and self.snap_correction_rect:
            l, b, r, t = self.correction_rect.translated(point)
            points = points + [Point(l,b), Point(l,t), Point(r,b), Point(r,t)]
        mindist = 1e100
        result = point
        for p in points:
            dist, snapped = self.snap_point_dist(context, p)
            if dist < mindist:
                mindist = dist
                result = point - p + snapped
        return result

    def snap_point(self, context, p):
        """Return the snapped point p"""
        dist, p = self.snap_point_dist(context, p)
        return p

    SnapPoint = snap_point # XXX perhaps remove snap_point altogether
        

    def snap_point_dist(self, context, p):
        """Determine the best way to snap the point p

        Return a tuple (distance, snapped_position). The distance will
        be at most what the max_snap_distance() method of the context
        argument returns.

        For each kind of active snapping determine the snapped point
        that is closest to the unsnapped point. Of these possible snaps
        return the one with the least distance.

        If no snapping is active return the unsnapped point with
        distance set to the max_snap_distance.
        """
        maxdist = context.max_snap_distance()
        pgrid = pobj = pguide = pmax = (maxdist, p)
        if self.snap_to_grid:
            pgrid = self.document.SnapToGrid(p)
        if self.snap_to_guide:
            pgh, pgv, pguide = self.document.SnapToGuide(p, maxdist)
            pguide = min(pgh, pgv, pguide)
            if pguide == pgh or pguide == pgv:
                dist, (x, y) = pguide
                if x is None:
                    if pgv[0] < maxdist:
                        x = pgv[-1][0]
                        dist = dist + pgv[0] - maxdist
                    else:
                        x = p.x
                else:
                    if pgh[0] < maxdist:
                        y = pgh[-1][1]
                        dist = dist + pgh[0] - maxdist
                    else:
                        y = p.y
                pguide = dist, Point(x, y)
        if self.snap_to_object and self.snap_points:
            x, y = p
            mindist = 1e100;
            xlow = x - maxdist; ylow = y - maxdist
            xhigh = x + maxdist; yhigh = y + maxdist
            points = self.snap_points
            ilow = 0; ihigh = len(points)
            while ilow < ihigh:
                imid = (ilow + ihigh) / 2
                if points[imid].x > xlow:
                    ihigh = imid
                else:
                    ilow = imid + 1
            for idx in range(ilow, len(points)):
                pmin = points[idx]
                if pmin.x > xhigh:
                    break
                if ylow < pmin.y < yhigh:
                    dist = max(abs(pmin.x - x), abs(pmin.y - y))
                    if dist < mindist:
                        pobj = pmin
                        mindist = dist
            if type(pobj) != TupleType:
                pobj = (abs(pobj - p), pobj)

        result = min(pgrid, pguide, pobj, pmax)
        return result

    def extract_snap_points(self, obj):
        """Internal: helper function for update_snap_points.

        Insert the return value of the object's GetSnapPoints() method
        into self.snap_points.
        """
        self.snap_points[0:0] = obj.GetSnapPoints()

    def update_snap_points(self):
        """Internal: Update the list of points for snap to objects."""
        if self.snap_to_object:
            self.snap_points = []
            self.document.WalkHierarchy(self.extract_snap_points, visible = 1,
                                        printable = 0)
            self.snap_points.sort()

    def ToggleSnapToGrid(self):
        """Toggle whether snapping to the grid is active.

        Issue a STATE message.
        """
        self.snap_to_grid = not self.snap_to_grid
        self.issue(STATE)

    def IsSnappingToGrid(self):
        """Return true iff snapping to the grid is active"""
        return self.snap_to_grid

    def ToggleSnapToGuides(self):
        """Toggle whether snapping to guides is active.

        Issue a STATE message.
        """
        self.snap_to_guide = not self.snap_to_guide
        self.issue(STATE)

    def IsSnappingToGuides(self):
        """Return true iff snapping to guides is active"""
        return self.snap_to_guide

    def ToggleSnapToObjects(self):
        """Toggle whether snapping to objects is active.

        Issue a STATE message.
        """
        self.snap_to_object = not self.snap_to_object
        self.update_snap_points()
        self.issue(STATE)

    def IsSnappingToObjects(self):
        """Return true iff snapping to objects is active"""
        return self.snap_to_object

    def InfoText(self):
        """Return a one line description of the current editor state.

        If there's an active tool call its InfoText method and return
        its result if it's returned anything. Otherwise return what the
        SelectionInfoText method returns.

        The return value is intended to be displayed in the status bar
        of an editor window.
        """
        if self.info_text is None:
            text = ''
            if self.tool is not None:
                text = self.tool.InfoText()
            if not text:
                if self.selection_text is None:
                    self.selection_text = self.SelectionInfoText()
                text = self.selection_text
            self.info_text = text
        return self.info_text

    def update_info_text(self):
        """Internal: Clear the info text cache and issue a CURRENTINFO message
        """
        self.info_text = None
        self.issue(CURRENTINFO)

    def _set_selection(self, selected, type):
        """Internal: Modify the selection
        
        The selected parameter is a list of objects while the type
        parameter indicates how the current selection is modified.
        Possible type values are:

        SelectSet        -- Select exactly the objects in the selected list
        SelectSubtract   -- Deselect the objects in selected
        SelectAdd        -- Add the objects to the selection
        SelectSubobjects -- like SelectSet here

        If the selection changed as a result of this method, issue a
        SELECTION message.
        """
        changed = 0
        if type == SelectAdd:
            if selected:
                changed = self.selection.Add(selected)
        elif type == SelectSubtract:
            if selected:
                changed = self.selection.Subtract(selected)
        else:
            # type is SelectSet or SelectSubobjects
            # set the selection. make a size selection if necessary
            changed = self.selection.SetSelection(selected)
        if changed:
            self.selection_changed()

    def selection_from_point(self, p, rect, device, path = None):
        """Return the object at point p

        Parameters:
         p -- The point in document coordinates
         rect -- small rect around p that defines the tolerance
         device -- HitTestDevice to use for hit-tests
         path -- (optional) tuple of integers describing the path to a
                compound object one of whose children should be selected
        """
        hit = self.document.selection_from_point(p, rect, device, path)
        # For historical reasons the return value is a tuple (path,
        # object). We're only interested in the object here.
        if hit:
            hit = hit[-1]
        return hit

    def selection_from_rect(self, rect):
        """Return a list with the objects in the given Rect"""
        info = self.document.selection_from_rect(rect)
        for i in range(len(info)):
            info[i] = info[i][-1]
        return info

    def Selection(self):
        """Return the selection object"""
        return self.selection

    def HasSelection(self):
        """Return whether any object is currently selected"""
        return len(self.selection) > 0

    def CountSelected(self):
        """Return the number of currently selected objects"""
        return len(self.selection)

    def SelectionInfoText(self):
        """Return a string describing the selected object(s)"""
        return self.selection.InfoText()

    def CurrentInfoText(self):
        return self.selection.CurrentInfoText()

    def SelectionBoundingRect(self):
        """Return the bounding rect of the current selection"""
        return self.selection.bounding_rect

    def CurrentObject(self):
        """If exactly one object is selected return that, None otherwise."""
        if len(self.selection) == 1:
            return self.selection.GetObjects()[0]
        return None

    def SelectedObjects(self):
        """Return the selected objects as a list.

        The list contains the objects in the order in which they are
        drawn.
        """
        # Return the selected objects as a list. They are listed in the
        # order in which they are drawn.
        return self.selection.GetObjects()

    def LastSelectedObjects(self):
        """Return the last selected objects as a list.

        The list contains the objects in the order in which they are
        drawn.
        """
        return self.selection.GetLastSelected()


    def SelectionHit(self, p, device, test_all = 1):
        """Return whether the point p hits the currently selected objects.

        The device parameter should be a HitTestDevice instance to use
        for hit testing.

        If test_all is true (the default), find the object that would be
        selected by SelectPoint and return true if it or one of its
        ancestors is currently selected and false otherwise.

        If test_all is false, just test the currently selected objects.
        """
        rect = device.HitRectAroundPoint(p)
        if len(self.selection) < 10 or not test_all:
            selection_hit = self.selection.Hit(p, rect, device)
            if not test_all or not selection_hit:
                return selection_hit
        if test_all:
            path = self.selection.GetPath()
            if len(path) > 2:
                path = path[:-1]
            else:
                path = ()
            hit = self.selection_from_point(p, rect, device, path)
            while hit:
                if hit in self.selection.GetObjects():
                    return 1
                hit = hit.parent
            return 0

    def SelectPoint(self, p, device, type = SelectSet):
        """Find object at point, and update the selection according to type

        The point p should be given in document coordinate. The device
        paramter should be a HitTestDevice instance.

        The type parameter should be one of the selection types and
        defaults to SelectSet.
        """
        self.BeginTransaction()
        try:
            try:
                if type == SelectSubobjects:
                    path = self.selection.GetPath()
                else:
                    path = ()
                rect = device.HitRectAroundPoint(p)
                selected = self.selection_from_point(p, rect, device, path)
                if selected and selected.is_GuideLine:
                    selected = None
                self._set_selection(selected, type)

            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def SelectRect(self, rect, mode = SelectSet):
        """Find all objects contained in rect and update the selection

        The rect should be given in document coordinates. The mode
        parameter should be one of the selection types and defaults to
        SelectSet.
        """
        self.BeginTransaction()
        try:
            try:
                selected = self.selection_from_rect(rect)
                self._set_selection(selected, mode)
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def SelectObject(self, objects, mode = SelectSet):
        """Update the selection with the objects in objects.

        The objects parameter may be a single GraphicsObject instance or
        a list of such objects. Update the current selection according
        to mode which defaults to SelectSet.
        """
        self.BeginTransaction()
        try:
            try:
                if type(objects) != ListType:
                    objects = [objects]
                selinfo = []
                for object in objects:
                    # hack to avoid selecting empty layers
                    if object.is_Layer and object.NumObjects() == 0:
                        continue
                    selinfo.append(object)
                self._set_selection(selinfo, mode)
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def SelectAll(self):
        """Select all selectable objects"""
        pass

    def SelectNone(self):
        """Deselect all objects"""
        self._set_selection(None, SelectSet)

    def PickActiveObject(self, device, p):
        """Return the object under point if it's selected or a guide line.

        Return None otherwise.
        """
        rect = device.HitRectAroundPoint(p)
        path = self.selection.GetPath()
        if len(path) > 2:
            path = path[:-1]
        else:
            path = ()
        hit = None
        if hit is None:
            hit = self.selection_from_point(p, rect, device, path)
        return hit

    def apply_to_selected(self, undo_text, func):
        """Call the callable object func for all objects.

        The undo_text should be a short string describing the action. It
        will usually be displayed in the Undo and Redo menu entries.

        The callable object when called with an object should return
        undo information for any modification it does.
        """
        if self.selection:
            self.BeginTransaction(undo_text)
            try:
                try:
                    self.document.AddUndo(self.selection.ForAllUndo(func))
                except:
                    self.AbortTransaction()
            finally:
                self.EndTransaction()

    def RemoveSelected(self):
        """Delete all selected objects."""
        self.BeginTransaction(_("Delete"))
        try:
            try:
                for parent, objects in self.selection_grouped_by_parent():
                    self.document.AddUndo(parent.RemoveObjects(objects))
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def TransformSelected(self, trafo, undo_text = ""):
        """Apply the transformation trafo to all selected objects.

        The undo_text should be a short string describing the action. It
        will usually be displayed in the Undo and Redo menu entries.
        """
        self.apply_to_selected(undo_text, lambda o, t = trafo: o.Transform(t))

    def TranslateSelected(self, offset, undo_text = ""):
        """Translate all selected objects by the vector offset.

        The undo_text should be a short string describing the action. It
        will usually be displayed in the Undo and Redo menu entries.
        """
        self.apply_to_selected(undo_text, lambda o, v = offset: o.Translate(v))

    def RemoveTransformation(self):
        """Call the RemoveTransformation method of all selected objects."""
        self.apply_to_selected(_("Remove Transformation"),
                               lambda o: o.RemoveTransformation())

    def group_selected(self, title, creator):
        """Group the selected objects.

        title is the title of the action suitable for the
        BeginTransaction call. creator is a constructor for the group
        object, usually the Group class, but it can be any callable that
        accepts a list of graphics objects and returns a group like
        object.

        The selected objects are removed from the document, passed to
        creator and the resulting group is inserted in the deepest
        common ancestor of the originally selected objects on top of the
        existing children of that object. If there is no such object,
        e.g. because the selected objects were from more than one layer,
        insert the group into the active layer.
        """
        self.BeginTransaction(title)
        try:
            try:
                # the selected objects.
                objects = self.selection.GetObjects()
                # the path (i.e. the sequence of indices) leading to the
                # common ancestor
                path = self.selection.CommonPath()

                # remove the object and create the group.
                self.RemoveSelected()
                group = creator(objects)

                # determine where to insert the group
                if path:
                    parent = self.document[path]
                else:
                    # There is not common ancestor. Use the active
                    # layer. FIXME: there is no active layer yet in the
                    # doceditor, so simply use the document's as long as
                    # it's still got that :)

                    parent = self.document.active_layer

                # insert the group and select it
                self.document.Insert(group, parent = parent)
                self._set_selection(group, SelectSet)
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def CanGroup(self):
        """Return true iff the currently selected object(s) can be grouped"""
        return len(self.selection) > 1

    def GroupSelected(self):
        """Group the selected objects into an ordinary group"""
        if self.CanGroup():
            self.group_selected(_("Create Group"), Group)

    def CanUngroup(self):
        """Return true iff the currently selected object can be ungrouped"""
        return len(self.selection) == 1 and self.CurrentObject().is_Group

    def UngroupSelected(self):
        """If the selected object is a group, ungroup it.

        Ungrouping means that the group itself is removed and the
        children of the group are inserted directly into the group's
        parent in the position in the stacking order where the group
        was. The children become the new selection.

        This method handles undo info itself.
        """
        if self.CanUngroup():
            self.BeginTransaction(_("Ungroup Group"))
            try:
                try:
                    # position to insert the group's children into the
                    # group's parent
                    index = self.selection.GetPath()[-1]

                    # The selected object. We know it's a group because
                    # CanUngroup succeeded.
                    group = self.CurrentObject()

                    # remember the parent now, because after the group
                    # object doesn't have a parent anymore.
                    parent = group.parent

                    # Remove the group
                    self.RemoveSelected()

                    # Ungroup and insert the children
                    objects = group.Ungroup()
                    self.document.Insert(objects, parent = parent,
                                         index = index)

                    # select the children
                    self._set_selection(objects, SelectSet)
                except:
                    self.AbortTransaction()
            finally:
                self.EndTransaction()

    def selection_grouped_by_parent(self):
        """Generate the selected objects grouped by parent

        The return value is an iterable of (parent, children) pairs.  In
        each pair, the children pair are all selected objects that are
        direct children of the parent.  The order is the same as in the
        return value of the SelectedObjects() method as far as possible
        withing the constraint that no parent will occur twice.
        """
        # This method is a bit more difficult than may appear at a first
        # glance.  Each parent must only be listed once.  However, even
        # though the objects in the selection are sorted in such a way
        # that the children of one parent immediately follow each other,
        # we can't make use of that order.  For instance, if an object
        # inside a group is selected and children of the group's parent
        # from both below the group and above the group are selected
        # too, the children of the group's parent are separated by the
        # group's child.  We deal with such situations by looking at all
        # selected objects and building the complete children before
        # returning anything.
        groups = {}
        order = []
        for obj in self.selection.GetObjects():
            parent = obj.parent
            if id(parent) not in groups:
                order.append(parent)
            groups.setdefault(id(parent), []).append(obj)
        for parent in order:
            yield parent, groups[id(parent)]

    def DuplicateSelected(self):
        """Duplicate the selected objects

        The usual action is the following: Each selected object is
        duplicated and then inserted into the document immediately above
        the original object and into the same compound object
        (i.e. layer or group) as the original object.  Since the actual
        duplication is handled by the compound objects, the actual
        behavior may be different, but the normal layers and groups
        behave as described.

        All the newly created duplicates will be selected afterwards.

        This method handles undo info itself.
        """
        self.BeginTransaction(_("Duplicate"))
        try:
            try:
                duplicates = []
                for parent, objects in self.selection_grouped_by_parent():
                    new_duplicates, undo = parent.DuplicateObjects(objects)
                    duplicates.extend(new_duplicates)
                    self.document.AddUndo(undo)
                self._set_selection(duplicates, SelectSet)
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def MoveSelectedToTop(self):
        """Move the selected objects to the top

        All selected objects are moved to the top of the stacking order
        within their parent object.  All selected objects that share an
        immediate parent will keep the same relative stacking order.
        Since the actual reordering is done the compound objects
        themselves, it may happen that the actual behavior differs from
        this, but for normal layers and groups it works as described
        above.

        All objects that are selected before this method was called will
        also be selected afterwards.

        This method handles undo info itself.
        """
        self.BeginTransaction(_("Move To Top"))
        try:
            try:
                for parent, objects in self.selection_grouped_by_parent():
                    self.document.AddUndo(parent.MoveObjectsToTop(objects))
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def MoveSelectedToBottom(self):
        """Move the selected objects to the bottom

        All selected objects are moved to the bottom of the stacking
        order within their parent object.  All selected objects that
        share an immediate parent will keep the same relative stacking
        order.  Since the actual reordering is done the compound objects
        themselves, it may happen that the actual behavior differs from
        this, but for normal layers and groups it works as described
        above.

        All objects that are selected before this method was called will
        also be selected afterwards.

        This method handles undo info itself.
        """
        self.BeginTransaction(_("Move To Bottom"))
        try:
            try:
                for parent, objects in self.selection_grouped_by_parent():
                    self.document.AddUndo(parent.MoveObjectsToBottom(objects))
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def MoveSelectedUp(self):
        """Move the selected objects one step higher in the stacking order

        All selected objects are moved one step higher in the stacking
        order within their parent object.  All selected objects that
        share an immediate parent will keep the same relative stacking
        order.  Since the actual reordering is done the compound objects
        themselves, it may happen that the actual behavior differs from
        this, but for normal layers and groups it works as described
        above.

        All objects that are selected before this method was called will
        also be selected afterwards.

        This method handles undo info itself.
        """
        self.BeginTransaction(_("Move Up"))
        try:
            try:
                for parent, objects in self.selection_grouped_by_parent():
                    self.document.AddUndo(parent.MoveObjectsUp(objects))
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def MoveSelectedDown(self):
        """Move the selected objects one step lower in the stacking order

        All selected objects are moved one step lower in the stacking
        order within their parent object.  All selected objects that
        share an immediate parent will keep the same relative stacking
        order.  Since the actual reordering is done the compound objects
        themselves, it may happen that the actual behavior differs from
        this, but for normal layers and groups it works as described
        above.

        All objects that are selected before this method was called will
        also be selected afterwards.

        This method handles undo info itself.
        """
        self.BeginTransaction(_("Move Down"))
        try:
            try:
                for parent, objects in self.selection_grouped_by_parent():
                    self.document.AddUndo(parent.MoveObjectsDown(objects))
            except:
                self.AbortTransaction()
        finally:
            self.EndTransaction()

    def SetProperties(self, **kw):
        """Call the SetProperties method of all selected objects.

        All keyword arguments to this method are passed to the object's
        SetProperties method as keyword arguments.
        """
        self.apply_to_selected(_("Set Properties"),
                               lambda o, kw=kw: apply(o.SetProperties, (), kw))

    def CanCombineBeziers(self):
        """Return true if the selection contains at least two objects
        that are curves or can be converted to curves.
        """
        curves = 0
        for obj in self.SelectedObjects():
            if curves < 2:
                if obj.is_curve:
                    curves += 1
            else:
                break
        return curves > 1

    def CombineBeziers(self):
        """Combine selected objects into a single PolyBezier.

        Combines as many as possible of the selected objects into a
        single PolyBezier, first converting them to curves if necessary.
        The objects that are not curves and can not be converted to
        curves are simply kept in the selection.

        To preserve as much as possible of the status before the
        operation, the layer and stacking order of the resulted object
        will be those of the first object that will be combined.
        """
        if self.CanCombineBeziers():
            AddUndo = self.document.AddUndo
            self.BeginTransaction(_("Combine Beziers"))
            try:
                try:
                    selection = []
                    beziers = []
                    for obj in self.SelectedObjects():
                        if obj.is_curve:
                            # If it's the first curve encountered,
                            # remember its layer and index.
                            if not len(beziers):
                                parent = obj.parent
                                index = parent.GetObjects().index(obj)
                            beziers.append(obj.AsBezier())
                            AddUndo(obj.parent.Remove(obj))
                        else:
                            selection.append(obj)

                    # create and insert the new object
                    combined = Sketch.Editor.curvefunc.CombineBeziers(beziers)
                    self.document.Insert(combined, parent = parent,
                                         index = index)
                    selection.append(combined)
                    self._set_selection(selection, SelectSet)
                except:
                    self.AbortTransaction()
            finally:
                self.EndTransaction()

    def CanSplitBeziers(self):
        """Return true if the selection contains at least a Polybezier
        which has more than one path.
        """
        can = 0
        for obj in self.SelectedObjects():
            if obj.is_Bezier and len(obj.Paths()) > 1:
                can = 1
                break
        return can

    def SplitBeziers(self):
        """Split selected curves into paths.

        The objects that are not curves with more than one path are
        simply kept in the selection.
        """
        if self.CanSplitBeziers():
            self.BeginTransaction(_("Split Beziers"))
            try:
                try:
                    selection = []
                    for obj in self.SelectedObjects():
                        if obj.is_Bezier and len(obj.Paths()) > 1:
                            parent = obj.parent
                            idx = parent.GetObjects().index(obj)
                            beziers = obj.PathsAsObjects()
                            self.document.AddUndo(parent.Remove(obj))
                            self.document.AddUndo(parent.Insert(beziers,
                                                                index = idx))
                            selection.extend(beziers)
                        else:
                            selection.append(obj)
                    self._set_selection(selection, SelectSet)
                except:
                    self.AbortTransaction()
            finally:
                self.EndTransaction()

    def CanConvertToCurve(self):
        """Return true if the selection contains at least one object that
        can be converted to curves and is not already a PolyBezier.
        """
        can = 0
        for obj in self.SelectedObjects():
            if obj.is_curve and not obj.is_Bezier:
                can = 1
                break
        return can

    def ConvertToCurve(self):
        """Convert selected objects to curves.

        Convert as many as possible of the selected objects to
        PolyBeziers by replacing each of them with a curve containing the
        paths returned by the object's AsBezier method. The objects that
        can not be converted to curves are simply kept in the selection.
        """
        if self.CanConvertToCurve():
            self.BeginTransaction(_("Convert To Curve"))
            try:
                try:
                    selection = []
                    for obj in self.SelectedObjects():
                        if obj.is_curve and not obj.is_Bezier:
                            bezier = obj.AsBezier()
                            parent = obj.parent
                            self.document.AddUndo(parent.ReplaceChild(obj,
                                                                      bezier))
                            selection.append(bezier)
                        else:
                            selection.append(obj)
                    self._set_selection(selection, SelectSet)
                except:
                    self.AbortTransaction()
            finally:
                self.EndTransaction()

sense_selection = lambda context: context.editor.HasSelection()

AddEditorBuiltin('undo', ''"Undo", lambda context: context.document.Undo(),
                 sensitive = lambda context: context.document.CanUndo(),
                 dyntext = lambda context: context.document.UndoMenuText(),
                 channels = (UNDO,))
AddEditorBuiltin('redo', ''"Redo", lambda context: context.document.Redo(),
                 sensitive = lambda context: context.document.CanRedo(),
                 dyntext = lambda context: context.document.RedoMenuText(),
                 channels = (UNDO,))

AddEditorBuiltin('delete', ''"Delete",
                 lambda context: context.editor.Delete(),
                 sensitive = sense_selection, channels = (SELECTION,))

AddEditorBuiltin('duplicate', ''"Duplicate",
                 lambda context: context.editor.DuplicateSelected(),
                 sensitive = sense_selection, channels = (SELECTION,))

AddEditorBuiltin('group', ''"Group",
                 lambda context: context.editor.GroupSelected(),
                 sensitive = lambda context: context.editor.CanGroup(),
                 channels = (SELECTION,))

AddEditorBuiltin('ungroup', ''"Ungroup",
                 lambda context: context.editor.UngroupSelected(),
                 sensitive = lambda context: context.editor.CanUngroup(),
                 channels = (SELECTION,))

AddEditorBuiltin('move_one_up', ''"Move Up",
                 lambda context: context.editor.MoveSelectedUp(),
                 sensitive = sense_selection, channels = (SELECTION,))
AddEditorBuiltin('move_one_down', ''"Move Down",
                 lambda context: context.editor.MoveSelectedDown(),
                 sensitive = sense_selection, channels = (SELECTION,))
AddEditorBuiltin('move_to_top', ''"Move To Top",
                 lambda context: context.editor.MoveSelectedToTop(),
                 sensitive = sense_selection, channels = (SELECTION,))
AddEditorBuiltin('move_to_bottom', ''"Move To Bottom",
                 lambda context: context.editor.MoveSelectedToBottom(),
                 sensitive = sense_selection, channels = (SELECTION,))

AddEditorBuiltin('snap_to_grid', ''"Snap to Grid",
                 lambda context: context.editor.ToggleSnapToGrid(),
                 checked = lambda context: context.editor.IsSnappingToGrid(),
                 channels = (STATE,))
AddEditorBuiltin('snap_to_guides', ''"Snap to Guides",
                 lambda context: context.editor.ToggleSnapToGuides(),
                 checked=lambda context: context.editor.IsSnappingToGuides(),
                 channels = (STATE,))
AddEditorBuiltin('snap_to_objects', ''"Snap to Objects",
                 lambda context: context.editor.ToggleSnapToObjects(),
                 checked=lambda context: context.editor.IsSnappingToObjects(),
                 channels = (STATE,))

AddEditorBuiltin('combine_beziers', ''"Combine Beziers",
                  lambda context: context.editor.CombineBeziers(),
                  sensitive = lambda ctxt: ctxt.editor.CanCombineBeziers(),
                  channels = (SELECTION,))
AddEditorBuiltin('split_beziers', ''"Split Beziers",
                 lambda context: context.editor.SplitBeziers(),
                 sensitive = lambda ctxt: ctxt.editor.CanSplitBeziers(),
                 channels = (SELECTION,))
AddEditorBuiltin('convert_to_curve', ''"Convert To Curve",
                 lambda context: context.editor.ConvertToCurve(),
                 sensitive = lambda ctxt: ctxt.editor.CanConvertToCurve(),
                 channels = (SELECTION,))
