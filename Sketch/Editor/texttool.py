# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from types import TupleType
from math import pi, floor

import Sketch

from Sketch import _, Translation, Rotation, Scale, SingularMatrix, Polar
from Sketch.Base.const import SelectDrag, SELECTION
from Sketch.Graphics.text import CommonText, SimpleText, InternalPathText, \
     pathtext, PathText
from Sketch.Graphics import EmptyPattern, SolidPattern, StandardColors
from Sketch.Editor import Creator, Editor, AddEditorBuiltin

import handle, tools


class SimpleTextCreator(Creator):

    creation_text = _("Create Text")

    def __init__(self, editor):
        Creator.__init__(self)
        self.editor = editor

    def ButtonDown(self, p, button, state):
        Creator.DragStart(self, p)

    def MouseMove(self, p, state):
        p = self.apply_constraint(p, state)
        Creator.MouseMove(self, p, state)
        
    def ButtonUp(self, p, button, state):
        p = self.apply_constraint(p, state)
        Creator.DragStop(self, p)

    def ButtonUp(self, p, button, state):
        Creator.DragStop(self, p)

    def DrawDragged(self, device, partially):
        device.DrawLine(self.drag_start, self.drag_cur)

    def apply_constraint(self, p, state):
        if state & Sketch.Editor.ConstraintMask:
            r, phi = (p - self.drag_start).polar()
            pi12 = pi / 12
            phi = pi12 * floor(phi / pi12 + 0.5)
            p = self.drag_start + Polar(r, phi)
        return p

    def CreatedObject(self):
        trafo = Translation(self.drag_start)
        r, phi = (self.drag_cur - self.drag_start).polar()
        if r:
            trafo = trafo(Rotation(phi))
        text = SimpleText(trafo = trafo)
        fill_pattern = SolidPattern(StandardColors.black) 
        text.SetProperties(fill_pattern = fill_pattern,
                           line_pattern = EmptyPattern)
        def create(editor = self.editor, text = text):
            editor.Insert(text)
            return text

        return text, create


class PathTextCreator(Creator):

    creation_text = _("Create PathText")

    def __init__(self, editor, curve):
        Creator.__init__(self)
        self.editor = editor
        self.curve = curve

    def ButtonDown(self, p, button, state):
        Creator.DragStart(self, p)

    def ButtonUp(self, p, button, state):
        Creator.DragStop(self, p)

    def CreatedObject(self):
        x, y = self.drag_cur
        paths = self.curve.Paths()
        t = paths[0].nearest_point(x, y)
        text = InternalPathText(start_pos = t, paths = paths)
        fill_pattern = SolidPattern(StandardColors.black) 
        text.SetProperties(fill_pattern = fill_pattern,
                           line_pattern = EmptyPattern)
        text.paths = paths
        def create(editor = self.editor, curve = self.curve, text = text,
                   pos = t):
            return _insert_path_text(editor, curve, text, pos)
        return text, create
       

def _insert_path_text(editor, curve, text, pos):
    parent = curve.parent
    path = curve.SelectionInfo()[0]
    pathtext = PathText(text, curve, start_pos = pos)
    editor.Document().AddUndo(parent.ReplaceChild(curve, pathtext))
    editor.SelectObject(pathtext.text)
    return pathtext.text

class CommonTextEditor(Editor):

    EditedClass = CommonText

    def __init__(self, object, creator = None):
	Editor.__init__(self, object)
	self.caret = 0
        if creator is not None:
            self.creator = creator
            self.create = 1
        else:
            self.create = 0

    def Destroy(self, editor):
        pass

    def set_object(self, object):
        if object is not self.object:
            self.object = object

    def ButtonDown(self, p, button, state):
	Editor.DragStart(self, p)

    def ButtonUp(self, p, button, state):
	Editor.DragStop(self, p)

    def SetCaret(self, caret):
        if caret > len(self.text):
            caret = len(self.text)
	self.caret = caret

    def Caret(self):
	return self.caret

    def InsertCharacter(self, char):
        undo = Sketch.Base.NullUndo
        if len(char) == 1 and self.properties.font.IsPrintable(char):
            text = self.text;   caret = self.caret
            text = text[:caret] + char + text[caret:]
            undo = self.SetText(text)
            self.SetCaret(caret + 1)
        if self.create:
            # self.document.BeginTransaction()
            self.set_object(self.creator())
            #self.document.Insert(self.object)
            # self.document.EndTransaction()
            del self.creator
            self.create = 0
            undo = Sketch.Base.NullUndo
        return undo

    def DeleteCharBackward(self):
	if self.text and self.caret > 0:
	    text = self.text; caret = self.caret
	    text = text[:caret - 1] + text[caret:]
            self.SetCaret(caret - 1)
	    return self.SetText(text)
	return Sketch.Base.NullUndo

    def DeleteCharForward(self):
	if self.text and self.caret < len(self.text):
	    text = self.text; caret = self.caret
	    text = text[:caret] + text[caret + 1:]
	    return self.SetText(text)
	return Sketch.Base.NullUndo

    def MoveForwardChar(self):
	if self.caret < len(self.text):
	    self.SetCaret(self.caret + 1)
	return Sketch.Base.NullUndo

    def MoveBackwardChar(self):
	if self.caret > 0:
	    self.SetCaret(self.caret - 1)
	return Sketch.Base.NullUndo

    def MoveToBeginningOfLine(self):
	self.SetCaret(0)
	return Sketch.Base.NullUndo

    def MoveToEndOfLine(self):
	self.SetCaret(len(self.text))
	return Sketch.Base.NullUndo




class SimpleTextEditor(CommonTextEditor):

    EditedClass = SimpleText

    def GetHandles(self):
	a = self.properties
	pos, up = a.font.TextCaretData(self.text, self.caret, a.font_size)
	pos = self.trafo(self.atrafo(pos))
	up = self.trafo.DTransform(up)
	return [handle.MakeCaretHandle(pos, up)]

    def ButtonUp(self, p, button, state):
        trafo = self.trafo(self.atrafo(Scale(self.properties.font_size)))
        trafo = trafo.inverse()
        p2 = trafo(p)
        pts = self.properties.font.TypesetText(self.text + ' ')
        dists = []
        for i in range(len(pts)):
            dists.append((abs(pts[i].x - p2.x), i))
        caret = min(dists)[-1]
        self.SetCaret(caret)
	return Sketch.Base.NullUndo

    def Destroy(self, editor):
        CommonTextEditor.Destroy(self, editor)
        if self.object.document is not None:
            self.object.document.AddAfterHandler(maybe_remove_text,
                                                 (self.object, editor))


def maybe_remove_text(text, editor):
    if text.parent is not None and not text.text:
        print 'Removing Text'
        editor.DeselectObject(text)
        text.document.AddUndo(text.parent.Remove(text))
        


class InternalPathTextEditor(CommonTextEditor):

    EditedClass = InternalPathText

    def GetHandles(self):
        a = self.properties
        if self.caret > 0 and self.trafos:
            # special case to deal with here: the characters that fall
            # off the end of the path are not visible. If the caret is
            # in this invisible area, display the caret after the last
            # visible character
            caret = 1
            index = min(self.caret, len(self.text), len(self.trafos)) - 1
            text = self.text[index]
            trafo = self.trafos[index]
        else:
            caret = 0
            if self.text and self.trafos:
                text = self.text[0]
                trafo = self.trafos[0]
            else:
                # XXX fix this
                self.start_point = self.paths[0].point_at(self.start_pos)
                return [handle.MakeNodeHandle(self.start_point, 1)]
        pos, up = a.font.TextCaretData(text, caret, a.font_size)
        pos = trafo(pos)
        up = trafo.DTransform(up)
        self.start_point = self.trafos[0].offset()
        return [handle.MakeCaretHandle(pos, up), #]
                handle.MakeNodeHandle(self.start_point, 1)]

    selection = None
    def SelectHandle(self, handle, mode = Sketch.Base.const.SelectSet):
        self.selection = handle

    def SelectPoint(self, p, test, mode):
        self.selection = None

    def ButtonDown(self, p, button, state):
        self.cache = {}
        if self.selection is not None:
            return p - self.start_point

    def nearest_start_pos(self, p):
        try:
            x, y = self.trafo.inverse()(p)
            t = self.paths[0].nearest_point(x, y)
        except SingularMatrix:
            # XXX
            t = 0.0
        return t
        

    def DrawDragged(self, device, partially):
        if self.selection is not None:
            text = self.text; trafos = self.trafos
            font = self.properties.font; font_size = self.properties.font_size
            t = self.nearest_start_pos(self.drag_cur)
            trafos = map(self.trafo, pathtext(self.paths[0], t, text, font,
                                              font_size, self.model))
            device.BeginComplexText(0, self.cache)
            for idx in range(len(trafos)):
                char = text[idx]
                if char not in '\n\r':
                    device.DrawComplexText(char, trafos[idx], font, font_size)
            device.EndComplexText()
            device.ResetFontCache()

    def ButtonUp(self, p, button, state):
        if self.selection is not None:
            CommonTextEditor.ButtonUp(self, p, button, state)
            return self.SetStartPos(self.nearest_start_pos(self.drag_cur))
        else:
            self.move_caret(p)

    def move_caret(self, p):
        if self.trafos:
            dists = []
            for i in range(len(self.trafos)):
                dists.append((abs(p - self.trafos[i].offset()), i))
                
            char = self.text[len(self.trafos) - 1]
            width = self.properties.font.metric.char_width(ord(char)) / 1000.0
            pos = self.trafos[-1](width * self.properties.font_size, 0)
            dists.append((abs(p - pos), len(self.trafos)))
            caret = min(dists)[-1]
            self.SetCaret(caret)


class TextToolInstance(tools.ToolInstance):

    title = ''"Text"

    def __init__(self, editor):
        tools.ToolInstance.__init__(self, editor)
        self.texteditor = None
        self.editor.Subscribe(SELECTION, self.selection_changed)

    def End(self):
        self.editor.Unsubscribe(SELECTION, self.selection_changed)
        tools.ToolInstance.End(self)

    def selection_changed(self):
        objects = self.editor.SelectedObjects()
        if len(objects) == 1 and objects[0].is_Text:
            object = objects[0]
            if self.texteditor is None or object is not self.texteditor.object:
                self.get_texteditor(object)
            return
        if self.texteditor is not None:
            self.texteditor.Destroy(self.editor)

    def DelayButtonPress(self):
        return 0

    def check_selection(self):
        if self.texteditor is None:
            objects = self.editor.SelectedObjects()
            if len(objects) != 1 or not objects[0].is_Text:
                self.editor.SelectNone()
            else:
                self.get_texteditor(objects[0])

    def get_texteditor(self, object, creator = None):
        if self.texteditor is not None:
            self.texteditor.Destroy(self.editor)
        self.texteditor = Sketch.Editor.GetTextEditor(object, creator=creator)
        self.editor.update_handles()

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        object = None
        hide_handles = 1
        self.check_selection()

        test = context.test_device()
        if handle is not None:
            self.texteditor.SelectHandle(handle, SelectDrag)
            object = self.texteditor
        elif self.editor.SelectionHit(p, test, test_all = 0):
            # The user pressed the button over an already selected object.
            # Prepare to edit/transform the current selection
            self.texteditor.SelectPoint(p, test, SelectDrag)
            object = self.texteditor
            hide_handles = 0
        else:
            # The user pressed the button somewhere else.
            # XXX ugly kludge
            rect = test.HitRectAroundPoint(p)
            object = self.editor.selection_from_point(p, rect, test)
            if object:
                if object.is_Text:
                    self.editor.SelectObject(object)
                    self.get_texteditor(object)
                    self.texteditor.SelectPoint(p, test, SelectDrag)
                    object = self.texteditor
                elif object.is_curve and not object.parent.is_PathTextGroup:
                    # XXX testing the parent is a kludge. A better
                    # implementation requires a different behaviour of
                    # selection_from_point
                    object = PathTextCreator(self.editor, object)
                else:
                    object = None
            if not object:
                object = Sketch.Editor.SimpleTextCreator(self.editor)
                self.editor.SelectNone()
        if object is not None:
            self.begin_edit_object(context, object, snapped, button, state,
                                   hide_handles = hide_handles)
                

    def ButtonRelease(self, context, p, snapped, button, state):
	object = self.end_edit_object(context, snapped, button, state)
        if isinstance(object, Sketch.Editor.SimpleTextCreator) \
           or isinstance(object, Sketch.Editor.PathTextCreator):
            object, creator = object.CreatedObject()
            self.get_texteditor(object, creator = creator)
        else:
            self.editor.update_handles()

    def CallObjectEditorMethod(self, aclass, methodname, args = ()):
        if type(args) != TupleType:
            args = (args,)
        if isinstance(self.texteditor, aclass):
            self.document.BeginTransaction()
            undo = apply(getattr(self.texteditor, methodname), args)
            self.document.AddUndo(undo)
            self.document.EndTransaction()
            # XXX having to call update_handles here to make sure that
            # the caret is updated when "MoveForwardChar" etc. are
            # executed is ugly.
            self.editor.update_handles()

    def Handles(self):
        if self.texteditor is not None:
            return self.texteditor.GetHandles()
        else:
            return []

    def Cursor(self, context, p, object):
        cursor = None
        if object is not None:
            if object.is_Text:
                cursor = 'CurTextEdit'
            elif object.is_curve and not object.parent.is_PathTextGroup:
                cursor = 'CurTextPath'
        return cursor
                     
    def StateInfo(self):
        if self.texteditor is not None:
            return self.texteditor.Caret()
        return None

    def SetStateInfo(self, info):
        if self.texteditor is not None and info is not None:
            self.texteditor.SetCaret(info)

TextTool = tools.ToolInfo("TextTool", TextToolInstance, cursor = 'CurText',
                          active_cursor = 1)

#
#
#


def insert_char(context, str):
    tool = context.editor.Tool()
    if isinstance(tool, TextToolInstance):
        tool.CallObjectEditorMethod(Sketch.Editor.CommonTextEditor,
                                    'InsertCharacter', str)

AddEditorBuiltin(insert_char, ''"Insert Character", pass_key = 1)

def call_method(context, method):
    tool = context.editor.Tool()
    if isinstance(tool, TextToolInstance):
        tool.CallObjectEditorMethod(Sketch.Editor.CommonTextEditor, method)

AddEditorBuiltin('move_char_forward', ''"Forward Character", call_method,
                 args = ('MoveForwardChar',))

AddEditorBuiltin('move_char_backward', ''"Backward Character", call_method,
                 args = ('MoveBackwardChar',))

AddEditorBuiltin('delete_char_forward', ''"Delete Character", call_method,
                 args = ('DeleteCharForward',))
AddEditorBuiltin('delete_char_backward', ''"Delete Character", call_method,
                 args = ('DeleteCharBackward',))
AddEditorBuiltin('move_to_beginning_of_line', ''"Beginning of Line",
                 call_method, args = ('MoveToBeginningOfLine',))

AddEditorBuiltin('move_to_end_of_line', ''"End of Line", call_method,
                 args = ('MoveToEndOfLine',))
