# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import operator
from math import acos, pi

import Sketch
from Sketch import CreatePath, Trafo, SingularMatrix, NullPoint, Point, \
     ContSmooth, ContAngle, _sketch
from Sketch.Base.warn import warn, INTERNAL

do_profile = 0

def smoothen_freehand_curve(object, pt_per_pixel):
    if do_profile:
        import profile
        warn(INTERNAL, 'profiling...')
        prof = profile.Profile()
        prof.runctx('smoothen_freehand_curve2(object, pt_per_pixel)',
                    globals(), locals())
        prof.dump_stats('/tmp/smoother.prof')
        warn(INTERNAL, 'profiling... (done)')
    else:
        smoothen_freehand_curve2(object, pt_per_pixel)
    

def smoothen_freehand_curve2(object, pt_per_pixel):
    remove_points_dist = 1.0 * pt_per_pixel
    accuracy = Sketch.Editor.preferences.freehand_accuracy * pt_per_pixel
    corner_radius = 4 * pt_per_pixel
    paths = object.Paths()
    result = []
    for path in paths:
        path = smoothen_curve(path.as_polygon(pt_per_pixel),
                              accuracy = accuracy,
                              average_repeat = 1,
                              remove_points_dist = remove_points_dist,
                              corner_radius = corner_radius)
        result.append(path)
    undo = object.SetPaths(tuple(result))

    

def smoothen_curve(points, average_repeat = 4,
                   remove_points_dist = 2.0,
                   corner_angle = pi / 3,
                   corner_radius = 4.0,
                   accuracy = 2.0):
    result = CreatePath()
    result.AppendLine(points[0])

    points = remove_near_points(points, remove_points_dist)
    
    segments = find_corners2(points, corner_angle, corner_radius)

    for i in range(len(segments)):
        segments[i] = average(segments[i], average_repeat, accuracy)

    for i in range(len(segments)):
        fit_segment(result, segments[i], accuracy)
    return result

def remove_near_points(points, min_dist):
    if len(points) > 2:
        points = points[:]
        for i in range(len(points) - 2, -1, -1):
            dist = abs(points[i] - points[i + 1])
            if dist < min_dist:
                del points[i]
    return points


def corner_angle2(v1, v2):
    try:
        prod = v1.normalized() * v2.normalized()
    except ZeroDivisionError:
        return 0
    if -1.0 <= prod <= 1.0:
        angle = acos(prod)
    elif prod > 1.0:
        angle = 0
    else:
        angle = pi
    return angle

def find_corners2(points, max_angle, corner_radius):
    corners = _sketch.FindFreehandCorners(points, max_angle, corner_radius)
    segments = []
    for i in range(len(corners) - 1):
        segments.append(points[corners[i]:corners[i + 1] + 1])
    return segments

def average(points, repeat, accuracy):
    if len(points) > 2:
        for k in range(repeat):
            p1, p2 = points[:2]
            for i in range(1, len(points) - 1):
                p0, p1, p2 = p1, p2, points[i + 1]
                f1 = accuracy / abs(p1 - p0)
                f2 = accuracy / abs(p1 - p2)
                if f1 > 1: f1 = 1
                if f2 > 1: f2 = 1
                points[i] = (f1 * p0 + p1 + f2 * p2) / (1 + f1 + f2)
    return points

def estimate_params(points):
    return _sketch.FreehandEstimateParams(points)

def tangents(points):
    t1 = (points[1] - points[0]).normalized()
    t2 = (points[-2] - points[-1]).normalized()
    return t1, t2

def tangents_at(points, index):
    t1 = points[index] - points[index - 1]
    t2 = points[index + 1] - points[index]
    t = 0.5 * (t1 + t2)
    t = t.normalized()
    return -t, t

def distances(points, params, p0, p1, p2, p3):
    return _sketch.FreehandDistances(points, params, p0, p1, p2, p3)

def fit_bezier_with_tangents(points, params, t1, t2):
    # Given the points and associated parameters, compute the optimal
    # Bezier control points p1 and p2 positioned along the tangents t1
    # starting at p0 = points[0] and t2 starting at p3 = points[-1]
    # respetively. Return (alpha1, alpha2) so that p1 = p0 + alpha1 * t1
    # and p2 = p3 + alpha2 * t2
    return _sketch.FitBezierWithTangents(points, params, t1, t2)

def calc_error(points, params, p0, p1, p2, p3):
    dists = map(abs, distances(points, params, p0, p1, p2, p3))
    error = 0; index = -1
    for i in range(len(dists)):
        d = dists[i]
        if d > error:
            error = d
            index = i
    return error, index

def calc_error_list(points, params, p0, p1, p2, p3):
    dists = map(abs, distances(points, params, p0, p1, p2, p3))
    dists = map(None, dists, range(len(dists)))
    dists.sort()
    dists.reverse()
    return dists

def reestimate_params(points, params, p0, p1, p2, p3):
    return _sketch.RefineParameters(points, params, p0, p1, p2, p3)


max_iter = 5
min_iter = 3

def fit_single_bezier_segment(points, t1, t2, epsilon):
    # Return the tuple (p1, p2, errors) of control points and errors.
    #
    # p1 and p2 are the second and third of the four control points of
    # the bezier segment that fits the points (the other two are
    # points[0] and points[-1]).
    #
    # errors is a list of (error, index) pairs where error is the
    # distance between points[index] and the corresponding point on the
    # estimated. The list is sorted by error in decreasing order.
    p0 = points[0];
    p3 = points[-1];
    # repeat fit and reestimate paramters several times
    last_errors = [(1e100, 0)]; last_p1 = last_p2 = None; last_index = None
    for iter in range(max_iter):
        if iter == 0:
            # on the first iteration estimate the parameters from the arc
            # lengths
            params = estimate_params(points)
        else:
            # on all other iterations refine the previously estimated
            # parameters
            params = reestimate_params(points, params, p0, p1, p2, p3)
            if not params:
                # refining the parameters made the fit worse. Give up
                # and return to caller to retry with a subdivided curve.
                return p1, p2, last_errors
        a1, a2 = fit_bezier_with_tangents(points, params, t1, t2)
        if a1 < 0:
            a1 = 0
        if a2 < 0:
            a2 = 0
        p1 = p0 + t1 * a1
        p2 = p3 + t2 * a2
        errors = calc_error_list(points, params, p0, p1, p2, p3)
        error, index = errors[0]
        if iter > min_iter:
            if error < epsilon:
                # success! The fit is accurate enough.
                return p1, p2, errors
            if error > last_errors[0][0]:
                # the error get worse. Return to caller with the previous
                # estimate to try again.
                return last_p1, last_p2, last_errors
        last_errors = errors
        last_p1 = p1; last_p2 = p2

    # max_iter iterations didn't produce a satisfactory result
    return p1, p2, errors

    
def fit_curve(curve, points, t1, t2, epsilon, smoothend = 0):
    p1, p2, errors = fit_single_bezier_segment(points, t1, t2, epsilon)
    if errors[0][0] > epsilon:
        # error too large. subdivide if enough points left to fit a
        # bezier to
        if len(points) >= 7:
            for error, index in errors:
                if index >= 3 and index <= len(points) - 4:
                    break
            else:
                index = (len(points) + 1) / 2
            t2a, t1a = tangents_at(points, index)
            fit_curve(curve, points[:index + 1], t1, t2a, epsilon, 1)
            fit_curve(curve, points[index:], t1a, t2, epsilon, smoothend)
        else:
            for p in points[1:]:
                curve.AppendLine(p)
    else:
        curve.AppendBezier(p1, p2, points[-1],
                           smoothend and ContSmooth or ContAngle)





def bernstein0(t):
    return (1 - t)**3

def bernstein1(t):
    return 3 * t * (1 - t)**2

def bernstein2(t):
    return 3 * t**2 * (1 - t)

def bernstein3(t):
    return t**3

def fit_bezier2(points, params):
    b0 = map(bernstein0, params)
    b1 = map(bernstein1, params)
    b2 = map(bernstein2, params)
    b3 = map(bernstein3, params)
    c11 = reduce(operator.add, map(operator.mul, b1, b1))
    c22 = reduce(operator.add, map(operator.mul, b2, b2))
    c12 = reduce(operator.add, map(operator.mul, b1, b2))
    c21 = c12
    trafo = Trafo(c11, c21, c12, c22, 0, 0)
    dists = distances(points, params,
                      points[0], NullPoint, NullPoint, points[-1])

    zeros = [0] * len(points)
    X1 = reduce(operator.add, map(operator.mul, dists, map(Point, b1, zeros)))
    X2 = reduce(operator.add, map(operator.mul, dists, map(Point, b2, zeros)))
    try:
        inv = trafo.inverse()
        x1, x2 = inv(X1, X2)
    except SingularMatrix:
        x1 = 0; x2 = 0

    X1 = reduce(operator.add, map(operator.mul, dists, map(Point, zeros, b1)))
    X2 = reduce(operator.add, map(operator.mul, dists, map(Point, zeros, b2)))
    try:
        inv = trafo.inverse()
        y1, y2 = inv(X1, X2)
    except SingularMatrix:
        y1 = 0; y2 = 0

    return Point(x1, y1), Point(x2, y2)


def fit_single_bezier_segment2(points, epsilon):
    # Return the tuple (p1, p2, errors) of control points and errors.
    #
    # p1 and p2 are the second and third of the four control points of
    # the bezier segment that fits the points (the other two are
    # points[0] and points[-1]).
    #
    # errors is a list of (error, index) pairs where error is the
    # distance between points[index] and the corresponding point on the
    # estimated. The list is sorted by error in decreasing order.
    p0 = points[0];
    p3 = points[-1];
    # repeat fit and reestimate paramters several times
    last_errors = [(1e100, 0)]; last_p1 = last_p2 = None; last_index = None
    for iter in range(max_iter):
        if iter == 0:
            # on the first iteration estimate the parameters from the arc
            # lengths
            params = estimate_params(points)
        else:
            # on all other iterations refine the previously estimated
            # parameters
            params = reestimate_params(points, params, p0, p1, p2, p3)
            if not params:
                # refining the parameters made the fit worse. Give up
                # and return to caller to retry with a subdivided curve.
                return p1, p2, last_errors
        p1, p2 = fit_bezier2(points, params)
        errors = calc_error_list(points, params, p0, p1, p2, p3)
        error, index = errors[0]
        if iter > min_iter:
            if error < epsilon:
                # success! The fit is accurate enough.
                return p1, p2, errors
            if error > last_errors[0][0]:
                # the error get worse. Return to caller with the previous
                # estimate to try again.
                return last_p1, last_p2, last_errors
        last_errors = errors
        last_p1 = p1; last_p2 = p2

    # max_iter iterations didn't produce a satisfactory result
    return p1, p2, errors
        
def fit_segment(curve, points, epsilon):
    if len(points) < 4:
        for p in points[1:]:
            curve.AppendLine(p)
    else:
        #if len(points) > 5:
        #    p1, p2, errors = fit_single_bezier_segment2(points, epsilon)
        #    if errors[0][0] < epsilon:
        #        curve.AppendBezier(p1, p2, points[-1])
        #        return
        t1, t2 = tangents(points)
        fit_curve(curve, points, t1, t2, epsilon)
