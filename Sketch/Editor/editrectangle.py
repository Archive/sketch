# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

# Rectangle:
#
# The Rectangle is actually better described as a Parallelogram. When
# created interactively, instances of this class are rectangles with
# edges parallel to the axes of the coordinate system. The user can then
# rotate and shear it interactively so that it may become a
# (nonrectangular) Parallelogram.
#

from math import hypot

import Sketch
from Sketch import Trafo
from Sketch.Editor import Editor

import handle, const


class RectangleEditor(Editor):

    EditedClass = Sketch.Graphics.Rectangle

    selection = None

    def ButtonDown(self, p, button, state):
        start = self.selection.p
	Editor.DragStart(self, start)
	return p - start

    def ButtonUp(self, p, button, state):
        Editor.DragStop(self, p)
        trafo, radius1, radius2 = self.resize()
        self.selection = None
        return self.object.SetTrafoAndRadii(trafo, radius1, radius2)

    def resize(self):
        code = self.selection.code
        trafo = self.trafo; radius1 = self.radius1; radius2 = self.radius2
        if code < 0:
            # a special handle that has to be treated as a normal handle
            # depending on the direction of the drag
            width = hypot(trafo.m11, trafo.m21)
            height = hypot(trafo.m12, trafo.m22)
            t = Trafo(trafo.m11 / width, trafo.m21 / width,
                      trafo.m12 / height, trafo.m22 / height, 0, 0)
            dx, dy = t.inverse()(self.off)
            if code > -5:
                # one of the corners in a rectangle with sharp corners
                if abs(dx) > abs(dy):
                    code = 4 - code
                else:
                    code = (12, 10, 11, 9)[code]
            else:
                # the edge handle and the round corner handles coincide
                if code >= -7:
                    # horizontal edges
                    if abs(dx) > abs(dy):
                        if dx < 0:
                            code = -code
                        else:
                            code = -code + 1
                    else:
                        code = -4 - code
                else:
                    # vertical edges
                    if abs(dx) > abs(dy):
                        code = code + 13
                    else:
                        if dy < 0:
                            code = -code
                        else:
                            code = -code + 1
        #
        # code is now a normal handle
        #
        x, y = trafo.inverse()(self.drag_cur)
        width = hypot(trafo.m11, trafo.m21)
        height = hypot(trafo.m12, trafo.m22)
        if code <= 4:
            # drag one of the edges
            if code == 1:
                t = Trafo(1, 0, 0, 1 - y, 0, y)
                if y != 1:
                    radius2 = radius2 / abs(1 - y)
                else:
                    radius1 = radius2 = 0
            elif code == 2:
                t = Trafo(x, 0, 0, 1, 0, 0)
                if x != 0:
                    radius1 = radius1 / abs(x)
                else:
                    radius1 = radius2 = 0
            elif code == 3:
                t = Trafo(1, 0, 0, y, 0, 0)
                if y != 0:
                    radius2 = radius2 / abs(y)
                else:
                    radius1 = radius2 = 0
            elif code == 4:
                t = Trafo(1 - x, 0, 0, 1, x, 0)
                if x != 1:
                    radius1 = radius1 / abs(1 - x)
                else:
                    radius1 = radius2 = 0
            trafo = trafo(t)
            if radius1 != 0 or radius2 != 0:
                ratio = radius1 / radius2
                if radius1 > 0.5:
                    radius1 = 0.5
                    radius2 = radius1 / ratio
                if radius2 > 0.5:
                    radius2 = 0.5
                    radius1 = radius2 * ratio
        else:
            # modify the round corners
            if radius1 == radius2 == 0:
                ratio = height / width
            else:
                ratio = radius1 / radius2
            
            if ratio > 1:
                max1 = 0.5
                max2 = max1 / ratio
            else:
                max2 = 0.5
                max1 = max2 * ratio
            if code < 9:
                if code == 6 or code == 8:
                    x = 1 - x
                radius1 = max(min(x, max1), 0)
                radius2 = radius1 / ratio
            else:
                if code == 10 or code == 12:
                    y = 1 - y
                radius2 = max(min(y, max2), 0)
                radius1 = radius2 * ratio
        return trafo, radius1, radius2
    
    def DrawDragged(self, device, partially):
        trafo, radius1, radius2 = self.resize()
        device.RoundedRectangle(trafo, radius1, radius2)

    def GetHandles(self):
	trafo = self.trafo; radius1 = self.radius1; radius2 = self.radius2
        handles = []

        if radius1 == radius2 == 0:
            for x, y, code in ((0, 0, -1), (1, 0, -2), (0, 1, -3), (1, 1, -4),
                               (0.5,   0, 1), (1.0, 0.5, 2),
                               (0.5, 1.0, 3), (  0, 0.5, 4)):
                handles.append(handle.MakeNodeHandle(trafo(x, y), code = code))
        else:
            # horizontal edges
            if round(radius1, 3) >= 0.5:
                handles.append(handle.MakeNodeHandle(trafo(0.5, 0), code = -5))
                handles.append(handle.MakeNodeHandle(trafo(0.5, 1), code = -7))
            else:
                coords = ((radius1, 0, 5), (0.5, 0, 1), (1 - radius1, 0, 6),
                          (radius1, 1, 7), (0.5, 1, 3), (1 - radius1, 1, 8))
                for x, y, code in coords:
                    handles.append(handle.MakeNodeHandle(trafo(x, y),
                                                         code = code))

            # vertical edges
            if round(radius2, 3) >= 0.5:
                handles.append(handle.MakeNodeHandle(trafo(0, 0.5), code = -9))
                handles.append(handle.MakeNodeHandle(trafo(1, 0.5), code =-11))
            else:
                coords = ((0, radius2, 9), (0, 0.5, 4), (0, 1 - radius2, 10),
                          (1, radius2, 11),(1, 0.5, 2), (1, 1 - radius2, 12))
                for x, y, code in coords:
                    handles.append(handle.MakeNodeHandle(trafo(x, y),
                                                         code = code))
        #
        return handles

    def SelectHandle(self, handle, mode):
	self.selection = handle
        return const.SelectSingle

    def SelectPoint(self, p, rect, device, mode):
	return const.SelectNone

    def HasSelection(self):
        return self.selection is not None

    def SelectNone(self):
        self.selection = None
