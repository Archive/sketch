# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch
from Sketch import _, Trafo
from Sketch.Editor import RectangularCreator, Creator
from Sketch.Graphics import Ellipse

class EllipseCreator(RectangularCreator):

    creation_text = _("Create Ellipse")

    def compute_trafo(self, state):
        start = self.drag_start
        end = self.drag_cur
        if state & Sketch.Editor.AlternateMask:
            # start is the center of the ellipse
            if state & Sketch.Editor.ConstraintMask:
                # end is a point of the periphery of a *circle* centered
                # at start
                radius = abs(start - end)
                self.trafo = Trafo(radius, 0, 0, radius, start.x, start.y)
            else:
                # end is a corner of the bounding box
                d = end - start
                self.trafo = Trafo(d.x, 0, 0, d.y, start.x, start.y)
        else:
            # the ellipse is inscribed into the rectangle with start and
            # end as opposite corners. 
            end = self.apply_constraint(self.drag_cur, state)
            d = (end - start) / 2
            self.trafo = Trafo(d.x, 0, 0, d.y, start.x + d.x, start.y + d.y)
            
    def MouseMove(self, p, state):
        # Bypass RectangularCreator
        Creator.MouseMove(self, p, state)
        self.compute_trafo(state)

    def ButtonUp(self, p, button, state):
        Creator.DragStop(self, p)
        self.compute_trafo(state)

    def DrawDragged(self, device, partially):
	device.DrawEllipse(self.trafo(-1, -1), self.trafo(1, 1))

    def CurrentInfoText(self):
        t = self.trafo
        data = {}
        if abs(round(t.m11, 2)) == abs(round(t.m22, 2)):
            text = _("Circle %(radius)[length], center %(center)[position]")
            data['radius'] = t.m11
        else:
            text = _("Ellipse %(size)[size], center %(center)[position]")
            data['size'] = (abs(t.m11), abs(t.m22))
        data['center'] = t.offset()
        return text, data

    def CreatedObject(self):
	return Ellipse(self.trafo)
