# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

from types import DictType, StringType, TupleType
from string import split, lower
import operator

from Sketch import _
from Sketch.Base.warn import warn, USER
from Sketch.Base import Publisher
from Sketch.Base.const import CHANGED

from command import SafeScript
import const


class ScriptRegistry:

    def __init__(self):
        self.registry = {}
        self.menu = {}

    def Add(self, script):
        self.registry[script.name] = script

    def AddFunction(self, name, title, function, args = (), channels = (),
                    sensitive = None, script_type = SafeScript):
        self.Add(script_type(name, title, function, args = args,
                             channels = channels, sensitive = sensitive))

    def Command(self, name):
        return self.registry.get(name, None)


class Menu(Publisher):

    def __init__(self, title, translatable = 0):
        self.title = title
        self.translatable = translatable
        self.items = []

    def Title(self):
        if self.translatable:
            return _(self.title)
        else:
            return self.title

    def Items(self):
        return self.items

    def Submenu(self, title):
        for item in self.items:
            # access title directly to avoid translation:
            if isinstance(item, Menu) and item.title == title: 
                return item
        return None

    def changed(self):
        self.issue(CHANGED)

    def AppendItem(self, item):
        self.items.append(item)
        self.changed()

    def AddItem(self, path, item):
        menu = self
        if path:
            if isinstance(path, StringType):
                path = (path,)
            for title in path:
                submenu = menu.Submenu(title)
                if submenu is None:
                    submenu = Menu(title)
                    menu.AppendItem(submenu)
                menu = submenu
        menu.AppendItem(item)

def BuildMenuTree(title, tree, translatable = 0):
    menu = Menu(title, translatable = translatable)
    for item in tree:
        if isinstance(item, TupleType):
            if isinstance(item[-1], StringType):
                item = item[-1]
            else:
                item = BuildMenuTree(item[0], item[-1], translatable = 1)
        menu.AppendItem(item)
    return menu


modifier_map = {'S': const.ShiftMask,
                'C': const.ControlMask,
                'M': const.MetaMask}
    
class Keymap:

    def __init__(self, map = None, special_keys = None):
        self.map = {}
        self.inverse = {}
        if special_keys is None:
            self.special_keys = {}
        else:
            self.special_keys = special_keys
        if map is not None:
            self.add_map(map)

    def add_map(self, map):
        for command, key in map:
            key = self.parse_key(key)
            #print command, key
            if key is not None:
                self.map[key] = command
                self.inverse[command] = key

    def AddItem(self, command, key):
        #print 'AddItem', command, key
        self.map[key] = command
        self.inverse[command] = key

    def RemoveItem(self, key):
        #print 'RemoveItem', key
        cmd = self.map.get(key)
        #print 'cmd', cmd
        if cmd is not None:
            del self.map[key]
            del self.inverse[cmd]
        # XXX should an exception be raised if the item is not defined?

    def Keystroke(self, command):
        return self.inverse.get(command)

    def Command(self, key):
        #print 'Command', key
        return self.map.get(key)

    def parse_key(self, stroke):
        if type(stroke) is TupleType:
            return stroke
        if stroke[-1] != '-':
            parts = split(stroke, '-')
            modifiers = parts[:-1]
            key = parts[-1]
        else:
            modifiers = split(stroke[:-2], '-')
            key = '-'
        flags = 0
        for modifier in modifiers:
            flags = flags | modifier_map.get(modifier)
        if len(key) > 1:
            key = self.special_keys.get(key)
            if key is None:
                warn(USER, "Cannot parse keystroke `%s' correctly", stroke)
                return None
        else:
            key = ord(lower(key))
        return key, flags
