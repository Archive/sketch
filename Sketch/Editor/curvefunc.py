# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA



from Sketch import CreatePath

from Sketch._sketch import ContAngle, ContSmooth, ContSymmetrical, Bezier, Line


def adjust_control_point(p, node, control, continuity):
    if continuity == ContSymmetrical:
	return 2 * node - control
    elif continuity == ContSmooth:
	try:
	    d = (control - node).normalized()
	    length = abs(p - node)
	    return node - length * d
	except ZeroDivisionError:
	    # control == node
	    return p
    else:
	return p

def subdivide(p0, p1, p2, p3, t = 0.5):
    t2 = 1 - t
    r = t2 * p1 + t * p2
    q1 = t2 * p0 + t * p1
    q2 = t2 * q1 + t * r
    q5 = t2 * p2 + t * p3
    q4 = t2 * r + t * q5
    q3 = t2 * q2 + t * q4
    return q1, q2, q3, q4, q5


def delete_segments(path):
    newpath = CreatePath()
    selected = path.selection_count()
    if (path.closed and selected == path.len - 1) or selected == path.len:
	return newpath
    f13 = 1.0 / 3.0;	f23 = 2.0 / 3.0
    i = 0
    while path.SegmentSelected(i):
	i = i + 1
    if path.closed and i > 0:
	if path.SegmentType(i) == Bezier:
	    last_p2 = path.Segment(i)[1][1]
	    last_type = Bezier
	else:
	    last_p2 = f23 * path.Node(i - 1) + f13 * path.Node(i)
	    last_type = Line
    else:
	last_p2 = None
    newpath.AppendLine(path.Node(i), path.Continuity(i))

    seg_p1 = None; seg_type = None
    for i in range(i + 1, path.len):
	type, p12, p, cont = path.Segment(i)
	if type == Bezier:
	    p1, p2 = p12
	if path.SegmentSelected(i):
	    if seg_type is None:
		seg_type = type
		if type == Bezier:
		    seg_p1 = p1
		else:
		    seg_p1 = f23 * path.Node(i - 1) + f13 * p
	else:
	    if seg_type is not None:
		if type == Bezier or seg_type == Bezier:
		    if type == Line:
			p2 = f13 * path.Node(i - 1) + f23 * p
		    newpath.AppendBezier(seg_p1, p2, p, cont)
		else:
		    newpath.AppendLine(p, cont)
		seg_type = None
	    else:
		newpath.AppendSegment(type, p12, p, cont)
    if path.closed:
	if last_p2 is not None:
	    if last_type == Bezier or seg_type == Bezier:
		newpath.AppendBezier(seg_p1, last_p2, newpath.Node(0),
				     newpath.Continuity(0))
	    else:
		newpath.AppendLine(newpath.Node(0), newpath.Continuity(0))
	newpath.ClosePath()
    return newpath

def insert_segments(path):
    newpath = CreatePath()
    newpath.AppendLine(path.Node(0), path.Continuity(0))
    newpath.select_segment(0, path.SegmentSelected(0))

    for i in range(1, path.len):
	type, p12, p, cont = path.Segment(i)
	if path.SegmentSelected(i) and path.SegmentSelected(i - 1):
	    if type == Line:
		node = 0.5 * path.Node(i - 1) + 0.5 * path.Node(i)
		newpath.AppendLine(node)
		newpath.select_segment(-1)
		newpath.AppendLine(path.Node(i))
		newpath.select_segment(-1)
	    else:
                if newpath.Continuity(-1) == ContSymmetrical:
                    newpath.SetContinuity(-1, ContSmooth)
                p1, p2 = p12
		p1, p2, node, p3, p4 = subdivide(path.Node(i - 1), p1, p2, p)
		newpath.AppendBezier(p1, p2, node, ContSymmetrical)
		newpath.select_segment(-1)
                if cont == ContSymmetrical:
                    cont = ContSmooth
		newpath.AppendBezier(p3, p4, p, cont)
		newpath.select_segment(-1)
	else:
	    newpath.AppendSegment(type, p12, p, cont)
	    newpath.select_segment(-1, path.SegmentSelected(i))
    if path.closed:
	newpath.ClosePath()
	newpath.SetContinuity(-1, path.Continuity(-1))
    return newpath

def copy_selection(path, newpath):
    for i in range(path.len):
	newpath.select_segment(i, path.SegmentSelected(i))

def segments_to_lines(path):
    newpath = CreatePath()
    newpath.AppendLine(path.Node(0))
    for i in range(1, path.len):
	if path.SegmentSelected(i) and path.SegmentSelected(i - 1):
	    if path.SegmentType(i) == Bezier:
		cont = ContAngle
		newpath.SetContinuity(-1, ContAngle)
	    else:
		cont = path.Continuity(i)
	    newpath.AppendLine(path.Node(i), cont)
	else:
	    apply(newpath.AppendSegment, path.Segment(i))
    if path.closed:
	cont = newpath.Continuity(-1)
	newpath.ClosePath()
	newpath.SetContinuity(-1, cont)
    copy_selection(path, newpath)
    return newpath

def segments_to_beziers(path):
    f13 = 1.0 / 3.0;	f23 = 2.0 / 3.0
    newpath = CreatePath()
    newpath.AppendLine(path.Node(0))
    for i in range(1, path.len):
	type, p12, p, cont = path.Segment(i)
	if path.SegmentSelected(i) and path.SegmentSelected(i - 1):
	    cont = path.Continuity(i)
	    if type == Line:
		node1 = path.Node(i - 1); node2 = path.Node(i)
		p1 = f23 * node1 + f13 * node2
		p2 = f13 * node1 + f23 * node2
		cont = ContAngle
	    else:
		p1, p2 = p12
	    newpath.AppendBezier(p1, p2, p, cont)
	else:
	    newpath.AppendSegment(type, p12, p, cont)
    if path.closed:
	cont = newpath.Continuity(-1)
	newpath.ClosePath()
	newpath.SetContinuity(-1, cont)
    copy_selection(path, newpath)
    return newpath

def set_continuity(path, cont):
    f13 = 1.0 / 3.0;	f23 = 2.0 / 3.0
    newpath = path.Duplicate()
    for i in range(1, path.len):
	if path.SegmentSelected(i):
	    newpath.SetContinuity(i, cont)
	    if cont == ContAngle:
		continue
	    if newpath.SegmentType(i) != Bezier:
		continue
	    if i == path.len - 1:
		if newpath.closed:
		    other = 1
		else:
		    continue
	    else:
		other = i + 1
	    if newpath.SegmentType(other) != Bezier:
		continue
	    type, (p1, p2), node, oldcont = newpath.Segment(i)
	    type, (p3, p4), other_node, other_cont = newpath.Segment(other)

	    d = p3 - p2
	    if cont == ContSymmetrical:
		d = 0.5 * d
	    p2 = adjust_control_point(p2, node, node + d, cont)
	    p3 = adjust_control_point(p3, node, node - d, cont)
	    newpath.SetBezier(i, p1, p2, node, cont)
	    newpath.SetBezier(other, p3, p4, other_node, other_cont)
    return newpath


def copy_path(dest, src, start = 0, end = -1, copy_selection = 1):
    if start < 0:
        start = src.len + start
    if end < 0:
        end = src.len + end
    for i in range(start, end + 1):
        type, control, node, cont = src.Segment(i)
        dest.AppendSegment(type, control, node, cont)
        if copy_selection:
            dest.select_segment(-1, src.SegmentSelected(i))


def insert_node_at(path, at):
    index = int(at)
    t = at - index
    newpath = CreatePath()
    copy_path(newpath, path, 0, index)
    type, control, node, cont = path.Segment(index + 1)
    if type == Line:
        newpath.AppendLine((1 - t) * path.Node(index) + t * node)
        newpath.select_segment(-1)
        newpath.AppendLine(node)
    else:
        if newpath.Continuity(-1) == ContSymmetrical:
            newpath.SetContinuity(-1, ContSmooth)
        p1, p2 = control
        p1, p2, q, p3, p4 = subdivide(newpath.Node(-1), p1, p2, node, t)
        newpath.AppendBezier(p1, p2, q, ContSmooth)
        newpath.select_segment(-1)
        if cont == ContSymmetrical:
            cont = ContSmooth
        newpath.AppendBezier(p3, p4, node, cont)
    copy_path(newpath, path, index + 2)
    if path.closed:
        newpath.ClosePath()
        newpath.SetContinuity(-1, path.Continuity(-1))
    return newpath

def split_path_at(path, at):
    index = int(at)
    t = at - index
    if path.closed:
        path1 = path2 = CreatePath()
        result = [path1]
    else:
        path1 = CreatePath()
        path2 = CreatePath()
        result = [path1, path2]
        copy_path(path1, path, 0, 0, copy_selection = 0)

    type, control, node, cont = path.Segment(index + 1)
    if type == Line:
        q = (1 - t) * path.Node(index) + t * node
        path2.AppendLine(q)
        path2.AppendLine(node)
        path2.select_segment(0)
        function = path1.AppendLine
        args = (q,)
    else:
        p1, p2 = control
        p1, p2, q, p3, p4 = subdivide(path.Node(index), p1, p2, node, t)
        path2.AppendLine(q)
        path2.AppendBezier(p3, p4, node, cont)
        path2.select_segment(0)
        function = path1.AppendBezier
        args = (p1, p2, q, ContSymmetrical)
    copy_path(path2, path, index + 2, copy_selection = 0)
    copy_path(path1, path, 1, index, copy_selection = 0)
    apply(function, args)
    return result
    
def segment_to_line(path, at):
    index = int(at)
    if path.SegmentType(index + 1) == Bezier:
        newpath = CreatePath()
        copy_path(newpath, path, 0, index)
        newpath.SetContinuity(-1, ContAngle)
        newpath.AppendLine(path.Node(index + 1), ContAngle)
        copy_path(newpath, path, index + 2)
        if path.closed:
            cont = newpath.Continuity(-1)
            newpath.ClosePath()
            newpath.SetContinuity(-1, cont)
    else:
        newpath = path
    return newpath

def segment_to_curve(path, at):
    index = int(at)
    if path.SegmentType(index + 1) == Line:
        newpath = CreatePath()
        copy_path(newpath, path, 0, index)
        f13 = 1.0 / 3.0;
	f23 = 2.0 / 3.0
        node1 = path.Node(index);
        node2 = path.Node(index + 1)
        p1 = f23 * node1 + f13 * node2
        p2 = f13 * node1 + f23 * node2
        newpath.AppendBezier(p1, p2, node2, path.Continuity(index + 1))
        copy_path(newpath, path, index + 2)
        if path.closed:
            cont = newpath.Continuity(-1)
            newpath.ClosePath()
            newpath.SetContinuity(-1, cont)
    else:
        newpath = path
    return newpath
    


def CombineBeziers(beziers):
    combined = beziers[0].Duplicate()
    paths = combined.paths
    for bezier in beziers[1:]:
	paths = paths + bezier.paths
    combined.paths = paths
    return combined


