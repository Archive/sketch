# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2003, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# Classes for handling selections. These include classes that represent
# lists of selected objects and classes that represent the combined
# bounding rectangle of all selected objects. The user can interact in
# the usual fashion with these selection rects to transform (translate,
# rotate, scale, shear) the selected objects.


import math

import Sketch
from Sketch import _, Point, Polar, Identity, Trafo, TrafoType, Rotation
from Sketch.Base.warn import pdebug
from Sketch.Base.const import SelectSet, SelectDrag, SELECTION, EDITED

from drawer import SelectAndDrag
from tools import ToolInstance, ToolInfo, selection_type
from selrect import SelRectBase, SelectionRectangle

class SizeRectangle(SelectionRectangle):

    def init_constraint(self):
        sel = self.selection
        if sel == 1:
            self.reference = tuple(self.end)
        elif sel == 3:
            self.reference = (self.start.x, self.end.y)
        elif sel == 5:
            self.reference = tuple(self.start)
        elif sel == 7:
            self.reference = (self.end.x, self.start.y)
        else:
            return
        width = abs(self.start.x - self.end.x)
        height = abs(self.start.y - self.end.y)
        if width >= 1e-10:
            self.aspect = height / width
        else:
            self.aspect = None

    def apply_constraint(self, p, state):
        if state & Sketch.Editor.ConstraintMask:
            if self.selection in self.selAspect:
                ref_x, ref_y = self.reference
                aspect = self.aspect
                if aspect is None:
                    # width is 0
                    p = Point(self.drag_start.x, p.y)
                else:
                    w = p.x - ref_x
                    h = p.y - ref_y
                    if w == 0:
                        w = 0.00001
                    a = h / w
                    if a > 0:
                        sign = 1
                    else:
                        sign = -1
                    if abs(a) > aspect:
                        h = sign * w * aspect
                    else:
                        w = sign * h / aspect
                    p = Point(ref_x + w, ref_y + h)
            elif self.selection == -1:
                pi4 = math.pi / 4
                off = p - self.drag_start
                d = Polar(pi4 * round(math.atan2(off.y, off.x) / pi4))
                p = self.drag_start + (off * d) * d
        return p

    def ButtonDown(self, p, button, state):
        self.trafo = Identity
        return SelectionRectangle.ButtonDown(self, p, button, state)

    def MouseMove(self, p, state):
        p = self.apply_constraint(p, state)
        SelectAndDrag.MouseMove(self, p, state)
        start, end = self.compute_endpoints()
        self.ComputeTrafo(self.start, self.end, start, end)

    def ComputeTrafo(self, oldStart, oldEnd, start, end):
        oldDelta = oldEnd - oldStart
        delta    = end - start
        if self.selection == -1:
            # a translation.
            self.text = _("Move Objects")
            self.trafo = start - oldStart
        else:
            try:
                m11 = delta.x / oldDelta.x
            except ZeroDivisionError:
                m11 = 0
                if __debug__:
                    pdebug(None, 'ComputeTrafo: ZeroDivisionError')
            try:
                m22 = delta.y / oldDelta.y
            except ZeroDivisionError:
                m22 = 0
                if __debug__:
                    pdebug(None, 'ComputeTrafo: ZeroDivisionError')
            offx = start.x - m11 * oldStart.x
            offy = start.y - m22 * oldStart.y
            self.text = _("Resize Objects")
            self.trafo = Trafo(m11, 0, 0, m22, offx, offy)

    def DrawDragged(self, device, partial):
        SelectionRectangle.DrawDragged(self, device, partial)
        if self.outline_object is not None:
            trafo = self.trafo
            device.PushTrafo()
            if type(trafo) == TrafoType:
                device.Concat(trafo)
            else:
                device.Translate(trafo.x, trafo.y)
            self.outline_object.DrawShape(device)
            device.PopTrafo()

    def CurrentInfoText(self):
        t = self.trafo
        data = {}
        if type(t) == TrafoType:
            x = t.m11
            y = t.m22
            #if round(x, 3) == round(y, 3):
            #    text = _("Uniform Scale %(factor)[factor]")
            #    data['factor'] = x
            #else:
            text = _("Scale %(factorx)[factor], %(factory)[factor]")
            data['factorx'] = x
            data['factory'] = y
        else:
            text = _("Move %(x)[length], %(y)[length]")
            data['x'] = t.x
            data['y'] = t.y
        return text, data



class TrafoRectangle(SelRectBase):

    selTurn = [1, 3, 5, 7]
    selShear = [2, 4, 6, 8]
    selCenter = 100

    def __init__(self, rect, center = None):
        SelRectBase.__init__(self)
        self.start = Point(rect.left, rect.bottom)
        self.end = Point(rect.right, rect.top)
        if center is None:
            self.center = rect.center()
        else:
            self.center = center

    def compute_trafo(self, state = 0):
        sel = self.selection
        if sel in self.selTurn:
            # rotation
            vec = self.drag_cur - self.center
            angle = math.atan2(vec.y, vec.x)
            angle = angle - self.start_angle + 2 * math.pi
            if state & Sketch.Editor.ConstraintMask:
                pi12 = math.pi / 12
                angle = pi12 * int(angle / pi12 + 0.5)
            self.trafo = Rotation(angle, self.center)
            self.trafo_desc = (1, angle)
        elif sel in self.selShear:
            if sel in (2,6):
                # horiz. shear
                height = self.drag_start.y - self.reference
                if height:
                    ratio = self.off.x / height
                    self.trafo = Trafo(1, 0, ratio, 1,
                                       - ratio * self.reference, 0)
                    self.trafo_desc = (2, ratio)
            else:
                # vert. shear
                width = self.drag_start.x - self.reference
                if width:
                    ratio = self.off.y / width
                    self.trafo = Trafo(1, ratio, 0, 1, 0,
                                       - ratio * self.reference)
                    self.trafo_desc = (3, ratio)

    def DrawDragged(self, device, partially):
        sel = self.selection
        if sel == self.selCenter:
            device.DrawPixmapHandle(self.drag_cur, "Center")
        else:
            trafo = self.trafo
            if trafo:
                device.PushTrafo()
                device.Concat(trafo)
                device.DrawRubberRect(self.start, self.end)
                if self.outline_object is not None:
                    self.outline_object.DrawShape(device)
                device.PopTrafo()

    def ButtonDown(self, p, button, state):
        self.drag_state = state
        self.trafo = Identity
        self.trafo_desc = (0, 0)
        SelectAndDrag.DragStart(self, p)
        sel = self.selection
        if sel == self.selCenter:
            self.drag_cur = self.drag_start = self.center
            return p - self.center
        ds_x = ds_y = 0
        if sel in self.selLeft:
            ds_x = self.start.x
        if sel in self.selTop:
            ds_y = self.start.y
        if sel in self.selRight:
            ds_x = self.end.x
        if sel in self.selBottom:
            ds_y = self.end.y
        self.drag_cur = self.drag_start = ds = Point(ds_x, ds_y)
        if sel in self.selTurn:
            vec = ds - self.center
            self.start_angle = math.atan2(vec.y, vec.x)
        else:
            if sel == 2:
                self.reference = self.end.y
            elif sel == 4:
                self.reference = self.start.x
            elif sel == 6:
                self.reference = self.start.y
            elif sel == 8:
                self.reference = self.end.x
        return p - ds

    def constrain_center(self, p, state):
        if state & Sketch.Editor.ConstraintMask:
            start = self.start
            end = self.end
            if p.x < 0.75 * start.x + 0.25 * end.x:
                x = start.x
            elif p.x > 0.25 * start.x + 0.75 * end.x:
                x = end.x
            else:
                x = (start.x + end.x) / 2
            if p.y < 0.75 * start.y + 0.25 * end.y:
                y = start.y
            elif p.y > 0.25 * start.y + 0.75 * end.y:
                y = end.y
            else:
                y = (start.y + end.y) / 2
            return Point(x, y)
        return p

    def MouseMove(self, p, state):
        self.drag_state = state
        if self.selection == self.selCenter:
            p = self.constrain_center(p, state)
        SelectAndDrag.MouseMove(self, p, state)
        self.compute_trafo(state)

    def ButtonUp(self, p, button, state):
        if self.selection == self.selCenter:
            p = self.constrain_center(p, state)
        SelectAndDrag.DragStop(self, p)
        sel = self.selection
        if sel == self.selCenter:
            self.center = self.drag_cur
            self.text = ''
            self.trafo = None
        else:
            self.compute_trafo(state)
            trafo = self.trafo
            if self.selection in self.selShear:
                self.text = _("Shear Objects")
            else:
                self.text = _("Rotate Objects")

    def CurrentInfoText(self):
        if self.selection == self.selCenter:
            text = _("Rotation Center at %(position)[position]")
            data = {'position': self.drag_cur}
        else:
            type, value = self.trafo_desc
            if type == 1:
                text = _("Rotate by %(angle)[angle]")
                data = {'angle': value}
            elif type == 2:
                text = _("Horizontal Shear by %(ratio)[factor]")
                data = {'ratio': value}
            elif type == 3:
                text = _("Vertical Shear by %(ratio)[factor]")
                data = {'ratio': value}
            else:
                text = _("Identity Transform")
                data = {}
        return text, data

    def Hit(self, p, rect, device):
        pass

    def Select(self):
        pass

    def SelectPoint(self, p, rect, device, mode = SelectSet):
        self.selection = 0
        return self.selection

    def SelectHandle(self, handle, mode = SelectSet):
        self.selection = handle.code

    def GetHandles(self):
        sx = self.start.x
        sy = self.start.y
        ex = self.end.x
        ey = self.end.y
        x2 = (sx + ex) / 2
        y2 = (sy + ey) / 2
        h = Sketch.Editor.handle.Handle
        c = self.handle_idx_to_sel
        e = Sketch.Editor
        return [h(e.HandleTurnTL, Point(sx, ey), code = c[0]),
                h(e.HandleShearT, Point(x2, ey), code = c[1]),
                h(e.HandleTurnTR, Point(ex, ey), code = c[2]),
                h(e.HandleShearL, Point(sx, y2), code = c[3]),
                h(e.HandleCenter, self.center, code = self.selCenter),
                h(e.HandleShearR, Point(ex, y2), code = c[4]),
                h(e.HandleTurnBL, Point(sx, sy), code = c[5]),
                h(e.HandleShearB, Point(x2, sy), code = c[6]),
                h(e.HandleTurnBR, Point(ex, sy), code = c[7])]



class SelectionToolInstance(ToolInstance):

    title = ''"Select"

    def __init__(self, editor):
        ToolInstance.__init__(self, editor)
        self.rectangle = SizeRectangle(self.editor.Selection().coord_rect)
        self.center = None
        self.editor.Subscribe(SELECTION, self.selection_changed)
        self.editor.Subscribe(EDITED, self.doc_was_edited)

    def End(self):
        self.editor.Unsubscribe(SELECTION, self.selection_changed)
        self.editor.Unsubscribe(EDITED, self.doc_was_edited)
        ToolInstance.End(self)

    def _toggle(self):
        if isinstance(self.rectangle, SizeRectangle):
            self.rectangle = TrafoRectangle(self.editor.Selection().coord_rect,
                                            self.center)
        else:
            self.rectangle = SizeRectangle(self.editor.Selection().coord_rect)

    def selection_changed(self):
        coord_rect = self.editor.Selection().coord_rect
        #print 'selection_changed', self.editor.HasSelection(), coord_rect
        self.rectangle = SizeRectangle(coord_rect)
        self.center = None

    def doc_was_edited(self, *args):
        coord_rect = self.editor.Selection().coord_rect
        if coord_rect is not None:
            if isinstance(self.rectangle, TrafoRectangle):
                self.rectangle = TrafoRectangle(coord_rect, self.center)
            else:
                self.rectangle = SizeRectangle(coord_rect)

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonPress(self, context, p, snapped, button, state,
                                 handle = handle)
        test = context.test_device()
        if handle is not None:
            # The user pressed the button over a handle. Prepare to drag
            # at the handle
            self.rectangle.SelectHandle(handle, SelectDrag)
            object = self.rectangle
        elif self.editor.SelectionHit(p, test):
            # The user pressed the button over an already selected object,
            # but not on a handle.
            # Prepare to edit/transform the current selection
            if isinstance(self.rectangle, TrafoRectangle):
                self._toggle()
            self.rectangle.Select()
            object = self.rectangle
        else:
            # The user pressed the button somewhere else.
            # check, whether a guide line is hit:
            pick = self.editor.PickActiveObject(test, p)
            if pick is not None and pick.is_GuideLine:
                # edit guide line
                object = Sketch.Editor.GuideEditor(pick)
            else:
                # Select by rubberbanding
                object = SelectionRectangle(p)
        self.begin_edit_object(context, object, snapped, button, state)

    def ButtonRelease(self, context, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, context, p, snapped, button, state)
        object = self.end_edit_object(context, snapped, button, state)
        if isinstance(object, SizeRectangle) \
             or isinstance(object, TrafoRectangle):
            trafo = object.trafo
            if trafo is not None:
                text = object.text
                if type(trafo) == TrafoType:
                    self.editor.TransformSelected(trafo, undo_text = text)
                else:
                    self.editor.TranslateSelected(trafo, undo_text = text)
            else:
                # the rotation center was moved
                self.editor.update_handles()
                self.center = object.center
        elif isinstance(object, SelectionRectangle):
            self.editor.SelectRect(object.bounding_rect, selection_type(state))

    def ButtonClick(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonClick(self, context, p, snapped, button, state,
                                 handle = handle)
        type = selection_type(state)
        test = context.test_device()
        if type == SelectSet and self.editor.SelectionHit(p, test):
            self._toggle()
            self.editor.update_handles()
        else:
            self.editor.SelectPoint(p, test, type)

    def Cancel(self, canvas):
        if self.current is not None:
            ToolInstance.Cancel(self, canvas)
        else:
            self.editor.SelectNone()

    def Handles(self):
        if self.editor.HasSelection():
            handles = self.rectangle.GetHandles()
            handles.append(self.editor.Selection().GetHandles()[0])
        else:
            handles = []
        return handles


SelectionTool = ToolInfo("SelectionTool", SelectionToolInstance,
                         active_cursor = 1)
