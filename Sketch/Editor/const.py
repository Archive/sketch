# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#
#       Constants for the editor part
#


#
#	Types of handles
#

HandleNode = 'Node'
HandleSelectedNode = 'SelectedNode'

HandleCurvePoint = 'CurvePoint'
HandleControlPoint = 'ControlPoint'
HandleLine = 'Line'

HandleNodeList = 'NodeList'
#HandleSelectedNodeList = 'SelectedNodeList'
#HandleControlPointList = 'ControlPointList'


HandleCaret = 'Caret'
HandlePathText = 'PathText'

HandleTurnTL = 'TurnTL'
HandleTurnTR = 'TurnTR'
HandleTurnBL = 'TurnBL'
HandleTurnBR = 'TurnBR'

HandleShearT = 'ShearT'
HandleShearB = 'ShearB'
HandleShearL = 'ShearL'
HandleShearR = 'ShearR'

HandleCenter = 'Center'

HandleSizeTL = 'SizeTL'
HandleSizeT = 'SizeT'
HandleSizeTR = 'SizeTR'
HandleSizeL = 'SizeL'
HandleSizeR = 'SizeR'
HandleSizeBL = 'SizeBL'
HandleSizeB = 'SizeB'
HandleSizeBR = 'SizeBR'

HandleIndicatorList = 'IndicatorList'

HandleAlignTL = 'AlignTL'
HandleAlignTC = 'AlignTC'
HandleAlignT = 'AlignT'
HandleAlignTR = 'AlignTR'
HandleAlignL = 'AlignL'
HandleAlignLC = 'AlignLC'
HandleAlignR = 'AlignR'
HandleAlignBL = 'AlignBL'
HandleAlignB = 'AlignB'
HandleAlignBR = 'AlignBR'
HandleAlignC = 'AlignC'



#
# Modifier Keys
#

ShiftMask = 1
ControlMask = 4
MetaMask = 8

Button1Mask = 256
Button2Mask = 512
Button3Mask = 1024
AllButtonsMask = Button1Mask | Button2Mask | Button3Mask

Button1 = 1
Button2 = 2
Button3 = 3
Button4 = 4
Button5 = 5

ContextButton	= Button3
ContextButtonMask = Button3Mask

AllowedModifierMask = ShiftMask | ControlMask | MetaMask
ConstraintMask = ControlMask
AlternateMask = ShiftMask
AddSelectionMask = ShiftMask
SubtractSelectionMask = ControlMask
SubobjectSelectionMask = ShiftMask | ControlMask

ZoomOutMask = ControlMask

#
#
#

SelectNone = 0
SelectMulti = 1
SelectSingle = 2
