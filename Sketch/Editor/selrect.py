# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA


# Classes for handling selections. These include classes that represent
# lists of selected objects and classes that represent the combined
# bounding rectangle of all selected objects. The user can interact in
# the usual fashion with these selection rects to transform (translate,
# rotate, scale, shear) the selected objects.


import Sketch
from Sketch import Point, Rect, RectType
from Sketch.Base.const import SelectSet

from drawer import SelectAndDrag


class SelRectBase(SelectAndDrag):

    #
    # Handle/selection numbers:
    #	sx		ex
    #	1	2	3	sy
    #
    #	8		4
    #
    #	7	6	5	ey
    #
    #	-1: whole object

    selTop	= (1, 2, 3)
    selBottom	= (7, 6, 5)
    selLeft	= (1, 8, 7)
    selRight	= (3, 4, 5)
    selAspect	= (1, 3, 5, 7)	# constrain aspect ratio for these selections

    handle_idx_to_sel = (7, 6, 5, 8, 4, 1, 2, 3)

    def __init__(self):
	SelectAndDrag.__init__(self)
	self.outline_object = None

    def update_rects(self):
	self.coord_rect = Rect(self.start, self.end)
	self.bounding_rect = self.coord_rect

    def __getattr__(self, attr):
        if attr == 'coord_rect' or attr == 'bounding_rect':
            self.update_rects()
        return self.__dict__[attr]

    def SetOutlineObject(self, obj):
	# XXX: this is a hack...
	if obj is not None:
	    if obj.is_Compound:
		objects = obj.GetObjects()
		if len(objects) == 1:
		    obj = objects[0]
		else:
		    return
	    if not obj.is_Compound and not obj.is_Text:
		self.outline_object = obj


class SelectionRectangle(SelRectBase):

    def __init__(self, rect, anchor = None):
	SelRectBase.__init__(self)
	if type(rect) == RectType:
	    self.start = Point(rect.left, rect.bottom)
	    self.end = Point(rect.right, rect.top)
	    self.Normalize()
	    self.anchor = anchor
	else:
	    # assume type Point and interactive creation
	    self.start = rect
	    self.end = rect
	    self.anchor = None
	    self.selection = 5

    def DrawDragged(self, device, partially):
	sel = self.selection

	if sel == -1:
	    sx, sy = self.start + self.off
	    ex, ey = self.end + self.off
	else:
	    if sel in self.selTop:
		sy = self.drag_cur.y
	    else:
		sy = self.start.y

	    if sel in self.selBottom:
		ey = self.drag_cur.y
	    else:
		ey = self.end.y

	    if sel in self.selLeft:
		sx = self.drag_cur.x
	    else:
		sx = self.start.x

	    if sel in self.selRight:
		ex = self.drag_cur.x
	    else:
		ex = self.end.x

	if sx > ex:
	    tmp = sx; sx = ex; ex = tmp
	if sy < ey:
	    tmp = sy; sy = ey; ey = tmp

	device.DrawRubberRect(Point(sx, sy), Point(ex, ey))

    def ButtonDown(self, p, button, state):
	SelectAndDrag.DragStart(self, p)
	sel = self.selection
	if sel == -1:
	    if self.anchor: #XXX shouldn't this be 'if self.anchor is not None'
		start = self.anchor
	    else:
		start = self.start
	    self.drag_start = self.drag_cur = start
	    return (p - start, self.coord_rect.translated(-start))
	ds_x , ds_y = (self.start + self.end) / 2
	if sel in self.selLeft:
	    ds_x = self.start.x
	if sel in self.selTop:
	    ds_y = self.start.y
	if sel in self.selRight:
	    ds_x = self.end.x
	if sel in self.selBottom:
	    ds_y = self.end.y
	self.drag_cur = self.drag_start = ds = Point(ds_x, ds_y)
	self.init_constraint()
	return p - ds

    def init_constraint(self):
	pass

    def apply_constraint(self, p, state):
	return p

    def MouseMove(self, p, state):
	p = self.apply_constraint(p, state)
	SelectAndDrag.MouseMove(self, p, state)

    def compute_endpoints(self):
	cur = self.drag_cur
	start = self.start
	end = self.end
	sel = self.selection
	if sel in self.selTop:
	    start = Point(start.x, cur.y)
	if sel in self.selBottom:
	    end	  = Point(end.x,   cur.y)
	if sel in self.selLeft:
	    start = Point(cur.x, start.y)
	if sel in self.selRight:
	    end = Point(cur.x, end.y)
	if sel == -1:
	    start = start + self.off
	    end = end + self.off
        return start, end

    def ButtonUp(self, p, button, state):
	p = self.apply_constraint(p, state)
	SelectAndDrag.DragStop(self, p)
	cur = self.drag_cur
	oldstart = self.start
	oldend = self.end
	start, end = self.compute_endpoints()
	self.start = start
	self.end = end
	result = self.ComputeTrafo(oldstart, oldend, start, end)
	self.Normalize()
	return result

    def ComputeTrafo(self, oldStart, oldEnd, start, end):
	pass

    def Normalize(self):
	sx, sy = self.start
	ex, ey = self.end
	if sx > ex:
	    sx, ex = ex, sx
	if sy > ey:
	    sy, ey = ey, sy
	self.start = Point(sx, sy)
	self.end = Point(ex, ey)

    def Hit(self, p, rect, device):
	pass

    def Select(self):
	self.selection = -1

    def SelectPoint(self, p, rect, device, mode = SelectSet):
	if p:
	    self.selection = 0
	else:
	    self.selection = -1
	return self.selection

    def SelectHandle(self, handle, mode = SelectSet):
	self.selection = handle.code

    def GetHandles(self):
	sx = self.start.x
	sy = self.start.y
	ex = self.end.x
	ey = self.end.y
	x2 = (sx + ex) / 2
	y2 = (sy + ey) / 2
        h = Sketch.Editor.handle.Handle
        c = self.handle_idx_to_sel
        e = Sketch.Editor
        return [h(e.HandleSizeTL, Point(sx, ey), code = c[0]),
                h(e.HandleSizeT, Point(x2, ey), code = c[1]),
                h(e.HandleSizeTR, Point(ex, ey), code = c[2]),
                h(e.HandleSizeL, Point(sx, y2), code = c[3]),
                h(e.HandleSizeR, Point(ex, y2), code = c[4]),
                h(e.HandleSizeBL, Point(sx, sy), code = c[5]),
                h(e.HandleSizeB, Point(x2, sy), code = c[6]),
                h(e.HandleSizeBR, Point(ex, sy), code = c[7])]
