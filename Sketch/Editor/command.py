# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

from types import TupleType
from Sketch.Base.warn import warn_tb, USER
import Sketch

from wrapper import DocumentWrapper


class Command:

    args = ()
    kwargs = None
    sensitive  = None
    checked = None
    dyntext = None
    channels = ()
    pass_key = 0
    translatable = 0

    def __init__(self, name, title, function, args = (), kwargs = None,
                 sensitive = None, checked = None, dyntext = None,
                 channels = None, pass_key = 0):
        self.name = name
        self.title = title
        self.function = function
        if args != ():
            if type(args) != TupleType:
                args = (args,)
            self.args = args
        if kwargs is not None:
            self.kwargs = kwargs
        if sensitive is not None:
            self.sensitive = sensitive
        if checked is not None:
            self.checked = checked
        if dyntext is not None:
            self.dyntext = dyntext
        if channels is not None:
            self.channels = channels
        if pass_key:
            self.pass_key = pass_key

    def Title(self):
        if self.translatable:
            return Sketch._(self.title)
        else:
            return self.title

    def Name(self):
        return self.name

    def Sensitive(self, context):
        if  self.sensitive is not None:
            return self.sensitive(context)
        return 1

    def Checked(self, context):
        if context.HasEditor() and self.checked is not None:
            return self.checked(context)
        return 0 # XXX raise an exception?

    def IsCheckCommand(self):
        return self.checked is not None

    def DynText(self, context):
        if self.dyntext is not None:
            return self.dyntext(context)
        return self.Title()

    def HasDynText(self):
        return self.dyntext is not None

    def PassKey(self):
        return self.pass_key

    def Execute(self, context, args = ()):
        kw = self.kwargs
        if kw is None:
            kw = {}
        if type(args) != TupleType:
            args = (args,)
        #print self.name, self.args, args
        apply(self.function, (context,) + self.args + args, kw)


class EditorCommand(Command):

    def Sensitive(self, context):
        if context.HasEditor():
            if self.sensitive is not None:
                return self.sensitive(context)
            return 1
        return 0

    def DynText(self, context):
        if context.HasEditor():
            return Command.DynText(self, context)
        return self.Title()
    
    

class SafeScript(EditorCommand):

    def Execute(self, context, args = ()):
        document = context.document
        document.BeginTransaction(self.Title())
        try:
            try:
                context.document = DocumentWrapper(context.document)
                EditorCommand.Execute(self, context, args)
            except:
                warn_tb(USER, 'Error in user script "%s"', self.name)
                document.AbortTransaction()
        finally:
            context.document = document
            document.EndTransaction()


class AdvancedScript(EditorCommand):

    def Execute(self, context, args = ()):
        document = context.document
        document.BeginTransaction(self.Title())
        try:
            try:
                EditorCommand.Execute(self, context, args)
            except:
                warn_tb(USER, 'Error in user script "%s"', self.name)
                document.AbortTransaction()
        finally:
            document.EndTransaction()

    
class BuiltinCommand(Command):
    translatable = 1

class BuiltinEditorCommand(AdvancedScript):
    translatable = 1
