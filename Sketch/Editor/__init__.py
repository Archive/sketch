# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from const import *
import types

import Sketch

from registry import ScriptRegistry, Menu, BuildMenuTree, Keymap
from command import Command, SafeScript, AdvancedScript, BuiltinCommand, \
     BuiltinEditorCommand

Registry = ScriptRegistry()

#AddFunction = Registry.AddFunction
Add = Registry.Add

DocCommand = AdvancedScript

def AddCommand(name, title, function, args = (), channels = (),
               sensitive = None, script_type = SafeScript):
    # XXX extend this with menu and key-stroke
    Registry.Add(script_type(name, title, function, args = args,
                             channels = channels, sensitive = sensitive))

def AddEditorBuiltin(name, title, function = None, *args, **kw):
    if type(name) == types.FunctionType:
        function = name
        name = function.__name__
    Registry.Add(apply(BuiltinEditorCommand, (name, title, function) + args,
                       kw))

def AddBuiltin(name, title, function = None, *args, **kw):
    if type(name) == types.FunctionType:
        function = name
        name = function.__name__
    Registry.Add(apply(BuiltinCommand, (name, title, function) + args, kw))


from drawer import Creator, RectangularCreator, Editor

from drawrectangle import RectangleCreator
from drawellipse import EllipseCreator
from drawbezier import PolyBezierCreator, PolyLineCreator, FreehandCreator

from editellipse import EllipseEditor
from editrectangle import RectangleEditor
from editbezier import PolyBezierEditor
from editguide import GuideEditor

from doceditor import EditorWithSelection, sense_selection
from context import Context

from document import InteractiveDocument

from texttool import SimpleTextCreator, CommonTextEditor, SimpleTextEditor, \
     InternalPathTextEditor, PathTextCreator

import tools, selectiontool, edittool, texttool, aligntool
import builtins

from smoothcurve import smoothen_freehand_curve


class EditorPreferences(Sketch.Base.Preferences):

    _filename = 'editor.prefs'

    #
    #   Undo
    #
    #	how many undo steps sketch remembers. None means unlimited.
    undo_limit = None

    #
    #	Duplication offset
    #
    #	When objects are duplicated, the new copies are translated by
    #	duplicate_offset, given in document coordiates
    #
    duplicate_offset = (10, 10)

    # Freehand tool
    #
    # Accuracy. maximum distance between the mouse samples and the
    # fitted bezier curves in pixels
    freehand_accuracy = 1.0


preferences = EditorPreferences()


standard_menu = [
    (''"File", ['file_new',
                'file_open',
                'file_save',
                'file_save_as',
                None,
                'file_print',
                None,
                'insert_document',
                None,
                'close_window',
                'exit',
                ]),
    (''"Edit", ['undo',
                'redo',
                'history_panel',
                None,
                'copy',
                'cut',
                'paste',
                'delete',
                None,
                'select_all',
                None,
                'duplicate',
                ]),
    (''"View", ['zoom_100',
                'zoom_in',
                'zoom_out',
                None,
                'view_fit_window',
                'view_fit_selection_to_window',
                'view_fit_page_to_window',
                None,
                'view_libart',
                'view_outlined',
                None,
                'tree_view',
                None,
                'create_new_editor',
                'create_new_view',
                ]),
    (''"Effects", ['flip_horizontal',
                   'flip_vertical',
                   None,
                   'remove_transformation',
                   None,
                   'blend_dialog',
                   'blend_cancel',
                   None,
                   'create_mask_group',
                   'create_path_text',
                   ]),
    (''"Curves", ['cont_angle',
                  'cont_smooth',
                  'cont_symmetrical',
                  'segments_to_lines',
                  'segments_to_curve',
                  'select_all_nodes',
                  'delete_nodes',
                  'insert_nodes',
                  'close_nodes',
                  'open_nodes',
                  None,
                  'combine_beziers',
                  'split_beziers',
                  None,
                  'convert_to_curve',
                  ]),
    (''"Arrange", ['align_dialog',
                   None,
                   'move_to_top',
                   'move_to_bottom',
                   'move_one_up',
                   'move_one_down',
                   None,
                   'abut_horizontal',
                   'abut_vertical',
                   None,
                   'group',
                   'ungroup',
                   None,
                   'snap_to_grid',
                   'snap_to_guides',
                   'snap_to_objects',
                   ]),
    (''"Style", ['set_fill_none',
                 'set_fill_color',
                 'fill_panel',
                 None,
                 'set_line_none',
                 'set_line_color',
                 None,
                 'property_panel'
                 ]),

    (''"Tools", ['selection_tool',
                 'edit_tool',
                 'align_tool',
                 'zoom_tool',
                 'ellipse_tool',
                 'rectangle_tool',
                 'curve_tool',
                 'line_tool',
                 'freehand_tool',
                 'text_tool',
                 None,
                 'switch_tool',
                 ]),
                  
    ]


standard_keystrokes = [
    ('file_open', 'C-o'),
    ('file_save', 'C-s'),
    ('exit', 'C-q'),
    ('close_window', 'C-w'),

    ('delete', 'Delete'),
    ('undo', 'C-z'),
    ('redo', 'C-r'),
    ('duplicate', 'C-d'),

    ('group', 'C-g'),
    ('ungroup', 'C-u'),

    ('selection_tool', 's'),
    ('edit_tool', 'e'),
    ('align_tool', 'a'),
    ('rectangle_tool', 'r'),
    ('ellipse_tool', 'c'),      # c = circle
    ('curve_tool', 'b'),        # b = bezier
    ('line_tool', 'l'),
    ('freehand_tool', 'f'),
    ('zoom_tool', 'z'),
    ('text_tool', 't'),

    ('switch_tool', 'space'),

    ('zoom_100', '1'),
    ('zoom_in', '>'),
    ('zoom_out', '<'),

    ('view_fit_window', 'S-F4'),
    ('view_fit_selection_to_window', 'C-F4'),
    ('view_fit_page_to_window', 'F4'),

    #('cont_angle', 'a'),
    ('cont_smooth', 's'),
    ('cont_symmetrical', 'y'),
    ('segments_to_lines', 'l'),
    ('segments_to_curve', 'b'),
    ]


text_keystrokes = [
    ('move_char_forward', 'Right'),
    ('move_char_forward', 'C-f'),
    ('move_char_backward', 'Left'),
    ('move_char_backward', 'C-b'),
    ('move_to_beginning_of_line', 'C-a'),
    ('move_to_beginning_of_line', 'Home'),
    ('move_to_end_of_line', 'C-e'),
    ('move_to_end_of_line', 'End'),

    ('delete_char_forward', 'Delete'),
    ('delete_char_forward', 'C-d'),
    ('delete_char_backward', 'BackSpace'),
    ]

align_keystrokes = [
    ('align_left', 'l'),
    ('align_top', 't'),
    ('align_right', 'r'),
    ('align_bottom', 'b'),
    ('align_center_x', 'v'),
    ('align_center_y', 'h'),
    ('align_center', 'c'),
    ('set_align_reference_page', 'p'),
    ('set_align_reference_last', 'z'),
    ('set_align_reference_selection', 's'),
    ]

# text command not listed: insert_char

toolmap = {'SelectionTool': selectiontool.SelectionTool,
           'EditTool': edittool.EditTool,
           'AlignTool': aligntool.AlignTool,
           'TextTool': texttool.TextTool,
           'ZoomTool': tools.ZoomTool,
           'CreateRect': tools.RectangleTool,
           'CreateCurve': tools.PolyBezierTool,
           'CreateEllipse': tools.EllipseTool,
           'CreatePoly': tools.PolyLineTool,
           'CreateFreehand': tools.FreehandTool
           }


def GetEditor(object):
    cls = object.__class__
    editor = None
    if issubclass(cls, Sketch.Graphics.Rectangle):
        editor = RectangleEditor
    elif issubclass(cls, Sketch.Graphics.Ellipse):
        editor = EllipseEditor
    elif issubclass(cls, Sketch.Graphics.PolyBezier):
        editor = PolyBezierEditor
    if editor is not None:
        editor = editor(object)
    return editor

def GetTextEditor(object, creator = None):
    cls = object.__class__
    editor = None
    if issubclass(cls, Sketch.Graphics.SimpleText):
        editor = SimpleTextEditor
    elif issubclass(cls, Sketch.Graphics.InternalPathText):
        editor = InternalPathTextEditor
    if editor is not None:
        editor = editor(object, creator = creator)
    return editor

def init(argv, load_user_preferences = 0):
    Sketch.Graphics.init(argv, load_user_preferences = load_user_preferences)
    Sketch.Plugin.init(argv, load_user_preferences = load_user_preferences)
    preferences._load()
