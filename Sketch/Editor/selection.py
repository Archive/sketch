# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import operator
from types import ListType, TupleType
import time
from sets import Set

import Sketch
from Sketch import _, UnionRects, _sketch
from Sketch.Base import CreateListUndo, NullUndo
from Sketch.Graphics import Bounded
from Sketch.Lib.util import flatten


class Selection(Bounded):

    """
    Class representing the set of selected objects.

    Selection objects are bound to the specific document object they're
    instantiated with.

    Internally, the selection is a list of objects which is kept in a
    normalized order. The order is the same as the stacking order of the
    objects in the document.

    The selection also remembers the list of the last selected objects.
    This list is updated via normalize() when objects are added or
    removed from the selection. When the selection is set, the last
    selected list is emptied as it would be identical to the list of
    objects.

    Some methods return a path to an object. A path here means a tuple
    of integers describing the path through the object hierarchy
    starting at the document down to the object itself. Each integer is
    an index into the list of children of one of the object's parents.
    The first is the index of the layer, and the last the index of the
    object in its immediate parent's list of children. This path can be
    used to retrieve an ancestor directly from the document through its
    __getitem__ hook (i.e. just use document[path])
    """

    # extend the _lazy_attrs with the common path cache
    _lazy_attrs = Bounded._lazy_attrs.copy()
    _lazy_attrs["_common_path"] = "update_common_path"
    _lazy_attrs["last_coord_rect"] = "update_last_coord_rect"

    def __init__(self, document):
        """Initialize the selection object.

        document is the document the selection is used with
        """
        self.objects = []
        self.last_selected = []
        self.document = document

    def normalize(self):
        # put the items of self into normalized form. In addition to
        # ordering them according to the stacking order in the document,
        # make sure that self.objects contains no object more than once
        # and that no two objects have a direct or indirect parent/child
        # relationship.

        # strategy: Create a list with tuples of the form (path, object)
        # where path is the path to the object and sort it. Sorting the
        # list serve two purposes. It will put duplicate entries next to
        # each other and since longer tuples compare larger than shorter
        # tuples with the same initial items (e.g. (a, b) < (a, b, c))
        # which means that a parent will be found immediately before its
        # descendants (when iterating forwards through the list).

        # If there's only one object or none at all, there's nothing to
        # do.
        if len(self.objects) < 2:
            return

        # track whether the list was changed
        changed = 0

        # get the (path, object) list and sort it
        objs, cache = self._make_path_list(self.objects, {})
        objs.sort()

        # iterate backwards through the list because we may be removing
        # items from the list.
        last_info, obj = objs[-1]
        for idx in range(len(objs) - 2, -1, -1):
            info, obj = objs[idx]

            if info == last_info:
                # Duplicate entry. Remove one
                del objs[idx]
                changed = 1
                continue

            if len(info) < len(last_info):
                # info may be the parent of last_info
                while info == last_info[:len(info)]:
                    # remove all descendants of info. As pointed out
                    # above all of them will follow immediately and all
                    # of them will have paths starting with info
                    del objs[idx + 1]
                    changed = 1

                    # since we just remove the object at idx + 1, idx +
                    # 1 is now the index of the item that followed the
                    # one just removed. Update last_info (the path of
                    # the next potential child of info) if that item
                    # exists, otherwise we've removed all the children.
                    if idx + 1 < len(objs):
                        last_info = objs[idx + 1][0]
                    else:
                        break
            last_info = info

        # update the actual set
        self.objects = map(operator.getitem, objs, [-1] * len(objs))

        # update the last objects list
        if self.last_selected:
            last_objs, cache = self._make_path_list(self.last_selected, cache)
            last_objs_dict = dict(last_objs)
            last_paths = Set(last_objs_dict.keys())
            common_paths = list(last_paths.intersection([b[0] for b in objs]))
            common_paths.sort()
            self.last_selected = [last_objs_dict[p] for p in common_paths]

        return changed

    def _make_path_list(self, objects, cache):
        path = self.path_for_object
        result = [(path(obj, cache), obj) for obj in objects]
        return result, cache

    def path_for_object(self, obj, cache, id = id, idind = _sketch.IdIndex):
        path = cache.get(id(obj))
        if path is not None:
            return path
        parent = obj.parent
        if parent is not None:
            path = self.path_for_object(parent, cache) \
                   + (idind(parent.objects, obj),)
        else:
            path = (idind(self.document.layers, obj),)
        cache[id(obj)] = path
        return path

    def SetSelection(self, objects):
        """Make objects the only selected objects.

        Return true if the set of selected objects has changed by this.
        """
        old_objs = self.objects
        # empty the last_selected list as it would be the same as
        # self.objects
        self.last_selected = []
        if objects is None:
            self.objects = []
        elif type(objects) == ListType:
            self.objects = objects[:]
            self.normalize()
        else:
            self.objects = [objects]
        self.del_lazy_attrs()
        return old_objs != self.objects

    def Add(self, objects):
        """Add the objects in the list objects to the set of seleced objecs.

        Return true if the set of selected objects has changed by this.
        """
        if not objects:
            return 0
        old_len = len(self.objects)
        if type(objects) == ListType:
            self.objects = self.objects + objects
            self.last_selected = objects
        elif objects:
            self.objects.append(objects)
            self.last_selected = [objects]
        changed = self.normalize()
        self.del_lazy_attrs()
        return changed or old_len != len(self.objects)

    def Subtract(self, objects):
        """Remove objects from the set of seleced objecs.

        The argument may either be a list of objects or a single object.

        Return true if the set of selected objects has been changed
        otherwise return false.
        """
        if not objects:
            return 0
        old_len = len(self.objects)
        if type(objects) != ListType:
            objects = [objects]
        for objs in [self.objects, self.last_selected]:
            dict = {}
            map(operator.setitem, [dict] * len(objs), map(id, objs),
                range(len(objs)))
            indices = map(dict.get, map(id, objects))
            indices.sort()
            indices.reverse()
            for idx in indices:
                if idx is not None:
                    del objs[idx]
        self.del_lazy_attrs()
        return old_len != len(self.objects)

    def GetObjects(self):
        """Return the set of selected objects as a list"""
        return self.objects[:]

    def GetLastSelected(self):
        """Return the last selected objects as a list"""
        if self.last_selected:
            return self.last_selected[:]
        else:
            return self.objects[:]


    def GetPath(self):
        """
        Return a tuple of integers describing the path to the selected object.

        If more than one object or none at all is selected, return an
        empty tuple
        """
        if len(self.objects) == 1:
            return self.path_for_object(self.objects[0], {})
        return ()

    def CommonPath(self):
        """Return the longest common path of the selected objects.

        The longest common path is the path of the common parent most
        deeply contained in the object hierarchy.
        """
        return self._common_path

    def update_common_path(self):
        # Upate the _common_path lazy attribute.
        #
        # The cases of exactly one or zero selected objects are trivial.
        # For several selected objects we can make use of the fact that
        # the objects are sorted according to their stacking order so
        # that we simply have to determine the first few integers the
        # paths of the bottom-most and top-most objects have in common.
        if len(self.objects) == 1:
            result = self.GetPath()
        elif len(self.objects) > 1:
            cache = {}
            first = self.path_for_object(self.objects[0], cache)
            last = self.path_for_object(self.objects[-1], cache)
            if len(first) > len(last):
                length = len(last)
                first = first[:length]
            else:
                length = len(first)
                last = last[:length]
            for i in range(length):
                if first[i] != last[i]:
                    result = first[:i]
                    break
            else:
                result = first
        else:
            result = ()
        self._common_path = result

    def update_last_coord_rect(self):
        if self.last_selected:
            self.last_coord_rect = reduce(UnionRects, [o.coord_rect for
                                                      o in self.last_selected])
        else:
            self.last_coord_rect = self.coord_rect

    def for_all(self, func):
        return map(func, self.objects)

    def ForAllUndo(self, func):
        if 0:
            import profile
            print 'profiling...'
            prof = profile.Profile()
            prof.runctx('undoinfo = self.for_all(func)', globals(), locals())
            prof.dump_stats("/tmp/forall.prof")
            print 'profiling... (done)'
        else:
            undoinfo = self.for_all(func)
        self.del_lazy_attrs()
        if len(undoinfo) == 1:
            undoinfo = undoinfo[0]
        if type(undoinfo) == ListType:
            return CreateListUndo(undoinfo)
        return undoinfo

    def __len__(self):
        return len(self.objects)

    __nonzero__ = __len__

    def update_rects(self):
        objects = self.objects
        boxes = map(lambda o: o.coord_rect, objects)
        if boxes:
            self.coord_rect = reduce(UnionRects, boxes)
        else:
            self.coord_rect = None
        boxes = map(lambda o: o.bounding_rect, objects)
        if boxes:
            self.bounding_rect = reduce(UnionRects, boxes)
        else:
            self.bounding_rect = None

    def Hit(self, p, rect, device):
        test = rect.overlaps
        for obj in self.objects:
            if test(obj.bounding_rect):
                if obj.Hit(p, rect, device):
                    return 1
        return 0

    def GetHandles(self):
        if self.objects:
            multiple = len(self.objects) > 1
            handles = flatten(self.for_all(lambda o, m = multiple:
                                           o.GetObjectHandle(m)))
            handles = [Sketch.Editor.handle.MakeIndicatorList(handles)]
        else:
            handles = []
        return handles

    def CallObjectMethod(self, aclass, methodname, args):
        if len(self.objects) == 1:
            obj = self.objects[0]
            if not isinstance(obj, aclass):
                return NullUndo
            try:
                method = getattr(obj, methodname)
            except AttributeError:
                return NullUndo

            undo = apply(method, args)
            if undo is None:
                undo = NullUndo
            return undo
        return NullUndo

    def GetObjectMethod(self, aclass, method):
        if len(self.objects) == 1:
            obj = self.objects[0]
            if isinstance(obj, aclass):
                try:
                    return getattr(obj, method)
                except AttributeError:
                    pass
        return None

    def InfoText(self):
        """Return a description of the selection.

        The return value is either a string with the text or a tuple
        consisting of a format string and a dictionary with values for
        the formatting. The formatting is expected to be done with
        Sketch.Lib.untils.format.
        """
        result = _("No Selection")
        common_path = self.CommonPath()
        if self.objects:
            sel_info = self.objects
            document = self.document
            if len(sel_info) == 1:
                dict = {'layer': document[common_path[0]].Name()}
                obj = sel_info[0]
                info = obj.Info()
                if type(info) == TupleType:
                    dict.update(info[1])
                    # the %% is correct here. The result has to be a
                    # %-template itself.
                    text = _("%s on `%%(layer)s'") % info[0]
                else:
                    dict['object'] = info
                    text = _("%(object)s on `%(layer)s'")
                result = text, dict
            else:
                # determine whether the objects are all on a single layer
                # or not. An easy way to do that is to check whether the
                # common path is empty (multiple layers) or not (a
                # single layer).
                if common_path:
                    # a single layer. The index of the layer is the
                    # first item of common_path
                    layer_name = document[common_path[0]].Name()
                    result = _("%(number)d objects on `%(layer)s'") \
                             % {'number':len(sel_info), 'layer':layer_name}
                else:
                    result = _("%d objects on several layers") % len(sel_info)

        return result
