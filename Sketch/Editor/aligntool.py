# Sketch - A Python-based interactive drawing program
# Copyright (C) 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from Sketch import Point, Rect
from Sketch.Base.const import SELECTION, EDITED, SelectAdd, SelectSubtract
from Sketch.Editor import HandleAlignTL, HandleAlignT, HandleAlignTC, \
     HandleAlignTR, HandleAlignL, HandleAlignLC, HandleAlignR, HandleAlignBL, \
     HandleAlignB, HandleAlignBR, HandleAlignC, HandleLine
from Sketch.Editor import AddEditorBuiltin

from selectiontool import SelectionRectangle, selection_type
from tools import ToolInstance, ToolInfo
from handle import Handle
from builtins import align_selected

class AlignRectangle(SelectionRectangle):

    def GetHandles(self):
        sx = self.start.x
        sy = self.start.y
        ex = self.end.x
        ey = self.end.y
        x2 = (sx + ex) / 2
        y2 = (sy + ey) / 2
        return [Handle(HandleAlignTL, Point(sx, ey)),
                Handle(HandleAlignT, Point(x2, ey)),
                Handle(HandleAlignTC, Point(x2, ey)),
                Handle(HandleAlignTR, Point(ex, ey)),
                Handle(HandleAlignL, Point(sx, y2)),
                Handle(HandleAlignLC, Point(sx, y2)),
                Handle(HandleAlignR, Point(ex, y2)),
                Handle(HandleAlignBL, Point(sx, sy)),
                Handle(HandleAlignB, Point(x2, sy)),
                Handle(HandleAlignBR, Point(ex, sy)),
                Handle(HandleAlignC, Point(x2, y2)),
                ]

class AlignToolInstance(ToolInstance):

    """A tool that allows to align the selected objects.

    USAGE:
        Click on the various handles to align the objects.
        The implicit reference is the coord_rect of the last selected
        objects.
        Click on any selected object to make it the active reference.
        Click on the page border to make the page the active reference.

    KEYBOARD SHORTCUTS:
        a: activate the align tool
        t, b, l, r: align to top, bottom, left, right respectively
        v, h: align centers vertically / horizontally
        c: align to center

        space: switch to selection tool

        p: set the reference to page.
        s: set the reference to the whole selection.
        z: set the reference to the last selected objects.
    """

    title = ''"Align"

    def __init__(self, editor):
        ToolInstance.__init__(self, editor)
        self.ref_rect = None
        self.rectangle = None
        self.SetReference('last')
        self.sides = {HandleAlignTL: ('top', 'left'),
                      HandleAlignT: ('top',),
                      HandleAlignTC: ('center_x',),
                      HandleAlignTR: ('top', 'right'),
                      HandleAlignL: ('left',),
                      HandleAlignLC: ('center_y',),
                      HandleAlignR: ('right',),
                      HandleAlignBL: ('bottom', 'left'),
                      HandleAlignB: ('bottom',),
                      HandleAlignBR: ('bottom', 'right'),
                      HandleAlignC: ('center_x', 'center_y'),
                      }
        self.editor.Subscribe(SELECTION, self.selection_changed)
        self.editor.Subscribe(EDITED, self.doc_was_edited)

    def SetReference(self, reference):
        """Sets the reference for the align operation.

        reference should be 'last', 'page' or 'selection'.
        """
        if reference not in ('last', 'page', 'selection'):
            raise ValueError("reference must be 'last', 'page' or 'selection'")

        self.reference = reference
        editor = self.editor
        if reference == 'page':
            self.ref_rect = editor.document.PageRect()
        elif reference == 'selection':
            self.ref_rect = editor.Selection().coord_rect
        elif reference == 'last':
            self.ref_rect = editor.Selection().last_coord_rect
        self.rectangle = AlignRectangle(self.ref_rect)
        self.editor.update_handles()

    def End(self):
        self.editor.Unsubscribe(SELECTION, self.selection_changed)
        self.editor.Unsubscribe(EDITED, self.doc_was_edited)
        ToolInstance.End(self)

    def selection_changed(self):
        self.SetReference(self.reference)

    def doc_was_edited(self, *args):
        self.selection_changed()

    def ButtonClick(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonClick(self, context, p, snapped, button, state,
                                 handle = handle)
        type = selection_type(state)
        test = context.test_device()
        # Calculate the page rect to detect clicks on the page border.
        d2w = context.DocToWin
        page_w, page_h = self.editor.document.PageSize()
        left, bottom = d2w(0, 0)
        right, top = d2w(page_w, page_h)
        win_page_rect = Rect(left, bottom, right, top)

        if handle is not None:
            # If a handle is clicked align the selected objects
            sides = self.sides[handle.type]
            self.editor.BeginTransaction('Align Objects')
            for side in sides:
                # FIXME: the context here is actually the canvas window
                # the user is interacting with.  It's not the context
                # object that should be passed to a command.
                align_selected(context, side,
                               ref_rect = self.rectangle.coord_rect)
            self.editor.EndTransaction()
        elif self.editor.SelectionHit(p, test) and type != SelectSubtract:
            # Make the clicked object be the last selected by adding it to
            # the selection (even if shift is not pressed)
            self.editor.SelectPoint(p, test, SelectAdd)
        elif win_page_rect.grown(10).contains_point(d2w(p)) and \
                    not win_page_rect.grown(-10).contains_point(d2w(p)):
            # A click near the page border sets the reference to the
            # whole page rect.
            self.SetReference('page')
        else:
            rect = test.HitRectAroundPoint(p)
            hit_obj = self.editor.selection_from_point(p, rect, test, ())
            if hit_obj:
                self.editor.SelectPoint(p, test, type)
            else:
                # A click outside the selection switches to Selection tool
                self.editor.SetTool('SelectionTool')

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonPress(self, context, p, snapped, button, state,
                                 handle = handle)
        test = context.test_device()
        # check, whether a guide line is hit:
        pick = self.editor.PickActiveObject(test, p)
        if pick is not None and pick.is_GuideLine:
            # edit guide line
            object = Sketch.Editor.GuideEditor(pick)
        else:
            # Select by rubberbanding
            object = SelectionRectangle(p)
        self.begin_edit_object(context, object, snapped, button, state)

    def ButtonRelease(self, context, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, context, p, snapped, button, state)
        object = self.end_edit_object(context, snapped, button, state)
        self.editor.SelectRect(object.bounding_rect, selection_type(state))

    def Handles(self):
        if self.editor.HasSelection():
            # Get the Align handles
            handles = self.rectangle.GetHandles()
            # Get the small indicator handles
            handles.append(self.editor.Selection().GetHandles()[0])
            # I found that it helps to see the coor_rect of the selection
            # when there are more objects selected
            if self.editor.CountSelected() > 1:
                sx, sy, ex, ey = self.editor.Selection().coord_rect
                handles += [Handle(HandleLine, Point(sx, sy), Point(sx, ey)),
                            Handle(HandleLine, Point(sx, ey), Point(ex, ey)),
                            Handle(HandleLine, Point(ex, ey), Point(ex, sy)),
                            Handle(HandleLine, Point(ex, sy), Point(sx, sy)),
                            ]
        else:
            handles = []
        return handles


AlignTool = ToolInfo("AlignTool", AlignToolInstance, active_cursor = 1)

def align_tool_has_selection(context):
    editor = context.editor
    return isinstance(editor.tool, AlignToolInstance) and editor.HasSelection()

def set_align_reference(context, reference):
    if align_tool_has_selection(context):
        context.editor.tool.SetReference(reference)

AddEditorBuiltin('set_align_reference_page', ''"Set align reference to page",
                 set_align_reference, args = ('page',),
                 sensitive = align_tool_has_selection)

AddEditorBuiltin('set_align_reference_last',
                 ''"Set align reference to last selected",
                 set_align_reference, args = ('last',),
                 sensitive = align_tool_has_selection)

AddEditorBuiltin('set_align_reference_selection',
                 ''"Set align reference to whole selection",
                 set_align_reference, args = ('selection',),
                 sensitive = align_tool_has_selection)

