# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from math import pi, floor

import Sketch
from Sketch import _, CreatePath, Polar
from Sketch._sketch import ContSymmetrical
from Sketch.Graphics import PolyBezier
from Sketch.Editor import Creator

from curvefunc import adjust_control_point


class PolyBezierCreator(Creator):

    creation_text = _("Create Curve")

    def __init__(self):
	self.path = CreatePath()
	Creator.__init__(self)

    def apply_constraints(self, p, state):
        if self.path.len > 0:
            node = self.path.Node(-1)
        elif self.dragging:
            node = self.drag_start
        else:
            return p
        
        if state & Sketch.Editor.ConstraintMask:
            radius, angle = (p - node).polar()
            pi12 = pi / 12
            angle = pi12 * floor(angle / pi12 + 0.5)
            p = node + Polar(radius, angle)
        return p

    def ButtonDown(self, p, button, state):
        p = self.apply_constraints(p, state)
	if self.path.len == 0:
	    self.path.AppendLine(p)
	else:
	    self.path.AppendBezier(self.drag_cur, p, p)
	return self.DragStart(p)

    def MouseMove(self, p, state):
	if not (state & Sketch.Editor.Button1Mask):
	    return
	self.DragMove(self.apply_constraints(p, state))

    def ButtonUp(self, p, button, state):
	if not (state & Sketch.Editor.Button1Mask):
	    return
        p = self.apply_constraints(p, state)
	self.DragStop(p)
	if self.path.len > 1:
	    type, (p1, p2), p, cont = self.path.Segment(-1)
	    p2 = adjust_control_point(p2, p, self.drag_cur, ContSymmetrical)
	    self.path.SetBezier(-1, p1, p2, p, ContSymmetrical)

    def EndCreation(self):
	return self.path.len > 1

    def DrawDragged(self, device, partially):
	if not partially:
	    self.path.draw_not_last(device.Bezier, device.Line)
	device.DrawHandleLine(self.path.Node(-1), self.drag_cur)
	device.DrawSmallRectHandle(self.drag_cur)
	if self.path.len > 1:
	    type, (p1, p2), p, cont = self.path.Segment(-1)
	    p2 = adjust_control_point(p2, p, self.drag_cur, ContSymmetrical)
	    device.Bezier(self.path.Node(-2), p1, p2, p)
	    device.DrawHandleLine(p, p2)
	    device.DrawSmallRectHandle(p2)

    def CreatedObject(self):
	return PolyBezier(paths = (self.path,))



class PolyLineCreator(Creator):

    creation_text = _("Create Poly-Line")

    def __init__(self):
	self.path = CreatePath()
        self.was_dragged = 0
	Creator.__init__(self)

    def apply_constraints(self, p, state):
        if self.path.len > 0:
            node = self.path.Node(-1)
        elif self.dragging:
            node = self.drag_start
        else:
            return p
        
        if state & Sketch.Editor.ConstraintMask:
            radius, angle = (p - node).polar()
            pi12 = pi / 12
            angle = pi12 * floor(angle / pi12 + 0.5)
            p = node + Polar(radius, angle)
        return p

    def ButtonDown(self, p, button, state):
        return self.DragStart(self.apply_constraints(p, state))

    def MouseMove(self, p, state):
	if not (state & Sketch.Editor.Button1Mask):
	    return
        self.was_dragged = 1
	self.DragMove(self.apply_constraints(p, state))

    def ButtonUp(self, p, button, state):
	if not (state & Sketch.Editor.Button1Mask):
	    return
	self.DragStop(self.apply_constraints(p, state))
        if self.was_dragged and self.path.len == 0:
            self.path.AppendLine(self.drag_start)
        self.path.AppendLine(self.drag_cur)

    def EndCreation(self):
	return self.path.len > 1

    def DrawDragged(self, device, partially):
	if self.path.len > 1:
	    if not partially:
                device.DrawBezierPath(self.path)
        if self.path.len >= 1:
	    device.Line(self.path.Node(-1), self.drag_cur)
        else:
            if self.was_dragged:
                device.Line(self.drag_start, self.drag_cur)

    def CreatedObject(self):
	return PolyBezier(paths = (self.path,))




class FreehandCreator(Creator):

    creation_text = _("Freehand Draw")

    def __init__(self):
	self.path = CreatePath()
        self.was_dragged = 0
	Creator.__init__(self)

    def ButtonDown(self, p, button, state):
        self.path.AppendLine(p)
	return self.DragStart(p)

    def MouseMove(self, p, state):
	if not (state & Sketch.Editor.Button1Mask):
	    return
        self.was_dragged = 1
	self.DragMove(p)
        self.path.AppendLine(p)

    def ButtonUp(self, p, button, state):
	if not (state & Sketch.Editor.Button1Mask):
	    return
	self.DragStop(p)
        self.path.AppendLine(p)

    def DrawDragged(self, device, partially):
        path = self.path
	if path.len > 1:
	    #if not partially:
            device.DrawBezierPath(path)
            #else:
            #       device.Line(path.Node(-2), path.Node(-1))

    def CreatedObject(self):
        #from smoothcurve import smoothen_curve
        #path = smoothen_curve(self.path)
	return PolyBezier(paths = (self.path,))

