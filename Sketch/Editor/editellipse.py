# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from math import sin, cos, atan2, pi, fmod, floor

import Sketch
from Sketch import Polar, SingularMatrix, Translation, Scale, Rotation
from Sketch.Base import CreateMultiUndo
from Sketch.Base.const import ArcArc, ArcChord, ArcPieSlice
from Sketch.Graphics import Ellipse

import handle, const

from Sketch.Editor import Editor


class EllipseEditor(Editor):

    EditedClass = Ellipse

    selection = -1

    def ButtonDown(self, p, button, state):
        if self.selection == 1:
	    start = self.trafo(cos(self.start_angle), sin(self.start_angle))
	else:
	    start = self.trafo(cos(self.end_angle), sin(self.end_angle))
	Editor.DragStart(self, start)
	return p - start

    def apply_constraint(self, p, state):
	if state & Sketch.Editor.ConstraintMask:
            try:
                inverse = self.trafo.inverse()
                p2 = inverse(p)
                r, phi = p2.polar()
                pi12 = pi / 12
                angle = pi12 * floor(phi / pi12 + 0.5)
                pi2 = 2 * pi
                d1 = fmod(abs(phi - angle), pi2)
                if self.selection == 1:
                    selected_angle = self.end_angle
                else:
                    selected_angle = self.start_angle
                d2 = fmod(abs(phi - selected_angle), pi2)
                if d2 < d1:
                    phi = selected_angle
                else:
                    phi = angle
                p = self.trafo(Polar(r, phi))
            except SingularMatrix:
                pass
	return p

    def MouseMove(self, p, state):
	p = self.apply_constraint(p, state)
	Editor.MouseMove(self, p, state)

    def ButtonUp(self, p, button, state):
	p = self.apply_constraint(p, state)
	Editor.DragStop(self, p)
	trafo, start_angle, end_angle, arc_type = self.parameters()
        self.selection = -1
	return CreateMultiUndo(self.object.SetAngles(start_angle, end_angle),
			       self.object.SetArcType(arc_type))

    def parameters(self):
	start_angle = self.start_angle; end_angle = self.end_angle
        arc_type = self.arc_type
        trafo = self.trafo

        # start / end angle
        if arc_type == ArcArc:
            arc_type = ArcPieSlice
        try:
            inverse = self.trafo.inverse()
            p = inverse(self.drag_cur)
            if self.selection == 1:
                start_angle = atan2(p.y, p.x)
            elif self.selection == 2:
                end_angle = atan2(p.y, p.x)
            if abs(p) > 1:
                arc_type = ArcArc
        except SingularMatrix:
            pass
        if fmod(abs(start_angle - end_angle), 2 * pi) < 0.0001:
            if self.selection == 1:
                start_angle = end_angle
            else:
                end_angle = start_angle
	return (trafo, start_angle, end_angle, arc_type)

    def DrawDragged(self, device, partially):
	trafo, start_angle, end_angle, arc_type = self.parameters()
	device.SimpleEllipse(trafo, start_angle, end_angle, arc_type)

    def GetHandles(self):
	trafo = self.trafo
	start_angle = self.start_angle; end_angle = self.end_angle
        handles = []
	p1 = trafo(cos(self.start_angle), sin(self.start_angle))
	if start_angle == end_angle:
            handles.append(handle.MakeNodeHandle(p1, code = 1))
        else:
            p2 = trafo(cos(self.end_angle), sin(self.end_angle))
            handles.append(handle.MakeNodeHandle(p1, code = 1))
            handles.append(handle.MakeNodeHandle(p2, code = 2))
        return handles

    def SelectHandle(self, handle, mode):
	self.selection = handle.code
        return const.SelectSingle

    def SelectPoint(self, p, rect, device, mode):
	return const.SelectNone

    def HasSelection(self):
        return self.selection > 0

    def SelectNone(self):
        self.selection = -1
