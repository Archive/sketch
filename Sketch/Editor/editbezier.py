# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from math import pi, floor, ceil, atan2

import Sketch
from Sketch import _, Point, Polar, EmptyRect, PointsToRect, CreatePath
from Sketch.Base.const import SelectSet, SelectAdd, SelectSubtract, \
     SelectDrag, SHAPE
from Sketch.Base.warn import pdebug, warn, INTERNAL

from Sketch._sketch import ContAngle, ContSmooth, ContSymmetrical, Bezier, \
     Line, SelNone, SelNodes, SelSegmentFirst, SelSegmentLast
from Sketch.Graphics import PolyBezier

from Sketch.Editor import Editor

import handle, const
from curvefunc import adjust_control_point, delete_segments, insert_segments, \
     segments_to_lines, segments_to_beziers, set_continuity, insert_node_at, \
     split_path_at, segment_to_line, segment_to_curve

import const

SelCurvePoint = -1

class PolyBezierEditor(Editor):

    EditedClass = PolyBezier

    def __init__(self, object):
        self.selected_path = -1
        self.selected_idx = -1
        self.selection_type = SelNone
        self.other_segment = -1
        Editor.__init__(self, object)
        self.Deselect()

    def SelectPoint(self, p, rect, device, mode):
        """Select the point on curve nearest to p and contained in rect.
        """
        self.Deselect()
        found = []
        for i in range(len(self.paths)):
            path = self.paths[i]
            t = path.nearest_point(p.x, p.y)
            if t is not None:
                p2 = path.point_at(t)
                if rect.contains_point(p2):
                    found.append((abs(p - p2), i, t, p2))
        if found:
            dist, i, t, p2 = min(found)
            self.selected_path = i
            self.selected_idx = t
            self.selection_type = SelCurvePoint
            return const.SelectSingle
        return const.SelectNone

    def SelectHandle(self, handle, mode = SelectSet):
        ret = const.SelectMulti
        path_idx, segment = handle.code
        if segment < 0:
            segment = -segment
            if segment % 2:
                self.selection_type = SelSegmentFirst
            else:
                self.selection_type = SelSegmentLast
            self.selected_idx = (segment + 1) / 2
            segment = segment / 2
            ret = const.SelectSingle
        else:
            self.selection_type = SelNodes
            self.selected_idx = segment
        self.selected_path = path_idx
        path = self.paths[path_idx]
        if mode == SelectSet or mode == SelectDrag:
            if not path.SegmentSelected(segment):
                self.deselect()
                ret = const.SelectSingle
            path.SelectSegment(segment)
        elif mode == SelectAdd:
            path.SelectSegment(segment)
        elif mode == SelectSubtract:
            path.SelectSegment(segment, 0)
        return ret

    def SelectRect(self, rect, mode = SelectSet):
        selected = 0
        selected_path = None
        for i in range(len(self.paths)):
            selected = self.paths[i].select_rect(rect, mode) or selected
            if selected:
                selected_path = i
        self.selection_type = SelNodes
        if selected_path is not None:
            self.selected_path = selected_path
        return selected and const.SelectMulti or const.SelectNone

    def CanSelectAllNodes(self):
        return True

    def SelectAllNodes(self):
        for path in self.paths:
            for i in range(path.len):
                path.SelectSegment(i, 1)

    def HasSelection(self):
        return self.selected_path >= 0

    def deselect(self):
        for path in self.paths:
            path.deselect()

    def Deselect(self):
        self.deselect()
        self.selected_path = -1
        self.selected_idx = -1
        self.selection_type = SelNone

    SelectNone = Deselect

    def ButtonDown(self, p, button, state):
        if self.selected_path >= 0:
            path = self.paths[self.selected_path]
            if self.selection_type == SelNodes:
                prop = self.properties
                if prop.HasLine():
                    start = arrow_tip(path, self.selected_idx,
                                      prop.line_arrow1, prop.line_arrow2,
                                      prop.line_width)
                else:
                    start = path.Node(self.selected_idx)
                self.DragStart(start)
                return p - start
            elif self.selection_type == SelSegmentFirst:
                segment = self.selected_idx
                if segment > 1 \
                   and path.SegmentType(segment - 1) == Bezier\
                   and path.Continuity(segment - 1):
                    self.other_segment = segment - 1
                elif path.closed and segment == 1 \
                     and path.SegmentType(-1) == Bezier\
                     and path.Continuity(-1):
                    self.other_segment = path.len - 1
                else:
                    self.other_segment = -1
                p1 = path.Segment(segment)[1][0]
                self.DragStart(p1)
                return p - p1
            elif self.selection_type == SelSegmentLast:
                segment = self.selected_idx
                self.other_segment = -1
                if path.Continuity(segment):
                    if segment < path.len - 1 \
                       and path.SegmentType(segment + 1) == Bezier:
                        self.other_segment = segment + 1
                    elif path.closed and segment == path.len - 1 \
                         and path.SegmentType(1) == Bezier:
                        self.other_segment = 1
                p2 = path.Segment(segment)[1][1]
                self.DragStart(p2)
                return p - p2
            elif self.selection_type == SelCurvePoint:
                start = path.point_at(self.selected_idx)
                segment = int(ceil(self.selected_idx))
                prev = next = -1
                type = path.SegmentType(segment)
                if type == Bezier:
                    if path.Continuity(segment):
                        if segment < path.len - 1 \
                           and path.SegmentType(segment + 1) == Bezier:
                            next = segment + 1
                        elif path.closed and segment == path.len - 1 \
                             and path.SegmentType(1) == Bezier:
                            next = 1
                    if segment > 1 \
                       and path.SegmentType(segment - 1) == Bezier\
                       and path.Continuity(segment - 1):
                        prev = segment - 1
                    elif path.closed and segment == 1 \
                         and path.SegmentType(-1) == Bezier\
                         and path.Continuity(-1):
                        prev = path.len - 1
                else:
                    if segment < path.len - 1:
                        next = segment + 1
                    elif path.closed and segment == path.len - 1:
                        next = 1
                    if segment >= 1:
                        prev = segment - 1
                    elif path.closed and segment == 1:
                        prev = path.len - 1
                self.other_segment = (prev, next)
                self.DragStart(start)
                return p - start

    def apply_constraints(self, p, state):
        if state & Sketch.Editor.ConstraintMask:
            if self.selection_type == SelNodes:
                pi4 = pi / 4
                off = p - self.drag_start
                d = Polar(pi4 * round(atan2(off.y, off.x) / pi4))
                p = self.drag_start + (off * d) * d
            elif self.selection_type in (SelSegmentFirst, SelSegmentLast):
                path = self.paths[self.selected_path]
                if self.selection_type == SelSegmentFirst:
                    node = path.Node(self.selected_idx - 1)
                else:
                    node = path.Node(self.selected_idx)
                radius, angle = (p - node).polar()
                pi12 = pi / 12
                angle = pi12 * floor(angle / pi12 + 0.5)
                p = node + Polar(radius, angle)
        return p

    def MouseMove(self, p, state):
        self.DragMove(self.apply_constraints(p, state))

    def ButtonUp(self, p, button, state):
        p = self.apply_constraints(p, state)
        self.DragStop(p)
        type = self.selection_type
        if type == SelNodes:
            undo = []
            for path in self.paths:
                if path.selection_count() > 0:
                    undo.append(path.move_selected_nodes(self.off))
                else:
                    undo.append(None)
            if undo:
                self._changed(SHAPE)
                return (self.do_undo, undo)
        elif type in (SelSegmentFirst, SelSegmentLast):
            idx = self.selected_path
            segment = self.selected_idx
            path = self.paths[idx].Duplicate()
            paths = self.paths[:idx] + (path,) + self.paths[idx + 1:]
            if type == SelSegmentFirst:
                type, (p1, p2), node, cont = path.Segment(segment)
                path.SetBezier(segment, self.drag_cur, p2, node, cont)
                if self.other_segment >= 0:
                    other = self.other_segment
                    type, (p1, p2), node, cont = path.Segment(other)
                    p2 = adjust_control_point(p2, node, self.drag_cur, cont)
                    path.SetBezier(other, p1, p2, node, cont)
                path.SelectSegment(segment - 1)
            elif type == SelSegmentLast:
                type, (p1, p2), node, cont = path.Segment(segment)
                path.SetBezier(segment, p1, self.drag_cur, node, cont)
                if self.other_segment >= 0:
                    other = self.other_segment
                    type, (p1, p2), node2, cont2 = path.Segment(other)
                    p1 = adjust_control_point(p1, node, self.drag_cur, cont)
                    path.SetBezier(other, p1, p2, node2, cont2)
                path.SelectSegment(segment)
            return self.set_paths(paths) # set_paths calls _changed()
        elif self.selection_type == SelCurvePoint:
            idx = self.selected_path
            path = self.paths[idx].Duplicate()
            paths = self.paths[:idx] + (path,) + self.paths[idx + 1:]

            segment = int(self.selected_idx)
            t = self.selected_idx - segment
            type, control, node, cont = path.Segment(segment + 1)
            if type == Bezier:
                p1, p2 = control
                if t <= 0.5:
                    alpha = ((t * 2) ** 3) / 2
                else:
                    alpha = 1 - (((1 - t) * 2) ** 3) / 2

                p1 = p1 + (self.off / (3 * t * (1 - t)**2)) * (1 - alpha)
                p2 = p2 + (self.off / (3 * t**2 * (1 - t))) * alpha

                path.SetBezier(segment + 1, p1, p2, node, cont)
            else:
                path.SetLine(segment + 1, node + self.off, cont)
            prev, next = self.other_segment
            if prev >= 0:
                _type, _control, _node, _cont = path.Segment(prev)
                if _type == Bezier:
                    _p1, _p2 = _control
                    if type == Bezier:
                        _p2 = adjust_control_point(_p2, _node, p1, _cont)
                    else:
                        _p2 = _p2 + self.off
                        _node = _node + self.off
                    path.SetBezier(prev, _p1, _p2, _node, _cont)
                else:
                    path.SetLine(prev, _node + self.off, _cont)
            if next >= 0:
                _type, _control, _node, _cont = path.Segment(next)
                if _type == Bezier:
                    _p1, _p2 = _control
                    if type == Bezier:
                        _p1 = adjust_control_point(_p1, node, p2, cont)
                    else:
                        _p1 = _p1 + self.off
                    path.SetBezier(next, _p1, _p2, _node, _cont)
            return self.set_paths(paths) # set_paths calls _changed()

    def DrawDragged(self, device, partially):
        if self.selection_type == SelNodes:
            for path in self.paths:
                path.draw_dragged_nodes(self.off, partially,
                                        device.Bezier, device.Line)
        elif self.selection_type == SelSegmentFirst:
            if not partially:
                for path in self.paths:
                    path.draw_unselected(device.Bezier, device.Line)
            path = self.paths[self.selected_path]
            segment = self.selected_idx
            node = path.Node(segment - 1)
            type, (p1, p2), node2, cont = path.Segment(segment)
            device.Bezier(node, self.drag_cur, p2, node2)
            device.DrawSmallRectHandle(self.drag_cur)
            device.DrawHandleLine(node, self.drag_cur)
            if self.other_segment >= 0:
                other = self.other_segment
                type, (p1, p2), node, cont = path.Segment(other)
                p2 = adjust_control_point(p2, node, self.drag_cur, cont)
                device.Bezier(path.Node(other - 1), p1, p2, node)
                device.DrawSmallRectHandle(p2)
                device.DrawHandleLine(node, p2)
        elif self.selection_type == SelSegmentLast:
            if not partially:
                for path in self.paths:
                    path.draw_unselected(device.Bezier, device.Line)
            path = self.paths[self.selected_path]
            segment = self.selected_idx
            type, (p1, p2), node, cont = path.Segment(segment)
            device.Bezier(path.Node(segment - 1), p1, self.drag_cur, node)
            device.DrawSmallRectHandle(self.drag_cur)
            device.DrawHandleLine(node, self.drag_cur)
            if self.other_segment >= 0:
                other = self.other_segment
                type, (p1, p2), node2, cont2 = path.Segment(other)
                p1 = adjust_control_point(p1, node, self.drag_cur, cont)
                device.Bezier(node, p1, p2, node2)
                device.DrawSmallRectHandle(p1)
                device.DrawHandleLine(node, p1)
        elif self.selection_type == SelCurvePoint:
            path = self.paths[self.selected_path]
            segment = int(self.selected_idx)
            t = self.selected_idx - segment
            prevnode = path.Node(segment)

            type, control, node, cont = path.Segment(segment + 1)
            if type == Bezier:
                p1, p2 = control
                if t <= 0.5:
                    alpha = ((t * 2) ** 3) / 2
                else:
                    alpha = 1 - (((1 - t) * 2) ** 3) / 2

                p1 = p1 + (self.off / (3 * t * (1 - t)**2)) * (1 - alpha)
                p2 = p2 + (self.off / (3 * t**2 * (1 - t))) * alpha

                device.Bezier(prevnode, p1, p2, node)
                device.DrawSmallRectHandle(p1)
                device.DrawHandleLine(prevnode, p1)
                device.DrawSmallRectHandle(p2)
                device.DrawHandleLine(node, p2)
            else:
                device.DrawLine(prevnode + self.off, node + self.off)
            prev, next = self.other_segment
            if prev > 0:
                _type, _control, _node, _cont = path.Segment(prev)
                if _type == Bezier:
                    _p1, _p2 = _control
                    if type == Bezier:
                        _p2 = adjust_control_point(_p2, _node, p1, _cont)
                        device.Bezier(path.Node(prev - 1), _p1, _p2, _node)
                        device.DrawSmallRectHandle(_p2)
                        device.DrawHandleLine(_node, _p2)
                    else:
                        device.Bezier(path.Node(prev - 1), _p1, _p2 + self.off,
                                      _node + self.off)
                else:
                    device.DrawLine(path.Node(prev - 1), _node + self.off)
            if next >= 0:
                _type, _control, _node, _cont = path.Segment(next)
                if _type == Bezier:
                    _p1, _p2 = _control
                    if type == Bezier:
                        _p1 = adjust_control_point(_p1, node, p2, cont)
                        device.Bezier(node, _p1, _p2, _node)
                        device.DrawSmallRectHandle(_p1)
                        device.DrawHandleLine(node, _p1)
                    else:
                        device.Bezier(node + self.off, _p1 + self.off, _p2,
                                      _node)
                else:
                    device.DrawLine(node + self.off, _node)

    def GetHandles(self):
        NodeHandle = handle.MakeNodeHandle
        ControlHandle = handle.MakeControlHandle
        LineHandle = handle.MakeLineHandle
        handles = []
        append = handles.append
        for path_idx in range(len(self.paths)):
            path = self.paths[path_idx]
            SegmentSelected = path.SegmentSelected
            Node = path.Node
            Segment = path.Segment
            SegmentType = path.SegmentType
            if path.len > 0:
                if not path.closed:
                    append(NodeHandle(Node(0), SegmentSelected(0),
                                      (path_idx, 0)))
                for i in range(1, path.len):
                    selected = SegmentSelected(i)
                    append(NodeHandle(Node(i), selected, (path_idx, i)))
                    if (SegmentType(i) == Bezier
                        and (selected or SegmentSelected(i - 1))):
                        type, (p1, p2), node, cont = Segment(i)
                        append(ControlHandle(p1, (path_idx, -(2 * i - 1))))
                        append(ControlHandle(p2, (path_idx, -(2 * i))))
                        append(LineHandle(Node(i - 1), p1))
                        append(LineHandle(p2, node))
        if self.selection_type == SelCurvePoint:
            p = self.paths[self.selected_path].point_at(self.selected_idx)
            handles.append(handle.MakeCurveHandle(p))
        return handles

#      def GetHandles(self):
#          #import time
#          #t1 = time.clock()
#       nodes = []
#          lines = []
#          control_points = []
#       for path_idx in range(len(self.paths)):
#              path = self.paths[path_idx]
#              SegmentSelected = path.SegmentSelected
#              Node = path.Node
#              Segment = path.Segment
#              SegmentType = path.SegmentType
#              if path.len > 0:
#                  if not path.closed:
#                      nodes.append((Node(0), SegmentSelected(0), (path_idx, 0)))
#                  for i in range(1, path.len):
#                      selected = SegmentSelected(i)
#                      nodes.append((Node(i), selected, (path_idx, i)))
#                      if (SegmentType(i) == Bezier
#                          and (selected or SegmentSelected(i - 1))):
#                          type, (p1, p2), node, cont = Segment(i)
#                          control_points.append((p1, (path_idx, -(2 * i - 1))))
#                          control_points.append((p2, (path_idx, -(2 * i))))
#                          lines.append(LineHandle(Node(i - 1), p1))
#                          lines.append(LineHandle(p2, node))
#          handles = [handle.MakeListHandle(const.HandleNodeList, nodes),
#                     handle.MakeListHandle(const.HandleControlPointList,
#                                           control_points)] + lines
#          if self.selection_type == SelCurvePoint:
#              p = self.paths[self.selected_path].point_at(self.selected_idx)
#              handles.append(handle.MakeCurveHandle(p))
#          #print 'editbezier.GetHandles', time.clock() - t1
#       return handles

    def Info(self):
        selected = 0
        idx = None
        paths = self.paths
        for i in range(len(paths)):
            path = paths[i]
            count = path.selection_count()
            if count > 0:
                selected = selected + count
                idx = i
        if selected > 1:
            return _("%d nodes in PolyBezier") % selected
        else:
            if idx is not None:
                path = paths[idx]
                for i in range(path.len):
                    if path.SegmentSelected(i):
                        break
                else:
                    warn(INTERNAL, 'Strange selection count')
                    return _("PolyBezier")
                if i == 0:
                    return _("First node of PolyBezier")
                elif i == path.len - 1:
                    return _("Last node of PolyBezier")
                else:
                    return _("1 node of PolyBezier")
            else:
                if self.selection_type == SelCurvePoint:
                    return _("Point on curve at position %.2f") \
                           % self.selected_idx
                else:
                    return _("No Node of PolyBezier")

    #
    #   Special poly bezier protocol: closing, continuity
    #

    def CanOpenNodes(self):
        """Return true if the curve can be cut.

        A curve can be cut when one of the following is true:
            * A point on the curve is selected
            * A node that is not an open end is selected
        """
        if self.selection_type == SelCurvePoint:
            return True
        else:
            for path in self.paths:
                if path.selection_count() > 0:
                    start_idx = 1
                    if path.closed:
                        start_idx = 0
                    for i in range(start_idx, path.len-1):
                        if path.SegmentSelected(i):
                            return True
        return False

    def OpenNodes(self):
        if self.selection_type == SelCurvePoint:
            index = self.selected_path
            paths = list(self.paths)
            path = paths[index]
            paths[index:index + 1] = split_path_at(path, self.selected_idx)
            self.selected_idx = int(self.selected_idx) + 1
            self.selection_type = SelNodes
        else:
            paths = []
            for path in self.paths:
                if path.selection_count() >= 1:
                    if path.closed:
                        for i in range(path.len - 1):
                            if path.SegmentSelected(i):
                                start_idx = i
                                break
                    else:
                        start_idx = 0

                    newpath = CreatePath()
                    paths.append(newpath)
                    p = path.Node(start_idx)
                    newpath.AppendLine(p, ContAngle)

                    for i in range(start_idx + 1, path.len):
                        type, control, p, cont = path.Segment(i)
                        if path.SegmentSelected(i):
                            # XXX remove this ?
                            cont = ContAngle
                        newpath.AppendSegment(type, control, p, cont)
                        if path.SegmentSelected(i) and i < path.len - 1:
                            newpath = CreatePath()
                            newpath.AppendLine(p, ContAngle)
                            paths.append(newpath)

                    if start_idx != 0:
                        # the path was closed and the first node was not
                        # selected
                        for i in range(1, start_idx + 1):
                            type, control, p, cont = path.Segment(i)
                            newpath.AppendSegment(type, control, p, cont)
                else:
                    paths.append(path)
        return self.set_paths(paths)

    def CanCloseNodes(self, *args):
        """Return true if two open ended nodes are selected.

        args is used only to check if the method was called from
        self.CloseNodes (in which case it returns some extra arguments needed
        by this method)
        """
        two = 0
        one = 0
        can = False
        for i in range(len(self.paths)):
            path = self.paths[i]
            selected = path.selection_count()
            if not selected:
                continue
            if (path.closed and selected) or selected not in (1, 2):
                can = False
                break
            if selected == 1:
                if path.SegmentSelected(0) or path.SegmentSelected(-1):
                    one = one + 1
                    continue
                can = False
                break
            else:
                if path.SegmentSelected(0) and path.SegmentSelected(-1):
                    two = two + 1
                    continue
                can = False
                break

        if (one, two) in ((2, 0), (0, 1)):
            can = True

        if len(args):
            # called from self.CloseNodes
            return can, two, one
        else:
            return can

    def CloseNodes(self):
        # find out if close is possible
        can, two, one = self.CanCloseNodes(None)
        # now, close the nodes
        if one == 2 and two == 0:
            paths = []
            append_to = None
            for path in self.paths:
                if path.selection_count():
                    if append_to:
                        # path is the second of the paths involved
                        end_node = append_to.Node(-1)
                        if path.SegmentSelected(0):
                            for i in range(1, path.len):
                                type, p12, p, cont = path.Segment(i)
                                if end_node is not None and type == Bezier:
                                    p12 = (p12[0] + end_node - path.Node(0),
                                           p12[1])
                                    end_node = None
                                append_to.AppendSegment(type, p12, p, cont)
                        else:
                            for i in range(path.len - 1, 0, -1):
                                type, p12, p3, cont = path.Segment(i)
                                if end_node is not None and type == Bezier:
                                    p12 = (p12[0],
                                           p12[1] + end_node - path.Node(-1))
                                    end_node = None
                                p = path.Node(i - 1)
                                if type == Bezier:
                                    p12 = (p12[1], p12[0])
                                append_to.AppendSegment(type, p12, p,
                                                        path.Continuity(i - 1))
                        continue
                    else:
                        # path is the first of the paths involved
                        if path.SegmentSelected(0):
                            # reverse the path
                            append_to = CreatePath()
                            p = path.Node(-1)
                            append_to.AppendLine(p, ContAngle)
                            for i in range(path.len - 1, 0, -1):
                                type, p12, p3, cont = path.Segment(i)
                                p = path.Node(i - 1)
                                if type == Bezier:
                                    p12 = (p12[1], p12[0])
                                append_to.AppendSegment(type, p12, p,
                                                        path.Continuity(i - 1))
                            path = append_to
                        else:
                            path = append_to = path.Duplicate()
                        append_to.SetContinuity(-1, ContAngle)
                paths.append(path)
            undo = self.set_paths(paths)
        elif one == 0 and two == 1:
            undo_list = []
            for path in self.paths:
                if path.selection_count():
                    undo_list.append(path.ClosePath())
                else:
                    undo_list.append(None)
            undo = (self.object.do_undo, undo_list)
            self._changed(SHAPE)
        else:
            return
        return undo

    def CanSetContinuity(self, cont):
        """Return true if there is selected at least a node whose
        continuity is not cont.

        TODO: For now, setting continuity on nodes that belong to line
        segments doesn't produce meaningful results, so this is
        disabled.
        The current behaviour could be modified to make more sense, for
        example: when the continuity of a line segment is set to smooth
        or symmetrical then adjust the next bezier segment (if there is
        one) so that its first control handle becames aligned with the
        line segment and constrain it to remain that way.
        """
        for path in self.paths:
            SegmentType = path.SegmentType
            SegmentSelected = path.SegmentSelected
            Continuity = path.Continuity
            if path.selection_count():
                if path.closed:
                    if SegmentSelected(0) and SegmentType(1) == Bezier and \
                            SegmentType(-1) == Bezier and \
                            Continuity(0) != cont:
                        return True
                for i in range(1, path.len - 1):
                    if SegmentSelected(i) and SegmentType(i) == Bezier and \
                            SegmentType(i+1) == Bezier and \
                            Continuity(i) != cont:
                        return True
        return False 

    def SetContinuity(self, cont):
        new_paths = []
        for path in self.paths:
            if path.selection_count():
                new_paths.append(set_continuity(path, cont))
            else:
                new_paths.append(path)
        return self.set_paths(new_paths)

    def CanConvertSegments(self, segment_type):
        """Return true if there is at least a segment selected that
        is not segment_type (Bezier or Line).
        """
        if self.selection_type == SelCurvePoint:
            path = self.paths[self.selected_path]
            idx = int(self.selected_idx) + 1
            if path.SegmentType(idx) != segment_type:
                return True
        else:
            for path in self.paths:
                # check if at least two consecutive nodes are selected
                prev_sel = False
                if path.selection_count() > 1:
                    SegmentSelected = path.SegmentSelected
                    SegmentType = path.SegmentType
                    for i in range(path.len):
                        if SegmentSelected(i):
                            if prev_sel and SegmentType(i) != segment_type:
                                return True
                            else:
                                prev_sel = True
                        else:
                            prev_sel = False
        return False

    def SegmentsToLines(self):
        if self.selection_type == SelCurvePoint:
            new_paths = list(self.paths)
            path = new_paths[self.selected_path]
            new_paths[self.selected_path] = segment_to_line(path,
                                                            self.selected_idx)
        else:
            new_paths = []
            for path in self.paths:
                if path.selection_count() > 1:
                    new_paths.append(segments_to_lines(path))
                else:
                    new_paths.append(path)
        return self.set_paths(new_paths)

    def SegmentsToCurve(self):
        if self.selection_type == SelCurvePoint:
            new_paths = list(self.paths)
            path = new_paths[self.selected_path]
            new_paths[self.selected_path] = segment_to_curve(path,
                                                             self.selected_idx)
        else:
            new_paths = []
            for path in self.paths:
                if path.selection_count() > 1:
                    new_paths.append(segments_to_beziers(path))
                else:
                    new_paths.append(path)
        return self.set_paths(new_paths)

    def CanDeleteNodes(self):
        """Return true if there is at least one node selected.
        """
        for path in self.paths:
            if path.selection_count() > 0:
                return True
        return False

    def DeleteNodes(self):
        new_paths = []
        for path in self.paths:
            if path.selection_count() > 0:
                newpath = delete_segments(path)
            else:
                newpath = path
            if newpath.len > 1:
                new_paths.append(newpath)
            else:
                # all nodes of path have been deleted
                if __debug__:
                    pdebug('bezier', 'path removed')
        if new_paths:
            return self.set_paths(new_paths)
        else:
            if __debug__:
                pdebug('bezier', 'PolyBezier removed')
            return self.parent.Remove(self.object)

    def CanInsertNodes(self):
        """Return true if a node can be inserted.

        A node can be inserted when a point on a path was selected
        or when at least two consecutive nodes are selected.
        """
        if self.selection_type == SelCurvePoint:
            return True
        else:
            for path in self.paths:
                previous_selected = False
                if path.selection_count() > 1:
                    for i in range(path.len):
                        if path.SegmentSelected(i):
                            if previous_selected:
                                return True
                            else:
                                previous_selected = True
                        else:
                            previous_selected = False
        return False

    def InsertNodes(self):
        if self.selection_type == SelCurvePoint:
            new_paths = list(self.paths)
            path = new_paths[self.selected_path]
            new_paths[self.selected_path] = insert_node_at(path,
                                                           self.selected_idx)
            self.selected_idx = int(self.selected_idx) + 1
            self.selection_type = SelNodes
        else:
            new_paths = []
            for path in self.paths:
                if path.selection_count() > 1:
                    new_paths.append(insert_segments(path))
                else:
                    new_paths.append(path)
        return self.set_paths(new_paths)



def arrow_tip(path, index, arrow1, arrow2, width):
    if not path.closed and path.len > 1:
        if index == 0 and arrow1:
            type, controls, p3, cont = path.Segment(1)
            p = path.Node(0)
            if type == Bezier:
                p1, p2 = controls
                dir = p - p1
                if not abs(dir):
                    dir = p - p2
            else:
                dir = p - p3
            return arrow1.Tip(p, dir, width)
        elif index == path.len - 1 and arrow2:
            type, controls, p, cont = path.Segment(-1)
            p3 = path.Node(-2)
            if type == Bezier:
                p1, p2 = controls
                dir = p - p2
                if not abs(dir):
                    dir = p - p1
            else:
                dir = p - p3
            return arrow2.Tip(p, dir, width)
    return path.Node(index)

