# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch
from Sketch import Point, NullPoint, Trafo
from Sketch.Graphics import Protocols
from Sketch.Base.const import SelectSet

import const

# Class Draggable
#
# This class maintains some instance variables for a click and drag
# operation on a graphics object.
#
# The scenario is this: The user has selected a graphics object, say a
# straight line between the points A and B, for editing. As a hint for
# the user where to click, the application shows two inverted rectangles
# at the endpoints. These rectangles are called handles. The user clicks
# on one of the handles, and, with the mouse button still pressed, drags
# the mouse to the new location of the selected endpoint. As feedback to
# the user, the application shows a `rubber-band' line during the drag
# to indicate what the line would look like if the user released the
# button.
#
# Two aspects of this operation are handled by the classes Draggable and
# EditSelect: Keeping track of the start point, the current point, the
# amount dragged, drawing the object during the drag and, in the case of
# Selectable, which parts of the object the user selected.
#
# Keeping track of where the drag started and how far in which direction
# the user has moved the mouse so far, is important, because, in the
# above example the endpoint should be moved not simply to the point the
# user dragged to, but by the amount the user dragged.
#
# To make this a little clearer: the handle is usually a few pixels
# wide, so the user may not click exactly on the pixel the endpoint lies
# on, but some pixels away. In that case, releasing the button without
# moving the mouse would still move the endpoint which is not what the
# user expected.
#
# Using only the offset of the drag is even more important when the
# entire object is being moved. In the above example, clicking on the
# middle of the line should select the entire line, i.e. both endpoints,
# for the drag. During the drag and at the end of the drag we can't move
# one or both endpoints to the current location of the mouse pointer, we
# have to move both endpoints by the same offset.
#
# To achieve this, an instance of Draggable has the following instance
# variables:
#
#	dragging	True, while being dragged
#	drag_start	start point
#	drag_cur	current point
#	off		offset by which the pointer was moved,
#			i.e. drag_cur - drag_start
#	drawn		true, if the object is visible on the screen in its
#			dragged form (see Hide() and Show())
#
# These variables only have meaningful values during the drag, that is,
# between the calls to DragStart() and DragStop(), see below.
# drag_start, drag_cur and off are of type Point. (See the developer's guide)


class Draggable:

    drawn	= 0
    dragging	= 0
    drag_start	= NullPoint
    drag_cur	= NullPoint
    off		= NullPoint

    drag_mask = const.Button1Mask # XXX move this to some other class ?


    def __init__(self):
	# not needed here, but if some derived class wants to call the
	# base class constructor...
	pass

    def DragStart(self, p):
	# Start the drag at P. Initialize the instance variables. Set
	# dragging to true.
	# XXX: document the meaning of the return value
	self.drawn = 0		# the object is not visible yet
	self.dragging = 1
	self.drag_start = p
	self.drag_cur = p
	self.off = NullPoint
	return self.off

    def DragMove(self, p):
	# The pointer has moved to p. Compute the new offset.
	self.off = p - self.drag_start
	self.drag_cur = p


    def MouseMove(self, p, state):
	# XXX add documentation for this
	if state & self.drag_mask:
	    self.off = p - self.drag_start
	    self.drag_cur = p

    def DragStop(self, p):
	# The drag stopped at p. Update drag_cur and off for the last
	# time, and set dragging to false.
	self.dragging = 0
	self.off = p - self.drag_start
	self.drag_cur = p

    def DragCancel(self):
	self.dragging = 0

    # The rest of Draggable's methods deal with drawing the object in
    # `dragged' form (usually an outline) on the screen. The output
    # device is assumed to be set up in such a way that drawing the same
    # object twice removes it again (usually using GCxor). Currently,
    # this will be an instance of InvertingDevice (graphics.py)
    #
    # Show() and Hide() use this assumption and the instance variable
    # drawn, to make certain that the object is visible or invisible,
    # respectively. If drawn is false Show() calls DrawDragged() to draw
    # the object and then sets drawn to true. This way Show() may be
    # called multiple times by the canvas widget if it thinks the
    # outline of the object should be visible, without removing the
    # outline accidentally.
    #
    # DrawDragged(), which obviously has to be implemented by some
    # derived class, has to draw the outline of the object on the output
    # device, using drag_cur or off to compute coordinates. The internal
    # state of the object, for example the endpoints of lines, should
    # only be changed temporarily during DrawDragged; the state of the
    # object should only change if the drag is completed successfully.
    #
    # The boolean parameter PARTIALLY indicates whether the object has
    # to be drawn completely or if it is sufficient to draw only the
    # parts that are changed by the drag. For instance, if a vertex of a
    # polygon is dragged, it might suffice to draw the two edges sharing
    # this vertex. It is safe to ignore this parameter and always draw
    # the whole object. It is especially useful for complex objects like
    # polygons or poly beziers, where it improves performance and
    # reduces flickering on the screen
    #
    # Implementation Note: Show and Hide are the methods normally used
    # by the canvas to show or hide the object while dragging. An
    # exception is the RedrawMethod of the canvas object where
    # DrawDragged is called directly.

    def DrawDragged(self, device, partially):
	pass

    def Show(self, device, partially = 0):
	if not self.drawn:
	    self.DrawDragged(device, partially)
	self.drawn = 1

    def Hide(self, device, partially = 0):
	if self.drawn:
	    self.DrawDragged(device, partially)
	self.drawn = 0

#
# Class Selectable
#
# This class defines the interface and default implementation for
# objects that can be selected by the user with a mouse click.
#


class EditSelect:

    def SelectPoint(self, p, rect, device, mode = SelectSet):
	# Select (sub)object at P. If something is selected, return
	# true, false otherwise.
	return const.SelectNone

    def SelectHandle(self, handle, mode = SelectSet):
	return const.SelectNone

    def SelectRect(self, rect, mode = SelectSet):
	# select (sub-)object(s) in RECT
	return const.SelectNone

    def SelectNone(self):
        pass

    def HasSelection(self):
        return 0

    def GetHandles(self):
	# In edit mode, this method will be called to get a list of
	# handles. A handle should be shown at every `hot' spot of the
	# object (e.g. the nodes of a PolyBezier). Handles are described
	# by tuples which can be easily created by the functions in
	# handle.py
	return []



class SelectAndDrag(Draggable, EditSelect):

    def __init__(self):
	Draggable.__init__(self)
        # EditSelect dosn't have a constructor

    def CurrentInfoText(self):
        # return a string describing the current state of the object
        # during a drag
        return ''


class Creator(SelectAndDrag, Protocols):

    is_Creator = 1
    creation_text = 'Create Object'

    def __init__(self):
	SelectAndDrag.__init__(self)

    def EndCreation(self):
	# This method will be called when the object was being created
	# interactively using more than one click-drag-release cycle,
	# and the user has finished. This method is needed by the
	# PolyBezier primitive for instance.
	#
	# Return true if creation was successful, false otherwise.
	return 1

    def ContinueCreation(self):
	# called during interactive creation when the user releases the
	# mouse button. Return true, if the object may need another
	# click-drag-release cycle, false for objects that are always
	# complete after one cycle. (XXX the `true' return value is
	# interpreted in a special way, see the PolyBezier primitive)
	#
	# XXX: Should we distinguish more cases? A rectangle for example
	# is always complete after one click-drag-release cycle. A
	# PolyBezier object needs at least two cycles but accepts any
	# number of additional cycles. A polygon (with straight lines)
	# needs at least one. We might return a value that indicates
	# whether the user *must* supply additional points, whether it's
	# optional or whether the object is complete and the user
	# *cannot* add points.
	return None



class RectangularCreator(Creator):

    def ButtonDown(self, p, button, state):
	self.trafo = Trafo(1, 0, 0, 1, p.x, p.y)
	Creator.DragStart(self, p)

    def apply_constraint(self, p, state):
	if state & Sketch.Editor.ConstraintMask:
	    trafo = self.trafo
            w, h = p - self.drag_start
	    if w == 0:
		w = 0.00001
	    a = h / w
	    if a > 0:
		sign = 1
	    else:
		sign = -1
	    if abs(a) > 1.0:
		h = sign * w
	    else:
		w = sign * h
	    p = self.drag_start + Point(w, h)
	return p

    def MouseMove(self, p, state):
	p = self.apply_constraint(p, state)
	Creator.MouseMove(self, p, state)

    def ButtonUp(self, p, button, state):
	p = self.apply_constraint(p, state)
	Creator.DragStop(self, p)
	x, y = self.off
	self.trafo = Trafo(x, 0, 0, y, self.trafo.v1, self.trafo.v2)


class Editor(SelectAndDrag):

    is_Editor = 1

    EditedClass = None
    context_commands = ()

    def __init__(self, object):
	self.object = object

    def __getattr__(self, attr):
	return getattr(self.object, attr)

    def Destroy(self):
	# called by the edit mode selection when the editor it not
	# needed anymore.
	pass
