# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from types import TupleType, StringType

import Sketch
from Sketch.Base.const import SelectSet, SelectAdd, SelectSubtract, \
     SelectSubobjects

import const
from selrect import SelectionRectangle


#
# Tools
#



def selection_type(state):
    state = state & const.AllowedModifierMask
    if state  == const.AddSelectionMask:
        type = SelectAdd
    elif state == const.SubtractSelectionMask:
        type = SelectSubtract
    elif state == const.SubobjectSelectionMask:
        type = SelectSubobjects
    else:
        type = SelectSet
    return type


class ToolInstance:

    def __init__(self, editor):
        self.editor = editor
        self.document = editor.Document()
        self.current = None

        # The canvas we're interacting with.  Set by the ButtonPress and
        # ButtonClick methods.  The context parameter to PointerMotion
        # and ButtonRelease it must be the same as the one used by the
        # preceding ButtonPress.
        self.canvas = None

    def begin_edit_object(self, context, object, p, button, state,
                          hide_handles = 1):
        if hide_handles:
            context.hide_handles()
        self.current = object
        self.editor.set_correction(self.current.ButtonDown(p, button, state))
        self.Show(context.inverting_device())

    def end_edit_object(self, context, p, button, state):
        object = self.stop_edit(context)
        if object is not None:
            undo = object.ButtonUp(p, button, state)
            if undo is not None:
                self.document.AddUndo(undo)
        return object

    def stop_edit(self, context = None):
        if self.current is None:
            return None
        if self.canvas is not None:
            assert self.canvas is context
        if context is not None:
            self.Hide(context.inverting_device())
        object = self.current
        self.current = None
        context.show_handles()
        return object

    def DelayButtonPress(self):
        return 1

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        self.canvas = context

    def PointerMotion(self, context, p, snapped, state):
        # Check whether context is the same as given to ButtonPress.
        # However, only do this check when self.canvas was actually set.
        # It can happen that PointerMotion is called when ButtonPress
        # wasn't called before e.g. when switching tools while dragging.
        if self.canvas is not None:
            assert self.canvas is context

        # If an object is being drawn or edited pass the pointer motion
        # event to it while updating the screen properly.
        if self.current is not None:
            device = context.inverting_device()
            self.Hide(device, 1)
            self.current.MouseMove(snapped, state)
            self.Show(device, 1)

    def ButtonRelease(self, context, p, snapped, button, state):
        # Check whether context is the same as given to ButtonPress.
        # However, only do this check when self.canvas was actually set.
        # It can happen that ButtonRelease is called when ButtonPress
        # wasn't called before e.g. when switching tools while dragging.
        if self.canvas is not None:
            assert self.canvas is context

    def ButtonClick(self, context, p, snapped, button, state, handle = None):
        self.canvas = context

    def Cancel(self, context):
        self.stop_edit(context)
        #if self.current is not None:
        #    self.Hide(context.inverting_device())
        #    self.current = None

    def End(self):
        if self.canvas is not None:
            self.Hide(self.canvas.inverting_device())
        self.editor = None
        self.canvas = None

    def SelectionChanged(self):
        pass

    def DrawDragged(self, device, partially):
        if self.current is not None:
            self.current.DrawDragged(device, partially)

    def Show(self, device, partially = 0):
        if self.current is not None:
            self.current.Show(device, partially)

    def Hide(self, device, partially = 0):
        if self.current is not None:
            self.current.Hide(device, partially)

    def Handles(self):
        return self.editor.Selection().GetHandles()

    def Delete(self):
        self.editor.RemoveSelected()

    def InfoText(self):
        if self.current is not None:
            return self.current.CurrentInfoText()

    def Cursor(self, context, p, object):
        return None

    def StateInfo(self):
        return None

    def SetStateInfo(self, state):
        pass

    def ResetStateInfo(self):
        pass


class ToolInfo:

    def __init__(self, name, editor_tool, cursor = 'CurStd',
                 active_cursor = 0, icon = ''):
        self.name = name
        self.editor_tool = editor_tool
        self.cursor = cursor
        self.active_cursor = active_cursor
        if not icon:
            icon = self.name
        self.icon = icon

    def Title(self):
        return self.editor_tool.title

    def Instantiate(self, editor):
        return self.editor_tool(editor)

    def __cmp__(self, other):
        # This is perhaps a bit too much syntactic sugar: make it
        # possible to compare tools with their names given as strings.
        if isinstance(other, StringType):
            return cmp(self.name, other)
        return cmp(id(self), id(other))


class ZoomToolInstance(ToolInstance):

    title = ''"Zoom"

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonPress(self, context, p, snapped, button, state,
                                 handle = None)
        self.begin_edit_object(context, SelectionRectangle(p),
                               p, button, state)

    def ButtonRelease(self, context, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, context, p, snapped, button, state)
        object = self.end_edit_object(context, p, button, state)
        context.zoom_area(object.bounding_rect,
                          out = state & const.ZoomOutMask)

    def ButtonClick(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonClick(self, context, p, snapped, button, state,
                                 handle = None)
        context.zoom_point(p, out = state & const.ZoomOutMask)

ZoomTool = ToolInfo("ZoomTool", ZoomToolInstance, cursor = 'CurZoom')


class DrawToolInstance(ToolInstance):

    def __init__(self, editor, object_class):
        ToolInstance.__init__(self, editor)
        self.object_class = object_class

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonPress(self, context, p, snapped, button, state,
                                 handle = None)
        self.begin_edit_object(context, self.object_class(), snapped, button,
                               state)

    def ButtonRelease(self, context, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, context, p, snapped, button, state)
        object = self.end_edit_object(context, snapped, button, state)
        if object is not None and object.EndCreation():
            self.editor.Insert(object.CreatedObject())


class RectangleToolInstance(DrawToolInstance):

    title = ''"Rectangle"

    def __init__(self, editor):
        DrawToolInstance.__init__(self, editor, Sketch.Editor.RectangleCreator)

RectangleTool = ToolInfo("RectangleTool", RectangleToolInstance,
                         cursor = 'CurDraw', icon = 'CreateRect')

class EllipseToolInstance(DrawToolInstance):

    title = ''"Ellipse"

    def __init__(self, editor):
        DrawToolInstance.__init__(self, editor, Sketch.Editor.EllipseCreator)

EllipseTool = ToolInfo("EllipseTool", EllipseToolInstance, cursor = 'CurDraw',
                       icon = 'CreateEllipse')

class FreehandToolInstance(DrawToolInstance):

    title = ''"Freehand"

    def __init__(self, document):
        DrawToolInstance.__init__(self, document,
                                  Sketch.Editor.FreehandCreator)

    def ButtonRelease(self, context, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, context, p, snapped, button, state)
        object = self.end_edit_object(context, snapped, button, state)
        if object is not None and object.EndCreation():
            object = object.CreatedObject()
            pt_per_pixel = context.test_device().LengthToDoc(1.0)
            Sketch.Editor.smoothen_freehand_curve(object, pt_per_pixel)
            self.editor.Insert(object)


FreehandTool = ToolInfo("FreehandTool", FreehandToolInstance,
                        cursor = 'CurDraw', icon = 'CreateFreehand')


class PolyDrawToolInstance(ToolInstance):

    def __init__(self, editor, object_class):
        ToolInstance.__init__(self, editor)
        self.object_class = object_class

    def DelayButtonPress(self):
        return 0

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        ToolInstance.ButtonPress(self, context, p, snapped, button, state,
                                 handle = None)
        if button == const.Button1:
            if self.current is None:
                object = self.object_class()
            else:
                self.Hide(context.inverting_device())
                object = self.current
            self.begin_edit_object(context, object, snapped, button, state)

    def ButtonRelease(self, context, p, snapped, button, state):
        ToolInstance.ButtonRelease(self, context, p, snapped, button, state)
        if button == const.Button2:
            self.finish()
        elif button == const.Button1:
            object = self.end_edit_object(context, snapped, button, state)
            self.current = object
            self.Show(context.inverting_device())

    def ButtonClick(self, context, p, snapped, button, state, handle = None):
        self.ButtonPress(context, p, snapped, button, state, handle)
        self.ButtonRelease(context, p, snapped, button, state)

    def End(self):
        self.finish()
        ToolInstance.End(self)

    def finish(self):
        object = self.stop_edit(self.canvas)
        if object is not None and object.EndCreation():
            self.editor.Insert(object.CreatedObject())


class PolyBezierToolInstance(PolyDrawToolInstance):

    title = ''"Curves"

    def __init__(self, editor):
        PolyDrawToolInstance.__init__(self, editor,
                                      Sketch.Editor.PolyBezierCreator)

PolyBezierTool = ToolInfo("PolyBezierTool", PolyBezierToolInstance,
                          cursor = 'CurDraw', icon = 'CreateCurve')


class PolyLineToolInstance(PolyDrawToolInstance):

    title = ''"Polylines"

    def __init__(self, editor):
        PolyDrawToolInstance.__init__(self, editor,
                                      Sketch.Editor.PolyLineCreator)

PolyLineTool = ToolInfo("PolyLineTool", PolyLineToolInstance,
                        cursor = 'CurDraw', icon = 'CreatePoly')
