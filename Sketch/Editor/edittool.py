# Sketch - A Python-based interactive drawing program
# Copyright (C) 2004, 2005 by Bernhard Herzog
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2001, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import Sketch
from Sketch import Point, ContAngle, ContSmooth, ContSymmetrical, Bezier, \
     Line
from Sketch.Base.const import SelectDrag, SELECTION, HANDLES
from Sketch.Editor import AddEditorBuiltin, PolyBezierEditor
import const
import tools
from selectiontool import SelectionRectangle

class MultiEditor:

    def __init__(self):
        self.editors = []
        self.editormap = {}
        self.offsets = []
        self.drag = []
        self.active = 0
        self.undo = None

    def UpdateEditors(self, selection):
        self.editors = []
        oldmap = self.editormap
        self.editormap = {}
        GetEditor = Sketch.Editor.GetEditor
        for obj in selection.GetObjects():
            if oldmap.has_key(id(obj)):
                editor = oldmap[id(obj)]
            else:
                editor = GetEditor(obj)
            if editor is not None:
                self.editors.append(editor)
                self.editormap[id(obj)] = editor
        self.active = 0

    def ButtonDown(self, p, button, state):
        if not self.editors:
            return None
        offsets = []
        drag = []
        for editor in self.editors:
            if editor.HasSelection():
                drag.append(1)
                off = editor.ButtonDown(p, button, state)
            else:
                drag.append(0)
                off = None
            if off is None:
                off = Point(0, 0)
            offsets.append(off)
        off = offsets[self.active]
        for i in range(len(offsets)):
            offsets[i] = off - offsets[i]
        self.offsets = offsets
        self.drag = drag
        return off

    def MouseMove(self, p, state):
        drag = self.drag
        for i in range(len(self.editors)):
            if drag[i]:
                self.editors[i].MouseMove(p + self.offsets[i], state)

    def ButtonUp(self, p, button, state):
        undo = []
        drag = self.drag
        for i in range(len(self.editors)):
            if drag[i]:
                info = self.editors[i].ButtonUp(p + self.offsets[i], button,
                                                state)
                if info is not None:
                    undo.append(info)
        return Sketch.Base.CreateListUndo(undo)

    def Handles(self):
        handles = []
        for i in range(len(self.editors)):
            h = self.editors[i].GetHandles()
            for tmp in h:
                tmp.editor = i
            handles.extend(h)
        return handles

    def SelectHandle(self, handle, mode):
        self.active = handle.editor
        change = self.editors[self.active].SelectHandle(handle, mode)
        if change == const.SelectSingle:
            editors = self.editors[:]
            del editors[self.active]
            for editor in editors:
                editor.SelectNone()

    def SelectPoint(self, p, device, mode):
        rect = device.HitRectAroundPoint(p)
        point_selected = False
        for i, editor in enumerate(self.editors):
            if editor.SelectPoint(p, rect, device, mode):
                self.active = i
                point_selected = True
        return point_selected

    def SelectRect(self, rect, mode):
        for editor in self.editors:
            editor.SelectRect(rect, mode)

    def DrawDragged(self, device, partially):
        drag = self.drag
        for i in range(len(self.editors)):
            if drag[i]:
                self.editors[i].DrawDragged(device, partially)

    def Show(self, device, partially = 0):
        drag = self.drag
        for i in range(len(self.editors)):
            if drag[i]:
                self.editors[i].Show(device, partially)

    def Hide(self, device, partially = 0):
        drag = self.drag
        for i in range(len(self.editors)):
            if drag[i]:
                self.editors[i].Hide(device, partially)

    def CurrentInfoText(self):
        if self.editors:
            return self.editors[self.active].CurrentInfoText()
        else:
            return ""



class EditToolInstance(tools.ToolInstance):

    title = ''"Edit"

    def __init__(self, editor):
        tools.ToolInstance.__init__(self, editor)
        self.editor.Subscribe(SELECTION, self.selection_changed)
        self.multiedit = MultiEditor()
        self.get_editors()

    def End(self):
        self.editor.Unsubscribe(SELECTION, self.selection_changed)
        tools.ToolInstance.End(self)

    def selection_changed(self):
        self.get_editors()

    def get_editors(self):
        self.multiedit.UpdateEditors(self.editor.Selection())

    def ButtonPress(self, context, p, snapped, button, state, handle = None):
        tools.ToolInstance.ButtonPress(self, context, p, snapped, button,
                                       state, handle = handle)
        object = None
        hide_handles = 1
        if handle is not None:
            # The user pressed the button over a handle. Prepare to drag
            # at the handle
            self.multiedit.SelectHandle(handle, SelectDrag)
            object = self.multiedit
        else:
            test = context.test_device()
            test.StartOutlineMode()
            if self.editor.SelectionHit(p, test, test_all = 0) \
                    and self.multiedit.SelectPoint(p, test, SelectDrag):
                # The user pressed the button over an already selected object.
                # Prepare to edit/transform the current selection
                self.multiedit.SelectPoint(p, test, SelectDrag)
                object = self.multiedit
            else:
                pick = self.editor.PickActiveObject(test, p)
                if pick is not None and pick.is_GuideLine:
                    # edit guide line
                    object = Sketch.Editor.GuideEditor(pick)
            test.EndOutlineMode()

        # if no object has been found to be edited, start a
        # rubberbanding selection
        if object is None:
            object = SelectionRectangle(p)
            hide_handles = 0

        self.begin_edit_object(context, object, snapped, button, state,
                               hide_handles = hide_handles)

    def ButtonRelease(self, context, p, snapped, button, state):
        tools.ToolInstance.ButtonRelease(self, context, p, snapped, button,
                                         state)
        object = self.end_edit_object(context, snapped, button, state)
        if isinstance(object, SelectionRectangle):
            self.multiedit.SelectRect(object.bounding_rect,
                                      tools.selection_type(state))
            self.editor.update_handles()

    def ButtonClick(self, context, p, snapped, button, state, handle = None):
        tools.ToolInstance.ButtonClick(self, context, p, snapped, button,
                                       state, handle = handle)
        mode = tools.selection_type(state)
        if handle is not None:
            self.multiedit.SelectHandle(handle, mode)
            self.editor.update_handles()
        else:
            test = context.test_device()
            test.StartOutlineMode()
            hit = self.editor.SelectionHit(p, test, test_all = 0)
            test.EndOutlineMode()
            if hit:
                self.multiedit.SelectPoint(p, test, mode)
                self.editor.update_handles()
            else:
                self.editor.SelectPoint(p, test, mode)

    def CallObjectEditorMethod(self, aclass, methodname, *args):
        for objecteditor in self.multiedit.editors:
            if isinstance(objecteditor, aclass):
                self.document.BeginTransaction()
                obmethod = getattr(objecteditor, methodname)
                undo = obmethod(*args)
                if undo is not None:
                    self.document.AddUndo(undo)
                self.document.EndTransaction()
                self.editor.update_handles()

    def Handles(self):
        if self.editor.HasSelection():
            handles = self.multiedit.Handles()
        else:
            handles = []
        return handles

EditTool = tools.ToolInfo("EditTool", EditToolInstance, cursor = 'CurEdit',
                          active_cursor = 1)

# PolyBezier commands
def check_polybezier_method(context, methodname, *args):
    """Return true if EditTool is active, there is at least a PolyBezier
    selected and calling methodname on PolyBezierEditor returns True.
    """
    can = 0
    editor = context.editor
    tool = editor.Tool()
    if isinstance(tool, EditToolInstance) and context.tool == 'EditTool' and \
            editor.HasSelection():
        for objeditor in tool.multiedit.editors:
            if isinstance(objeditor, PolyBezierEditor):
                checkmethod = getattr(objeditor, methodname)
                if checkmethod(*args):
                    can = 1
                    break
    return can

def call_polybezier_method(context, method, *args):
    tool = context.editor.Tool()
    if isinstance(tool, EditToolInstance):
        tool.CallObjectEditorMethod(PolyBezierEditor, method, *args)

AddEditorBuiltin('cont_angle', ''"Angle",
                call_polybezier_method, ('SetContinuity', ContAngle),
                sensitive = lambda context: check_polybezier_method(context,
                                               'CanSetContinuity', ContAngle),
                channels = (HANDLES,))
AddEditorBuiltin('cont_smooth', ''"Smooth",
                call_polybezier_method, ('SetContinuity', ContSmooth),
                sensitive = lambda context: check_polybezier_method(context,
                                              'CanSetContinuity', ContSmooth),
                channels = (HANDLES,))
AddEditorBuiltin('cont_symmetrical', ''"Symmetrical",
                call_polybezier_method, ('SetContinuity', ContSymmetrical),
                sensitive = lambda context: check_polybezier_method(context,
                                         'CanSetContinuity', ContSymmetrical),
                channels = (HANDLES,))
AddEditorBuiltin('segments_to_lines', ''"Segments To Lines",
                 call_polybezier_method, ('SegmentsToLines',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                   'CanConvertSegments', Line),
                 channels = (HANDLES,))
AddEditorBuiltin('segments_to_curve', ''"Segments To Curves",
                 call_polybezier_method, ('SegmentsToCurve',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                 'CanConvertSegments', Bezier),
                 channels = (HANDLES,))
AddEditorBuiltin('select_all_nodes', ''"Select All Nodes",
                 call_polybezier_method, ('SelectAllNodes',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                          'CanSelectAllNodes'),
                 channels = (HANDLES,))
AddEditorBuiltin('delete_nodes', ''"Delete Nodes",
                 call_polybezier_method, ('DeleteNodes',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                             'CanDeleteNodes'),
                 channels = (HANDLES,))
AddEditorBuiltin('insert_nodes', ''"Insert Nodes",
                 call_polybezier_method, ('InsertNodes',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                             'CanInsertNodes'),
                 channels = (HANDLES,))
AddEditorBuiltin('close_nodes', ''"Close Nodes",
                 call_polybezier_method, ('CloseNodes',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                              'CanCloseNodes'),
                 channels = (HANDLES,))
AddEditorBuiltin('open_nodes', ''"Cut Curve",
                 call_polybezier_method, ('OpenNodes',),
                 sensitive = lambda context: check_polybezier_method(context,
                                                               'CanOpenNodes'),
                 channels = (HANDLES,))
