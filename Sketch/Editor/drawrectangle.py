# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch
from Sketch import _, Trafo
from Sketch.Graphics import Rectangle
from Sketch.Editor import RectangularCreator

class RectangleCreator(RectangularCreator):

    creation_text = _("Create Rectangle")

    state = 0
    
    def MouseMove(self, p, state):
        self.state = state
        RectangularCreator.MouseMove(self, p, state)
    
    def ButtonUp(self, p, button, state):
        if self.state & Sketch.Editor.AlternateMask:
            p = self.apply_constraint(p, state)
            self.DragStop(p)
            off = 2 * self.off
            end = self.trafo.offset() - self.off
            self.trafo = Trafo(off.x, 0, 0, off.y, end.x, end.y)
        else:
            RectangularCreator.ButtonUp(self, p, button, state)

    def DrawDragged(self, device, partially):
        start = self.drag_start
        if self.state & Sketch.Editor.AlternateMask:
            start = start - self.off
	device.DrawRectangle(start, self.drag_cur)

    def CurrentInfoText(self):
        start = self.drag_start
        if self.state & Sketch.Editor.AlternateMask:
            start = start - self.off
        width, height = self.drag_cur - start
        return 'Rectangle: %(size)[size]', {'size': (abs(width), abs(height))}

    def CreatedObject(self):
	return Rectangle(self.trafo)

