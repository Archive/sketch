# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from Sketch import Point, Trafo
from Sketch.Base.const import SELECTION
from Sketch.Editor import AddEditorBuiltin

#
# Builtin commands
#

def abut_horizontal(context):
    doc = context.document
    pos = []
    for obj in context.editor.SelectedObjects():
        rect = obj.coord_rect
        pos.append((rect.left, rect.top, rect.right - rect.left, obj))
    if pos:
        pos.sort()
        undo = []
        start, top, width, ob = pos[0]
        next = start + width
        for left, top, width, obj in pos[1:]:
            off = Point(next - left, 0)
            doc.AddUndo(obj.Translate(off))
            next = next + width

AddEditorBuiltin(abut_horizontal, ''"Abut Horizontal", channels=(SELECTION,),
              sensitive = lambda context: context.editor.CountSelected() > 1)

def abut_vertical(context):
    doc = context.document
    pos = []
    for obj in context.editor.SelectedObjects():
        rect = obj.coord_rect
        pos.append((rect.top, -rect.left, rect.top - rect.bottom, obj))
    if pos:
        pos.sort()
        pos.reverse()
        start, left, height, ob = pos[0]
        next = start - height
        for top, left, height, obj in pos[1:]:
            off = Point(0, next - top)
            doc.AddUndo(obj.Translate(off))
            next = next - height
AddEditorBuiltin(abut_vertical, ''"Abut Vertical", channels = (SELECTION,),
              sensitive = lambda context: context.editor.CountSelected() > 1)


def flip_selected(context, horizontal = 0, vertical = 0):
    selection = context.editor.Selection()
    if selection and (horizontal or vertical):
        rect = selection.coord_rect
        if horizontal:
            xoff = rect.left + rect.right
            factx = -1
        else:
            xoff = 0
            factx = 1
        if vertical:
            yoff = rect.top + rect.bottom
            facty = -1
        else:
            yoff = 0
            facty = 1
        trafo = Trafo(factx, 0, 0, facty, xoff, yoff)
        context.editor.TransformSelected(trafo)

AddEditorBuiltin('flip_horizontal', ''"Flip Horizontal", flip_selected,
                 args = (1, 0), channels = (SELECTION,),
                 sensitive = lambda context: context.editor.HasSelection())
AddEditorBuiltin('flip_vertical', ''"Flip Vertical", flip_selected,
                 args = (0, 1), channels = (SELECTION,),
                 sensitive = lambda context: context.editor.HasSelection())

def align_selected(context, side, ref_rect = None):
    selection = context.editor.Selection()
    objects = selection.GetObjects()
    AddUndo = context.document.AddUndo
    if selection:
        if ref_rect is None:
            ref_rect = selection.last_coord_rect
        for obj in objects:
            r = obj.coord_rect
            xoff = yoff = 0
            if side == 'left':
                xoff = ref_rect.left - r.left
            elif side == 'center_x':
                xoff = ref_rect.center().x - r.center().x
            elif side == 'right':
                xoff = ref_rect.right - r.right
            elif side == 'top':
                yoff = ref_rect.top - r.top
            elif side == 'center_y':
                yoff = ref_rect.center().y - r.center().y
            elif side == 'bottom':
                yoff = ref_rect.bottom - r.bottom
            elif side == 'center':
                xoff = ref_rect.center().x - r.center().x
                yoff = ref_rect.center().y - r.center().y

            if xoff or yoff:
                AddUndo(obj.Translate(Point(xoff, yoff)))

sides = ('left', 'top', 'right', 'bottom', 'center_x', 'center_y', 'center')
for side in sides:
    AddEditorBuiltin("align_%s" % side, ''"Align Objects",
                     align_selected,
                     args = (side,), channels = (SELECTION,),
                     sensitive = lambda context: context.editor.HasSelection())
