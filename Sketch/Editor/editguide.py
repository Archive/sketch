# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch
from Sketch import _, Point
from Sketch.Graphics import GuideLine

from Sketch.Editor import Editor


class GuideEditor(Editor):

    EditedClass = GuideLine

    selection = -1

    def ButtonDown(self, p, button, state):
	self.DragStart(p)
        if self.horizontal:
            result = Point(0, p.y - self.point.y)
        else:
            result = Point(p.x - self.point.x, 0)
        return result

    def ButtonUp(self, p, button, state):
	self.DragStop(p)
        self.document.MoveGuideLine(self.object, p)

    def DrawDragged(self, device, partially):
	device.DrawGuideLine(self.drag_cur, self.horizontal)

    def CurrentInfoText(self):
        if self.horizontal:
            text = _("Horizontal Guide Line at %(coord)[length]")
            dict = {'coord': self.drag_cur.y}
        else:
            text = _("Vertical Guide Line at %(coord)[length]")
            dict = {'coord': self.drag_cur.x}
        return text, dict

