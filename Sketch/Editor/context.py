# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch

from Sketch.Editor import AddBuiltin

from Sketch.Base.const import CHANGED, SELECTION, UNDO, TOOL, CONFIG,\
     LAYER, LAYER_ORDER, LAYER_ACTIVE, STATE, STYLE, OBJECT_CHANGED, \
     TRANSACTION_START, TRANSACTION_END, HANDLES

main_tools = ('SelectionTool', 'EditTool')

class Context(Sketch.Base.Publisher):

    editor_channels = (SELECTION, STATE, HANDLES, TOOL)
    document_channels = (UNDO, STYLE, LAYER, LAYER_ORDER, LAYER_ACTIVE,
                         OBJECT_CHANGED, TRANSACTION_START, TRANSACTION_END)

    def __init__(self, application):
        self.application = application
        self.document = None
        self.editor = None
        self.main_tool = 'SelectionTool'
        Sketch.Base.preferences.Subscribe(CHANGED, self.forward, CONFIG)
        Sketch.Graphics.preferences.Subscribe(CHANGED, self.forward, CONFIG)
        Sketch.Editor.preferences.Subscribe(CHANGED, self.forward, CONFIG)

    def __getattr__(self, attr):
        """Implement the tool attribute.

        The tool attribute is None when self.editor is None, otherwise
        it's the name of the current tool of self.editor.
        """
        if attr == "tool":
            if self.editor is not None:
                return self.editor.toolname
            return None
        else:
            raise AttributeError(attr)

    def set_editor(self, editor):
        if editor is not self.editor:
            self._unsubscribe()
            if editor is None:
                self.editor = None
                self.document = None
            else:
                self.editor = editor
                self.document = editor.Document()
            self._subscribe()
            self.issue(CHANGED)
            self.issue(TOOL, self.tool)

    def _subscribe(self):
        forward = self.forward
        if self.document is not None:
            self.document.IncRef()
            for channel in self.document_channels:
                self.document.Subscribe(channel, forward, channel)
        if self.editor is not None:
            for channel in self.editor_channels:
                self.editor.Subscribe(channel, forward, channel)

    def _unsubscribe(self):
        forward = self.forward
        if self.editor is not None:
            for channel in self.editor_channels:
                self.editor.Unsubscribe(channel, forward, channel)
        if self.document is not None:
            for channel in self.document_channels:
                self.document.Unsubscribe(channel, forward, channel)
            self.document.DecRef()

    def forward(self, *args):
        #print 'forward', args
        if len(args) > 1:
            args = (args[-1],) + args[:-1]
        apply(self.issue, args)

    def HasEditor(self):
        return self.editor is not None

    def SetTool(self, tool):
        if tool in main_tools:
            self.main_tool = tool
        if self.editor is not None:
            self.editor.SetTool(tool)

    def MainTool(self):
        return self.main_tool


def switch_tool(context):
    if context.tool in main_tools:
        if context.tool == 'SelectionTool':
            tool = 'EditTool'
        elif context.tool == 'EditTool':
            tool = 'SelectionTool'
        else:
            # XXX shouldn't happen
            tool = 'SelectionTool'
    else:
        tool = context.MainTool()
    context.SetTool(tool)

def set_tool(context, tool):
    context.SetTool(tool)

AddBuiltin(switch_tool, ''"Switch Tool")

AddBuiltin('edit_tool', ''"Edit Tool", set_tool, args = 'EditTool')
AddBuiltin('align_tool', ''"Align Tool", set_tool, args = 'AlignTool')
AddBuiltin('selection_tool', ''"Selection Tool", set_tool,
           args = 'SelectionTool')
AddBuiltin('zoom_tool', ''"Zoom Tool", set_tool, args = 'ZoomTool')
AddBuiltin('ellipse_tool', ''"Ellipse Tool", set_tool, args = 'CreateEllipse')
AddBuiltin('rectangle_tool', ''"Rectangle Tool", set_tool, args = 'CreateRect')
AddBuiltin('curve_tool', ''"Curve Tool", set_tool, args = 'CreateCurve')
AddBuiltin('line_tool', ''"Line Tool", set_tool, args = 'CreatePoly')
AddBuiltin('freehand_tool', ''"Freehand Tool", set_tool,
           args = 'CreateFreehand')
AddBuiltin('text_tool', ''"Text Tool", set_tool, args = 'TextTool')

