# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import os, sys, string

_pkgdir = __path__[0]
_parentdir = os.path.join(_pkgdir, '..')
SketchVersion = string.strip(open(os.path.join(_pkgdir, 'VERSION')).read())
for _dir in ('Modules',):
    __path__.insert(0, os.path.join(_pkgdir, _dir))

for dir in ('Lib', 'Filter'):
    dir = os.path.join(_parentdir, dir)
    if os.path.isdir(dir):
        sys.path.insert(1, dir)

import Base.warn

message_dir = os.path.join(_parentdir, 'Resources/Messages')
try:
    #from intl import gettext, dgettext, bindtextdomain
    #import intl
    import gettext
    intl = gettext
    from gettext import gettext, dgettext, bindtextdomain
    Base.warn.pdebug("i18n", 'gettext imported')
    bindtextdomain("skencil", message_dir)
    intl.textdomain("skencil")
except ImportError:
    Base.warn.pdebug("i18n", 'gettext not imported')
    def gettext(text):
        return text
    def dgettext(domain, text):
        return text
    def bindtextdomain(*args):
        pass
_ = gettext
def N_(text):
    return text


import _sketch

from _sketch import Point, Polar, PointType
NullPoint = Point(0, 0)

from _sketch import Rect, PointsToRect, UnionRects, IntersectRects, \
     EmptyRect, InfinityRect, RectType
UnitRect = Rect(0, 0, 1, 1)

from _sketch import Trafo, Scale, Translation, Rotation, SingularMatrix, \
     TrafoType
Identity = Trafo(1, 0, 0, 1, 0, 0)
IdentityMatrix = Identity.matrix()


from _sketch import CreatePath, RectanglePath, RoundedRectanglePath, \
     approx_arc, CreateFontMetric, SKCache, TransformRectangle

from _sketch import ContAngle, ContSmooth, ContSymmetrical, \
     SelNone, SelNodes, SelSegmentFirst, SelSegmentLast, Bezier, Line


# Connector interface for global messages, i.e. messages apparently sent
# by None
def Issue(*args, **kw):
    apply(Base.connector.Issue, (None,) + args, kw)
Subscribe = Base.connector.Subscribe
def Unsubscribe(channel, function, *args):
    """Disconnect function and args from channel channel of None"""
    Base.connector.Disconnect(None, channel, function, args)


import Base.config
Base.config.init_directories(_parentdir)


#

_graphics_import = ["Arrow", "Style", "EmptyFillStyle",
                    "EmptyLineStyle", "PropertyStack",
                    "EmptyProperties", "Protocols", "MismatchError",
                    "Blend", "BlendTrafo", "BlendGroup",
                    "CreateBlendGroup", "BlendInterpolation",
                    "CreateRGBColor", "XRGBColor", "CreateCMYKColor",
                    "StandardColors", "Compound", "EditableCompound",
                    "Document", "GetFont", "MultiGradient",
                    "Group", "GuideLine",
                    "Image", "load_image", "ImageData", "Layer",
                    "GuideLayer", "GridLayer", "MaskGroup",
                    "EmptyPattern", "SolidPattern", "HatchingPattern",
                    "LinearGradient", "RadialGradient",
                    "ConicalGradient", "ImageTilePattern",
                    "PluginCompound", "TrafoPlugin", "Rectangle",
                    "Ellipse", "PolyBezier", "CombineBeziers",
                    "PostScriptDevice", "SimpleText", "PathText",
                    "InternalPathText",

                    ("arrow", "StandardArrows"),
                    ("dashes", "StandardDashes"),
                    ("properties", "FillStyle", "LineStyle"),
                    ("gradient", "CreateSimpleGradient"),
                    ]
_base_import = ["SketchError", "SketchLoadError"]


import Graphics
def compatibility_bindings():
    dict = globals()
    from types import TupleType
    for pkg, names in (("Graphics", _graphics_import), ("Base", _base_import)):
        for name in names:
            if type(name) == TupleType:
                items = list(name[1:])
                module = __import__(pkg + '.' + name[0], dict, {}, items)
                for item in items:
                    dict[item] = getattr(module, item)
            else:
                dict[name] = getattr(dict[pkg], name)
    for name in ("const", "warn"):
        dict[name] = __import__("Base." + name, dict, {}, [''])
        sys.modules["Sketch." + name] = dict[name]
    for name in ("plugins", "load"):
        dict[name] = __import__("Plugin." + name, dict, {}, [''])
        sys.modules["Sketch." + name] = dict[name]
compatibility_bindings()

