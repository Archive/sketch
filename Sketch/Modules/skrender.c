/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1999, 2000, 2001, 2002 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA
 */

#include <math.h>
#include <Python.h>
#include <stdio.h>

#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_render_aa.h>
#include <libart_lgpl/art_alphagamma.h>

#include "skrender.h"

/*
 *	Low-level rendering functions for the libart-based renderer
 */

/*
 *	General notes
 *
 * Pixbuf and Coordinates
 *
 * All rendering is done on an SKPixbuf. Coordinates in a pixbuf are
 * mostly the usual system for 2D raster graphics with the origin in the
 * top-left and x increasing to the right and y increasing downwards
 * (note that this is different from the system normally used in
 * Sketch). In the code here the top-left corner is not the origin,
 * though. Most functions have arguments called x0, y0, x1, y1 which
 * give the actual coordinates assumed for the top-left corner (x0, y0)
 * and the bottom right corner (x1, y1). These values always have the
 * property that x1 - x0 is the width and y1 - y0 the height of the
 * pixbuf in pixels.
 *
 * This system is covenient when rendering only part of a window, in
 * which case the pixbuf doesn't have to cover the whole window, or to
 * avoid recalculations of SVPs when scrolling.
 *
 * In the comments for the individual functions this coordinate system
 * will be referred to as the virtual pixel coordinates.
 *
 * When rendering raster images we have another coordinate system, that
 * of the source image. The source coordinates are always the natural
 * coordinates of the source pixbug ( (0, 0) is actuall the upper left
 * corner). This system is called the source coordinate system here.
 *
 *
 * Libart and SVPs
 *
 * Not all of the functions here actually use libart but all render into
 * a pixbuf and are used by the _libart module. The functions that
 * actually use libart are the ones that render into regions defined by
 * SVPs
 *
 * Libart's SVP rendering function art_svp_render_aa takes a callback as
 * parameter that is called for each scan-line to do the actual
 * rendering work. Libart allows to pass a pointer through to this
 * callback with application specific data.
 *
 * Here the callback directly called by art_svp_render_aa is
 * skrender_svp_alpha_opaque_callback which assume that the custom
 * pointer is a pointer to a SKRenderAlphaData struct which among other
 * things contains two function pointers one to render a run of opaque
 * pixels and the other to render a run of pixels with a constant alpha.
 * The alpha comes from the SVP and opaque only means that unless the
 * color to be rendered has some other alpha as well (as in rendering an
 * RGBA image) the pixels can just be copied to the target pixels.
 */

/*
 * Typedefs for SKRenderAlphaData function pointers
 */

/* Render the N RGB pixels in buf32 starting at X. Y is the y coordinate
 * of the scan-line to fill and is usually not needed. ALPHA is the
 * overall alpha value from the SVP for this run of pixels. DATA is the
 * custom data pointer of the SKRenderAlphaData struct.
 */
typedef void (*SKRenderFillAlphaFunc)(art_u32 *buf32, int n, int x, int y,
				      int alpha, void * data);

/* Same as SKRenderFillAlphaFunc, but render the interior of the SVP,
 * hence no ALPHA parameter.
 */
typedef void (*SKRenderFillOpaqueFunc)(art_u32 *buf32, int n, int x, int y,
				       void *data);


/* The custom struct to pass through art_svp_render_aa to the callbacks.
 */
typedef struct _SKRenderAlphaData SKRenderAlphaData;
struct _SKRenderAlphaData {

    /* The SKPixbuf to render to */
    SKPixbuf * pixbuf;

    /* The virtual pixel coordinates of the pixbuf */
    int x0, x1, y0, y1;

    /* custom data to pass through to the fill_opaque and fill_alpha */
    void * data;

    /* Table to map the SVPs alpha to the actual alpha when rendering. */
    int alphatab[256];

    /* Functions to actuall render pixels. More details above */
    SKRenderFillOpaqueFunc fill_opaque;
    SKRenderFillAlphaFunc fill_alpha;
};

/* Struct to pass through to the functions that render uniform RGB
 * values */
typedef struct {
    art_u8 r, g, b, alpha;
} SKRenderRGBData;

/* Struct to pass through to the functions that render scaled images */
typedef struct {
    /* The source image */
    SKPixbuf * source;

    /* The destination region in virtual pixel coordinates */
    int startx, starty, width, height;

    /* unused */
    int x0, y0;

    /* arrays to map destination coordinates to source coordinates. e.g.
     * when rendering line y origy[y - starty] is the y coordinate of
     * the source line (provided y >= starty && y < starty + height)
     */
    int *origx, *origy;
} SKRenderScaleImageData;


/* Struct to pass through to the functions that render transformed
 * images */
typedef struct {
    /* The source image */
    SKPixbuf * source;

    /* destination region */
    int startx, starty, width, height;

    /* unused */
    int x0, y0;

    /* minumum and maximum x destination coordinate relative to startx
     * for each scanline. I.e. for scanline y with y >= starty && y <
     * starty + height, minx[y - starty] is the minimum x coordinate of
     * the transformed image */
    int *minx, *maxx;

    /* whether a scanline is drawn on at all (similar to minx and miny) */
    int *visible;

    /* Affine transformation of destination coordinates to source
     * coordinates */
    SKTrafoObject * trafo;
} SKRenderTransformImageData;

/*
 * Blending definitions
 */

#define BLEND_SRC_ALPHA(src, src_alpha, dest) \
    dest + (((src - dest) * src_alpha) / 255)

/*
 *  Helper functions
 */

/* Fill the alpha table ALPHATAB for the alpha value ALPHA.
 *
 * The alpha table is an array of 256 ints in the range 0..255 mapping
 * the alpha values from the SVP to the alpha value to use when
 * rendering with an additional opacity of ALPHA.
 */
static void
fill_alphatab(int * alphatab, int alpha)
{
    int a = 0x8000, i;
    int da = (alpha * 66051 + 0x80) >> 8; /* 66051 equals 2 ^ 32 / (255*255) */

    for (i = 0; i < 256; i++)
    {
	alphatab[i] = a >> 16;
	a += da;
    }
}

/* Convert a color from the big-endian art_u32 format (red in the most
 * significant bits) to the machine specific format where red is in the
 * byte written first
 */
static art_u32
convert_color(art_u32 rgba)
{
    union {
	art_u32 u32;
	struct {
	    art_u8 r;
	    art_u8 g;
	    art_u8 b;
	    art_u8 a;
	}
	rgb;
    } color;

    color.rgb.r = rgba >> 24;
    color.rgb.g = (rgba >> 16) & 0xff;
    color.rgb.b = (rgba >> 8) & 0xff;
    color.rgb.a = rgba & 0xff;

    return color.u32;
}


/*
 * Render uniform RGB values
 */

/* Fill the N pixels starting at index X in the scanline BUF32. Assume
 * DATA is a pointer to SKRenderRGBData containing the color to render.
 * Set the alpha channel of the result to 255.
 */
static void
skrender_rgb_fill_run(art_u32 *buf32, int n, int x, int y, void * data)
{
    int i;
    union {
	art_u32 u32;
	struct {
	    art_u8 r;
	    art_u8 g;
	    art_u8 b;
	    art_u8 a;
	}
	rgb;
    } color;

    color.rgb.r = ((SKRenderRGBData*)data)->r;
    color.rgb.g = ((SKRenderRGBData*)data)->g;
    color.rgb.b = ((SKRenderRGBData*)data)->b;
    color.rgb.a = 255;

    for (i = 0; i < n; i++)
    {
	*buf32++ = color.u32;
    }
}


/* Fill the N pixels starting at index X in the scanline BUF32 with
 * opacity ALPHA. Assume DATA is a pointer to SKRenderRGBData containing
 * the color to render. Use 255 as the opacity of the color.
 */
static void
skrender_rgb_run_alpha(art_u32 *buf32, int n, int x, int y, int alpha,
		       void * data)
{
    int i;
    int v, r, g, b;
    art_u8 * buf = (art_u8*)buf32;

    r = ((SKRenderRGBData*)data)->r;
    g = ((SKRenderRGBData*)data)->g;
    b = ((SKRenderRGBData*)data)->b;

    for (i = 0; i < n; i++)
    {
	v = *buf;
	*buf++ = v + (((r - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((g - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((b - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((255 - v) * alpha + 0x80) >> 8);
    }
}



/*
 *	Scale Image
 */


/* Fill the N pixels starting at index X in the scanline BUF32 with with
 * pixels from a scaled image. Assume that DATA is a pointer to *
 * SKRenderScaleImageData.
 */
static void
skrender_scale_image_fill_run(art_u32 *buf32, int n, int x, int y, void * data)
{
    SKRenderScaleImageData* imgdata = data;
    art_u32 * src;
    int startx = imgdata->startx, starty = imgdata->starty;
    int * origx = imgdata->origx;
    int maxx, i;

    if (y < starty || y >= starty + imgdata->height)
	return;
    src = imgdata->source->data32[imgdata->origy[y - starty]];

    if (x < startx)
    {
	n = n - startx + x;
	buf32 = buf32 + startx - x;
	x = startx;
	if (n <= 0)
	    return;
    }
    else if (x >= startx + imgdata->width)
	return;
    if (x + n > startx + imgdata->width)
	n = startx + imgdata->width - x;

    maxx = x + n - startx;
    for (i = x - startx; i < maxx; i++)
    {
	*buf32++ = src[origx[i]];
    }
}


/* Fill the N pixels starting at index X in the scanline BUF32 with with
 * pixels from a scaled image with a constant opacity of ALPHA. Assume
 * that DATA is a pointer to * SKRenderScaleImageData.
 */
static void
skrender_scale_image_run_alpha(art_u32 *buf32, int n, int x, int y, int alpha,
			       void * data)
{
    SKRenderScaleImageData* imgdata = data;
    art_u32 * src;
    int startx = imgdata->startx, starty = imgdata->starty;
    int * origx = imgdata->origx;
    int v;
    art_u8 * buf;
    art_u8 * srcbytes;
    int maxx, i;

    if (y < starty || y >= starty + imgdata->height)
	return;
    src = imgdata->source->data32[imgdata->origy[y - starty]];

    if (x < startx)
    {
	n = n - startx + x;
	buf32 = buf32 + startx - x;
	x = startx;
	if (n <= 0)
	    return;
    }
    else if (x >= startx + imgdata->width)
	return;
    if (x + n > startx + imgdata->width)
	n = startx + imgdata->width - x;

    buf = (art_u8*)buf32;

    maxx = x + n - startx;
    for (i = x - startx; i < maxx; i++)
    {
	srcbytes = (art_u8*)(src + origx[i]);
	v = *buf;
	*buf++ = v + (((srcbytes[0] - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((srcbytes[1] - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((srcbytes[2] - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((255 - v) * alpha + 0x80) >> 8);
    }
}


/* Fill the N pixels starting at index X in the scanline BUF32 with with
 * pixels from a scaled image with per-pixel alpha values as well as a
 * constant opacity of ALPHA. Assume that DATA is a pointer to
 * SKRenderScaleImageData.
 */
static void
skrender_scale_image_run_alpha_trans(art_u32 *buf32, int n, int x, int y,
				     int alpha, void * data)
{
    SKRenderScaleImageData* imgdata = data;
    art_u32 * src;
    int startx = imgdata->startx, starty = imgdata->starty;
    int * origx = imgdata->origx;
    int v;
    art_u8 * buf;
    art_u8 * srcbytes;
    int maxx, i;
    int combined; /* Added for transparency. */
    int dest_alpha;

    if (y < starty || y >= starty + imgdata->height)
	return;
    src = imgdata->source->data32[imgdata->origy[y - starty]];

    if (x < startx)
    {
	n = n - startx + x;
	buf32 = buf32 + startx - x;
	x = startx;
	if (n <= 0)
	    return;
    }
    else if (x >= startx + imgdata->width)
	return;
    if (x + n > startx + imgdata->width)
	n = startx + imgdata->width - x;

    buf = (art_u8*)buf32;

    maxx = x + n - startx;
    for (i = x - startx; i < maxx; i++)
    {
	srcbytes = (art_u8*)(src + origx[i]);

	/* Combine the per-pixel image alpha with the constant alpha
	 * passed to this function.
	 */
	combined = (srcbytes[3] * alpha) / 255;

	/* Blend the components of the pixel from the image with the
	 * components of the pixel on the canvas using the blend
	 * function
	 *
	 * result = src_alpha * src_component
	 *          + (1 - src_alpha) * dest_component.
	 *
	 * rather than
	 *
	 * result = src_alpha * src_component
	 *          + (1 - src_alpha) * (dest_alpha * dest_component).
	 */

	dest_alpha = buf[3];

	/* Use division by 255 since 255 is equivalent to 1.0 (opaque).
	 * Using a system where 256 is equivalent to 1.0 would allow us
	 * to use bit shifting to optimise division by 256.
	 */
	v = *buf;
	*buf++ = BLEND_SRC_ALPHA(srcbytes[0], combined, v);
	v = *buf;
	*buf++ = BLEND_SRC_ALPHA(srcbytes[1], combined, v);
	v = *buf;
	*buf++ = BLEND_SRC_ALPHA(srcbytes[2], combined, v);

	/* Just overwrite the alpha value of the canvas with a value
	 * corresponding to full opacity.
	 */
	*buf++ = 255;
    }
}


/*
 *	Transform Image
 */

/* Fill the N pixels starting at index X in the scanline BUF32 with with
 * pixels from a transformed image. Assume that DATA is a pointer to *
 * SKRenderTransformImageData.
 */
static void
skrender_transform_image_fill_run(art_u32 *buf32, int n, int x, int y,
				  void * data)
{
    SKRenderTransformImageData * imgdata = data;
    art_u32 ** src = imgdata->source->data32;
    int starty = imgdata->starty;
    int minx, maxx, start = x;
    SKTrafoObject * trafo = imgdata->trafo;
    double sx, sy;
    int i;

    if (y < starty || y >= starty + imgdata->height
	|| !imgdata->visible[y - starty])
	return;

    minx = imgdata->minx[y - starty];
    maxx = imgdata->maxx[y - starty];

    if (x > maxx)
	return;

    if (x < minx)
    {
	n -= minx - x;
	if (n <= 0)
	    return;
	buf32 += minx - x;
	start = minx;
    }

    if (start + n > maxx + 1)
	n = maxx - start + 1;

    sx = trafo->m11 * start + trafo->m12 * y + trafo->v1;
    sy = trafo->m21 * start + trafo->m22 * y + trafo->v2;

    for (i = 0; i < n ; i++, buf32++, sx += trafo->m11, sy += trafo->m21)
    {
	*buf32 = src[(int)sy][(int)sx];
    }
}

/* Fill the N pixels starting at index X in the scanline BUF32 with with
 * pixels from a transformed image and constant opacity ALPHA. Assume
 * that DATA is a pointer to * SKRenderTransformImageData.
 */
static void
skrender_transform_image_run_alpha(art_u32 *buf32, int n, int x, int y,
				   int alpha, void * data)
{
    SKRenderTransformImageData * imgdata = data;
    art_u32 ** src = imgdata->source->data32;
    int starty = imgdata->starty;
    int minx, maxx, start = x;
    SKTrafoObject * trafo = imgdata->trafo;
    double sx, sy;
    int v;
    art_u8 * buf;
    art_u8 * srcbytes;
    int i;

    if (y < starty || y >= starty + imgdata->height ||
	!imgdata->visible[y - starty])
	return;

    minx = imgdata->minx[y - starty];
    maxx = imgdata->maxx[y - starty];

    if (x > maxx)
	return;

    if (x < minx)
    {
	n -= minx - x;
	if (n <= 0)
	    return;
	buf32 += minx - x;
	start = minx;
    }

    if (start + n > maxx + 1)
	n = maxx - start + 1;

    buf = (art_u8*)buf32;

    sx = trafo->m11 * start + trafo->m12 * y + trafo->v1;
    sy = trafo->m21 * start + trafo->m22 * y + trafo->v2;

    for (i = 0; i < n; i++, sx += trafo->m11, sy += trafo->m21)
    {
	srcbytes = (art_u8*)(src[(int)sy] + (int)sx);
	v = *buf;
	*buf++ = v + (((srcbytes[0] - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((srcbytes[1] - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((srcbytes[2] - v) * alpha + 0x80) >> 8);
	v = *buf;
	*buf++ = v + (((255 - v) * alpha + 0x80) >> 8);
    }
}


/* Fill the N pixels starting at index X in the scanline BUF32 with with
 * pixels from a transformed image with per-pixel alpha as well as a
 * constant opacity ALPHA. Assume that DATA is a pointer to
 * SKRenderTransformImageData.
 */
static void
skrender_transform_image_run_alpha_trans(art_u32 *buf32, int n, int x, int y,
					 int alpha, void * data)
{
    SKRenderTransformImageData * imgdata = data;
    art_u32 ** src = imgdata->source->data32;
    int starty = imgdata->starty;
    int minx, maxx, start = x;
    SKTrafoObject * trafo = imgdata->trafo;
    double sx, sy;
    int v;
    art_u8 * buf;
    art_u8 * srcbytes;
    int i;
    int combined;
    int dest_alpha;

    if (y < starty || y >= starty + imgdata->height ||
	!imgdata->visible[y - starty])
	return;

    minx = imgdata->minx[y - starty];
    maxx = imgdata->maxx[y - starty];

    if (x > maxx)
	return;

    if (x < minx)
    {
	n -= minx - x;
	if (n <= 0)
	    return;
	buf32 += minx - x;
	start = minx;
    }

    if (start + n > maxx + 1)
	n = maxx - start + 1;

    buf = (art_u8*)buf32;

    sx = trafo->m11 * start + trafo->m12 * y + trafo->v1;
    sy = trafo->m21 * start + trafo->m22 * y + trafo->v2;

    for (i = 0; i < n; i++, sx += trafo->m11, sy += trafo->m21)
    {
	srcbytes = (art_u8*)(src[(int)sy] + (int)sx);

	/* Combine the per-pixel image alpha with the constant alpha
	 * passed to this function. Ignore the destination alpha.
	 */
	combined = (srcbytes[3] * alpha) / 255;

	/* Blend the components of the pixel from the image with the
	 * components of the pixel on the canvas using the blend
	 * function
	 *
	 *  result = src_alpha * src_component
	 *         + (1 - src_alpha) * dest_component.
	 */

	dest_alpha = buf[3];

	/* Use division by 255 since 255 is equivalent to 1.0 (opaque).
	 * Using a system where 256 is equivalent to 1.0 would allow us
	 * to use bit shifting to optimise division by 256.
	 */

	v = *buf;
	*buf++ = BLEND_SRC_ALPHA(srcbytes[0], combined, v);
	v = *buf;
	*buf++ = BLEND_SRC_ALPHA(srcbytes[1], combined, v);
	v = *buf;
	*buf++ = BLEND_SRC_ALPHA(srcbytes[2], combined, v);

	/* Just overwrite the alpha value of the canvas with a value
	 * corresponding to full opacity.
	 */
	*buf++ = 255;
    }
}



/*
 *	art_svp_render_aa callback
 */


/* Render the scanline with y-coordinate Y. CALLBACK_DATA must be a
 * pointer to a SKRenderAlphaData. START, STEPS, and N_STEPS define the
 * opacities for the individual pixels as described in the libart
 * documentation for art_svp_render_aa.
 */
static void
skrender_svp_aa_callback(void *callback_data, int y, int start,
			 ArtSVPRenderAAStep *steps, int n_steps)
{
    SKRenderAlphaData *data = callback_data;
    art_u32 *linebuf;
    int run_x0, run_x1;
    int running_sum = start;
    int x0, x1;
    int k;
    int *alphatab;
    int alpha;

    linebuf = data->pixbuf->data32[y - data->y0];
    x0 = data->x0;
    x1 = data->x1;

    alphatab = data->alphatab;

    if (n_steps > 0)
    {
	run_x1 = steps[0].x;
	if (run_x1 > x0)
	{
	    alpha = alphatab[(running_sum >> 16) & 0xFF];
	    if (alpha)
	    {
		if (alpha == 255)
		    data->fill_opaque(linebuf, run_x1 - x0, x0, y, data->data);
		else
		    data->fill_alpha(linebuf, run_x1 - x0, x0, y,
				     alpha, data->data);
	    }
	}

	/* render the steps into tmpbuf */
	for (k = 0; k < n_steps - 1; k++)
	{
	    running_sum += steps[k].delta;
	    run_x0 = run_x1;
	    run_x1 = steps[k + 1].x;
	    if (run_x1 > run_x0)
	    {
		alpha = alphatab[(running_sum >> 16) & 0xFF];
		if (alpha)
		{
		    if (alpha == 255)
			data->fill_opaque(linebuf + (run_x0 - x0),
					  run_x1 - run_x0, run_x0, y,
					  data->data);
		    else
			data->fill_alpha(linebuf + (run_x0 - x0),
					 run_x1 - run_x0, run_x0, y,
					 alpha, data->data);
		}
	    }
	}
	running_sum += steps[k].delta;
	if (x1 > run_x1)
	{
	    alpha = alphatab[(running_sum >> 16) & 0xFF];
	    if (alpha)
	    {
		if (alpha == 255)
		    data->fill_opaque(linebuf + (run_x1 - x0), x1 - run_x1,
				      run_x1, y, data->data);
		else
		    data->fill_alpha(linebuf + (run_x1 - x0), x1 - run_x1,
				     run_x1, y, alpha, data->data);
	    }
	}
    }
    else
    {
	alpha = alphatab[(running_sum >> 16) & 0xFF];
	if (alpha)
	{
	    if (alpha == 255)
		data->fill_opaque(linebuf, x1 - x0, x0, y, data->data);
	    else
		data->fill_alpha(linebuf, x1 - x0, x0, y, alpha, data->data);
	}
    }
}


/*
 *	Main entry points.
 */

/* Render the svp SVP into PIXBUF with the uniform color RGBA.
 *
 * X0, Y0, X1, Y1 are the virtual pixel coordinates of PIXBUF.
 */
static void
skrender_rgb_svp_alpha(const ArtSVP *svp, int x0, int y0, int x1, int y1,
		       art_u32 rgba, SKPixbuf * pixbuf,
		       ArtAlphaGamma *alphagamma)
{
    SKRenderAlphaData data;
    SKRenderRGBData rgbdata;
    int r, g, b, alpha;

    r = rgba >> 24;
    g = (rgba >> 16) & 0xff;
    b = (rgba >> 8) & 0xff;
    alpha = rgba & 0xff;

    rgbdata.r = r;
    rgbdata.g = g;
    rgbdata.b = b;
    rgbdata.alpha = alpha;


    fill_alphatab(data.alphatab, alpha);

    data.pixbuf = pixbuf;
    data.x0 = x0;
    data.x1 = x1;
    data.y0 = y0;
    data.y1 = y1;
    data.data = &rgbdata;
    data.fill_alpha = skrender_rgb_run_alpha;
    data.fill_opaque = skrender_rgb_fill_run;

    art_svp_render_aa(svp, x0, y0, x1, y1, skrender_svp_aa_callback, &data);
}


/* Render the svp SVP into PIXBUF with the uniform color RGBA.
 *
 * X0, Y0, X1, Y1 are the virtual pixel coordinates of PIXBUF.
 */
void
skrender_svp_rgb(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
		 art_u32 color, const ArtSVP *svp)
{
    skrender_rgb_svp_alpha(svp, x0, y0, x1, y1, color, pixbuf, NULL);
}



/* Render SOURCE onto PIXBUF scaling and clipping it on the fly.
 *
 * SVP is the clipping path. X0, Y0, X1, Y1 are PIXBUF's virtual pixel
 * coordinates. SOURCE is the source image. DEST_X, DEST_Y, DEST_WIDTH
 * and DEST_HEIGHT describe a rectangle in virtual pixel coordinates on
 * which the source image is mapped. FLIP_X and FLIP_Y indicate whether
 * to flip the image axis (e.g. if FLIP_Y is true the image will be
 * drawn upside down). PIXBUF is the target pixbuf.
 */
static void
skrender_scale_image_svp_alpha(const ArtSVP *svp,
			       int x0, int y0, int x1, int y1,
			       SKPixbuf * source,
			       int dest_x, int dest_y,
			       int dest_width, int dest_height,
			       int flip_x, int flip_y,
			       SKPixbuf * pixbuf,
			       ArtAlphaGamma *alphagamma)
{
    SKRenderAlphaData data;
    SKRenderScaleImageData imagedata;
    int startx = dest_x, starty = dest_y;
    int width = dest_width, height = dest_height;
    int alpha = 255;
    int * origx, *origy;
    int x, y;


    if (startx >= x1 || startx + dest_width <= x0)
	return;
    if (startx < x0)
    {
	width = width - x0 + startx;
	startx = x0;
    }

    if (starty >= y1 || starty + dest_height <= y0)
	return;
    if (starty < y0)
    {
	height = height - y0 + starty;
	starty = y0;
    }

    if (startx + width > x1)
	width = x1 - startx;
    if (starty + height > y1)
	height = y1 - starty;

    origx = malloc((width + height) * sizeof(int));
    if (!origx)
	return;
    origy = origx + width;

    for (x = 0; x < width; x++)
    {
	origx[x] = ((x + startx - dest_x) * source->width) / dest_width;
    }
    if (flip_x)
	for (x = 0; x < width; x++)
	    origx[x] = source->width - origx[x] - 1;

    for (y = 0; y < height; y++)
    {
	origy[y] = ((y + starty - dest_y) * source->height) / dest_height;
    }
    if (flip_y)
	for (y = 0; y < height; y++)
	    origy[y] = source->height - origy[y] - 1;

    fill_alphatab(data.alphatab, alpha);

    imagedata.source = source;
    imagedata.startx = startx;
    imagedata.starty = starty;
    imagedata.width = width;
    imagedata.height = height;
    imagedata.origx = origx;
    imagedata.origy = origy;
    imagedata.x0 = x0;
    imagedata.y0 = y0;
    data.pixbuf = pixbuf;
    data.x0 = x0;
    data.x1 = x1;
    data.y0 = y0;
    data.y1 = y1;
    data.data = &imagedata;

    if (source->has_alpha)
	data.fill_alpha = skrender_scale_image_run_alpha_trans;
    else
	data.fill_alpha = skrender_scale_image_run_alpha;
    data.fill_opaque = skrender_scale_image_fill_run;

    art_svp_render_aa(svp, x0, y0, x1, y1, skrender_svp_aa_callback, &data);

    free(origx);
}


/* Render SOURCE onto PIXBUF scaling and clipping it on the fly.
 *
 * X0, Y0, X1, Y1 are PIXBUF's virtual pixel coordinates. SOURCE is the
 * source image. DEST_X, DEST_Y, DEST_WIDTH and DEST_HEIGHT describe a
 * rectangle in virtual pixel coordinates on which the source image is
 * mapped. DEST_WIDTH and DEST_HEIGHT may be negative in which case the
 * image is flipped along the corresponding axis (e.g. if DEST_HEIGHT is
 * negative the image will be drawn upside down). SVP is the clipping
 * path.
 */
void
skrender_scale_image_svp(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
			 SKPixbuf * source, int dest_x, int dest_y,
			 int dest_width, int dest_height,
			 const ArtSVP *svp)
{
    skrender_scale_image_svp_alpha(svp, x0, y0, x1, y1, source, dest_x, dest_y,
				   abs(dest_width), abs(dest_height),
				   dest_width < 0, dest_height < 0,
				   pixbuf, NULL);
}



/* Render SOURCE on PIXBUF scaling it on the fly.
 *
 * Iterate through all the pixels in STARTX, STARTY, WIDTH, HEIGHT (in
 * virtual pixel coordinates) and use ORIGX, ORIGY to look up the
 * corresponding source pixel. ORIGX and ORIGY are arrays with WIDTH
 * resp. HEIGHT coordinates, one for each column or row of the target
 * region in PIXBUF (index 0 refers the STARTX column, resp. the STARTY
 * row).
 */
static void
skrender_scale_image_32(SKPixbuf * pixbuf, SKPixbuf *source,
			int startx, int starty, int width, int height,
			int * origx, int * origy)
{
    art_u32 * dest;
    art_u32 * src;
    int x, y, lasty;

    lasty = -1;
    for (y = 0; y < height; y++)
    {
	dest = pixbuf->data32[y + starty] + startx;

	if (origy[y] != lasty)
	{
	    src = source->data32[origy[y]];
	    for (x = 0; x < width; x++, dest++)
	    {
		*dest = src[origx[x]];
	    }
	    lasty = origy[y];
	}
	else
	{
	    memcpy(dest, pixbuf->data32[y + starty - 1] + startx, width * 4);
	}
    }
}

/* Render SOURCE on PIXBUF scaling it on the fly.
 *
 * As the previous function, but using the alpha component of the source
 * image to blend it onto the canvas.
 */
static void
skrender_scale_image_32_trans(SKPixbuf * pixbuf, SKPixbuf *source,
			      int startx, int starty, int width, int height,
			      int * origx, int * origy)
{
    art_u32 * dest;
    art_u32 * src;
    int x, y, lasty;
    art_u8 *v, *fg;

    lasty = -1;
    for (y = 0; y < height; y++)
    {
	dest = pixbuf->data32[y + starty] + startx;

	src = source->data32[origy[y]];
	for (x = 0; x < width; x++, dest++)
	{
	    v = (art_u8 *)dest;
	    fg = (art_u8 *)&src[origx[x]];

	    /* Blend the components of the pixel from the image with the
	     * components of the pixel on the canvas using the blend
	     * function
	     *
	     * result = src_alpha * src_component
	     *        + (1 - src_alpha) * dest_component.
	     */

	    /* Use division by 255 since 255 is equivalent to 1.0
	     * (opaque). Using a system where 256 is equivalent to 1.0
	     * would allow us to use bit shifting to optimise division
	     * by 256.
	     */
	    v[0] = BLEND_SRC_ALPHA(fg[0], fg[3], v[0]);
	    v[1] = BLEND_SRC_ALPHA(fg[1], fg[3], v[1]);
	    v[2] = BLEND_SRC_ALPHA(fg[2], fg[3], v[2]);

	    /* Just overwrite the alpha value of the canvas with a value
	     * corresponding to full opacity.
	     */
	    v[3] = 255;
	}
	lasty = origy[y];
    }
}

/* Render SOURCE onto PIXBUF scaling it on the fly.
 *
 * X0, Y0, X1, Y1 are PIXBUF's virtual pixel coordinates. SOURCE is the
 * source image. DEST_X, DEST_Y, DEST_WIDTH and DEST_HEIGHT describe a
 * rectangle in virtual pixel coordinates on which the source image is
 * mapped. DEST_WIDTH and DEST_HEIGHT may be negative in which case the
 * image is flipped along the corresponding axis (e.g. if DEST_HEIGHT is
 * negative the image will be drawn upside down).
 */
void
skrender_scale_image(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
		     SKPixbuf * source, int dest_x, int dest_y,
		     int dest_width, int dest_height)
{
    int flip_x, flip_y;
    int startx = dest_x, starty = dest_y;
    int width = dest_width, height = dest_height;
    int * origx, *origy;
    int x, y;

    flip_x = dest_width < 0;
    dest_width = abs(dest_width);
    width = dest_width;
    flip_y = dest_height < 0;
    dest_height = abs(dest_height);
    height = dest_height;

    if (startx >= x1 || startx + dest_width <= x0)
	return;
    if (startx < x0)
    {
	width = width - x0 + startx;
	startx = x0;
    }

    if (starty >= y1 || starty + dest_height <= y0)
	return;
    if (starty < y0)
    {
	height = height - y0 + starty;
	starty = y0;
    }

    if (startx + width > x1)
	width = x1 - startx;
    if (starty + height > y1)
	height = y1 - starty;

    origx = malloc((width + height) * sizeof(int));
    if (!origx)
	return;
    origy = origx + width;

    for (x = 0; x < width; x++)
    {
	origx[x] = ((x + startx - dest_x) * source->width) / dest_width;
    }
    if (flip_x)
	for (x = 0; x < width; x++)
	    origx[x] = source->width - origx[x] - 1;

    for (y = 0; y < height; y++)
    {
	origy[y] = ((y + starty - dest_y) * source->height) / dest_height;
    }
    if (flip_y)
	for (y = 0; y < height; y++)
	    origy[y] = source->height - origy[y] - 1;

    if (source->has_alpha)
        skrender_scale_image_32_trans(pixbuf, source, startx - x0, starty - y0,
				      width, height, origx, origy);
	else
        skrender_scale_image_32(pixbuf, source, startx - x0, starty - y0,
				width, height, origx, origy);

    free(origx);
}


/* Render the SOURCE onto PIXBUF applying a transformation on the fly.
 *
 * TRAFO is the affine transformation from target coordinates to source
 * coordinates. X0, Y0 are the virtual pixel coordinates of the top-left
 * corner of PIXBUF. STARTY and HEIGHT describe a range of scanlines in
 * virtual pixel coordinates for each of which the arrays VISBLE, MINX
 * and MAXX describe which pixels are covered by the transformed image.
 * See make_region for more details on these arrays.
 */
static void
skrender_transform_image_32(SKPixbuf * pixbuf, SKPixbuf * source,
			    SKTrafoObject * trafo,
			    int x0, int y0, int starty, int height,
			    int * visible, int * minx, int * maxx)
{
    art_u32 *dest;
    int x, y, desty;
    double sx, sy;
    art_u32 ** src = source->data32;

    for (y = 0; y < height; y++)
    {
	if (!visible[y])
	    continue;
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = pixbuf->data32[desty - y0] + minx[y] - x0;
	for (x = minx[y]; x <= maxx[y];
	     x++, dest++, sx += trafo->m11, sy += trafo->m21)
	{
	    *dest = src[(int)sy][(int)sx];
	}
    }
}

/* Render the SOURCE onto PIXBUF applying a transformation on the fly.
 *
 * As the previous function, but using the alpha component of the source
 * image to blend it onto the canvas.
 */
static void
skrender_transform_image_32_trans(SKPixbuf * pixbuf, SKPixbuf * source,
				  SKTrafoObject * trafo,
				  int x0, int y0, int starty, int height,
				  int * visible, int * minx, int * maxx)
{
    art_u32 *dest;
    int x, y, desty;
    double sx, sy;
    art_u32 ** src = source->data32;
    art_u8 *v, *fg;

    for (y = 0; y < height; y++)
    {
	if (!visible[y])
	    continue;
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = pixbuf->data32[desty - y0] + minx[y] - x0;
	for (x = minx[y]; x <= maxx[y];
	     x++, dest++, sx += trafo->m11, sy += trafo->m21)
	{
	    v = (art_u8 *)dest;
	    fg = (art_u8 *)&src[(int)sy][(int)sx];

	    /* Blend the components of the pixel from the image with the
	     * components of the pixel on the canvas using the blend
	     * function
	     *
	     * result = src_alpha * src_component
	     *        + (1 - src_alpha) * dest_component.
	     */

	    /* Use division by 255 since 255 is equivalent to 1.0
	     * (opaque). Using a system where 256 is equivalent to 1.0
	     * would allow us to use bit shifting to optimise division
	     * by 256.
	     */
	    v[0] = BLEND_SRC_ALPHA(fg[0], fg[3], v[0]);
	    v[1] = BLEND_SRC_ALPHA(fg[1], fg[3], v[1]);
	    v[2] = BLEND_SRC_ALPHA(fg[2], fg[3], v[2]);

	    /* Just overwrite the alpha value of the canvas with a value
	     * corresponding to full opacity. */
	    v[3] = 255;
	}
    }
}

/* Fill the int-arrays describing the region a transformed image will cover.
 *
 * TRAFO is the affine transformation from the target's virtual pixel *
 * coordinates to source coordinates. XSIZE, YSIZE is the size of the
 * source image in pixels. STARTX, STARTY, WIDTH, HEIGHT is a rectangle
 * in the virtual pixel coordinates to which the rendering will be
 * restricted.
 *
 * PVISIBLE, PMINX, PMAXX are pointers to arrays of HEIGHT ints. They
 * will be filled with the minumum and maximum x coordinates covered by
 * the transformed image in a scanline (index 0 refers to the scanline
 * STARTY). The visible array holds a boolean indicating whether that
 * scanline will be modified at all.
 */
static void
make_region(SKTrafoObject * trafo, int xsize, int ysize,
	    int startx, int starty, int width, int height,
	    int * pvisible, int * pminx, int * pmaxx)
{
    double sx, sy;
    int y, desty;
    int minx, maxx;
    double minxx, minxy, maxxx, maxxy;

    for (y = 0; y < height; y++)
    {
	pvisible[y] = 0;
	desty = starty + y;
	sx = trafo->m11 * startx + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * startx + trafo->m22 * desty + trafo->v2;

	if (trafo->m11 > 0)
	{
	    if (sx < 0)
		minxx = ceil(- sx / trafo->m11);
	    else
		minxx = 0;

	    if (sx < xsize)
	    {
		maxxx = floor((xsize - sx) / trafo->m11);
	    }
	    else
	    {
		continue;
	    }
	}
	else if (trafo->m11 < 0)
	{
	    if (sx < 0)
		continue;
	    else
		maxxx = floor(-sx / trafo->m11);
	    if (sx > xsize)
		minxx = ceil((xsize - sx) / trafo->m11);
	    else
		minxx = 0;
	}
	else
	{
	    if (sx < 0 || sx > xsize)
		continue;
	    minxx = 0;
	    maxxx = width;
	}

	if (trafo->m21 > 0)
	{
	    if (sy < 0)
		minxy = ceil(- sy / trafo->m21);
	    else
		minxy = 0;

	    if (sy < ysize)
	    {
		maxxy = floor((ysize - sy) / trafo->m21);
	    }
	    else
		continue;
	}
	else if (trafo->m21 < 0)
	{
	    if (sy < 0)
		continue;
	    else
		maxxy = floor(-sy / trafo->m21);
	    if (sy > ysize)
		minxy = ceil((ysize - sy) / trafo->m21);
	    else
		minxy = 0;
	}
	else
	{
	    if (sy < 0 || sy > ysize)
		continue;
	    minxy = 0;
	    maxxy = width;
	}
	minx = minxx > minxy ? minxx : minxy;

	maxx = maxxx < maxxy ? maxxx : maxxy;
	if (maxx >= width)
	    maxx = width - 1;
	if (maxx < minx)
	    continue;
	maxx += startx;
	minx += startx;

	pminx[y] = minx; pmaxx[y] = maxx;
	pvisible[y] = 1;
    }
}

/* Render a transformed image onto a pixbuf.
 *
 * X0, Y0, X1, Y1 are PIXBUF's virtual pixel coordinates. SOURCE is the
 * source image. TRAFO is the affine transformation from virtual pixel
 * coordinats to the to source coordinates. DEST_X, DEST_Y and
 * DEST_WIDTH, DEST_HEIGHT is the region in PIXBUF where the image will
 * end up and is used to determine whether the image should be rendered
 * at all. The DEST_* parameters may describe a region partially or
 * completely outside of the virtual pixel coordinates X0, Y0, X1, Y1.
 * SVP is an optional clipping path (NULL means no clipping).
 */
void
skrender_transform_image(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
			 SKPixbuf * source, SKTrafoObject * trafo,
			 int dest_x, int dest_y,
			 int dest_width, int dest_height,
			 const ArtSVP *svp)
{
    int startx = dest_x, starty = dest_y;
    int width = dest_width, height = dest_height;
    int *minx, *maxx, *visible;

    /* adjust the target region (start*, width, height) to the clip
     * region (x0,...). Return if it's clear that the final region will
     * be empty.
     */
    if (startx >= x1 || startx + dest_width <= x0)
	return;
    if (startx < x0)
    {
	width = width - x0 + startx;
	startx = x0;
    }

    if (starty >= y1 || starty + dest_height <= y0)
	return;
    if (starty < y0)
    {
	height = height - y0 + starty;
	starty = y0;
    }

    if (startx + width > x1)
	width = x1 - startx;
    if (starty + height > y1)
	height = y1 - starty;

    /* Allocate and fill the int-arrays describing the region */
    minx = malloc(3 * height * sizeof(int));
    if (!minx)
	return;
    maxx = minx + height;
    visible = maxx + height;
    make_region(trafo, source->width, source->height, startx, starty,
		width, height, visible, minx, maxx);

    /* if we have a clipping path, go through libart and render via
     * callbacks, otherwise render directly.
     */
    if (svp)
    {
	SKRenderAlphaData data;
	SKRenderTransformImageData imagedata;
	imagedata.source = source;
	imagedata.startx = startx;
	imagedata.starty = starty;
	imagedata.width = width;
	imagedata.height = height;
	imagedata.maxx = maxx;
	imagedata.minx = minx;
	imagedata.visible = visible;
	imagedata.x0 = x0;
	imagedata.y0 = y0;
	imagedata.trafo = trafo,
	data.pixbuf = pixbuf;
	data.x0 = x0;
	data.x1 = x1;
	data.y0 = y0;
	data.y1 = y1;
	data.data = &imagedata;
	if (source->has_alpha)
	    data.fill_alpha = skrender_transform_image_run_alpha_trans;
	else
	    data.fill_alpha = skrender_transform_image_run_alpha;
	data.fill_opaque = skrender_transform_image_fill_run;
	fill_alphatab(data.alphatab, 255);
	art_svp_render_aa(svp, x0, y0, x1, y1,
			  skrender_svp_aa_callback, &data);
    }
    else
    {
	if (source->has_alpha != 0)
	    skrender_transform_image_32_trans(pixbuf, source, trafo,
					      x0, y0, starty, height,
					      visible,  minx, maxx);
	else
	    skrender_transform_image_32(pixbuf, source, trafo,
					x0, y0, starty, height,
					visible,  minx, maxx);
    }

    free(minx);
}

/* Return true if the point at coordinate X (along a horizontal or
 * vertical line should be drawn or not if the line has a dashes with
 * length DASH
 */
#define TEST_DASH(x,dash) \
 ( (x >= 0 && (x % (2*dash_length)) < dash_length) \
  || (x < 0 && (abs(x) % (2*dash_length)) >= dash_length))

/* Render a horizontal guide line onto a pixbuf with color COLOR.
 *
 * X0, Y0, X1, Y1 are the virtual pixel coordinates of PIXBUF. Y is the
 * virtual pixel y coordinate of the guide line and COLOR the color
 * value to use.
 *
 * The dash length is hard wired to 5 pixels.
 */
void
skrender_horizontal_guide_line(SKPixbuf * pixbuf,
			       int x0, int y0, int x1, int y1,
			       int y, art_u32 color)
{
    int dash_length = 5, x;
    art_u32 *row;

    color = convert_color(color);

    if (y < y0 || y >= y1)
	return;

    row = pixbuf->data32[y - y0];
    for (x = x0; x < x1; x++)
    {
	if (TEST_DASH(x, dash_length))
	{
	    row[x - x0] = color;
	}
    }
}

/* Render a vertical guide line onto a pixbuf.
 *
 * X0, Y0, X1, Y1 are the virtual pixel coordinates of PIXBUF. X is the
 * virtual pixel x coordinate of the guide line and COLOR the color
 * value to use.
 *
 * The dash length is hard wired to 5 pixels.
 */
void
skrender_vertical_guide_line(SKPixbuf * pixbuf,
			     int x0, int y0, int x1, int y1,
			     int x, art_u32 color)
{
    int y, dash_length = 5;
    int column = x - x0;

    color = convert_color(color);

    if (x < x0 || x >= x1)
	return;

    for (y = y0; y < y1; y++)
    {
	if (TEST_DASH(y, dash_length))
	{
	    pixbuf->data32[y - y0][column] = color;
	}
    }
}


/* Render a grid onto a pixbuf.
 *
 * X0, Y0, X1, Y1 are the virtual pixel coordinates of PIXBUF. ORIG_X,
 * ORIG_Y is one point of the grid in virtual pixel coordinates. XWIDTH
 * and YWIDTH are the horizontal and vertical distances between the
 * individual grid points in virtual pixel coordinates.
 *
 * COLOR is the color to use.
 */
void
skrender_grid(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
	      double orig_x, double orig_y, double xwidth, double ywidth,
	      art_u32 color)
{
    double x, y;

    color = convert_color(color);

    orig_x = orig_x + rint((x0 - orig_x) / xwidth) * xwidth;
    orig_y = orig_y + rint((y0 - orig_y) / ywidth) * ywidth;

    if (rint(orig_x) < x0)
	orig_x += xwidth;
    if (rint(orig_y) < y0)
	orig_y += ywidth;

    for (y = orig_y; rint(y) < y1; y += ywidth)
    {
	for (x = orig_x; rint(x) < x1; x += xwidth)
	{
	    pixbuf->data32[(int)(rint(y)) - y0][(int)(rint(x)) - x0] = color;
	}
    }
}
