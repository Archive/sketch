/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef CURVE_LOW_H
#define CURVE_LOW_H

#define BEZIER_DEPTH 5
#define BEZIER_NUM_STEPS ((2 << (BEZIER_DEPTH + 1)) + 1)
#define BEZIER_FILL_LENGTH (BEZIER_NUM_STEPS + 1)

int bezier_hit_segment(int * x, int * y, int px, int py);
int bezier_hit_line(int sx, int sy, int ex, int ey, int px, int py);


void bezier_point_at(double *x, double *y, double t,
		     double * result_x, double * result_y);
void bezier_tangent_at(double *x, double *y, double t,
		       double * result_x, double * result_y);

void bezier_coefficients(double * coeff_x, double * coeff_y,
			 const double *x, const double *y);

#endif
