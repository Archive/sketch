/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1999, 2000, 2002 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA
 */


#ifndef SKRENDER_H
#define SKRENDER_H

#include <libart_lgpl/art_svp.h>

/* including sktrafo.h introduces a Python.h dependency */
#include "sktrafo.h"

typedef struct {
    int width, height;
    int channels;
    art_u32 ** data32;
    int has_alpha;
} SKPixbuf;

#define SKPIXBUF_RGB 3
#define SKPIXBUF_RGBA 4

void skrender_svp_rgb(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
		      art_u32 color, const ArtSVP *svp);
void skrender_scale_image_svp(SKPixbuf * pixbuf,
			      int x0, int y0, int x1, int y1,
			      SKPixbuf * source,
			      int destx, int desty, int width, int height,
			      const ArtSVP *svp);

void skrender_scale_image(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
			  SKPixbuf * source, int dest_x, int dest_y,
			  int dest_width, int dest_height);

void skrender_transform_image(SKPixbuf * pixbuf, int x0, int y0, int x1,int y1,
			      SKPixbuf * source, SKTrafoObject * trafo,
			      int dest_x, int dest_y,
			      int dest_width, int dest_height,
			      const ArtSVP *svp);

void skrender_horizontal_guide_line(SKPixbuf * pixbuf,
				    int x0, int y0, int x1, int y1,
				    int y, art_u32 color);

void skrender_vertical_guide_line(SKPixbuf * pixbuf,
				  int x0, int y0, int x1, int y1,
				  int x, art_u32 color);

void skrender_grid(SKPixbuf * pixbuf, int x0, int y0, int x1, int y1,
		   double orig_x, double orig_y, double xwidth, double ywidth,
		   art_u32 color);

#endif /* SKARTRENDER_H */
