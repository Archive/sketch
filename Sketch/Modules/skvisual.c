/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999, 2000, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <math.h>
#include <Python.h>
#include <structmember.h>

#include <gtk/gtk.h>

#include "skcolor.h"
#include "skvisual.h"
#include "_skgtkmodule.h"


/*
 * SKVisual
 *
 * Handle the color capabilities of variuous X visuals. This type
 * provides methods to convert device independent color descriptions
 * (currently RGBColor) to device dependent pixel values.
 */


/*
 *	functions for true color visuals
 */

static PyObject *
skvisual_truecolor_get_pixel(SKVisualObject * self, SKColorObject * color)
{
    int r, g, b;
/*    double gamma = 1.0 / 2.2;*/
    
    r = rint(pow(color->red, self->gamma_inv) * 255);
    g = rint(pow(color->green, self->gamma_inv) * 255);
    b = rint(pow(color->blue, self->gamma_inv) * 255);
    
/*    r = rint(color->red * 255);
    g = rint(color->green * 255);
    b = rint(color->blue * 255);*/

    return PyInt_FromLong(self->red_bits[r] | self->green_bits[g]
			  | self->blue_bits[b]);
}

static int
skvisual_init_truecolor(SKVisualObject * self)
{
    GdkVisual * visual = self->visual;
    if (visual->depth == 16 || visual->depth == 15 ||
	visual->depth == 24 || visual->depth == 32)
    {
	int red_bit_count = 0, green_bit_count = 0, blue_bit_count = 0;
	int red_off = -1, green_off = -1, blue_off = -1;
	int bit, idx;

	for (bit = 0; bit < 32; bit++)
	{
	    if (visual->red_mask & (1 << bit))
	    {
		red_bit_count++;
		if (red_off < 0)
		    red_off = bit;
	    }
	    if (visual->green_mask & (1 << bit))
	    {
		green_bit_count++;
		if (green_off < 0)
		    green_off = bit;
	    }
	    if (visual->blue_mask & (1 << bit))
	    {
		blue_bit_count++;
		if (blue_off < 0)
		    blue_off = bit;
	    }
	}
	for (idx = 0; idx < 256; idx++)
	{
	    self->red_bits[idx] = (idx >> (8 - red_bit_count)) << red_off;
	    self->green_bits[idx] = (idx >> (8 - green_bit_count)) <<green_off;
	    self->blue_bits[idx] = (idx >> (8 - blue_bit_count)) << blue_off;
	}
	/* this will only work for depths 24/32 with 8 bits per component...*/
	self->red_index = red_off / 8;
	self->green_index = green_off / 8;
	self->blue_index = blue_off / 8;

	/*
	 * init function pointers
	 */
	self->get_pixel = skvisual_truecolor_get_pixel;
	self->free_extra = NULL;
    }
    else
    {
	PyErr_SetString(PyExc_ValueError, "Only TrueColor visuals of depths "
			"15, 16, 24 and 32 are supported");
	return 0;
    }
    return 1;
}

/*
 *	functions for PseudoColor visuals
 */


static int
skvisual_fill_tile(SKVisualObject * self, SKColorObject * color)
{
    int red, green, blue;
    int x, y;
    long *colors = self->pseudocolor_table;
    SKDitherInfo *dither_red = self->dither_red;
    SKDitherInfo *dither_green = self->dither_green;
    SKDitherInfo *dither_blue = self->dither_blue;
    SKDitherInfo r, g, b;
    unsigned char **dither_matrix;
    unsigned char *matrix;
    unsigned char *dest;
    GdkImage * image = self->tile;
    int equal = 1;

    red = (int)(255 * color->red) & 0xFF;
    green = (int)(255 * color->green) & 0xFF;
    blue = (int)(255 * color->blue) & 0xFF;

    for (y = 0; y < 8; y++)
    {
	dither_matrix = self->dither_matrix[y];
	dest = (unsigned char*)image->mem + image->bpl * y;

	for (x = 0; x < 8; x++, dest++)
	{
	    r = dither_red[red];
	    g = dither_green[green];
	    b = dither_blue[blue];
		
	    matrix = dither_matrix[x];
	    *dest = colors[(r.c[matrix[r.s[1]]] +
			    g.c[matrix[g.s[1]]] +
			    b.c[matrix[b.s[1]]])];
	    equal = equal && *dest == *(unsigned char*)(image->mem);
	}
    }
    return equal;
}


static PyObject *
skvisual_pseudocolor_get_pixel(SKVisualObject * self, SKColorObject * color)
{
    int idx;

    if (color->red == color->green && color->green == color->blue)
	/* XXX: gray should also be dithered */
	idx = self->cube_size + color->red * (self->shades_gray - 1) + 0.5;
    else
    {
	if (!skvisual_fill_tile(self, color))
	{
	    GdkPixmap *tile = gdk_pixmap_new(self->window, 8, 8,
					     self->visual->depth);
	    if (tile)
	    {
		PyObject * result;
		gdk_draw_image(tile, self->tilegc, self->tile, 0, 0,
			       0, 0, 8, 8);
		result = pygobject_new(G_OBJECT(tile));
		gdk_pixmap_unref(tile);
		return result;
	    }
	    else
		fprintf(stderr, "Cannot allocate tile pixmap, "
			"using solid fill\n");
	}
	
	/* the tile is solid color or the creation of the pixmap failed */
	idx = (int)(color->blue * (self->shades_b - 1) + 0.5)
	    + (self->shades_b
	       * ((int)(color->green * (self->shades_g - 1) + 0.5)
		  + self->shades_g * (int)(color->red * (self->shades_r - 1)
					   + 0.5)));
    }
	
    if (idx < 0)
	idx = 0;
    else if (idx > 255)
	idx = 255;

    return PyInt_FromLong(self->pseudocolor_table[idx]);
}

static void
skvisual_pseudocolor_free(SKVisualObject * self)
{
    int i, j;

    gdk_image_destroy(self->tile);
    gdk_gc_unref(self->tilegc);
    if (self->dither_matrix)
    {
	for (i = 0; i < 8; i++)
	{
	    for (j = 0; j < 8; j++)
	    {
		PyMem_DEL(self->dither_matrix[i][j]);
	    }
	    PyMem_DEL(self->dither_matrix[i]);
	}
	PyMem_DEL(self->dither_matrix);
    }
    if (self->dither_red)
	PyMem_DEL(self->dither_red);
    if (self->dither_green)
	PyMem_DEL(self->dither_green);
    if (self->dither_blue)
	PyMem_DEL(self->dither_blue);
    if (self->dither_gray)
	PyMem_DEL(self->dither_gray);
}

static int
skvisual_init_pseudocolor(SKVisualObject * self, PyObject * args)
{
    int r, g, b, gray;
    PyObject * list;
    int length, i;
    
    if (!PyArg_ParseTuple(args, "iiiiO!", &r, &g, &b, &gray,
			  &PyList_Type, &list))
	return 0;

    self->shades_r = r;
    self->shades_g = g;
    self->shades_b = b;
    self->shades_gray = gray;
    self->cube_size = r * g * b;

    if (self->cube_size + self->shades_gray > self->visual->colormap_size)
    {
	PyErr_SetString(PyExc_ValueError,
			"color cube is larger that colormap");
	return 0;
    }

    self->tile = gdk_image_new(GDK_IMAGE_NORMAL, self->visual, 8, 8);
    if (!self->tile)
    {
	PyErr_NoMemory();
	return 0;
    }

    self->tilegc = gdk_gc_new(self->window);
    if (!self->tilegc)
    {
	gdk_image_destroy(self->tile);
	PyErr_SetString(PyExc_RuntimeError,
			"Cannot create gc for dither pattern");
	return 0;
    }

    self->dither_matrix = NULL;
    self->dither_red = NULL;
    self->dither_green = NULL;
    self->dither_blue = NULL;
    self->dither_gray = NULL;
    
    length = PyList_Size(list);
    /* XXX: compare length with color cube */
    length = length > 256 ? 256 : length;
    for (i = 0; i < length; i++)
    {
	PyObject * entry = PyList_GetItem(list, i);
	if (PyInt_Check(entry))
	    self->pseudocolor_table[i] = PyInt_AsLong(entry);
	else
	{
	    PyErr_SetString(PyExc_TypeError, "list of ints expected");
	    return 0;
	}
    }

    /* dither */
    SKVisual_InitDither(self);

    /*
     * init function pointers
     */
    self->get_pixel = skvisual_pseudocolor_get_pixel;
    self->free_extra = skvisual_pseudocolor_free;
    
    return 1;
}

    

static PyObject *
SKVisual_FromWindow(GdkWindow * window, PyObject * args)
{
    SKVisualObject * self = PyObject_NEW(SKVisualObject, &SKVisualType);
    int result = 0;
    
    if (!self)
	return NULL;

    self->window = window;
    gdk_window_ref(self->window);
    self->visual = gdk_window_get_visual(window);
    gdk_visual_ref(self->visual);

    self->get_pixel = NULL;
    self->free_extra = NULL;

    self->gamma = self->gamma_inv = 1.0;

    /* fill the visual specific attributes */
    if (self->visual->type == GDK_VISUAL_TRUE_COLOR)
	result = skvisual_init_truecolor(self);
    else if (self->visual->type == GDK_VISUAL_PSEUDO_COLOR)
	result = skvisual_init_pseudocolor(self, args);
    else
	PyErr_SetString(PyExc_ValueError, "specified visual not supported");
    
    if (!result)
    {
	Py_DECREF(self);
	return NULL;
    }

    return (PyObject*)self;
}
    

static void
skvisual_dealloc(SKVisualObject * self)
{
    if (self->free_extra)
	self->free_extra(self);
    gdk_visual_unref(self->visual);
    gdk_window_unref(self->window);
    PyObject_DEL(self);
}

static PyObject *
skvisual_repr(SKVisualObject * self)
{
    char buf[100];
    sprintf(buf, "<SKVisual at %ld>", (long)self);
    return PyString_FromString(buf);
}


/*
 *	Python methods
 */

/*
 *	visual.get_pixel(COLOR)
 */

static PyObject *
skvisual_get_pixel(SKVisualObject * self, PyObject * args)
{
    PyObject * color;

    if (!PyArg_ParseTuple(args, "O", &color))
	return NULL;

    if (PyInt_Check(color))
    {
	/* pass ints back unchanged. Special feature to support device
	dependent colors for highlighting etc.*/
	Py_INCREF(color);
	return color;
    }
    if (color->ob_type != sketch_interface->SKColorType)
    {
	PyErr_SetString(PyExc_TypeError, "Argument must be SKColor or int");
	return NULL;
    }
    if (self->get_pixel)
	return self->get_pixel(self, (SKColorObject*)color);

    PyErr_SetString(PyExc_RuntimeError, "Visual is not initialized correctly");
    return NULL;
}

static PyObject *
skvisual_set_gamma(SKVisualObject * self, PyObject * args)
{
    double gamma;

    if (!PyArg_ParseTuple(args, "d", &gamma))
	return NULL;

    self->gamma = gamma;
    self->gamma_inv = 1.0 / gamma;

    Py_INCREF(Py_None);
    return Py_None;
}

#define OFF(x) offsetof(GdkVisual, x)
static struct memberlist skvisual_memberlist[] = {
    {"type",		T_INT,		OFF(type),		RO},
    {"depth",		T_INT,		OFF(depth),		RO},
    {"red_mask",	T_ULONG,	OFF(red_mask),		RO},
    {"green_mask",	T_ULONG,	OFF(green_mask),	RO},
    {"blue_mask",	T_ULONG,	OFF(blue_mask),		RO},
    {"colormap_size",	T_INT,		OFF(colormap_size),	RO},
    {"bits_per_rgb",	T_INT,		OFF(bits_per_rgb),	RO},
    {NULL} 
};
#undef OFF

static struct PyMethodDef skvisual_methods[] = {
    {"get_pixel",		(PyCFunction)skvisual_get_pixel, METH_VARARGS},
    {"set_gamma",		(PyCFunction)skvisual_set_gamma, METH_VARARGS},
    {NULL,	NULL}
};


static PyObject *
skvisual_getattr(PyObject * self, char * name)
{
    PyObject * result;

    result = Py_FindMethod(skvisual_methods, self, name);
    if (result != NULL)
	return result;
    PyErr_Clear();

    return PyMember_Get((char *)(((SKVisualObject*)self)->visual),
			skvisual_memberlist, name);
}


PyTypeObject SKVisualType = {
	PyObject_HEAD_INIT(NULL)
	0,
	"skvisual",
	sizeof(SKVisualObject),
	0,
	(destructor)skvisual_dealloc,	/*tp_dealloc*/
	(printfunc)NULL,		/*tp_print*/
	skvisual_getattr,		/*tp_getattr*/
	0,				/*tp_setattr*/
	(cmpfunc)0,			/*tp_compare*/
	(reprfunc)skvisual_repr,	/*tp_repr*/
	0,				/*tp_as_number*/
	0,				/*tp_as_sequence*/
	0,				/*tp_as_mapping*/
	0,				/*tp_hash*/
	(ternaryfunc)0,			/*tp_call*/
};





/*
 *		Module Functions
 */


/*
 *	CreateVisual(WINDOW)
 */
PyObject *
SKVisual_PyCreateVisual(PyObject * self, PyObject * args)
{
    PyObject * skvisual, *additional_args = NULL;
    PyGObject * window_object;

    if (!PyArg_ParseTuple(args, "O!|O!", &PyGObject_Type, &window_object,
			  &PyTuple_Type, &additional_args))
	return NULL;

    skvisual = SKVisual_FromWindow(GDK_WINDOW(window_object->obj),
				   additional_args);

    return skvisual;
}


