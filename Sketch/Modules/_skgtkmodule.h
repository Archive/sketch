/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1998, 1999, 2000, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _SKGTK_H
#define _SKGTK_H

#include <Python.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "_sketchpublic.h"

extern Sketch_Interface * sketch_interface;

#ifdef SKGTK_MAIN
#include <pygobject.h>
#include <pygtk/pygtk.h>
#else
#define NO_IMPORT_PYGTK
#include <pygtk/pygtk.h>
#endif

/* This is initialized in init_skgtk */
extern PyTypeObject *SKGTK_PyGObject_Type;
#define PyGObject_Type (*SKGTK_PyGObject_Type)

#endif /* _SKGTK_H */
