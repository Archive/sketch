/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*#define __NO_MATH_INLINES
#include <math.h>*/
#include <Python.h>
#include <structmember.h>

#include <gdk/gdk.h>

#include <Imaging.h>
#include "sktrafo.h"
#include "imageobject.h"
#include "regionobject.h"
#include "skvisual.h"
#include "_skgtkmodule.h"


/* redefine the ImagingObject struct defined in PIL's _imagingmodule.c
 * there should be a better way to do this...
 */
typedef struct {
    PyObject_HEAD
    Imaging image;
} ImagingObject;


static void
image_scale_rgb_16(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		   int startx, int starty, int width, int height,
		   int * origx, int * origy)
{
    long *red_bits = visual->red_bits;
    long *green_bits = visual->green_bits;
    long *blue_bits  = visual->blue_bits;
    short * dest;
    int x, y, lasty;
    INT32 * src;
    unsigned char * rgb;

    lasty = -1;
    for (y = 0; y < height; y++)
    {
	dest = (short*)((unsigned char*)ximage->mem + ximage->bpl * (y+starty))
	       + startx;

	if (origy[y] != lasty)
	{
	    src = image->image32[origy[y]];
	    for (x = 0; x < width; x++, dest++)
	    {
		rgb = (unsigned char*)(src + origx[x]);
		*dest = red_bits[(int)rgb[0]]
		    | green_bits[(int)rgb[1]]
		    | blue_bits[(int)rgb[2]];
	    }
	    lasty = origy[y];
	}
	else
	{
	    memcpy(dest, ((char*)dest) - ximage->bpl,
		   width * sizeof(*dest));
	}
    }
}

static void
image_scale_rgb_24(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		   int startx, int starty, int width, int height,
		   int * origx, int * origy)
{
    int red_index = visual->red_index;
    int green_index = visual->green_index;
    int blue_index = visual->blue_index;
    unsigned char *dest;
    int bpp = ximage->bpp;
    int x, y, lasty;
    INT32 * src = NULL;
    unsigned char * rgb;

    if (ximage->byte_order == GDK_MSB_FIRST)
    {
	red_index = 3 - red_index;
	green_index = 3 - green_index;
	blue_index = 3 - blue_index;
    }

    lasty = -1;
    for (y = 0; y < height; y++)
    {
	dest = ((unsigned char*)ximage->mem + ximage->bpl * (y + starty))
	    + bpp * startx;

	if (origy[y] != lasty)
	{
	    src = image->image32[origy[y]];
	    for (x = 0; x < width; x++, dest += bpp)
	    {
		rgb= (unsigned char*)(src + origx[x]);
		dest[red_index] = rgb[0];
		dest[green_index] = rgb[1];
		dest[blue_index] = rgb[2];
	    }
	    lasty = origy[y];
	}
	else
	{
	    memcpy(dest, ((char*)dest) - ximage->bpl,
		   width * bpp);
	}
    }
}

static void
image_scale_rgb_8(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		  int startx, int starty, int width, int height,
		  int * origx, int * origy)
{
    int x, y;
    INT32 * src;
    long *colors = visual->pseudocolor_table;
    SKDitherInfo *dither_red = visual->dither_red;
    SKDitherInfo *dither_green = visual->dither_green;
    SKDitherInfo *dither_blue = visual->dither_blue;
    SKDitherInfo r, g, b;
    unsigned char **dither_matrix;
    unsigned char *matrix;
    unsigned char *dest;
    unsigned char * rgb;

    for (y = 0; y < height; y++)
    {
	dither_matrix = visual->dither_matrix[(y + starty) & 0x7];
	dest = ((unsigned char*)ximage->mem + ximage->bpl * (y + starty))
	       + startx;

	src = image->image32[origy[y]];
	for (x = 0; x < width; x++, dest++)
	{
	    rgb = (unsigned char*)(src + origx[x]);
	    r = dither_red[rgb[0]];
	    g = dither_green[rgb[1]];
	    b = dither_blue[rgb[2]];

	    matrix = dither_matrix[x & 0x7];
	    *dest = colors[(r.c[matrix[r.s[1]]] +
			    g.c[matrix[g.s[1]]] +
			    b.c[matrix[b.s[1]]])];
	}
    }
}

static void
image_scale_gray_16(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		    int startx, int starty, int width, int height,
		    int * origx, int * origy)
{
    long *red_bits = visual->red_bits;
    long *green_bits = visual->green_bits;
    long *blue_bits  = visual->blue_bits;
    short * dest;
    int x, y, lasty, gray;
    UINT8 * src;

    lasty = -1;
    for (y = 0; y < height; y++)
    {
	dest = (short*)((unsigned char*)ximage->mem + ximage->bpl * (y+starty))
 	       + startx;

	if (origy[y] != lasty)
	{
	    src = image->image8[origy[y]];
	    for (x = 0; x < width; x++, dest++)
	    {
		gray = src[origx[x]];
		*dest = red_bits[gray] | green_bits[gray] | blue_bits[gray];
	    }
	    lasty = origy[y];
	}
	else
	{
	    memcpy(dest, ((char*)dest) - ximage->bpl,
		   width * sizeof(*dest));
	}
    }
}

static void
image_scale_gray_24(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		    int startx, int starty, int width, int height,
		    int * origx, int * origy)
{
    int red_index = visual->red_index;
    int green_index = visual->green_index;
    int blue_index = visual->blue_index;
    unsigned char *dest;
    int bpp = ximage->bpp;
    int x, y, lasty;
    UINT8 * src = NULL;

    if (ximage->byte_order == GDK_MSB_FIRST)
    {
	red_index = 3 - red_index;
	green_index = 3 - green_index;
	blue_index = 3 - blue_index;
    }

    lasty = -1;
    for (y = 0; y < height; y++)
    {
	dest = ((unsigned char*)ximage->mem + ximage->bpl * (y + starty))
	    + bpp * startx;

	if (origy[y] != lasty)
	{
	    src = image->image8[origy[y]];
	    for (x = 0; x < width; x++, dest += bpp)
	    {
		dest[red_index] = dest[green_index] = dest[blue_index] \
		    = src[origx[x]];
	    }
	    lasty = origy[y];
	}
	else
	{
	    memcpy(dest, ((char*)dest) - ximage->bpl,
		   width * bpp);
	}
    }
}

static void
image_scale_gray_8(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		   int startx, int starty, int width, int height,
		   int * origx, int * origy)
{
    int x, y;
    UINT8 * src;
    long *colors = visual->pseudocolor_table;
    SKDitherInfo *dither_red = visual->dither_red;
    SKDitherInfo *dither_green = visual->dither_green;
    SKDitherInfo *dither_blue = visual->dither_blue;
    SKDitherInfo r, g, b;
    unsigned char **dither_matrix;
    unsigned char *matrix;
    unsigned char *dest;
    int gray;

    for (y = 0; y < height; y++)
    {
	dither_matrix = visual->dither_matrix[(y + starty) & 0x7];
	dest = ((unsigned char*)ximage->mem + ximage->bpl * (y + starty))
	       + startx;

	src = image->image8[origy[y]];
	for (x = 0; x < width; x++, dest++)
	{
	    gray = src[origx[x]];
	    r = dither_red[gray];
	    g = dither_green[gray];
	    b = dither_blue[gray];

	    matrix = dither_matrix[x & 0x7];
	    *dest = colors[(r.c[matrix[r.s[1]]] +
			    g.c[matrix[g.s[1]]] +
			    b.c[matrix[b.s[1]]])];
	}
    }
}

static void
scale_image(SKVisualObject * visual, Imaging image, GdkImage * ximage,
	    int dest_x, int dest_y, int dest_width, int dest_height,
	    int flip_x, int flip_y)
{
    int startx = dest_x, starty = dest_y;
    int width = dest_width, height = dest_height;
    int *origx, *origy;
    int x, y;

    if (ximage->depth != 15 && ximage->depth != 16 && ximage->depth != 24
	&& ximage->depth != 8)
    {
	fprintf(stderr, "cannot scale image: depth = %d, pixelsize = %d\n",
		ximage->depth, image->pixelsize);
	return;
    }

    if (startx >= ximage->width || startx + dest_width <= 0)
	return;
    if (startx < 0)
    {
	width = width + startx;
	startx = 0;
    }

    if (starty >= ximage->height || starty + dest_height <= 0)
	return;
    if (starty < 0)
    {
	height = height + starty;
	starty = 0;
    }

    if (startx + width > ximage->width)
	width = ximage->width - startx;
    if (starty + height > ximage->height)
	height = ximage->height - starty;

    origx = malloc((width + height) * sizeof(int));
    if (!origx)
	return;
    origy = origx + width;

    for (x = 0; x < width; x++)
    {
	origx[x] = ((x + startx - dest_x) * image->xsize) / dest_width;
    }
    if (flip_x)
	for (x = 0; x < width; x++)
	    origx[x] = image->xsize - origx[x] - 1;

    for (y = 0; y < height; y++)
    {
	origy[y] = ((y + starty - dest_y) * image->ysize) / dest_height;
    }
    if (flip_y)
	for (y = 0; y < height; y++)
	    origy[y] = image->ysize - origy[y] - 1;

    if (strncmp(image->mode, "RGB", 3) == 0)
    {
	switch (ximage->depth)
	{
	case 32:
	case 24:
	    image_scale_rgb_24(visual, image, ximage, startx, starty,
			       width, height, origx, origy);
	    break;
	case 16:
	case 15:
	    image_scale_rgb_16(visual, image, ximage, startx, starty,
			       width, height, origx, origy);
	    break;
	case 8:
	    image_scale_rgb_8(visual, image, ximage, startx, starty,
			      width, height, origx, origy);
	    break;
	default:
	    fprintf(stderr, "sketch:scale_image:unsupported depth\n");
	}
    }
    else if (strcmp(image->mode, "L") == 0)
    {
	switch (ximage->depth)
	{
	case 32:
	case 24:
	    image_scale_gray_24(visual, image, ximage, startx, starty,
				width, height, origx, origy);
	    break;
	case 16:
	case 15:
	    image_scale_gray_16(visual, image, ximage, startx, starty,
				width, height, origx, origy);
	    break;
	case 8:
	    image_scale_gray_8(visual, image, ximage, startx, starty,
			       width, height, origx, origy);
	    break;
	default:
	    fprintf(stderr, "sketch:scale_image:unsupported depth\n");
	}
    }

    free(origx);
}

PyObject *
copy_image_to_ximage(PyObject * self, PyObject * args)
{
    ImagingObject * src;
    SKImageObject * dest;
    SKVisualObject * visual;
    int dest_x, dest_y, dest_width, dest_height;

    if (!PyArg_ParseTuple(args, "O!OO!iiii", &SKVisualType, &visual,
			  &src, &SKImageType, &dest,
			  &dest_x, &dest_y, &dest_width, &dest_height))
	return NULL;

    scale_image(visual, src->image, dest->image,
		dest_x, dest_y, abs(dest_width), abs(dest_height),
		dest_width < 0, dest_height < 0);

    Py_INCREF(Py_None);
    return Py_None;
}


static void
image_trafo_rgb_16(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		   SKTrafoObject * trafo, int startx, int starty, int height,
		   int * minx, int * maxx)
{
    long *red_bits = visual->red_bits;
    long *green_bits = visual->green_bits;
    long *blue_bits  = visual->blue_bits;
    short *dest;
    int x, y, desty;
    double sx, sy;
    INT32 ** src = image->image32;
    unsigned char * rgb;

    for (y = 0; y < height; y++)
    {
	if (minx[y] == -1)
	    continue;
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = (short*)((unsigned char*)ximage->mem + desty * ximage->bpl)
	    + minx[y];
	for (x = minx[y]; x <= maxx[y];
	     x++, dest++, sx += trafo->m11, sy += trafo->m21)
	{
	    rgb = (unsigned char*)(src[(int)sy] + (int)sx);
	    *dest = red_bits[(int)rgb[0]]
		| green_bits[(int)rgb[1]]
		| blue_bits[(int)rgb[2]];
	}
    }
}

static void
image_trafo_rgb_24(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		   SKTrafoObject * trafo, int startx, int starty, int height,
		   int * minx, int * maxx)
{
    unsigned char *dest;
    int bpp = ximage->bpp;
    int x, y, desty;
    double sx, sy;
    INT32 ** src = image->image32;
    unsigned char * rgb;
    int red_index = visual->red_index;
    int green_index = visual->green_index;
    int blue_index = visual->blue_index;

    if (ximage->byte_order == GDK_MSB_FIRST)
    {
	red_index = 3 - red_index;
	green_index = 3 - green_index;
	blue_index = 3 - blue_index;
    }

    for (y = 0; y < height; y++)
    {
	if (minx[y] == -1)
	    continue;
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = ((unsigned char*)ximage->mem + desty * ximage->bpl)
	       + bpp * minx[y];
	for (x = minx[y]; x <= maxx[y];
	     x++, dest += bpp, sx += trafo->m11, sy += trafo->m21)
	{
	    rgb = (unsigned char*)(src[(int)sy] + (int)sx);
	    dest[red_index] = rgb[0];
	    dest[green_index] = rgb[1];
	    dest[blue_index] = rgb[2];
	}
    }
}

static void
image_trafo_rgb_8(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		  SKTrafoObject * trafo, int startx, int starty, int height,
		  int * minx, int * maxx)
{
    int x, y, desty;
    double sx, sy;
    INT32 ** src = image->image32;
    unsigned char * rgb;
    long *colors = visual->pseudocolor_table;
    SKDitherInfo *dither_red = visual->dither_red;
    SKDitherInfo *dither_green = visual->dither_green;
    SKDitherInfo *dither_blue = visual->dither_blue;
    SKDitherInfo r, g, b;
    unsigned char **dither_matrix;
    unsigned char *matrix;
    unsigned char *dest;

    for (y = 0; y < height; y++)
    {
	if (minx[y] == -1)
	    continue;
	dither_matrix = visual->dither_matrix[y & 0x7];
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = (unsigned char*)ximage->mem + desty * ximage->bpl + minx[y];
	for (x = minx[y]; x <= maxx[y];
	     x++, dest++, sx += trafo->m11, sy += trafo->m21)
	{
	    rgb = (unsigned char*)(src[(int)sy] + (int)sx);
	    r = dither_red[rgb[0]];
	    g = dither_green[rgb[1]];
	    b = dither_blue[rgb[2]];

	    matrix = dither_matrix[x & 0x7];
	    *dest = colors[r.c[matrix[r.s[1]]] + g.c[matrix[g.s[1]]]
			   + b.c[matrix[b.s[1]]]];
	}
    }
}

static void
image_trafo_gray_16(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		    SKTrafoObject * trafo, int startx, int starty, int height,
		    int * minx, int * maxx)
{
    long *red_bits = visual->red_bits;
    long *green_bits = visual->green_bits;
    long *blue_bits  = visual->blue_bits;
    short *dest;
    int x, y, desty;
    double sx, sy;
    UINT8 ** src = image->image8;
    int gray;

    for (y = 0; y < height; y++)
    {
	if (minx[y] == -1)
	    continue;
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = (short*)((unsigned char*)ximage->mem + desty * ximage->bpl)
	    + minx[y];
	for (x = minx[y]; x <= maxx[y];
	     x++, dest++, sx += trafo->m11, sy += trafo->m21)
	{
	    gray = src[(int)sy][(int)sx];
	    *dest = red_bits[gray] | green_bits[gray] | blue_bits[gray];
	}
    }
}

static void
image_trafo_gray_24(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		    SKTrafoObject * trafo, int startx, int starty, int height,
		    int * minx, int * maxx)
{
    unsigned char *dest;
    int bpp = ximage->bpp;
    int x, y, desty;
    double sx, sy;
    UINT8 ** src = image->image8;
    int red_index = visual->red_index;
    int green_index = visual->green_index;
    int blue_index = visual->blue_index;

    if (ximage->byte_order == GDK_MSB_FIRST)
    {
	red_index = 3 - red_index;
	green_index = 3 - green_index;
	blue_index = 3 - blue_index;
    }

    for (y = 0; y < height; y++)
    {
	if (minx[y] == -1)
	    continue;
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = ((unsigned char*)ximage->mem + desty * ximage->bpl)
	       + bpp * minx[y];
	for (x = minx[y]; x <= maxx[y];
	     x++, dest += bpp, sx += trafo->m11, sy += trafo->m21)
	{
	    dest[red_index] = dest[green_index] = dest[blue_index] \
		= src[(int)sy][(int)sx];
	}
    }
}

static void
image_trafo_gray_8(SKVisualObject * visual, Imaging image, GdkImage * ximage,
		   SKTrafoObject * trafo, int startx, int starty, int height,
		   int * minx, int * maxx)
{
    int x, y, desty;
    double sx, sy;
    UINT8 ** src = image->image8;
    int gray;
    long *colors = visual->pseudocolor_table;
    SKDitherInfo *dither_red = visual->dither_red;
    SKDitherInfo *dither_green = visual->dither_green;
    SKDitherInfo *dither_blue = visual->dither_blue;
    SKDitherInfo r, g, b;
    unsigned char **dither_matrix;
    unsigned char *matrix;
    unsigned char *dest;

    for (y = 0; y < height; y++)
    {
	if (minx[y] == -1)
	    continue;
	dither_matrix = visual->dither_matrix[y & 0x7];
	desty = starty + y;
	sx = trafo->m11 * minx[y] + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * minx[y] + trafo->m22 * desty + trafo->v2;

	dest = (unsigned char*)ximage->mem + desty * ximage->bpl + minx[y];
	for (x = minx[y]; x <= maxx[y];
	     x++, dest++, sx += trafo->m11, sy += trafo->m21)
	{
	    gray = src[(int)sy][(int)sx];
	    r = dither_red[gray];
	    g = dither_green[gray];
	    b = dither_blue[gray];

	    matrix = dither_matrix[x & 0x7];
	    *dest = colors[r.c[matrix[r.s[1]]] + g.c[matrix[g.s[1]]]
			   + b.c[matrix[b.s[1]]]];
	}
    }
}

static void
make_region(SKTrafoObject * trafo, int xsize, int ysize,
	    int startx, int starty, int width, int height,
	    int * pminx, int * pmaxx, GdkRegion * region)
{
    GdkRectangle rect;
    double sx, sy;
    int y, desty;
    int minx, maxx;
    double minxx, minxy, maxxx, maxxy;

    rect.height = 1;

    for (y = 0; y < height; y++)
    {
	pminx[y] = -1;
	desty = starty + y;
	sx = trafo->m11 * startx + trafo->m12 * desty + trafo->v1;
	sy = trafo->m21 * startx + trafo->m22 * desty + trafo->v2;

	if (trafo->m11 > 0)
	{
	    if (sx < 0)
		minxx = ceil(- sx / trafo->m11);
	    else
		minxx = 0;

	    if (sx < xsize)
	    {
		maxxx = floor((xsize - sx) / trafo->m11);
	    }
	    else
	    {
		continue;
	    }
	}
	else if (trafo->m11 < 0)
	{
	    if (sx < 0)
		continue;
	    else
		maxxx = floor(-sx / trafo->m11);
	    if (sx > xsize)
		minxx = ceil((xsize - sx) / trafo->m11);
	    else
		minxx = 0;
	}
	else
	{
	    if (sx < 0 || sx > xsize)
		continue;
	    minxx = 0;
	    maxxx = width;
	}

	if (trafo->m21 > 0)
	{
	    if (sy < 0)
		minxy = ceil(- sy / trafo->m21);
	    else
		minxy = 0;

	    if (sy < ysize)
	    {
		maxxy = floor((ysize - sy) / trafo->m21);
	    }
	    else
		continue;
	}
	else if (trafo->m21 < 0)
	{
	    if (sy < 0)
		continue;
	    else
		maxxy = floor(-sy / trafo->m21);
	    if (sy > ysize)
		minxy = ceil((ysize - sy) / trafo->m21);
	    else
		minxy = 0;
	}
	else
	{
	    if (sy < 0 || sy > ysize)
		continue;
	    minxy = 0;
	    maxxy = width;
	}
	minx = minxx > minxy ? minxx : minxy;

	maxx = maxxx < maxxy ? maxxx : maxxy;
	if (maxx >= width)
	    maxx = width - 1;
	if (maxx < minx)
	    continue;
	maxx += startx;
	minx += startx;

	pminx[y] = minx; pmaxx[y] = maxx;

	rect.x = minx; rect.y = desty; rect.width = maxx - minx + 1;
	gdk_region_union_with_rect(region, &rect);
    }
}

static PyObject *
transform_image(SKVisualObject * visual, SKTrafoObject * trafo,
		Imaging image, GdkImage * ximage, int dest_x, int dest_y,
		int dest_width, int dest_height, GdkRegion * region)
{
    int startx = dest_x, starty = dest_y;
    int width = dest_width, height = dest_height;
    int *minx, *maxx;

    if (ximage->depth != 15 && ximage->depth != 16 && ximage->depth != 24
	&& ximage->depth != 8)
    {
	fprintf(stderr, "cannot copy image: depth = %d, pixelsize = %d\n",
		ximage->depth, image->pixelsize);
	Py_INCREF(Py_None);
	return Py_None;
    }

    if (startx >= ximage->width || startx + dest_width <= 0)
    {
	Py_INCREF(Py_None);
	return Py_None;
    }
    if (startx < 0)
    {
	width = width + startx;
	startx = 0;
    }

    if (starty >= ximage->height || starty + dest_height <= 0)
    {
	Py_INCREF(Py_None);
	return Py_None;
    }
    if (starty < 0)
    {
	height = height + starty;
	starty = 0;
    }

    if (startx + width > ximage->width)
	width = ximage->width - startx;
    if (starty + height > ximage->height)
	height = ximage->height - starty;

    minx = malloc(2 * height * sizeof(int));
    if (!minx)
	return PyErr_NoMemory();
    maxx = minx + height;
    make_region(trafo, image->xsize, image->ysize, startx, starty,
		width, height, minx, maxx, region);

    if (strncmp(image->mode, "RGB", 3) == 0)
    {
	switch (ximage->depth)
	{
	case 32:
	case 24:
	    image_trafo_rgb_24(visual, image, ximage, trafo,
			       startx, starty, height, minx, maxx);
	    break;
	case 16:
	case 15:
	    image_trafo_rgb_16(visual, image, ximage, trafo,
			       startx, starty, height, minx, maxx);
	    break;
	case 8:
	    image_trafo_rgb_8(visual, image, ximage, trafo,
			      startx, starty, height, minx, maxx);
	    break;
	default:
	    fprintf(stderr, "sketch:transform_image:unsupported depth\n");
	}
    }
    else if (strcmp(image->mode, "L") == 0)
    {
	switch (ximage->depth)
	{
	case 32:
	case 24:
	    image_trafo_gray_24(visual, image, ximage, trafo,
				startx, starty, height, minx, maxx);
	    break;
	case 16:
	case 15:
	    image_trafo_gray_16(visual, image, ximage, trafo,
				startx, starty, height, minx, maxx);
	    break;
	case 8:
	    image_trafo_gray_8(visual, image, ximage, trafo,
			       startx, starty, height, minx, maxx);
	    break;
	default:
	    fprintf(stderr, "sketch:transform_image:unsupported depth\n");
	}
    }

    free(minx);

    Py_INCREF(Py_None);
    return Py_None;
}


PyObject *
transform_to_ximage(PyObject * self, PyObject * args)
{
    SKVisualObject * visual;
    ImagingObject * src;
    SKImageObject * dest;
    SKTrafoObject * trafo;
    SKRegionObject * region;
    int dest_x, dest_y, dest_width, dest_height;

    if (!PyArg_ParseTuple(args, "O!O!OO!iiiiO!", &SKVisualType, &visual,
			  sketch_interface->SKTrafoType, &trafo,
			  &src, &SKImageType, &dest,
			  &dest_x, &dest_y, &dest_width, &dest_height,
			  &SKRegionType, &region))
	return NULL;

    return transform_image(visual, trafo, src->image, dest->image,
			   dest_x, dest_y, dest_width, dest_height,
			   region->region);
}

