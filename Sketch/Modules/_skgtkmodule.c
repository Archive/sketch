/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <math.h>
#include <gtk/gtk.h>

#include <Python.h>
#include <structmember.h>

#include "regionobject.h"
#include "imageobject.h"
#include "clipmask.h"
#include "skgtkimage.h"
#include "skvisual.h"
#include "curvedraw.h"

#define SKGTK_MAIN
#ifndef NO_LIBART
#define USE_LIBART
#endif
#include "_skgtkmodule.h"

#ifdef USE_LIBART
/*#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_pixbuf.h>*/
#include "_libartmodule.h"
#endif


static PyObject *
skgtk_widget_visual_type(PyObject * self, PyObject * args)
{
    PyGObject * widget_object;
    GdkVisual * visual;
    
    if (!PyArg_ParseTuple(args, "O!", &PyGObject_Type, &widget_object))
	return NULL;

    visual = gtk_widget_get_visual(GTK_WIDGET(widget_object->obj));

    return PyInt_FromLong(visual->type);
}

static PyObject *
skgtk_window_set_colormap(PyObject * self, PyObject * args)
{
    PyGObject * window_object;
    PyGObject * cmap_object;

    if (!PyArg_ParseTuple(args, "O!O!", &PyGObject_Type, &window_object,
			  &PyGObject_Type, &cmap_object))
	return NULL;

    gdk_window_set_colormap(GDK_WINDOW(window_object->obj),
			    GDK_COLORMAP(cmap_object->obj));

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject*
skgtk_copy_colormap(PyObject * self, PyObject * args)
{
    PyGObject * cmap_object;
    GdkVisual * visual;

    if (!PyArg_ParseTuple(args, "O!", &PyGObject_Type, &cmap_object))
	return NULL;

    visual = gdk_colormap_get_visual(GDK_COLORMAP(cmap_object->obj));
    return pygobject_new(G_OBJECT(gdk_colormap_new(visual, 1)));
}


static PyObject*
skgtk_colormap_alloc_color(PyObject * self, PyObject * args)
{
    PyGObject * cmap_object;
    GdkColor color = {0, 0, 0, 0};
    int red, green, blue;
    int writeable, best_match;

    if (!PyArg_ParseTuple(args, "O!iiiii", &PyGObject_Type, &cmap_object,
			  &(red), &(green), &(blue), &writeable, &best_match))
    {
	return NULL;
    }
    color.red = red;
    color.green = green;
    color.blue = blue;
    
    if (!gdk_colormap_alloc_color(GDK_COLORMAP(cmap_object->obj),
				  &color, writeable, best_match))
    {
	PyErr_SetString(PyExc_RuntimeError, "couldn't allocate color");
	return NULL;
    }
    return pyg_boxed_new(GDK_TYPE_COLOR, &color, TRUE, TRUE);
}


static PyObject *
skgtk_create_region(PyObject * self, PyObject *args)
{
    return SKRegion_FromRegion(gdk_region_new());
}



static PyObject *
skgtk_set_foreground_and_fill(PyObject *self, PyObject *args)
{
    PyObject * pixel_or_pixmap;
    PyGObject * gc_object;
    
    if (!PyArg_ParseTuple(args, "O!O", &PyGObject_Type, &gc_object,
			  &pixel_or_pixmap))
	return NULL;

    if (PyInt_Check(pixel_or_pixmap))
    {
	GdkColor color;
	color.pixel = PyInt_AsLong(pixel_or_pixmap);
	color.red = color.green = color.blue = 0;
	
	gdk_gc_set_foreground(GDK_GC(gc_object->obj), &color);
	gdk_gc_set_fill(GDK_GC(gc_object->obj), GDK_SOLID);
    }
    else if (pygobject_check(pixel_or_pixmap, &PyGObject_Type))
    {
	PyGObject * pixmap_object = (PyGObject*)pixel_or_pixmap;
	if (GDK_IS_PIXMAP(pixmap_object->obj))
	{
	    gdk_gc_set_tile(GDK_GC(gc_object->obj),
			    GDK_PIXMAP(pixmap_object->obj));
	    gdk_gc_set_fill(GDK_GC(gc_object->obj), GDK_TILED);
	}
    }
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
skgtk_set_clip_mask(PyObject *self, PyObject *args)
{
    PyObject * mask;
    PyGObject * gc_object;
    
    if (!PyArg_ParseTuple(args, "O!O", &PyGObject_Type, &gc_object, &mask))
	return NULL;

    if (pygobject_check(mask, &PyGObject_Type))
    {
	gdk_gc_set_clip_mask(GDK_GC(gc_object->obj),
			     GDK_PIXMAP(((PyGObject*)mask)->obj));
    }
    else if (SKRegion_Check(mask))
    {
	gdk_gc_set_clip_region(GDK_GC(gc_object->obj),
			       SKRegion_AsRegion(mask));
    }
    else if (mask == Py_None)
    {
	gdk_gc_set_clip_mask(GDK_GC(gc_object->obj), NULL);
    }
    else
    {
	PyErr_SetString(PyExc_TypeError,
			"mask must be a bitmap, a region or None");
	return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}
    
static PyObject *
skgtk_draw_image(PyObject *self, PyObject *args)
{
    PyGObject *window, *gc;
    PyObject *image;
    gint xsrc, ysrc, xdest, ydest, width, height;

    if (!PyArg_ParseTuple(args, "O!O!O!iiiiii", &PyGObject_Type, &window,
			  &PyGObject_Type, &gc, &SKImageType, &image,
			  &xsrc, &ysrc, &xdest, &ydest, &width, &height))
	return NULL;
    
    gdk_draw_image(GDK_DRAWABLE(window->obj), GDK_GC(gc->obj),
		   SKImage_AsImage(image), xsrc, ysrc,
		   xdest, ydest, width, height);

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject *
skgtk_draw_bitmap(PyObject *self, PyObject *args)
{
    PyGObject * window, *gc, *bitmap;
    gint xdest, ydest;
    gint width, height;

    if (!PyArg_ParseTuple(args, "O!O!O!ii", &PyGObject_Type, &window,
			  &PyGObject_Type, &gc, &PyGObject_Type, &bitmap,
			  &xdest, &ydest))
	return NULL;

    if (!GDK_IS_PIXMAP(bitmap->obj)
	|| gdk_drawable_get_depth(GDK_DRAWABLE(bitmap->obj)) > 1)
    {
	PyErr_SetString(PyExc_TypeError, "bitmap expected");
	return NULL;
    }

    gdk_drawable_get_size(GDK_DRAWABLE(bitmap->obj), &width, &height);


    gdk_gc_set_ts_origin(GDK_GC(gc->obj), xdest, ydest);
    gdk_gc_set_stipple(GDK_GC(gc->obj), GDK_PIXMAP(bitmap->obj));
    gdk_gc_set_fill(GDK_GC(gc->obj), GDK_STIPPLED);
    gdk_draw_rectangle(GDK_DRAWABLE(window->obj), GDK_GC(gc->obj), TRUE,
		       xdest, ydest, width, height);
    gdk_gc_set_fill(GDK_GC(gc->obj), GDK_SOLID);

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject *
skgtk_clear_area(PyObject * self, PyObject * args)
{
    PyGObject * window;
    gint x, y, width, height;

    if (!PyArg_ParseTuple(args, "O!iiii", &PyGObject_Type, &window,
			  &x, &y, &width, &height))
	return NULL;

    gdk_window_clear_area(GDK_WINDOW(window->obj), x, y, width, height);

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject *
skgtk_copy_area(PyObject * self, PyObject * args)
{
    PyGObject *window, *srcwin, *gc;
    gint src_x, src_y, dest_x, dest_y;
    guint width, height;

    if (!PyArg_ParseTuple(args, "O!O!iiO!iiii", &PyGObject_Type, &window,
			  &PyGObject_Type, &gc,
			  &dest_x, &dest_y,
			  &PyGObject_Type, &srcwin, &src_x, &src_y,
			  &width, &height))
	return NULL;

    gdk_window_copy_area(GDK_WINDOW(window->obj), GDK_GC(gc->obj),
			 dest_x, dest_y,
			 GDK_WINDOW(srcwin->obj), src_x, src_y, width, height);

    Py_INCREF(Py_None);
    return Py_None;
}

#ifdef USE_LIBART
/*
  typedef struct {
    PyObject_HEAD
    ArtPixBuf * pixbuf;
} SKArtPixBufObject;
*/

static PyObject * PSKArtPixBufType;

static PyObject *
skgtk_draw_artpixbuf(PyObject *self, PyObject *args)
{
    PyGObject *window, *gc;
    SKArtPixBufObject * pixbuf;
    gint x, y;

    if (!PyArg_ParseTuple(args, "O!O!O!ii", &PyGObject_Type, &window,
			  &PyGObject_Type, &gc, PSKArtPixBufType, &pixbuf,
			  &x, &y))
	return NULL;

    gdk_rgb_init();
    if (pixbuf->artpixbuf->n_channels == 3)
    {
	gdk_draw_rgb_image(GDK_DRAWABLE(window->obj), GDK_GC(gc->obj),
			   x, y,
			   pixbuf->artpixbuf->width,
			   pixbuf->artpixbuf->height,
			   GDK_RGB_DITHER_NORMAL,
			   pixbuf->artpixbuf->pixels,
			   pixbuf->artpixbuf->rowstride);
    }
    else
    {
	gdk_draw_rgb_32_image(GDK_DRAWABLE(window->obj), GDK_GC(gc->obj),
			      x, y,
			      pixbuf->artpixbuf->width,
			      pixbuf->artpixbuf->height,
			      GDK_RGB_DITHER_NORMAL,
			      pixbuf->artpixbuf->pixels,
			      pixbuf->artpixbuf->rowstride);
    }

    Py_INCREF(Py_None);
    return Py_None;
}

#endif

/*
struct _GtkAdjustment
{
  GtkData data;
  
  gfloat lower;
  gfloat upper;
  gfloat value;
  gfloat step_increment;
  gfloat page_increment;
  gfloat page_size;
};
*/

#define OFF(x) offsetof(GtkAdjustment, x)
static struct memberlist adjustment_memberlist[] = {
    {"lower",		T_FLOAT,	OFF(lower)},
    {"upper",		T_FLOAT,	OFF(upper)},
    {"value",		T_FLOAT,	OFF(value)},
    {"step_increment",	T_FLOAT,	OFF(step_increment)},
    {"page_increment",	T_FLOAT,	OFF(page_increment)},
    {"page_size",	T_FLOAT,	OFF(page_size)},
    {NULL} 
};


static PyObject *
skgtk_set_adjustment(PyObject * self, PyObject * args)
{
    PyGObject *adjustment;
    PyObject *dict, *key, *value;
    int pos;

    if (!PyArg_ParseTuple(args, "O!O!", &PyGObject_Type, &adjustment,
			  &PyDict_Type, &dict))
	return NULL;

    pos = 0;
    while (PyDict_Next(dict, &pos, &key, &value))
    {
	if (PyString_Check(key) && PyNumber_Check(value))
	{
	    if (PyMember_Set((char*)(GTK_ADJUSTMENT(adjustment->obj)),
			     adjustment_memberlist, PyString_AsString(key),
			     value) == -1)
		return NULL;
	    
	}
	else
	{
	    PyErr_SetString(PyExc_TypeError,
		       "dict with string keys and number values required");
	    return NULL;
	}
    }

    Py_INCREF(Py_None);
    return Py_None;
}



static PyObject *
draw_grid(PyObject * self, PyObject * arg)
{
    double xwidth, ywidth;
    double orig_x, orig_y;
    PyGObject *gc_object, *window_object;
    int nx, ny;
    int ix, iy;
    GdkPoint *points, *current;

    if (!PyArg_ParseTuple(arg, "O!O!ddddii",
			  &PyGObject_Type, &window_object,
			  &PyGObject_Type, &gc_object,
			  &orig_x, &orig_y, &xwidth, &ywidth, &nx, &ny))
	return NULL;

    points = malloc(sizeof(GdkPoint) * nx * ny);

    current = points;
    for (ix = 0; ix < nx; ix++)
    {
	for (iy = 0; iy < ny; iy++)
	{
	    current->x = (int)rint(orig_x + xwidth * ix);
	    current->y = (int)rint(orig_y + ywidth * iy);
	    current++;
	}
    }
    gdk_draw_points(GDK_DRAWABLE(window_object->obj), GDK_GC(gc_object->obj),
		    points, nx * ny);
    free(points);

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject *
get_pixel(PyObject * self, PyObject * arg)
{
    GdkImage  * image;
    PyGObject * window_object;
	 
    int		x, y;
    int		retval;

    if (!PyArg_ParseTuple(arg, "O!ii", &PyGObject_Type, &window_object,
			  &x, &y))
	return NULL;

    image = gdk_image_get(GDK_DRAWABLE(window_object->obj), x, y, 1, 1);
    if (!image)
    {
	fprintf(stderr, "Warning! skaux.GetPixel: image == NULL");
	retval = 0;
    }
    else
    {
	retval = gdk_image_get_pixel(image, 0, 0);
	gdk_image_destroy(image);
    }

    return PyInt_FromLong(retval);
}


/*
 *	xlfd_char_range
 */

PyObject *
xlfd_char_range(PyObject * self, PyObject * args)
{
    unsigned char * text;
    int len;
    PyObject * result;
    int idx, count;
    char used[256];
    char * cur;
    char * ranges;

    if (!PyArg_ParseTuple(args, "s#", &text, &len))
	return NULL;

    if (len == 0)
    {
	return PyString_FromString("");
    }

    for (idx = 0; idx < 256; idx++)
	used[idx] = 0;

    for (idx = 0; idx < len; idx++)
	used[(int)(text[idx])] = 1;

    count = 0;
    for (idx = 0; idx < 256; idx++)
	if (used[idx])
	    count++;

    ranges = malloc(4 * count + 1);
    if (!ranges)
	return NULL;

    cur = ranges;
    idx = 0;
    while (idx < 256)
    {
	if (used[idx])
	{
	    int first = idx, last;
	    while (idx < 256 && used[idx])
		idx++;
	    last = idx - 1;
	    if (first == last)
		cur += sprintf(cur, " %d", first);
	    else
		cur += sprintf(cur, " %d_%d", first, last);
	}
	else
	    idx++;
    }
    
    result = PyString_FromString(ranges + 1);
    free(ranges);
    return result;
}


static PyMethodDef skgtk_functions[] = {
    /* skgtk */
    {"widget_visual_type",	skgtk_widget_visual_type,	METH_VARARGS},
    {"window_set_colormap",	skgtk_window_set_colormap,	METH_VARARGS},
    {"copy_colormap",		skgtk_copy_colormap,		METH_VARARGS},
    {"allocate_color",		skgtk_colormap_alloc_color,	METH_VARARGS},
    {"create_region",		skgtk_create_region,		METH_VARARGS},
    {"set_foreground_and_fill", skgtk_set_foreground_and_fill,	METH_VARARGS},
    {"set_clip_mask",		skgtk_set_clip_mask,		METH_VARARGS},
    {"draw_image",		skgtk_draw_image,		METH_VARARGS},
    {"draw_bitmap",		skgtk_draw_bitmap,		METH_VARARGS},
    {"clear_area",		skgtk_clear_area,		METH_VARARGS},
    {"copy_area",		skgtk_copy_area,		METH_VARARGS},
    {"set_adjustment",		skgtk_set_adjustment,		METH_VARARGS},
    {"xlfd_char_range",		xlfd_char_range,		METH_VARARGS},
    {"GetPixel",		get_pixel,			METH_VARARGS},
    {"DrawGrid",		draw_grid,			METH_VARARGS},
    
    {"create_image",		SKImage_PyNew,			METH_VARARGS},

    {"draw_transformed",	SKCurve_PyDrawTransformed,	METH_VARARGS},
    {"draw_multipath",		SKCurve_PyDrawMultipath,	METH_VARARGS},
    {"multicurve_region",	SKCurve_PyMultipathRegion,	METH_VARARGS},
    {"DrawBezier",		SKCurve_PyDrawBezierSegment,	METH_VARARGS},

    {"polygon_region",		SKRegion_PyFromPolygon,		METH_VARARGS},

    /* clipmask */
    {"intersect_clipmasks",	SKClipMask_PyIntersectMasks,	METH_VARARGS},

    /* image */
    {"copy_image_to_ximage",	copy_image_to_ximage,		METH_VARARGS},
    {"transform_to_ximage",	transform_to_ximage,		METH_VARARGS},
#ifdef USE_LIBART
    {"draw_artpixbuf",		skgtk_draw_artpixbuf,		METH_VARARGS},
#endif
    /* visual */
    {"CreateVisual",		SKVisual_PyCreateVisual,	METH_VARARGS},
   /* */
    {NULL,		NULL} 
};



/*
 */

Sketch_Interface * sketch_interface;

PyTypeObject *SKGTK_PyGObject_Type;

/*
 *	Init module
 */
static void
add_int(PyObject * dict, char * name, int i)
{
    PyObject *v;
    
    v = Py_BuildValue("i", i);
    if (v)
    {
	PyDict_SetItemString(dict, name, v);
	Py_DECREF(v);
    }
}

void
init_skgtk(void)
{
    PyObject * d, *m, *module;

    /* Patch object types */
    SKImageType.ob_type = &PyType_Type;
    SKRegionType.ob_type = &PyType_Type;
    SKVisualType.ob_type = &PyType_Type;

    m = Py_InitModule("_skgtk", skgtk_functions);
    d = PyModule_GetDict(m);

    /**/
    PyDict_SetItemString(d, "SKRegionType", (PyObject*)&SKRegionType);

#define ADD_INT(name) add_int(d, #name, name)
    /* add some gtk/gdk constants */
    ADD_INT(GDK_VISUAL_STATIC_GRAY);
    ADD_INT(GDK_VISUAL_GRAYSCALE);
    ADD_INT(GDK_VISUAL_STATIC_COLOR);
    ADD_INT(GDK_VISUAL_PSEUDO_COLOR);
    ADD_INT(GDK_VISUAL_TRUE_COLOR);
    ADD_INT(GDK_VISUAL_DIRECT_COLOR);
    ADD_INT(GDK_IMAGE_NORMAL);
    ADD_INT(GDK_IMAGE_SHARED);
    ADD_INT(GDK_IMAGE_FASTEST);

    /* import some objects from gobject */
    if ((module = PyImport_ImportModule("gobject")) != NULL) {
        PyObject *moddict = PyModule_GetDict(module);

        SKGTK_PyGObject_Type = (PyTypeObject *)PyDict_GetItemString(moddict,
							       "GObject");
    }
    else
    {
        Py_FatalError("could not import gobject");
        return;
    }

    /* import some objects from pygtk */
    init_pygtk();

    /* import some objects from _sketch */
    module = PyImport_ImportModule("Sketch._sketch");
    if (module)
    {
	PyObject * r;
	
	r = PyObject_GetAttrString(module, "interface");
	if (r)
	{
	    sketch_interface = PyCObject_AsVoidPtr(r);
	}
	Py_DECREF(r);
    }

#ifdef USE_LIBART
    /* import some objects from _libart */
    module = PyImport_ImportModule("Sketch._libart");
    if (module)
    {
	PSKArtPixBufType = PyObject_GetAttrString(module, "SKArtPixBufType");
    }
#endif
}
