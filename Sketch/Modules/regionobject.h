/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1998, 1999, 2002 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef SKREGION_H
#define SKREGION_H

#include <gdk/gdk.h>


typedef struct {
    PyObject_HEAD
    GdkRegion * region;
} SKRegionObject;


extern PyTypeObject SKRegionType;

#define SKRegion_Check(x) 	((x)->ob_type == &SKRegionType)

extern GdkRegion * SKRegion_AsRegion Py_PROTO((PyObject *));

extern PyObject * SKRegion_FromRegion(GdkRegion*);

PyObject * SKRegion_PyFromPolygon(PyObject *self, PyObject *args);

/* union in place */
extern void SKRegion_UnionWithRegion(SKRegionObject*,GdkRegion*);



#endif /* SKREGION_H */
