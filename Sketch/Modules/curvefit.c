/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1998, 1999 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <math.h>
#include "skpoint.h"
#include "curvefit.h"
#include "curvelow.h"

#ifndef PI
#define PI 3.14159265358979323846264338327
#endif


/*
 *  Find the corners in a freehand curve
 */
static int
extract_point(PyObject * points, int i, double * x, double * y)
{
    PyObject * temp;

    temp = PySequence_GetItem(points, i);
    if (temp)
    {
	if (!skpoint_extract_xy(temp, x, y))
	{
	    PyErr_SetString(PyExc_TypeError,
		      "point list elements must be sequences of two numbers");
	    Py_DECREF(temp);
	    return 0;
	}
	Py_DECREF(temp);
    }
    else
	return 0;

    return 1;
}

PyObject*
SKCurve_PyFindFreehandCorners(PyObject* self, PyObject * args)
{
    PyObject * points;
    double max_angle, corner_radius;
    int num_points, cur;
    PyObject * corners;

    if (!PyArg_ParseTuple(args, "Odd", &points, &max_angle, &corner_radius))
	return NULL;

    num_points = PySequence_Length(points);

    corners = PyList_New(0);
    if (!corners)
	return NULL;

    if (PyList_Append(corners, PyInt_FromLong(0)) < 0)
	goto fail;

    for (cur = 1; cur < num_points - 1; cur++)
    {
	double x0, y0, x1, y1;
	double tangent1x, tangent1y, tangent2x, tangent2y;
	double dx, dy, length, angle, product;
	int other;
	int num_neighbors;

	if (!extract_point(points, cur, &x0, &y0))
	    goto fail;

	other = cur - 1;
	tangent1x = tangent1y = 0.0;
	num_neighbors = 0;
	while (other >= 0)
	{
	    if (!extract_point(points, other, &x1, &y1))
		goto fail;
	    dx = x0 - x1;
	    dy = y0 - y1;
	    if (hypot(dx, dy) > corner_radius && num_neighbors > 0)
		break;
	    tangent1x += dx;
	    tangent1y += dy;
	    num_neighbors += 1;
	    other -= 1;
	}
	tangent1x /= num_neighbors;
	tangent1y /= num_neighbors;
	length = hypot(tangent1x, tangent1y);
	if (length)
	{
	    tangent1x /= length;
	    tangent1y /= length;
	}
	else
	    continue;

	other = cur + 1;
	tangent2x = tangent2y = 0.0;
	num_neighbors = 0;
	while (other < num_points)
	{
	    if (!extract_point(points, other, &x1, &y1))
		goto fail;
	    dx = x1 - x0;
	    dy = y1 - y0;
	    if (hypot(dx, dy) > corner_radius && num_neighbors > 0)
		break;
	    tangent2x += dx;
	    tangent2y += dy;
	    num_neighbors += 1;
	    other += 1;
	}
	tangent2x /= num_neighbors;
	tangent2y /= num_neighbors;
	length = hypot(tangent2x, tangent2y);
	if (length)
	{
	    tangent2x /= length;
	    tangent2y /= length;
	}
	else
	    continue;

	product = tangent1x * tangent2x + tangent1y * tangent2y;
	if (product >= -1 && product <= 1)
	    angle = acos(product);
	else if (product > 1)
	    angle = PI;
	else
	    angle = 0.0;
	    
	if (angle > max_angle)
	{
	    PyObject * temp = PyInt_FromLong(cur);
	    if (!temp)
		goto fail;
	    if (PyList_Append(corners, temp) < 0)
		goto fail;
	}
    }

    {
	PyObject * temp = PyInt_FromLong(num_points - 1);
	if (!temp)
	    goto fail;
	if (PyList_Append(corners, temp) < 0)
	    goto fail;
    }
    return corners;

 fail:
    Py_DECREF(corners);
    return NULL;
}

/*
 *  Curve fitting
 */


static int
extract_point_and_param(PyObject * points, PyObject * params, int i,
			double * x, double * y, double * t)
{
    PyObject * temp;

    temp = PySequence_GetItem(params, i);
    if (temp)
    {
	*t = PyFloat_AsDouble(temp);
	Py_DECREF(temp);
    }
    else
	return 0;

    temp = PySequence_GetItem(points, i);
    if (temp)
    {
	if (!skpoint_extract_xy(temp, x, y))
	{
	    PyErr_SetString(PyExc_TypeError,
		      "point list elements must be sequences of two numbers");
	    Py_DECREF(temp);
	    return 0;
	}
	Py_DECREF(temp);
    }
    else
	return 0;

    return 1;
}


static int
fit_bezier_with_tangents(PyObject * points, PyObject * params,
			 double tangent1x, double tangent1y,
			 double tangent2x, double tangent2y,
			 double * alpha1, double * alpha2)
{
    double c11 = 0.0, c12 = 0.0, c21 = 0.0, c22 = 0.0;
    double distsum1 = 0.0, distsum2 = 0.0;
    double det;
    double startx, starty;
    double endx, endy;
    int i, length;
    double u;

    length = PySequence_Length(points);
    if (!extract_point_and_param(points, params, 0, &startx, &starty, &u))
	return 0;
    if (!extract_point_and_param(points, params, length - 1, &endx, &endy, &u))
	return 0;

    for (i = 0; i < length; i++)
    {
	double u2, u3;
	double v;
	double x, y;
	double b1, b2;
	double distx, disty;
	double factor1, factor2;

	if (!extract_point_and_param(points, params, i, &x, &y, &u))
	    return 0;

	u2 = u * u;
	u3 = u2 * u;
	v = 1.0 - u;
	b1 = 3 * u * v * v;
	b2 = 3 * u * u * v;
	c11 += b1 * b1; c22 += b2 * b2; c12 += b1 * b2;

	factor1 = (2 * u3 - 3 * u2 + 1);
	factor2 = (3 * u2 - 2 * u3);
	distx = x - (factor1 * startx + factor2 * endx);
	disty = y - (factor1 * starty + factor2 * endy);

	distsum1 += b1 * (distx * tangent1x + disty * tangent1y);
	distsum2 += b2 * (distx * tangent2x + disty * tangent2y);
    }

    c12 = c12 * (tangent1x * tangent2x + tangent1y * tangent2y);
    c21 = c12;

    det = c11 * c22 - c12 * c21;

    if (det != 0.0)
    {
	double inv11, inv12, inv21, inv22;
	inv11 = c22 / det;
	inv12 = -c12 / det;
	inv21 = -c21 / det;
	inv22 = c11 / det;

	*alpha1 = distsum1 * inv11 + distsum2 * inv12;
	*alpha2 = distsum1 * inv21 + distsum2 * inv22;
    }
    else
    {
	*alpha1 = *alpha2 = 0.0;
    }

    return 1;
}

PyObject *
SKCurve_PyFitBezierWithTangents(PyObject * self, PyObject * args)
{
    PyObject * point_list, * param_list;
    PyObject * tangent1, * tangent2;
    double tangent1x, tangent1y;
    double tangent2x, tangent2y;
    double alpha1, alpha2;
    
    if (!PyArg_ParseTuple(args, "OOOO", &point_list, &param_list,
			  &tangent1, &tangent2))
	return NULL;

    if (!skpoint_extract_xy(tangent1, &tangent1x, &tangent1y))
    {
	PyErr_SetString(PyExc_TypeError,
			"tangent must be a sequence of two numbers");
	return NULL;
    }
    if (!skpoint_extract_xy(tangent2, &tangent2x, &tangent2y))
    {
	PyErr_SetString(PyExc_TypeError,
			"tangent must be a sequence of two numbers");
	return NULL;
    }

    if (PySequence_Check(point_list) && PySequence_Check(param_list)
	&& PySequence_Length(point_list) == PySequence_Length(param_list))
    {
	if (!fit_bezier_with_tangents(point_list, param_list,
				      tangent1x, tangent1y,
				      tangent2x, tangent2y,
				      &alpha1, &alpha2))
	    return NULL;
    }
    else
    {
	return PyErr_Format(PyExc_TypeError,
	       "points and params must have be sequences of the same length");
    }

    return Py_BuildValue("dd", alpha1, alpha2);
}	


/* reestimate parameters */

#define EVAL(coeff, t) (((coeff[0]*(t)+coeff[1])*(t) +coeff[2])*(t) + coeff[3])
#define EVAL1(coeff, t) ((3*coeff[0]*(t) + 2*coeff[1])*(t) + coeff[2])
#define EVAL2(coeff, t) (6*coeff[0]*(t) + 2*coeff[1])
static int
refine_params(PyObject * points, PyObject * params, PyObject * result,
	      double * x, double * y)
{
    double coeff_x[4], coeff_y[4];
    int i, length;

    bezier_coefficients(coeff_x, coeff_y, x, y);

    length = PySequence_Length(params);
    for (i = 0; i < length; i++)
    {
	PyObject * temp;
	double t;      /* initial estimate of parameter */
	double px, py; /* sampled point */
	double x0, y0; /* point on curve corresponding to t */
	double x1, y1; /* first derivative */
	double x2, y2; /* second derivative */
	double dx, dy; /* distance from sample to curvepoint */
	double denom;

	if (!extract_point_and_param(points, params, i, &px, &py, &t))
	    return -1;
	
	x0 = EVAL(coeff_x, t);	y0 = EVAL(coeff_y, t);
	x1 = EVAL1(coeff_x, t);	y1 = EVAL1(coeff_y, t);
	x2 = EVAL2(coeff_x, t);	y2 = EVAL2(coeff_y, t);
	dx = x0 - px; dy = y0 - py;
	denom = x1 * x1 + y1 * y1 + dx * x2 + dy * y2;
	if (denom)
	{
	    t -= (dx * x1 + dy * y1) / denom;
	    if (t < 0)
		t = 0;
	    else if (t > 1)
		t = 1;
	    if (hypot(dx, dy) < hypot(EVAL(coeff_x,t)-px, EVAL(coeff_y,t)-py))
		return 0;
	}
	else
	    return 0;

	temp = PyFloat_FromDouble(t);
	if (temp)
	{
	    if (PyList_Append(result, temp) < 0)
	    {
		Py_DECREF(temp);
		return -1;
	    }
	    Py_DECREF(temp);
	}
	else
	    return -1;

    }

    return 1;
}

PyObject *
SKCurve_PyRefineParameters(PyObject * self, PyObject * args)
{
    PyObject * point_list, * param_list;
    PyObject * result;
    PyObject * p[4];
    double x[4], y[4];
    int i, ret;
    
    if (!PyArg_ParseTuple(args, "OOOOOO", &point_list, &param_list,
			  &p[0], &p[1], &p[2], &p[3]))
	return NULL;

    for (i = 0; i < 4; i++)
    {
	if (!skpoint_extract_xy(p[i], x+i, y+i))
	    return PyErr_Format(PyExc_TypeError,
				"points must be sequences of two numbers");
    }

    result = PyList_New(0);
    if (!result)
	return NULL;

    ret = refine_params(point_list, param_list, result, x, y);
    if (ret < 0)
    {
	Py_DECREF(result);
	return NULL;
    }
    else if (ret == 0)
    {
	Py_DECREF(result);
	result = Py_None;
	Py_INCREF(Py_None);
    }
    return result;
}


/*
 *
 */

PyObject*
SKCurve_PyFreehandDistances(PyObject * self, PyObject * args)
{
    PyObject * result;
    PyObject * point_list, * param_list;
    PyObject * p[4];
    double x[4], y[4];
    double coeff_x[4], coeff_y[4];
    int i, length;

    if (!PyArg_ParseTuple(args, "OOOOOO", &point_list, &param_list,
			  &p[0], &p[1], &p[2], &p[3]))
	return NULL;

    for (i = 0; i < 4; i++)
    {
	if (!skpoint_extract_xy(p[i], x+i, y+i))
	    return PyErr_Format(PyExc_TypeError,
				"points must be sequences of two numbers");
    }
    
    length = PySequence_Length(point_list);
    result = PyList_New(length);
    if (!result)
	return NULL;    
    bezier_coefficients(coeff_x, coeff_y, x, y);

    for (i = 0; i < length; i++)
    {
	PyObject * temp;
	double t;      /* initial estimate of parameter */
	double px, py; /* sampled point */
	double x0, y0; /* point on curve corresponding to t */

	if (!extract_point_and_param(point_list, param_list, i, &px, &py, &t))
	{
	    Py_DECREF(result);
	    return NULL;
	}
	
	x0 = EVAL(coeff_x, t);	y0 = EVAL(coeff_y, t);
	temp = PyFloat_FromDouble(hypot(x0 - px, y0 - py));
	if (!temp)
	{
	    Py_DECREF(result);
	    return NULL;
	}
	PyList_SetItem(result, i, temp);
    }

    return result;
}

/*
 *
 */

PyObject*
SKCurve_PyFreehandEstimateParams(PyObject * self, PyObject * args)
{
    PyObject * result = NULL;
    PyObject * point_list;
    int i, length;
    double * distances = NULL;
    double sum, lastx, lasty;

    if (!PyArg_ParseTuple(args, "O", &point_list))
	return NULL;

    length = PySequence_Length(point_list);
    distances = malloc(length * sizeof(double));
    
    result = PyList_New(length);
    if (!result)
	return NULL;    

    sum = 0.0;
    distances[0] = 0.0;
    if (!extract_point(point_list, 0, &lastx, &lasty))
	goto fail;
    
    for (i = 1; i < length; i++)
    {
	double px, py; /* sampled point */

	if (!extract_point(point_list, i, &px, &py))
	    goto fail;
	
	distances[i] = distances[i - 1] + hypot(px - lastx, py - lasty);
	lastx = px;
	lasty = py;
    }

    sum += distances[length - 1];
    if (!sum)
    {
	PyErr_SetString(PyExc_ZeroDivisionError, "sum of distances is zero");
	goto fail;
    }

    for (i = 0; i < length; i++)
    {
	PyObject * temp;
	
	temp = PyFloat_FromDouble(distances[i] /= sum);
	if (!temp)
	    goto fail;
	PyList_SetItem(result, i, temp);
    }
    
    free(distances);

    return result;

 fail:
    if (distances)
	free(distances);
    Py_XDECREF(result);
    return NULL;
}

