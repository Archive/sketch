/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1998, 1999, 2000, 2003, 2004 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "Python.h"
#include "regionobject.h"

PyObject *
SKRegion_FromRegion(GdkRegion * region)
{
    SKRegionObject  *self;
    self = PyObject_NEW(SKRegionObject,  &SKRegionType);
    if (self == NULL)
	return NULL;
    self->region  = region;
    return (PyObject*)self;
}

PyObject *
SKRegion_PyFromPolygon(PyObject *self, PyObject *args)
{
    PyObject *seq, *item;
    int fillrule, npoints, i;
    GdkPoint *points;
    GdkRegion * region;

    if (!PyArg_ParseTuple(args, "Oi", &seq, &fillrule))
	return NULL;
    if (!PySequence_Check(seq))
    {
	PyErr_SetString(PyExc_TypeError, "first argument not a sequence");
	return NULL;
    }
    npoints = PySequence_Length(seq);
    points = g_new(GdkPoint, npoints);
    for (i = 0; i < npoints; i++)
    {
	item = PySequence_GetItem(seq, i);
	if (!PyArg_ParseTuple(item, "ii", &(points[i].x), &(points[i].y))) {
	    Py_DECREF(item);
	    PyErr_Clear();
	    PyErr_SetString(PyExc_TypeError, "sequence member not a 2-tuple");
	    g_free(points);
	    return NULL;
	}
	Py_DECREF(item);
    }
    region = gdk_region_polygon(points, npoints, fillrule);
    g_free(points);

    return SKRegion_FromRegion(region);
}



static PyObject *
region_ClipBox(SKRegionObject *self, PyObject *args)
{
    GdkRectangle r;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;
    gdk_region_get_clipbox(self->region, &r);
    return Py_BuildValue("(iiii)", r.x, r.y, r.width, r.height);
}

static PyObject *
region_EmptyRegion(SKRegionObject *self, PyObject *args)
{
    if (!PyArg_ParseTuple(args, ""))
	return NULL;
    return PyInt_FromLong(gdk_region_empty(self->region));
}

static PyObject *
region_EqualRegion(SKRegionObject *self, PyObject *args)
{
    SKRegionObject *r;
    if (!PyArg_ParseTuple(args, "O!", &SKRegionType, &r))
	return NULL;
    return PyInt_FromLong(gdk_region_equal(self->region, r->region));
}

static PyObject *
region_PointInRegion(SKRegionObject *self, PyObject *args)
{
    int x, y;
    if (!PyArg_ParseTuple(args, "ii", &x, &y))
	return NULL;
    return PyInt_FromLong(gdk_region_point_in(self->region, x, y));
}

static PyObject *
region_RectInRegion(SKRegionObject *self, PyObject *args)
{
    GdkRectangle r;
    if (!PyArg_ParseTuple(args, "iiii", &r.x, &r.y, &r.width, &r.height))
	return NULL;
    return PyInt_FromLong(gdk_region_rect_in(self->region, &r));
}

static PyObject *
region_IntersectRegion(SKRegionObject *self, PyObject *args)
{
    SKRegionObject * other;

    if (!PyArg_ParseTuple(args, "O!", &SKRegionType, &other))
	return NULL;

    gdk_region_intersect(self->region, other->region);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
region_UnionRegion(SKRegionObject *self, PyObject *args)
{
    SKRegionObject *r;

    if (!PyArg_ParseTuple(args, "O!", &SKRegionType, &r))
	return NULL;

    gdk_region_union(self->region, r->region);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
region_UnionRectWithRegion(SKRegionObject *self, PyObject *args)
{
    GdkRectangle r;

    if (!PyArg_ParseTuple(args, "iiii", &r.x, &r.y, &r.width, &r.height))
	return NULL;

    gdk_region_union_with_rect(self->region, &r);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
region_SubtractRegion(SKRegionObject *self, PyObject *args)
{
    SKRegionObject *r;

    if (!PyArg_ParseTuple(args, "O!", &SKRegionType, &r))
	return NULL;

    gdk_region_subtract(self->region, r->region);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
region_XorRegion(SKRegionObject *self, PyObject *args)
{
    SKRegionObject *r;

    if (!PyArg_ParseTuple(args, "O!", &SKRegionType, &r))
	return NULL;

    gdk_region_xor(self->region, r->region);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
region_OffsetRegion(SKRegionObject *self, PyObject *args)
{
    gint dx, dy;
    if (!PyArg_ParseTuple(args, "ii", &dx, &dy))
	return NULL;
    gdk_region_offset(self->region, dx, dy);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
region_ShrinkRegion(SKRegionObject *self, PyObject *args)
{
    gint dx, dy;
    if (!PyArg_ParseTuple(args, "ii", &dx, &dy))
	return NULL;
    gdk_region_shrink(self->region, dx, dy);
    Py_INCREF(Py_None);
    return Py_None;
}


static PyMethodDef region_methods[] = {
    {"ClipBox",		(PyCFunction)region_ClipBox,		METH_VARARGS},
    {"EmptyRegion",	(PyCFunction)region_EmptyRegion,	METH_VARARGS},
    {"EqualRegion",	(PyCFunction)region_EqualRegion,	METH_VARARGS},
    {"IntersectRegion",	(PyCFunction)region_IntersectRegion,	METH_VARARGS},
    {"OffsetRegion",	(PyCFunction)region_OffsetRegion,	METH_VARARGS},
    {"PointInRegion",	(PyCFunction)region_PointInRegion,	METH_VARARGS},
    {"RectInRegion",	(PyCFunction)region_RectInRegion,	METH_VARARGS},
    {"ShrinkRegion",	(PyCFunction)region_ShrinkRegion,	METH_VARARGS},
    {"SubtractRegion",	(PyCFunction)region_SubtractRegion,	METH_VARARGS},
    {"UnionRectWithRegion", (PyCFunction)region_UnionRectWithRegion,
     METH_VARARGS},
    {"UnionRegion",	(PyCFunction)region_UnionRegion,	METH_VARARGS},
    {"XorRegion",	(PyCFunction)region_XorRegion,		METH_VARARGS},
    {0, 0} 
};

static
PyObject * region_getattr(SKRegionObject  *self, char *name)
{
    return Py_FindMethod(region_methods, (PyObject *)self, name);
}

static void
region_dealloc(SKRegionObject  *self)
{
    gdk_region_destroy(self->region);
    PyMem_DEL(self);
}


PyTypeObject SKRegionType  = {
	PyObject_HEAD_INIT(NULL)
	0,			/*ob_size*/
	"SKRegion" ,		/*tp_name*/
	sizeof(SKRegionObject),	/*tp_size*/
	0,			/*tp_itemsize*/
	(destructor)region_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)region_getattr , /*tp_getattr*/
	(setattrfunc)0 ,	/*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
};



GdkRegion *
SKRegion_AsRegion(PyObject *self)
{
    if (self && SKRegion_Check(self))
	return ((SKRegionObject  *)self)-> region ;
    PyErr_BadInternalCall();
    return NULL;
}

void
SKRegion_UnionWithRegion(SKRegionObject * self, GdkRegion *region)
{
    gdk_region_union(self->region, region);
}
