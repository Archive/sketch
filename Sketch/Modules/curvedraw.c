/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/*
 *	Draw bezier paths on gdk
 */

#include <math.h>
#include <Python.h>
#include <gdk/gdk.h>

#include "skrect.h"
#include "sktrafo.h"
#include "curveobject.h"
#include "curvelow.h"

#include "regionobject.h"
#include "_skgtkmodule.h"

#define SKTrafo_TransformXY (sketch_interface->SKTrafo_TransformXY)
#define SKRect_AddXY (sketch_interface->SKRect_AddXY)

#define PREC_BITS 4
#define ROUND(x) (((x) + (1 << (PREC_BITS - 1))) >> PREC_BITS)

#define SMOOTH_EPSILON 8
/* return true if the curve segment given by x[] and y[] is smooth. */
static int
is_smooth(int * x, int * y)
{
    long vx, vy, dx, dy, len = 0, lensqr, dist, par;

    vx = x[3] - x[0]; vy = y[3] - y[0];
    lensqr = vx * vx + vy * vy;

    dx = x[1] - x[0]; dy = y[1] - y[0];
    if (lensqr)
    {
	par = vx * dx + vy * dy;
	if (par < 0 || par > lensqr)
	    return 0;
	len = sqrt(lensqr);
	dist = abs(vx * dy - vy * dx);
	if (dist > len * SMOOTH_EPSILON)
	    return 0;
    }
    else if (dx != 0 || dy != 0)
	return 0;

    dx = x[2] - x[3]; dy = y[2] - y[3];
    if (lensqr)
    {
	par = vx * dx + vy * dy;
	if (par > 0 || par < -lensqr)
	    return 0;
	dist = abs(vx * dy - vy * dx);
	if (dist > len * SMOOTH_EPSILON)
	    return 0;
    }
    else if (dx != 0 || dy != 0)
	return 0;

    return 1;
}

static GdkPoint *
bezier_recurse(GdkPoint * points, int * x, int * y, int depth)
{
    int		u[7], v[7];
    int		tx, ty;

    u[1] = x[0] + x[1];
    v[1] = y[0] + y[1];
    tx = x[1] + x[2];
    ty = y[1] + y[2];
    u[5] = x[2] + x[3];
    v[5] = y[2] + y[3];

    u[2] = u[1] + tx;
    v[2] = v[1] + ty;
    u[4] = u[5] + tx;
    v[4] = v[5] + ty;

    u[3] = (u[2] + u[4] + 4) >> 3;
    v[3] = (v[2] + v[4] + 4) >> 3;

    if (depth > 0)
    {
	u[0] = x[0];		v[0] = y[0];
	u[1] = (u[1] + 1) >> 1;	v[1] = (v[1] + 1) >> 1;
	u[2] = (u[2] + 2) >> 2;	v[2] = (v[2] + 2) >> 2;
	if (!is_smooth(u, v))
	    points = bezier_recurse(points, u, v, depth - 1);
    }

    points->x = ROUND(u[3]);
    points->y = ROUND(v[3]);
    points++;

    if (depth > 0)
    {
	u[4] = (u[4] + 2) >> 2;	v[4] = (v[4] + 2) >> 2;
	u[5] = (u[5] + 1) >> 1;	v[5] = (v[5] + 1) >> 1;
	u[6] = x[3];		v[6] = y[3];
	if (!is_smooth(u + 3, v + 3))
	    points = bezier_recurse(points, u + 3, v + 3, depth - 1);
    }

    return points;
}

int
bezier_fill_points(GdkPoint * start, int * x, int * y)
{
    GdkPoint * points = start;
    int i;

    points->x = x[0];
    points->y = y[0];

    for (i = 0; i < 4; i++)
    {
	x[i] <<= PREC_BITS; y[i] <<= PREC_BITS;
    }

    if (!is_smooth(x, y))
	points = bezier_recurse(points + 1, x, y, BEZIER_DEPTH);
    else
	points += 1;

    points->x = ROUND(x[3]);
    points->y = ROUND(y[3]);

    return points - start + 1;
}




/* Return the maximum number of GdkPoints needed to represent the path.
 */
static int
estimate_number_of_points(SKCurveObject * self)
{
    int i, count = 0;
    CurveSegment * segment;
    
    segment = self->segments;
    for (i = 0; i < self->len; i++, segment++)
    {
	if (segment->type == CurveBezier)
	    /* BEZIER_FILL_LENGTH includes the start and endpoint of a segment.
	       The GdkPoint list contains internal nodes only once. */
	    count += BEZIER_FILL_LENGTH - 1;
	else
	    count += 1;
    }
    return count + 1; /* The start point must be included */
}

/* draw a poly bezier on a GC, applying the transformation trafo to each
 * point. Assume that the gc is set up as needed. This means that we can
 * only fill with the standard X fill capabilities, that is, no gradient
 * patterns, hatching etc.
 *
 * To achieve this, we need to construct a list of GdkPoints that holds
 * the entire curve approximated by line segments. To avoid the overhead
 * of malloc/free we define an array on the stack that is large enough
 * for most objects. Only if the estimated number of points is larger
 * than this array the required amount of memory is malloc'ed. This
 * array is quite large, which might cause problems on some systems.
 *
 * Another, related thing is that the estimated number of points is on
 * average about 40 (forty) times too large (at least for the pictures I
 * tested this with (imported with pstoedit)). Unfortunately, I see no
 * other efficient way to make the estimation more accurate while still
 * giving a correct upper bound for the number of points.
 *
 * As it turns out, on Linux at least malloc seems to be fast enough to
 * always malloc the array without a noticable (but measurable)
 * performance penalty
 */

#define ARRAY_LENGTH (BEZIER_FILL_LENGTH * 30)
/*#define ARRAY_LENGTH 1*/
PyObject *
SKCurve_PyDrawTransformed(PyObject * self, PyObject * args)
{
    PyObject * trafo;
    int length, i, added;
    GdkPoint	* points;
    GdkPoint	point_array[ARRAY_LENGTH];
    CurveSegment * segment;
    SKCoord nx, ny, x1, y1, x2, y2, lastx, lasty;
    int		x[4], y[4];
    PyObject * fill, * line, *rect_or_none;
    PyGObject * gc_object;
    PyGObject * window_object;
    SKRectObject * clip_rect = NULL;
    SKCurveObject * path;
    int optimize_clip = 0;

    if (!PyArg_ParseTuple(args, "O!O!O!O!OOO",
			  &PyGObject_Type, &window_object,
			  &PyGObject_Type, &gc_object,
			  sketch_interface->SKCurveType, &path,
			  sketch_interface->SKTrafoType, &trafo,
			  &line, &fill, &rect_or_none))
	return NULL;

    if (rect_or_none == Py_None)
	clip_rect = NULL;
    else if (rect_or_none->ob_type == sketch_interface->SKRectType)
	clip_rect = (SKRectObject*)rect_or_none;
    else
    {
	PyErr_SetString(PyExc_TypeError, "Rect or None expected");
	return NULL;
    }

    optimize_clip = !PyObject_IsTrue(line);

    length = estimate_number_of_points(path);

    if (length <= 0)
    {
	/* should never happen */
	PyErr_SetString(PyExc_RuntimeError,
			"SKCurve_PyDrawTransformed: estimeted length <= 0");
	return NULL;
    }

    if (length > ARRAY_LENGTH)
    {
	/* allocate the array, if not large enough */
	points = malloc(length * sizeof(GdkPoint));
	if (!points)
	{
	    PyErr_NoMemory();
	    return NULL;
	}
    }
    else
	points = point_array;

    /* the first point */
    segment = path->segments;
    SKTrafo_TransformXY(trafo, segment->x, segment->y, &lastx, &lasty);
    /* round to nearest int. Assumes that window coordinates are positive */
    points[0].x = rint(lastx);
    points[0].y = rint(lasty);
    length = 1;

    /* the rest */
    segment++;
    for (i = 1; i < path->len; i++, segment++)
    {
	int do_bezier = segment->type == CurveBezier;
	if (do_bezier && clip_rect && optimize_clip)
	{
	    /* check, whether part of the segment lies in the clip region */
	    /* XXX: this does not work correctly for very thick lines (lines
	       wider than a few pixels in window coordinates. Therefore we do
	       this only if optimize_clip is true. */
	    SKRectObject r;
	    r.left = r.right = segment[-1].x;
	    r.top = r.bottom = segment[-1].y;
	    SKRect_AddXY(&r, segment->x1, segment->y1);
	    SKRect_AddXY(&r, segment->x2, segment->y2);
	    SKRect_AddXY(&r, segment->x, segment->y);

	    if (r.left > clip_rect->right || r.right < clip_rect->left
		|| r.top < clip_rect->bottom || r.bottom > clip_rect->top)
		do_bezier = 0;
	}

	if (do_bezier)
	{
	    SKTrafo_TransformXY(trafo, segment->x1, segment->y1, &x1, &y1);
	    SKTrafo_TransformXY(trafo, segment->x2, segment->y2, &x2, &y2);
	    SKTrafo_TransformXY(trafo, segment->x, segment->y, &nx, &ny);
	    x[0] = rint(lastx);	y[0] = rint(lasty);
	    x[1] = rint(x1);	y[1] = rint(y1);
	    x[2] = rint(x2);    y[2] = rint(y2);
	    x[3] = rint(nx);    y[3] = rint(ny);
	    added = bezier_fill_points(points + length - 1, x, y);
	    length += added - 1;
	}
	else
	{
	    SKTrafo_TransformXY(trafo, segment->x, segment->y, &nx, &ny);
	    points[length].x = rint(nx);
	    points[length].y = rint(ny);
	    if (i >= path->len - 1
		|| abs(points[length].x - points[length - 1].x)
		|| abs(points[length].y - points[length - 1].y))
		length++;
	}

	lastx = nx;
	lasty = ny;
    }

    if (length > 1)
    {
	if (path->closed && PyObject_IsTrue(fill))
	{
	    gdk_draw_polygon(GDK_DRAWABLE(window_object->obj),
			     GDK_GC(gc_object->obj), 1, points, length);
	}
	if (PyObject_IsTrue(line))
	{
	    gdk_draw_lines(GDK_DRAWABLE(window_object->obj),
			   GDK_GC(gc_object->obj), points, length);
	}
    }
#if 0
    else
    {
	fprintf(stderr, "DrawTransformed: length = 1\n");
    }
#endif
    
    if (points != point_array)
    {
	free(points);
    }
    Py_INCREF(Py_None);
    return Py_None;
}



static int
curve_add_transformed_points(SKCurveObject * self, GdkPoint * points,
			     PyObject * trafo, SKRectObject * clip_rect,
			     int optimize_clip)
{
    int length, i, added;
    CurveSegment *segment;
    SKCoord nx, ny, x1, y1, x2, y2, lastx, lasty;
    int x[4], y[4];

    /* the first point */
    segment = self->segments;
    SKTrafo_TransformXY(trafo, segment->x, segment->y, &lastx, &lasty);
    /* round to nearest int. Assumes that window coordinates are positive */
    points[0].x = rint(lastx);
    points[0].y = rint(lasty);
    length = 1;

    /* the rest */
    segment++;
    for (i = 1; i < self->len; i++, segment++)
    {
	int do_bezier = segment->type == CurveBezier;
	if (do_bezier && clip_rect && optimize_clip)
	{
	    /* check, whether part of the segment lies in the clip region */
	    /* XXX: this does not work correctly for very thick lines (lines
	       wider than a few pixels in window coordinates */
	    SKRectObject r;
	    r.left = r.right = segment[-1].x;
	    r.top = r.bottom = segment[-1].y;
	    SKRect_AddXY(&r, segment->x1, segment->y1);
	    SKRect_AddXY(&r, segment->x2, segment->y2);
	    SKRect_AddXY(&r, segment->x, segment->y);

	    if (r.left > clip_rect->right || r.right < clip_rect->left
		|| r.top < clip_rect->bottom || r.bottom > clip_rect->top)
		do_bezier = 0;
	}

	if (do_bezier)
	{
	    SKTrafo_TransformXY(trafo, segment->x1, segment->y1, &x1, &y1);
	    SKTrafo_TransformXY(trafo, segment->x2, segment->y2, &x2, &y2);
	    SKTrafo_TransformXY(trafo, segment->x, segment->y, &nx, &ny);
	    x[0] = rint(lastx);	y[0] = rint(lasty);
	    x[1] = rint(x1);	y[1] = rint(y1);
	    x[2] = rint(x2);	y[2] = rint(y2);
	    x[3] = rint(nx);	y[3] = rint(ny);
	    added = bezier_fill_points(points + length - 1, x, y);
	    length += added - 1;
	}
	else
	{
	    SKTrafo_TransformXY(trafo, segment->x, segment->y, &nx, &ny);
	    points[length].x = rint(nx);
	    points[length].y = rint(ny);
	    if (i >= self->len - 1
		|| abs(points[length].x - points[length - 1].x)
		|| abs(points[length].y - points[length - 1].y))
		length++;
	}

	lastx = nx;
	lasty = ny;
    }

    return length;
}

static int
find_arrow_point_start(GdkPoint * points, int num_points, double length)
{
    int pos = 1, result;
    double sum = 0, dist = 0.0;

    if (length <= 0.0)
	return 0;

    while (pos < num_points && sum < length)
    {
	dist = hypot(points[pos].x - points[pos - 1].x,
		     points[pos].y - points[pos - 1].y);
	sum += dist;
	pos += 1;
    }

    if (sum > length)
    {
	double factor = (sum - length) / dist;
	double x, y;
	pos -= 2;
	x = factor * points[pos].x + (1 - factor) * points[pos + 1].x;
	y = factor * points[pos].y + (1 - factor) * points[pos + 1].y;
	points[pos].x = rint(x);
	points[pos].y = rint(y);
	result = pos;
    }
    else
    {
	result = pos - 1;
    }

    return result;
}


static int
find_arrow_point_end(GdkPoint * points, int num_points, double length)
{
    int pos, result;
    double sum = 0, dist = 0.0;

    if (length <= 0.0)
	return 0;

    pos = num_points - 1;
    while (pos >= 0 && sum < length)
    {
	pos -= 1;
	dist = hypot(points[pos].x - points[pos + 1].x,
		     points[pos].y - points[pos + 1].y);
	sum += dist;
    }

    if (sum > length)
    {
	double factor = (sum - length) / dist;
	double x, y;
	x = factor * points[pos + 1].x + (1 - factor) * points[pos].x;
	y = factor * points[pos + 1].y + (1 - factor) * points[pos].y;
	points[pos + 1].x = rint(x);
	points[pos + 1].y = rint(y);
	result = num_points - pos - 2;
    }
    else
    {
	result = num_points - pos - 1;
    }

    return result;
}


/*
 * Draw a multipath bezier on a gc.
 *
 * The multipath bezier is represented by a tuple of SKCurveObject objects.
 * The callback functions fill_func and line_func allow arbitrary fill
 * patterns.
 *
 */

PyObject *
SKCurve_PyDrawMultipath(PyObject* self, PyObject * args)
{
    PyObject * trafo;
    int length, i, added, filled;
    GdkPoint * points = NULL;
    int *start_idx = NULL, *lengths = NULL;
    PyObject *fill_func, *line_func, *push_clip, *pop_clip, *set_clip;
    PyObject *rect_or_none, *paths;
    SKCurveObject *path;
    PyGObject * gc_object;
    PyGObject * window_object;
    SKRectObject * clip_rect = NULL;
    GdkPoint start;
    SKRegionObject * oregion = NULL;
    int is_proc_fill = 0, do_clip = 0;
    double arrow_retract1 = 0.0, arrow_retract2 = 0.0;

    if (!PyArg_ParseTuple(args, "O!O!O!OOOOOOO!O!ii|dd",
			  &PyGObject_Type, &window_object,
			  &PyGObject_Type, &gc_object,
			  sketch_interface->SKTrafoType, &trafo,
			  &line_func, &fill_func, &push_clip, &pop_clip,
			  &set_clip, &rect_or_none,
			  &PyTuple_Type, &paths,
			  &SKRegionType, &oregion,
			  &is_proc_fill, &do_clip,
			  &arrow_retract1, &arrow_retract2))
	return NULL;
    /* XXX  */

    if (rect_or_none == Py_None)
	clip_rect = NULL;
    else if (rect_or_none->ob_type == sketch_interface->SKRectType)
	clip_rect = (SKRectObject*)rect_or_none;
    else
    {
	PyErr_SetString(PyExc_TypeError,
			"8th parameter must None or an SKRectObject");
	return NULL;
    }

    if (!PyObject_IsTrue((PyObject*)oregion))
	oregion = NULL;

    filled = PyObject_IsTrue(fill_func) || do_clip;

    length = 0;
    for (i = 0; i < PyTuple_Size(paths); i++)
    {
	path = (SKCurveObject*)PyTuple_GetItem(paths, i);
	if (path->ob_type != sketch_interface->SKCurveType)
	{
	    PyErr_SetString(PyExc_TypeError,
			    "paths must be a tuple of bezier path objects");
	    return NULL;
	}
	length += estimate_number_of_points(path);
    }
    /* additional points to close the paths: */
    if (filled)
	length += 2 * PyTuple_Size(paths);

    if (length <= 0)
    {
	/* can theoretically happen if there is only one path with no segments.
	 * Such paths should have been automatically removed from the drawing.
	 */
	Py_INCREF(Py_None);
	return Py_None;
    }

    points = malloc(length * sizeof(GdkPoint));
    start_idx = malloc(PyTuple_Size(paths) * sizeof(int));
    lengths = malloc(PyTuple_Size(paths) * sizeof(int));
    if (!points || !start_idx || !lengths)
    {
	PyErr_NoMemory();
	goto fail;
    }

    length = 0;
    for (i = 0; i < PyTuple_Size(paths); i++)
    {
	start_idx[i] = length;
	path = (SKCurveObject*)PyTuple_GetItem(paths, i);
	added = curve_add_transformed_points(path, points + length, trafo,
					     clip_rect,
					     !PyObject_IsTrue(line_func));
	if (!added)
	    goto fail;
	lengths[i] = added;

	if (filled)
	{
	    if (!path->closed)
	    {
		points[length + added] = points[length];
		added++;
	    }
	    if (i == 0)
	    {
		start = points[0];
	    }
	    else
	    {
		points[length + added] = start;
		added++;
	    }
	}
	length += added;
    }

    if (length > 1)
    {
	if (filled)
	{
	    GdkRegion * region  = gdk_region_polygon(points, length,
						     GDK_EVEN_ODD_RULE);
	    SKRegion_UnionWithRegion(oregion, region);
	    gdk_region_destroy(region);
	    
	    if (is_proc_fill)
	    {
		PyObject * result;

		if (!do_clip)
		{
		    result = PyObject_CallObject(push_clip, NULL);
		    if (!result)
			goto fail;
		    Py_DECREF(result);
		}
		result = PyObject_CallFunction(set_clip, "(O)", oregion);
		if (!result)
		    goto fail;
		Py_DECREF(result);
		result = PyObject_CallObject(fill_func, NULL);
		if (!result)
		    goto fail;
		Py_DECREF(result);
		if (!do_clip)
		{
		    result = PyObject_CallObject(pop_clip, NULL);
		    if (!result)
			goto fail;
		    Py_DECREF(result);
		}
	    }
	    else /* !is_proc_fill */
	    {
		if (PyObject_IsTrue(fill_func))
		{
		    PyObject * result = PyObject_CallObject(fill_func, NULL);
		    if (!result)
			goto fail;
		    Py_DECREF(result);
		    gdk_draw_polygon(GDK_DRAWABLE(window_object->obj),
				     GDK_GC(gc_object->obj),
				     1, points, length);
		}

		if (do_clip)
		{
		    PyObject * result = PyObject_CallFunction(set_clip, "(O)",
							      oregion);
		    if (!result)
			goto fail;
		    Py_DECREF(result);
		}
	    }
	} /* if (filled) */

	if (PyObject_IsTrue(line_func))
	{
	    PyObject * result = PyObject_CallObject(line_func, NULL);
	    if (!result)
		goto fail;
	    Py_DECREF(result);
	    for (i = 0; i < PyTuple_Size(paths); i++)
	    {
		GdkPoint * pts = points + start_idx[i];
		int len = lengths[i];
		int retract;
		
		if (arrow_retract1)
		{
		    retract = find_arrow_point_start(pts, len, arrow_retract1);
		    pts += retract;
		    len -= retract;
		}
		if (arrow_retract1)
		{
		    retract = find_arrow_point_end(pts, len, arrow_retract2);
		    len -= retract;
		}
		if (len)
		    gdk_draw_lines(GDK_DRAWABLE(window_object->obj),
				   GDK_GC(gc_object->obj), pts, len);
	    }
	}
    }

    free(points);
    free(lengths);
    free(start_idx);

    Py_INCREF(Py_None);
    return Py_None;

fail:
    free(points);
    free(lengths);
    free(start_idx);
    return NULL;

}

/*
 *
 */

PyObject *
SKCurve_PyMultipathRegion(PyObject* self, PyObject * args)
{
    PyObject * trafo;
    int length, i, added;
    GdkPoint	* points = NULL;
    PyObject	*rect_or_none, *paths;
    SKCurveObject *path;
    SKRectObject * clip_rect = NULL;
    SKRegionObject * oregion = NULL;
    GdkPoint start;

    if (!PyArg_ParseTuple(args, "O!O!OO!",
			  sketch_interface->SKTrafoType, &trafo,
			  &PyTuple_Type, &paths, &rect_or_none,
			  &SKRegionType, &oregion))
	return NULL;

    if (rect_or_none == Py_None)
	clip_rect = NULL;
    else if (rect_or_none->ob_type == sketch_interface->SKRectType)
	clip_rect = (SKRectObject*)rect_or_none;
    else
    {
	PyErr_SetString(PyExc_TypeError,
			"3rd parameter must None or an SKRectObject");
	return NULL;
    }

    length = 0;
    for (i = 0; i < PyTuple_Size(paths); i++)
    {
	path = (SKCurveObject*)PyTuple_GetItem(paths, i);
	if (path->ob_type != sketch_interface->SKCurveType)
	{
	    PyErr_SetString(PyExc_TypeError,
			    "paths must be a tuple of bezier path objects");
	    return NULL;
	}
	length += estimate_number_of_points(path);
    }
    /* additional points needed to close the paths: */
    length += 2 * PyTuple_Size(paths);

#if 0
    if (length <= 0)
    {
	/* can theoretically happen if there is only one path with no segments.
	 * Such paths should have been automatically removed from the drawing.
	 */
	Py_INCREF(Py_None);
	return Py_None;
    }
#endif

    points = malloc(length * sizeof(GdkPoint));
    if (!points)
    {
	PyErr_NoMemory();
	goto fail;
    }

    length = 0;
    for (i = 0; i < PyTuple_Size(paths); i++)
    {
	path = (SKCurveObject*)PyTuple_GetItem(paths, i);
	added = curve_add_transformed_points(path, points + length, trafo,
					    clip_rect, 1);
	if (!added)
	    goto fail;

	if (!path->closed)
	{
	    points[length + added] = points[length];
	    added++;
	}
	if (i == 0)
	{
	    start = points[0];
	}
	else
	{
	    points[length + added] = start;
	    added++;
	}
	length += added;
    }

    if (length > 1)
    {
	GdkRegion * region = gdk_region_polygon(points, length,
						GDK_EVEN_ODD_RULE);
	SKRegion_UnionWithRegion(oregion, region);
	gdk_region_destroy(region);
    }
    
    free(points);

    Py_INCREF(Py_None);
    return Py_None;

fail:
    free(points);
    return NULL;

}



PyObject *
SKCurve_PyDrawBezierSegment(PyObject * self, PyObject * arg)
{
    PyGObject *gc_object;
    PyGObject * window_object;
    int		x[4],
		y[4];
    GdkPoint	points[BEZIER_FILL_LENGTH];
    int		count;
    
    if (!PyArg_ParseTuple(arg, "O!O!iiiiiiii",
			  &PyGObject_Type, &window_object,
			  &PyGObject_Type, &gc_object,
			  &x[0], &y[0], &x[1], &y[1],
			  &x[2], &y[2], &x[3], &y[3]))
	return NULL;

    count = bezier_fill_points(points, x, y);
    gdk_draw_lines(GDK_DRAWABLE(window_object->obj), GDK_GC(gc_object->obj),
		   points, count);

    Py_INCREF(Py_None);
    return Py_None;
}

