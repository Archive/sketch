/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999, 2000, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>

#include "Python.h"
#include "modsupport.h"
#include "structmember.h"

#include <gdk/gdk.h>

#include "imageobject.h"
#include "_skgtkmodule.h"

/*
 *struct _GdkImage
 *{
 * GdkImageType	type;
 * GdkVisual    *visual;	    / * visual used to create the image * /
 * GdkByteOrder	byte_order;
 * guint16	width;
 * guint16	height;
 * guint16	depth;
 * guint16	bpp;	    / * bytes per pixel * /
 * guint16	bpl;	    / * bytes per line * /
 * gpointer	mem;
 *};
*/

#define OFF(x) offsetof(GdkImage, x)
static struct memberlist image_memberlist[] = {
    {"type",		T_INT,		OFF(type),		RO},
    {"byte_order",	T_INT,		OFF(byte_order),	RO},
    {"width",		T_USHORT,	OFF(width),		RO},
    {"height",		T_USHORT,	OFF(height),		RO},
    {"depth",		T_USHORT,	OFF(depth),		RO},
    {"bits_per_pixel",	T_USHORT,	OFF(bpp),		RO},
    {"bytes_per_line",	T_USHORT,	OFF(bpl),		RO},
    {NULL} 
};


static PyObject *
image_PutPixel(SKImageObject * self, PyObject * args)
{
    gint x, y;
    guint32 value;

    if (!PyArg_ParseTuple(args, "iil", &x, &y, &value))
	return NULL;

    gdk_image_put_pixel(self->image, x, y, value);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
image_GetPixel(SKImageObject * self, PyObject * args)
{
    gint x, y;

    if (!PyArg_ParseTuple(args, "ii", &x, &y))
	return NULL;

    return PyInt_FromLong(gdk_image_get_pixel(self->image, x, y));
}

static PyObject *
image_dump_data(SKImageObject * self, PyObject * args)
{
    char * filename;
    FILE * file;

    if (!PyArg_ParseTuple(args, "s", &filename))
	return NULL;

    file = fopen(filename, "w");
    if (!file)
    {
	PyErr_SetString(PyExc_IOError, "cannot open file");
	return NULL;
    }

    fwrite(self->image->mem,
	   self->image->bpl, self->image->height, file);
    fclose(file);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef image_methods[] = {
    {"GetPixel",	(PyCFunction)image_GetPixel,		METH_VARARGS},
    {"PutPixel",	(PyCFunction)image_PutPixel,		METH_VARARGS},
    {"dump_data",	(PyCFunction)image_dump_data,		METH_VARARGS},
    {NULL, NULL}, 
};

static PyObject *
image_getattr(SKImageObject *self, char *name)
{
    PyObject * result;
    
    result = PyMember_Get((char *)(self->image), image_memberlist, name);
    if (result != NULL)
	return result;
    /* PyMember_Get failed. reset exceptions */
    PyErr_Clear(); 

    return Py_FindMethod(image_methods, (PyObject *) self, name);
}

static void
image_dealloc(SKImageObject *self)
{
    gdk_image_destroy(self->image);
    
    PyMem_DEL(self);
}

PyTypeObject SKImageType = {
	PyObject_HEAD_INIT(NULL)
	0,			/*ob_size*/
	"SKImage",		/*tp_name*/
	sizeof(SKImageObject),	/*tp_size*/
	0,			/*tp_itemsize*/
	(destructor)image_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)image_getattr, /*tp_getattr*/
	0,			/*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
};

GdkImage *
SKImage_AsImage(PyObject *self)
{
    if (self->ob_type != &SKImageType) {
	PyErr_BadArgument();
	return NULL;
    }
    return ((SKImageObject *) self)->image;
}

PyObject *
SKImage_FromImage(GdkImage *image)
{
	SKImageObject *self = PyObject_NEW(SKImageObject, &SKImageType);

	if (self == NULL)
		return NULL;
	self->image = image;
	return (PyObject *) self;
}

PyObject *
SKImage_PyNew(PyObject * self, PyObject * args)
{
    PyGObject * window;
    int width, height;
    int type;
    GdkImage * image;
    PyObject * image_object;

    if (!PyArg_ParseTuple(args, "O!iii", &PyGObject_Type, &window,
			  &type, &width, &height))
	return NULL;

    image = gdk_image_new(type,
			  gdk_window_get_visual(GDK_WINDOW(window->obj)),
			  width, height);
    image_object = SKImage_FromImage(image);
    if (!image_object)
    {
	gdk_image_destroy(image);
    }
    return image_object;
}

