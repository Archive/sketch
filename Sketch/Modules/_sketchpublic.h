/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1998, 1999 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _SKETCHPUBLIC_H
#define _SKETCHPUBLIC_H

#include "skpoint.h"
#include "skrect.h"

typedef struct {
    PyTypeObject * SKPointType;
    PyTypeObject * SKTrafoType;
    PyTypeObject * SKRectType;
    PyTypeObject * SKColorType;
    PyTypeObject * SKCurveType;
    void (*SKTrafo_TransformXY)(PyObject * trafo, double x, double y,
				SKCoord * out_x, SKCoord * out_y);
    int (*SKRect_AddXY)(SKRectObject * self, double x, double y);
    int (*skpoint_extract_xy)(PyObject * sequence, double * x, double * y);
} Sketch_Interface;


#endif /* _SKGTKPUBLIC_H */
