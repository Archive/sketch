/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1998, 1999, 2002, 2003, 2004 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Determine intersection of clip masks for the gtk renderer
 *
 * In the gtk renderer, clipmasks can be of three types, None, i.e. no
 * clipmask, a region object or a pixmap of depth 1.  The main function
 * in this module is SKClipMask_Intersect which returns a new reference
 * to a pthon object representing the intersection of two clip masks
 * given as python objects.  There are essentiall 4 cases of input type
 * combinations to consider, keeping in mind that the operation is
 * commutative:
 *
 *    None, (None, region or bitmap)
 *
 *         Return the right-hand argument with increased reference
 *         count.
 *
 *    region, region
 *
 *	   Return the intersection as a new region object.
 *
 *    region, bitmap
 *
 *	   Return the intersection as a new bitmap.
 *
 *    bitmap, bitmap
 *
 *	   Return the intersection as a new bitmap.
 *
 * In all cases, no arguments are modified in place.  This is important
 * because the regions in particular are often maintained by the windows
 * and passed to the renderers to indicate which region to draw.  The
 * renders must not modify the region in place since they don't own it.
 */

#include <gdk/gdk.h>
#include <Python.h>
#include "regionobject.h"
#include "_skgtkmodule.h"
#include "clipmask.h"

static PyObject *
mask_intersect_regions(GdkRegion * region1, GdkRegion * region2)
{
    GdkRegion * result = gdk_region_copy(region1);
    gdk_region_intersect(result, region2);
    return SKRegion_FromRegion(result);
}

static PyObject *
mask_intersect_region_with_bitmap(GdkRegion * region, GdkWindow *bitmap)
{
    GdkGCValues values;
    GdkGC * gc;
    GdkPixmap * result;
    GdkColor color;
    gint x, y;
    guint width, height, depth;
    
    gdk_window_get_geometry(bitmap, &x, &y, &width, &height, &depth);

    if (depth != 1)
    {
	PyErr_SetString(PyExc_TypeError, "pixmap must have depth 1");
	return NULL;
    }
    
    result = gdk_pixmap_new(bitmap, width, height, 1);

    color.red = color.green = color.blue = 0;
    color.pixel = 0;
    values.foreground = color;
    values.background = color;
    gc = gdk_gc_new_with_values(bitmap, &values,
				GDK_GC_FOREGROUND | GDK_GC_BACKGROUND);
    gdk_draw_rectangle(result, gc, 1, 0, 0, width, height);
    color.pixel = 1;
    gdk_gc_set_foreground(gc, &color);
    gdk_gc_set_clip_region(gc, region);
    gdk_window_copy_area(result, gc, 0, 0, bitmap, 0, 0, width, height);
	
    gdk_gc_destroy(gc);

    return pygobject_new(G_OBJECT(result));
}


static PyObject *
mask_intersect_bitmaps(GdkPixmap * bitmap1, GdkPixmap * bitmap2)
{
    GdkGCValues values;
    GdkGC * gc;
    GdkPixmap * result;
    GdkColor color;
    gint x, y;
    guint width, height, width2, height2, depth;
    
    gdk_window_get_geometry(bitmap1, &x, &y, &width, &height, &depth);
    if (depth != 1)
    {
	PyErr_SetString(PyExc_TypeError, "pixmap must have depth 1");
	return NULL;
    }

    gdk_window_get_geometry(bitmap2, &x, &y, &width2, &height2, &depth);
    if (depth != 1)
    {
	PyErr_SetString(PyExc_TypeError, "pixmap must have depth 1");
	return NULL;
    }
    
    if (width != width2 || height != height2)
    {
	PyErr_SetString(PyExc_ValueError, "bitmaps must have the same size");
	return NULL;
    }
    
    
    result = gdk_pixmap_new(bitmap1, width, height, 1);

    color.red = color.green = color.blue = 0;
    color.pixel = 1;
    values.foreground = color;
    color.pixel = 0;
    values.background = color;
    gc = gdk_gc_new_with_values(bitmap1, &values,
				GDK_GC_FOREGROUND | GDK_GC_BACKGROUND);
    gdk_window_copy_area(result, gc, 0, 0, bitmap1, 0, 0, width, height);
    gdk_gc_set_function(gc, GDK_AND);
    gdk_window_copy_area(result, gc, 0, 0, bitmap1, 0, 0, width, height);
	
    gdk_gc_destroy(gc);

    return pygobject_new(G_OBJECT(result));
}

static PyObject *
SKClipMask_Intersect(PyObject * mask1, PyObject * mask2)
{
    if (mask1 == Py_None)
    {
	Py_INCREF(mask2);
	return mask2;
    }
    if (mask2 == Py_None)
    {
	Py_INCREF(mask1);
	return mask1;
    }

    if (SKRegion_Check(mask1))
    {
	if (SKRegion_Check(mask2))
	{
	    return mask_intersect_regions(SKRegion_AsRegion(mask1),
					  SKRegion_AsRegion(mask2));
	}
	else if (pygobject_check(mask2, &PyGObject_Type))
	{
	    PyGObject * gmask = (PyGObject*)mask2;
	    if (GDK_IS_PIXMAP(gmask->obj))
	     return mask_intersect_region_with_bitmap(SKRegion_AsRegion(mask1),
						      GDK_PIXMAP(gmask->obj));
	}
    }
    else if (pygobject_check(mask1, &PyGObject_Type))
    {
	PyGObject * gmask1 = (PyGObject*)mask1;
	
	if (SKRegion_Check(mask2))
	    return mask_intersect_region_with_bitmap(SKRegion_AsRegion(mask2),
						     GDK_PIXMAP(gmask1->obj));
	else if (pygobject_check(mask2, &PyGObject_Type))
	{
	    PyGObject * gmask2 = (PyGObject*)mask2;
	    return mask_intersect_bitmaps(GDK_PIXMAP(gmask1->obj),
					  GDK_PIXMAP(gmask2->obj));
	}
    }

    PyErr_SetString(PyExc_TypeError,
		    "arguments must be regions and/or bitmaps");
    return NULL;
}


PyObject *
SKClipMask_PyIntersectMasks(PyObject * self, PyObject * args)
{
    PyObject * object1, *object2;

    if (!PyArg_ParseTuple(args, "OO", &object1, &object2))
	return NULL;

    return SKClipMask_Intersect(object1, object2);
}
