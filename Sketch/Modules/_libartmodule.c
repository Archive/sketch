/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1999, 2000, 2001, 2002, 2003, 2004, 2005 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA
 */

#include <math.h>
#include <string.h>
#include <Python.h>
#include <structmember.h>

#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_bpath.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_vpath_bpath.h>
#include <libart_lgpl/art_vpath_dash.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>
#include <libart_lgpl/art_svp_render_aa.h>
#include <libart_lgpl/art_rgb_svp.h>
#include <libart_lgpl/art_gray_svp.h>
#include <libart_lgpl/art_svp_wind.h>
#include <libart_lgpl/art_svp_ops.h>
#include <libart_lgpl/art_pixbuf.h>
#include <libart_lgpl/art_rgb_svp.h>
#include <libart_lgpl/art_rect.h>
#include <libart_lgpl/art_rect_svp.h>
#include <libart_lgpl/art_svp_intersect.h>

/* PIL headers and defs */
#include <Imaging.h>

typedef struct {
    PyObject_HEAD
    Imaging image;
} ImagingObject;


/* Sketch specific headers */
#include "_sketchpublic.h"
#include "curveobject.h"

/* sklibart headers */
/*#include "pilrender.h"*/
#include "skrender.h"

#include "_libartmodule.h"


/*
 *
 */

Sketch_Interface * sketch_interface;


/*
 *  SVP object
 */


static void print_svp(ArtSVP * vp);


static PyObject *
SKSVP_FromArtSVP(ArtSVP * svp)
{
    SKSVPObject * self;

    self = PyObject_NEW(SKSVPObject, &SKSVPType);
    if (self == NULL)
	return NULL;

    self->svp = svp;

    return (PyObject*)self;
}

static void
SKSVP_dealloc(SKSVPObject * self)
{
    art_svp_free(self->svp);
    PyMem_DEL(self);
}

static PyObject *
SKSVP_repr(SKSVPObject * self)
{
    char buf[100];
    sprintf(buf, "<SKSVPObject at %d>", (int)self);
    return PyString_FromString(buf);
}

static PyObject *
SKSVP_bounding_rect(SKSVPObject * self, PyObject * args)
{
    ArtDRect drect;
    ArtIRect irect;

    art_drect_svp(&drect, self->svp);
    art_drect_to_irect(&irect, &drect);

    return Py_BuildValue("iiii", irect.x0, irect.y0, irect.x1, irect.y1);
}

static struct PyMethodDef sksvp_methods[] = {
    {"bounding_rect",	(PyCFunction)SKSVP_bounding_rect,	METH_VARARGS},
    {NULL,	NULL}
};

static PyObject *
SKSVP_getattr(PyObject * self, char * name)
{
    return Py_FindMethod(sksvp_methods, self, name);
}


PyTypeObject SKSVPType = {
	PyObject_HEAD_INIT(NULL)
	0,
	"sksvp",
	sizeof(SKSVPObject),
	0,
	(destructor)SKSVP_dealloc,	/*tp_dealloc*/
	(printfunc)NULL,		/*tp_print*/
	SKSVP_getattr,		/*tp_getattr*/
	0,				/*tp_setattr*/
	(cmpfunc)NULL,	/*tp_compare*/
	(reprfunc)SKSVP_repr,		/*tp_repr*/
	0,				/*tp_as_number*/
	0,				/*tp_as_sequence*/
	0,				/*tp_as_mapping*/
	0,				/*tp_hash*/
	(ternaryfunc)NULL,	/* tp_call */
};


/*
 * ArtPixBuf object
 */


static PyObject *
SKArtPixBuf_FromArtPixBuf(ArtPixBuf * artpixbuf)
{
    SKArtPixBufObject * self;

    self = PyObject_NEW(SKArtPixBufObject, &SKArtPixBufType);
    if (self == NULL)
	return NULL;

    self->artpixbuf = artpixbuf;

    self->skpixbuf.width = artpixbuf->width;
    self->skpixbuf.height = artpixbuf->height;
    self->skpixbuf.channels = artpixbuf->n_channels;
    self->skpixbuf.data32 = malloc(artpixbuf->height * sizeof(art_u32*));
    self->skpixbuf.has_alpha = artpixbuf->has_alpha;

    if (!self->skpixbuf.data32)
    {
	art_pixbuf_free(self->artpixbuf);
	PyMem_DEL(self);
	self = NULL;
    }
    else
    {
	int y, rowstride = artpixbuf->rowstride;
	art_u8 * pixels = artpixbuf->pixels;
	
	for (y = 0; y < self->artpixbuf->height; y++)
	{
	    self->skpixbuf.data32[y] = (art_u32*)(pixels + y * rowstride);
	}
    }

    return (PyObject*)self;
}

static void
SKArtPixBuf_dealloc(SKArtPixBufObject * self)
{
    art_pixbuf_free(self->artpixbuf);
    PyMem_DEL(self);
}

static PyObject *
SKArtPixBuf_repr(SKArtPixBufObject * self)
{
    char buf[100];
    sprintf(buf, "<SKArtPixBufObject at %d>", (int)self);
    return PyString_FromString(buf);
}

static struct PyMethodDef skartpixbuf_methods[] = {
/*    {"bounding_rect",	(PyCFunction)SKSVP_bounding_rect, METH_VARARGS},*/
    {NULL,	NULL}
};

static PyObject *
SKArtPixBuf_getattr(PyObject * self, char * name)
{
    return Py_FindMethod(skartpixbuf_methods, self, name);
}

PyTypeObject SKArtPixBufType = {
	PyObject_HEAD_INIT(NULL)
	0,
	"skartpixbuf",
	sizeof(SKArtPixBufObject),
	0,
	(destructor)SKArtPixBuf_dealloc,	/*tp_dealloc*/
	(printfunc)NULL,		/*tp_print*/
	SKArtPixBuf_getattr,		/*tp_getattr*/
	0,				/*tp_setattr*/
	(cmpfunc)NULL,	/*tp_compare*/
	(reprfunc)SKArtPixBuf_repr,		/*tp_repr*/
	0,				/*tp_as_number*/
	0,				/*tp_as_sequence*/
	0,				/*tp_as_mapping*/
	0,				/*tp_hash*/
	(ternaryfunc)NULL,	/* tp_call */
};


/*
 * Convert a python object into a SKPixbuf*
 *
 * The python object may be either a SKArtPixBufObject or a
 * ImagingObject. In the first case, simply return the pointer to the
 * object's skpixbuf, in the second case fill *pixbuf with the
 * appropriate values and return pixbuf.
 */
static SKPixbuf * 
convert_image_or_pixbuf(PyObject * image_or_pixbuf, SKPixbuf * pixbuf)
{
    SKPixbuf * pdestbuf;
    
    if (SKArtPixBuf_Check(image_or_pixbuf))
    {
	pdestbuf = &(((SKArtPixBufObject*)image_or_pixbuf)->skpixbuf);
    }
    else
    {
	ImagingObject * image = (ImagingObject*)image_or_pixbuf;
	pixbuf->width = image->image->xsize;
	pixbuf->height = image->image->ysize;
	pixbuf->data32 = (art_u32**)(image->image->image32);
	pixbuf->has_alpha = strcmp(image->image->mode, "RGBA") == 0;
	pdestbuf = pixbuf;
    }
    return pdestbuf;
}


/*
 *   Module functions
 */


/* Apply the affine transformation in self to the point (x, y) writing
 * the result into *out_x and *out_y
 */
static void
transformxy(SKTrafoObject * self, double x, double y,
	    double * out_x, double * out_y)
{
    *out_x = self->m11 * x + self->m12 * y + self->v1;
    *out_y = self->m21 * x + self->m22 * y + self->v2;
}


/* Return an ArtVPath created from Sketch curve objects.
 *
 * The ArtVPath is created from an intermediary ArtBpath which is a
 * transformed copy of the array of SKCurveObject pointers, curves, with
 * ncurves items. The affine transformation is given by trafo. If closed
 * is true, the curves will be closed. If perturb is true, call
 * art_vpath_perturb on the VPath.
 */
static ArtVpath *
vpath_from_curves(SKCurveObject ** curves, int ncurves,
		  SKTrafoObject * trafo, int closed, int perturb)
{
    ArtBpath * bpath;
    ArtVpath * vpath, *tmp;
    CurveSegment * segments;
    int i, idx, k, sum = 0;

    for (k = 0; k < ncurves; k++)
    {
	sum += curves[k]->len;
    }

    /* +ncurves to make room for closing segments */
    bpath = art_new(ArtBpath, sum + ncurves + 1);

    idx = 0;
    for (k = 0; k < ncurves; k++)
    {
	segments = curves[k]->segments;
	for(i = 0; i < curves[k]->len; i++, idx++)
	{
	    if (segments[i].type == CurveBezier)
	    {
		bpath[idx].code = ART_CURVETO;
		transformxy(trafo, segments[i].x1, segments[i].y1,
			    &(bpath[idx].x1), &(bpath[idx].y1));
		transformxy(trafo, segments[i].x2, segments[i].y2,
			    &(bpath[idx].x2), &(bpath[idx].y2));
		transformxy(trafo, segments[i].x, segments[i].y,
			    &(bpath[idx].x3), &(bpath[idx].y3));
	    }
	    else
	    {
		int code = ART_LINETO;
		if (i == 0)
		{
		    if (closed || curves[k]->closed)
			code = ART_MOVETO;
		    else
			code = ART_MOVETO_OPEN;
		}
		bpath[idx].code = code;
		transformxy(trafo, segments[i].x, segments[i].y,
			    &(bpath[idx].x3), &(bpath[idx].y3));
	    }
	}
	if (closed && i > 0)
	{
	    bpath[idx].code = ART_LINETO;
	    bpath[idx].x3 = bpath[idx - curves[k]->len].x3;
	    bpath[idx].y3 = bpath[idx - curves[k]->len].y3;
	    idx++;
	}
    }
    bpath[idx].code = ART_END;
    bpath[idx].x3 = 0;
    bpath[idx].y3 = 0;

    vpath = art_bez_path_to_vec(bpath, 0.5);
    art_free(bpath);

    if (perturb)
    {
	tmp = art_vpath_perturb(vpath);
	art_free(vpath);
	vpath = tmp;
    }
	
    return vpath;
}

/* Return an SVP object for the interior of a PolyBezier object.
 */
static PyObject *
SKSVP_FromSKCurve(PyObject * self, PyObject * args)
{
    PyObject * ocurves;
    SKCurveObject ** curves;
    SKTrafoObject * trafo;
    int ncurves = 0;
    int wind = -1;
    int perturb = 1;
    
    ArtVpath * vpath;
    ArtSVP * svp;
    ArtSVP * tmp;
    ArtSvpWriter *swr;

    if (!PyArg_ParseTuple(args, "OO!|ii", &ocurves,
			  sketch_interface->SKTrafoType, &trafo,
			  &wind, &perturb))
	return NULL;

    if (ocurves->ob_type == sketch_interface->SKCurveType)
    {
	curves = (SKCurveObject**)&ocurves;
	ncurves = 1;
    }
    else if (PyTuple_Check(ocurves))
    {
	curves = (SKCurveObject**)(&PyTuple_GET_ITEM(ocurves, 0));
	ncurves = PyTuple_Size(ocurves);
    }
    else
    {
	PyErr_SetString(PyExc_TypeError, "argument must be a single "
			"curve object or a tuple of curves");
	return NULL;
    }

    vpath = vpath_from_curves(curves, ncurves, trafo, 1, perturb);
    if (!vpath)
	return NULL;

    svp = art_svp_from_vpath(vpath);
    if (wind > 0)
    {
	swr = art_svp_writer_rewind_new(wind);
	art_svp_intersector(svp, swr);
	tmp = art_svp_writer_rewind_reap(swr);  /* this also frees swr */

	art_svp_free(svp);
	svp = tmp;
    }

    art_free(vpath);
    if (!svp)
	return PyErr_NoMemory();

    return SKSVP_FromArtSVP(svp);
}

/* Fill an ArtVpathDash struct from a Python tuple.
 *
 * ARTDASH is the pointer to the ArtVpathDash to fill. DASHTUPLE is a
 * Python tuple of float objects and WIDTH the width of the line. In
 * Sketch the dashes are proportionaly to the line width, so multiply
 * all items in DASHTUPLE by WIDTH when adding them to ARTDASH.
 *
 * Return 1 on success and set a Python exception and return 0 when an
 * error occurs.
 */
static int
fill_art_dash(ArtVpathDash * artdash, PyObject *dashtuple, double width)
{
    int i;

    artdash->offset = 0.0;
    artdash->n_dash = PySequence_Length(dashtuple);
    artdash->dash = malloc(artdash->n_dash * sizeof (*(artdash->dash)));

    if (!artdash->dash)
    {
	PyErr_NoMemory();
	return 0;
    }
    for (i = 0; i < artdash->n_dash; i++)
    {
	PyObject * d;
	d = PySequence_GetItem(dashtuple, i);
	artdash->dash[i] = width * PyFloat_AsDouble(d);
    }
    return 1;
}


/* Return an SVP object for the outline of a PolyBezier object
 */
static PyObject *
SKSVP_FromSKCurveStroke(PyObject * self, PyObject * args)
{
    PyObject * ocurves, *dashtuple;
    int ncurves;
    SKCurveObject ** curves;
    SKTrafoObject * trafo;
    double scale;
    int join, cap;
    double width;
    
    ArtVpath * vpath = NULL;
    ArtSVP * svp;
    ArtVpathDash artdash;

    artdash.dash = NULL;

    if (!PyArg_ParseTuple(args, "OO!ddiiO", &ocurves,
			  sketch_interface->SKTrafoType, &trafo, &scale,
			  &width, &join, &cap, &dashtuple))
	return NULL;

    if (ocurves->ob_type == sketch_interface->SKCurveType)
    {
	curves = (SKCurveObject**)&ocurves;
	ncurves = 1;
    }
    else if (PyTuple_Check(ocurves))
    {
	curves = (SKCurveObject**)(&PyTuple_GET_ITEM(ocurves, 0));
	ncurves = PyTuple_Size(ocurves);
    }
    else
    {
	PyErr_SetString(PyExc_TypeError, "argument must be a single "
			"curve object or a tuple of curves");
	return NULL;
    }

    vpath = vpath_from_curves(curves, ncurves, trafo, 0, 1);
    if (!vpath)
	goto fail;

    if (PySequence_Check(dashtuple) && PyObject_IsTrue(dashtuple))
    {
	ArtVpath * tmp;
	double dash_width;

	if (width < 1.0)
	    dash_width = scale;
	else
	    dash_width = scale * width;

	if (!fill_art_dash(&artdash, dashtuple, dash_width))
	    goto fail;
	
	tmp = art_vpath_dash(vpath, &artdash);
	if (!tmp)
	    goto fail;
	art_free(vpath);
	vpath = tmp;
    }
    
    width = scale * width;
    if (width < 1.0)
	width = 1.0;
    svp = art_svp_vpath_stroke(vpath, join, cap, width, 4, 0.5);

    if (artdash.dash)
	free(artdash.dash);
    art_free(vpath);
    if (!svp)
	return PyErr_NoMemory();

    return SKSVP_FromArtSVP(svp);

 fail:
    if (artdash.dash)
	free(artdash.dash);
    if (vpath)
	art_free(vpath);
    return NULL;
}

/* Return an SVP object for a straight line between two points
 */
static PyObject *
SKSVP_FromLine(PyObject * self, PyObject * args)
{
    PyObject * start, * end, *dashtuple = NULL;
    ArtVpath vpatharray[3];
    ArtVpath *vpath = vpatharray, *dashed = NULL;
    ArtVpathDash artdash;
    int join, cap;
    double width;
    ArtSVP * svp;

    artdash.dash = NULL;

    if (!PyArg_ParseTuple(args, "OOdii|O", &start, &end, &width, &join, &cap,
			  &dashtuple))
	return NULL;

    if (width < 1.0)
	width = 1.0;

    if (!sketch_interface->skpoint_extract_xy(start, &vpath[0].x, &vpath[0].y)
	|| !sketch_interface->skpoint_extract_xy(end, &vpath[1].x,&vpath[1].y))
    {
	PyErr_SetString(PyExc_TypeError, "The first two arguments must be "
			"points or tuplesd of two numbers");
	return NULL;
    }

    vpath[0].code = ART_MOVETO_OPEN;
    vpath[1].code = ART_LINETO;
    vpath[2].code = ART_END;
    vpath[2].x = vpath[2].y = 0;

    if (dashtuple && PySequence_Check(dashtuple) && PyObject_IsTrue(dashtuple))
    {
	if (!fill_art_dash(&artdash, dashtuple, width))
	    goto fail;
	
	dashed = art_vpath_dash(vpath, &artdash);
	if (!dashed)
	    goto fail;
	vpath = dashed;
    }

    svp = art_svp_vpath_stroke(vpath, join, cap, width, 4, 0.5);
    if (dashed)
	art_free(dashed);
    if (!svp)
	return PyErr_NoMemory();
    return SKSVP_FromArtSVP(svp);

 fail:
    if (artdash.dash)
	free(artdash.dash);
    if (dashed)
	art_free(dashed);
    return NULL;
}

/* Return an SVP object for a filled axis-aligned rectangle
 */
static PyObject *
SKSVP_FromFilledRectangle(PyObject * self, PyObject * args)
{
    ArtVpath vpath[6];
    double x0, y0, x1, y1;
    ArtSVP * svp, *tmp;

    if (!PyArg_ParseTuple(args, "dddd", &x0, &y0, &x1, &y1))
	return NULL;

    vpath[0].x = x0;
    vpath[0].y = y0;
    vpath[0].code = ART_MOVETO;
    vpath[1].x = x1;
    vpath[1].y = y0;
    vpath[1].code = ART_LINETO;
    vpath[2].x = x1;
    vpath[2].y = y1;
    vpath[2].code = ART_LINETO;
    vpath[3].x = x0;
    vpath[3].y = y1;
    vpath[3].code = ART_LINETO;
    vpath[4].x = x0;
    vpath[4].y = y0;
    vpath[4].code = ART_LINETO;
    vpath[5].code = ART_END;
    vpath[5].x = vpath[5].y = 0;

    svp = art_svp_from_vpath(vpath);
    if (!svp)
	return PyErr_NoMemory();

    tmp = art_svp_uncross(svp);
    art_svp_free(svp);
    svp = tmp;
    if (!svp)
	return PyErr_NoMemory();
    tmp = art_svp_rewind_uncrossed(svp, ART_WIND_RULE_ODDEVEN);
    art_svp_free(svp);
    svp = tmp;
    if (!svp)
	return PyErr_NoMemory();
    
    return SKSVP_FromArtSVP(svp);
}


/* Print the contents of an ArtSVP to stdout.
 * For debugging purposes.
 */
static void
print_svp(ArtSVP * vp)
{
    int i, j;

    printf("SVP: %d segments\n", vp->n_segs);
  
    for (i = 0; i < vp->n_segs; i++)
    {
	printf ("segment %d, dir = %s (%f, %f) - (%f, %f)\n",
		i, vp->segs[i].dir ? "down" : "up",
		vp->segs[i].bbox.x0,
		vp->segs[i].bbox.y0,
		vp->segs[i].bbox.x1,
		vp->segs[i].bbox.y1);
	for (j = 0; j < vp->segs[i].n_points; j++)
	    printf ("  (%g, %g)\n",
		    vp->segs[i].points[j].x,
		    vp->segs[i].points[j].y);
    }
}


/* Print the contents of an SVP object to stdout.
 * For debugging purposes.
 */
static PyObject *
sksvp_print(PyObject * self, PyObject * args)
{
    SKSVPObject * svp;

    if (!PyArg_ParseTuple(args, "O!", &SKSVPType, &svp))
	return NULL;
    print_svp(svp->svp);

    Py_INCREF(Py_None);
    return Py_None;
}


/* Return the intersection of two SVP objects.
 */
static PyObject *
sksvp_intersect(PyObject * self, PyObject * args)
{
    SKSVPObject * svp1, *svp2;
    ArtSVP * result;
	 
    if (!PyArg_ParseTuple(args, "O!O!", &SKSVPType, &svp1,
			  &SKSVPType, &svp2))
	return NULL;

    result = art_svp_intersect(svp1->svp, svp2->svp);
    if (!result)
	return PyErr_NoMemory();

    return SKSVP_FromArtSVP(result);
}


/* Render an SVP object with a solid RGB color onto a pixbuf or image
 *
 * The offset is a pair of ints which describe the position of the top
 * left corner of the pixbuf in the coordinate system of the SVP.
 */
static PyObject *
sksvp_render_rgb(PyObject * self, PyObject * args)
{
    PyObject * image_or_pixbuf;
    SKSVPObject * svp;
    int color;
    int x0 = 0, y0 = 0, x1, y1;
    PyObject * offset = NULL;

    if (!PyArg_ParseTuple(args, "OO!i|O", &image_or_pixbuf, &SKSVPType, &svp,
			  &color, &offset))
	return NULL;

    if (offset)
    {
	if (!PyArg_ParseTuple(offset, "ii", &x0, &y0))
	    return NULL;
    }

    if (SKArtPixBuf_Check(image_or_pixbuf))
    {
	ArtPixBuf * pixbuf = ((SKArtPixBufObject*)image_or_pixbuf)->artpixbuf;
	x1 = x0 + pixbuf->width;
	y1 = y0 + pixbuf->height;

	/* FIXME: Should this be checking the has_alpha attribute instead? */
	if (pixbuf->n_channels == 3)
	    art_rgb_svp_alpha(svp->svp, x0, y0, x1, y1, color,
			      pixbuf->pixels, pixbuf->rowstride, NULL);
	else
	    skrender_svp_rgb(&((SKArtPixBufObject*)image_or_pixbuf)->skpixbuf,
			       x0, y0, x1, y1, color, svp->svp);
    }
    else
    {
	SKPixbuf skpixbuf, *pixbuf;
	pixbuf = convert_image_or_pixbuf(image_or_pixbuf, &skpixbuf);
	x1 = x0 + pixbuf->width;
	y1 = y0 + pixbuf->height;
        skrender_svp_rgb(pixbuf, x0, y0, x1, y1, color, svp->svp);
    }

    Py_INCREF(Py_None);
    return Py_None;
}

/* Render a scaled image onto a pixbuf
 */ 

static PyObject *
sksvp_render_scaled_image(PyObject * self, PyObject * args)
{
    PyObject * image_or_pixbuf;
    PyObject * svp_or_none;
    PyObject * srcimage;
    SKSVPObject * svp = NULL;
    int x0 = 0, y0 = 0, x1, y1;
    int dest_x, dest_y, dest_width, dest_height;
    PyObject * offset = NULL;
    SKPixbuf destbuf, srcbuf;
    SKPixbuf * pdestbuf;
    SKPixbuf * psrcbuf;

    if (!PyArg_ParseTuple(args, "OOOiiii|O", &image_or_pixbuf,
			  &svp_or_none, &srcimage,
			  &dest_x, &dest_y, &dest_width, &dest_height,
			  &offset))
	return NULL;

    if (SKSVP_Check(svp_or_none))
	svp = (SKSVPObject*)svp_or_none;
    
    if (offset)
    {
	if (!PyArg_ParseTuple(offset, "ii", &x0, &y0))
	    return NULL;
    }

    pdestbuf = convert_image_or_pixbuf(image_or_pixbuf, &destbuf);
    x1 = x0 + pdestbuf->width;
    y1 = y0 + pdestbuf->height;

    psrcbuf = convert_image_or_pixbuf(srcimage, &srcbuf);

    if (svp)
	skrender_scale_image_svp(pdestbuf, x0, y0, x1, y1,
				 &srcbuf, dest_x, dest_y,
				 dest_width, dest_height, svp->svp);
    else
	skrender_scale_image(pdestbuf, x0, y0, x1, y1,
			     &srcbuf, dest_x, dest_y,
			     dest_width, dest_height);
	
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
sksvp_render_transformed_image(PyObject * self, PyObject * args)
{
    PyObject * image_or_pixbuf;
    PyObject * svp_or_none;
    PyObject * srcimage;
    SKSVPObject * svp = NULL;
    SKTrafoObject * trafo;
    int x0 = 0, y0 = 0, x1, y1;
    int dest_x, dest_y, dest_width, dest_height;
    PyObject * offset = NULL;
    SKPixbuf destbuf, srcbuf;
    SKPixbuf * pdestbuf;
    SKPixbuf * psrcbuf;

    if (!PyArg_ParseTuple(args, "OOOO!iiii|O", &image_or_pixbuf,
			  &svp_or_none, &srcimage,
			  sketch_interface->SKTrafoType, &trafo,
			  &dest_x, &dest_y, &dest_width, &dest_height,
			  &offset))
	return NULL;

    if (SKSVP_Check(svp_or_none))
	svp = (SKSVPObject*)svp_or_none;
    
    if (offset)
    {
	if (!PyArg_ParseTuple(offset, "ii", &x0, &y0))
	    return NULL;
    }

    pdestbuf = convert_image_or_pixbuf(image_or_pixbuf, &destbuf);
    x1 = x0 + pdestbuf->width;
    y1 = y0 + pdestbuf->height;

    psrcbuf = convert_image_or_pixbuf(srcimage, &srcbuf);

    skrender_transform_image(pdestbuf, x0, y0, x1, y1,
			     &srcbuf, trafo, dest_x, dest_y,
			     dest_width, dest_height, svp ? svp->svp : NULL);
	
    Py_INCREF(Py_None);
    return Py_None;
}

/*
 *  guide lines
 */

static PyObject *
render_guide_line(PyObject * self, PyObject * args)
{
    PyObject * image_or_pixbuf;
    PyObject * offset = NULL;
    int coordinate, horizontal, color;
    int x0 = 0, y0 = 0, x1, y1;
    SKPixbuf * dest;
    SKPixbuf skpixbuf;

    if (!PyArg_ParseTuple(args, "OiiiO", &image_or_pixbuf, &coordinate,
			  &horizontal, &color, &offset))
	return NULL;

    if (offset)
    {
	if (!PyArg_ParseTuple(offset, "ii", &x0, &y0))
	    return NULL;
    }

    dest = convert_image_or_pixbuf(image_or_pixbuf, &skpixbuf);
    x1 = x0 + dest->width;
    y1 = y0 + dest->height;

    if (horizontal)
	skrender_horizontal_guide_line(dest, x0, y0, x1, y1, coordinate,
				       color);
    else
	skrender_vertical_guide_line(dest, x0, y0, x1, y1, coordinate, color);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
render_grid(PyObject * self, PyObject * arg)
{
    double xwidth, ywidth;
    double orig_x, orig_y;
    PyObject * image_or_pixbuf;
    PyObject * offset = NULL;
    int color;
    int x0 = 0, y0 = 0, x1, y1;
    SKPixbuf * dest;
    SKPixbuf skpixbuf;

    if (!PyArg_ParseTuple(arg, "OddddiO", &image_or_pixbuf, &orig_x, &orig_y,
			  &xwidth, &ywidth, &color, &offset))
	return NULL;

    if (offset)
    {
	if (!PyArg_ParseTuple(offset, "ii", &x0, &y0))
	    return NULL;
    }

    dest = convert_image_or_pixbuf(image_or_pixbuf, &skpixbuf);
    x1 = x0 + dest->width;
    y1 = y0 + dest->height;

    skrender_grid(dest, x0, y0, x1, y1, orig_x, orig_y, xwidth, ywidth, color);

    Py_INCREF(Py_None);
    return Py_None;
}

    

/*
 * Grid
 */

static PyObject *
skpixbuf_new_rgb(PyObject * arg, PyObject * args)
{
    ArtPixBuf * buf;
    art_u8 * data;
    PyObject * result;
    int width, height, rowstride = 0;

    if (!PyArg_ParseTuple(args, "ii|i", &width, &height, &rowstride))
	return NULL;

    if (!rowstride)
    {
	rowstride = width * 4;
    }
    data = art_alloc(height * rowstride);
    if (!data)
	return PyErr_NoMemory();
    memset(data, 255, height * rowstride);
    buf = art_pixbuf_new_rgba(data, width, height, rowstride);
    if (!buf)
    {
	art_free(data);
	return PyErr_NoMemory();
    }

    result = SKArtPixBuf_FromArtPixBuf(buf);
    if (!buf)
    {
	art_pixbuf_free(buf);
	return PyErr_NoMemory();
    }
    
    return result;
}    


static PyObject *
skpixbuf_export_pixbuf(PyObject * arg, PyObject * args)
{
    PyObject * image_or_pixbuf;
    SKPixbuf skpixbuf, *pixbuf;
    char *output;
    int i, k, size;

    if (!PyArg_ParseTuple(args, "O", &image_or_pixbuf))
        return NULL;
    
    /* Obtain an SKPixbuf object. */
    pixbuf = convert_image_or_pixbuf(image_or_pixbuf, &skpixbuf);
    
    /* The alpha values must be removed. */
    
    output = malloc(pixbuf->width * pixbuf->height * 3 * sizeof(char));
    
    if (output == NULL)
    {
        Py_INCREF(Py_None);
        return Py_None;
    }
    
    if (pixbuf->has_alpha == 1)
        size = 4;
    else
        size = 3;
    
    i = 0;
    k = 0;
    
    while (i < pixbuf->height)
    {
        char *row = (char *)pixbuf->data32[i];
        int j = 0;
        
        while (j < (pixbuf->width * size))
        {
            output[k]   = row[j];
            output[k+1] = row[j+1];
            output[k+2] = row[j+2];
            j = j + size;
            k = k + 3;
        }
        
        i = i + 1;
    }
    
/*    Py_INCREF(output);*/
    
    return Py_BuildValue( "(i,i,s#)", pixbuf->width, pixbuf->height,
                          output, pixbuf->width * pixbuf->height * 3 );
}


/* Color helper functions.
 * 
 * Starting with Python 2.4 (actually 2.3 because it prints warnings)
 * bitwise operations on ints become a bit more tedious if you need 32
 * bits and want to stay with normal int-objects and not longs.
 */


/* Return the color given by the float values for RGB components and
 * opacity as an int object
 */
static PyObject*
convert_color(PyObject * self, PyObject * args)
{
    double red, green, blue, opacity;

    if (!PyArg_ParseTuple(args, "dddd", &red, &green, &blue, &opacity))
	return NULL;

    return PyInt_FromLong(((int)(red * 255) << 24)
			  | ((int)(green * 255) << 16) 
			  | ((int)(blue * 255) << 8)
			  | (int)(opacity * 255));
}

/* Return an int with the same RGB components but new opacity */
static PyObject*
set_opacity(PyObject * self, PyObject * args)
{
    int color;
    double opacity;

    if (!PyArg_ParseTuple(args, "id", &color, &opacity))
	return NULL;

    return PyInt_FromLong((color & 0xFFFFFF00) | (int)(opacity * 255));
}

/****/

static PyMethodDef libart_functions[] = {
    {"svp_from_curve",		SKSVP_FromSKCurve,	METH_VARARGS},
    {"svp_from_curve_stroke",	SKSVP_FromSKCurveStroke,METH_VARARGS},
    {"svp_from_line",		SKSVP_FromLine,		METH_VARARGS},
    {"svp_from_filled_rectangle",SKSVP_FromFilledRectangle,	METH_VARARGS},
    {"print_svp",		sksvp_print,		METH_VARARGS},
    {"intersect_svp",		sksvp_intersect,	METH_VARARGS},
    {"render_svp_rgb",		sksvp_render_rgb,	METH_VARARGS},
    {"render_svp_scaled_image",	sksvp_render_scaled_image, METH_VARARGS},
    {"render_svp_transformed_image",sksvp_render_transformed_image,
     METH_VARARGS},
    {"render_guide_line",	render_guide_line,	METH_VARARGS},
    {"render_grid",		render_grid,		METH_VARARGS},
    {"pixbuf_new_rgb",		skpixbuf_new_rgb,	METH_VARARGS},
    {"export_pixbuf",       skpixbuf_export_pixbuf,	METH_VARARGS},

    {"convert_color",		convert_color,		METH_VARARGS},
    {"set_opacity",		set_opacity,		METH_VARARGS},
    
    {NULL, NULL}
};

static void
add_int(PyObject * dict, char * name, int i)
{
    PyObject *v;
    
    v = Py_BuildValue("i", i);
    if (v)
    {
	PyDict_SetItemString(dict, name, v);
	Py_DECREF(v);
    }
}

void
init_libart(void)
{
    PyObject * d, *m, *module;

    /* Patch object types */
    SKSVPType.ob_type = &PyType_Type;
    SKArtPixBufType.ob_type = &PyType_Type;
    
    m = Py_InitModule("_libart", libart_functions);
    d = PyModule_GetDict(m);

#define ADD_INT(name) add_int(d, #name, name)

    ADD_INT(ART_PATH_STROKE_JOIN_MITER);
    ADD_INT(ART_PATH_STROKE_JOIN_ROUND);
    ADD_INT(ART_PATH_STROKE_JOIN_BEVEL);
    ADD_INT(ART_PATH_STROKE_CAP_BUTT);
    ADD_INT(ART_PATH_STROKE_CAP_ROUND);
    ADD_INT(ART_PATH_STROKE_CAP_SQUARE);
    ADD_INT(ART_WIND_RULE_NONZERO);
    ADD_INT(ART_WIND_RULE_INTERSECT);
    ADD_INT(ART_WIND_RULE_ODDEVEN);
    ADD_INT(ART_WIND_RULE_POSITIVE);

    /* libart type objects */
    PyDict_SetItemString(d, "SKSVPType", (PyObject*)&SKSVPType);
    PyDict_SetItemString(d, "SKArtPixBufType", (PyObject*)&SKArtPixBufType);
    
    /* import some objects from _sketch */
    module = PyImport_ImportModule("Sketch._sketch");
    if (module)
    {
	PyObject * r;

	/* get some things from _sketchmodule.so */
	r = PyObject_GetAttrString(module, "interface");
	if (r)
	{
	    /* 0.7.x defines the interface pointer ... */
	    sketch_interface = PyCObject_AsVoidPtr(r);
	}
	else
	{
	    /* ... but 0.6.x doesn't. Create a local interface struct
               and fill it with the necessary values */
	    static Sketch_Interface inter;
	    PyErr_Clear();
	    inter.SKTrafoType \
		= (PyTypeObject*)PyObject_GetAttrString(module, "TrafoType");
	    inter.SKCurveType \
		= (PyTypeObject*)PyObject_GetAttrString(module, "CurveType");
	    sketch_interface = &inter;
	}
	Py_XDECREF(r);
    }

}
