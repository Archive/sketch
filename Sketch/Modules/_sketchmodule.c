/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <math.h>
#include <float.h>
#include <Python.h>

#include "skpoint.h"
#include "skrect.h"
#include "sktrafo.h"
#include "skfm.h"
#include "curveobject.h"
#include "curvefunc.h"
#include "curvefit.h"
#include "skimage.h"
#include "skcolor.h"
#include "skaux.h"
#include "_sketchpublic.h"

static Sketch_Interface interface = {
    (PyTypeObject*)&SKPointType,
    (PyTypeObject*)&SKTrafoType,
    (PyTypeObject*)&SKRectType,
    (PyTypeObject*)&SKColorType,
    (PyTypeObject*)&SKCurveType,
    SKTrafo_TransformXY,
    SKRect_AddXY,
    skpoint_extract_xy,
};


static PyMethodDef sketch_functions[] = {

    /* Point functions */
    {"Point",			SKPoint_PyPoint,		METH_VARARGS},
    {"Polar",			SKPoint_PyPolar,		METH_VARARGS},
    {"points_allocated",	skpoint_allocated,		METH_VARARGS},

    /* Rect functions */
    {"Rect",			skrect_skrect,			METH_VARARGS},
    {"UnionRects",		skrect_unionrects,		METH_VARARGS},
    {"PointsToRect",		skrect_PointsToRect,		METH_VARARGS},
    {"IntersectRects",		skrect_intersect,		METH_VARARGS},
    {"rects_allocated",		skrect_allocated,		METH_VARARGS},

    /* Trafo functions */
    {"Trafo",			sktrafo_sktrafo,		METH_VARARGS},
    {"Scale",			sktrafo_scale,			METH_VARARGS},
    {"Translation",		sktrafo_translation,		METH_VARARGS},
    {"Rotation",		sktrafo_rotation,		METH_VARARGS},
    {"trafos_allocted",		sktrafo_allocated,		METH_VARARGS},
    
    /* FontMetric functions */
    {"CreateFontMetric",	SKFM_PyCreateMetric,		METH_VARARGS},
    
    /* Curve functions */
    {"test_transformed",	SKCurve_PyTestTransformed,	METH_VARARGS},
    {"blend_paths",		SKCurve_PyBlendPaths,		METH_VARARGS},
    {"CreatePath",		SKCurve_PyCreatePath,		METH_VARARGS},
    {"approx_arc",		SKCurve_PyApproxArc,		METH_VARARGS},
    {"RectanglePath",		SKCurve_PyRectanglePath,	METH_VARARGS},
    {"RoundedRectanglePath",	SKCurve_PyRoundedRectanglePath,	METH_VARARGS},
    {"FindFreehandCorners",	SKCurve_PyFindFreehandCorners,	METH_VARARGS},
    {"FitBezierWithTangents",	SKCurve_PyFitBezierWithTangents,METH_VARARGS},
    {"RefineParameters",	SKCurve_PyRefineParameters,	METH_VARARGS},
    {"FreehandDistances",	SKCurve_PyFreehandDistances,	METH_VARARGS},
    {"FreehandEstimateParams",	SKCurve_PyFreehandEstimateParams,METH_VARARGS},
    {"num_allocated",		_SKCurve_PyNumAllocated,	METH_VARARGS},

    /* image functions */
    {"fill_rgb_xy",		fill_rgb_xy,			METH_VARARGS},
    {"fill_rgb_z",		fill_rgb_z,			METH_VARARGS},
    {"fill_hsv_xy",		fill_hsv_xy,			METH_VARARGS},
    {"fill_hsv_z",		fill_hsv_z,			METH_VARARGS},
    {"fill_axial_gradient",	fill_axial_gradient,		METH_VARARGS},
    {"fill_radial_gradient",	fill_radial_gradient,		METH_VARARGS},
    {"fill_conical_gradient",	fill_conical_gradient,		METH_VARARGS},
    {"fill_transformed_tile",	fill_transformed_tile,		METH_VARARGS},
    {"write_ps_hex",		skimage_write_ps_hex,		METH_VARARGS},

    /* color functions */
    {"RGBColor",		SKColor_PyRGBColor,		METH_VARARGS},
    {"colors_allocated",	SKColor_PyNumAllocated,		METH_VARARGS},

    /* skaux */
    {"TransformRectangle",	SKAux_PyTransformRectangle,	METH_VARARGS},
    {"IdIndex",			SKAux_PyIdIndex,		METH_VARARGS},
    {"SKCache",			SKCache_PyCreate,		METH_VARARGS},

    /* */
    {NULL,		NULL} 
};


/*
 *	Init module
 */
static void
add_int(PyObject * dict, char * name, int i)
{
    PyObject *v;
    
    v = Py_BuildValue("i", i);
    if (v)
    {
	PyDict_SetItemString(dict, name, v);
	Py_DECREF(v);
    }
}

static void
add_ptr(PyObject * dict, char * name, void * ptr)
{
    PyObject *v;
    
    v = PyCObject_FromVoidPtr(ptr, NULL);
    if (v)
    {
	PyDict_SetItemString(dict, name, v);
	Py_DECREF(v);
    }
}

void
init_sketch(void)
{
    PyObject * d, *m, *r;

    /* Patch object types */
    SKCurveType.ob_type = &PyType_Type;
    SKCacheType.ob_type = &PyType_Type;
    SKColorType.ob_type = &PyType_Type;
    SKFontMetricType.ob_type = &PyType_Type;
    SKPointType.ob_type = &PyType_Type;
    SKRectType.ob_type = &PyType_Type;
    SKTrafoType.ob_type = &PyType_Type;

    m = Py_InitModule("_sketch", sketch_functions);
    d = PyModule_GetDict(m);

    /* Sketch type objects */
    PyDict_SetItemString(d, "RectType", (PyObject*)&SKRectType);
    PyDict_SetItemString(d, "PointType", (PyObject*)&SKPointType);
    PyDict_SetItemString(d, "TrafoType", (PyObject*)&SKTrafoType);
    PyDict_SetItemString(d, "CurveType", (PyObject*)&SKCurveType);
    PyDict_SetItemString(d, "ColorType", (PyObject*)&SKColorType);

    /* Rect specific initialization */
    /* The InfinityRect is initialized with FLT_MAX instead of HUGE_VAL
       now (Sketch 0.5.4), because of problems with HUGE_VAL on Alpha
       Linux. */
    r = SKRect_FromDouble(-FLT_MAX, -FLT_MAX, FLT_MAX, FLT_MAX);
    PyDict_SetItemString(d, "InfinityRect", r);
    SKRect_InfinityRect = (SKRectObject*)r;
    
    r = SKRect_FromDouble(0.0, 0.0, 0.0, 0.0);
    PyDict_SetItemString(d, "EmptyRect", r);
    SKRect_EmptyRect = (SKRectObject*)r;

    /* Trafo specific initialization */
    SKTrafo_ExcSingular = PyErr_NewException("_sketch.SingularMatrix",
					     PyExc_ArithmeticError, NULL);
    PyDict_SetItemString(d, "SingularMatrix", SKTrafo_ExcSingular);
    
    /* Curve specific initialization */
#define ADD_INT(name) add_int(d, #name, name)
#define ADD_INT2(i, name) add_int(d, name, i)
    ADD_INT(ContAngle);
    ADD_INT(ContSmooth);
    ADD_INT(ContSymmetrical);
    ADD_INT2(CurveBezier, "Bezier");
    ADD_INT2(CurveLine, "Line");
    ADD_INT(SelNone);
    ADD_INT(SelNodes);
    ADD_INT(SelSegmentFirst);
    ADD_INT(SelSegmentLast);

    _SKCurve_InitCurveObject();

    add_ptr(d, "interface", &interface);
}
