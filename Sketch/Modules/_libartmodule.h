/* Sketch - A Python-based interactive drawing program
 * Copyright (C) 1999 by Bernhard Herzog
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA
 */

#ifndef SKLIBART_H
#define SKLIBART_H

#include <Python.h>
#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_pixbuf.h>

#include "skrender.h"

typedef struct {
    PyObject_HEAD
    ArtSVP * svp;
} SKSVPObject;

extern PyTypeObject SKSVPType;
#define SKSVP_Check(v)		((v)->ob_type == &SKSVPType)

typedef struct {
    PyObject_HEAD
    ArtPixBuf * artpixbuf;
    SKPixbuf skpixbuf;
} SKArtPixBufObject;

extern PyTypeObject SKArtPixBufType;
#define SKArtPixBuf_Check(v)	((v)->ob_type == &SKArtPixBufType)


#endif /* SKLIBART_H */
