# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2002, 2003 by Bernhard Herzog
#
# ScratchPad - Simplified pixmap drawing
# Copyright (C) 2001 by Mark Rose <mrose@stm.lbl.gov>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""ScratchPad

General purpose rendering to bitmaps
"""

__version__ = "$Revision$"

from Sketch import Trafo, Point
import gtk
from Sketch.Graphics import StandardColors

from gtkdevice import GraphicsDevice
try:
    from libartdevice import LibartDevice
    libart_supported = 1
except ImportError:
    libart_supported = 0

class ScratchPad:

    """
    General purpose rendering to bitmaps

    Instantiate with desired size and scale, then use like the
    libartdevice (whether or not libart is available).

    BeginDraw initializes a new pixmap to which further
    drawing occurs.  After EndDraw, call GetPixmap to
    retrieve the rendered pixmap.  You can then call
    BeginDraw again to render another pixmap if desired.
    """

    def __init__(self, window, width, height, scale = 1.0, libart = 1):
        if libart_supported and libart:
            self.device = device = LibartDevice()
            device.init_gc(window, None)
            self.libart = 1
        else:
            self.device = device = GraphicsDevice()
            device.init_gc(window, graphics_exposures = 1)
            self.libart = 0
        trafo = Trafo(scale, 0, 0, scale, width * 0.5, height * 0.5)
        device.SetViewportTransform(scale, trafo, trafo.inverse())
        self.window = window
        self.width = width
        self.height = height

    def __getattr__(self, attr):
        return getattr(self.device, attr)

    def BeginDraw(self, depth = None):
        self._new_pixmap(depth)
        if self.libart:
            self.device.BeginDraw()
        else:
            w, h = self.width, self.height
            color = self.current_color
            self.SetFillColor(StandardColors.white)
            self.FillRectangle(-w * 0.5, -h * 0.5, w, h)
            self.SetFillColor(color)

    def DrawRectangle(self, *args):
        self.device.DrawRectangle(Point(args[0], args[1]),
                                  Point(args[2], args[3]))

    def EndDraw(self):
        if self.libart:
            self.device.EndDraw()

    def _new_pixmap(self, depth = None):
        if depth is None:
            depth = -1
        self.pixmap = gtk.gdk.Pixmap(self.window, self.width, self.height,
                                     depth)
        self.device.drawable = self.pixmap

    def GetPixmap(self):
        return self.pixmap

