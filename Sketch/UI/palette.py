# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2002, 2003, 2005 by Bernhard Herzog
#
# Palette - Configurable fill palettes for Sketch
# Copyright (C) 2001 by Mark Rose <mrose@stm.lbl.gov>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

__version__ = "$Revision$"

import time
import os.path
from glob import glob
import gtk

import Sketch
import Sketch.UI

from Sketch import _, Trafo, Rect, Point
from Sketch.Base import Publisher, config
from Sketch.Base.const import CHANGED

from Sketch.Graphics import StandardColors, CreateRGBColor
from Sketch.Graphics.pattern import SolidPattern, EmptyPattern
from Sketch.Graphics.arrow import read_arrows

from skpanel import SketchPanel
from fillpanel import PatternSample
from gtkmisc import SketchDrawingArea
from read_palette import read_palette_file
from scratchpad import ScratchPad

white = StandardColors.white
black = StandardColors.black


# DESIGN ISSUES & OTHER RAMBLINGS
#
# Drag-'n-drop: seems pointless for acting on canvas widgets.
# It is just as easy (and intuitive) to select one or more
# objects on the canvas, then pick a color.  It's always the
# same (or less) mouse work as drag-'n-drop.
#
# Where DND may be useful is creating customized palettes.
# For now, however,  palettes are static.
#
# The GeoWidget layout and sliding is asymmetric in x and y,
# which may make tall or wide windows easier to work with.
# Give some thought to other layout schemes.
#
# Should we include palette editing in the UI?  There are
# a few empty hooks for managing entire palettes, but there
# could also be ways to interactively customize palettes,
# such as selecting a range of patterns and dragging them
# into another palette window.  Double clicking a palette
# entry could bring up the fill panel to let the user modify
# it.  Then we might want to think about mixing different
# fill types in a single palette.  There could also be a
# "magic" palette of recently used fills.  Palettes could
# also be extended to include line styles.  Note, however,
# that some of these capabilities would overlap with the
# function of the Properties / Style panel.

# TODO break this module into smaller modules


# Palette classes
#
# currently there is a separate class for each type of palette,
# although it may be desirable to mix together different classes
# in custom palettes (e.g. solid and gradient fills).  See the
# design notes above.

class Palette(Publisher):

    def __init__(self, name=''):
        self.name = name
        self.items = []

    def load(self):
        return 0

    def Name(self):
        return self.name


class SolidPalette(Palette):

    def __init__(self, name=''):
        Palette.__init__(self, name)

    def load(self, filename):
        items = read_palette_file(filename)
        self.items = map(lambda x: (apply(CreateRGBColor,x[0]), x[1]), items)
        self.items[0:0] = [(None, "Empty")]
        return len(self.items)

    def NewFrame(self, context):
        frame = SolidPaletteFrame(self)
        frame.SetContext(context)
        return frame


class ArrowPalette(Palette):

    def load(self, filename):
        self.items = read_arrows(filename)
        self.bboxes= []
        for arrow in self.items:
            bbox = arrow.BoundingRect(Point(0, 0), Point(1, 0), 1)
            self.bboxes.append(bbox)
        return len(self.items)

    def GeneratePixmaps(self, window, size = 32):
        pad = ScratchPad(window, size, size, 1.0)
        maps = []
        scale = 1.0
        for arrow in self.items:
            bbox = arrow.BoundingRect(Point(0, 0), Point(1, 0), 1)
            m = max(abs(bbox[0]), abs(bbox[1]), abs(bbox[2]), abs(bbox[3]))
            pad.BeginDraw()
            scaleby = size * 0.5 / (scale * m)
            scale = scale * scaleby
            pad.Scale(scaleby)
            arrow.Draw(pad)
            pad.EndDraw()
            maps.append(pad.GetPixmap())
        return maps

    def NewFrame(self, context):
        frame = ArrowPaletteFrame(self)
        frame.SetContext(context)
        return frame


# SwatchWidget, SwatchFrame, SwatchSample and SwatchTip
#
# classes for interactive palette display.
#    SwatchWidget : lays out and draws the palette
#    SwatchFrame  : handles mouse and keyboard events, controls sliding
#    SwatchSample : part of SwatchTip to provide pattern previews
#    SwatchTip    : "tooltip" giving palette info and preview

LEFT_SHIFT = 1 << 1
RIGHT_SHIFT = 1 << 2
LEFT_CONTROL = 1 << 3
RIGHT_CONTROL = 1 << 4
SHIFT = LEFT_SHIFT | RIGHT_SHIFT
CONTROL = LEFT_CONTROL | RIGHT_CONTROL

class SwatchWidget(SketchDrawingArea):

    VERTICAL = 0
    HORIZONTAL = 1

    def __init__(self, scale = 16, direction = VERTICAL):
        SketchDrawingArea.__init__(self)
        self.scale = scale
        self.pixmap = None
        self.origin = Point(0, 0)
        self.cols = self.rows = 0
        self.items = []
        self.set_size_request(self.scale, self.scale)
        self.set_direction(direction)

    def set_direction(self, dir):
        self.direction = dir

    def SetDirection(self, dir):
        if dir != self.direction:
            self.set_direction(dir)
            self.layout(1)

    def ResizedMethod(self, width, height):
        self.gc.WindowResized(width, height)
        self.UpdateWhenIdle()
        self.compute_trafo()

    def compute_trafo(self):
        if not hasattr(self, 'window'):
            return
        width, height = self.window.get_size()
        doc_to_win = Trafo(1, 0, 0, 1, 0, 0)
        win_to_doc = doc_to_win.inverse()
        self.gc.SetViewportTransform(1.0, doc_to_win, win_to_doc)
        self.layout()

    def layout(self, force = 0):
        if self.window is None:
            return
        w, h = self.window.get_size()
        if w > h: self.direction = self.HORIZONTAL
        else: self.direction = self.VERTICAL
        s = self.scale
        if self.direction == self.VERTICAL:
            cols = int(w / s)
            if cols == 0: cols = 1
            rows = float(len(self.items)) / cols
            if rows > int(rows): rows = rows + 1
            rows = int(rows)
            x = self.origin.x
            y = min(0, max(self.origin.y, h - rows * s))
            refresh = cols != self.cols or y != self.origin.y
        else:
            rows = int(h / s)
            if rows == 0: rowss = 1
            cols = float(len(self.items)) / rows
            if cols > int(cols): cols = cols + 1
            cols = int(cols)
            x = min(0, max(self.origin.x, w - cols * s))
            y = self.origin.y
            refresh = rows != self.rows or x != self.origin.x
        if refresh or force:
            self.origin = Point(x, y)
            self.rows, self.cols = rows, cols
            self.render()

    def realize_method(self, *args):
        apply(SketchDrawingArea.realize_method, (self,) + args)
        self.fill_rect = Rect(0, 0, *self.window.get_size())
        self.compute_trafo()

    def hit(self, x, y):
        x = x - self.origin.x
        y = y - self.origin.y
        scale = 1.0 / self.scale
        col =  x * scale
        row =  y * scale
        edge = 0 # 1 * scale
        if abs(row - round(row)) < edge: return None
        if abs(col - round(col)) < edge: return None
        index = int(row) * self.cols + int(col)
        if index >= len(self.items): return None
        return index

    def render(self):
        if not self.gcs_initialized:
            return
        win = self.gc.window
        scale = self.scale
        w, h = self.cols * scale, self.rows * scale
        self.pixsize = Point(w, h)
        self.pixmap = gtk.gdk.Pixmap(win, w, h, -1)
        self.gc.drawable = self.pixmap
        self.gc.SetFillColor(StandardColors.white)
        self.gc.FillRectangle(0, 0, w, h)
        self.render_items(w, h)
        self.gc.drawable = self.gc.window

    # override in derived class
    def render_items(self, width, height):
        self.gc.SetFillColor(StandardColors.white)
        self.gc.FillRectangle(0, 0, width, height)

    # override in derived class
    def Activate(self, index, state = 0):
        pass

    def SetItems(self, items):
        self.items = items
        self.layout(1)

    def get_motion_parameters(self):
        width, height = self.window.get_size()
        if self.direction == self.VERTICAL:
            direction = Point(0, 1)
            dimension = height
        else:
            direction = Point(1, 0)
            dimension = width
        return direction, dimension

    def Drag(self, delta):
        width, height = self.window.get_size()
        delta = int(delta)
        if self.direction == self.VERTICAL:
            limit = height - self.pixsize.y
            y = self.origin.y + delta
            y = min(0, max(y, limit))
            origin = Point(self.origin.x, y)
        else:
            limit = width - self.pixsize.x
            x = self.origin.x + delta
            x = min(0, max(x, limit))
            origin = Point(x, self.origin.y)
        if self.origin != origin:
            self.origin = origin
            self.RedrawMethod()

    def RedrawMethod(self, region = None):
        if self.pixmap is None:
            self.layout(1)
        win = self.window
        gc = self.gc
        gc.StartDblBuffer()
        gc.SetFillColor(StandardColors.white)
        gc.FillRectangle(0, 0, *win.get_size())
        x = int(self.origin.x)
        y = int(self.origin.y)
        w = int(self.pixsize.x)
        h = int(self.pixsize.y)
        gc.drawable.draw_drawable(gc.gc, self.pixmap, 0, 0, x, y, w, h)
        gc.EndDblBuffer()


class SwatchSample(PatternSample):

    def RedrawMethod(self, region = None):
        if self.window is not None:
            PatternSample.RedrawMethod(self, region)


class SwatchTip(gtk.Window):

    def __init__(self, parent):
        gtk.Window.__init__(self, gtk.WINDOW_POPUP)
        self._parent = parent
        self.enabled = 0
        self.timer_tag = None
        self.delay = 500 # default in milliseconds
        self.offset = Point(0, 10) # default
        self.pattern = None
        self.build_window()

    def build_window(self):
        self.set_resizable(0)
        self.label = gtk.Label('Tip!')
        self.label.set_padding(3,3)
        self.label.show()
        self.sample = SwatchSample()
        self.sample.SetPattern(EmptyPattern)
        self.sample.set_size_request(64, 64) # default (fixed)
        self.vbox = gtk.VBox()
        self.vbox.show()
        self.vbox.add(self.label)
        self.vbox.add(self.sample)
        frame = gtk.Frame()
        frame.show()
        frame.add(self.vbox)
        self.add(frame)

    def Pointer(self, point):
        self.hide()
        self.set_clock()
        x, y = self._parent.window.get_origin()
        self.point = point + Point(x, y)

    def SetTip(self, tip, pattern = None):
        self.hide()
        self.tip = tip
        self.label.set_text(tip)
        if pattern is None:
            self.sample.hide()
        else:
            self.sample.SetPattern(pattern)
            self.sample.show()
        self.pattern = pattern

    def Reset(self):
        self.hide()
        self.set_clock()

    def Enable(self):
        self.enabled = 1
        self.set_clock()

    def Disable(self):
        if self.enabled:
            self.enabled = 0
            self.hide()
            self.set_clock(1)

    def SetOffset(self, x, y):
        self.offset = Point(x, y)

    def SetDelay(self, ms):
        self.delay = ms

    def set_clock(self, off = 0):
        if self.timer_tag is not None:
            gtk.timeout_remove(self.timer_tag)
            self.timer_tag = None
        if self.enabled and not self.visible and not off:
            self.timer_tag = gtk.timeout_add(self.delay, self.popup)

    def hide(self):
        gtk.Window.hide(self)
        self.visible = 0

    # screen edge placement correction
    def correct(self, position, offset, size, screen_size):
        p = position + offset
        if p < 0:
            p = position - offset
        elif p + size > screen_size:
            if offset > 0:
                p = position - offset
            p = p - size
        return p

    # place window offset from pointer, minding screen edges
    def place(self):
        self.realize()
        x, y = self.point
        w, h =  tuple(self.get_allocation())[2:4]
        ox, oy = self.offset
        sw = gtk.gdk.screen_width()
        sh = gtk.gdk.screen_height()
        x = self.correct(x, ox, w, sw)
        y = self.correct(y, oy, h, sh)
        return int(x), int(y)

    def popup(self):
        self.visible = 1
        x, y = self.place()
        self.move(x, y)
        self.show()



class SwatchFrame(gtk.EventBox):

    MASK = gtk.gdk.BUTTON_PRESS_MASK    | \
           gtk.gdk.BUTTON_RELEASE_MASK  | \
           gtk.gdk.POINTER_MOTION_MASK  | \
           gtk.gdk.ENTER_NOTIFY_MASK    | \
           gtk.gdk.LEAVE_NOTIFY_MASK    | \
           gtk.gdk.KEY_PRESS_MASK       | \
           gtk.gdk.KEY_RELEASE_MASK     | \
           gtk.gdk.POINTER_MOTION_HINT_MASK

    def __init__(self):
        gtk.EventBox.__init__(self)
        self.swatch = None
        self.press = 0
        self.pointer = None
        self.sliding = 0
        self.idle_tag = None
        self.tip = SwatchTip(self)
        self._state = 0
        self.config_events()

    def config_events(self):
        self.set_events(self.MASK)
        self.connect('button_press_event', self.on_event)
        self.connect('button_release_event', self.on_event)
        self.connect('motion_notify_event', self.on_event)
        self.connect('enter_notify_event', self.on_event)
        self.connect('leave_notify_event', self.on_event)
        self.connect('focus_in_event', self.on_focus_in)
        self.connect('focus_out_event', self.on_focus_out)
        self.connect_after('key_press_event', self.key_press)
        self.connect_after('key_release_event', self.key_release)
        self.set_flags(gtk.CAN_FOCUS)

    def on_focus_in(self, *args):
        self.set_flags(gtk.HAS_FOCUS)

    def on_focus_out(self, *args):
        self.unset_flags(gtk.HAS_FOCUS)

    def add(self, widget):
        if not isinstance(widget, SwatchWidget):
            raise "SwatchFrame only accepts SwatchWidgets."
        gtk.EventBox.add(self, widget)
        self.swatch = widget

    def refresh_tip(self):
        self.tip.Pointer(self.pointer)
        hit = self.child.hit(self.pointer.x, self.pointer.y)
        if hit is None or self.sliding:
            self.tip.Disable()
        else:
            rgb, name = self.swatch.items[hit]
            if rgb is not None:
                tip = "(%.2f,%.2f,%.2f)\n%s" % (rgb.red, rgb.green, rgb.blue,
                                                name)
                pattern = SolidPattern(rgb)
            else:
                tip = name
                pattern = EmptyPattern
            self.tip.Pointer(self.pointer)
            self.tip.SetTip(tip, pattern)
            self.tip.Enable()

    #
    # palette sliding logic
    # similar to gv's "anti-dragging" behavior with the addition of
    # continuous scrolling once the mouse leaves the window
    # (much like when extending a selection in a word processor).
    # sounds a bit wierd but works pretty well...
    #

    def slide(self, point = None):
        now = time.clock()
        if point is None:
            point = self.pointer
            self.idle_tag = None
            dt = (now - self.idle_time) * 1000 # ms
        else:
            dt = 1.0
        if not self.sliding: return
        direction, dimension = self.child.get_motion_parameters()
        pos = (point * direction)
        # cursor is out of window, scroll continuously
        if pos < 0 or pos - dimension > 0:
            if pos < 0:
                speed = 0.1 + 0.02 * pos  # pixels per millisecond
            else:
                speed = 0.02 * (pos - dimension) - 0.1 # pixels per millisecond
            dist = speed * dt
            if abs(dist) > 0.5:
                self.child.Drag(-dist)
            idle = 1
        # cursor is in window, scroll opposite displacement
        else:
            # make jumping back into the window smoother
            if self.idle_tag is not None:
                gtk.idle_remove(self.idle_tag)
                self.idle_tag = None
                self.idle_time = None
            else:
                dist = (self.pointer - point) * direction * 3.0
                self.child.Drag(dist)
            idle = 0
        if idle and self.idle_tag is None:
            self.idle_tag = gtk.idle_add(self.slide)
            self.idle_time = now


    #
    # future hooks for handling multiple cursors and
    # tracking keyboard modifiers
    #

    def set_cursor(self, cursor):
        cursor = gtk.cursor_new(cursor)
        self.window.set_cursor(cursor)

    def update_cursor(self):
        pass

    def key_press(self, widget, event):
        mode = 0
        if   event.keyval == gtk.keysyms.Shift_L: mode = LEFT_SHIFT
        elif event.keyval == gtk.keysyms.Shift_R: mode = RIGHT_SHIFT
        elif event.keyval == gtk.keysyms.Control_L: mode = LEFT_CONTROL
        elif event.keyval == gtk.keysyms.Control_R: mode = RIGHT_CONTROL
        self._state = self._state | mode

    def key_release(self, widget, event):
        mode = 0
        if   event.keyval == gtk.keysyms.Shift_L: mode = LEFT_SHIFT
        elif event.keyval == gtk.keysyms.Shift_R: mode = RIGHT_SHIFT
        elif event.keyval == gtk.keysyms.Control_L: mode = LEFT_CONTROL
        elif event.keyval == gtk.keysyms.Control_R: mode = RIGHT_CONTROL
        self._state = self._state & ~mode

    #
    # handle all mouse related events
    #

    def on_event(self, widget, event):
        if event.type == gtk.gdk.BUTTON_PRESS:
            if self.press: return
            self.pointer = Point(event.x, event.y)
            self.tip.Disable()
            if event.button == 1:
                self.select = self.child.hit(event.x, event.y)
                self.press = 1
                self.press_point = self.pointer
        elif event.type == gtk.gdk.MOTION_NOTIFY:
            if event.is_hint:
                x, y = self.get_pointer()
            else:
                x, y = event.x, event.y
            current_point = Point(x, y)
            if self.pointer is None:
                self.pointer = current_point
            if self.press and not self.sliding and \
               abs(current_point - self.press_point) > 2:
                self.sliding = 1
                gtk.grab_add(self)
            if self.sliding:
                self.slide(current_point)
            self.pointer = current_point
            if not self.press: # and not self.sliding:
                self.refresh_tip()
        elif event.type == gtk.gdk.BUTTON_RELEASE:
            if event.button == 1:
                if self.press and not self.sliding and \
                   self.select is not None and \
                   self.select == self.child.hit(event.x, event.y) and \
                   self.child is not None:
                    self.child.Activate(self.select, self._state)
                self.press = 0
                if self.sliding:
                    self.sliding = 0
                    gtk.grab_remove(self)
        elif event.type == gtk.gdk._2BUTTON_PRESS:
            pass
        elif event.type == gtk.gdk.LEAVE_NOTIFY:
            self.tip.Disable()
            self.update_cursor()
        elif event.type == gtk.gdk.ENTER_NOTIFY:
            self.grab_focus()
            self.update_cursor()
        return 0



# SolidPaletteWidget
#
# realization of SwatchWidget class for solid patterns

class SolidPaletteWidget(SwatchWidget):

    def __init__(self):
        SwatchWidget.__init__(self, direction = self.HORIZONTAL)
        self.context = None
        self.is_fill = 1

    def render_items(self, width, height):
        self.gc.SetFillColor(StandardColors.white)
        self.gc.FillRectangle(0, 0, width, height)
        x = y = 0
        col = 0
        scale = self.scale
        for color, name in self.items:
            if color is not None:
                self.gc.SetFillColor(color)
                self.gc.FillRectangle(x, y, x + scale - 1, y + scale - 1)
            else:
                self.gc.SetFillColor(white)
                self.gc.FillRectangle(x, y, x + scale - 1, y + scale - 1)
                self.gc.SetLineColor(black)
                self.gc.DrawLineXY(x, y, x + scale - 1, y + scale - 1)
            x = x + scale
            if x >= width:
                x = 0
                y = y + scale

    def SetPalette(self, palette):
        self.SetItems(palette.items)

    def SetIsFill(self, is_fill):
        self.is_fill = is_fill
        return is_fill

    def SetContext(self, context):
        self.context = context

    def Activate(self, index, state = 0):
        color = self.items[index][0]
        if color is None:
            pattern = EmptyPattern
        else:
            pattern = SolidPattern(color)
        try:
            editor = self.context.editor
        except:
            return
        if editor is not None:
            is_fill = self.is_fill
            if state & SHIFT:
                is_fill = not is_fill
            if is_fill:
                editor.SetProperties(fill_pattern = pattern)
            else:
                editor.SetProperties(line_pattern = pattern)


# PaletteFrame
#
# abstract base class for palette frames.  frames
# derived from this class can be inserted into the
# notebook in the palette panel.

class PaletteFrame(gtk.EventBox):

    def __init__(self, palette):
        gtk.EventBox.__init__(self)
        self.palette = palette
        self.palette.Subscribe(CHANGED, self.changed)
        self.context = None

    def __del__(self):
        self.palette.Unsubscribe(CHANGED, self.changed)

    def changed(self):
        pass

    def SetIsFill(self, is_fill):
        return is_fill

    def SetContext(self, context):
        self.context = context


class SolidPaletteFrame(PaletteFrame):

    def __init__(self, palette):
        PaletteFrame.__init__(self, palette)
        self.pal = SolidPaletteWidget()
        self.pal.SetPalette(palette)
        frame = SwatchFrame()
        frame.add(self.pal)
        self.add(frame)
        self.show_all()

    def SetIsFill(self, is_fill):
        return self.pal.SetIsFill(is_fill)

    def SetContext(self, context):
        PaletteFrame.SetContext(self, context)
        self.pal.SetContext(context)

    def changed(self):
        #self.pal.SetPalette(self.palette)
        pass


class ArrowPaletteFrame(PaletteFrame):

    def __init__(self, palette):
        PaletteFrame.__init__(self, palette)
        self.list = list = gtk.CList(3, ('Arrow', 'Width', 'Height'))
        self.scroll = scroll = gtk.ScrolledWindow()
        scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)
        scroll.add_with_viewport(list)
        self.add(self.scroll)
        self.show_all()
        self.connect('realize', self.on_realize)

    def on_realize(self, *args):
        self.populate_list()

    def populate_list(self):
        row = 0
        size = 16
        pixmaps = self.palette.GeneratePixmaps(self.window, size)
        list = self.list
        list.set_row_height(size)
        for arrow in self.palette.items:
            bbox = self.palette.bboxes[row]
            list.append(['','',''])
            list.set_pixmap(row, 0, pixmaps[row])
            list.set_text(row, 1, "%0.2f" % abs(bbox.right - bbox.left))
            list.set_text(row, 2, "%0.2f" % abs(bbox.top - bbox.bottom))
            row = row + 1


# PalettePanel
#
# floating dialog to display palettes

class PalettePanel(SketchPanel):

    multiple = 1

    def __init__(self, context):
        self.is_fill = getattr(Sketch.UI.preferences,
                               'default_palette_is_fill', 1)
        SketchPanel.__init__(self, context, _("Palettes"))
        self.set_name("palette_panel")
        w, h = getattr(Sketch.UI.preferences,
                       'default_palette_size', (68,134))
        self.set_default_size(w, h)

    def build_window(self):
        self.lookup = {}
        self.notebook = gtk.Notebook()
        self.vbox.pack_start(self.notebook)
        self.notebook.set_show_border(0)
        self.notebook.set_show_tabs(0)
        self.build_palettes()
        self.vbox.set_border_width(0)
        self.vbox.show_all()
        self.action_area.hide()

    def build_palettes(self):
        global palettes
        default_palette = getattr(Sketch.UI.preferences,
                                  'default_palette', None)
        default_n = 0
        menu = gtk.Menu()
        item = gtk.TearoffMenuItem()
        menu.append(item)
        item = gtk.RadioMenuItem(label = "Fill")
        item.connect('activate', self.on_set_fill)
        menu.append(item)
        item = gtk.RadioMenuItem(group = item, label = "Line")
        item.connect('activate', self.on_set_line)
        menu.append(item)
        item = gtk.MenuItem()
        menu.append(item)
        n = 0
        self.pages = []
        for cat in palettes.keys():
            sub = gtk.Menu()
            item = gtk.MenuItem(cat)
            item.set_submenu(sub)
            menu.append(item)
            palettes[cat].sort(lambda x, y: cmp(x.Name(), y.Name()))
            for palette in palettes[cat]:
                name = palette.Name()
                item = gtk.MenuItem(name)
                item.connect("activate", self.choose_palette)
                sub.append(item)
                frame = palette.NewFrame(self.context)
                item.set_data('frame',frame)
                self.notebook.append_page(frame, gtk.Label(name))
                palette.Subscribe(CHANGED, self.palette_changed, frame)
                title = '%s - %s' % (cat, name)
                self.pages.append((frame, title, name, cat))
                self.lookup[frame] = (title, name)
                if title == default_palette:
                    default_n = n
                n = n + 1
        if n > 0:
            self.notebook.set_current_page(default_n)
            frame, title = self.pages[default_n][0:2]
            self.is_fill = frame.SetIsFill(self.is_fill)
            self.set_title(title)
        self.menu = menu
        self.add_menu_ops()
        menu.show_all()
        self.connect('button_press_event', self.button_press)

    def set_fill_or_line(self, fill):
        frame = self.get_current_frame()
        self.is_fill = frame.SetIsFill(fill)
        self.set_title()

    def on_set_fill(self, *args):
        self.set_fill_or_line(fill = 1)

    def on_set_line(self, *args):
        self.set_fill_or_line(fill = 0)

    def palette_changed(self, *args):
        pass

    def context_changed(self, *args):
        pass
#       for frame in self.titles.keys():
#           frame.SetContext(context)

    def set_title(self, title = None):
        if title is None:
            title = self._title
        self._title = title
        if self.is_fill:
            title = "FILL - " + title
        else:
            title = "LINE - " + title
        SketchPanel.set_title(self, title)

    def choose_palette(self, widget):
        frame = widget.get_data('frame')
        n = self.notebook.page_num(frame)
        title = self.lookup[frame][0]
        self.set_title(title)
        self.notebook.set_page(n)

    def button_press(self, widget, event):
        if event.button == 3:
            self.popup_context_menu(event)

    def popup_context_menu(self, event):
        self.menu.popup(None, None, None, event.button, event.time)

    def add_menu_ops(self):
        menu = gtk.Menu()
        ops = ( None,
                (1, _("Set as default"), self.set_as_default),
                (0, _("New palette"), self.new_palette),
                (0, _("Delete palette"), self.delete_palette),
                (0, _("Rename palette"), self.rename_palette),
              )
        for op in ops:
            if op is None:
                item = gtk.MenuItem()
            else:
                enable, op_name, op_cb = op
                item = gtk.MenuItem(op_name)
                item.set_sensitive(enable)
                item.connect('activate',op_cb)
            menu.append(item)
        item = gtk.MenuItem('Palette Ops')
        item.set_submenu(menu)
        self.menu.append(item)

    def get_current_frame(self):
        n = self.notebook.get_current_page()
        return self.pages[n][0]

    def set_as_default(self, widget):
        frame = self.get_current_frame()
        title = self.lookup[frame][0]
        Sketch.UI.preferences.default_palette = title
        x, y, w, h = self.get_allocation()
        Sketch.UI.preferences.default_palette_size = (w, h)
        Sketch.UI.preferences.default_palette_is_fill = self.is_fill

    def new_palette(self, widget):
        pass

    def delete_palette(self, widget):
        pass

    def rename_palette(self, widget):
        pass





##############################################################

# palette categories (label, extension, class)

SOLID = (_('Solid Colors'), '.spl', SolidPalette)
# GRADIENT = (_('Gradient Fills'), '.gradients')
# HATCH = (_('Hatch Fills'), '.hatches')
# IMAGE = (_('Image Fills'), '.images')
# DASHES = (_('Lines'), '.dashes')
# ARROW = (_('Arrows'), '.arrow', ArrowPalette)
#categories = (SOLID , ARROW)

categories = (SOLID, )
palettes = {}

def load_palettes():
    global palettes
    palettes = {}
    resdir = glob(os.path.join(config.std_res_dir,'*'))
    for name, ext, obj in categories:
        plist = []
        resources = filter(lambda x, ext = ext: x.endswith(ext), resdir)
        if len(resources) == 0: continue
        for filename in resources:
            pname = os.path.basename(filename)
            pname = os.path.splitext(pname)[0]
            palette = obj(pname)
            if palette.load(filename):
                plist.append(palette)
        palettes[name] = plist

load_palettes()
