# Sketch - A Python-based interactive drawing program
# Copyright (C) 2000, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import gtk, gobject
from gtk import FILL, EXPAND

import Sketch
from Sketch import _

from skpanel import SketchPanel

COL_KEY = 0
COL_VALUE = 1


class MetaDataPanel(SketchPanel):

    """Panel to let the user edit the meta data of the current object.

    Skencil objects may have arbitrary associated meta data (see
    ObjectWithMetaData in Skencil.Graphics.base).  This panel shows a
    list view of the data associated with the currently selected object
    and lets the user edit the data.

    The meta data is a bunch of key/value pairs where the values are (to
    a limited extent) arbitrary python objects.  Values that are not
    strings have to have a repr that can be passed to eval to yield an
    equal object as before.  This feature is used to let the user edit
    non-string objects in that they get the repr of the object to edit.
    """

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("MetaData"))
        self.context.Subscribe(Sketch.Base.const.SELECTION,
                               self.context_changed)
        self.context.Subscribe(Sketch.Base.const.TRANSACTION_END,
                               self.context_changed)

    def build_window(self):
        vbox = self.vbox

        # scrolled window for the list of key/value pairs
        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw)

        # create model and view for the list of key/value pairs
        self.model = model = gtk.ListStore(gobject.TYPE_STRING,
                                          gobject.TYPE_PYOBJECT)
        treeview = self.treeview = gtk.TreeView(model)
        treeview.set_rules_hint(True)
        treeview.set_search_column(COL_KEY)
        sw.add(treeview)

        # tree view columns
        column = gtk.TreeViewColumn(_("Key"), gtk.CellRendererText(),
                                    text=COL_KEY)
        column.set_sort_column_id(COL_KEY)
        treeview.append_column(column)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_("Value"), renderer)
        column.set_sort_column_id(COL_KEY)
        column.set_cell_data_func(renderer, self.set_value_data)
        treeview.append_column(column)

        # selection settings
        selection = treeview.get_selection()
        selection.set_mode(gtk.SELECTION_SINGLE)
        selection.connect("changed", self.selection_changed)


        # entry widgets to edit keys and values
        table = gtk.Table(2, 3)
        vbox.pack_start(table, expand = 0, fill = 0)

        label = gtk.Label(_("Key"))
        table.attach(label, 0, 1, 0, 1, 0, 0)
        self.key_entry = gtk.Entry()
        table.attach(self.key_entry, 1, 2, 0, 1, EXPAND|FILL, 0)

        label = gtk.Label(_("Value"))
        table.attach(label, 0, 1, 1, 2, 0, 0)
        self.value_entry = gtk.Entry()
        table.attach(self.value_entry, 1, 2, 1, 2, EXPAND|FILL, 0)

        self.eval_check = gtk.CheckButton(_("Value is Python Expression"))
        table.attach(self.eval_check,  0, 2, 2, 3, EXPAND|FILL, 0)

        hbox = gtk.HBox(spacing = 5)
        hbox.set_border_width(5)
        self.vbox.pack_start(hbox, expand = 0, fill = 0)

        # add/set, delete buttons
        button = gtk.Button(_("Add/Set"))
        button.connect("clicked", self.add_data)
        hbox.pack_start(button)
        button = self.delete_button = gtk.Button(_("Delete"))
        button.connect("clicked", self.delete_data)
        # The delete button starts out as insensitive.  It will become
        # sensitive as soon as a row in the list is selected
        button.set_sensitive(False)
        hbox.pack_start(button)

        #
        button = gtk.Button(_("Close"))
        button.connect("clicked", self.close)
        self.action_area.pack_start(button, expand = 0, fill = 0)

        vbox.show_all()

    def close(self, *args):
        """Unsubscribe all subscriptions and close the panel"""
        self.context.Unsubscribe(Sketch.Base.const.SELECTION,
                                 self.context_changed)
        self.context.Unsubscribe(Sketch.const.TRANSACTION_END,
                                 self.context_changed)
        SketchPanel.close(self)

    def context_changed(self, *args):
        """Update the list store from the currently selected object

        If no objectis selected the list will be emptied.

        This method is subscribed to various messages that indicate that
        the meta data may have changed.
        """
        items = ()
        if self.context.HasEditor():
            obj = self.context.editor.CurrentObject()
            if obj is not None:
                items = obj.DataItems()
                items.sort()

        # fill the model
        self.model.clear()
        for key, value in items:
            itr = self.model.append()
            self.model.set(itr, COL_KEY, key)
            self.model.set(itr, COL_VALUE, value)

    def add_data(self, *args):
        """Add the key/value pair from the entry widgets.

        If the 'Eval' button is checked, eval the string taken from the
        value entry and use the resulting value as the value of the meta
        data, otherwise the string is used as is.

        This method is a handler for the 'clicked' signal of the
        'Add/Set' button.
        """
        if self.context.HasEditor():
            obj = self.context.editor.CurrentObject()
            if obj is not None:
                key = self.key_entry.get_text().strip()
                if not key:
                    return
                value = self.value_entry.get_text()
                if self.eval_check.get_active():
                    value = eval(value)
                editor = self.context.editor
                editor.CallObjectMethod(Sketch.Graphics.ObjectWithMetaData,
                                        "SetData", (key, value),
                                        title = _("Set Data '%s'") % key)

    def delete_data(self, *args):
        """Delete the currently selected key/value pair.

        This method deletes the currently selected key and associated
        value from the meta data of the currently selected object.

        This method is a handler for the 'clicked' signal of the
        'Delete' button.
        """
        if self.context.HasEditor():
            obj = self.context.editor.CurrentObject()
            if obj is not None:
                selection = self.treeview.get_selection()
                model, itr = selection.get_selected()
                if itr is not None:
                    key = model.get(itr, COL_KEY)[0]
                    editor = self.context.editor
                    editor.CallObjectMethod(Sketch.Graphics.ObjectWithMetaData,
                                            "DelData", (key,),
                                            title = _("Delete Data '%s'") % key)

    def selection_changed(self, selection):
        """Fill the entry widgets with the currently select key and value.

        This method updates the entry widets for the key and value from
        the line of the tree view indicated by the parameter selection.
        If the value object is not a string, the 'Eval' check button
        will be checked and the string put into the value entry widget
        will be a string representation of the actual value as
        implemented by self.value_as_string.

        Also, gray out the delete button if no pair is selected because
        the button always deletes the selected pair, regardless of
        what's in the entry widgets.

        This method is a handler for the 'changed' signal of the tree
        view.
        """
        model, itr = selection.get_selected()
        if itr is not None:
            key, value = model.get(itr, COL_KEY, COL_VALUE)
            self.key_entry.set_text(key)
            was_converted, value = self.value_as_string(value)
            self.value_entry.set_text(value)
            self.eval_check.set_active(was_converted)
            self.delete_button.set_sensitive(True)
        else:
            self.key_entry.set_text("")
            self.value_entry.set_text("")
            self.eval_check.set_active(0)
            self.delete_button.set_sensitive(False)

    def value_as_string(self, value):
        """Return the string representation of value

        Value should be the value of a meta data item.  If the value is
        already a string, it is returned as is.  otherwise its repr is
        returned.  The return value is a a tuple (was_converted,
        string_value) where was_converted is True if string_value is the
        repr of value and False if it is the same as value.
        """
        if isinstance(value, basestring):
            return False, value
        return (True, repr(value))

    def set_value_data(self, column, cell, model, itr):
        """cell data func for the value column

        Set the text attribute of cell to the value of COL_VALUE of the
        model at itr.  The value is converted to a string with
        self.value_as_string.
        """
        value = model.get(itr, COL_VALUE)[0]
        was_converted, value = self.value_as_string(value)
        cell.set_property("text", value)
