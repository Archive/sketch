# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#
#	Pixmap/Bitmap handling stuff
#

import os

import gtk

from Sketch import _skgtk
from Sketch.Base import config

from Sketch.Lib.readxbm import read_xbm

def load_bitmap(window, filename):
    image = read_xbm(filename)
    # The bitorder in PIL is the reverse of the bitorder in XBMs so the
    # bits are swapped when we use tostring without arguments. Provide
    # the "1;R" rawmode to reverse the bits
    data = image.tostring("raw", "1;R")
    width, height = image.size
    if image.info.has_key("hot_spot"):
        x_hot, y_hot = image.info["hot_spot"]
    else:
        x_hot = y_hot = None
    bitmap = gtk.gdk.bitmap_create_from_data(window, data, width, height)
    return bitmap, x_hot, y_hot

_bitmap_names = ['TurnTL', 'TurnTR', 'TurnBL', 'TurnBR', 'Center',
                 'ShearLR', 'ShearUD',
                 'AlignTL', 'AlignTC', 'AlignT', 'AlignTR', 'AlignL',
                 'AlignLC', 'AlignR', 'AlignBL', 'AlignB', 'AlignBR',
                 'AlignC']

class SketchBitmaps:

    def InitFromWidget(self, widget):
        window = widget.window
        dir = config.pixmap_dir
        for name in _bitmap_names:
            file = os.path.join(dir, name) + '.xbm'
            setattr(self, name, load_bitmap(window, file)[0])

bitmaps = SketchBitmaps()

_pixmap_names = ['SelectionTool', 'EditTool', 'ZoomTool', 'TextTool',
                 'CreateRect', 'CreateCurve', 'CreateEllipse', 'CreatePoly',
                 'CreateFreehand',
                 'tree_layer', 'tree_object', 'tree_ellipse', 'tree_group',
                 'tree_rectangle',
                 'LightBulbOn', 'LightBulbOff', 

                 'JoinMiter', 'JoinRound', 'JoinBevel',
                 'CapButt', 'CapRound', 'CapProjecting',

                 'fill_gradient', 'fill_hatch', 'fill_none',
                 'fill_solid', 'fill_tile',

                 'MiniFill', 'MiniTransform', 'MiniLine', 'MiniWidth',
                 'MiniCap', 'MiniJoin', 'MiniDashes',
                 'MiniArrowRight', 'MiniArrowLeft',
                 'MiniFont', 'MiniFontSize']

class SketchPixmaps:

    def InitFromWidget(self, widget):
        dir = config.pixmap_dir
        for name in _pixmap_names:
            file = os.path.join(dir, name) + '.xpm'
            setattr(self, name,
                    gtk.gdk.pixmap_create_from_xpm(widget.window, None, file))

pixmaps = SketchPixmaps()

_load_pixbuf_cache = {}
def load_pixbuf(name):
    """Load one of Skencil's xpm or xbm files as a pixbuf

    The pixbufs are cached so when this function is called with the same
    name multiple times the same pixbuf is returned.
    """
    pixbuf = _load_pixbuf_cache.get(name)
    if pixbuf is None:
        if name in _pixmap_names:
            filename = os.path.join(config.pixmap_dir, name) + '.xpm'
        elif name in _bitmap_names:
            filename = os.path.join(config.pixmap_dir, name) + '.xbm'
        else:
            raise ValueError("Unknown xpm/xbm %r" % name)
        pixbuf = gtk.gdk.pixbuf_new_from_file(filename)
        _load_pixbuf_cache[name] = pixbuf
    return pixbuf


_cursors = ['CurEdit', 'CurZoom', 'CurText', 'CurTextEdit', 'CurTextPath']

def init_cursors(widget):
    import cursors
    window = widget.window
    cmap = widget.get_colormap()
    fg = cmap.alloc_color('black')
    bg = cmap.alloc_color('white')
    dir = config.pixmap_dir
    for name in _cursors:
        base = os.path.join(dir, name)
        bitmap, x_hot, y_hot = load_bitmap(window, base + '.xbm')
        mask = load_bitmap(window, base + '_mask.xbm')[0]
        cursor = gtk.gdk.Cursor(bitmap, mask, fg, bg, x_hot, y_hot)
        setattr(cursors, name, cursor)
        
    

def init_from_widget(widget):
    bitmaps.InitFromWidget(widget)
    pixmaps.InitFromWidget(widget)
    init_cursors(widget)
