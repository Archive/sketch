# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2002, 2003, 2004 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import sys

import Sketch


class UIPreferences(Sketch.Base.Preferences):

    _filename = 'ui.prefs'

    #
    #	Units
    #
    #	The default unit used in various places.
    #
    #	Supported values: 'pt', 'in', 'cm', 'mm'
    #
    default_unit = 'pt'

    #   If true, setting the unit in the position indicator in the
    #   statusbar also sets the default unit
    poslabel_sets_default_unit = 1

    #	If the text on the screen becomes smaller than greek_threshold,
    #	don't render a font, but draw little lines instead.
    # XXX see comments in gtkdevice.py
    greek_threshold = 5

    #
    #	Maximum Snap Distance
    #
    #	When snapping is active, coordinates specified with the mouse
    #	are snapped to the nearest `special' point (e.g. a grid point)
    #	if that is nearer than max_snap_distance pixels. (Thus, this
    #	length is given in window (pixel-) coordinates).
    #
    max_snap_distance = 10

    #
    #	Snap Current Position
    #
    #	If true and snapping is active, the current position displayed
    #	in the status bar is the position the mouse position would be
    #	snapped to.
    snap_current_pos = 1

    #
    #	List of most recently used files.
    #
    mru_files = ()
    mru_files_length = 10

    #
    #
    viewport_ring_length = 10

    #	Font dialog sample text. Can be changed by simply editing it in
    #	the font dialog.
    sample_text = 'ABCD abcd'

    #	Screen resolution in pixel per point. Used by the canvas to
    #	convert document coordinates to screen coordinates for a zoom
    #	factor of 100%
    #
    #	None means to compute it from information obtained from the
    #	X-Server (ScreenWidth and ScreenMMWidth). 1.0 means 72 pixels
    #	per inch.
    screen_resolution = 1.0

    #
    #	Document Windows
    #
    #	Window Title template. This template is used to form the window
    #	title via the %-operator and a dictionary containing the keys:
    #
    #	    'docname'	The name of the document (usually the filename)
    #	    'appname'	The name of the application (from Sketch.config)
    window_title_template = '%(docname)s - %(appname)s'

    #
    #   The initial window size
    #
    #   A tuple of ints (width, height)
    #
    window_size = (600, 650)

    #
    #	Panels
    #

    #	The panels save their screen position in the preferences file.
    #	These variables control whether this information is used and in
    #	what way.

    #	If true, use the saved coordinates when opening a panel
    panel_use_coordinates = 1

    #	If true, try to compensate for the coordinate changes the window
    #	manager introduces by reparenting.
    panel_correct_wm = 1

    #
    #	Blend Panel
    #
    blend_panel_default_steps = 10

    #
    #	Print Dialog
    #

    #	Default print destination. 'file' for file, 'printer' for printer
    print_destination = 'file'

    #	default directory for printing to file
    print_directory = ''


    #   Object Panel

    #   Predefined line widths. Maintained by the ObjectPanel
    user_line_widths = [0.2, 0.5, 1.0, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0]

    #   Maximum length of user_line_widths. Enforced by the ObjectPanel
    user_line_widths_length = 10

    #	Color
    #
    #	For PseudoColor displays:
    color_cube = (5, 5, 5, 5)

    reduce_color_flashing = 1

    #
    #   Misc
    #

    #   The line width for the outlines during a drag. On some servers
    #   dashed lines with a width of 0 are drawn solid. Set
    #   editor_line_width to 1 in those cases.
    editor_line_width = 0


preferences = UIPreferences()


#
#	Global Variables
#

application = None

#
#
#

def init(argv, load_user_preferences = 1):
    Sketch.Editor.init(argv, load_user_preferences = load_user_preferences)
    preferences._load()


_usage = ""'''\
Usage:	skencil [Options] [filename]

skencil accepts these options:

  -h --help		Print this help message
  -d --display=DISPLAY	Use DISPLAY a X Display
  --version		Print the version number to stdout
'''

_version = '''\
Skencil %s
Copyright (C) 1998 Bernhard Herzog
Skencil comes with ABSOLUTELY NO WARRANTY.
You may redistribute copies of Skencil
under the terms of the GNU Library General Public License.
For more information about these matters, see the files named COPYING.'''


def _process_args(argv):
    import sys, getopt
    from Sketch.Lib import util

    opts, argv = getopt.getopt(argv, 'hi', ['display=', 'help', 'version',
                                            "sync"])
    # the option -i is a hack to allow sketch to be used as a `python
    # interpreter' in the python shell in python-mode.el

    options = util.Empty()
    gtkargs = []

    for optchar, value in opts:
        if optchar == '--display':
            gtkargs.append(optchar)
            gtkargs.append(value)
        elif optchar == '--sync':
            gtkargs.append(optchar)
            gtkargs.append(value)
        elif optchar == '-h' or optchar == '--help':
            print _usage
            sys.exit(0)
        elif optchar == '--version':
            print _version % Sketch.SketchVersion
            sys.exit(0)

    sys.argv[1:] = gtkargs
    return options, argv


def main(argv):
    global application

    options, filenames = _process_args(argv[1:])

    from skapp import SketchApplication

    init(argv)

    #import gtk
    #gtk.push_rgb_visual()

    application = SketchApplication(filenames)
    #Sketch.Issue(Sketch.const.APP_INITIALIZED, application)
    application.Run()
    application.SavePreferences()
