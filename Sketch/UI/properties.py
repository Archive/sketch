# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from types import StringType

import gtk

import Sketch
from Sketch import _, Rect, Trafo
from Sketch.Graphics import StandardColors, EmptyLineStyle, PropertyStack,\
     EmptyPattern
import Sketch, Sketch.Graphics

from gtkmisc import SketchDrawingArea
from skpanel import SketchPanel
from gtkdevice import PatternDevice
import skpixmaps
pixmaps = skpixmaps.pixmaps


class PatternSample(SketchDrawingArea):

    def __init__(self,  **kw):
        apply(SketchDrawingArea.__init__, (self,), kw)
        self.pattern = EmptyPattern
        self.properties = PropertyStack()
        self.properties.AddStyle(EmptyLineStyle)
        self.properties.SetProperty(fill_pattern = self.pattern)
        self.gc.SetProperties(self.properties)
        self.fill_rect = None

    def realize_method(self, *args):
        apply(SketchDrawingArea.realize_method, (self,) + args)
        self.fill_rect = Rect(0, 0, self.window.width, self.window.height)
        self.compute_trafo()

    def compute_trafo(self):
        if not hasattr(self, 'window'):
            return
        height = self.window.height
        doc_to_win = Trafo(1, 0, 0, -1, 0, height)
        win_to_doc = doc_to_win.inverse()
        self.gc.SetViewportTransform(1.0, doc_to_win, win_to_doc)
        self.fill_rect = Rect(0, 0, self.window.width, self.window.height)
        self.gc.SetProperties(self.properties, self.fill_rect)

    def SetPattern(self, pattern):
        if pattern != self.pattern:
            self.pattern = pattern
            self.UpdateWhenIdle()
            self.properties.SetProperty(fill_pattern = pattern)
            self.gc.SetProperties(self.properties, self.fill_rect)

    def RedrawMethod(self, region = None):
        win = self.window
        #self.gc.StartDblBuffer()
        self.gc.SetFillColor(StandardColors.white)
        self.gc.FillRectangle(0, 0, win.width, win.height)
        if self.properties.HasFill():
            self.gc.Rectangle(Trafo(win.width, 0, 0, win.height, 0, 0))
        else:
            self.gc.SetLineColor(StandardColors.black)
            self.gc.DrawLineXY(0, win.height, win.width, 0)
        #self.gc.EndDblBuffer()

    def ResizedMethod(self, width, height):
        self.gc.WindowResized(width, height)
        self.UpdateWhenIdle()
        self.compute_trafo()


class PreviewFactory(PatternDevice):

    def __init__(self):
        PatternDevice.__init__(self, gradient_steps =
                               Sketch.Base.preferences.gradient_steps_editor)

    def CreatePreview(self, pattern, width = 16, height = 16):
        if pattern.preview is not None:
            return pattern.preview
        pixmap = gtk.create_pixmap(self.window, width, height)
        self.drawable = pixmap
        doc_to_win = Trafo(1, 0, 0, -1, 0, height)
        win_to_doc = doc_to_win.inverse()
        self.SetViewportTransform(1.0, doc_to_win, win_to_doc)
        self.ximage = None
        if pattern.is_procedural:
            self.fill_rect = Rect(0, 0, width, height)
            pattern.Execute(self, self.fill_rect)
        else:
            pattern.Execute(self, None)
            self.FillRectangle(0, 0, width, height)
        self.drawable = self.window
        pattern.preview = pixmap
        return pixmap
        


property_defs = [
    ('fill_pattern', 'MiniFill', "convert_pattern"),
    ('fill_transform', 'MiniTransform', "convert_boolean"),
    ('line_pattern', 'MiniLine', "convert_pattern"),
    ('line_width', 'MiniWidth', "convert_length"),
    ('line_cap', 'MiniCap', "convert_cap"),
    ('line_join', 'MiniJoin', "convert_join"),
    ('line_dashes', 'MiniDashes', "convert_dummy"),
    ('line_arrow1', 'MiniArrowLeft', "convert_arrow"),
    ('line_arrow2', 'MiniArrowRight', "convert_arrow"),
    ('font', 'MiniFont', "convert_font"),
    ('font_size', 'MiniFontSize', "convert_length"),
]

maskbitmap = None

class StyleList(gtk.CList):

    def __init__(self, preview_factory):
        self.preview_factory = preview_factory
        titles = ['Name']
        for name, pixmap, converter in property_defs:
            titles.append(getattr(pixmaps, pixmap))

        gtk.CList.__init__(self, len(titles))
        for index, title in enumerate(titles):
            if isinstance(title, StringType):
                self.set_column_title(index, title)
            else:
                pixmap, mask = title
                image = gtk.Image()
                image.set_from_pixmap(pixmap, mask)
                self.set_column_widget(index, image)
        self.column_titles_show()

    def SetStyleList(self, styles):
        self.freeze()
        try:
            self.clear()
            for row in range(len(styles)):
                style = styles[row]
                self.append([''] * (len(property_defs) + 1))
                if style.Name():
                    text = style.Name()
                else:
                    text = '<unnamed>'
                self.set_text(row, 0, text)
                for col in range(len(property_defs)):
                    name, pixmap, converter = property_defs[col]
                    if hasattr(style, name):
                        value = getattr(self, converter)(getattr(style, name))
                    else:
                        value = ''
                    if not isinstance(value, StringType):
                        pixmap, mask = value
                        self.set_pixmap(row, col+1, pixmap, mask)
                    else:
                        self.set_text(row, col + 1, value)
            self.set_column_width(0, self.optimal_column_width(0))
        finally:
            self.thaw()

    def convert_dummy(self, value):
        return '*'

    def convert_length(self, value):
        return `value`

    def convert_arrow(self, value):
        if value is not None:
            return '+'
        else:
            return '-'

    def convert_boolean(self, value):
        return value and '+' or '-'

    def convert_pattern(self, value):
        if value is not EmptyPattern:
            return self.preview_factory.CreatePreview(value), maskbitmap
        else:
            return '-'

    def convert_font(self, value):
        if value is not None:
            return value.PostScriptName()
        return '-'

    def convert_join(self, value):
        return getattr(pixmaps, ('JoinMiter', 'JoinRound', 'JoinBevel')[value])

    def convert_cap(self, value):
        return getattr(pixmaps,
                       ('', 'CapButt', 'CapRound', 'CapProjecting')[value])

class PropertyPanel(SketchPanel):

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("Properties"))
        self.context.Subscribe(Sketch.Base.const.SELECTION,
                               self.context_changed)
        self.context.Subscribe(Sketch.Base.const.TRANSACTION_END,
                               self.context_changed)
        self.context.Subscribe(Sketch.Base.const.STYLE, self.style_changed)
        self.style_changed()
        self.context_changed()

    def build_window(self):
        vbox = self.vbox
        self.preview_factory = PreviewFactory()

        #self.fillsample = PatternSample()
        #self.fillsample.size(100, 100)
        #self.fillsample.show()
        #vbox.pack_start(self.fillsample)

        notebook = gtk.Notebook()
        vbox.pack_start(notebook)

        swin = gtk.ScrolledWindow()
        swin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)        
        self.current_styles = StyleList(self.preview_factory)
        swin.add(self.current_styles)
        notebook.append_page(swin, gtk.Label(_("Current Object")))

        swin = gtk.ScrolledWindow()
        swin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)        
        self.dynamic_styles = StyleList(self.preview_factory)
        swin.add(self.dynamic_styles)
        notebook.append_page(swin, gtk.Label(_("Styles")))
        
        vbox.show_all()

        vbox.realize()
        self.preview_factory.init_gc(vbox.window)
        global maskbitmap
        maskbitmap = gtk.gdk.bitmap_create_from_data(vbox.window, '\377'*32,
                                                     16, 16)

    def close(self, *args):
        self.context.Unsubscribe(Sketch.Base.const.SELECTION,
                                 self.context_changed)
        self.context.Unsubscribe(Sketch.Base.const.STYLE, self.style_changed)
        self.context.Unsubscribe(Sketch.const.TRANSACTION_END,
                                 self.context_changed)
        SketchPanel.close(self)

    def context_changed(self, *args):
        #print 'context_changed'
        pattern = EmptyPattern
        properties = None
        if self.context.HasEditor():
            obj = self.context.editor.CurrentObject()
            if obj is not None and obj.has_properties:
                properties = obj.Properties()
                if obj.has_fill:
                    pattern = properties.fill_pattern.Copy()

        #self.fillsample.SetPattern(pattern)
        if properties is not None:
            self.current_styles.SetStyleList(properties.Styles())
        else:
            self.current_styles.SetStyleList([])
        self.style_changed()

    def style_changed(self, *args):
        if self.context.HasEditor():
            self.dynamic_styles.SetStyleList(self.context.document.Styles())
        else:
            self.dynamic_styles.SetStyleList([])
