# Skencil - A Python-based interactive drawing program
# Copyright (C) 2004, 2005 by Valentin Ungureanu
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


"""The History Panel

The history panel is a non-modal dialog showing the editing history of
the current document as a list.  The list contains information about
each undo or redo step available in chronological order with the oldest
changes at the top.  The user can double click on a change to undo or
redo until the state of that step is reached again.
"""

# FIXME: Use real model-view-controller.  The code currently makes too
# many assumptions about how undo works.  E.g. in the row_activated
# method it "knows" which rows of the list view will be affected in
# which ways by the undo or redo.  It would be better to always consult
# with the real undo information in the document.  The interface of the
# undo object may have to be extended for that.
#
# The code also makes assumptions about the internal representation of
# undo information, e.g. the order of items in the lists managed by the
# UndoRedo class and the structure of individual undo items.  This
# should be fixed, too, and may likewise need interface extensions.

import gtk
import gobject

import Sketch
from Sketch import _
from Sketch.Base.const import UNDO
from Sketch.UI.skpanel import SketchPanel

COLUMN_OPERATION   = 0
COLUMN_DESCRIPTION = 1
COLUMN_PIXBUF      = 2

class HistoryPanel(SketchPanel):

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("History"))
        self.set_name('history_panel')
        self.context.Subscribe(UNDO, self.undo_redo)
        self.undo_from_dialog = 0

    def build_window(self):
        vbox = self.vbox

        self.file_label = gtk.Label('')
        self.update_file_label()
        vbox.pack_start(self.file_label,expand=0, fill=0)

        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw)

        self.model = gtk.ListStore(gobject.TYPE_STRING, # "Undo" or "Redo"
                                   gobject.TYPE_STRING, # Description
                                   gobject.TYPE_STRING  # stock-id for icon
                                   )

        treeview = gtk.TreeView(self.model)
        treeview.set_headers_visible(False)
        sw.add(treeview)
        column = gtk.TreeViewColumn('', gtk.CellRendererPixbuf(),
                                    stock_id=COLUMN_PIXBUF)
        treeview.append_column(column)

        column = gtk.TreeViewColumn('Operation', gtk.CellRendererText(),
                                    text=COLUMN_OPERATION)
        treeview.append_column(column)

        column = gtk.TreeViewColumn('Description', gtk.CellRendererText(),
                                    text=COLUMN_DESCRIPTION)
        treeview.append_column(column)

        # Scroll to the most recent undo entry and highlight it
        if self.context.HasEditor():
            self.update_model()
            lastundopos = len(self.context.editor.document.undo.undoinfo) - 1
            if lastundopos > 0:
                treeview.scroll_to_cell((lastundopos,), column, False, 0.5,0.5)
                treeview.set_cursor((lastundopos,), column, False)

        treeview.connect("row-activated", self.row_activated)

        self.set_default_size(0, 300)
        self.show_all()


    def update_model(self):
        self.model.clear()
        doc_undo = self.context.editor.document.undo

        for item in doc_undo.redoinfo:
            iter = self.model.append()
            self.model.set(iter,
                           COLUMN_OPERATION, _("Redo"),
                           COLUMN_DESCRIPTION, item[0],
                           COLUMN_PIXBUF, "gtk-redo")

        for item in doc_undo.undoinfo:
            iter = self.model.prepend()
            self.model.set(iter,
                           COLUMN_OPERATION, _("Undo"),
                           COLUMN_DESCRIPTION, item[0],
                           COLUMN_PIXBUF, "gtk-undo")


    def row_activated(self, treeview, row, column):
        """Performs Undo or Redo operations and updates the model"""
        self.undo_from_dialog = 1

        model = treeview.get_model()
        iter = model.get_iter(row)
        path = model.get_path(iter)

        if model.get_value(iter, COLUMN_OPERATION) == _("Undo"):
            while iter:
                if model.get_value(iter, COLUMN_OPERATION) == _("Undo"):
                    self.context.editor.document.Undo()
                    model.set(iter, COLUMN_OPERATION, _("Redo"))
                    model.set(iter, COLUMN_PIXBUF, "gtk-redo")
                iter = model.iter_next(iter)
        else:
            iter = model.get_iter_first()
            while iter:
                if model.get_path(iter)[0] <= path[0] and \
                       model.get_value(iter, COLUMN_OPERATION) == _("Redo"):
                    self.context.editor.document.Redo()
                    model.set(iter, COLUMN_OPERATION, _("Undo"))
                    model.set(iter, COLUMN_PIXBUF, "gtk-undo")
                iter = model.iter_next(iter)

        self.undo_from_dialog = 0


    def undo_redo(self):
        """Updates the treeview model when an undo-redo operation
        is not performed from the history dialog"""
        if not self.undo_from_dialog:
            self.update_model()

    def update_file_label(self):
        if self.context.HasEditor():
            filename = self.context.editor.document.meta.filename
        else:
            filename = ''
        self.file_label.set_text(filename)

    def context_changed(self):
        self.update_file_label()
        if self.context.HasEditor():
            self.update_model()
        else:
            self.model.clear()


