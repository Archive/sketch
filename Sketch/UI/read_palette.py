# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2002 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

__version__ = "$Revision$"

# RGB palette file-reading code lifted from Sketch 0.6.12

from string import strip, split, atof, atoi

magic_rgb_palette = '##Sketch RGBPalette 0'
magic_gimp_palette = 'GIMP Palette'


def read_palette_file(filename):
    """Read the palette file filename"""
    file = open(filename)
    line = file.readline()
    line = strip(line)
    palette = None
    try:
        if line == magic_rgb_palette:
            palette = ReadRGBPaletteFile(filename)
        elif line == magic_gimp_palette:
            palette = Read_X_RGB_TXT(filename)
    except:
        warn_tb(USER)
    return palette


def ReadRGBPaletteFile(filename):
    file = open(filename)

    line = file.readline()
    if line != magic_rgb_palette + '\n':
        file.close()
        raise ValueError, 'Invalid file type'

    palette = []

    linenr = 1
    for line in file.readlines():
        line = strip(line)
        linenr = linenr + 1
        if not line or line[0] == '#':
            continue

        line = split(line, None, 3)

        if len(line) != 4:
            warn(INTERNAL, '%s:%d: wrong number of fields', filename, linenr)
            continue
        try:
            rgb = tuple(map(atof, line[:3]))
        except:
            warn(INTERNAL, '%s:%d: cannot parse rgb values', filename, linenr)
            continue

        for value in rgb:
            if value < 0 or value > 1.0:
                warn(INTERNAL, '%s:%d: value out of range', filename, linenr)
                continue

        name = strip(line[-1])

        palette.append((rgb,name))

    file.close()

    return palette



def Read_X_RGB_TXT(filename):
    file = open(filename)

    palette = []

    linenr = 0
    color_num = 0
    for line in file.readlines():
        line = strip(line)
        linenr = linenr + 1
        if not line or line[0] in ('#', '!'):
            # an empty line or an X-style comment (!) or a GIMP comment (#)
            # GIMP's palette files have practically the same format as rgb.txt
            continue

        line = split(line, None, 3)
        if len(line) == 3:
            # the name is missing
            while 1:
                name = 'color ' + str(color_num)
                try:
                    palette[name]
                    used = 1
                except KeyError:
                    used = 0
                if not used:
                    line.append(name)
                    break
                color_num = color_num + 1
        if len(line) != 4:
            warn(INTERNAL, '%s:%d: wrong number of fields', filename, linenr)
            continue
        try:
            values = map(atoi, line[:3])
        except:
            warn(INTERNAL, '%s:%d: cannot parse rgb values', filename, linenr)
            continue

        rgb = []
        for value in values:
            value = round(value / 255.0, 3)
            if value < 0:
                value = 0.0
            elif value > 1.0:
                value = 1.0
            rgb.append(value)
        rgb = tuple(rgb)

        name = strip(line[-1])

        palette.append((rgb, name))

    file.close()

    return palette

