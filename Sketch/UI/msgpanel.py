# Sketch - A Python-based interactive drawing program
# Copyright (C) 2000, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# A panel to display warning messages. This is much better than having
# to click away lots of message boxes.
#
# Currently this dialog uses a CList widget, but a text widget would
# probably be better.
#
# Some other enhancements:
#
# - collect all messages (including error messages with or without
#   tracebacks) in the application object and make this panel just a
#   view of that.
#
# - If tracebacks are supported, there should be a way to hide/show the
#   traceback
#
# - hide/show the different kinds of messages (warnings, errors,...)
#

import gtk
from gtk import FILL, EXPAND

import Sketch
from Sketch import _

from skpanel import SketchPanel


class MessagePanel(SketchPanel):

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("Messages"))

    def build_window(self):
        vbox = self.vbox

        table = gtk.Table(2, 2)
        table.show()
        vbox.pack_start(table)

        hadjust = gtk.Adjustment()
        hscrollbar = gtk.HScrollbar(hadjust)
        hscrollbar.show()
        table.attach(hscrollbar, 0, 1, 1, 2, EXPAND|FILL, 0)
        
        vadjust = gtk.Adjustment()
        vscrollbar = gtk.VScrollbar(vadjust)
        vscrollbar.show()
        table.attach(vscrollbar, 1, 2, 0, 1, 0, EXPAND|FILL)

        
        self.listbox = gtk.CList()
        self.listbox.set_hadjustment(hadjust)
        self.listbox.set_vadjustment(vadjust)
        table.attach(self.listbox, 0, 1, 0, 1, EXPAND|FILL, EXPAND|FILL)

        button = gtk.Button(_("Close"))
        button.connect("clicked", self.close)
        self.action_area.pack_start(button, expand = 0, fill = 0)
        button = gtk.Button(_("Clear"))
        button.connect("clicked", self.clear)
        self.action_area.pack_start(button, expand = 0, fill = 0)
        
        vbox.show_all()

    def print_warning(self, message):
        self.listbox.append((message,))

    def clear(self, *args):
        self.listbox.clear()
