# Sketch - A Python-based interactive drawing program
# Copyright (C) 1998, 1999, 2000, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from types import TupleType
                
import gtk

import Sketch
from Sketch.Base.const import CHANGED
from Sketch.Lib.units import unit_dict, unit_names
from Sketch.Lib.util import format

import Sketch._sketch

from converters import converters

# NLS:
formats = {'in': '(%3.3f", %3.3f")',
	   'pt': '(%3.1fpt, %3.1fpt)',
	   'cm': '(%2.2fcm, %2.2fcm)',
	   'mm': '(%3.1fmm, %3.1fmm)',
	   }

class PositionLabel(gtk.Label):

    context_menu = None

    def __init__(self, update = None):
        self.update = update
	gtk.Label.__init__(self, "")
	#self.bind('<ButtonPress-3>', self.popup_context_menu)
	self.connect('destroy', self.destroy_event)
        self.set_unit(Sketch.UI.preferences.default_unit)
        Sketch.UI.preferences.Subscribe(CHANGED, self.preference_changed)

    def destroy_event(self, widget):
        self.update = None

    def Update(self, *rest):
	if self.update is not None:
	    x, y = self.update()
	    x = x / self.factor
	    y = y / self.factor
	    self.set_text(self.format % (x, y))

    def popup_context_menu(self, event):
        # XXX gtk
	if self.context_menu is None:
	    items = []
	    set_unit = self.SetUnit
	    for unit in unit_names:
		items.append(MenuCommand(unit, set_unit, unit))
	    self.context_menu = UpdatedMenu(self, items, tearoff = 0)
	self.context_menu.Popup(event.x_root, event.y_root)

    def set_unit(self, unit):
        self.factor = unit_dict[unit]
        self.format = formats[unit]
        self.Update()
        
    def SetUnit(self, unit):
	self.unit = unit
        if Sketch.UI.preferences.poslabel_sets_default_unit:
            Sketch.UI.preferences.default_unit = unit
        else:
            self.set_unit(unit)

    def preference_changed(self, pref, value):
        if pref == 'default_unit':
            self.set_unit(value)



            
class InfoLabel(gtk.Label):

    def __init__(self, update = None):
        self.update = update
	gtk.Label.__init__(self, "")
	self.connect('destroy', self.destroy_event)
        Sketch.UI.preferences.Subscribe(CHANGED, self.preference_changed)
        self.idle_added = 0
        self.Update()

    def __del__(self):
        print '__del__', self

    def destroy_event(self, widget):
        self.update = None
        Sketch.UI.preferences.Unsubscribe(CHANGED, self.preference_changed)

    def do_update(self, *args):
        self.idle_added = 0
        text = self.update()
        if type(text) == TupleType:
            template, dict = text
            text = format(template, converters, dict)
        self.set_text(text)
        return 0

    def Update(self, *rest):
        if self.update is not None:
            text = self.update()
            if type(text) == TupleType:
                template, dict = text
                text = format(template, converters, dict)
            self.set_text(text)

    def preference_changed(self, pref, value):
        if pref == 'default_unit':
            self.Update()
