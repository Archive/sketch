# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

import gtk
from gtk import EXPAND, FILL, SHRINK, UPDATE_DELAYED, CAN_FOCUS

import Sketch
import Sketch.Editor
from Sketch import _
from Sketch.Base import Publisher, config
from Sketch.Base.const import TOOL, POSITION, CURRENTINFO, CHANGED, UNDO
from Sketch.Plugin import plugins
from Sketch.Lib import util

from canvas import SketchCanvas
import command
import poslabel
import gtkdevice
from skpixmaps import pixmaps

class IndicatorButton(gtk.ToggleButton):

    def __init__(self, context, main_window):
        image = gtk.Image()
        image.set_from_pixmap(*pixmaps.LightBulbOn)
        self.pixmap_on = image
        image = gtk.Image()
        image.set_from_pixmap(*pixmaps.LightBulbOff)
        self.pixmap_off = image
        gtk.ToggleButton.__init__(self)
        self.add(self.pixmap_off)
        self.main_window = main_window
        self.context = context
        self.context.Subscribe(CHANGED, self.ContextChanged)
        self.clicked_id = self.connect('clicked', self.set_main_window)
        self.connect('destroy', self.Destroy)

    def Destroy(self, *args):
        self.context.Unsubscribe(CHANGED, self.ContextChanged)
        self.main_window = None

    def ContextChanged(self):
        self.handler_block(self.clicked_id)
        on = self.main_window == self.context.window
        self.set_active(on)
        self.remove(self.get_children()[0])
        if on:
            self.add(self.pixmap_on)
        else:
            self.add(self.pixmap_off)
        self.show_all()
        self.handler_unblock(self.clicked_id)

    def set_main_window(self, widget):
        if self.main_window is not self.context.window:
            self.context.SetWindow(self.main_window)
        else:
            self.ContextChanged()


class SketchMainWindow(Publisher):

    def __init__(self, application, document = None, editor = None):
        self.application = application
        if document is not None:
            self.document = document
            #self.editor = Sketch.Editor.DocumentEditor(self.document)
            self.editor = Sketch.Editor.EditorWithSelection(self.document)
        elif editor is not None:
            self.document = editor.Document()
            self.editor = editor
        else:
            raise TypeError("SketchMainWindow must be instantiated with"
                            " either a document or an editor")
        self.document.IncRef()
        self.editor.IncRef()
        self.canvas = None
        self.window = None
        self.build_window()
        self.set_window_title()

    def __del__(self):
        #pass
        print 'SketchMainWindow.__del__'

    def close(self, *args):
        print 'SketchMainWindow.close', self
        self.editor.Unsubscribe(CURRENTINFO, self.infolabel.Update)
        self.document.Unsubscribe(UNDO, self.modified.Update)
        self.window.destroy()
        self.application.remove_view(self)
        self.canvas.Destroy()
        self.document.DecRef()
        self.editor.DecRef()
        self.canvas = self.window = self.editor = None
        Publisher.Destroy(self)

    def Close(self):
        print 'SketchMainWindow.Close', self,
        self.window.destroy()

    def build_window(self):
        self.window = gtk.Window()
        self.window.connect("destroy", self.close)
        self.window.realize()
        #gtkdevice.set_colormap(self.window)

        vbox = gtk.VBox()
        vbox.set_resize_mode(gtk.RESIZE_QUEUE)
        self.window.add(vbox)

        table = gtk.Table(3, 3)
        vbox.pack_start(table)

        menubutton = gtk.Button()
        arrow = gtk.Arrow(gtk.ARROW_RIGHT, gtk.SHADOW_OUT)
        menubutton.add(arrow)

        menubutton.connect('button_press_event', self.show_menu)
        table.attach(menubutton, 0, 1, 0, 1, FILL, FILL)

        self.hruler = gtk.HRuler()
        table.attach(self.hruler, 1, 2, 0, 1, EXPAND|FILL, 0)

        self.vruler = gtk.VRuler()
        table.attach(self.vruler, 0, 1, 1, 2, 0, EXPAND|FILL)

        hadjust = gtk.Adjustment()
        hscrollbar = gtk.HScrollbar(hadjust)
        hscrollbar.set_update_policy(UPDATE_DELAYED)
        table.attach(hscrollbar, 1, 2, 2, 3, EXPAND|FILL, 0)

        vadjust = gtk.Adjustment()
        vscrollbar = gtk.VScrollbar(vadjust)
        vscrollbar.set_update_policy(UPDATE_DELAYED)
        table.attach(vscrollbar, 2, 3, 1, 2, 0, EXPAND|FILL)

        button = IndicatorButton(self.application.context, self)
        button.unset_flags(CAN_FOCUS)
        self.application.tooltips.set_tip(button, _("Set Active Window"))
        table.attach(button, 2, 3, 0, 1, FILL, FILL)

        self.canvas = SketchCanvas(self.application, self, self.document,
                                   self.editor)
        self.canvas.SetAdjustments(hadjust, vadjust)
        self.canvas.SetRulers(self.hruler, self.vruler)
        self.canvas.set_name('canvas')
        self.canvas.Subscribe(POSITION, self.update_rulers)
        table.attach(self.canvas, 1, 2, 1, 2)

        self.statusbar = gtk.HBox() #GtkTable(3, 1)
        #self.statusbar.set_resize_mode(gtk.RESIZE_QUEUE)
        vbox.pack_start(self.statusbar, 0, 0)

        frame = gtk.Frame()
        frame.set_shadow_type(gtk.SHADOW_IN)
        #self.statusbar.attach(frame, 0, 1, 0, 1, SHRINK|FILL, 0)
        self.statusbar.pack_start(frame)
        self.modified = poslabel.InfoLabel(self.EditedInfoText)
        self.document.Subscribe(UNDO, self.modified.Update)
        frame.add(self.modified)

        frame = gtk.Frame()
        frame.set_shadow_type(gtk.SHADOW_IN)
        #self.statusbar.attach(frame, 1, 2, 0, 1, SHRINK|FILL, 0)
        self.statusbar.pack_start(frame)
        self.poslabel = poslabel.PositionLabel(self.canvas.CurrentPos)
        self.canvas.Subscribe(POSITION, self.poslabel.Update)
        frame.add(self.poslabel)

        frame = gtk.Frame()
        frame.set_shadow_type(gtk.SHADOW_IN)
        frame.show()
        #self.statusbar.attach(frame, 2, 3, 0, 1, SHRINK|EXPAND|FILL, 0)
        self.statusbar.pack_start(frame)
        self.infolabel = poslabel.InfoLabel(self.editor.InfoText)
        self.editor.Subscribe(CURRENTINFO, self.infolabel.Update)
        self.infolabel.show()
        frame.add(self.infolabel)

        self.window.set_size_request(*Sketch.UI.preferences.window_size)
        #self.window.set_policy(1, 1, 0)
        self.window.show_all()

        self.canvas.FitPageToWindow()
        self.canvas.grab_focus()
        #gtkdevice.set_colormap(self.canvas)

    #
    #
    #

    def update_rulers(self):
        pos = self.canvas.CurrentPos()
        self.hruler.set_property('position', pos.x)
        self.vruler.set_property('position',  pos.y)

    def show_menu(self, widget, event):
        # this was inspired by gimp...
        menu = self.application.Menu(self)
        menu.popup(None, None, None, event.button, event.time)
        widget.emit_stop_by_name('button_press_event')

    def Document(self):
        return self.document

    def Canvas(self):
        return self.canvas

    def Editor(self):
        return self.editor

    def EditedInfoText(self):
        if self.document.WasEdited():
            return _("modified")
        return _("unmodified")

    def set_window_title(self):
        #self.root.client(util.gethostname())
        if self.window is not None:
            if self.document is not None:
                appname = config.name
                meta = self.document.meta
                if meta.compressed:
                    docname = os.path.split(meta.compressed_file)[1]
                    docname = os.path.splitext(docname)[0]
                else:
                    docname = self.document.meta.filename
                title = Sketch.UI.preferences.window_title_template % locals()
                command = (config.sketch_command,
                           self.document.meta.fullpathname)
            else:
                title = config.name
                command = (config.sketch_command, )
            self.window.set_title(title)
            #self.root.command(command)

    def SaveInteractively(self, force_dialog = 0):
        filename =  self.document.meta.fullpathname
        native_format = self.document.meta.native_format
        compressed_file = self.document.meta.compressed_file
        app = self.application
        if force_dialog or not filename or not native_format:
            dir = self.document.meta.directory
            if not dir:
                dir = os.getcwd()
            name = self.document.meta.filename
            basename, ext = os.path.splitext(name)
            if not native_format:
                name = basename + '.sk'
            filename = app.GetSaveFilename(#filetypes = skapp.savefiletypes(),
                                           initialdir = dir,
                                           initialfile = name)
            if not filename:
                return
        self.SaveToFile(filename, 'auto')

    def SaveToFile(self, filename, fileformat = None):
        app = self.application
        if fileformat == 'auto':
            extension = os.path.splitext(filename)[1]
            fileformat = plugins.guess_export_plugin(extension)
            if not fileformat:
                fileformat = Sketch.Plugin.preferences.native_format
        try:
            if not self.document.meta.backup_created:
                try:
                    #if compressed_file:
                    #   util.make_backup(compressed_file)
                    #    else:
                    util.make_backup(filename)
                except util.BackupError, value:
                    backupfile = value.filename
                    strerror = value.strerror
                    msg = (_("Cannot create backup file %(filename)s:\n"
                             "%(message)s\n"
                             "Choose `continue' to try saving anyway,\n"
                             "or `cancel' to cancel.")
                           % {'filename':`backupfile`, 'message':strerror})
                    cancel = _("Cancel")
                    result = app.MessageBox(title = _("Save To File"),
                                            message = msg, icon = 'warning',
                                            buttons = (_("Continue"), cancel))
                    if result == cancel:
                        return

                self.document.meta.backup_created = 1
            if fileformat is None:
                fileformat = Sketch.Plugin.preferences.native_format
            try:
                saver = plugins.find_export_plugin(fileformat)
                #if compressed_file:
                #    # XXX there should be a plugin interface for this kind
                #    # of post-processing
                #    file = os.popen('gzip -c -9 > ' + compressed_file, 'w')
                #    saver(self.document, filename, file = file)
                #else:
                saver(self.document, filename)
            finally:
                saver.UnloadPlugin()
        except IOError, value:
            if type(value) == type(()):
                value = value[1]
            app.MessageBox(title = _("Save To File"),
                           message = _("Cannot save %(filename)s:\n"
                                       "%(message)s") \
                           % {'filename':`os.path.split(filename)[1]`,
                              'message':value},
                           icon = 'warning')
            app.remove_mru_file(filename)
            return

        if fileformat == Sketch.Plugin.preferences.native_format:
            dir, name = os.path.split(filename)
            # XXX should meta.directory be set for non-native formats as well
            self.document.meta.directory = dir
            self.document.meta.filename = name
            self.document.meta.fullpathname = filename
            self.document.meta.file_type \
                            = Sketch.Plugin.preferences.native_format
            self.document.meta.native_format = 1
        #if not compressed_file:
        #    self.document.meta.compressed_file = ''
        #    self.document.meta.compressed = ''
        #if compressed_file:
        #    self.add_mru_file(compressed_file)
        #else:
        app.add_mru_file(filename)

        self.set_window_title()



add = Sketch.Editor.Add
cmd = command.WindowCommand
ecmd = Sketch.Editor.Command

add(cmd('close_window', _("Close Window"),
        lambda context: context.window.Close()))
add(ecmd('file_save', _("Save"),
        lambda context: context.window.SaveInteractively(),
        sensitive = lambda context: context.HasEditor()))
add(ecmd('file_save_as', _("Save As..."),
        lambda context: context.window.SaveInteractively(force_dialog = 1),
        sensitive = lambda context: context.HasEditor()))
