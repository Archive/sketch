# Sketch - A Python-based interactive drawing program
# Copyright (C) 2004, 2005 by Bernhard Herzog
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2001, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import os
import gtk
from Sketch import _, _skgtk
from Sketch.Base import Publisher, const
from Sketch.Graphics import CreateRGBColor
from gtkdevice import GraphicsDevice


class MessageBox(gtk.Dialog):

    def __init__(self, title, message, buttons, default = 0,
                 pixmap = None, modal = 1):
        gtk.Dialog.__init__(self)
        self.set_title(title)
        self.connect("destroy", self.quit)
        self.connect("delete_event", self.quit)
        self._modal = modal
        if modal:
            self.grab_add()
        hbox = gtk.HBox(spacing = 5)
        hbox.set_border_width(5)
        self.vbox.pack_start(hbox)
        hbox.show()
        if pixmap:
            self.realize()
            pixmap = gtk.Pixmap(self, pixmap)
            hbox.pack_start(pixmap, expand=FALSE)
            pixmap.show()
        label = gtk.Label(message)
        label.set_justify(gtk.JUSTIFY_LEFT)
        hbox.pack_start(label)
        label.show()

        for text in buttons:
            b = gtk.Button(text)
            b.set_border_width(10)
            b.set_flags(gtk.CAN_DEFAULT)
            b.set_data("user_data", text)
            b.connect("clicked", self.click)
            self.action_area.pack_start(b, expand = 0, fill = 0)
            b.show()
        self.ret = None

    def quit(self, *args):
        if self._modal:
            self.grab_remove()
        self.hide()
        self.destroy()
        gtk.main_quit()

    def click(self, button):
        self.ret = button.get_data("user_data")
        self.quit()

# create a message box, and return which button was pressed
def message_box(title, message, buttons = None, default = 0, pixmap = None,
                modal = 1):
    if buttons is None:
        buttons  = (_("Ok"),)
    win = MessageBox(title, message, buttons, default, pixmap = pixmap,
                     modal = modal)
    win.show()
    gtk.main()
    return win.ret


class ModalDialogMixin:

    """Mixin-class to turn GTK dialogs into modal dialogs with more
    convenient information passing"""

    def __init__(self):
        # will hold the data the user entered. Initialize to None so
        # that in closing the dialog through the WM is treated like
        # Cancel.
        self.result = None

        # bind the standard buttons.
        self.ok_button.connect("clicked", self.ok)
        self.cancel_button.connect("clicked", self.cancel)

        # closing dialogs with window manager leaves mainloop
        # running if we don't catch the destroy signal...
        self.connect("destroy", self.on_destroy)

        # turn on the modal flag so that the dialog becomes modal as
        # soon as it's shown.
        self.set_modal(1)

    def on_destroy(self, *args):
        """Quit the main-loop."""
        gtk.main_quit()

    def ok(self, *args):
        """Retrieve the user's input and quit the dialog.

        Callback for the OK button.
        """
        self.quit(self.get_result())

    def cancel(self, *args):
        """Quit the dialog without result"""
        self.quit(None)

    def quit(self, result):
        """Quit the dialog with result result.

        Call this method from OK with the data the user entered in one
        Python object or from Cancel with None.
        """
        self.result = result
        self.destroy()

    def run(self):
        """Run the dialog and return the data the user entered or None.

        Running the dialog means show it, enter a recursive main-loop,
        and once the main-loop finishes, return the data passed to the
        quit method, which normally is None if the dialog was cancelled
        or an object containing the data the user entered, which depends
        on the dialog.
        """
        self.show()
        gtk.main()
        return self.result

    def get_result(self):
        """Return the data the user entered.

        Override this method to return whatever is suitable for the
        dialog. This method is called be ok() method to get the user's
        imput data.
        """
        return None


class FileSelector(ModalDialogMixin, gtk.FileSelection):

    def __init__(self, title):
        gtk.FileSelection.__init__(self, title)
        ModalDialogMixin.__init__(self)

    def get_result(self):
        return self.get_filename()

def get_filename(title, initialdir = '', initialfile = '', filetypes = ()):
    win = FileSelector(title)
    win.set_filename(os.path.join(initialdir, initialfile))
    return win.run()


class ColorDialog(ModalDialogMixin, gtk.ColorSelectionDialog):

    def __init__(self, title, color = None):
        gtk.ColorSelectionDialog.__init__(self, title)
        ModalDialogMixin.__init__(self)
        if color is not None:
            cmap = self.get_colormap()
            r, g, b = color
            gdk_color = cmap.alloc_color(int(round(0xFFFF * r)),
                                         int(round(0xFFFF * g)),
                                         int(round(0xFFFF * b)))
            self.colorsel.set_current_color(gdk_color)

    def get_result(self):
        gdk_color = self.colorsel.get_current_color()
        return CreateRGBColor(gdk_color.red / 65536.0,
                              gdk_color.green / 65536.0,
                              gdk_color.blue / 65536.0)


def get_color(title, color = None):
    win = ColorDialog(title, color)
    return win.run()

def _gtk_colors(cmap, color):
    r, g, b = color
    bg = cmap.alloc_color(int(round(r * 0xFFFF)),
                          int(round(g * 0xFFFF)),
                          int(round(b * 0xFFFF)))
    luminance = 0.30*r + 0.59*g + 0.11*b
    #luminance = (min(color) + max(color)) / 2
    if luminance > 0.5:
        fg = cmap.alloc_color(0, 0, 0)
    else:
        fg = cmap.alloc_color(0xFFFF, 0xFFFF, 0xFFFF)
    return fg, bg

class ColorButton(gtk.Button, Publisher):

    def __init__(self, title, dialog_title, color):
        gtk.Button.__init__(self, title)
        self.dialog_title = dialog_title
        self.color = color
        self.connect("clicked", self.choose_color)
        self.connect("realize", self.realized)

    def realized(self, *args):
        self.SetColor(self.color)

    def SetColor(self, color):
        self.color = color
        style = self.get_style().copy()
        fg, bg = _gtk_colors(self.get_colormap(), color)
        for i in range(5):
            style.fg[i] = fg
            style.bg[i] = bg
        self.set_style(style)
        self.get_children()[0].set_style(style)

    def Color(self):
        return self.color

    def choose_color(self, *args):
        color = get_color(self.dialog_title, self.color)
        if color is not None:
            self.SetColor(color)
            self.issue(const.CHANGED, self.color)


class SketchDrawingArea(gtk.DrawingArea):

    def __init__(self, **kw):
        gtk.DrawingArea.__init__(self, **kw)
        self.region = _skgtk.create_region()
        self.idle_redraw_tag = None
        self.gcs_initialized = 0
        self.gc = GraphicsDevice()
        self.bind_events()

    def bind_events(self):
        self.connect("expose_event", self.expose_event)
        self.connect("realize", self.realize_method)
        self.connect("configure_event", self.configure_event)
        self.connect("focus_in_event", self.focus_in_event)
        self.connect("focus_out_event", self.focus_out_event)

    def focus_in_event(self, widget, event):
        """Handle focus_in_event

        Do nothing, just return true. The default behavior of the
        drawingarea is to queue a redraw which is not needed in Skencil
        and just causes unnecessary redraws.
        """
        return gtk.TRUE

    def focus_out_event(self, widget, event):
        """Handle focus_out_event

        Do nothing, just return true. The default behavior of the
        drawingarea is to queue a redraw which is not needed in Skencil
        and just causes unnecessary redraws.
        """
        return gtk.TRUE

    def configure_event(self, widget, event):
        self.ResizedMethod(event.width, event.height)

    def ResizedMethod(self, width, height):
        # to be implemented by derived classes
        pass

    def expose_event(self, widget, event):
        apply(self.region.UnionRectWithRegion, event.area)
        self.UpdateWhenIdle()
        return 0

    def UpdateWhenIdle(self):
        if self.idle_redraw_tag is None:
            self.idle_redraw_tag = gtk.idle_add(self.idle_redraw)

    def idle_redraw(self):
        self.idle_redraw_tag = None
        region = self.region
        self.region = _skgtk.create_region()
        self.RedrawMethod(region)

    def realize_method(self, *args):
        if not self.gcs_initialized:
            self.init_gcs()

    def init_gcs(self):
        self.gc.init_gc(self.window, graphics_exposures = 1)
        self.gcs_initialized = 1


class CommonComboBox(gtk.ComboBox, Publisher):

    """Generic combobox wrapper.

    Derived classes have to create a tree model with the data shown in
    the combobox and some information about the model so that the
    default implementations of the various methods can do the right
    thing.  Derived classes also have to create any column objects that
    display the contents of the tree model.

    To better tie in with Skencil's message passing, instances of this
    class issue a CHANGED message with the return value of GetOption as
    a parameter when the user selects a new value.
    """

    def __init__(self, store, default_value = None, value_column = 0):
        """Initialize the generic combobox

        Parameters:
           store -- a tree model holding the data to be displayed
           default_value -- the object to return in GetOption when no
                            value is currently selected.
           value_column -- the column in the tree model which holds the
                           value to be returned by GetOption

        If default_value is omitted, it defaults to None.  If
        value_column is omitted it defaults to 0 so that the usual case
        of only one column doesn't require the derived class to specify
        a value.
        """
        self.value_column = value_column
        self.default_value = default_value
        gtk.ComboBox.__init__(self, store)
        self.connect("changed", self.issue_changed)

    def issue_changed(self, widget, *args):
        """Issue the CHANGED message with self.GetOption() as parameter

        This method is connected to the ComboBox's 'changed' gtk signal
        so that the CHANGED message is issued whenever the user selects
        a new value.
        """
        value = self.GetOption()
        self.issue(const.CHANGED, value)

    def GetOption(self):
        """Return the value associated with the currently active row

        The return value is the value of the column value_column given
        to __init__ of the active row.  If no row is active, return the
        default_value given to __init__.
        """
        idx = self.get_active()
        if idx >= 0:
            return self.store[idx][self.value_column]
        return self.default_value

    def SetOption(self, value):
        """Activate the row holding value in its value_column.

        If no row holds the value, deactivate any active row.
        """
        for row in self.store:
            if value == row[self.value_column]:
                self.set_active(row.path[0])
                break
        else:
            # width is not in self.store, so deselect any currently
            # selected width
            self.set_active(-1)
