# Sketch - A Python-based interactive drawing program
# Copyright (C) 2000, 2003 by Bernhard Herzog
# Author 2004 Bernhard Reiter
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, string

import gobject
import gtk

import Sketch
from Sketch import _
from Sketch.Base.warn import pdebug, warn_tb, INTERNAL

from skpanel import SketchPanel

# column numeration for the tree
MODULENAME_COLUMN = 0
FILENAME_COLUMN = 1


class ReloadPanel(SketchPanel): # SketchPanel is derived from Gtk.Dialog

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("Reload Modules"))

    def build_window(self):
        """Use Treeview like in pyGTK 'Tree Store' example for reload dialog.
        """
        # vbox is the upper part of gtk.Dialog
        vbox = self.vbox

        # Scrolled Window ensures that its contents gets scrollbars
        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        vbox.pack_start(sw, 1, 1)

        # create model
        self.model = gtk.TreeStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        # model will be filled in update_list() later

        # create treeview
        self.treeview = gtk.TreeView(self.model)
        self.treeview.set_rules_hint(True) # might lead to more readable lines

        # add column renderers
        renderer = gtk.CellRendererText()
        renderer.set_property("xalign", 0.0)
        column = gtk.TreeViewColumn("Module", renderer, text=MODULENAME_COLUMN)
        self.treeview.append_column(column)

        renderer = gtk.CellRendererText()
        renderer.set_property("xalign", 0.0)
        column = gtk.TreeViewColumn("Filename", renderer, text=FILENAME_COLUMN)
        self.treeview.append_column(column)

        # TODO: also wire double click on row to reload or so.


        sw.add(self.treeview)


        # lower part of gtk.Dialog
        button = gtk.Button(_("Close"))
        button.connect("clicked", self.close)
        self.action_area.pack_start(button, expand = 0, fill = 0)
        button = gtk.Button(_("Update"))
        button.connect("clicked", self.update_list)
        self.action_area.pack_start(button, expand = 0, fill = 0)
        button = gtk.Button(_("Reload"))
        button.connect("clicked", self.do_reload)
        self.action_area.pack_start(button, expand = 0, fill = 0)

        # update and show
        self.update_list()
        self.set_default_size(300, 520)
        self.show_all()


    def update_list(self, *args):
        """Refill the model with the current sys.modules and expand view."""
        model = self.model
        model.clear()

        modules = sys.modules.items()
        modules.sort()

        packages = {}
        for name, module in modules:
            if module is not None:
                if '.' in name:
                    # module is part of a package
                    parentname = string.join(string.split(name, '.')[:-1], '.')
                    parent = packages.get(parentname)
                else:
                    parent = None
                    sibling = None
                package = hasattr(module, '__path__')
                file = getattr(module, '__file__', '')

                iter = model.append(parent)
                model.set(iter,
                          MODULENAME_COLUMN, name,
                          FILENAME_COLUMN, file)
                if package:
                    packages[name] = iter

        self.treeview.expand_all()

    def do_reload(self, *args):
        """Try to reload the selected module."""
        model, iter = self.treeview.get_selection().get_selected()
        if not iter:
            # no selection
            return

        name=model.get_value(iter, MODULENAME_COLUMN)
        pdebug(None, 'reloading', name)
        try:
            reload(sys.modules[name])
        except:
            warn_tb(INTERNAL)
