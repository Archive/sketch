# Sketch - A Python-based interactive drawing program
# Copyright (C) 2004, 2005 by Bernhard Herzog
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2002, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

import gtk, gtk.keysyms

import Sketch
import Sketch.Editor

from Sketch import _
from Sketch.Base import config, Publisher, SketchLoadError
from Sketch.Base.const import CLIPBOARD, VIEW, CHANGED, CONFIG, \
     PRINT_WARNING, PRINT_TRACEBACK, SAVE_PREFERENCES
from Sketch.Base.warn import pdebug
from Sketch.Graphics import document
from Sketch.Plugin import plugins, load

main_menu = Sketch.Editor.BuildMenuTree('<mainmenu>',
                                        Sketch.Editor.standard_menu)
keymap = Sketch.Editor.Keymap(Sketch.Editor.standard_keystrokes,
                              special_keys = gtk.keysyms.__dict__)
text_keymap = Sketch.Editor.Keymap(Sketch.Editor.text_keystrokes,
                                   special_keys = gtk.keysyms.__dict__)
align_keymap = Sketch.Editor.Keymap(Sketch.Editor.align_keystrokes,
                                   special_keys = gtk.keysyms.__dict__)
# XXX the keymaps should be maintained by the Editor sub-package

from mainwindow import SketchMainWindow
from toolbox import SketchToolbox
from menu import make_menu
import gtkmisc

#
# meta info defaults
#
meta_defaults = [
    ('fullpathname', None),             # the full pathname if read from a file
    ('filename', 'unnamed.sk'),         # filename without dir
    ('directory', None),                # the directory
    ('backup_created', 0),              # true if a backup has been created
#    ('format_name', plugins.NativeFormat),# the filetype (do we need this ?)
    ('native_format', 1),               # whether the file was in native format
    ('ps_directory', None),             # dir where PostScript file was created
    ('ps_filename', ''),                # name of last postscript file
    ('compressed', ''),                 # was compressed (by gzip)
    ('compressed_file', ''),            # filename of compressed file
    ('load_messages', ''),              # (warning) messages
    ]

for key, val in meta_defaults:
    if not hasattr(document.MetaInfo, key):
        setattr(document.MetaInfo, key, val)


#
#
#


#
#       Application classes


class GtkApplication:

    def __init__(self):
        pass

    def MessageBox(self, *args, **kw):
        if kw.has_key('icon'):
            del kw['icon']
        return apply(gtkmisc.message_box, args, kw)

    def GetOpenFilename(self, initialdir = '', initialfile = '',
                        filetypes = ()):
        return gtkmisc.get_filename(_("Open File"), initialdir = initialdir,
                                    initialfile = initialfile)


    def GetSaveFilename(self, initialdir = '', initialfile = '',
                        filetypes = ()):
        return gtkmisc.get_filename(_("Save File"), initialdir = initialdir,
                                    initialfile = initialfile)


    clipboard = None

    def EmptyClipboard(self):
        self.SetClipboard(None)

    def SetClipboard(self, data):
        self.clipboard = data

    def GetClipboard(self):
        return self.clipboard

    def ClipboardContainsData(self):
        return self.clipboard is not None


class ClipboardWrapper:

    def __init__(self, object):
        self.object = object

    def __del__(self):
        pdebug('__del__', '__del__', self)
        self.object.Destroy()

    def Object(self):
        return self.object


class AppContext(Sketch.Editor.Context):

    def __init__(self, application):
        self.window = None
        self.canvas = None
        Sketch.Editor.Context.__init__(self, application)
        Sketch.UI.preferences.Subscribe(CHANGED, self.forward, CONFIG)

    def _subscribe_window(self):
        forward = self.forward
        if self.canvas is not None:
            self.canvas.Subscribe(VIEW, forward, VIEW)

    def _unsubscribe_window(self):
        forward = self.forward
        if self.canvas is not None:
            self.canvas.Unsubscribe(VIEW, forward, VIEW)

    def SetWindow(self, window):
        if window is not self.window:
            self._unsubscribe_window()
            self.window = window
            if window is None:
                self.canvas = None
                self.set_editor(None)
            else:
                same = self.editor == window.Editor()
                self.canvas = window.Canvas()
                self.set_editor(window.Editor())
                if same:
                    # issue a changed message it the window changed. If
                    # the editor also changes, this is done by
                    # set_editor, so only do it if the editor is the
                    # same
                    self.issue(CHANGED)
            self._subscribe_window()

    def HasWindow(self):
        return self.window is not None


class SketchApplication(GtkApplication, Publisher):

    def __init__(self, filenames):
        self.filenames = filenames
        GtkApplication.__init__(self)
        gtk.rc_parse(os.path.join(config.std_res_dir, config.gtk_defaults))
        self.views = []
        self.toolbox = None
        self.menu = None
        self.accelgroup = None
        self.file_dialog = None
        self.tooltips = gtk.Tooltips()
        self.context = AppContext(self)

        # Keep track of the open panels. To that end panels maps the
        # panel classes to a dictionary of instances of the class
        # (mapping their id's to the objects). All panels must be
        # created through the InstantiatePanel method.
        self.panels = {}

        self.msgpanel = None
        Sketch.Subscribe(PRINT_WARNING, self.print_warning)
        Sketch.Subscribe(PRINT_TRACEBACK, self.print_traceback)
        self.build_window()

    def issue_clipboard(self):
        self.issue(CLIPBOARD)

    def SetClipboard(self, data):
        if data is not None:
            data = ClipboardWrapper(data)
        GtkApplication.SetClipboard(self, data)
        self.issue_clipboard()

    def AskUser(self, title, message):
        print title
        print message
        #return self.MessageBox(title = title, message = message,
        #                      buttons = tkext.YesNo) == tkext.Yes

    def print_warning(self, message):
        if self.msgpanel is not None:
            self.msgpanel.print_warning(message)
        else:
            self.MessageBox(title = _("Warning"), message = message)

    def print_traceback(self, message, traceback):
        tb = _("Print Traceback")
        result = self.MessageBox(title = _("Warning"), message = message,
                                 icon = 'warning', buttons = (_("OK"), tb))
        if result == tb:
            self.MessageBox(title = _("Traceback"), message = traceback)

    def load_file_dialog(self, *args):
        if self.file_dialog is None:
            self.file_dialog = gtk.FileSelection(_("Open File"))
            self.file_dialog.ok_button.connect("clicked", self.do_load_file)
            self.file_dialog.cancel_button.connect("clicked",
                                         lambda x: self.file_dialog.hide())
            self.file_dialog.connect("destroy", self.close_file_dialog)
        self.file_dialog.show()

    def close_file_dialog(self, *args):
        self.file_dialog = None

    def do_load_file(self, *args):
        #self.file_dialog.hide()
        self.LoadDocumentIntoNewEditor(self.file_dialog.get_filename())

    def Run(self):
        self.SetClipboard(None)
        gtk.main()

    def Exit(self):
        gtk.main_quit()

    def SavePreferences(self, *args):
        Sketch.Issue(SAVE_PREFERENCES)

    def build_window(self):
        # create the toolbox window
        self.toolbox = SketchToolbox(self, self.context)
        # then load the files
        if self.filenames:
            for filename in self.filenames:
                self.LoadDocumentIntoNewEditor(filename)
            self.filenames = []

    def add_view(self, view):
        self.views.append(view)
        if not self.context.HasWindow():
            self.SetContext(view)

    def remove_view(self, view):
        self.views.remove(view)
        if view is self.context.window:
            self.SetContext(None)

    def add_mru_file(self, filename):
        files = list(Sketch.UI.preferences.mru_files)
        if filename in files:
            files.remove(filename)
        files.insert(0, filename)
        if len(files) > Sketch.UI.preferences.mru_files_length:
            files = files[:Sketch.UI.preferences.mru_files_length]
        Sketch.UI.preferences.mru_files = tuple(files)

    def remove_mru_file(self, filename):
        files = list(Sketch.UI.preferences.mru_files)
        if filename in files:
            files.remove(filename)
        Sketch.UI.preferences.mru_files = tuple(files)

    def NewDocument(self, filename = ""):
        """
        Return a newly created document after opening an editor window for it.

        If a filename is given, associate that filename with the
        document object so that it's saved to that file by default.
        """
        doc = Sketch.Editor.InteractiveDocument(create_layer = 1)
        if filename:
            # set the document's meta data. FIXME: there should be a
            # better way to set it than through a function in the
            # Plugins package
            format = plugins.guess_export_plugin(os.path.splitext(filename)[1])
            load.add_meta_data(doc, filename, format)
        self.CreateDocumentEditor(doc)
        return doc

    def LoadDocument(self, filename):
        """Return the document read from the file filename.

        If there are errors, display a dialog with an error message.
        """
        doc = None
        try:
            if not os.path.isabs(filename):
                filename = os.path.join(os.getcwd(), filename)
            doc = load.load_drawing(filename,
                                doc_class = Sketch.Editor.InteractiveDocument)
            self.add_mru_file(filename)
        except SketchLoadError, value:
            # an error occurred. Display the error message
            self.MessageBox(title = _("Open"),
                            message = _("An error occurred:\n") + str(value))
            #warn_tb(USER, "An error occurred:\n", str(value))
            self.remove_mru_file(filename)
        else:
            # the loading was successful. See if the import filter had
            # any warning messages and display them
            messages = doc.meta.load_messages
            if messages:
                self.MessageBox(title = _("Open"),
                                message=_("Warnings from the import filter:\n")
                                + messages,
                                modal = 0)
            doc.meta.load_messages = ''
        return doc

    def CreateDocumentEditor(self, document = None, editor = None):
        self.add_view(SketchMainWindow(self, document, editor = editor))

    def LoadDocumentIntoNewEditor(self, filename):
        """
        Read the document from the file filename and create an editor window

        If there are errors, display a dialog with an error message.
        If the file does not exist, create an empty document.
        """
        if not os.path.exists(filename):
            # the file doesn't exist, so a new document
            self.NewDocument(filename)
        else:
            # the file exists, so load it
            doc = self.LoadDocument(filename)
            if doc is not None:
                self.CreateDocumentEditor(doc)

    def SaveDocument(self, document, filename):
        pass

    def Menu(self, window):
        self.context.SetWindow(window)
        if self.accelgroup is None:
            self.accelgroup = gtk.AccelGroup()
        if self.menu is None:
            self.menu = make_menu(main_menu, keymap, self.context,
                                  accel = self.accelgroup)
        return self.menu

    def SetContext(self, window):
        self.context.SetWindow(window)

    def InstantiatePanel(self, panelclass):
        """Popup and return a new panel, an instance of panelclass.

        If panelclass.multiple is true, i.e. if multiple panels of the
        same class are allowed, always create a new panel. Otherwise
        only create a new one if there's no panel of that class yet.

        All panels must be instantiated through this method to make sure
        that the information which panels are currently open is
        maintained correctly.
        """
        multiple = panelclass.multiple
        panel = self.panels.get(panelclass)
        if panel is None or multiple:
            # The panel is not instantiated yet or there can be multiple
            # instances.
            if panel is None:
                # There's no instance of panelclass yet. Initialize the
                # dictionary of panelclass instances
                self.panels[panelclass] = {}

            # crate the panel and put it into the list of instances
            panel = panelclass(self.context)
            self.panels[panelclass][id(panel)] = panel

            # subscribe to the destroy event so that it can be removed
            # from the list again.
            panel.connect("destroy", self.panel_closed, panelclass, panel)

            panel.show()
        else:
            # An instance already exists and it can't be instantiated
            # multiple times. So, panel is a dictionary containg the
            # panel. Retrieve it.
            panel = panel.values()[0]
        return panel

    def panel_closed(self, widget, panelclass, panel):
        """Remove panel from the panel dictionary.

        Called as a signal handler from the panel's destroy signal"""
        # remove the panel itself. The entry should always exist so it
        # should never fail.
        del self.panels[panelclass][id(panel)]
        # if there are no more instances of panelclass move its dict of
        # instances, too.
        if not self.panels[panelclass]:
            del self.panels[panelclass]

    def TreeView(self):
        import tree
        tree = tree.TreeViewPanel(self.context)
        tree.show()

    def MessagePanel(self):
        import msgpanel
        self.msgpanel = self.InstantiatePanel(msgpanel.MessagePanel)
        self.msgpanel.connect("destroy",
                              lambda widget, self:
                              setattr(self, "msgpanel", None),
                              self)

_ = Sketch._

AddBuiltin = Sketch.Editor.AddBuiltin
AddEditorBuiltin = Sketch.Editor.AddEditorBuiltin

def file_new(context):
    context.application.NewDocument()

AddBuiltin(file_new, ''"New")
AddBuiltin('file_open', ''"Open",
           lambda context: context.application.load_file_dialog())
AddBuiltin('exit', ''"Exit", lambda context: context.application.Exit())

def create_new_editor(context):
    context.application.CreateDocumentEditor(context.document)

AddEditorBuiltin(create_new_editor, ''"New Editor")

def create_new_view(context):
    context.application.CreateDocumentEditor(editor = context.editor)

AddEditorBuiltin(create_new_view, ''"New View")


AddBuiltin('tree_view', ''"Tree View",
           lambda context: context.application.TreeView())

AddBuiltin('message_panel', ''"Message Panel",
           lambda context: context.application.MessagePanel())

def property_panel(context):
    import properties
    panel = properties.PropertyPanel(context)
    panel.show()

Sketch.Editor.Add(Sketch.Editor.Command('property_panel',
                                        _("Property Panel"), property_panel))


def fill_panel(context):
    import fillpanel
    context.application.InstantiatePanel(fillpanel.FillPanel)

Sketch.Editor.Add(Sketch.Editor.Command('fill_panel',
                                        _("Fill Panel"), fill_panel))


def meta_panel(context):
    import metapanel
    context.application.InstantiatePanel(metapanel.MetaDataPanel)

Sketch.Editor.Add(Sketch.Editor.Command('meta_panel',
                                        _("Meta Panel"), meta_panel))


def palette_panel(context):
    import palette
    context.application.InstantiatePanel(palette.PalettePanel)

Sketch.Editor.Add(Sketch.Editor.Command('palette_panel',
                                        _("Palette Panel"), palette_panel))

def object_panel(context):
    import objectpanel
    context.application.InstantiatePanel(objectpanel.ObjectPanel)

Sketch.Editor.Add(Sketch.Editor.Command('object_panel',
                                        _("Object Panel"), object_panel))

def history_panel(context):
    import historypanel
    context.application.InstantiatePanel(historypanel.HistoryPanel)

Sketch.Editor.Add(Sketch.Editor.Command('history_panel',
                                        _("History Panel"), history_panel))

# import some development commands

import devcmds
