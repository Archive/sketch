# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from types import StringType

import gtk

import Sketch.Editor

import skapp

# Map sketch command names to gtk stock ids. The stock ids will be used
# to create menu items with stock items.
stock_item_map = {
    "file_new": "gtk-new",
    "file_open": "gtk-open",
    "file_save": "gtk-save",
    "file_save_as": "gtk-save-as",
    "exit": "gtk-quit",
    "undo": "gtk-undo",
    "redo": "gtk-redo",
    "delete": "gtk-delete",
    "zoom_100": "gtk-zoom-100",
    "zoom_in": "gtk-zoom-in",
    "zoom_out": "gtk-zoom-out",
}

class SketchMenuItem:

    def __init__(self, command, context, accel, stroke = ''):
        self.command = command
        self.context = context
        self.activate_id = self.connect("activate", self.call_command)
        self.subscribe()
        if stroke:
            key, mods = stroke
            self.add_accelerator("activate", accel, key, mods, 'visible')
        # FIXME: Handle user changes to the accelerator. In PyGTK
        # 0.6/GTK1.2 this was done with these events:
        # self.connect('add-accelerator', self.accel_added)
        # self.connect('remove-accelerator', self.accel_removed)

    def subscribe(self):
        for channel in self.command.channels:
            self.context.Subscribe(channel, self.changed)
        self.context.Subscribe(Sketch.Base.const.CHANGED, self.changed)
        self.changed()

    def changed(self, *args):
        self.set_sensitive(self.command.Sensitive(self.context))
        if self.command.HasDynText():
            text = self.command.DynText(self.context)
            if text:
                self.get_child().set_text(text)
                self.show()
            else:
                self.hide()

    def call_command(self, widget):
        self.command.Execute(self.context)

    def accel_added(self, item, signalid, accel, key, mod, flags):
        skapp.keymap.AddItem(self.command.Name(), (key, mod))

    def accel_removed(self, item, accel, key, mod):
        skapp.keymap.RemoveItem((key, mod))

class SketchMenuCommand(SketchMenuItem, gtk.ImageMenuItem):

    def __init__(self, command, context, accel, stroke = '',
                 stock_id = ""):
        """Initialize the SketchMenuCommand

        If the stock_id parameter is given it should be the gtk stock id
        to use when initializing the GtkImageMenuItem.
        """
        if stock_id:
            gtk.ImageMenuItem.__init__(self, stock_id = stock_id)
        else:
            gtk.ImageMenuItem.__init__(self)
            label = gtk.AccelLabel(command.Title())
            label.set_alignment(0.0, 0.5)
            label.show()
            self.add(label)
        SketchMenuItem.__init__(self, command, context, accel, stroke = stroke)

class SketchMenuCheck(SketchMenuItem, gtk.CheckMenuItem):

    def __init__(self, command, context, accel, stroke = ''):
        gtk.CheckMenuItem.__init__(self, command.Title())
        SketchMenuItem.__init__(self, command, context, accel, stroke)

    def changed(self):
        SketchMenuItem.changed(self)
        checked = self.command.Checked(self.context)
        active = self.active
        if checked and not active or not checked and active:
            self.handler_block(self.activate_id)
            self.set_active(checked)
            self.handler_unblock(self.activate_id)
        
def make_menu(menutree, keymap, context, accel = None):
    if accel is None:
        accel = gtk.AccelGroup()
    stroke = ()
    menu = gtk.Menu()
    menuitem = gtk.TearoffMenuItem()
    menuitem.show()
    menu.append(menuitem)
    last = None
    for item in menutree.Items():
        show = 1
        if item is None:
            # a separator. Only add it if the last one wasn't a
            # separator too
            if last is not None:
                menuitem = gtk.MenuItem()
            else:
                continue
        elif isinstance(item, StringType):
            # a command
            if keymap is not None:
                stroke = keymap.Keystroke(item)
            cmd = Sketch.Editor.Registry.Command(item)
            if cmd is None:
                continue
            if cmd.IsCheckCommand():
                menuitem = SketchMenuCheck(cmd, context, accel, stroke)
            else:
                stock_id = stock_item_map.get(cmd.Name(), "")
                menuitem = SketchMenuCommand(cmd, context, accel, stroke,
                                             stock_id = stock_id)
            if not cmd.DynText(context):
                show = 0
        else:
            # must be a submenu
            menuitem = gtk.MenuItem(item.Title())
            menuitem.set_submenu(make_menu(item, keymap, context, accel))
        last = item
        if show:
            menuitem.show()
        menu.append(menuitem)
    return menu
