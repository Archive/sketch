# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997 -- 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import os, string
import urlparse

import gtk, gtk.gdk

from Sketch import _
from Sketch.Base import Publisher
import Sketch, Sketch.Editor


from Sketch.Base.const import TOOL, COMMAND, CONFIG
OPTIONS = 'OPTIONS'

import menu

from skpanel import SketchPanel

import skpixmaps
pixmaps = skpixmaps.pixmaps


dnd_targets = [
    ('STRING', 0, 0),
    ('text/uri-list', 0, 0),
#    ('application/x-color', 0, 1)
    ]


tools = ['SelectionTool', 'EditTool', 'ZoomTool',
         'CreateRect', 'CreatePoly', 'CreateFreehand',
         'CreateEllipse', 'CreateCurve', 'TextTool']


class ToolButton(Publisher, gtk.ToggleButton):

    def __init__(self, toolname):
        gtk.ToggleButton.__init__(self)
        self.toolname = toolname
        self.clicked_id = self.connect('clicked', self.issue_command)
        self.connect('button_press_event', self.button_press)

    def ToolChanged(self, tool):
        """Tell the button that the active tool has changed

        This method is intended as a subscriber for the context's TOOL
        messages.  Hence the tool parameter should either be a string
        with the name of the current tool in which case the button will
        be set to active if the it's the same as the toolname give to
        the ToolButton constructor, inactive otherwise.

        The tool parameter may also be None, in which case the button
        will be made insensitive.  Otherwise the button will be made
        sensitive again.
        """
        # calling self.set_active will trigger the "clicked" signal.
        # Since the change here is not initiated directly by the user,
        # but instead is done to reflect the state of the application we
        # have to block the clicked signal while we change the button.
        self.handler_block(self.clicked_id)
        try:
            self.set_sensitive(tool is not None)
            self.set_active(self.toolname == tool)
        finally:
            self.handler_unblock(self.clicked_id)

    def issue_command(self, widget):
        self.issue(COMMAND)
        self.ToolChanged(self.toolname)

    def button_press(self, widget, event):
        if event.type == gtk.gdk._2BUTTON_PRESS:
            self.issue(OPTIONS)


class SketchToolbox(Publisher):

    def __init__(self, application, context):
        self.application = application
        self.context = context
        self.file_dialog = None
        self.build_window()
        self.build_menu()
        self.active_tool = None
        self.tool_options = None

    def close(self, *args):
        self.application.Exit()

    def build_window(self):
        self.window = gtk.Window()
        self.window.realize()
        import skpixmaps
        skpixmaps.init_from_widget(self.window)
        import gtkdevice
        gtkdevice.InitFromWidget(self.window)
        self.window.connect("destroy", self.close)

        self.window.drag_dest_set(gtk.DEST_DEFAULT_ALL, dnd_targets,
                                  gtk.gdk.ACTION_COPY | gtk.gdk.ACTION_MOVE)
        self.window.connect('drag_data_received', self.dnd_drop)

        self.vbox = gtk.VBox()
        self.window.add(self.vbox)

        self.menubar = gtk.MenuBar()
        self.menubar.set_name('menubar')
        self.vbox.pack_start(self.menubar, expand = 0)

        self.table = gtk.Table(3, 3)
        self.vbox.pack_start(self.table)

        dirname = os.path.split(__file__)[0]
        group = None
        for i in range(len(tools)):
            x = i % 3; y = i / 3
            toolname = tools[i]
            tool = Sketch.Editor.toolmap[toolname]
            button = ToolButton(toolname)
            button.Subscribe(COMMAND, self.context.SetTool, toolname)
            button.Subscribe(OPTIONS, self.show_tool_options)
            image = gtk.Image()
            image.set_from_pixmap(*getattr(pixmaps, tool.icon))
            button.add(image)
            button.set_mode(0)
            self.context.Subscribe(TOOL, button.ToolChanged)
            button.ToolChanged(self.context.tool)
            self.application.tooltips.set_tip(button, tool.Title())
            self.table.attach(button, x, x + 1, y, y + 1, 0, 0)

        self.window.show_all()

        self.window.set_title("Skencil")

    def build_menu(self):
        menubar = self.menubar
        menuitem = gtk.MenuItem(_("File"))
        menuitem.set_submenu(self.build_file_menu())
        menubar.append(menuitem)
        menuitem.show()
        menuitem = gtk.MenuItem(_("Xtns"))
        menuitem.set_submenu(self.build_win_menu())
        menubar.append(menuitem)
        menuitem.show()

    def build_file_menu(self):
        descr = ['file_new', 'file_open', None]
        for i in range(10):
            descr.append('mrufile%2d' % i)
        descr = descr + [None, 'exit']
        tree = Sketch.Editor.BuildMenuTree('<toolbox>', descr)
        self.file_menu = menu.make_menu(tree, None, self.context)
        self.context.Subscribe(CONFIG, self.config_changed)
        self.config_changed('mru_files', Sketch.UI.preferences.mru_files)
        return self.file_menu

    def build_win_menu(self):
        # XXX the reload panel should not be here in the final version.
        # The reload panel is only interesting for developers and script
        # writers, so it should at least require a config-flag.
        descr = ['message_panel', 'reload_panel',
                 None, 'property_panel', 'fill_panel', 'meta_panel',
                 'palette_panel', 'object_panel', 'history_panel']
        tree = Sketch.Editor.BuildMenuTree('<toolbox>', descr)
        return menu.make_menu(tree, None, self.context)

    def config_changed(self, key, value):
        if key == 'mru_files':
            # Check whether the list of mru files is empty and hide the
            # unnecessary separator.
            #
            # This is a hack to get this effect. Maybe there should be a
            # smart separator class.
            items = self.file_menu.get_children()
            if not value:
                items[-2].hide()
            else:
                items[-2].show()

    def show_tool_options(self):
        if self.tool_options is None:
            self.tool_options = ToolOptions(self.context)
            self.tool_options.connect("destroy", self.tool_options_closed)
            self.tool_options.show()

    def tool_options_closed(self, *args):
        self.tool_options = None

    def dnd_drop(self, w, context, x, y, data, info, time):
        target = str(data.target)
        if target == 'text/uri-list':
            for file in string.split(data.data):
                parsed = urlparse.urlparse(file)
                if parsed[0] == 'file':
                    self.application.LoadDocumentIntoNewEditor(parsed[2])
#          elif target == 'application/x-color':
#              import struct
#              if data.format == 16 and len(data.data) == 8:
#                  r, g, b, a = struct.unpack('HHHH', data.data)
#                  print r / 255, g / 255, b / 255
#          print 'dnd_drop', w, x, y, info, time
#          print '\ncontext members:'
#          for name in context.__members__:
#              print name,
#              try:
#                  print getattr(context, name)
#              except:
#                  print '*ERROR*'
#          print '\ndata members:'
#          for name in data.__members__:
#              print name,
#              try:
#                  print `getattr(data, name)`
#              except:
#                  print '*ERROR*'


class OptionPanel:

    def show(self):
        self.widget.show()

class FreeHandOptions(OptionPanel):

    def __init__(self):
        self.widget = box = gtk.HBox()

        label = gtk.Label(_("Tolerance"))
        box.pack_start(label)
        label.show()

        value = Sketch.Editor.preferences.freehand_accuracy
        self.accuracy = gtk.Adjustment(value = value, lower = 0, upper = 100,
                                       step_incr = 1)
        entry = gtk.SpinButton(adjustment = self.accuracy, digits = 1)
        box.pack_start(entry)
        entry.show()
        self.accuracy.connect("value_changed", self.entry_changed)

    def entry_changed(self, *args):
        Sketch.Editor.preferences.freehand_accuracy = self.accuracy.value

class EmptyOptions(OptionPanel):

    def __init__(self):
        self.widget = gtk.Label(_("This tool has no options"))
        self.widget.set_padding(4, 4)

class ToolOptions(SketchPanel):

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("Tool Options"))
        self.panels = {}
        self.context.Subscribe(Sketch.Base.const.TOOL, self.tool_changed)
        self.tool_changed(context.tool)

    def destroyed(self, *args):
        self.context.Unsubscribe(Sketch.Base.const.TOOL, self.tool_changed)
        apply(SketchPanel.destroyed, (self,) + args)

    def build_window(self):
        vbox = self.vbox

        self.label = gtk.Label("")
        self.label.set_padding(2, 3)
        self.label.show()
        self.vbox.pack_start(self.label)

        sep = gtk.HSeparator()
        sep.show()
        self.vbox.pack_start(sep)

        self.default_panel = EmptyOptions()
        self.default_panel.show()
        self.vbox.pack_start(self.default_panel.widget)
        self.current_panel = self.default_panel

        # buttons
        button = gtk.Button(_("Close"))
        button.connect("clicked", self.close)
        self.action_area.pack_start(button)
        button.show()

    def tool_changed(self, tool):
        title = Sketch.Editor.toolmap[tool].Title()
        self.label.set_text(title)
        panel = self.panels.get(tool)
        if panel is None:
            if tool == 'CreateFreehand':
                panel = FreeHandOptions()
            else:
                panel = self.default_panel
            self.panels[tool] = panel
        if panel is not self.current_panel:
            self.vbox.remove(self.current_panel.widget)
            self.vbox.pack_start(panel.widget)
            self.current_panel = panel
            self.current_panel.show()


def load_mru_file(context, num):
    files = Sketch.UI.preferences.mru_files
    if len(files) > num:
        file = files[num]
        if file:
            doc = context.application.LoadDocumentIntoNewEditor(file)

def mru_text(num):
    files = Sketch.UI.preferences.mru_files
    if len(files) > num:
        return os.path.split(files[num])[1]
    else:
        return ''

for i in range(10):
    cmd = Sketch.Editor.Command('mrufile%2d' % i, "",
                                load_mru_file, args = (i,),
                                dyntext = lambda c, n=i: mru_text(n),
                                channels = (CONFIG,))
    Sketch.Editor.Add(cmd)
