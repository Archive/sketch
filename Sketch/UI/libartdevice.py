# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000, 2003, 2004, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from math import pi
import PIL.Image

from Sketch import Trafo, Translation, Rotation, Scale, \
     CreatePath, Point, IntersectRects, SingularMatrix
import Sketch._sketch
from Sketch.Graphics.device import CommonDevice
from Sketch.Graphics import PropertyStack, StandardColors, EmptyPattern, \
     LineStyle
SolidLine = LineStyle

from Sketch import _libart


class MinimalDevice:

    draw_visible = 0
    draw_printable = 1
    gradient_steps = 100

    def PushTrafo(self):
        pass

    def Concat(self, trafo):
        pass

    def Translate(self, x, y = None):
        pass

    def Rotate(self, angle):
        pass

    def Scale(self, scale):
        pass

    def PopTrafo(self):
        pass

    def PushClip(self):
        pass

    def PopClip(self):
        pass

    def SetFillColor(self, color):
        pass

    def SetLineColor(self, color):
        pass

    def SetFillOpacity(self, opacity):
        pass

    def SetLineOpacity(self, opacity):
        pass

    def SetLineAttributes(self, width, cap = 1, join = 0, dashes = ()):
        pass

    def SetLineSolid(self):
        pass

    def DrawLine(self, start, end):
        pass

    def DrawLineXY(self, x1, y1, x2, y2):
        pass

    def DrawRectangle(self, start, end):
        pass

    def FillRectangle(self, left, bottom, right, top):
        pass

    def DrawCircle(self, center, radius):
        pass

    def FillCircle(self, center, radius):
        pass

    def FillPolygon(self, pts):
        pass

    def DrawBezierPath(self, path, rect = None):
        pass


    def FillBezierPath(self, path, rect = None):
        pass


    def MultiBezier(self, paths, rect = None, clip = 0):
        pass

    def Rectangle(self, trafo, clip = 0):
        pass

    def RoundedRectangle(self, trafo, radius1, radius2, clip = 0):
        pass

    def SimpleEllipse(self, trafo, start_angle, end_angle, arc_type,
                      rect = None, clip = 0):
        pass

    def DrawText(self, text, trafo, clip = 0, cache = None):
        pass

    complex_text = None
    def BeginComplexText(self, clip = 0, cache = None):
        pass

    def DrawComplexText(self, text, trafo, font, font_size):
        pass

    def EndComplexText(self):
        pass

    def DrawImage(self, image, trafo, clip = 0):
        pass

    def DrawEps(self, data, trafo):
        pass

    def DrawGuideLine(self, *args):
        pass

    def SetProperties(self, properties, rect = None):
        pass


    has_axial_gradient = 0
    def AxialGradient(self, gradient, p0, p1):
        pass

    has_radial_gradient = 0
    def RadialGradient(self, gradient, p, r0, r1):
        pass

    has_conical_gradient = 0
    def ConicalGradient(self, gradient, p, angle):
        pass

    def TileImage(self, tile, trafo):
        pass

    #
    #   Outline Mode
    #   This will be ignored in PostScript devices
    #

    def StartOutlineMode(self, *rest):
        pass

    def EndOutlineMode(self, *rest):
        pass

    def IsOutlineActive(self, *rest):
        return 0


class TrafoDevice(MinimalDevice):

    def __init__(self):
        self.trafo_stack = ()
        self.win_to_doc = Trafo()
        self.doc_to_win = Trafo()

    def init_trafo_stack(self):
        self.trafo_stack = ()

    def PushTrafo(self):
        self.trafo_stack = (self.win_to_doc, self.doc_to_win, self.trafo_stack)

    def PopTrafo(self):
        self.win_to_doc, self.doc_to_win, self.trafo_stack = self.trafo_stack

    def Concat(self, trafo):
        self.doc_to_win = self.doc_to_win(trafo)
        try:
            self.win_to_doc = trafo.inverse()(self.win_to_doc)
        except SingularMatrix:
            pass

    def Translate(self, *args):
        self.Concat(apply(Translation, args))

    def Scale(self, factor):
        self.Concat(Scale(factor))

    def Rotate(self, angle):
        self.Concat(Rotation(angle))




circle_path = Sketch._sketch.approx_arc(0, 2 * pi)
rect_path = CreatePath()
rect_path.AppendLine(Point(0, 0))
rect_path.AppendLine(Point(1, 0))
rect_path.AppendLine(Point(1, 1))
rect_path.AppendLine(Point(0, 1))
rect_path.AppendLine(Point(0, 0))
rect_path.ClosePath()

class LibartDevice(TrafoDevice, CommonDevice):

    def __init__(self, visible = 1, printable = 0):
        TrafoDevice.__init__(self)
        self.color = 0xFF
        self.opacity = 1.0
        self.line_width = 1.0
        self.cap = 0
        self.join = 0
        self.dashes = ()
        self.draw_visible = visible
        self.draw_printable = printable
        self.ximage = None
        self.width = self.height = None
        self.image = None
        self.gc = None
        self.drawable = None
        self.properties = PropertyStack()
        self.outline_style = PropertyStack()
        self.outline_style.AddStyle(SolidLine(StandardColors.black))
        self.outline_mode = None
        self.svp_cache = {}

    def drawable_size(self):
        return self.drawable.get_size()

    def InitClip(self):
        self.clip_stack = ()
        self.clip_region = None

    def init_gc(self, window, visual, **gcargs):
        self.window = window
        self.drawable = self.window
        self.visual = visual
        self.gc = apply(window.new_gc, (), gcargs)
        self.InitClip()

    def WindowResized(self, width, height):
        self.ximage = None
        self.image = None
        self.create_image()

    def create_image(self, width = None, height = None):
        if self.drawable is None:
            return
        if width is None:
            width, height = self.drawable_size()
        if self.image is None:
            self.image = _libart.pixbuf_new_rgb(width, height)

    def DeleteImages(self):
        self.image = None
        self.ximage = None

    def PushTrafo(self):
        self.trafo_stack = (self.win_to_doc, self.doc_to_win,
                            self.libart_trafo, self.trafo_stack)

    def PopTrafo(self):
        self.win_to_doc, self.doc_to_win, self.libart_trafo, self.trafo_stack \
                         = self.trafo_stack

    def Concat(self, trafo):
        TrafoDevice.Concat(self, trafo)
        self.libart_trafo = self.libart_trafo(trafo)
        self.libart_matrix = self.doc_to_win.matrix()

    def SetViewportTransform(self, scale, doc_to_win, win_to_doc):
        self.scale = scale
        self.doc_to_win = doc_to_win
        self.win_to_doc = win_to_doc
        self.init_trafo_stack()

    def BeginDraw(self, region = None):
        self.image = None
        dw, dh = self.drawable_size()
        if region is not None:
            rx, ry, w, h = region.ClipBox()
            if rx > dw or rx + w <= 0:
                return 0
            if ry > dh or ry + h <= 0:
                return 0
            if rx < 0:
                w = w + rx
                rx = 0
            if ry < 0:
                h = h + ry
                ry = 0
            if rx + w > dw:
                w = dw - rx - 1
            if ry + h > dh:
                h = dh - ry - 1
            self.create_image(w, h)
        else:
            rx = ry = 0
            w = dw
            h = dh
            self.create_image()
        self.image_rect = rx, ry, w, h
        self.libart_matrix = self.doc_to_win.matrix()
        self.libart_trafo = apply(Trafo, self.libart_matrix)
        x, y = -self.doc_to_win.offset()
        self.libart_offset = int(round(x)) + rx, int(round(y) + ry)
        return 1

    def EndDraw(self, region = None):
        # Import _skgtk only in this method to make the whole module
        # less dependend on gtk.
        # FIXME: It would be better to solve it a bit differently so
        # that the whole module is gtk independent.
        from Sketch import _skgtk
        x, y, w, h = self.image_rect
        if region is not None:
            _skgtk.set_clip_mask(self.gc, region)
        _skgtk.draw_artpixbuf(self.drawable, self.gc, self.image, x, y)
        _skgtk.set_clip_mask(self.gc, None)

    def PushClip(self):
        self.clip_stack = self.clip_region, self.clip_stack

    def PopClip(self):
        self.clip_region, self.clip_stack = self.clip_stack

    def ClipRegion(self, svp):
        # Intersect the current clip region and svp and make the
        # result the new clip region.
        if self.clip_region is not None:
            self.clip_region = _libart.intersect_svp(self.clip_region, svp)
        else:
            self.clip_region = svp

    def convert_color(self, color):
        # Unpack the color components from the RGBColor object.
        red, green, blue = color
        return _libart.convert_color(red, green, blue, self.opacity)

    def _set_color(self, color):
        self.color = self.convert_color(color)

    SetFillColor = _set_color

    def SetLineColor(self, color):
        self.opacity = 1.0
        self._set_color(color)

    def _set_opacity(self, opacity):
        self.opacity = opacity
        self.color = _libart.set_opacity(self.color, opacity)

    SetFillOpacity = _set_opacity
    SetLineOpacity = _set_opacity

    def SetLineAttributes(self, width, cap = 1, join = 0, dashes = ()):
        self.line_width = width
        self.join = join
        self.cap = cap - 1
        self.dashes = dashes

    def SetLineSolid(self):
        self.dashes = ()

    def DrawRectangle(self, start, end):
        left, bottom = start
        right, top = end
        trafo = self.libart_trafo(Trafo(right - left, 0, 0, top-bottom,
                                        left, bottom))
        svp = _libart.svp_from_curve_stroke((rect_path,), trafo, self.scale,
                                            self.line_width,
                                            self.join, self.cap, self.dashes)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, self.color,
                               self.libart_offset)

    def FillRectangle(self, left, bottom, right, top):
        trafo = self.libart_trafo(Trafo(right - left, 0, 0, top-bottom,
                                        left, bottom))
        svp = _libart.svp_from_curve((rect_path,), trafo,
                                     _libart.ART_WIND_RULE_ODDEVEN)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, self.color,
                               self.libart_offset)

    def FillCircle(self, center, radius):
        trafo = self.libart_trafo(Trafo(radius, 0, 0, radius,
                                        center.x, center.y))
        svp = _libart.svp_from_curve((circle_path,), trafo,
                                     _libart.ART_WIND_RULE_ODDEVEN)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, self.color,
                               self.libart_offset)

    def FillPolygon(self, pts):
        path = CreatePath()
        for p in pts:
            path.AppendLine(p)
        path.AppendLine(pts[0])
        path.ClosePath()
        svp = _libart.svp_from_curve((path,), self.libart_trafo,
                                     _libart.ART_WIND_RULE_ODDEVEN)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, self.color,
                               self.libart_offset)

    def DrawLine(self, start, end):
        self.DrawLineXY(start.x, start.y, end.x, end.y)

    def DrawLineXY(self, x1, y1, x2, y2):
        start = self.libart_trafo(x1, y1)
        end = self.libart_trafo(x2, y2)
        width = self.scale * self.line_width
        svp = _libart.svp_from_line(start, end, width, self.cap, self.join)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, self.color,
                               self.libart_offset)

    def DrawBezierPath(self, path, rect = None):
        stroke_svp = _libart.svp_from_curve_stroke((path,),
                                                   self.libart_trafo,
                                                   self.scale,
                                                   self.line_width,
                                                   self.join, self.cap,
                                                   self.dashes)
        if self.clip_region is not None:
            stroke_svp = _libart.intersect_svp(self.clip_region, stroke_svp)
        _libart.render_svp_rgb(self.image, stroke_svp, self.color,
                               self.libart_offset)



    def FillBezierPath(self, path, rect = None):
        fill_svp = _libart.svp_from_curve((path,), self.libart_trafo,
                                          _libart.ART_WIND_RULE_ODDEVEN)
        if self.clip_region is not None:
            fill_svp = _libart.intersect_svp(self.clip_region, fill_svp)
        _libart.render_svp_rgb(self.image, fill_svp, self.color,
                               self.libart_offset)


    #
    #
    #

    def set_properties(self, properties):
        self.properties = properties
        if properties.HasFill():
            self.fill = 1
            self.proc_fill = properties.IsAlgorithmicFill()
        else:
            self.fill = 0
            self.proc_fill = 0
        if properties.HasLine():
            self.line = 1
            self.proc_line = properties.IsAlgorithmicLine()
            self.line_width = properties.line_width
            self.cap = properties.line_cap - 1
            self.join = properties.line_join
            self.dashes = properties.line_dashes
        else:
            self.line = 0
            self.proc_line = 0

    def SetProperties(self, properties, rect = None):
        if self.outline_mode is None:
            self.fill_rect = rect
            self.set_properties(properties)
        else:
            # if outline, set only font properties
            self.properties.SetProperty(font = properties.font,
                                        font_size = properties.font_size)


    def StartOutlineMode(self, color = None):
        self.outline_mode = (self.properties, self.outline_mode)
        if color is None:
            properties = self.outline_style
        else:
            properties = PropertyStack()
            properties.SetProperty(fill_pattern = EmptyPattern)
            properties.AddStyle(SolidLine(color))
        self.set_properties(properties)

    def EndOutlineMode(self):
        if self.outline_mode:
            properties, self.outline_mode = self.outline_mode
            self.set_properties(properties)

    def IsOutlineActive(self):
        return self.outline_mode


    #
    #   Patterns
    #
    def get_pattern_image(self):
        width, height = self.drawable_size()
        winrect = self.libart_trafo(self.fill_rect)
        left, top, right, bottom = map(int, map(round, winrect))
        x0, y0 = self.libart_offset
        l = max(left, x0);      r = min(right, x0 + width);
        t = max(top, y0);       b = min(bottom, y0 + height);
        if self.clip_region is not None:
            cx, cy, cw, ch = self.clip_region.bounding_rect()
            l = max(l, cx);     r = min(r, cw)
            t = max(t, cy);     b = min(b, ch)
        if l >= r or t >= b:
            return None, None, None
        image = PIL.Image.new('RGB', (r - l, b - t), (255, 255, 255))
        trafo = Translation(-l, -t)(self.libart_trafo)
        return image, trafo, (l, t)

    def draw_pattern_image(self, image, pos):
        x, y = pos
        w, h = image.size
        if self.clip_region is not None:
            _libart.render_svp_scaled_image(self.image, self.clip_region,
                                            image.im, x, y, w, h,
                                            self.libart_offset)

    has_axial_gradient = 1
    def AxialGradient(self, gradient, p0, p1):
        # p0 and p1 may be PointSpecs
        image, trafo, pos = self.get_pattern_image()
        if image is None:
            return
        x0, y0 = trafo(p0)
        x1, y1 = trafo(p1)
        Sketch._sketch.fill_axial_gradient(image.im, gradient.Colors(),
                                           x0,y0, x1,y1)
        self.draw_pattern_image(image, pos)

    has_radial_gradient = 1
    def RadialGradient(self, gradient, p, r0, r1):
        # p may be PointSpec
        image, trafo, pos = self.get_pattern_image()
        if image is None:
            return
        x, y = trafo.DocToWin(p)
        r0 = int(round(abs(trafo.DTransform(r0, 0))))
        r1 = int(round(abs(trafo.DTransform(r1, 0))))
        Sketch._sketch.fill_radial_gradient(image.im, gradient.Colors(),
                                            x, y, r0, r1)
        self.draw_pattern_image(image, pos)


    has_conical_gradient = 1
    def ConicalGradient(self, gradient, p, angle):
        # p may be PointSpec
        image, trafo, pos = self.get_pattern_image()
        if image is None:
            return
        cx, cy = trafo.DocToWin(p)
        Sketch._sketch.fill_conical_gradient(image.im, gradient.Colors(),
                                             cx, cy, -angle)
        self.draw_pattern_image(image, pos)

    def TileImage(self, tile, trafo):
        image, temp_trafo, pos = self.get_pattern_image()
        if image is None:
            return
        trafo = temp_trafo(trafo)
        try:
            Sketch._sketch.fill_transformed_tile(image.im, tile.im,
                                                 trafo.inverse())
            self.draw_pattern_image(image, pos)
        except SingularMatrix:
            pass


    #
    #   complex objects
    #
    def MultiBezier(self, paths, rect = None, clip = 0):
        if self.fill or clip:
            fill_svp = _libart.svp_from_curve(paths, self.libart_trafo,
                                              _libart.ART_WIND_RULE_ODDEVEN)
            if self.proc_fill:
                if not clip:
                    self.PushClip()
                self.ClipRegion(fill_svp)
                self.properties.ExecuteFill(self, self.fill_rect)
                if not clip:
                    self.PopClip()
                clip = 0
            elif self.fill:
                if self.clip_region is not None:
                    fill_svp = _libart.intersect_svp(self.clip_region,fill_svp)
                self.properties.ExecuteFill(self)
                _libart.render_svp_rgb(self.image, fill_svp, self.color,
                                       self.libart_offset)
        if self.line:
            if not self.proc_line:
                self.properties.ExecuteLine(self)
            stroke_svp = _libart.svp_from_curve_stroke(paths,
                                                       self.libart_trafo,
                                                       self.scale,
                                                       self.line_width,
                                                       self.join, self.cap,
                                                       self.dashes)
            if self.clip_region is not None:
                stroke_svp = _libart.intersect_svp(self.clip_region,stroke_svp)
            _libart.render_svp_rgb(self.image, stroke_svp, self.color,
                                   self.libart_offset)
        if clip:
            self.ClipRegion(fill_svp)

    def SimpleEllipse(self, trafo, start_angle, end_angle, arc_type,
                      rect = None, clip = 0):
        trafo2 = self.libart_trafo(trafo)

        if start_angle != end_angle:
            arc = Sketch._sketch.approx_arc(start_angle, end_angle, arc_type)
        else:
            arc = circle_path
        paths = (arc,)
        if self.fill or clip:
            fill_svp = _libart.svp_from_curve(paths, trafo2)
            if self.clip_region is not None:
                fill_svp = _libart.intersect_svp(self.clip_region, fill_svp)
            if self.fill and not self.proc_fill:
                self.properties.ExecuteFill(self)
            _libart.render_svp_rgb(self.image, fill_svp, self.color,
                                   self.libart_offset)
        if self.line:
            if not self.proc_line:
                self.properties.ExecuteLine(self)
            stroke_svp = _libart.svp_from_curve_stroke(paths, trafo2,
                                                       self.scale,
                                                       self.line_width,
                                                       self.join, self.cap,
                                                       self.dashes)
            if self.clip_region is not None:
                stroke_svp = _libart.intersect_svp(self.clip_region,stroke_svp)
            _libart.render_svp_rgb(self.image, stroke_svp, self.color,
                                   self.libart_offset)
        if clip:
            self.ClipRegion(fill_svp)

    def DrawGuideLine(self, point, horizontal):
        x, y = self.libart_trafo.DocToWin(point)
        self.properties.ExecuteLine(self)
        if horizontal:
            coord = y
        else:
            coord = x
        _libart.render_guide_line(self.image, coord, horizontal, self.color,
                                  self.libart_offset)

    def DrawPageOutline(self, width, height):
        # Draw the outline of the page whose size is given by width and
        # height. The page's lower left corner is at (0,0) and its upper
        # right corner at (width, height) in doc coords. The outline is
        # drawn as a rectangle with a thin shadow.
        color = self.convert_color(StandardColors.gray)
        sw = 5  # shadow width  XXX: should be configurable ?
        left, bottom = self.libart_trafo(0, 0)
        right, top = self.libart_trafo(width, height)
        svp = _libart.svp_from_filled_rectangle(left + sw, bottom,
                                                right + sw, bottom + sw)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, color, self.libart_offset)
        svp = _libart.svp_from_filled_rectangle(right, top + sw,
                                                right + sw, bottom + sw)
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp, color, self.libart_offset)


        trafo = self.libart_trafo(Trafo(width, 0, 0, height, 0, 0))
        svp = _libart.svp_from_curve_stroke((rect_path,), trafo, 1.0, 1.0,
                                            _libart.ART_PATH_STROKE_JOIN_MITER,
                                            _libart.ART_PATH_STROKE_CAP_BUTT,
                                            ())
        if self.clip_region is not None:
            svp = _libart.intersect_svp(self.clip_region, svp)
        _libart.render_svp_rgb(self.image, svp,
                               self.convert_color(StandardColors.black),
                               self.libart_offset)

    def DrawDocument(self, document, rect, clip_region = None,
                     draw_page_outline = 1):
        overlaps = rect.overlaps
        if self.BeginDraw(clip_region):
            if draw_page_outline:
                w, h = document.PageSize()
                self.DrawPageOutline(w, h)
            for layer in document.Layers():
                if (self.draw_visible and layer.Visible()
                    or self.draw_printable and layer.Printable()):
                    if overlaps(layer.bounding_rect):
                        if layer.Outlined() or self.IsOutlineActive():
                            self.StartOutlineMode(layer.OutlineColor())
                        if layer.is_GridLayer:
                            xorg, yorg, xwidth, ywidth = layer.Geometry()
                            self.DrawGrid(xorg, yorg, xwidth, ywidth)
                        else:
                            self.DrawObjects(layer.GetObjects(), rect,
                                             overlaps)
                        if layer.Outlined() or self.IsOutlineActive():
                            self.EndOutlineMode()

            self.EndDraw(clip_region)

    def DrawObjects(self, objects, rect, overlaps):
        for obj in objects:
            if overlaps(obj.bounding_rect):
                if obj.is_MaskGroup:
                    self.draw_mask_group(obj, rect, overlaps)
                elif obj.is_Compound:
                    self.DrawObjects(obj.GetObjects(), rect, overlaps)
                elif obj.is_curve or obj.is_Text:
                    # test is_Text separately because InternalPathText
                    # doesn't have is_curve set yet.
                    self.SetProperties(obj.Properties(), obj.bounding_rect)
                    old_line = self.line
                    if obj.is_Text and not self.IsOutlineActive():
                        self.line = 0
                    self.draw_curve(obj, rect)
                    self.line = old_line
                elif obj.is_Image:
                    self.draw_image(obj.Data(), obj.Trafo())
                elif obj.is_Eps:
                    image = obj.Preview()
                    if image is not None:
                        self.draw_image(image, obj.Trafo())
                elif obj.is_GuideLine:
                    self.DrawGuideLine(obj.point, obj.Horizontal())

    def get_svps(self, obj, clip = 0):
        cache = self.svp_cache
        key = id(obj)
        use_cached = 0
        matrix = self.libart_matrix
        trafo = self.libart_trafo
        if cache.has_key(key):
            m, om, fill_svp, stroke_svp, paths = cache[key]
            if m == matrix and om == (self.outline_mode is None):
                use_cached = 1

        if not use_cached:
            paths = obj.Paths()
            if self.fill or clip:
                fill_svp = _libart.svp_from_curve(paths, trafo,
                                               _libart.ART_WIND_RULE_ODDEVEN)
            else:
                fill_svp = None
            if self.line:
                stroke_svp = _libart.svp_from_curve_stroke(paths, trafo,
                                                           self.scale,
                                                           self.line_width,
                                                           self.join, self.cap,
                                                           self.dashes)
            else:
                stroke_svp = None
        cache[key] = (matrix, self.outline_mode is None,
                      fill_svp, stroke_svp, paths)
        return fill_svp, stroke_svp, paths

    def objects_changed(self, objects):
        # Called with a list of objects that have changed or that have
        # been removed to clear the cached information.
        cache = self.svp_cache
        for i in map(id, objects):
            try:
                del cache[i]
            except KeyError:
                pass

    def draw_curve(self, obj, rect):
        fill_svp, stroke_svp, paths = self.get_svps(obj)
        off = self.libart_offset
        if fill_svp:
            if self.proc_fill:
                self.PushClip()
                self.ClipRegion(fill_svp)
                self.properties.ExecuteFill(self, self.fill_rect)
                self.PopClip()
            elif self.fill:
                if self.clip_region is not None:
                    fill_svp = _libart.intersect_svp(self.clip_region,fill_svp)
                self.properties.ExecuteFill(self)
                _libart.render_svp_rgb(self.image, fill_svp, self.color,off)
        if stroke_svp:
            if not self.proc_line:
                self.properties.ExecuteLine(self)
            if self.clip_region is not None:
                stroke_svp = _libart.intersect_svp(self.clip_region,stroke_svp)
            _libart.render_svp_rgb(self.image, stroke_svp, self.color, off)
            self.draw_arrows(paths, rect)

    def draw_image(self, image, trafo, clip = 0):
        if image.mode not in ('RGB', 'RGBA'):
            return
        w, h = image.size
        #if self.IsOutlineActive():
        #    self.PushTrafo()
        #    self.Concat(trafo)
        #    self.DrawRectangle(Point(0, 0), Point(w, h))
        #    self.PopTrafo()
        #    return
        dtw = self.libart_trafo.DocToWin
        llx, lly = dtw(trafo.offset())
        lrx, lry = dtw(trafo(w, 0))
        ulx, uly = dtw(trafo(0, h))
        urx, ury = dtw(trafo(w, h))
        if llx == ulx and lly == lry:
            if llx < lrx:
                sx = llx;       w = lrx - llx + 1
            else:
                sx = lrx;       w = lrx - llx - 1
            if uly < lly:
                sy = uly;       h = lly - uly + 1
            else:
                sy = lly;       h = lly - uly - 1

            #if self.clip_region is not None:
            _libart.render_svp_scaled_image(self.image, self.clip_region,
                                            image.im, sx, sy, w, h,
                                            self.libart_offset)
            #if w < 0:  w = -w
            #if h < 0:  h = -h
            #if not clip:
        #       self.PushClip()
        #    self.ClipRect((sx, sy, w, h))
        else:
            #self.PushTrafo()
            #self.Concat(trafo)
            #self.Concat(Trafo(1, 0, 0, -1, 0, h))
            t = self.libart_trafo
            t = t(trafo)
            t = t(Trafo(1, 0, 0, -1, 0, h))
            dtw = t.DocToWin
            inverse = t.inverse()
            ulx, uly = dtw(0, 0)
            urx, ury = dtw(0, h)
            llx, lly = dtw(w, 0)
            lrx, lry = dtw(w, h)
            #self.PopTrafo()
            sx = min(ulx, llx, urx, lrx)
            ex = max(ulx, llx, urx, lrx)
            sy = min(uly, lly, ury, lry)
            ey = max(uly, lly, ury, lry)
            if self.clip_region is not None:
                cx, cy, cex, cey = self.clip_region.bounding_rect()
                if cx >= ex or cex <= sx or cy >= ey or cey <= sy:
                    return
                if cx  > sx and cx  < ex:       sx = cx
                if cex < ex and cex > sx:       ex = cex
                if cy  > sy and cy  < ey:       sy = cy
                if cey < ey and cey > sy:       ey = cey
            w = ex - sx
            h = ey - sy

            _libart.render_svp_transformed_image(self.image, self.clip_region,
                                                 image.im, inverse, sx, sy,
                                                 w, h, self.libart_offset)
            #if not clip:
        #       self.PushClip()
        #    self.ClipRegion(region)


    def draw_mask_group(self, obj, rect, overlaps):
        objects = obj.GetObjects()
        mask = objects[0]
        if mask.has_properties:
            properties = mask.properties
        else:
            properties = None
        stroke_svp = None
        self.PushClip()
        try:
            # fill mask
            if mask.is_curve:
                self.SetProperties(properties, mask.bounding_rect)
                fill_svp, stroke_svp, paths = self.get_svps(mask, clip = 1)
                self.ClipRegion(fill_svp)
                if self.proc_fill:
                    self.properties.ExecuteFill(self, self.fill_rect)
                elif self.fill:
                    self.properties.ExecuteFill(self)
                    _libart.render_svp_rgb(self.image, self.clip_region,
                                           self.color, self.libart_offset)
            if rect:
                rect = IntersectRects(rect, mask.bounding_rect)
                overlaps = rect.overlaps
            self.DrawObjects(objects[1:], rect, overlaps)
        finally:
            self.PopClip()
        if stroke_svp is not None:
            self.SetProperties(properties, mask.bounding_rect)
            if not self.proc_line:
                self.properties.ExecuteLine(self)
            if self.clip_region is not None:
                stroke_svp = _libart.intersect_svp(self.clip_region,stroke_svp)
            _libart.render_svp_rgb(self.image, stroke_svp, self.color,
                                   self.libart_offset)
            self.draw_arrows(paths, rect)


    def DrawGrid(self, orig_x, orig_y, xwidth, ywidth):
        # Draw a grid with a horizontal width XWIDTH and vertical width
        # YWIDTH whose origin is at (ORIG_X, ORIG_Y). RECT gives the
        # region of the document for which the grid has to be drawn.
        # Note: This functions assumes that doc_to_win is
        # in its initial state
        xwinwidth = xwidth * self.scale
        if not xwinwidth:
            return
        ywinwidth = ywidth * self.scale
        if not ywinwidth:
            return
        # make the minimum distance between drawn points at least 5
        # pixels XXX: should be configurable
        if xwinwidth < 5:
            xwinwidth = (int(5.0 / xwinwidth) + 1) * xwinwidth
            xwidth = xwinwidth / self.scale
        if ywinwidth < 5:
            ywinwidth = (int(5.0 / ywinwidth) + 1) * ywinwidth
            ywidth = ywinwidth / self.scale
        winx, winy = self.libart_trafo((orig_x, orig_y))
        self.properties.ExecuteLine(self)
        _libart.render_grid(self.image, winx, winy, xwinwidth, ywinwidth,
                            self.color, self.libart_offset)

    def export_image(self, filename):
        """Write the image the device renders to into a PPM file.

        This method exists purely for debug purposes.
        """
        # we use a special function so that we can easily handle both
        # PIL images and libart pixbufs
        width, height, image = _libart.export_pixbuf(self.image)

        # Write the image to the file specified.
        try:
            file = open(filename, "wb")
        except IOError:
            return

        # Write the file header.
        file.write("P6 %i %i 255\n" % (width, height))

        # Write the image.
        file.write(image)

        # Close the file.
        file.close()
