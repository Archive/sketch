# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import Sketch, Sketch.Editor, skapp

add = Sketch.Editor.Add
cmd = Sketch.Editor.AdvancedScript

def add_arrow_retract(context):
    if context.editor.HasSelection():
        object = context.editor.CurrentObject()
        if object.has_line:
            arrow = Sketch.Graphics.Arrow([(0, 0), (-6, 2), (-6, -2), (0, 0)],
                                          1, 2.5)
            context.document.AddUndo(object.SetProperties(line_arrow1 = arrow,
                                                          line_arrow2 = arrow,
                                                          line_width = 4.0))

def add_arrow_hotspot(context):
    if context.editor.HasSelection():
        object = context.editor.CurrentObject()
        if object.has_line:
            arrow = Sketch.Graphics.Arrow([(-0.5, 1.5), (10, 0), (-0.5, -1.5),
                                           (-0.5, 1.5)], 1,
                                          tip = Sketch.Point(10, 0))
            context.document.AddUndo(object.SetProperties(line_arrow1 = arrow,
                                                          line_arrow2 = arrow,
                                                          line_width = 4.0))

add(cmd("add_arrow_retract", "Add Arrow (Retract)", add_arrow_retract))
skapp.main_menu.AddItem(("Devel",), "add_arrow_retract")
add(cmd("add_arrow_hotspot", "Add Arrow (Hotspot)", add_arrow_hotspot))
skapp.main_menu.AddItem(("Devel",), "add_arrow_hotspot")

def reload_panel(context):
    import reload
    context.application.InstantiatePanel(reload.ReloadPanel)

add(Sketch.Editor.Command("reload_panel", "Reload Panel", reload_panel))
skapp.main_menu.AddItem(("Devel",), "reload_panel")
