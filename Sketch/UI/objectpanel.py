# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000, 2002, 2003, 2005 by Bernhard Herzog
#
# Object Properties Panel and Line Style Dialog
# Copyright (C) 2001 by Mark Rose <mrose@stm.lbl.gov>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

__version__ = "$Revision$"

# DESIGN NOTES AND ISSUES
#
# "Set default" only changes the default graphics style, not the
# default text style.  In 0.6 a popup dialog let the user pick which
# would be affected.  The same could be done here, if desired, or
# perhaps there should be separate menu choices for each.
#
# "Update from..." remains unimplemented because 0.7 lacks the
# infrastructure needed to pick an object.  Once a means of allowing
# the user to designate a source object is implemented, the rest
# is easy.
#
# The dialog panels do a bit of resizing when first opened that
# could perhaps be reduced or eliminated with more careful gtk
# code.
#
# Help needs to be implemented
#

from math import log10

import gtk
import gobject

import Sketch
import Sketch.Graphics
from Sketch import _, Point, CreatePath
from Sketch._sketch import ContAngle
import Sketch.Lib.units
from Sketch.Base import Publisher
from Sketch.Base.const import CHANGED, SELECTION, STYLE, \
     TRANSACTION_END, CapButt, CapRound, CapProjecting, \
     JoinMiter, JoinRound, JoinBevel
from Sketch.Graphics import StandardColors, PropertyStack,\
     EmptyPattern, SolidPattern, CreateRGBColor, StandardDashes, LineStyle
from Sketch.Graphics.arrow import StandardArrows
from Sketch.Graphics.properties import set_graphics_defaults, \
     DefaultGraphicsProperties, property_names

from gtkmisc import ColorButton, CommonComboBox
from skpanel import SketchPanel
from scratchpad import ScratchPad
import skpixmaps

pixmaps = skpixmaps.pixmaps
black = StandardColors.black


# find properties common to several PropertyStacks
def common_properties(plist):
    """Return the properties all PropertyStacks in plist have in common.

    The return value is a dict mapping property names to their values.
    """
    common = {}
    conflicts = {}
    for p in plist:
        for property in property_names:
            if hasattr(p, property):
                value = getattr(p, property)
                if common.has_key(property):
                    if value != common[property]:
                        conflicts[property] = 1
                else:
                    common[property] = value
    for conflict in conflicts.keys():
        del common[conflict]
    return common

# construct a PropertyStack with properties common to several objects
def common_object_properties(objects):
    plist = [obj.Properties() for obj in objects if obj.has_properties]
    common = common_properties(plist)
    pstack = DefaultGraphicsProperties() # XXX or text properties?
    apply(pstack.SetProperty, (), common)
    return pstack




class PixmapMenu(CommonComboBox):

    """ComboBox displaying Pixmaps"""

    def __init__(self, values):
        """Initialize the PixmapMenu

        The parameter should be a sequence of (PIXMAP_NAME, VALUE) pairs
        where PIXMAP_NAME is the name of a pixmap to look up in
        skpixmaps.pixmaps and VALUE is the python object associated with
        it and to be returned by e.g. GetOption.
        """
        self.store = gtk.ListStore(gtk.gdk.Pixbuf,
                                   gobject.TYPE_PYOBJECT)
        for pixmap_name, value in values:
            pixbuf = skpixmaps.load_pixbuf(pixmap_name)
            self.store.append((pixbuf, value))

        CommonComboBox.__init__(self, self.store, value_column = 1)

        cell = gtk.CellRendererPixbuf()
        self.pack_start(cell)
        self.set_attributes(cell, pixbuf = 0)


class UnitMenu(CommonComboBox):

    """ComboBox letting the user select a unit.

    The units are taken from Sketch.Lib.units.unit_dict.
    """

    def __init__(self):
        self.store = gtk.ListStore(gobject.TYPE_STRING,
                                   gobject.TYPE_DOUBLE)
        units = Sketch.Lib.units.unit_dict.items()
        units.sort()
        for name, factor in units:
            self.store.append((name, factor))

        CommonComboBox.__init__(self, self.store, default_value = None,
                                value_column = 1)
        cell = gtk.CellRendererText()
        self.pack_start(cell)
        self.set_attributes(cell, text = 0)

        self.set_active(3)

    def GetOption(self):
        """Override the inherited method to return a pair (name, scale)"""
        idx = self.get_active()
        if idx >= 0:
            itr = self.store.get_iter((idx,))
            return self.store.get(itr, 0, 1)
        # explicitly return None when no value has been selected yet.
        return None



def set_width_and_dashes(gc, width, dashes):
    gc.line_width = int(round(width))
    if dashes:
        # scale the dash pattern by width
        dashes = [int(round(dash * width)) for dash in dashes]

        # Adjust for limitations in X.  An element in a dash pattern
        # must be >=1 and <=255.
        for idx in range(len(dashes)):
            length = dashes[idx]
            if length <= 0:
                dashes[idx] = 1
            elif length > 255:
                dashes[idx] = 255

        # assign the dash pattern to the gc
        gc.line_style = gtk.gdk.LINE_ON_OFF_DASH
        gc.set_dashes(0, dashes) # offset 0
    else:
        # no dashes, so a simple solid line
        gc.line_style = gtk.gdk.LINE_SOLID


class LineCellRenderer(gtk.GenericCellRenderer):

    """Cell renderer showing a line sample with a certain width and dashes

    The renderer has two properties, line_width and line_dashes.
    line_width must be a float (gobject.TYPE_DOUBLE in the model) and
    line_dashes a python object (gobject.TYPE_PYOBJECT in the model)
    which must be a valid Skencil dash pattern.  No dashes, i.e. the
    empty tuple, is the default.
    """

    __gproperties__ = {
        'line_width': (gobject.TYPE_DOUBLE, 'line width',
                       'line width of drawn sample', 0.0, 1e6, 3.0,
                       gobject.PARAM_READWRITE),
        'line_dashes': (gobject.TYPE_PYOBJECT, 'line dashes',
                       'line dashes of drawn sample',
                        gobject.PARAM_READWRITE),
        }

    def __init__(self):
        self.__gobject_init__()
        self.line_width = 3.0
        self.line_dashes = ()

    def do_set_property(self, pspec, value):
        if pspec.name == 'line-width':
            self.line_width = value
        elif pspec.name == 'line-dashes':
            self.line_dashes = value
        else:
            raise AttributeError, 'unknown property %s' % pspec.name

    def do_get_property(self, pspec):
        if pspec.name == 'line-width':
            return self.line_width
        elif pspec.name == 'line-dashes':
            return self.line_dashes
        else:
            raise AttributeError, 'unknown property %s' % pspec.name

    def on_get_size(self, widget, cell_area):
        return (0, 0, 100, 16)

    def on_render(self, window, widget, background_area, cell_area,
                  expose_area, flags):
        gc = window.new_gc()
        if self.line_dashes:
            set_width_and_dashes(gc, self.line_width, self.line_dashes)
        else:
            gc.line_width = int(round(self.line_width))
        y = cell_area.y + cell_area.height / 2
        window.draw_line(gc, cell_area.x, y, cell_area.x + cell_area.width, y)



gobject.type_register(LineCellRenderer)


class LineDashesMenu(CommonComboBox):

    """LineDashes Menu

    The values are taken from StandardDashes()
    """

    def __init__(self):
        self.store = gtk.ListStore(gobject.TYPE_PYOBJECT)
        for dashes in StandardDashes():
            self.store.append((dashes,))
        CommonComboBox.__init__(self, self.store, default_value = ())

        cell = LineCellRenderer()
        self.pack_start(cell)
        self.set_attributes(cell, line_dashes=0)
        self.set_active(0)



class LineWidthMenu(CommonComboBox):

    """Line Width Menu

    Values are taken from and saved in Sketch.UI.preferences.
    Values are stored internally in points (pts), but the unit of display
    can be set with SetUnits().
    New widths can be added with AddWidth(), which will remove the oldest
    width if the menu grows too big (size set in Sketch.UI.preferences).
    """

    def __init__(self):
        self.scale = 1.0
        self.digits = 1
        self.store = gtk.ListStore(gobject.TYPE_DOUBLE)
        CommonComboBox.__init__(self, self.store, default_value = 1)

        cell = gtk.CellRendererText()
        self.pack_start(cell)
        self.set_cell_data_func(cell, self.line_width_data_func, None)

        cell = LineCellRenderer()
        self.pack_start(cell)
        self.set_attributes(cell, line_width=0)

        values = Sketch.UI.preferences.user_line_widths
        limit = Sketch.UI.preferences.user_line_widths_length
        if len(values) > limit:
            values = values[:limit]
        values.sort()
        self.update(values)

        self.set_active(0)

    def line_width_data_func(self, celllayout, cell, model, itr, user_data):
        """Set the 'text' property of the cell renderer cell to the line widht
        """
        format = "%%0.%df" % self.digits
        line_width = self.store.get(itr, 0)[0]
        cell.set_property("text", format % (line_width / self.scale,))

    def update(self, values):
        values = list(values)
        values.sort()
        self.store.clear()
        for width in values:
            self.store.append((width,))

    def SetUnits(self, scale, digits):
        update = self.scale != scale or self.digits != digits
        self.scale = scale
        self.digits = digits
        if update:
            # make sure the widget is redrawn if necessary
            self.queue_draw()

    def AddWidth(self, width):
        """Add width to the user's line widths and update the combo box

        The user's line widths are kept in the list
        Sketch.UI.preferences.user_line_widths.  The new widths is added
        to that list at the front, removing any later occurrence.
        """
        user_line_widths = list(Sketch.UI.preferences.user_line_widths)
        # remove any occurrence of width
        user_line_widths = [w for w in user_line_widths if w != width]
        # add width to the front
        user_line_widths.insert(0, width)

        # remove surplus line widths and update the preference
        del user_line_widths[Sketch.UI.preferences.user_line_widths_length:]
        Sketch.UI.preferences.user_line_widths = user_line_widths

        # update the combo box and select the new width
        self.update(user_line_widths)
        self.SetOption(width)


class ArrowCellRenderer(gtk.GenericCellRenderer):

    """Cell renderer showing a Skencil arrow head

    The property arrow should be set to the Skencil arrow to display.
    """

    __gproperties__ = {
        'arrow': (gobject.TYPE_PYOBJECT, 'arrow head',
                  'arrow head specification', gobject.PARAM_READWRITE),
        }

    def __init__(self, direction):
        self.__gobject_init__()
        self.arrow = None
        self.direction = direction

    def do_set_property(self, pspec, value):
        if pspec.name == 'arrow':
            self.arrow = value
        else:
            raise AttributeError, 'unknown property %s' % pspec.name

    def do_get_property(self, pspec):
        if pspec.name == 'arrow':
            return self.arrow
        else:
            raise AttributeError, 'unknown property %s' % pspec.name

    def on_get_size(self, widget, cell_area):
        return (0, 0, 64, 32)

    def on_render(self, window, widget, background_area, cell_area,
                  expose_area, flags):
        width = cell_area.width
        height = cell_area.height

        arrow_props = PropertyStack()
        arrow_props.AddStyle(LineStyle(black, 1))
        origin_props = PropertyStack()
        origin_props.AddStyle(LineStyle(CreateRGBColor(0.1, 0.1, 0.1), 1,
                                        dashes = (2, 2)))
        origin_path = CreatePath()
        origin_path.AppendLine(Point(0, -height/2), ContAngle)
        origin_path.AppendLine(Point(0, height/2), ContAngle)

        pad = ScratchPad(window, width, height, 1.0, libart = 1)
        pad.BeginDraw()
        pad.DrawRectangle(-width/2, -height/2, width/2, height/2)
        pad.SetProperties(origin_props)
        pad.MultiBezier((origin_path,))
        pad.SetProperties(arrow_props)
        pad.PushTrafo()
        pad.Scale(2)
        if not self.direction:
            pad.Scale(-1)
        if self.arrow is not None:
            retract = self.arrow.retract
            self.arrow.Draw(pad)
        else:
            retract = 0
        path = CreatePath()
        path.AppendLine(Point(-width/2, 0), ContAngle)
        path.AppendLine(Point(-retract, 0), ContAngle)
        pad.MultiBezier((path,))
        pad.PopTrafo()
        pad.EndDraw()

        x = cell_area.x + cell_area.width / 2
        y = cell_area.y + cell_area.height / 2
        gc = window.new_gc()
        window.draw_drawable(gc, pad.GetPixmap(), 0, 0,
                             cell_area.x, cell_area.y, -1, -1)

gobject.type_register(ArrowCellRenderer)


class ArrowMenu(CommonComboBox):

    """ComboBox for Arrow Heads.

    The arrows shown are taken from StandardArrows()
    """

    def __init__(self, direction):
        """Initialize the arrow menu.

        If direction is 1 the arrow points to the right, if it's 0 to
        the left.
        """
        self.store = gtk.ListStore(gobject.TYPE_PYOBJECT)

        self.store.append((None,))
        for arrow in StandardArrows():
            self.store.append((arrow,))
        CommonComboBox.__init__(self, self.store)

        cell = ArrowCellRenderer(direction = direction)
        self.pack_start(cell)
        self.set_attributes(cell, arrow=0)



class ObjectPropertiesFrame(gtk.VBox, Publisher):

    """Base class for frames to be inserted into an ObjectPropertiesPanel
    """

    def __init__(self, context):
        gtk.VBox.__init__(self)
        self.context = context
        self.auto_update = 1
        self.build_frame()

    def build_menu(self, items, accel = None):
        if accel is None:
            accel = gtk.AccelGroup()
        # Keep a reference to the factory or the menu might go away to
        # soon
        factory = self._factory = gtk.ItemFactory(gtk.Menu, '<menu>', accel)
        factory.create_items(items)
        menu = factory.get_widget('<menu>')
        return menu, accel, factory

    def object_changed(self, objects, properties):
        if self.auto_update:
            self.update_from_object(objects, properties)

    def Apply(self):
        pass

    def HasApply(self):
        return 1


class LinePropertiesFrame(ObjectPropertiesFrame):

    """Dialog for setting line styles."""

    def __init__(self, context):
        self.no_line = 0
        self.unit_scale = 1.0
        self.ignore_width = 0
        self.current_width = 1.0

        # set to true while an update because of a changed selection.
        # See update_from_object for more details.
        self.updating = 0

        ObjectPropertiesFrame.__init__(self, context)

    def on_auto_update(self, idx, widget, *args):
        self.auto_update = widget.get_active()

    def on_none(self, widget, *args):
        self.no_line = widget.get_active()
        if self.no_line:
            self.apply(EmptyPattern, 'line_pattern')
        else:
            self.apply(self.color.Color(), 'line_pattern')

    def set_width(self, width):
        self.ignore_width = 1
        self.width.set_value(width / self.unit_scale)
        self.ignore_width = 0

    def on_width(self, widget, *args):
        if not self.ignore_width:
            width = widget.get_value()
            width = width * self.unit_scale
            if width != self.current_width:
                self.apply(width, 'line_width')
                self.width_menu.SetOption(width)
                self.current_width = width

    def on_add(self, widget, *args):
        width = self.width.get_value()
        width = width * self.unit_scale
        self.width_menu.AddWidth(width)

    def on_units(self, value):
        unit, scale = value
        adj = self.width.get_adjustment()
        width = self.width.get_value()
        width = self.unit_scale * width / scale
        digits = max(0, int(1.5 + log10(scale)))
        # Black-magic (tm) step size calculation:
        # Tries to get a nice "round" number close to 1 pt.  Yields 0.01 in,
        # 1 pt, 1 mm, 0.1 cm and should give reasonable values for arbitrary
        # units.
        ten_scale = 10.0**(digits - 1)
        step = max(1, int((1.0 / scale) * ten_scale + 0.5)) / ten_scale
        self.unit_scale = scale
        self.ignore_width = 1
        # XXX Arbitary upper limit of 12 inches.  Is this reasonable or
        # should it be larger or smaller?  Note that 0.6 had no upper limit.
        adj.set_all(width, 0, 12.0 * 72.0 / scale, step, 0, 0)
        self.width.set_digits(digits)
        self.width_menu.SetUnits(scale, digits)
        self.ignore_width = 0

    def set_no_line(self, on):
        self.no_line = on
        self.none.set_active(on)

    def set_auto_update(self, on):
        self.auto_update = on
        self.auto_update_item.set_active(on)

    def on_set_default(self, widget, *args):
        properties = self.extract_properties()
        set_graphics_defaults(properties)

    def get_options_menu(self):
        options = ((_('/_Auto update'), '<control>A',
                    self.on_auto_update, 0, '<CheckItem>'),
                   (_('/Set as _default'), None,
                    self.on_set_default, 0, '<Item>'),
                   )
        menu, accel, factory = self.build_menu(options)
        self.auto_update_item = factory.get_widget(_('/Auto update'))
        self.set_auto_update(self.auto_update)
        return menu, accel

    def extract_properties(self):
        pattern = EmptyPattern
        if not self.no_line:
            pattern = SolidPattern(self.color.Color())
        width = self.width.get_value()
        width = width / self.unit_scale
        properties = {}
        properties['line_width'] = width
        properties['line_dashes'] = self.dash_menu.GetOption()
        properties['line_arrow1'] = self.left_arrow.GetOption()
        properties['line_arrow2'] = self.right_arrow.GetOption()
        properties['line_pattern'] = pattern
        properties['line_join'] = self.line_joints.GetOption()
        properties['line_cap'] = self.line_caps.GetOption()
        for property in ('line_width', 'line_dashes', 'line_pattern',
                         'line_cap', 'line_join'):
            if properties[property] is None:
                del properties[property]
        return properties

    def Apply(self):
        editor = self.context.editor
        if editor is None: return
        properties = self.extract_properties()
        apply(editor.SetProperties, (), properties)

    def apply(self, *args):
        # bail out early if we're called because the widgets have been
        # changed by the update_from_object method or when there's no
        # editor
        if self.updating or not self.context.HasEditor():
            return

        property, args = args[-1], args[:-1]
        if property == 'line_width':
            # update line width spinbox
            (width, ) = args
            self.set_width(width)
        editor = self.context.editor
        if property == 'line_pattern':
            (color, ) = args
            if color == EmptyPattern:
                pattern = EmptyPattern
            else:
                if color is None: return
                pattern = SolidPattern(color)
                self.set_no_line(0)
            editor.SetProperties(line_pattern = pattern)
        elif property == 'line_width':
            (width, ) = args
            editor.SetProperties(line_width = width)
        elif property == 'line_dashes':
            (dashes, ) = args
            editor.SetProperties(line_dashes = dashes)
        elif property == 'line_join':
            (join, ) = args
            editor.SetProperties(line_join = join)
        elif property == 'line_cap':
            (cap, ) = args
            editor.SetProperties(line_cap = cap)
        elif property == 'line_arrow1':
            (arrow, ) = args
            editor.SetProperties(line_arrow1 = arrow)
        elif property == 'line_arrow2':
            (arrow, ) = args
            editor.SetProperties(line_arrow2 = arrow)

    def update_from_object(self, objects, properties):
        """Update the frame with the properties"""
        # Updating the widges may trigger their callbacks which would
        # lead to calls to apply. Set updating to true to tell apply to
        # ignore this.
        self.updating = 1
        try:
            pattern = properties.line_pattern
            self.set_no_line(pattern.is_Empty)
            if pattern.is_Solid:
                self.color.SetColor(pattern.color)
            width = properties.line_width
            if width is not None:
                self.set_width(width)
            self.width_menu.SetOption(properties.line_width)
            self.dash_menu.SetOption(properties.line_dashes)
            self.line_caps.SetOption(properties.line_cap)
            self.line_joints.SetOption(properties.line_join)
            self.left_arrow.SetOption(properties.line_arrow1)
            self.right_arrow.SetOption(properties.line_arrow2)
        finally:
            # make sure to reset updating even in case of errors
            self.updating = 0

    def build_frame(self):
        vbox = self
        # no line, color
        color_hbox = gtk.HBox(homogeneous = 1)
        self.none = none = gtk.ToggleButton(_("No Line"))
        none.connect('clicked', self.on_none)
        self.color = color = ColorButton(_("Color"), _("Line Color"),
                                         StandardColors.black)
        color.Subscribe(CHANGED, self.apply, 'line_pattern')
        color_hbox.pack_start(none, 0, 1)
        color_hbox.pack_start(color, 0, 1)
        # width, units
        label = gtk.Label(_("Width:"))
        width_hbox = gtk.HBox(homogeneous = 0)
        adjust = gtk.Adjustment(1, 0, 72, 1, 0, 0)
        self.width = width = gtk.SpinButton(adjust)
        width.connect('changed', self.on_width)
        self.unit_menu = unit_menu = UnitMenu()
        unit_menu.Subscribe(CHANGED, self.on_units)
        width_hbox.pack_start(label, 0, 1)
        width_hbox.pack_start(width, 1, 1)
        width_hbox.pack_start(unit_menu, 0, 1)
        # width option menu
        width_menu_hbox = gtk.HBox(homogeneous = 0)
        self.width_menu = width_menu = LineWidthMenu()
        width_menu.Subscribe(CHANGED, self.apply, 'line_width')
        self.add_button = add_button = gtk.Button(_("Add"))
        add_button.connect('clicked', self.on_add)
        width_menu_hbox.pack_start(width_menu, 1, 1)
        width_menu_hbox.pack_start(add_button, 0, 1)
        # reset width adjustment to default unit
        self.on_units(unit_menu.GetOption())
        # dash option menu
        self.dash_menu = dash_menu = LineDashesMenu()
        dash_menu.Subscribe(CHANGED, self.apply, 'line_dashes')
        # arrows, joints, and caps
        self.left_arrow = left_arrow = ArrowMenu(0)
        left_arrow.Subscribe(CHANGED, self.apply, 'line_arrow1')
        self.right_arrow = right_arrow = ArrowMenu(1)
        right_arrow.Subscribe(CHANGED, self.apply, 'line_arrow2')
        line_caps = PixmapMenu([('CapButt', CapButt), ('CapRound', CapRound),
                                ('CapProjecting', CapProjecting)])
        line_caps.Subscribe(CHANGED, self.apply, 'line_cap')
        self.line_caps = line_caps
        line_joints = PixmapMenu([('JoinMiter', JoinMiter),
                                  ('JoinRound', JoinRound),
                                  ('JoinBevel', JoinBevel)])
        line_joints.Subscribe(CHANGED, self.apply, 'line_join')
        self.line_joints = line_joints
        table = gtk.Table(2, 2, homogeneous = 0)
        table.attach(left_arrow, 0, 1, 0, 1)
        table.attach(right_arrow, 1, 2, 0, 1)
        table.attach(line_joints, 0, 1, 1, 2)
        table.attach(line_caps, 1, 2, 1, 2)

        vbox.pack_start(color_hbox, 0, 1)
        vbox.pack_start(width_hbox, 0, 1)
        vbox.pack_start(width_menu_hbox, 0, 1)
        vbox.pack_start(dash_menu, 0, 1)
        vbox.pack_start(table, 0, 1)
        vbox.show_all()


class ObjectPanel(SketchPanel):

    title = _('Object Properties')
    multiple = 1

    def __init__(self, context):
        self.current_frame = None
        self.current_accel = None
        self.current_omenu = None
        SketchPanel.__init__(self, context, self.title)
        # no resize, my personal preference but tastes vary :-)
        self.set_resizable(0)
        context.Subscribe(SELECTION, self.context_changed)
        context.Subscribe(TRANSACTION_END, self.context_changed)
        context.Subscribe(STYLE, self.context_changed)
        self.context_changed()

    def destroyed(self, *args):
        apply(SketchPanel.destroyed, (self,) + args)
        self.context.Unsubscribe(STYLE, self.context_changed)
        self.context.Unsubscribe(TRANSACTION_END, self.context_changed)
        self.context.Unsubscribe(SELECTION, self.context_changed)

    def build_window(self):
        self.build_frames()
        self.build_menu()
        self.vbox.pack_start(self.menubar, 0, 1)
        self.framebox = gtk.Frame()
        self.vbox.pack_start(self.framebox, 0, 1)
        self.vbox.show_all()
        self.update_from_button = gtk.Button(_('Update From...'))
        self.update_from_button.connect('clicked', self.on_update_from)
        # XXX "update from..." is not yet implemented (see notes above)
        self.update_from_button.set_sensitive(0)
        self.update_from_button.show()
        self.apply_button = gtk.Button(_('Apply'))
        self.apply_button.connect('clicked', self.on_apply)
        self.apply_button.show()
        self.action_area.pack_start(self.update_from_button, 0, 1)
        self.action_area.pack_start(self.apply_button, 0, 1)
        self.action_area.set_border_width(0)
        self.action_area.set_spacing(0)
        self.set_frame(0)

    def build_frames(self):
        context = self.context
        self.line_frame = LinePropertiesFrame(context)
        # XXX temporary for testing purposes
        self.fill_frame = LinePropertiesFrame(context)
        self.font_frame = LinePropertiesFrame(context)
        # XXX
        self.frames = ((self.line_frame, _("Line Style")),
                       (self.fill_frame, _("Fill Pattern")),
                       (self.font_frame, _("Text Attributes")),
                       )

    menu_items = [ ("/_" + _("Dialogs"), None, None, 0, "<Branch>"),
                   ("/_" + _("Options"), None, None, 0, "<Branch>"),
                   ("/_" + _("Help"), None, None, 0, "<LastBranch>"),
                 ]
    option_menu_index = 1   # XXX bad form.  better approach?
    help_menu_index = 2     # XXX bad form.  better approach?

    def next_page(self, *args):
        idx = self.idx + 1
        if idx == len(self.frames): idx = 0
        self.set_frame(idx)

    def prev_page(self, *args):
        idx = self.idx - 1
        if idx < 0: idx = len(self.frames) - 1
        self.set_frame(idx)

    def build_menu(self):
        accel = gtk.AccelGroup()
        self.add_accel_group(accel)
        factory = gtk.ItemFactory(gtk.MenuBar, '<obj_panel>', accel)
        factory.create_items(self.menu_items)
        # Keep a reference to the factory. If the factory is garbage
        # collected the menu will be empty
        self._factory = factory

        # FIXME: The connect method isn't wrapped in PyGTK at the moment
        # and GObject implements a connect method as well which does
        # something different. Therefore this is commented out for now.
        #accel.connect(gtk.keysyms.Page_Down, 0, 0, self.next_page)
        #accel.connect(gtk.keysyms.Page_Up, 0, 0, self.prev_page)

        self.menubar = factory.get_widget("<obj_panel>")
        roots = self.menubar.get_children()
        self.options_menu = roots[self.option_menu_index]
        self.help_menu = roots[self.help_menu_index]
        # XXX help is not yet implemented
        self.help_menu.set_sensitive(0)
        menu = factory.get_widget("/" + _("Dialogs"))
        for idx in range(len(self.frames)):
            frame, title = self.frames[idx]
            item = gtk.MenuItem(title)
            item.connect('activate', self.on_frame, idx)
            menu.append(item)

    def on_frame(self, widget, idx, *args):
        self.set_frame(idx)

    def set_frame(self, idx):
        self.idx = idx
        frame, frame_title = self.frames[idx]
        self.set_title("%s - %s" % (self.title, frame_title))
        if self.current_frame is not None:
            self.current_frame.hide()
            self.framebox.remove(self.current_frame)
        if self.current_accel is not None:
            self.remove_accel_group(self.current_accel)
        if self.current_omenu is not None:
            self.options_menu.remove_submenu()
        self.framebox.add(frame)
        self.current_frame = frame
        self.current_omenu, self.current_accel = frame.get_options_menu()
        self.add_accel_group(self.current_accel)
        self.options_menu.set_submenu(self.current_omenu)
        self.apply_button.set_sensitive(frame.HasApply())
        frame.show()

    def context_changed(self, *args):
        objects = []
        common = PropertyStack()
        can_apply = 0
        if self.context.HasEditor():
            objects = self.context.editor.SelectedObjects()
            common = common_object_properties(objects)
            if len(objects) > 0: can_apply = 1
        for frame, name in self.frames:
            frame.object_changed(objects, common)
        can_apply = can_apply and self.current_frame.HasApply()
        self.apply_button.set_sensitive(can_apply)

    #def update_from_object(self, object):
    #   print "update from ", object

    # XXX implement me
    def on_update_from(self, *args):
        print '"UPDATE FROM..." is not implemented'
        # 0.6.12ish approach (not applicable in 0.7):
        # canvas = self.context.application.canvas
        # if canvas is not None:
        #     canvas.PickObject(self.update_from_object)

    def on_apply(self, *args):
        self.current_frame.Apply()
