# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000, 2001, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA


import gtk

import Sketch

class SketchPanel(gtk.Dialog):

    # if true, there can be multiple panels of this class, otherwise
    # only one.
    multiple = 0

    def __init__(self, context, title):
        gtk.Dialog.__init__(self)
        self.context = context
        self.set_title(title)

        # this window is not really a dialog because it's not modal, so
        # set the type hint accordingly.  We maybe should switch to
        # gtk.Window as the base-class, but that would require changes
        # to the derived classess which expect certain predefined
        # children such as self.vbox.
        self.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_NORMAL)

        self.build_window()
        self.connect("destroy", self.destroyed)
        self.context.Subscribe(Sketch.Base.const.CHANGED, self.context_changed)

    def build_window(self):
        pass

    def close(self, *args):
        self.destroy()

    def destroyed(self, *args):
        self.context.Unsubscribe(Sketch.Base.const.CHANGED,
                                 self.context_changed)
                
    def context_changed(self, *args):
        pass
