# Sketch - A Python-based interactive drawing program
# Copyright (C) 1999, 2000, 2001, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

from types import TupleType

import gtk
from gtk import FILL, EXPAND

import Sketch
from Sketch import _
from Sketch.Base import const
from Sketch.Lib.util import format
from converters import converters
from skpixmaps import pixmaps
from skpanel import SketchPanel


class SketchTree(gtk.CTree):

    def __init__(self, context):
        self.context = context
        gtk.CTree.__init__(self)
        self.object_to_node = {}
        self.context.Subscribe(const.CHANGED, self.context_changed)
        self.context.Subscribe(const.OBJECT_CHANGED, self.object_changed)
        self.context.Subscribe(const.SELECTION, self.selection_changed)
        self.context.Subscribe(const.TRANSACTION_START, self.transaction_start)
        self.context.Subscribe(const.TRANSACTION_END, self.transaction_end)
        self.connect("select_row", self.select_row)
        self.connect("unselect_row", self.unselect_row)
        self.set_selection_mode(gtk.SELECTION_MULTIPLE)
        self.build_tree()

    def build_tree(self):
        doc = self.context.document
        if doc is None:
            self.clear()
            return

        self.freeze()
        try:
            root = self.insert_node(None, None, (_("Document"),), is_leaf = 0,
                                    expanded = 1)
            node = None
            for layer in doc.Layers():
                node = self.make_node(layer, root, node,
                                      not layer.is_SpecialLayer)
                self.add_compound(node, layer)
        finally:
            self.thaw()
        
    def add_compound(self, parent, group):
        selectable = group.allow_traversal
        sibling = None
        for object in group.GetObjects():
            sibling = self.make_node(object, parent, sibling, selectable)
            if object.is_Compound:
                self.add_compound(sibling, object)

    def make_node(self, object, parent, sibling, selectable = 1):
        text = object.Info()
        if type(text) == TupleType:
            template, dict = text
            text = format(template, converters, dict)
        if object.is_Compound:
            is_leaf = 0
            if object.is_Layer:
                pixmap, mask = pixmaps.tree_layer
            else:
                pixmap, mask = pixmaps.tree_group
        else:
            is_leaf = 1
            if object.is_Ellipse:
                pixmap, mask = pixmaps.tree_ellipse
            elif object.is_Rectangle:
                pixmap, mask = pixmaps.tree_rectangle
            else:
                pixmap, mask = pixmaps.tree_object
        node = self.insert_node(parent, sibling, (text,),
                                pixmap_closed = pixmap,
                                mask_closed = mask,
                                pixmap_opened = pixmap,
                                mask_opened = mask,
                                is_leaf = is_leaf)
        selectable = selectable and self.node_get_selectable(parent)
        self.node_set_selectable(node, selectable)
        self.node_set_row_data(node, object)
        self.object_to_node[id(object)] = node
        return node

    def update_node(self, object):
        node = self.object_to_node.get(id(object))
        if node is not None:
            text, spacing, pixmap, mask = self.node_get_pixtext(node, 0)
            text = object.Info()
            if type(text) == TupleType:
                template, dict = text
                text = format(template, converters, dict)
            self.node_set_pixtext(node, 0, text, spacing, pixmap, mask)

    def context_changed(self, *args):
        self.clear()
        self.object_to_node.clear()
        self.build_tree()

    def object_changed(self, obj, what, detail):
        #print obj, what, detail
        self.freeze()
        try:
            if what:
                self.update_node(obj)
                node = self.object_to_node.get(id(obj))
                if obj.is_Compound and what == const.CHILDREN:
                    children = obj.GetObjects()
                    sibling = None
                    compounds = []

                    for child in children:
                        childnode = self.object_to_node.get(id(child))
                        if childnode is None:
                            childnode = self.make_node(child, node, sibling)
                            if child.is_Compound:
                                compounds.append((childnode, child))
                        else:
                            self.move(childnode, node, sibling)
                        sibling = childnode

                    childnodes = node.children
                    childnodes.reverse()
                    for childnode in childnodes[len(children):]:
                        child = self.node_get_row_data(childnode)
                        del self.object_to_node[id(child)]
                        self.remove_node(childnode)

                    for childnode, child in compounds:
                        self.add_compound(childnode, child)
        finally:
            self.thaw()

    def selection_changed(self, *args):
        #pass
        self.unselect_all()
        for obj in self.context.editor.SelectedObjects():
            node = self.object_to_node[id(obj)]
            self.select(node)

    def select_row(self, widget, row, column, event):
        # for some annoying reason clist/ctree even emits the select_row
        # signal if the user clicks on a non-selectable row, so we have
        # to deal with this special case here.
        if not self.get_selectable(row):
            return
        obj = self.get_row_data(row)
        if event.state & gtk.gdk.SHIFT_MASK:
            mode = const.SelectAdd
        else:
            mode = const.SelectSet
        self.context.editor.SelectObject(obj, mode)

    def unselect_row(self, widget, row, column, event):
        obj = self.get_row_data(row)
        self.context.editor.SelectObject(obj, const.SelectSubtract)

    def transaction_start(self):
        self.freeze()

    def transaction_end(self, *args):
        self.thaw()


class TreeViewPanel(SketchPanel):

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("Tree View"))

    def build_window(self):
        vbox = self.vbox

        # tree
        table = gtk.Table(2, 2)
        table.show()
        vbox.pack_start(table)

        hadjust = gtk.Adjustment()
        hscrollbar = gtk.HScrollbar(hadjust)
        #hscrollbar.set_update_policy(UPDATE_DELAYED)
        hscrollbar.show()
        table.attach(hscrollbar, 0, 1, 1, 2, EXPAND|FILL, 0)
        
        vadjust = gtk.Adjustment()
        vscrollbar = gtk.VScrollbar(vadjust)
        #vscrollbar.set_update_policy(UPDATE_DELAYED)
        vscrollbar.show()
        table.attach(vscrollbar, 1, 2, 0, 1, 0, EXPAND|FILL)
        
        self.tree = SketchTree(self.context)
        self.tree.set_hadjustment(hadjust)
        self.tree.set_vadjustment(vadjust)
        self.tree.show()
        
        table.attach(self.tree, 0, 1, 0, 1, EXPAND|FILL, EXPAND|FILL)

        # buttons
        button = gtk.Button(_("Close"))
        #button.set_flags(CAN_DEFAULT)
        button.connect("clicked", lambda event: self.destroy())
        self.action_area.pack_start(button)
        button.show()
