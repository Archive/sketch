# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import gtk.gdk as GDK

CurStd		= GDK.TOP_LEFT_ARROW
CurEdit         = GDK.DRAFT_SMALL
CurHandle	= GDK.CROSSHAIR
CurTurn		= GDK.EXCHANGE
CurPick		= GDK.DRAFT_SMALL
CurDraw         = GDK.PENCIL
CurPlace	= GDK.CROSSHAIR
CurDragColor	= GDK.SPRAYCAN
CurText         = GDK.XTERM     # standard text mode cursor (bitmap)
CurTextPoint    = GDK.XTERM     # pointer is above is point text (bitmap) 
CurTextPath     = GDK.XTERM     # pointer is above is path text (bitmap)
CurVGuide       = GDK.SB_H_DOUBLE_ARROW
CurHGuide       = GDK.SB_V_DOUBLE_ARROW
#  CurSizeL        = GDK.LEFT_TEE
#  CurSizeR        = GDK.RIGHT_TEE
#  CurSizeT        = GDK.TOP_TEE
#  CurSizeB        = GDK.BOTTOM_TEE
#  CurSizeTL       = GDK.UL_ANGLE
#  CurSizeTR       = GDK.UR_ANGLE
#  CurSizeBL       = GDK.LL_ANGLE
#  CurSizeBR       = GDK.LR_ANGLE
CurSizeL        = GDK.LEFT_SIDE
CurSizeR        = GDK.RIGHT_SIDE
CurSizeT        = GDK.TOP_SIDE
CurSizeB        = GDK.BOTTOM_SIDE
CurSizeTL       = GDK.TOP_LEFT_CORNER
CurSizeTR       = GDK.TOP_RIGHT_CORNER
CurSizeBL       = GDK.BOTTOM_LEFT_CORNER
CurSizeBR       = GDK.BOTTOM_RIGHT_CORNER
CurZoom	        = GDK.CIRCLE	# is replaced by bitmap specification in
				# skpixmaps.py
# CurEdit is also added by skpixmaps.py
