# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2001, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from math import atan2, pi, cos, sin

import gobject
import gtk

import Sketch
import Sketch.Graphics
from Sketch import _, Rect, Trafo, Point, Polar, _skgtk
from Sketch.Base import Publisher
from Sketch.Base.const import EDITED, CHANGED
from Sketch.Graphics import StandardColors, EmptyLineStyle, PropertyStack,\
     LinearGradient, RadialGradient, ConicalGradient, HatchingPattern, \
     EmptyPattern, SolidPattern, CreateRGBColor, CreateSimpleGradient, \
     MultiGradient
from Sketch.Editor.const import ConstraintMask

from gtkmisc import SketchDrawingArea, get_color, ColorButton, CommonComboBox
from skpanel import SketchPanel
import skpixmaps
pixmaps = skpixmaps.pixmaps


class PatternSample(SketchDrawingArea):

    """Window that displays a fill pattern

    The pattern is drawn as if it were used to fill a Skencil rectangle
    object covering the window exactly.
    """

    def __init__(self,  **kw):
        apply(SketchDrawingArea.__init__, (self,), kw)
        self.pattern = EmptyPattern
        self.properties = PropertyStack()
        self.properties.AddStyle(EmptyLineStyle)
        self.properties.SetProperty(fill_pattern = self.pattern)
        self.gc.SetProperties(self.properties)
        self.fill_rect = None

    def realize_method(self, *args):
        apply(SketchDrawingArea.realize_method, (self,) + args)
        self.fill_rect = Rect(0, 0, *self.window.get_size())
        self.compute_trafo()

    def compute_trafo(self):
        if not hasattr(self, 'window'):
            return
        width, height = self.window.get_size()
        doc_to_win = Trafo(1, 0, 0, -1, 0, height)
        win_to_doc = doc_to_win.inverse()
        self.gc.SetViewportTransform(1.0, doc_to_win, win_to_doc)
        self.fill_rect = Rect(0, 0, width, height)
        self.gc.SetProperties(self.properties, self.fill_rect)

    def SetPattern(self, pattern):
        if pattern != self.pattern:
            self.pattern = pattern
            self.UpdateWhenIdle()
            self.properties.SetProperty(fill_pattern = pattern)
            self.gc.SetProperties(self.properties, self.fill_rect)

    def RedrawMethod(self, region = None):
        win = self.window
        width, height = win.get_size()
        self.gc.StartDblBuffer()
        self.gc.SetFillColor(StandardColors.white)
        self.gc.FillRectangle(0, 0, width, height)
        if self.properties.HasFill():
            self.gc.Rectangle(Trafo(width, 0, 0, height, 0, 0))
        else:
            self.gc.SetLineColor(StandardColors.black)
            self.gc.DrawLineXY(0, height, width, 0)
        self.gc.EndDblBuffer()

    def ResizedMethod(self, width, height):
        self.gc.WindowResized(width, height)
        self.UpdateWhenIdle()
        self.compute_trafo()


class InteractiveSample(PatternSample, Publisher):

    """Interactive Sample Editor

    The class extends PatternSample to let the user specify the center
    and direction parameters of patterns.  These parameters are not
    supported by all patterns and there are patterns that only need on
    of them, so it's possible to change which of the parameters can be
    specified.

    The position of the center can be constrained to the corners of the
    window, the centers of the sides and the center of the window.  The
    direction can be constrained to multiples of 15 degrees.  The user
    can activate the constraints by holding down the control key while
    clicking and dragging the mouse.
    """

    allow_edit_center = 0
    allow_edit_dir = 0

    def __init__(self, **kw):
        apply(PatternSample.__init__, (self,),  kw)
        self.add_events(gtk.gdk.POINTER_MOTION_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK)
        self.connect('button_press_event', self.button_press)
        self.connect('motion_notify_event', self.motion_notify)
        self.connect('button_release_event', self.button_release)
        self.current_mode = 0   # 0: center, 1: direction
        self.current_pos = (0, 0, 0)
        self.drawn = 0
        self.dragging = 0
        self.drag_button = None

    def destroy(self):
        Publisher.Destroy(self)
        PatternSample.destroy(self)

    def SetEditMode(self, center, direction):
        self.allow_edit_center = center
        self.allow_edit_dir = direction

    def init_gcs(self):
        PatternSample.init_gcs(self)
        self.inv_gc = self.window.new_gc(function = gtk.gdk.XOR)
        _skgtk.set_foreground_and_fill(self.inv_gc, ~0)

    def _set_current_pos(self, event):
        """Set self.current_pos from event"""
        self.current_pos = (int(event.x), int(event.y),
                            event.state & ConstraintMask)

    def button_press(self, widget, event):
        button = event.button
        if button == 1 and self.allow_edit_center:
            self.current_mode = 0
        elif self.allow_edit_dir:
            self.current_mode = 1
        else:
            return
        self._set_current_pos(event)
        self.show_edit()
        self.dragging = 1
        self.drag_button = button

    def motion_notify(self, widget, event):
        if self.dragging:
            self.hide_edit()
            self._set_current_pos(event)
            self.show_edit()

    def button_release(self, widget, event):
        if self.dragging and event.button == self.drag_button:
            self.hide_edit()
            self._set_current_pos(event)
            self.report_edit()
            self.dragging = 0
            self.drag_button = None

    def show_edit(self):
        if not self.drawn:
            self.draw_edit()
            self.drawn = 1

    def hide_edit(self):
        if self.drawn:
            self.draw_edit()
            self.drawn = 0

    def center_in_window_coordinates(self):
        """\
        Return the position of the center of the pattern in window coordinates

        If the current pattern doesn't allow editing the center, this
        method assumes that the pattern doesn't have a center and uses
        the center of the window instead.
        """
        if self.allow_edit_center:
            x, y = self.pattern.Center()
        else:
            x = y = 0.5
        width, height = self.window.get_size()
        return (int(width * x), int(height * (1 - y)))

    def constrain_center(self, x, y):
        """Map the point (x, y) to a constrained center position

        The constrained positions are the corners, the centers of the
        edges and the center of the window.  The return value is a pair
        of ints (x, y) with the contrained position.
        """
        width, height = self.window.get_size()
        x = float(x) / width
        y = float(y) / height
        if   x < 0.25:  x = 0
        elif x < 0.75:  x = 0.5
        else:           x = 1.0

        if   y < 0.25:  y = 0
        elif y < 0.75:  y = 0.5
        else:           y = 1.0

        return (int(width * x), int(height * y))

    def draw_edit(self):
        """Draw the crosshairs/direction line during edits"""
        x, y, constraint = self.current_pos
        if self.current_mode == 0:
            # moving the parameter "center"
            if constraint:
                x, y = self.constrain_center(x, y)
            width, height = self.window.get_size()
            self.window.draw_line(self.inv_gc, 0, y, width, y)
            self.window.draw_line(self.inv_gc, x, 0, x, height)
        else:
            # changing the parameter "direction"
            cx, cy = self.center_in_window_coordinates()

            angle = atan2(y - cy, x - cx) + 2 * pi
            if constraint:
                pi12 = pi / 12
                angle = pi12 * int(angle / pi12 + 0.5)

            # choose an end point in the right direction that is very
            # likely outside of the window.  A distance of 1000 pixels
            # should be enough for now.
            x = int(cx + cos(angle) * 1000.0)
            y = int(cy + sin(angle) * 1000.0)

            self.window.draw_line(self.inv_gc, cx, cy, x, y)

    def report_edit(self):
        x, y, constraint = self.current_pos
        if self.current_mode == 0:
            if constraint:
                x, y = self.constrain_center(x, y)
            width, height = self.window.get_size()
            what = "center"
            detail = Point(float(x) / width, 1 - (float(y) / height))
        else:
            cx, cy = self.center_in_window_coordinates()
            angle = atan2(y - cy, x - cx) + 2 * pi
            if constraint:
                pi12 = pi / 12
                angle = pi12 * int(angle / pi12 + 0.5)
            what = "direction"
            detail = Point(cos(angle), -sin(angle))

        self.issue(EDITED, what, detail)

    def RedrawMethod(self, region):
        PatternSample.RedrawMethod(self, region)
        self.drawn = 0


class PatternFrame(Publisher):

    def __init__(self, pattern = None):
        self._inhibit_changed_messages = 0
        self._has_chaned = False
        if pattern is not None:
            self.pattern = pattern
        else:
            self.pattern = EmptyPattern

    def destroy(self):
        Publisher.Destroy(self)
        gtk.HBox.destroy()

    def inhibit_changed(self):
        """Inhibit issueing of CHANGED messages until the call to allow_changed

        Calls to inhibit_changed and allow_changed may be nested.  Each
        inhibit_changed call must have a matching allow_changed call.
        """
        self._inhibit_changed_messages += 1
        if self._inhibit_changed_messages == 1:
            self._has_chaned = False

    def allow_changed(self):
        """Allow issuing of CHANGED messages.

        If self.changed was called at least once between the first
        inhibit_changed call and the matching lst allow_changed call,
        the last call will issue a CHANGED message by calling
        self.changed.
        """
        self._inhibit_changed_messages -= 1
        assert self._inhibit_changed_messages >= 0
        if self._inhibit_changed_messages == 0:
            self.changed()

    def changed(self):
        """Issue a CHANGED message

        Normally, the CHANGED is issued immediately.  However, if
        self.inhibit_changed has been called but the corresponding
        allow_changed call hasn't occurred yet, this method sets a flag
        that so that the message will be issued by the last
        allow_changed()
        """
        if self._inhibit_changed_messages > 0:
            self._has_chaned = True
        else:
            self.issue(CHANGED)

    def SetPattern(self, pattern):
        changed = self.pattern != pattern
        self.pattern = pattern
        if changed:
            self.changed()

    def Pattern(self):
        return self.pattern

    def SetCenter(self, center):
        pass

    def SetDirection(self, dir):
        pass

    def Center(self):
        return (0.5, 0.5)

    def EditModes(self):
        return (0, 0)


class EmptyPatternFrame(gtk.VBox, PatternFrame):

    def __init__(self):
        gtk.VBox.__init__(self)
        PatternFrame.__init__(self)
        label = gtk.Label(_("No Fill"))
        self.pack_start(label)
        label.show()


class SolidPatternFrame(gtk.VBox, PatternFrame):

    def __init__(self):
        gtk.VBox.__init__(self)
        PatternFrame.__init__(self, SolidPattern(StandardColors.black))
        button = gtk.Button(_("Choose Color..."))
        self.pack_start(button, expand = 0)
        button.show()
        button.connect("clicked", self.ChooseColor)

    def ChooseColor(self, *args):
        color = get_color(_("Select Fill Color"), self.pattern.Color())
        if color is not None:
            self.SetPattern(SolidPattern(color))
            self.changed()


class GradientPatternFrame(gtk.VBox, PatternFrame):

    def __init__(self):
        gtk.VBox.__init__(self)
        start_color = StandardColors.black
        end_color = StandardColors.white
        gradient = CreateSimpleGradient(start_color, end_color)
        PatternFrame.__init__(self, LinearGradient(gradient))
        box = gtk.HBox()
        self.start_button = ColorButton(_("Start"), _("Start Color"),
                                        start_color)
        self.start_button.Subscribe(CHANGED, self.set_color, 0)
        box.pack_start(self.start_button)
        self.end_button = ColorButton(_("End"), _("End Color"), end_color)
        self.end_button.Subscribe(CHANGED, self.set_color, 1)
        box.pack_start(self.end_button)
        self.pack_start(box, expand = 0)

        store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_INT)
        for name, kind in ((_("Axial"),0), (_("Radial"),1), (_("Conical"),2)):
            store.append((name, kind))
        self.combobox = CommonComboBox(store, value_column = 1)
        self.combobox.store = store
        cell = gtk.CellRendererText()
        self.combobox.pack_start(cell)
        self.combobox.set_attributes(cell, text = 0)
        self.combobox.Subscribe(CHANGED, self.set_type)
        self.pack_start(self.combobox, expand = 0)

        box = gtk.HBox()
        label = gtk.Label(_("Border"))
        box.pack_start(label)

        self.adjust_border = gtk.Adjustment(value = 0,
                                            lower = 0, upper = 100,
                                            step_incr = 1)
        entry = gtk.SpinButton(adjustment = self.adjust_border, digits = 0)
        box.pack_start(entry)
        self.adjust_border.connect("value_changed", self.border_changed)
        self.pack_start(box, expand = 0)

        label = gtk.Label(_("Center"))
        label.set_alignment(0.0, 0.5)
        self.pack_start(label, expand = 0)

        box = gtk.HBox()
        # Use some arbitrary but relatively high bounds for the center
        # coordinates. The center may well lie outside the bounding
        # rect.
        self.adjust_cx = gtk.Adjustment(value = 0,
                                        lower = -10000, upper = 10000,
                                        step_incr = 1)
        entry = gtk.SpinButton(adjustment = self.adjust_cx, digits = 0)
        box.pack_start(entry)
        self.adjust_cx.connect("value_changed", self.center_changed, 0)
        self.adjust_cy = gtk.Adjustment(value = 0,
                                        lower = -10000, upper = 10000,
                                        step_incr = 1)
        entry = gtk.SpinButton(adjustment = self.adjust_cy, digits = 0)
        box.pack_start(entry)
        self.adjust_cy.connect("value_changed", self.center_changed, 1)
        self.pack_start(box, expand = 0)

        box = gtk.HBox()
        label = gtk.Label(_("Angle"))
        box.pack_start(label)

        # The range for the angle doesn't matter as long as it contains
        # more than the range 0...360. Values outside that range are
        # automatically wrapped into that range by converting from angle
        # to vector in angle_changed and back in SetPattern
        self.adjust_angle = gtk.Adjustment(value = 0,
                                           lower = -1000, upper = 1000,
                                           step_incr = 1)
        entry = gtk.SpinButton(adjustment = self.adjust_angle, digits = 1)
        box.pack_start(entry)
        self.adjust_angle.connect("value_changed", self.angle_changed)
        self.pack_start(box, expand = 0)
        self.SetPattern(self.pattern)

    def SetPattern(self, pattern):
        self.inhibit_changed()
        try:
            PatternFrame.SetPattern(self, pattern)
            border = 0.0
            center = (0, 0)
            angle = 0.0
            if pattern.is_AxialGradient:
                history = 0
                border = pattern.Border()
                angle = pattern.Direction().polar()[1]
            elif pattern.is_RadialGradient:
                history = 1
                border = pattern.Border()
                center = pattern.Center()
            else:
                history = 2
                center = pattern.Center()
                angle = pattern.Direction().polar()[1]
            self.combobox.SetOption(history)
            gradient = pattern.Gradient()
            self.start_button.SetColor(gradient.StartColor())
            self.end_button.SetColor(gradient.EndColor())
            self.adjust_cx.set_value(round(center[0] * 100))
            self.adjust_cy.set_value(round(center[1] * 100))
            self.adjust_border.set_value(round(border * 100))
            angle = angle * 180 / pi
            if angle < 0:
                angle = angle + 360
            self.adjust_angle.set_value(angle)
        finally:
            self.allow_changed()

    def Center(self):
        if self.pattern.is_AxialGradient:
            return (0.5, 0.5)
        return tuple(self.pattern.Center())

    def EditModes(self):
        if self.pattern.is_AxialGradient:
            return (0, 1)
        elif self.pattern.is_RadialGradient:
            return (1, 0)
        else:
            return (1, 1)

    def SetCenter(self, center):
        pattern = self.pattern
        if pattern.is_RadialGradient or pattern.is_ConicalGradient:
            self.SetPattern(pattern.WithCenter(center))
            self.changed()

    def SetDirection(self, direction):
        pattern = self.pattern
        if pattern.is_AxialGradient or pattern.is_ConicalGradient:
            self.SetPattern(pattern.WithDirection(direction))
            self.changed()

    def set_color(self, color, which):
        colors = self.pattern.Gradient().Colors()[:]
        if which == 0:
            colors[0] = (0, color)
        else:
            colors[-1] = (1, color)
        self.SetPattern(self.pattern.WithGradient(MultiGradient(colors)))
        self.changed()

    def set_type(self, type):
        if type == 0:
            type = LinearGradient
        elif type == 1:
            type = RadialGradient
        else:
            type = ConicalGradient
        self.SetPattern(type(duplicate = self.pattern))
        self.changed()

    def border_changed(self, *args):
        if self.pattern.is_AxialGradient or self.pattern.is_RadialGradient:
            border = self.adjust_border.value / 100.0
            self.SetPattern(self.pattern.WithBorder(border))
            self.changed()

    def center_changed(self, widget, which):
        if self.pattern.is_RadialGradient or self.pattern.is_ConicalGradient:
            x = self.adjust_cx.value / 100.0
            y = self.adjust_cy.value / 100.0
            self.SetPattern(self.pattern.WithCenter(Point(x, y)))
            self.changed()

    def angle_changed(self, *args):
        if self.pattern.is_AxialGradient or self.pattern.is_ConicalGradient:
            dir = Polar((self.adjust_angle.value * pi) / 180.0)
            self.SetPattern(self.pattern.WithDirection(dir))
            self.changed()

class HatchingPatternFrame(gtk.VBox, PatternFrame):

    def __init__(self):
        gtk.VBox.__init__(self)
        fg = StandardColors.red
        bg = StandardColors.white
        PatternFrame.__init__(self, HatchingPattern(fg, bg))
        self.fg_button = ColorButton(_("Foreground"),_("Foreground Color"), fg)
        self.fg_button.Subscribe(CHANGED, self.set_color, 0)
        self.pack_start(self.fg_button, expand = 0)
        self.bg_button = ColorButton(_("Background"),_("Background Color"), bg)
        self.bg_button.Subscribe(CHANGED, self.set_color, 1)
        self.pack_start(self.bg_button, expand = 0)

    def SetPattern(self, pattern):
        self.inhibit_changed()
        try:
            PatternFrame.SetPattern(self, pattern)
            self.fg_button.SetColor(pattern.Foreground())
            self.bg_button.SetColor(pattern.Background())
        finally:
            self.allow_changed()

    def EditModes(self):
        return (0, 1)

    def SetDirection(self, direction):
        self.pattern = self.pattern.WithDirection(direction)
        self.changed()

    def set_color(self, color, which):
        if which == 0:
            pattern = self.pattern.WithForeground(color)
        else:
            pattern = self.pattern.WithBackground(color)
        self.SetPattern(pattern)
        self.changed()


class ImageTilePatternFrame(gtk.VBox, PatternFrame):

    def __init__(self):
        gtk.VBox.__init__(self)
        PatternFrame.__init__(self)
        label = gtk.Label("Image Tile Fill")
        self.pack_start(label)
        label.show()

class FillPanel(SketchPanel):

    def __init__(self, context):
        SketchPanel.__init__(self, context, _("Fill"))
        self.context.Subscribe(Sketch.Base.const.SELECTION,
                               self.context_changed)
        self.active_frame = None
        self.set_name("fill_panel")
        self.context_changed()

    def destroyed(self, *args):
        self.context.Unsubscribe(Sketch.Base.const.SELECTION,
                                 self.context_changed)
        apply(SketchPanel.destroyed, (self,) + args)

    def build_window(self):
        vbox = self.vbox

        self.sample = InteractiveSample()
        self.sample.set_size_request(100, 100)
        vbox.pack_start(self.sample)
        self.sample.Subscribe(EDITED, self.sample_edited)

        self.empty_frame = EmptyPatternFrame()
        self.solid_frame = SolidPatternFrame()
        self.gradient_frame = GradientPatternFrame()
        self.hatching_frame = HatchingPatternFrame()
        self.imagetile_frame = ImageTilePatternFrame()

        self.notebook = notebook = gtk.Notebook()
        for frame, icon in ((self.empty_frame, "fill_none"),
                            (self.solid_frame, "fill_solid"),
                            (self.gradient_frame, "fill_gradient"),
                            (self.hatching_frame, "fill_hatch"),
                            (self.imagetile_frame, "fill_tile")):
            image = gtk.Image()
            image.set_from_pixmap(*getattr(pixmaps, icon))
            notebook.append_page(frame, image)
            frame.Subscribe(CHANGED, self.update_sample, frame)

        vbox.pack_start(notebook)

        box = gtk.HBox()
        label = gtk.Label(_("Opacity"))
        box.pack_start(label)

        # the opacity starts out as 100 by default so users get
        # completely opaque objects
        self.adjust_opacity = gtk.Adjustment(value = 100,
                                             lower = 0, upper = 100,
                                             step_incr = 1)
        entry = gtk.SpinButton(adjustment = self.adjust_opacity, digits = 0)
        box.pack_start(entry)
        vbox.pack_start(box, expand = 0)

        self.apply_button = button = gtk.Button(_("Apply"))
        button.connect("clicked", self.do_apply)
        self.action_area.pack_start(button, expand = 0, fill = 0)
        button = gtk.Button(_("Close"))
        button.connect("clicked", self.close)
        self.action_area.pack_start(button, expand = 0, fill = 0)

        vbox.show_all()
        notebook.connect("switch-page", self.page_switched)

    def page_switched(self, notebook, page, page_num):
        self.activate_frame(notebook.get_nth_page(page_num))

    def context_changed(self, *args):
        if self.context.HasEditor():
            self.apply_button.set_sensitive(1)
            object = self.context.editor.CurrentObject()
            if object is not None:
                if object.has_fill:
                    pattern = object.Properties().fill_pattern
                    frame = self.empty_frame
                    if pattern.is_Solid:
                        frame = self.solid_frame
                    elif pattern.is_Gradient:
                        frame = self.gradient_frame
                    elif pattern.is_Hatching:
                        frame = self.hatching_frame
                    elif pattern.is_Image:
                        frame = self.imagetile_frame
                    if frame is not None:
                        frame.SetPattern(pattern)
                        self.activate_frame(frame)

                    opacity = object.Properties().fill_opacity
                    self.adjust_opacity.set_value(round(opacity * 100))
        else:
            self.apply_button.set_sensitive(0)

    def activate_frame(self, frame):
        """Make frame the active frame.

        If frame is already the active frame nothing is done.
        Otherwise, the frame is set as the current notebook page and the
        pattern sample is updated to show the pattern defined in frame.
        """
        # Only calling notebook.set_current_page when the frame is
        # actually changing is important because otherwise the notebook
        # will emit a switch-page event and this method will be called
        # for frame again.  At least that is what happens when the
        # dialog is shown for the first time.
        if frame is not self.active_frame:
            self.active_frame = frame
            index = self.notebook.page_num(frame)
            self.notebook.set_current_page(index)
            self.update_sample(frame)

    def update_sample(self, frame):
        """Update the sample window from the settings in frame"""
        self.sample.SetPattern(frame.Pattern())
        apply(self.sample.SetEditMode, frame.EditModes())

    def sample_edited(self, what, detail):
        if self.active_frame is not None:
            if what == "center":
                self.active_frame.SetCenter(detail)
            else:
                self.active_frame.SetDirection(detail)

    def do_apply(self, *args):
        if self.active_frame is not None and self.context.HasEditor():
            pattern = self.active_frame.Pattern()
            opacity = self.adjust_opacity.value / 100.0
            self.context.editor.SetProperties(fill_pattern = pattern,
                                              fill_opacity = opacity)

