# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	USA

import sys
import gtk

from Sketch import Rect, EmptyRect, IntersectRects, _skgtk
from Sketch.Base import SketchInternalError, QueueingPublisher
from Sketch.Base.warn import pdebug
from Sketch.Base.const import STATE, VIEW, DOCUMENT, LAYOUT, REDRAW,\
     LAYER, LAYER_STATE, LAYER_ORDER, LAYER_COLOR, TRANSACTION_END, \
     OBJECT_CHANGED

from viewport import Viewport

from gtkdevice import GraphicsDevice
from gtkmisc import SketchDrawingArea

try:
    from libartdevice import LibartDevice
    libart_supported = 1
except ImportError:
    print """Anti-aliased rendering will not be available"""
    libart_supported = 0



class SketchView(SketchDrawingArea, Viewport, QueueingPublisher):

    document = None

    def __init__(self, document, show_visible = 0, show_printable = 1,
                 antialiased = 0, resolution = None, **kw):
        apply(SketchDrawingArea.__init__, (self,), kw)
	Viewport.__init__(self, resolution)
	QueueingPublisher.__init__(self)

	self.move_window_count = 0
	self.show_page_outline = 1
	self.show_visible = show_visible
	self.show_printable = show_printable
        self.hadjust = self.vadjust = None

        if antialiased:
            self.antialiased = libart_supported
        else:
            self.antialiased = antialiased
        self.libart_device = None

        self.changed_objects = []

	self.init_transactions()
        self.SetDocument(document)
            
    def bind_events(self):
        SketchDrawingArea.bind_events(self)
        self.connect("destroy", self.destroy_event)

    def destroy_event(self, widget):
        self.unsubscribe_doc()
        self.document.DecRef()
        self.document = None
        self.hadjust = self.vadjust = None
        # XXX hack to make sure libart_device gets deleted to free up
        # memory. There are some refcounting bugs in pygtk at the moment
        # that make the canvas objects immortal.
        self.libart_device = None
	QueueingPublisher.Destroy(self)

    def init_gcs(self):
        SketchDrawingArea.init_gcs(self)
	self.gc.draw_visible = self.show_visible
	self.gc.draw_printable = self.show_printable
	self.gc.allow_outline = 0
        if libart_supported:
            self.libart_device = LibartDevice()
            self.libart_device.init_gc(self.window, self.gc.visual)
        else:
            self.libart_device = None
	self.FitPageToWindow()
	self.set_gc_transforms()

    def set_gc_transforms(self):
	self.gc.SetViewportTransform(self.scale, self.doc_to_win,
				     self.win_to_doc)
        if self.libart_device is not None:
            self.libart_device.SetViewportTransform(self.scale,
                                                    self.doc_to_win,
                                                    self.win_to_doc)

    #
    #	Channels
    #

    def issue_state(self):
	self.queue_message(STATE)

    def issue_view(self):
	self.queue_message(VIEW)

    def issue_document(self):
	self.doc_changed = 1

    def queue_message(self, channel):
	if self.transaction:
	    QueueingPublisher.queue_message(self, channel)
	else:
	    self.issue(channel)

    def init_transactions(self):
	self.sb_update_pending = 0
	self.doc_changed = 0
	self.transaction = 0

    def begin_transaction(self):
	self.transaction = self.transaction + 1

    def end_transaction(self):
	self.transaction = self.transaction - 1
	if self.transaction == 0:
	    if self.doc_changed:
		self.issue(DOCUMENT, self.document)
	    self.sb_update_pending = 0
	    self.doc_changed = 0
	    self.flush_message_queue()
	elif self.transaction < 0:
	    raise SketchInternalError('transaction count < 0')

    #
    #	receivers
    #

    def redraw_doc(self, all, rects = None):
	if all:
	    self.clear_window()
	else:
	    map(self.clear_area_doc, rects)

    def layout_changed(self):
	if self.show_page_outline:
	    self.clear_window()

    def layer_changed(self, *args):
	if args:
	    redraw = EmptyRect
	    if args[0] == LAYER_STATE:
		layer, visible_changed, printable_changed, outlined_changed \
		       = args[1]
		rect = layer.bounding_rect
		if rect is not EmptyRect:
		    if self.show_printable and printable_changed:
			redraw = rect
		    if self.show_visible:
			if visible_changed:
			    redraw = rect
			if outlined_changed and layer.Visible():
			    redraw = rect
	    elif args[0] == LAYER_ORDER:
		layer = args[1]
		if (self.show_printable and layer.Printable()
		    or self.show_visible and layer.Visible()):
		    redraw = layer.bounding_rect
		if len(args) > 2:
		    other = args[2]
		    if (self.show_printable and other.Printable()
			or self.show_visible and other.Visible()):
			redraw = IntersectRects(redraw, other.bounding_rect)
		    else:
			redraw = EmptyRect
	    elif args[0] == LAYER_COLOR:
		layer = args[1]
		rect = layer.bounding_rect
		if self.show_visible and rect is not EmptyRect \
		   and layer.Visible():
		    redraw = rect
	    self.clear_area_doc(redraw)

    def transaction_end(self, cause, *rest):
        if cause == "normal":
            edited, undo, insertions = rest
            if self.libart_device is not None:
                self.libart_device.objects_changed(insertions.RemovedObjects())
                self.libart_device.objects_changed(self.changed_objects)
        self.changed_objects = []

    def object_changed(self, obj, what, detail):
        self.changed_objects.append(obj)


    #
    #	Widget Methods (Redraw, ... )
    #


    time_redraw = 0
    def RedrawMethod(self, region = None):
        # draw the document
	if __debug__:
	    if self.time_redraw:
		import time
		start = time.clock()
	if self.move_window_count >= 2:
	    self.clear_window(update = 0)
        self.move_window_count = 0

	region = self.do_clear(region)
        if self.antialiased:
            self.redraw_libart(region)
        else:
            self.redraw_gdk(region)
            
	if __debug__:
	    if self.time_redraw:
		pdebug('timing', 'redraw', time.clock() - start)

        return region

    def redraw_gdk(self, region):
	# draw document
	self.gc.InitClip()
	self.gc.ResetFontCache()
	if region:
	    self.gc.PushClip()
	    self.gc.ClipRegion(region)

	if region:
	    x, y, w, h = region.ClipBox()
	else:
	    x = y = 0
            w, h = self.window_size()
	p1 = self.WinToDoc(x - 1, y - 1)
	p2 = self.WinToDoc(x + w + 1, y + h + 1)
	rect = Rect(p1, p2)

        self.gc.ClearWindowRectangle(x, y, w, h)

        #	draw paper
	if self.show_page_outline:
	    w, h = self.document.PageSize()
	    self.gc.DrawPageOutline(w, h)

	self.document.Draw(self.gc, rect)
	if region:
	    self.gc.PopClip()

    def redraw_libart(self, region):
        window = self.window
        if region:
	    x, y, w, h = region.ClipBox()
	else:
            x = y = 0
            w, h = self.window_size()
	p1 = self.WinToDoc(x - 1, y - 1)
	p2 = self.WinToDoc(x + w + 1, y + h + 1)
	rect = Rect(p1, p2)

        dev = self.libart_device
        if self.IsOutlineMode():
            dev.StartOutlineMode()
        dev.DrawDocument(self.document, rect, clip_region = region,
                         draw_page_outline = self.show_page_outline)
        if self.IsOutlineMode():
            dev.EndOutlineMode()


    def ResizedMethod(self, width, height):
	Viewport.ResizedMethod(self, width, height)
	self.gc.WindowResized(width, height)
        if self.libart_device is not None:
            self.libart_device.WindowResized(width, height)


    #
    #	Viewport- and related methods
    #
    #	extend some Viewport methods to issue VIEW whenever
    #	the displayed area changes
    #

    def ForceRedraw(self):
	# Force a redraw of the whole window
	self.clear_window()

    def set_origin(self, xorg, yorg, move_contents = 1):
	self.begin_transaction()
	try:
	    Viewport.set_origin(self, xorg, yorg,
				move_contents = move_contents)
	    self.set_gc_transforms()
	    self.issue_view()
	finally:
	    self.end_transaction()

    def move_window_contents(self, offx, offy):
	# implement the method needed by Viewport.set_origin
	w = self.window
	width, height = self.window_size()
	if abs(offx) < width and abs(offy) < height:
            gc = self.gc.gc
            if gc is not None:
                _skgtk.copy_area(w, gc, 0, 0, w, offx, offy, width, height)
                self.move_window_count = self.move_window_count + 1
	else:
	    self.clear_window()

    def SetScale(self, scale, do_center = 1):
	# Set current scale
	self.begin_transaction()
	try:
	    Viewport.SetScale(self, scale, do_center = do_center)
	    self.set_gc_transforms()
	finally:
	    self.end_transaction()

    def zoom_fit_rect(self, rect, out = 0, save_viewport = 0):
	if save_viewport:
	    self.save_viewport()
	Viewport.zoom_fit_rect(self, rect, out = out)


    #
    #	other view related methods
    #
    def FitToWindow(self, save_viewport = 1):
        self.begin_transaction()
        try:
            rect = self.document.BoundingRect()
            if rect:
                self.zoom_fit_rect(rect, save_viewport = save_viewport)
        finally:
            self.end_transaction()

    def FitPageToWindow(self, save_viewport = 1):
	self.begin_transaction()
	try:
	    w, h = self.document.PageSize()
	    self.zoom_fit_rect(Rect(0, 0, w, h).grown(10),
			       save_viewport = save_viewport)
	finally:
	    self.end_transaction()


    #
    #	Outline Mode
    #
    #	Although not directly related to the viewport methods (the outline
    #	mode doesn't change the displayed area) the outline mode changes the
    #	way the drawing is displayed and thus issues VIEW.

    def SetOutlineMode(self, on = 1):
	self.begin_transaction()
	try:
	    if on:
		if self.gc.IsOutlineActive():
		    return
		else:
		    self.gc.StartOutlineMode()
		    self.hitgc.StartOutlineMode()
	    else:
		if self.gc.IsOutlineActive():
		    self.gc.EndOutlineMode()
		    self.hitgc.EndOutlineMode()
		else:
		    return
	    self.issue_view()
	    self.clear_window()
	finally:
	    self.end_transaction()

    def ToggleOutlineMode(self):
	self.SetOutlineMode(not self.IsOutlineMode())

    def IsOutlineMode(self):
	return self.gc and self.gc.IsOutlineActive()

    #
    #	Show page outline on/off
    #

    def SetPageOutlineMode(self, on = 1):
	self.begin_transaction()
	try:
	    self.show_page_outline = on
	    self.issue_view()
	    self.clear_window()
	finally:
	    self.end_transaction()

    def TogglePageOutlineMode(self):
	self.SetPageOutlineMode(not self.IsPageOutlineMode())

    def IsPageOutlineMode(self):
	return self.show_page_outline

    #
    #
    #

    def ToggleAntialiased(self):
        # if antialiased drawing is reported, toggle whether drawing is
        # antialised.
        self.antialiased = libart_supported and not self.antialiased
        if libart_supported:
            self.ForceRedraw()
            self.libart_device.DeleteImages()
            self.issue_view()

    def IsAntialiased(self):
        # return true if antialiased drawing is switched on
        return libart_supported and self.antialiased

    def IsAntialiasedSupported(self):
        # return true is antialiased drawing is supported
        return libart_supported
    #
    #
    #

    def unsubscribe_doc(self):
        if self.document is not None:
            self.document.Unsubscribe(REDRAW, self.redraw_doc)
            self.document.Unsubscribe(LAYOUT, self.layout_changed)
            self.document.Unsubscribe(LAYER, self.layer_changed)
            self.document.Unsubscribe(TRANSACTION_END, self.transaction_end)
            self.document.Unsubscribe(OBJECT_CHANGED, self.object_changed)

    def subscribe_doc(self):
        self.document.Subscribe(REDRAW, self.redraw_doc)
        self.document.Subscribe(LAYOUT, self.layout_changed)
        self.document.Subscribe(LAYER, self.layer_changed)
        self.document.Subscribe(TRANSACTION_END, self.transaction_end)
        self.document.Subscribe(OBJECT_CHANGED, self.object_changed)

    def SetDocument(self, doc):
	self.begin_transaction()
	try:
            if self.document is not None:
                self.unsubscribe_doc()
                self.document.DecRef()
	    self.document = doc
            self.document.IncRef()
            self.subscribe_doc()
	    self.clear_window()
	    self.SetPageSize(self.document.Layout().Size())
	    self.FitPageToWindow(save_viewport = 0)
	    self.issue_document()
	    self.issue_state()
	    self.issue_view()
	finally:
	    self.end_transaction()

