# Sketch - A Python-based interactive drawing program
# Copyright (C) 1996, 1997, 1998, 1999, 2000, 2003, 2005 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from types import IntType
from math import hypot

import gtk, gtk.keysyms

import Sketch
from Sketch import _, Point
from Sketch.Base.warn import pdebug
from Sketch.Base.const import POSITION, VIEW

from gtkdevice import HitTestDevice, InvertingDevice
from view import SketchView
import skapp
import skpixmaps, cursors
bitmaps = skpixmaps.bitmaps

constraint_keysyms =   {gtk.keysyms.Control_L : Sketch.Editor.ControlMask,
                        gtk.keysyms.Control_R : Sketch.Editor.ControlMask,
                        gtk.keysyms.Shift_L   : Sketch.Editor.ShiftMask,
                        gtk.keysyms.Shift_R   : Sketch.Editor.ShiftMask}

# physical handle types
# for rect/circle handles: filled == handle_id & 1
Handle_OpenRect         = 0
Handle_FilledRect       = 1
Handle_SmallOpenRect    = 2
Handle_SmallFilledRect  = 3

Handle_OpenCircle       = 4
Handle_FilledCircle     = 5
Handle_SmallOpenCircle  = 6
Handle_SmallFilledCircle = 7

Handle_SmallOpenRectList = 8

Handle_Line             = 9
Handle_Pixmap           = 10
Handle_Caret            = 11
Handle_PathText         = 12

#Handle_RectList = 13
#Handle_SmallRectList = 14

e = Sketch.Editor
handle_info = {
    e.HandleNode: (Handle_OpenRect, None, 'CurHandle', ''),
    e.HandleSelectedNode: (Handle_FilledRect, None, 'CurHandle', ''),
    e.HandleCurvePoint: (Handle_FilledCircle, None, '', ''),
    e.HandleControlPoint:(Handle_SmallFilledRect, None, 'CurHandle', ''),
    e.HandleLine: (Handle_Line, None, '', ''),
    e.HandleCaret: (Handle_Caret, None, '', ''),
    e.HandlePathText: (Handle_PathText, None, '', ''),
    e.HandleIndicatorList: (Handle_SmallOpenRectList, None, '', ''),
#    e.HandleNodeList: (Handle_RectList, None, 'CurHandle', ''),
#    e.HandleControlPointList: (Handle_SmallRectList, None, 'CurHandle', ''),

    e.HandleTurnTL: (Handle_Pixmap, (-1, +1), 'CurTurn', 'TurnTL'),
    e.HandleTurnTR: (Handle_Pixmap, (+1, +1), 'CurTurn', 'TurnTR'),
    e.HandleTurnBL: (Handle_Pixmap, (-1, -1), 'CurTurn', 'TurnBL'),
    e.HandleTurnBR: (Handle_Pixmap, (+1, -1), 'CurTurn', 'TurnBR'),
    e.HandleShearT: (Handle_Pixmap, (0, 1), 'CurTurn', 'ShearLR'),
    e.HandleShearB: (Handle_Pixmap, (0, -1), 'CurTurn', 'ShearLR'),
    e.HandleShearL: (Handle_Pixmap, (-1, 0), 'CurTurn', 'ShearUD'),
    e.HandleShearR: (Handle_Pixmap, (1, 0), 'CurTurn', 'ShearUD'),
    e.HandleCenter: (Handle_Pixmap, (0, 0), 'CurHandle', 'Center'),

    e.HandleSizeTL: (Handle_FilledRect, (-1, +1), 'CurSizeTL', ''),
    e.HandleSizeT: (Handle_FilledRect, (0, 1), 'CurSizeT', ''),
    e.HandleSizeTR: (Handle_FilledRect, (+1, +1), 'CurSizeTR', ''),
    e.HandleSizeL: (Handle_FilledRect, (-1, 0), 'CurSizeL', ''),
    e.HandleSizeR: (Handle_FilledRect, (1, 0), 'CurSizeR', ''),
    e.HandleSizeBL: (Handle_FilledRect, (-1, -1), 'CurSizeBL', ''),
    e.HandleSizeB: (Handle_FilledRect, (0, -1), 'CurSizeB', ''),
    e.HandleSizeBR: (Handle_FilledRect, (+1, -1), 'CurSizeBR', ''),

    e.HandleAlignTL: (Handle_Pixmap, (-2, +2), 'CurSizeTL', 'AlignTL'),
    e.HandleAlignTC: (Handle_Pixmap, (0, +5), 'CurVGuide', 'AlignTC'),
    e.HandleAlignT: (Handle_Pixmap, (0, 2), 'CurSizeT', 'AlignT'),
    e.HandleAlignTR: (Handle_Pixmap, (+2, +2), 'CurSizeTR', 'AlignTR'),
    e.HandleAlignL: (Handle_Pixmap, (-2, 0), 'CurSizeL', 'AlignL'),
    e.HandleAlignLC: (Handle_Pixmap, (-5, 0), 'CurHGuide', 'AlignLC'),
    e.HandleAlignR: (Handle_Pixmap, (2, 0), 'CurSizeR', 'AlignR'),
    e.HandleAlignBL: (Handle_Pixmap, (-2, -2), 'CurSizeBL', 'AlignBL'),
    e.HandleAlignB: (Handle_Pixmap, (0, -2), 'CurSizeB', 'AlignB'),
    e.HandleAlignBR: (Handle_Pixmap, (+2, -2), 'CurSizeBR', 'AlignBR'),
    e.HandleAlignC: (Handle_Pixmap, (0, 0), 'CurHandle', 'AlignC'),
    }

del e

def translate_state(state):
    # translate the modifier/button state from gtk to the Editor
    # constants. Since the editor constants are the same as in gtk this
    # isn't really necessary, but it might be needed when Sketch is ever
    # ported to a different toolkit.
    return state

class SketchCanvas(SketchView):

    def __init__(self, application, main_window, document, editor, **kw):
        self.init_handles()
        kw['show_visible'] = 1
        kw['show_printable'] = 0
        kw['antialiased'] = 1
        apply(SketchView.__init__, (self, document), kw)
        self.application = application
        self.main_window = main_window
        self.editor = editor
        self.cursor_shape = cursors.CurStd
        self.start_event = None
        self.start_drag = 0
        self.dragging = 0
        self.cancelled = 0
        self.subscribe_editor()
        self.active_cursor = 0
        self.set_flags(gtk.CAN_FOCUS)
        self.current_pos = Point(0, 0)

    def __del__(self):
        pass
        #print '__del__', self

    def Destroy(self):
        self.unsubscribe_editor()
        self.editor = None
        self.main_window = None
        self.gc = self.invgc = self.hitgc = None
        SketchView.Destroy(self)

    def realize_method(self, *args):
        """Extend inherited method so that the cursors are set properly"""
        SketchView.realize_method(self, *args)
        self.tool_changed(self.editor.toolname)

    def init_gcs(self):
        self.invgc = InvertingDevice()
        self.invgc.init_gc(self.window)
        self.hitgc = HitTestDevice()
        self.hitgc.init_gc(self.window)
        self.draw_handle_funcs = [self.invgc.DrawRectHandle,
                                  self.invgc.DrawRectHandle,
                                  self.invgc.DrawSmallRectHandle,
                                  self.invgc.DrawSmallRectHandle,
                                  self.invgc.DrawCircleHandle,
                                  self.invgc.DrawCircleHandle,
                                  self.invgc.DrawSmallCircleHandle,
                                  self.invgc.DrawSmallCircleHandle,
                                  self.invgc.DrawSmallRectHandleList,
                                  self.invgc.DrawHandleLine,
                                  self.invgc.DrawPixmapHandle,
                                  self.invgc.DrawCaretHandle]
        SketchView.init_gcs(self)
        self.gc.allow_outline = 1

    def set_gc_transforms(self):
        SketchView.set_gc_transforms(self)
        if self.gcs_initialized:
            self.invgc.SetViewportTransform(self.scale, self.doc_to_win,
                                            self.win_to_doc)
            self.hitgc.SetViewportTransform(self.scale, self.doc_to_win,
                                            self.win_to_doc)

    def subscribe_editor(self):
        self.editor.Subscribe(Sketch.Base.const.HANDLES,
                              self.update_handles_idle)
        self.editor.Subscribe(Sketch.Base.const.TOOL,
                              self.tool_changed)

    def unsubscribe_editor(self):
        self.editor.Unsubscribe(Sketch.Base.const.TOOL,
                                self.tool_changed)
        self.editor.Unsubscribe(Sketch.Base.const.HANDLES,
                                self.update_handles_idle)

    def SetTool(self, toolname):
        self.editor.SetTool(toolname)

    def tool_changed(self, toolname):
        info = Sketch.Editor.toolmap[toolname]
        cursor = getattr(cursors, info.cursor)
        self.cursor_shape = cursor
        self.set_window_cursor(cursor)
        self.active_cursor = info.active_cursor

    def bind_events(self):
        SketchView.bind_events(self)
        self.add_events(gtk.gdk.POINTER_MOTION_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK
                        | gtk.gdk.BUTTON_RELEASE_MASK
                        | gtk.gdk.POINTER_MOTION_HINT_MASK
                        | gtk.gdk.KEY_PRESS_MASK | gtk.gdk.KEY_RELEASE_MASK)
        self.connect('button_press_event', self.button_press)
        self.connect('motion_notify_event', self.motion_notify)
        self.connect('button_release_event', self.button_release)
        self.connect_after('key_press_event', self.key_press)
        self.connect_after('key_release_event', self.key_release)

    def button_press(self, widget, event):
        self.application.SetContext(self.main_window)
        if event.button == 1:
            # For some reason the event object is always the same in
            # PyGTK 1.99. So copy it explicitly
            self.start_event = event.copy()
            self.start_drag = 1
            self.cancelled = 0
            if not self.editor.DelayButtonPress():
                self.begin_drag(event)
        elif event.button == 3:
            self.popup_context_menu(event)

    def button_release(self, widget, event):
        self.start_event = None
        self.start_drag = 0
        if event.window == self.window:
            p = self.win_to_doc(event.x, event.y)
        else:
            x, y = self.get_pointer()
            p = self.win_to_doc(x, y)
        if self.cancelled and event.button == 1:
            return
        state = translate_state(event.state)
        if self.dragging:
            self.dragging = 0
            self.editor.ButtonRelease(self, p, event.button, state)
        else:
            handle = self.handle_hit(event.x, event.y)
            self.editor.ButtonClick(self, p, event.button, state, handle)

    def motion_notify(self, widget, event):
        if event.is_hint:
            x, y = self.get_pointer()
        else:
            x = event.x
            y = event.y
        if self.start_drag:
            sx = self.start_event.x
            sy = self.start_event.y
            if hypot(x - sx, y - sy) >= 3:
                self.begin_drag(self.start_event)
        p = self.win_to_doc(x, y)
        self.set_current_pos(p)
        if self.dragging:
            self.editor.PointerMotion(self, p, translate_state(event.state))
        elif self.active_cursor:
            self.set_handle_cursor(event.x, event.y)

    def begin_drag(self, event):
        handle = self.handle_hit(event.x, event.y)
        p = self.win_to_doc(event.x, event.y)
        self.editor.ButtonPress(self, p, event.button,
                                translate_state(event.state), handle)
        self.dragging = 1
        self.start_drag = 0

    def fake_motion_event(self, event):
        if self.dragging:
            x, y = self.get_pointer()
            p = self.win_to_doc(x, y)
            state = translate_state(event.state)
            state = state ^ constraint_keysyms[event.keyval]
            self.editor.PointerMotion(self, p, state)


    def key_press(self, widget, event):
        self.application.SetContext(self.main_window)
        #print 'key_press:', event.keyval, `event.string`, event.state
        if constraint_keysyms.has_key(event.keyval):
            # pretend a motion event has arrived if a modifier key was
            # pressed to update the display to reflect whatever
            # constraints are triggered by the modifier.
            self.fake_motion_event(event)
        elif event.keyval == gtk.keysyms.Escape:
            self.cancel()
        else:
            #print event.keyval, event.state
            state = translate_state(event.state)
            state = state & Sketch.Editor.AllowedModifierMask
            if len(event.string) == 1 and state & Sketch.Editor.ShiftMask:
                keyval = ord(event.string)
                state = state & ~Sketch.Editor.ShiftMask
            else:
                keyval = event.keyval
            if 0x041 <= keyval <= 0x05a:
                keyval = keyval + 32
            #print keyval, event.string, state
            command = self.lookup_key(keyval, state, event.string)
            #print command
            if command is not None:
                command = Sketch.Editor.Registry.Command(command)
                if command is not None:
                    context = self.application.context
                    if command.Sensitive(context):
                        if command.PassKey():
                            command.Execute(context, event.string)
                        else:
                            command.Execute(context)
        return 1

    def key_release(self, widget, event):
        #print 'key_release:', event.keyval, `event.string`, event.state
        if constraint_keysyms.has_key(event.keyval):
            self.fake_motion_event(event)
        return 1

    def lookup_key(self, keyval, state, str):
        tool = self.application.context.tool
        # XXX implement a better mechanism for context dependent keymaps
        if tool == 'TextTool':
            #obj = self.editor.SelectedObjects()
            #if obj:
            #    obj = obj[0]
            #    if obj.is_Text:
            command = skapp.text_keymap.Command((keyval, state))
            if command is not None:
                return command
            if self.is_normal_char(keyval, state, str):
                return 'insert_char'
        elif tool == 'AlignTool':
            command = skapp.align_keymap.Command((keyval, state))
            if command is not None:
                return command
        return skapp.keymap.Command((keyval, state))

    def is_normal_char(self, keyval, state, str):
        return len(str) == 1 and state & ~Sketch.Editor.ShiftMask == 0

    #

    def cancel(self):
        self.editor.Cancel(self)
        if self.dragging or self.start_drag:
            self.cancelled = 1
        self.dragging = 0
        self.start_drag = 0

    #

    def popup_context_menu(self, event):
        menu = self.application.Menu(self.main_window)
        menu.popup(None, None, None, event.button, event.time)

    #
    #   Report the current pointer position in doc coords.
    #   (might be also interesting for the view)
    #

    def set_current_pos(self, p):
        if Sketch.UI.preferences.snap_current_pos:
            p = self.editor.SnapPoint(self, p)
        self.current_pos = p
        self.issue(POSITION)

    def CurrentPos(self):
        return self.current_pos

    #

    def ResizedMethod(self, width, height):
        SketchView.ResizedMethod(self, width, height)

    def RedrawMethod(self, region = None):
        region = SketchView.RedrawMethod(self, region)

        # draw the handles
        self.invgc.InitClip()
        if region:
            self.invgc.PushClip()
            self.invgc.ClipRegion(region)
        #if self.current is not None:
        #    self.current.DrawDragged(self.invgc, 0)
        #else:
        self.show_handles(1)
        if region:
            self.invgc.PopClip()
        #self.show_handles()


    #
    #   Handles
    #

    def init_handles(self):
        self.handle_points = None
        self.handle_funcs = None
        self.handles_drawn = 0
        self.handles_idle_tag = None

    def update_handles_idle(self):
        if self.handles_idle_tag is None:
            self.handles_idle_tag = gtk.idle_add(self.update_handles)

    def update_handles(self, redraw = 1):
        self.handles_idle_tag = None
        self.set_handles(self.editor.Handles(), redraw)

    def update_handles_lazily(self):
        self.handle_points = self.handle_funcs = None

    def set_handles(self, handles, redraw = 1):
        draw_handle_funcs = self.draw_handle_funcs
        DocToWin = self.DocToWin
        points = []
        funcs = []
        lines = []
        points_used = {}
        if self.gcs_initialized:
            factor = self.gc.LengthToDoc(8)
        else:
            factor = 1

        for handle in handles:
            handle_type = handle.type
            p = handle.p
            cursor = pixmap = offset = None

            info = handle_info.get(handle_type)
            if info is not None:
                handle_type, offset, cursor, pixmap = info
                if pixmap:
                    pixmap = getattr(bitmaps, pixmap)
                if cursor:
                    cursor = getattr(cursors, cursor)

            if offset is not None:
                p = p + Point(offset[0] * factor, offset[1] * factor)

            if handle_type == Handle_Line:
                lines.append((p, handle.p2))
            elif handle_type == Handle_Pixmap:
                if cursor:
                    points.append(self.DocToWin(p) + (cursor, handle))
                funcs.append((draw_handle_funcs[handle_type], (p, pixmap)))
            elif handle_type == Handle_Caret:
                funcs.append((self.invgc.DrawCaretHandle, (p, handle.p2)))
            elif handle_type == Handle_SmallOpenRectList:
                p = handle.list
                if p:
                    pts = map(DocToWin, p)
                    last = pts[-1]
                    for idx in range(len(p) - 2, -1, -1):
                        if pts[idx] == last:
                            del p[idx]
                        else:
                            last = pts[idx]
                    funcs.append((draw_handle_funcs[handle_type], (p, 0)))
            else:
                win = DocToWin(p)
                if cursor:
                    points.append(win + (cursor, handle))
                key = (win, handle_type)
                if not points_used.has_key(key):
                    funcs.append((draw_handle_funcs[handle_type],
                                  (p, handle_type & 1)))
                    points_used[key] = 0
        if redraw:
            self.hide_handles()

        if lines:
            funcs.append((self.invgc.DrawHandleLines, (lines,)))
        self.handle_points = points
        self.handle_funcs  = funcs
        if redraw:
            self.show_handles()

    def show_handles(self, force = 0):
        if __debug__:
            pdebug('handles', 'show_handles: drawn = %d, force = %d',
                   self.handles_drawn, force)
        if not self.handles_drawn or force:
            self.draw_handles()
            self.handles_drawn = 1

    def hide_handles(self):
        if __debug__:
            pdebug('handles', 'hide_handles: drawn = %d', self.handles_drawn)
        if self.handles_drawn:
            self.draw_handles()
            self.handles_drawn = 0

    def draw_handles(self):
        if self.handle_funcs is None:
            self.update_handles(0)
        for f, args in self.handle_funcs:
            apply(f, args)


    handle_hit_radius = 4
    def set_handle_cursor(self, x, y):
        if self.handle_points is None:
            self.update_handles(0)
        dist = self.handle_hit_radius
        for xp, yp, cursor, handle in self.handle_points:
            if abs(xp - x) < dist and abs(yp - y) < dist:
                self.set_window_cursor(cursor)
                break
        else:
            if self.application.context.canvas is self:
                cursor = self.editor.Cursor(self.application.context,
                                            self.win_to_doc(x, y))
                if cursor:
                    self.set_window_cursor(getattr(cursors, cursor))
                else:
                    self.set_window_cursor(self.cursor_shape)
            else:
                self.set_window_cursor(self.cursor_shape)

    def handle_hit(self, x, y):
        if self.handle_points is None:
            self.update_handles(0)
        dist = self.handle_hit_radius
        handle_points = self.handle_points
        for idx in range(len(handle_points)):
            xp, yp, cursor, handle = handle_points[idx]
            if abs(xp - x) < dist and abs(yp - y) < dist:
                return handle
        return None

    last_cursor = None
    def set_window_cursor(self, cursor):
        if cursor != self.last_cursor:
            self.last_cursor = cursor
            if type(cursor) is gtk.gdk.CursorType:
                cursor = gtk.gdk.Cursor(cursor)
                # else, assume cursor is a cursor object
            self.window.set_cursor(cursor)

    #
    #
    #

    def FitSelectedToWindow(self, save_viewport = 1):
        self.begin_transaction()
        try:
            rect = self.editor.SelectionBoundingRect()
            if rect:
                self.zoom_fit_rect(rect, save_viewport = save_viewport)
        finally:
            self.end_transaction()

    #
    #   override some inherited methods
    #

    def set_origin(self, xorg, yorg, move_contents = 1):
        SketchView.set_origin(self, xorg, yorg,
                              move_contents = move_contents)
        self.update_handles_lazily()

    def SetScale(self, scale, do_center = 1):
        SketchView.SetScale(self, scale, do_center = do_center)
        self.update_handles_lazily()

    #
    #  context for the doc editor
    #

    def test_device(self):
        return self.hitgc

    def inverting_device(self):
        return self.invgc

    def zoom_area(self, rect, out = 0):
        self.zoom_fit_rect(rect, out = out)

    def zoom_point(self, p, out = 0):
        if out:
            scale = 0.5 * self.scale
        else:
            scale = 2 * self.scale
        self.SetScale(scale / self.pixel_per_point, do_center = 0)
        self.SetCenter(p, move_contents = 0)

    def max_snap_distance(self):
        # Return the maximal snap distance in document coordinates
        return Sketch.UI.preferences.max_snap_distance / self.scale



_ = Sketch._

add = Sketch.Editor.AddEditorBuiltin

add('zoom_100', ''"1:1",
    lambda context, scale: context.canvas.SetScale(scale),
    args = (1,))

def zoom_factor(context, factor):
    canvas = context.canvas
    canvas.SetScale(factor * canvas.Scale())

add('zoom_in', ''"Zoom In", zoom_factor, args = (2.0,))
add('zoom_out', ''"Zoom Out", zoom_factor, args = (0.5,))

add('view_fit_window', ''"Fit to Window",
    lambda context: context.canvas.FitToWindow())
add('view_fit_selection_to_window', ''"Fit Selected to Window",
    lambda context: context.canvas.FitSelectedToWindow(),
    sensitive = lambda context: context.editor.HasSelection(),
    channels = (Sketch.Base.const.SELECTION,))

add('view_fit_page_to_window', ''"Fit Page to Window",
        lambda context: context.canvas.FitPageToWindow())


add('view_libart', ''"Antialiased",
    lambda context: context.canvas.ToggleAntialiased(),
    checked = lambda context: context.canvas.IsAntialiased(),
    sensitive = lambda context: context.canvas.IsAntialiasedSupported(),
    channels = (VIEW,))

add('view_outlined', ''"Outlined",
        lambda context: context.canvas.ToggleOutlineMode(),
        checked = lambda context: context.canvas.IsOutlineMode(),
        channels = (VIEW,))
