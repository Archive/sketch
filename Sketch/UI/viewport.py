# Sketch - A Python-based interactive drawing program
# Copyright (C) 1997, 1998, 1999, 2000, 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import Sketch
from Sketch import Trafo, Point, RectType, EmptyRect, InfinityRect, _skgtk
from Sketch.Graphics.papersize import Papersize


def modify_adjustment(adjustment, **kw):
    _skgtk.set_adjustment(adjustment, kw)
    adjustment.changed()

class Viewport:

    max_scale = 16
    min_scale = 0.05

    def __init__(self, resolution = None):
	self.base_width = 1200
	self.base_height = 1200
	self.nominal_scale = 1
	self.scale = 1.0
	self.virtual_width = self.base_width
	self.virtual_height =  self.base_height
	self.virtual_x = 0
	self.virtual_y = 0
	self.init_resolution(resolution)
	self.set_page_size(Papersize['A4']) # XXX config
	self.compute_win_to_doc()
	self.clear_rects = []
	self.clear_entire_window = 0

	self.init_viewport_ring()

        self.hruler = self.vruler = None

    def window_size(self):
        """Return the size of the window as a tuple (width, height)"""
        if self.window is not None:
            width, height = self.window.get_size()
        else:
            width = height = 10
        return width, height

    def init_resolution(self, resolution):
        # XXX gtk
        self.pixel_per_point = 1.0
        return
    
	if resolution is not None:
	    self.pixel_per_point = resolution
	else:
	    width = self.winfo_screenwidth()
	    width_mm = self.winfo_screenmmwidth()
	    pixel_per_point = (width / float(width_mm)) * 25.4 / 72
	    self.pixel_per_point = float(pixel_per_point)
	self.max_scale = self.__class__.max_scale * self.pixel_per_point

    #
    #	Coordinate conversions
    #

    def set_page_size(self, (width, height)):
	self.page_width = width
	self.page_height = height

    def compute_win_to_doc(self):
	scale = self.scale
	# virtual coords of ll corner of page
	llx = round((self.virtual_width - self.page_width * scale) / 2)
	lly = round((self.virtual_height + self.page_height * scale) / 2)
	self.doc_to_win = Trafo(scale, 0.0, 0.0, -scale,
				llx - self.virtual_x, lly - self.virtual_y)
        self.win_to_doc = self.doc_to_win.inverse()

    #	document to window coordinates

    def DocToWin(self, *args):
	# returns a tuple of ints
	return apply(self.doc_to_win.DocToWin, args)

    #	window to document coordinates

    def WinToDoc(self, *args):
	return apply(self.win_to_doc, args)

    def LengthToDoc(self, len):
	return len / self.scale

    #
    #	Redraw Parts of the window.
    #

    def clear_area_doc(self, rect):
	# Mark the rectangular area of self, given by RECT in document
	# coordinates, as invalid. The rect is put into a list of rects
	# that have to be cleared via do_clear() once RedrawMethod() is
	# invoked.
	if not self.clear_entire_window:
	    if rect is EmptyRect:
		return
	    elif rect is InfinityRect:
		self.clear_window()
	    elif type(rect) == RectType:
		x1, y1 = self.DocToWin(rect.left, rect.top)
		x2, y2 = self.DocToWin(rect.right, rect.bottom)
		if x1 > x2:
		    t = x1; x1 = x2; x2 = t
		if y1 > y2:
		    t = y1; y1 = y2; y2 = t
		self.clear_rects.append((x1 - 1, y1 - 1,
					 x2 - x1 + 2, y2 - y1 + 2))
	    else:
		# special case: clear a guide line
		p, horizontal = rect
		x, y = self.DocToWin(p)
                width, height = self.window_size()
		if horizontal:
		    self.clear_rects.append((0, y, width, 1))
		else:
		    self.clear_rects.append((x, 0, 1, height))
	self.UpdateWhenIdle()

    def clear_window(self, update = 1):
	# Mark the entire window as invalid.
	self.clear_entire_window = 1
	self.clear_rects = []
	if update:
	    self.UpdateWhenIdle()

    def do_clear(self, region):
	# Clear all areas marked as invalid by clear_area_doc() or
	# clear_window(). These areas are added to REGION via its
	# UnionRectWithRegion method. This function should be called by
	# RedrawMethod() before any drawing is done.
	if region and not self.clear_entire_window:
	    union = region.UnionRectWithRegion
	    for rect in self.clear_rects:
		apply(union, rect)
	else:
	    _skgtk.clear_area(self.window, 0, 0, 0, 0)
	    region = None
	self.clear_entire_window = 0
	self.clear_rects = []
	return region

    #
    #
    #

    def set_origin(self, xorg, yorg, move_contents = 1):
	old_org_x = xo = self.virtual_x
	old_org_y = yo = self.virtual_y
	if xorg is not None:
	    xo = xorg
	if yorg is not None:
	    yo = yorg
	offx = round(xo - old_org_x)
	offy = round(yo - old_org_y)
	xo = old_org_x + offx
	yo = old_org_y + offy
	self.virtual_x = xo
	self.virtual_y = yo
	offx = int(offx)
	offy = int(offy)
	if move_contents and (offx or offy):
	    self.move_window_contents(offx, offy)
	self.compute_win_to_doc()
	self.update_scrollbars()


    def move_window_contents(self, offx, offy):
	# Noop here, because moving the window contents requires a gc.
	# The implementation could be as follows:
	#
	#	w = self.window
	#	width = w.width
	#	height = w.height
	#	w.CopyArea(w, self.gc.gc, offx, offy, width, height, 0, 0)
	pass


    #
    #	Managing the displayed area
    #

    def SetCenter(self, center, move_contents = 1):
	# set origin so that center (in doc-coords) is in the center of the
	# widget
	cx, cy = self.DocToWin(center)
        width, height = self.window_size()
	self.set_origin(self.virtual_x + cx - width / 2,
			self.virtual_y + cy - height / 2,
			move_contents = move_contents)

    def SetScale(self, scale, do_center = 1):
	# Set current scale
	scale = scale * self.pixel_per_point
	if scale > self.max_scale:
	    scale = self.max_scale
	elif scale < self.min_scale:
	    scale = self.min_scale
	self.scale = scale
	self.nominal_scale = scale / self.pixel_per_point
	width = int(scale * self.base_width)
	height = int(scale * self.base_height)
	self.virtual_width = width
	self.virtual_height = height
	if do_center:
            window_width, window_height = self.window_size()
	    cx = window_width / 2
	    cy = window_height / 2
	    center = self.WinToDoc(cx, cy)

	self.compute_win_to_doc()

	if do_center:
	    self.SetCenter(center, move_contents = 0)
	else:
	    self.set_origin(0, 0, move_contents = 0)

	self.gc.SetViewportTransform(scale, self.doc_to_win, self.win_to_doc)

	self.clear_window()

    def SetPageSize(self, size):
	self.set_page_size(size)
	self.compute_win_to_doc()

    def Scale(self):
        return self.scale

    #
    #	Some convenient methods
    #

    def zoom_fit_rect(self, rect, out = 0):
        # set the scale and origin according to rect and out. If out is
        # false, scale and move so that the document rectangle rect is
        # centered and fills the window. If out is true, scale and move
        # so that the rectangle currently filling the viewport fits into
        # rect.
        if self.window is None:
            return

        epsilon = 1e-10

        rw = rect.right - rect.left
        rh = rect.top - rect.bottom
        if abs(rw) < epsilon or abs(rh) < epsilon:
            return
        width, height = self.window_size()
        scalex = width / rw
        scaley = height / rh

        if out:
            scale = (self.scale ** 2) / max(scalex, scaley)
            x, y = self.DocToWin(rect.center())
            center = rect.center() + Point(width/2 - x, -height/2 + y) / scale
        else:
            scale = min(scalex, scaley)
            center = rect.center()

        self.SetScale(scale / self.pixel_per_point, do_center = 0)
        self.SetCenter(center, move_contents = 0)

    def ZoomInfoText(self):
	# Return a string that describes the current zoom factor in percent.
	# Usually this is displayed in a status bar.
        # NLS
	return "%g%%" % round(100 * self.nominal_scale, 1)


    #
    #	Scrollbar handling
    #

    def ResizedMethod(self, width, height):
	self.update_scrollbars()

    def SetAdjustments(self, hadjust, vadjust):
	self.hadjust = hadjust
        self.hadjust.connect('value_changed', self.hadjust_changed)
	self.vadjust = vadjust
        self.vadjust.connect('value_changed', self.vadjust_changed)

    def SetRulers(self, hruler, vruler):
	self.hruler = hruler
	self.vruler = vruler
        
    def hadjust_changed(self, adjust):
        self.set_origin(adjust.value, None)

    def vadjust_changed(self, adjust):
        self.set_origin(None, adjust.value)

    def update_adjustment(self, adj, upper, page_size, overlap, step, value):
        lower = 0
        if value < lower:
            lower = value
        elif value > upper:
            upper = value
        if page_size > upper:
            page_size = upper
        if page_size > overlap:
            page_increment = page_size - overlap
        else:
            page_increment = page_size
        if step > page_increment:
            step = 1
        if adj.upper != upper or adj.lower != lower \
           or adj.page_size != page_size:
            #modify_adjustment(adj, lower = lower, upper = upper,
            #                  page_size = page_size,
            #                  page_increment = page_increment,
            #                  step_increment = step)
            adj.set_all(value, lower, upper, step, page_increment, page_size)
        if adj.value != value:
            adj.set_value(value) 

    def update_scrollbars(self):
        if self.window is None:
            return
        width, height = self.window_size()
        overlap = 20
        step = 20
        if self.hadjust is not None:
            self.update_adjustment(self.hadjust, self.virtual_width,
                                   width, overlap, step,
                                   self.virtual_x)

        if self.vadjust is not None:
            self.update_adjustment(self.vadjust, self.virtual_height,
                                   height, overlap, step,
                                   self.virtual_y)

        start = self.WinToDoc(0, 0)
        end = self.WinToDoc(width, height)
        if self.hruler is not None:
            self.hruler.set_property("lower", start.x)
            self.hruler.set_property("upper", end.x)
            self.hruler.set_property("max_size", 100.0)
        if self.vruler is not None:
            self.vruler.set_property("lower", start.y)
            self.vruler.set_property("upper", end.y)
            self.vruler.set_property("max_size", 100.0)


    #
    #	 the viewport ring.
    #

    def init_viewport_ring(self):
	self.viewport_ring = []

    def save_viewport(self):
	self.viewport_ring.insert(0,
				  (self.scale, self.virtual_x, self.virtual_y))
	length = Sketch.UI.preferences.viewport_ring_length
	self.viewport_ring = self.viewport_ring[:length]

    def restore_viewport(self):
	if self.viewport_ring:
	    scale, vx, vy = self.viewport_ring[0]
	    del self.viewport_ring[0]
	    self.SetScale(scale / self.pixel_per_point, do_center = 0)
	    self.set_origin(vx, vy, move_contents = 0)
