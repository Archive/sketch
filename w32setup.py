
# Sketch setup for windows

# Adjust the appropriate comments to enable Libart
# http://gnuwin32.sourceforge.net/packages/libart.htm

from distutils.core import setup, Extension
import glob, os

libraries = ["gtk-win32-2.0", "gdk-win32-2.0", "gmodule-2.0",
             "gobject-2.0", "glib-2.0"
# Remove comment to enable Libart
#            , "libart_lgpl"
            ]

python_dir = "C:/python23/"
gtk_base = "C:/Dev-C++/"

pil_dir = python_dir +"include/pil"
libdirs = [gtk_base + "lib"]
gtk_dirs = [gtk_base + "include/gtk-2.0/", gtk_base + "include/glib-2.0",
            gtk_base + "include/pango-1.0", gtk_base + "lib/glib-2.0/include",
            gtk_base + "lib/gtk-2.0/include", gtk_base + "include/atk-1.0",
            gtk_base + "include/", python_dir + "include/pygtk-2.0"]

mod_dir = "Sketch/Modules/"
filter_dir = "Filter/"

packages = ["Sketch", 'Sketch.Base', 'Sketch.Editor',
            'Sketch.Graphics', 'Sketch.Lib',
            'Sketch.Plugin', 'Sketch.UI']

version = file("Sketch/VERSION").read().strip()

ext_modules=[
    Extension("Sketch.Modules._type1", [mod_dir+"_type1module.c"]),

    Extension("Sketch.Modules._sketch",
                    [mod_dir+filename for filename in ("_sketchmodule.c",
                    "skpoint.c", "sktrafo.c", "skrect.c", "skfm.c",
                    "curvefunc.c", "curveobject.c", "curvelow.c",
                    "curvemisc.c", "curvefit.c", "skaux.c", "skimage.c",
                    "skcolor.c")], include_dirs=[pil_dir],
                    libraries=libraries, library_dirs=libdirs),

    Extension("Sketch.Modules.skread", [mod_dir+"skreadmodule.c"]),

    Extension("Sketch.Modules.pstokenize", [mod_dir+"pstokenize.c"],
                    include_dirs=[filter_dir]),

# Remove comments to enable Libart
#    Extension("Sketch.Modules._libart", [mod_dir+"_libartmodule.c",
#                    mod_dir+"skrender.c"], library_dirs=libdirs,
#                    libraries=libraries, include_dirs=gtk_dirs+[pil_dir]),

    Extension("Sketch.Modules._skgtk",
                    [mod_dir+filename for filename in ("_skgtkmodule.c",
                    "regionobject.c", "imageobject.c", "clipmask.c",
                    "skvisual.c", "skgtkimage.c", "skdither.c",
                    "curvedraw.c")], include_dirs=gtk_dirs+[pil_dir],
# Comment the following line to enable Libart
                    define_macros=[('NO_LIBART', None)],
                    libraries=libraries, library_dirs=libdirs)]

filter_globals = {}
execfile(os.path.join("Filter", "setup.py"), filter_globals)
define_filter_extensions = filter_globals["define_extensions"]
ext_modules.extend(define_filter_extensions(0, dir_prefix = "Filter/",
                                           module_prefix = "Filter."))



datafile_basedir="Lib/site-packages/Sketch"

data_files=[]

datafile_filters = ["sketch.py", "Plugins/*/*.py", "Sketch/VERSION",
                    "Sketch/Pixmaps/*.xbm", "Sketch/Pixmaps/*.xpm",
                    "Resources/Fontmetrics/*.afm",
                    "Resources/Fontmetrics/*.sfd", "Resources/Misc/*"]

for filter in datafile_filters:
    files = glob.glob(filter)
    for file in files:
        if os.path.isdir(file):
            continue
        dir = os.path.dirname(file)
        data_files.append((os.path.join(datafile_basedir, dir), [file]))

scripts=["w32/sketch_postinstall.py"]

long_description = """\
Sketch is an interactive vector drawing program
"""

setup(name = "Sketch",
      version = "2003-11-26",
      description = "Vector drawing program",
      long_description = long_description,
      license = "LGPL",
      author = "Bernhard Herzog",
      author_email = "bh@intevation.de",
      url = "http://sketch.sourceforge.net/",

      packages=packages, ext_modules=ext_modules, data_files=data_files,
      scripts=scripts, extra_path="Sketch")
