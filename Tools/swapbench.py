# how fast are swaps?
#
# For swapping the values of two variables `a' and `b', there are
# basically two idioms in Python:
#
# 1. The traditional approach with a temporary variable:
#
#       temp = a; a = b; b = temp
#
# 2. Multiple assignments
#
#       a, b = b, a
#
# It turns out, that in the standard Pythin distribution (at least up to
# Python 1.5.1), the traditional method is *faster* (for local
# variables). At least on my machine (Linux, Pentium 166):
#
# 1000000 traditional swaps in 5.06 seconds
# 1000000 multi-assignment swaps in 5.9 seconds
#
# For most python programs this speed difference is not really an issue
# and the question of which of the idioms to use is a matter of taste.
#
# An optimizing byte code compiler could optimize the multi-assignment
# by not actually creating the tuple.

import time

num = 1000
r = range(num)


def traditional():
    x = 1
    y = 0

    t1 = time.clock()
    for i in r:
	for j in r:
	    q = x
	    x = y
	    y = q
    t2 = time.clock()
    return t2 - t1

def multipleassignment():
    x = 1
    y = 0

    t1 = time.clock()
    for i in r:
	for j in r:
	    x, y = y, x
    t2 = time.clock()
    return t2 - t1

t = traditional()
print '%d traditional swaps in %g seconds' % (num * num, t)

t = multipleassignment()
print '%d multi-assignment swaps in %g seconds' % (num * num, t)

