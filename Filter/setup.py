# Sketch - A Python-based interactive drawing program
# Copyright (C) 2003 by Bernhard Herzog
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""Distutils setup script for the stream filters."""

__version__ = "$Revision$"
# $Source$
# $Id$

import os
from distutils.core import setup, Extension
import distutils

from string import split
import string

minimal_filter_files = ["streamfilter.c", "filterobj.c", "linefilter.c",
                        "subfilefilter.c", "base64filter.c",
                        "nullfilter.c", "stringfilter.c", "binfile.c",
                        "hexfilter.c"]

extra_filter_files = ["zlibfilter.c", "ascii85filter.c"]

def define_extensions(with_extra_filters,
                      dir_prefix = "", module_prefix = ""):
    define_macros = []
    libraries = []
    files = minimal_filter_files
    if with_extra_filters:
        define_macros.append(("ALL_FILTERS", None))
        libraries.append("z")
        files.extend(extra_filter_files)

    return [Extension(module_prefix + "streamfilter",
                      [dir_prefix + file for file in files],
                      define_macros = define_macros,
                      libraries = libraries)]


if __name__ == "__main__":
    setup(name = "Stream Filters",
          version = "CVS",
          description = "Stream filters",
          long_description = "Stream filters",
          licence = "LGPL",
          author = "Bernhard Herzog",
          author_email = "bh@intevation.de",
          url = "http://sketch.sourceforge.net/",

          ext_modules = define_extensions(1))
